#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

PLUGIN_NAME="${PLUGIN_NAME:-diablo}"
RESULTS_DIR="${RESULTS_DIR:-/tmp/results}"

mkdir -p e2e-reports
# run tests
if [ $CASE_TYPE == "diablo" ]
then
    yarn e2e:retry
elif [ $CASE_TYPE == "asf" ]
then
    yarn e2e:asf-retry
elif [ $CASE_TYPE == "all" ]
then
    yarn e2e:full-retry
else
    echo "CASE_TYPE is erorr"
fi
cp ./e2e-reports/*.xml ${RESULTS_DIR}
# cp test.log ${RESULTS_DIR}
# cat test.log

# Gather results into one file
cd ${RESULTS_DIR}
tar -czf ${PLUGIN_NAME}.tar.gz *

# Let the sonobuoy worker know the job is done
echo -n "${RESULTS_DIR}/${PLUGIN_NAME}.tar.gz" >"${RESULTS_DIR}/done"
