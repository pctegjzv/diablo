/**
 * 管理配置文件
 * Created by liuwei on 2018/3/2.
 */

const conf = {
  TESTDATAPATH: process.cwd() + '/e2e/test_data/',
  TESTIMAGE: `${process.env.TESTIMAGE ||
    'index.alauda.cn/alaudaorg/qaimages:helloworld'}`,
  JENKINSURL: `${process.env.JENKINSURL || 'http://10.105.83.243:8080'}`,
  USER_TOKEN: `${process.env.USER_TOKEN || 'false'}`,
  CLUSTER_MASTER_IP: `${process.env.CLUSTER_MASTER_IP || '127.0.0.1'}`,
};

export const ServerConf = conf;
