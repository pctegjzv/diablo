/**
 * 封装更新资源，更新注释 页面类，包含key, value 的修改和增加功能
 * Created by liuwei on 2018/8/7.
 */

import { $, by } from 'protractor';

import { AuiSelect } from '../element_objects/alauda.auiSelect';
import { AuiSwitch } from '../element_objects/alauda.auiSwitch';
import { AlaudaButton } from '../element_objects/alauda.button';
import { AlaudaInputbox } from '../element_objects/alauda.inputbox';
import { KeyValueForm } from '../element_objects/alauda.keyValueForm';
import { AlaudaRadioButton } from '../element_objects/alauda.radioButton';
import { AlaudaTable } from '../element_objects/alauda.table';

export class VolumeMount {
  private _titleSelector: string;
  private _updateButtonSelector: string;
  private _cancelButtonSelector: string;

  constructor(
    titleSelector: string = 'aui-dialog-header .aui-dialog__header-title',
    updateButtonSelector: string = '.aui-dialog__footer div button[aui-button="primary"]',
    cancelButtonSelector: string = '.aui-dialog__footer div button[aui-button=""]',
  ) {
    this._titleSelector = titleSelector;
    this._updateButtonSelector = updateButtonSelector;
    this._cancelButtonSelector = cancelButtonSelector;
  }

  /**
   * dialog 页面title
   * @return {ElementFinder}
   */
  get title() {
    return $(this._titleSelector);
  }

  /**
   * 添加存储卷挂载的类型，（配置字段，保密字典，主机路径）
   */
  get volumeType() {
    return new AlaudaRadioButton();
  }

  /**
   * 配置字典
   */
  get configmap() {
    return new AuiSelect(
      $('aui-select[name="configmap"] input'),
      $('aui-select[name="configmap"] span'),
      'aui-tooltip aui-option div',
    );
  }

  /**
   * 单独引用
   */
  get switch() {
    return new AuiSwitch();
  }

  /**
   * key value form
   */
  get keyValueForm() {
    return new KeyValueForm();
  }

  /**
   * 容器路径
   */
  get containerPath() {
    return new AlaudaInputbox(by.css('input[name="containerpath"]'));
  }

  /**
   * 保密字典
   */
  get secret() {
    return new AuiSelect(
      $('aui-select[name="secret"] input'),
      $('aui-select[name="secret"] span'),
      'aui-tooltip aui-option div',
    );
  }

  /**
   * 主机路径
   */
  get hostPath() {
    return new AlaudaInputbox(by.css('input[name="hostpath"]'));
  }

  /**
   * 更新按钮
   * @return {AlaudaButton}
   */
  get buttonAdd() {
    return new AlaudaButton(by.css(this._updateButtonSelector));
  }

  /**
   * 取消按钮
   * @return {AlaudaButton}
   */
  get buttonCancel() {
    return new AlaudaButton(by.css(this._cancelButtonSelector));
  }

  get tableMount() {
    return new AlaudaTable(
      'tbody tr th',
      '.volume-info-container>table>tbody>tr>td',
      '.volume-info-container>table>tbody>tr',
    );
  }

  mount(type: string, source: string, containerPath: string) {
    this.volumeType.clickByName(type);
    switch (type) {
      case '配置字典':
        this.configmap.select(source);
        this.containerPath.input(containerPath);
        break;
      case '保密字典':
        this.secret.select(source);
        this.containerPath.input(containerPath);
        break;
      case '主机路径':
        this.hostPath.input(source);
        this.containerPath.input(containerPath);
        break;
    }
    this.buttonAdd.click();
  }

  refMount(type: string, source: string, arrayValue) {
    this.volumeType.clickByName(type);
    this.switch.open();

    switch (type) {
      case '配置字典':
        this.configmap.select(source);
        break;
      case '保密字典':
        this.secret.select(source);
        break;
    }
    this.keyValueForm.newValues(arrayValue);
    this.buttonAdd.click();
  }
}
