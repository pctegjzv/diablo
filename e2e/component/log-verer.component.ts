/**
 * Created by liuwei on 2018/7/19.
 *
 */
import { $$, ElementFinder, browser, by, element } from 'protractor';

import { AuiSelect } from '../element_objects/alauda.auiSelect';
import { AlaudaCheckBox } from '../element_objects/alauda.checkbox';
import { AlaudaInputbox } from '../element_objects/alauda.inputbox';
import { PageBase } from '../page_objects/page.base';

export class Logviewer extends PageBase {
  private _getAuiSelect(name: string) {
    const item = $$('alo-application-log aui-form-field')
      .filter(elem => {
        return elem
          .$('.aui-form-field__label')
          .getText()
          .then(text => {
            return text.trim() === name;
          });
      })
      .first();
    this.waitElementPresent(item, '日志 aui-select 控件没有正常加载', 30000);

    return new AuiSelect(item.$('aui-select'), item.$('input'));
  }
  /**
   * 资源名称的下拉框
   */
  get resourceName() {
    return this._getAuiSelect('资源名称');
  }

  /**
   * 容器组的下拉框
   */
  get containerGroup() {
    return this._getAuiSelect('容器组');
  }

  /**
   * 容器的下拉框
   */
  get container() {
    return this._getAuiSelect('容器');
  }

  /**
   * 日志文本框
   */
  get logTextarea() {
    return new AlaudaInputbox(by.css('.overflow-guard .inputarea'));
  }

  /**
   * 查找工具栏的文本框
   */
  get finder_inputbox() {
    return new AlaudaInputbox(
      by.css('.monaco-findInput .monaco-inputbox input'),
    );
  }

  /**
   * 自动更新checkbox
   */
  get checkBoxAutoRefresh() {
    return new AlaudaCheckBox(
      '自动更新',
      '.alo-log-view__toolbar aui-checkbox',
    );
  }

  /**
   * 查找下拉工具栏的文本框右侧的查找结果
   */
  get resultfinder(): ElementFinder {
    return element(by.css('.find-widget .matchesCount'));
  }

  /**
   * 判断日志是否是夜间模式
   */
  isNightMode() {
    return element(by.css('alo-log-view .alo-log-view'))
      .getAttribute('class')
      .then(function(css) {
        return css.includes('--dark');
      });
  }

  /**
   * 获取所有行号
   */
  get lineNumber() {
    return element.all('alo-log-view .alo-log-view .line-numbers');
  }

  /**
   * 获取日志查询时间
   */
  get logqueryTime() {
    return this.getToolbarButton('时间').getText();
  }

  waitLogRefresh() {
    this.logqueryTime.then(logTime => {
      browser.driver.wait(() => {
        return this.logqueryTime.then(text => {
          return text.trim() !== String(logTime).trim();
        });
      }, 60000);
    });
  }

  /**
   * 等待日志显示
   * @param expectText 日志的部分内容，
   * @param timeout 超时时间
   */
  waitLogDisplay(expectText: string = 'hehe.txt', timeout = 120000) {
    return browser.driver
      .wait(() => {
        return this.logTextarea.value.then(text => {
          return text.trim().includes(String(expectText).trim());
        });
      }, timeout)
      .then(
        () => true,
        () => {
          this.logTextarea.value.then(text => {
            console.log(
              `容器中的日志没有包含${expectText} \n此时日志显示的是： ${text}`,
            );
          });
          return false;
        },
      );
  }

  /**
   * 获得工具栏上的按钮
   * @param innerText 工具栏button 的文本
   */
  getToolbarButton(innerText = '首页'): any {
    let elembuttonList = element.all(by.css('.alo-log-view__pages a'));
    switch (innerText) {
      case '首页':
        return elembuttonList.get(0);
      case '上一页':
        return elembuttonList.get(1);
      case '下一页':
        return elembuttonList.get(2);
      case '最后一页':
        return elembuttonList.get(3);
      case '时间':
        return element(by.css('.alo-log-view__toolbar>span'));
      case '自动更新':
        return new AlaudaCheckBox('自动更新', 'alo-log-view aui-checkbox');
    }
    elembuttonList = element.all(by.css('.alo-log-view__tools a'));
    return elembuttonList.filter(elem => {
      return elem.getText().then(text => {
        return text.trim() === innerText;
      });
    });
  }
}
