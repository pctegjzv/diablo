/**
 * 资源列表的页面类, 包含单击资源名进入详情页， 单击右侧选择操作，验证排序等功能
 * Created by liuwei on 2018/3/1.
 *
 */
import { $, ElementFinder, browser, by, element, promise } from 'protractor';

import { AloSearch } from '../element_objects_new/alauda.aloSearch';
import { AlaudaAuiTable } from '../element_objects_new/alauda.aui_table';
import { AlaudaDropdown } from '../element_objects_new/alauda.dropdown';

export class AuiTableComponent extends AlaudaAuiTable {
  private _searchBox: any;

  constructor(
    root: ElementFinder = $('alo-card-section'), // 包含检索框和aui-table 控件的元素
    tableSelector = '.alo-card__section-body aui-table',
    searchBoxSelector = '.alo-search', // 检索框的父元素
  ) {
    super(root.$(tableSelector));
    this._searchBox = new AloSearch($(searchBoxSelector));
  }

  /**
   * 数据加载中的控件
   */
  get loading(): ElementFinder {
    return $('.list-status-loading');
  }

  /**
   * 重写父类的方法，单击表头后等待提示消失
   * @param headername 表头列名
   */
  clickHeaderByName(headername): promise.Promise<void> {
    super.clickHeaderByName(headername);
    this.waitElementPresent(this.loading, 60000);
    return browser.sleep(100);
  }

  /**
   * 单击资源名称进入详情页
   * @param keys 根据关键字keys, 找到资源列表的一行
   * @param coulmeName 列名
   */
  clickResourceNameByRow(
    keys,
    coulmeName: string = '名称',
  ): promise.Promise<void> {
    this.getCell(coulmeName, keys).then(cellElem => {
      const nameItem = cellElem.$('a');
      this.waitElementPresent(nameItem);
      nameItem.click();
      this.waitProgressBarNotPresent();
    });

    return browser.sleep(100);
  }

  /**
   * 根据关键字keys, 找到资源列表的一行， 根据columeName 找到所在列的单元格里面的标签
   * @param keys [podname2317, namespace2317, 'Pod']
   * @return {any}
   */
  getCellLabel(keys, columeName = '名称'): promise.Promise<string> {
    return this.getCell(columeName, keys).then(elem => {
      return elem.$$(`.plain-container__label`).getText();
    });
  }

  /**
   * 根据关键字keys, 找到资源列表的一行，单击操作按钮
   * @param keys [podname2317, namespace2317, 'Pod']
   * @param actionName 操作的名字，例如 '更新'， '更新标签'， '更新注解'， '删除'
   * @return {any}
   */
  clickOperationButtonByRow(
    keys,
    actionName,
    iconButtonSelector = 'alo-menu-trigger aui-icon',
    itemListSelector = 'aui-menu-item button',
  ): promise.Promise<void> {
    const row = this.getRow(keys);
    const option = new AlaudaDropdown(
      row.$(iconButtonSelector),
      row.$$(itemListSelector),
    );
    return option.select(actionName);
  }

  /**
   * 重写父类的方法 getColumeTextByName, 验证排序的方法会依赖这个
   * @param name 表格的列的名称
   */
  getColumeTextByName(name): promise.Promise<string> {
    return this.getColumeIndexByColumeName(name).then(tableInfo => {
      const index = (tableInfo.columeIndex + 1).toString();
      switch (name) {
        case '名称':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//a'))
            .getText();
        case '创建时间':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//span'))
            .getAttribute('title');
        default:
          return super.getColumeTextByName(name);
      }
    });
  }

  /**
   * 表格右上角的检索框，子类不一样时需要重载
   */
  get aloSearch() {
    return this._searchBox;
  }

  /**
   * 检索资源
   * @param name 检索的资源名称
   * @param rowCount 期望检索到的资源数量
   */
  searchByResourceName(
    name: string,
    rowCount: number = 1,
    searchType: string = '名称',
  ): promise.Promise<void> {
    this.aloSearch.search(name, searchType);
    browser.driver.wait(() => {
      return this.getRowCount().then(count => {
        return count === rowCount;
      });
    });
    return browser.sleep(100);
  }

  /**
   * 表格为空时的显示, 子类不一样时需要重载
   */
  get noResult(): ElementFinder {
    return $('.aui-card__content .no-data span');
  }
}
