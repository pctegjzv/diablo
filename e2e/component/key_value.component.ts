/**
 * 封装更新资源，更新注释 页面类，包含key, value 的修改和增加功能
 * Created by liuwei on 2018/3/16.
 */

import { $, $$, browser } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class KeyValueControl {
  private _titleSelector: string;
  private _addButtonSelector: string;
  private _keyValueRowSelector: string;
  private _keySelector: string;
  private _valueSelector: string;
  private _updateButtonSelector: string;
  private _cancelButtonSelector: string;

  constructor(
    titleSelector: string = 'aui-dialog-header .aui-dialog__header-title',
    addButtonSelector: string = 'alo-key-value-form button[aui-button="primary"]',
    keyValueRowSelector: string = 'alo-key-value-form .key-value-form-row',
    keySelector: string = 'input[placeholder="键"]',
    valueSelector: string = 'input[placeholder="值"]',
    updateButtonSelector: string = '.aui-dialog__footer div button[aui-button="primary"]',
    cancelButtonSelector: string = '.aui-dialog__footer div button[aui-button=""]',
  ) {
    this._titleSelector = titleSelector;
    this._addButtonSelector = addButtonSelector;
    this._keyValueRowSelector = keyValueRowSelector;
    this._keySelector = keySelector;
    this._valueSelector = valueSelector;
    this._updateButtonSelector = updateButtonSelector;
    this._cancelButtonSelector = cancelButtonSelector;
  }

  /**
   * 更新资源标签，更新注解，的 dialog 页面title
   * @return {ElementFinder}
   */
  get title() {
    return $(this._titleSelector);
  }

  /**
   * 修改一个已经存在的值
   * @param key
   * @param value
   */
  setValue(key: string, value: string) {
    // 找到所有文本框
    const rows = $$(`${this._keyValueRowSelector}`);

    // 查找值等于key的文本框
    const parentRow = rows
      .filter(row => {
        return row
          .$(this._keySelector)
          .getAttribute('value')
          .then(text => {
            return text.trim() === key;
          });
      })
      .first();

    // 在找到的文本框中，输入value
    const inputboxValue = parentRow.$(this._valueSelector);
    inputboxValue.clear();
    inputboxValue.sendKeys(value);
    browser.sleep(100);
  }

  /**
   * 新增一个值
   * @param key
   * @param value
   */
  newValue(key: string, value: string) {
    // 找到【+】 按钮，注：页面中只有一个【+】按钮
    const addbutton = $(this._addButtonSelector);

    // 单击 【+】 按钮后，页面新增2个空白文本框，要求输入key, value
    addbutton.click();
    browser.sleep(100);

    const rows = $$(this._keyValueRowSelector);
    rows.count().then(count => {
      const index = count * 2 - 1;
      const parent = $(`${this._keyValueRowSelector}:nth-child(${index})`);
      const keyElem = parent.$(this._keySelector);
      const valueElem = parent.$(this._valueSelector);
      keyElem.clear();
      keyElem.sendKeys(key);
      browser.sleep(100);

      valueElem.clear();
      valueElem.sendKeys(value);
      browser.sleep(100);
    });
  }

  /**
   * 更新按钮
   * @return {AlaudaButton}
   */
  get buttonUpdate() {
    return $(this._updateButtonSelector);
  }

  /**
   * 单击更新按钮
   * @return {any}
   */
  clickUpdate() {
    CommonPage.waitElementPresent(this.buttonUpdate);
    this.buttonUpdate.click();
    // 单击成功后等待元素消失
    CommonPage.waitElementNotPresent(this.buttonUpdate);
  }

  /**
   * 取消按钮
   * @return {AlaudaButton}
   */
  get buttonCancel() {
    return $(this._cancelButtonSelector);
  }

  /**
   * 单击取消按钮
   * @return {any}
   */
  clickCancel() {
    this.buttonCancel.click();
    return CommonPage.waitElementNotPresent(this.buttonCancel);
  }
}
