/**
 * AloSearch控件
 * Created by liuwei on 2018/8/10.
 */
import {
  $,
  $$,
  ElementArrayFinder,
  ElementFinder,
  browser,
  promise,
} from 'protractor';

import { AuiSearch } from './alauda.auiSearch';
import { AlaudaDropdown } from './alauda.dropdown';

export class AloSearch {
  private _aloSearch: ElementFinder;
  private _iconButton: ElementFinder;
  private _itemList: ElementArrayFinder;
  private _auiSearchSelector;

  constructor(
    aloSearch: ElementFinder,
    auiSearchSelector: string = 'aui-search',
    iconButton: ElementFinder = $('.alo-search button'),
    itemList: ElementArrayFinder = $$('aui-menu-item button'),
  ) {
    this._aloSearch = aloSearch;
    this._iconButton = iconButton;
    this._itemList = itemList;
    this._auiSearchSelector = auiSearchSelector;
  }

  /**
   * 检索框左侧的dropdwon 控件
   */
  get dropdown() {
    return new AlaudaDropdown(this._iconButton, this._itemList);
  }

  /**
   * 检索框控件
   */
  get auiSearch() {
    return new AuiSearch(this._aloSearch.$(this._auiSearchSelector));
  }

  /**
   * 检索
   * @param selectItem 要检索的类型
   * @param searchItem 要检索的值
   */
  search(
    searchItem: string,
    selectItem: string = '名称',
  ): promise.Promise<void> {
    return this.dropdown.isPresent().then(isPresent => {
      if (isPresent) {
        // 如果 dropdown 控件存在，选择一个选项
        this.dropdown.select(selectItem);
        browser.sleep(2000);
        // 在检索框中输入内容检索
        this.auiSearch.search(searchItem);
        return browser.sleep(100);
      } else {
        // 在检索框中输入内容检索
        this.auiSearch.search(searchItem);
        return browser.sleep(100);
      }
    });
  }
}
