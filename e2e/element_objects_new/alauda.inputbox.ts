/**
 * 文本框控件
 * Created by liuwei on 2018/8/13.
 */

import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaInputbox extends AlaudaElementBase {
  private _inputBox: ElementFinder;

  constructor(inputBox: ElementFinder) {
    super();
    this._inputBox = inputBox;
  }

  /**
   * 获得文本框
   */
  get inputbox(): ElementFinder {
    this.waitElementPresent(this._inputBox);
    return this._inputBox;
  }

  /**
   * 判断Inputbox是否显示
   */
  isPresent(): promise.Promise<boolean> {
    return this.inputbox.isPresent();
  }

  /**
   * 在文本框中输入一个值
   * @param inputValue 要输入的值
   */
  input(inputValue: string): promise.Promise<void> {
    this.inputbox.clear();
    this.inputbox.sendKeys(inputValue);
    return browser.sleep(100);
  }

  /**
   * 获取文本框的值
   */
  getText(): promise.Promise<string> {
    return this.inputbox.getText();
  }

  /**
   * 获取文本框的值
   */
  get value(): promise.Promise<string> {
    return this.inputbox.getAttribute('value');
  }
}
