/**
 * aui-dailog 控件
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AlaudaButton } from './alauda.button';
import { AlaudaElementBase } from './element.base';

export class AuiDialog extends AlaudaElementBase {
  private _auiDialog: ElementFinder;
  constructor(auiDialog: ElementFinder) {
    super();
    this._auiDialog = auiDialog;
  }

  /**
   * 获得dialog控件
   */
  get auiDialog(): ElementFinder {
    this.waitElementPresent(this._auiDialog);
    return this._auiDialog;
  }

  /**
   * Dialog 控件的标题
   */
  get title(): ElementFinder {
    return this.auiDialog.$('aui-dialog-header .aui-dialog__header-title');
  }

  get aui_icon(): AuiIcon {
    return new AuiIcon(this.auiDialog.$('aui-dialog-header aui-icon'));
  }

  /**
   * Dialog 控件的内容
   */
  get content(): any {
    return this.auiDialog.$('.aui-dialog__content');
  }

  /**
   * 确定按钮
   */
  get buttonConfirm(): AlaudaButton {
    return new AlaudaButton(
      this.auiDialog.$('.aui-dialog__footer button[aui-button="primary"]'),
    );
  }

  /**
   * 取消按钮
   */
  get buttonCancel(): AlaudaButton {
    return new AlaudaButton(
      this.auiDialog.$('.aui-dialog__footer button[aui-button=""]'),
    );
  }

  /**
   * 单击确定按钮
   */
  clickConfirm(): promise.Promise<void> {
    this.buttonConfirm.click();
    this.waitProgressBarNotPresent();
    return browser.sleep(100);
  }

  /**
   * 单击取消按钮
   */
  clickCancel(): promise.Promise<void> {
    this.buttonCancel.click();
    return browser.sleep(100);
  }
}
