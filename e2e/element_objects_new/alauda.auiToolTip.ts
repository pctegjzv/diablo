/**
 * alaudaSwitch 列表控件
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AuiToolTip extends AlaudaElementBase {
  private _auiTooltip: ElementFinder;
  constructor(auiToolTip: ElementFinder) {
    super();
    this._auiTooltip = auiToolTip;
  }

  /**
   * 获得开关控件
   */
  get auiToolTip(): ElementFinder {
    this.waitElementPresent(this._auiTooltip);
    return this._auiTooltip;
  }

  get tagName(): promise.Promise<string> {
    // 找到要操做的item
    const item = this.auiToolTip.$(
      '.aui-option-container__content>*:first-child',
    );
    return this.waitElementPresent(item).then(isPresent => {
      if (isPresent) {
        return item.getTagName();
      }
      return null;
    });
  }

  /**
   * 打开开关
   */
  selectAui_option(value: string): promise.Promise<void> {
    // 找到要操做的item
    const item = this.auiToolTip
      .$$('.aui-option-container__content aui-option>div')
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === value;
        });
      })
      .first();
    this.waitElementPresent(item);
    item.click();
    // 等待item 消失
    this.waitElementNotPresent(item);
    return browser.sleep(100);
  }

  selectAui_option_group(key: string, value: string): promise.Promise<boolean> {
    // 找到要操做的item
    const optionGroup = this.auiToolTip
      .$$('.aui-option-container__content aui-option-group')
      .filter(elem => {
        return elem
          .$('.aui-option-group__title')
          .getText()
          .then(text => {
            return text.trim() === key;
          });
      })
      .first();
    this.waitElementPresent(optionGroup);
    // 找到选择项，单击
    const item = optionGroup
      .$$('aui-option>div')
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === value;
        });
      })
      .first();
    item.click();
    browser.sleep(100);
    return item.isPresent();
  }
}
