/**
 * 下拉框列表控件
 * Created by liuwei on 2018/8/13.
 */
import { $, ElementFinder, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaElementBase {
  /**
   * 面包屑下边的进度条
   */
  get progressbar(): ElementFinder {
    return $('div[class="global-loader loading"]');
  }

  /**
   * progressbar 出现后，等待消失
   */
  waitProgressBarNotPresent(): promise.Promise<boolean> {
    return this.progressbar.isPresent().then(isPresent => {
      if (isPresent) {
        return this.waitElementNotPresent(this.progressbar, 60000);
      }
    });
  }
  /**
   * 等待页面元素出现
   * @param elem 页面元素
   * @param timeout 超时时间
   */
  waitElementPresent(
    elem: ElementFinder,
    timeout: number = 20000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementPresent(elem, timeout);
  }

  /**
   * 等待页面元素不出现
   * @param elem 页面元素
   * @param timeout 超时时间
   */
  waitElementNotPresent(
    elem: ElementFinder,
    timeout: number = 20000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementNotPresent(elem, timeout);
  }
}
