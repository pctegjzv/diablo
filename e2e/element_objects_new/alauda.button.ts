/**
 * 按钮控件
 * Created by liuwei on 2018/8/13.
 */

import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaButton extends AlaudaElementBase {
  private _button;

  constructor(button: ElementFinder) {
    super();
    this._button = button;
  }

  get button(): ElementFinder {
    this.waitElementPresent(this._button);
    return this._button;
  }

  /**
   * 判断按钮是否存在
   */
  isPresent(): promise.Promise<boolean> {
    return this.button.isPresent();
  }

  /**
   * 单击按钮
   */
  click(): promise.Promise<void> {
    this.button.click();
    return browser.sleep(100);
  }

  /**
   * 获得按钮的文字
   */
  getText(): promise.Promise<string> {
    return this.button.getText();
  }
}
