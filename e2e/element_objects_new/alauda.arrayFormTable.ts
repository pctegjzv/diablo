/**
 * aui-select 下拉框列表控件
 * Created by liuwei on 2018/8/13.
 */
import { $, ElementFinder, browser, promise } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AuiSelect } from './alauda.auiSelect';
import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaElementBase } from './element.base';

export class ArrayFormTable extends AlaudaElementBase {
  private _alkArrayFormTable: ElementFinder;

  /**
   * 构造函数
   * @param inputbox ElementFinder 类型，inputbox 页面元素
   * @param icon ElementFinder 类型，inputbox 页面元素右侧的小图标，单击列出所有下拉项
   * @param itemlistselector string， 所有下拉项的css 选择器
   */
  constructor(alkArrayFormTable: ElementFinder) {
    super();
    this._alkArrayFormTable = alkArrayFormTable;
  }

  /**
   * 获得alk-array-from-table 控件
   */
  get alkArrayFormTable(): ElementFinder {
    this.waitElementPresent(this._alkArrayFormTable);
    return this._alkArrayFormTable;
  }

  /**
   * 获得表头的文字
   */
  get headerText(): promise.Promise<string> {
    return this.alkArrayFormTable.$$('th').getText();
  }

  /**
   * 填写表格
   * @param parameters
   */
  fill(parameters: Array<any>) {
    this.alkArrayFormTable
      .$$('tbody>tr>td>*:first-child')
      .each((elem, index) => {
        elem.getTagName().then(tagName => {
          if (Array.isArray(parameters[index])) {
            this._fillForm(
              tagName,
              elem,
              parameters[index][0],
              parameters[index][1],
            );
          } else {
            this._fillForm(
              tagName,
              elem,
              parameters[index],
              parameters[index + 1],
            );
          }
        });
      });
  }

  /**
   * 单击控件底部的button
   * @param buttonInnerText button上的显示文字
   */
  click(buttonInnerText: string = '添加'): promise.Promise<void> {
    // 找到要操作的按钮
    const bottonButtons = this.alkArrayFormTable
      .$('.bottom-control-buttons')
      .$$('button');
    const button = bottonButtons
      .filter(elem => {
        return elem.getText().then(text => {
          return text === buttonInnerText;
        });
      })
      .first();
    // 等待按钮出现
    this.waitElementPresent(button).then(isPresent => {
      if (!isPresent) {
        console.log(`要单击的button元素【${buttonInnerText}】没出现`);
      }
    });
    // 单击按钮
    button.click();
    return browser.sleep(100);
  }

  private _fillForm(
    tagName: string,
    elem: ElementFinder,
    key: string,
    value: string,
  ) {
    switch (tagName) {
      case 'input':
        const inputbox = new AlaudaInputbox(elem);
        inputbox.input(key);
        break;
      case 'aui-select':
        const auiSelect = new AuiSelect(
          elem,
          $('.cdk-overlay-container aui-tooltip'),
        );
        auiSelect.select(key, value);
        break;
      case 'aui-icon':
        const auiIcon = new AuiIcon(elem);
        if (String(key).toLowerCase() === 'true') {
          auiIcon.click();
        }
        break;
    }
  }
}
