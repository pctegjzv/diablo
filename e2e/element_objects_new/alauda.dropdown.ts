/**
 * 下拉框列表控件
 * Created by liuwei on 2018/3/14.
 */
import {
  ElementArrayFinder,
  ElementFinder,
  browser,
  promise,
} from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaDropdown extends AlaudaElementBase {
  private _iconButton: ElementFinder;
  private _itemlist: ElementArrayFinder;

  constructor(iconButton: ElementFinder, itemlist: ElementArrayFinder) {
    super();
    this._iconButton = iconButton;
    this._itemlist = itemlist;
  }

  /**
   * dropdown 是否存在
   */
  isPresent(): promise.Promise<boolean> {
    return this._iconButton.isPresent();
  }

  /**
   * 单击下拉框， 选择值
   * @param selectvalue 从下拉框中要选择的值
   */
  select(selectvalue): promise.Promise<void> {
    this.waitElementPresent(this._iconButton);
    this._iconButton.click();
    browser.sleep(100);
    const item = this._itemlist
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === selectvalue;
        });
      })
      .first();

    // 找到要选择的元素, 单击
    this.waitElementPresent(item, 60000);
    item.click();

    this._itemlist.count().then(count => {
      if (count > 0) {
        // 如果下拉框任然显示，单击icon按钮隐藏
        this._iconButton.click();
      }
    });
    return browser.sleep(100);
  }
}
