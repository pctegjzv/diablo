/**
 * alaudaSwitch 列表控件
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AuiSwitch extends AlaudaElementBase {
  private _auiSwitch: ElementFinder;
  constructor(auiSwitch: ElementFinder) {
    super();
    this._auiSwitch = auiSwitch;
  }

  /**
   * 获得开关控件
   */
  get auiSwitch(): ElementFinder {
    this.waitElementPresent(this._auiSwitch);
    return this._auiSwitch;
  }

  /**
   * 打开开关
   */
  open(checkedSelector: string = 'aui-switch--checked'): promise.Promise<void> {
    return this.auiSwitch
      .$(checkedSelector)
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          this.auiSwitch.click();
        }
        return browser.sleep(100);
      });
  }

  /**
   * 关闭开关
   */
  close(
    checkedSelector: string = 'aui-switch--checked',
  ): promise.Promise<void> {
    return this.auiSwitch
      .$(checkedSelector)
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          this.auiSwitch.click();
        }
        return browser.sleep(100);
      });
  }
}
