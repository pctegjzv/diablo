/**
 * aui-select 下拉框列表控件
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, promise } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AuiToolTip } from './alauda.auiToolTip';
import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaElementBase } from './element.base';

export class AuiSelect extends AlaudaElementBase {
  private _auiSelect: ElementFinder;
  private _auiToolTip: ElementFinder;

  /**
   * 构造函数
   * @param auiSelect ElementFinder 类型，auiSelect 页面元素
   * @param auiToolTip ElementFinder 类型, auiToolTip 页面元素
   */
  constructor(auiSelect: ElementFinder, auiToolTip: ElementFinder) {
    super();
    this._auiSelect = auiSelect;
    this._auiToolTip = auiToolTip;
  }

  /**
   * Aui-select 控件的文本框
   */
  get inputBox(): AlaudaInputbox {
    return new AlaudaInputbox(this._auiSelect.$('input'));
  }

  /**
   * Aui-select 控件的下拉框
   */
  get auiToolTip(): AuiToolTip {
    return new AuiToolTip(this._auiToolTip);
  }

  /**
   * Aui-select 控件的aui-icon 控件
   */
  get auiIcon(): AuiIcon {
    return new AuiIcon(this._auiSelect.$('aui-icon'));
  }

  /**
   * 获得文本框中的输入值
   */
  get value(): promise.Promise<string> {
    return this.inputBox.value;
  }

  /**
   * 单击 icon, 从下拉框中选择一个item
   * @param text 要选择的item
   * @param itemListSelector string， 所有下拉项的css 选择器
   */
  select(key: string, value: string = ''): promise.Promise<void> {
    this.auiIcon.click();
    return this.auiToolTip.tagName.then(tagName => {
      switch (tagName) {
        case 'aui-option-group':
          this.auiToolTip.selectAui_option_group(key, value);
          break;
        case 'aui-option':
          this.auiToolTip.selectAui_option(key);
          break;
      }
    });
  }
}
