/**
 * Created by liuwei on 2018/3/8.
 */

import { CommonKubectl } from './common.kubectl';
import { CommonMethod } from './common.method';

export class CommonApi {
  /**
   * 检查资源是否存在
   *
   * @parameter {items} [kubectl get configmap -o json] 命令获得的返回体里面的items
   * "apiVersion": "v1",
    "items": [
        {
            "apiVersion": "v1",
            "data": {
                "cm1": "cm1",
                "cm2": "cm2"
            },
   * @parameter {name} string 资源名称
   *
   */
  private static _exist(items, name, namespace) {
    for (const item of items) {
      if (item.metadata.name === name) {
        if (
          (namespace && item.metadata.namespace === namespace) ||
          !namespace
        ) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * 执行kubectl get 命令，返回json 格式
   *
   * @parameter {kind} string 类型，资源类型
   * @return 命令执行成功，解析成JSON 格式返回，命令执行失败返回字符串格式的错误信息
   */
  private static _queryResource(kind) {
    const cmdResult = CommonKubectl.execKubectlCommand(
      'kubectl get ' + kind + ' --all-namespaces -o json',
    );
    try {
      return JSON.parse(cmdResult);
    } catch (exc) {
      return cmdResult;
    }
    // if (!cmdResult.includes('Error')) {
    //   return JSON.parse(cmdResult);
    // }
    // return cmdResult;
  }

  /**
   * 检查资源是否存在，如果不存在则创建
   *
   * @parameter {filename} string 类型
   * @parameter {parameter} 字典
   * @parameter {kind} string 类型
   * @parameter {namespace} string 类型
   *
   * @example createResource('namespace.yaml', {'$NAME': namespace1, '$LABEL': namespace1}, 'namespace', 'default');
   */
  static createResource(filename, parameter, kind, namespace) {
    if (!CommonApi.isExistResource(parameter['${NAME}'], kind, namespace)) {
      // 资源不存在创建资源
      CommonKubectl.createResource(filename, parameter, 'temp');

      // 资源创建后，检查资源是否存在，30秒 超时
      let timeout = 1;
      while (
        !CommonApi.isExistResource(parameter['${NAME}'], kind, namespace)
      ) {
        CommonMethod.sleep(1000);
        timeout++;
        if (timeout > 30) {
          break;
        }
      }
    }
  }

  /**
   * 检查资源是否存在，如果存在则删除
   *
   * @parameter {name} string 类型
   * @parameter {kind} string 类型
   * @parameter {namespace} string 类型
   *
   * @example deleteResource('example', 'secret', 'default');
   */
  static deleteResource(name, kind, namespace) {
    if (CommonApi.isExistResource(name, kind, namespace)) {
      // 如果资源存在，删除资源
      // console.log('删除命令是 ： kubectl delete ' + kind + ' ' + name + ' -n ' + namespace);
      CommonKubectl.execKubectlCommand(
        'kubectl delete ' + kind + ' ' + name + ' -n' + namespace,
      );
      CommonMethod.sleep(1000);

      let timeout = 0;
      // 每隔一秒查询一次如果存在继续查询，查询60秒后超时退出
      while (CommonApi.isExistResource(name, kind, namespace)) {
        CommonMethod.sleep(1000);
        timeout++;
        if (timeout > 60) {
          break;
        }
      }
    }
  }

  /**
   * 检查资源是否存在
   *
   * @parameter {name} string 资源名称
   * @parameter {kind} string 资源类型
   *
   * @example isExistResource('alauda-deployment-test2250','deploy')
   *
   * @return 布尔类型，true，存在，false 不存在
   */
  static isExistResource(name, kind, namespace) {
    const isExist = CommonApi._queryResource(kind);
    if (typeof isExist === 'object') {
      return CommonApi._exist(
        CommonApi._queryResource(kind).items,
        name,
        namespace,
      );
    }
    return false;
  }

  /**
   * 删除测试包含前缀的测试数据
   * @param kind k8s 资源类型
   * @param prefix 测试数据前缀
   */
  static clearTestData(kind, prefix) {
    const namespacelist = CommonMethod.parseYaml(
      CommonMethod.execCommand(`kubectl get ${kind} -o yaml`),
    );
    for (const iterator of namespacelist.items) {
      const name = iterator.metadata.name;
      if (name.includes(prefix)) {
        console.log(`==========> clear ${kind} test data`);
        console.log(CommonMethod.execCommand(`kubectl delete ${kind} ${name}`));
      }
    }
  }
}
