/**
 * Created by liuwei on 2018/2/28.
 */
import { execSync } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';

import { ServerConf } from '../config/serverConf';

export class CommonMethod {
  static isClearTestData = true;
  /**
   * 比较两个字符串变量的大小
   * stringObject < target 返回 1
   * stringObject > target 返回 -1
   *
   * @parameter {stringObject} string 变量
   * @parameter {target} string 量
   */
  static stringDown(stringObject, target) {
    return stringObject.localeCompare(target) < 0 ? 1 : -1;
  }

  /**
   * 读取YAML（{fileName}）文件，返回文件内容
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {parameter} 键值对格式，键写在YAML文件中 例如： { '$NAME': 20, '$PASSWORD': 30 }
   *
   * @example  readyamlfile('../test_data/configmap.yaml', {'$NAME':'liuweiconfigmap'});
   * @returns  string 类型，返回YAML 文件内容，该内容包含的所有键值被对应的值替换
   *
   */
  static readyamlfile(fileName, parameter: { [key: string]: string } = {}) {
    let data = readFileSync(ServerConf.TESTDATAPATH + fileName, 'utf8');
    for (const key of Object.keys(parameter)) {
      while (data.indexOf(key) >= 0) {
        data = data.replace(key, parameter[key]);
      }
      data = data.replace(key, parameter[key]);
    }
    return data;
  }

  /**
   * 将 {value} 值写入{fileName} 文件，返回文件的完整名
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {value} string 类型, 要写入 {fileName 的文件内容
   *
   * @returns  string 类型，返回文件的完整路径
   * @example writeyamlfile('temp.yaml', 'yaml 格式的文件字符串内容')
   */
  static writeyamlfile(fileName, value) {
    writeFileSync(ServerConf.TESTDATAPATH + 'temp/' + fileName, value);
    return ServerConf.TESTDATAPATH + 'temp/' + fileName;
  }

  /**
   * 命令行执行任意一个命令
   *
   * @parameter {cmd} string 类型, 命令， 例如： kubectl get service -n liuweinamespace
   *
   * @returns  string 类型，命令执行后在屏幕上输出的内容
   * @example execCommand('kubectl get configmap');
   */
  static execCommand(cmd) {
    try {
      return String(execSync(cmd));
    } catch (ex) {
      return ex.toString();
    }
  }

  /**
   * 程序等待 milliSeconds
   *
   * @parameter {milliSeconds} int 类型, 命令.
   *
   * @returns  string 类型，命令执行后在屏幕上输出的内容
   * @example sleep(1000);
   */
  static sleep(milliSeconds) {
    const startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliSeconds) {}
  }

  /**
   * 解析yaml
   * @param yamlValue, string 类型，从yaml读出的值
   * @return {any}
   */
  static parseYaml(yamlValue) {
    const yaml = require('js-yaml');
    try {
      return yaml.safeLoad(yamlValue);
    } catch (error) {
      return error;
    }
  }

  /**
   * 将yaml 对象保存到文件中
   * @param filename 保存的文件名
   * @param yaml yaml 对象
   */
  static dumpYaml(filename: string, yaml: any) {
    const jsyaml = require('js-yaml');
    try {
      writeFileSync(filename, jsyaml.dump(yaml), 'utf8');
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * 生成测试数据
   */
  static random_generate_testData(prefix: string = '') {
    // CommonMethod.clearTestData();
    CommonMethod.sleep(1);
    return 'diablo-auto-' + prefix + String(new Date().getMilliseconds());
  }

  /**
   * 清理由于异常中断引起的脏数据
   */
  static clearTestData() {
    if (CommonMethod.isClearTestData) {
      const namespacelist = CommonMethod.parseYaml(
        CommonMethod.execCommand('kubectl get namespaces -o yaml'),
      );
      for (const iterator of namespacelist.items) {
        const name = iterator.metadata.name;
        if (name.includes('link-auto-')) {
          const patt1 = new RegExp('^[0-9]*$');
          if (patt1.test(name.replace('link-auto-', ''))) {
            console.log('==========> clear test data');
            console.log(
              CommonMethod.execCommand(`kubectl delete namespace ${name}`),
            );
          }
        }
      }
    }
    CommonMethod.isClearTestData = false;
  }

  /**
   * base64 解码
   * @param base64Str base64 字符串
   */
  static decode(base64Str: string): string {
    return atob(base64Str);
  }
}
