const { execSync, exec } = require('child_process');
const { browser, $, element, by } = require('protractor');
let fs = require('fs');
let join = require('path').join;

class TestData {
  get testdataPath() {
    return process.cwd() + '/e2e/test_data/temp';
  }

  constructor() {}

  parseYaml(yamlValue) {
    const yaml = require('js-yaml');
    try {
      return yaml.safeLoad(yamlValue);
    } catch (error) {
      return error;
    }
  }

  /**
   * 根据类型资源清理资源
   * @param {string} 资源类型名称
   */
  clearTestData(resourceType, timeout) {
    // 获取所有资源
    const namespacelist = this.parseYaml(
      execSync(`kubectl get ${resourceType} -o yaml`),
    );

    for (const iterator of namespacelist.items) {
      const name = iterator.metadata.name;
      const creationTimestamp = iterator.metadata.creationTimestamp.getTime();
      let timeDiff = parseInt(new Date().getTime() - creationTimestamp);
      // 获得资源存在了多长时间
      timeDiff = parseInt(timeDiff / (1000 * 60));

      if (name.includes('diablo-auto-')) {
        try {
          if (timeDiff > timeout) {
            console.log('==========> clear test data');
            // 如果超过了3小时， 删除
            console.log(
              `脏数据 ${resourceType}【${
                iterator.metadata.name
              }】 存在超过了3个小时，自动删除了`,
            );
            // execSync(`kubectl delete namespace ${name}`);
            exec(`kubectl delete ${resourceType} ${name}`, (err, sto) => {});
          }
        } catch (error) {}
      }
    }
  }

  clear() {
    this.clearTestData('project', 180);
    this.clearTestData('namespaces', 180);
    this.clearTestData('jenkins', 180);
    this.clearTestData('CodeRepoService', 180);
    // 资源不够，不支持并行跑流水线
    this.clearTestData('MicroservicesEnvironment', 1);
  }

  /**
   * 通过测试数据文件清理测试数据
   */
  clearFromTestDataFile() {
    var filenames = this.findSync(this.testdataPath);
    filenames.forEach(testdata => {
      if (!testdata.includes('.gitignore')) {
        try {
          // execSync(`kubectl delete -f ${testdata}`).then((error,std) =>{});
          exec(`kubectl delete -f ${testdata}`, (err, sto) => {});
        } catch (error) {}
      }
    });
  }

  /**
   * 获得startPath 路径下所有的文件名
   * @param {string} startPath
   */
  findSync(startPath) {
    let result = [];
    function finder(path) {
      let files = fs.readdirSync(path);
      files.forEach((val, index) => {
        let fPath = join(path, val);
        let stats = fs.statSync(fPath);
        if (stats.isDirectory()) finder(fPath);
        if (stats.isFile()) result.push(fPath);
      });
    }
    finder(startPath);
    return result;
  }

  /**
   * 判断OIDC 是否开启
   */
  isOpenOIDC() {
    try {
      const authConfigmap = this.parseYaml(
        execSync('kubectl get configmap auth-config -n alauda-system -o yaml'),
      );
      if (String(authConfigmap.data.enabled).toLocaleUpperCase() === 'TRUE') {
        return true;
      }
    } catch (error) {
      return false;
    }
  }
}

module.exports = {
  // 通用
  TestData: new TestData(),
};
