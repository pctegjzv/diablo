/**
 * alaudaSwitch 列表控件
 * Created by liuwei on 2018/8/7.
 */
import { $, browser } from 'protractor';

export class AuiSwitch {
  private _selector;
  private _checkedSelector;

  constructor(
    selector: string = 'aui-switch',
    checkedSelector = 'aui-switch--checked',
  ) {
    this._selector = selector;
    this._checkedSelector = checkedSelector;
  }

  private get _button() {
    return $(this._selector);
  }

  /**
   * 打开开关
   */
  open() {
    this._button
      .$(this._checkedSelector)
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          this._button.click();
          browser.sleep(100);
        }
      });
  }

  /**
   * 关闭开关
   */
  close() {
    this._button
      .$(this._checkedSelector)
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          this._button.click();
          browser.sleep(100);
        }
      });
  }
}
