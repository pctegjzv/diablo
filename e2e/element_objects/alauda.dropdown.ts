/**
 * 下拉框列表控件
 * Created by liuwei on 2018/3/14.
 */
import { browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaDropdown {
  private _iconselector;
  private _itemlistselector;

  constructor(iconselector, itemlistselector) {
    this._iconselector = iconselector;
    this._itemlistselector = itemlistselector;
  }

  /**
   * 单击下拉框， 选择值
   *
   * @parameter {selectvalue} 从下拉框中要选择的值
   */
  select(selectvalue) {
    CommonPage.waitElementPresent(element(this._iconselector));
    const iconButton = element(this._iconselector);
    iconButton.click();
    browser.sleep(100);
    const itemlist = element.all(this._itemlistselector);
    const options = itemlist.filter(elem => {
      return elem.getText().then(text => {
        return text.trim() === selectvalue;
      });
    });

    options.count().then(count => {
      if (count === 0) {
        CommonPage.waitElementPresent(options.first());
      } else {
        options.first().click();
      }
    });

    itemlist.count().then(count => {
      if (count > 0) {
        iconButton.click();
      }
    });
    return browser.sleep(100);
  }
  /**
   * alauda_ui单击下拉框， 选择值
   *
   * @parameter {selectvalue} 从下拉框中要选择的值
   */
  select_item(selectvalue) {
    CommonPage.waitElementPresent(element(this._iconselector));
    const select = element(this._iconselector);
    browser.sleep(1000);
    select.click();
    // 等待要选择的元素出现
    CommonPage.waitElementPresent(
      element
        .all(this._itemlistselector)
        .filter(elem => {
          return elem.getText().then(function(text) {
            return text.indexOf(selectvalue) !== -1;
          });
        })
        .first(),
    );
    const options = element.all(this._itemlistselector).filter(elem => {
      return elem.getText().then(function(text) {
        return text.indexOf(selectvalue) !== -1;
      });
    });
    if (options !== null) {
      options.click();
    }

    // 如果下拉框任然显示，单击下icon, 关闭下拉框
    element
      .all(this._itemlistselector)
      .count()
      .then(count => {
        if (count > 0) {
          select.click();
        }
      });

    return browser.sleep(200);
  }
}
