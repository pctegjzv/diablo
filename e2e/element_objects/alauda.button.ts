/**
 * 按钮控件
 * Created by liuwei on 2018/2/22.
 */

import { ElementFinder, browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaButton {
  public button: ElementFinder;
  private enabled;

  constructor(selector, enabled = null) {
    this.button = element(selector);
    this.enabled = enabled;
  }

  /**
   * 判断按钮是否存在
   */
  checkButtonIsPresent() {
    CommonPage.waitElementPresent(this.button);
    if (this.enabled !== null) {
      CommonPage.waitElementNotPresent(element(this.enabled));
    }
    return this.button.isPresent();
  }

  checkButtonIsNotPresent() {
    CommonPage.waitElementNotPresent(this.button);
    return this.button.isPresent();
  }

  /**
   * 单击按钮
   */
  click() {
    CommonPage.waitElementPresent(this.button);
    this.button.click();
    return browser.sleep(500);
  }

  getButtonText() {
    return this.button.getText();
  }
}
