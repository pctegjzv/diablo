/**
 * 编辑YAML 的控件
 * Created by liuwei on 2018/2/22.
 */
import { browser, by, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaYamlEditor {
  private _codeEditorSelector;
  private _toolbarselector;

  /**
   * 编辑器
   *
   * @parameter {codeEditorSelector} code 编辑器的selector
   * @parameter {toolbarselector} code 编辑器上面的 toolbar
   *
   */
  constructor(
    codeEditorSelector = by.xpath('//aui-code-editor'),
    toolbarselector = by.xpath(
      '//following::div[contains(@class,"aui-code-editor-toolbar__control-button")]',
    ),
  ) {
    this._codeEditorSelector = codeEditorSelector;
    this._toolbarselector = toolbarselector;
  }

  checkEditorIsPresent() {
    CommonPage.waitElementPresent(element(this._codeEditorSelector));
    return element(this._codeEditorSelector).isPresent();
  }
  /**
   * 编辑器控件，根据此元素可以找到下面的所有元素
   *
   * @return element 对象
   */
  get codeEditor() {
    return element(this._codeEditorSelector);
  }

  /**
   * 编辑器上面的toolbar,
   *
   * @return element list 对象, 获得toolbar 上的所有按钮
   */
  get toolbar() {
    return element.all(this._toolbarselector);
  }

  clickToolbarByName(name) {
    return this.toolbar
      .filter(elem => {
        return elem.getText().then(function(text) {
          return text === name;
        });
      })
      .first()
      .click();
  }

  /**
   * 获取编辑器里面的yaml 值
   * 注意: 这里的javascript 只使用于发行版的项目，如果移到其它项目需要重写这个javascript
   *
   * @return Promise 对象
   * @example   getYamlValue().then((yaml) => {
   *                console.log(yaml)
   *            })
   */
  getYamlValue() {
    const queryscript =
      'return monaco.editor.getModels().find(model => model.id === document.querySelector("aui-code-editor [model-id]").attributes["model-id"].value).getValue()';
    return browser.executeScript(queryscript);
  }

  /**
   * 编辑器里面的输入 yaml 值
   * 注意: 这里的javascript 只使用于发行版的项目，如果移到其它项目需要重写这个javascript
   *
   * @parameter {stringYaml} yaml 的值
   * @example let yamlstring = CommonMethod.readyamlfile('configmap.yaml', { '$NAME' : 'liuwei', '$NAMESPACE' : 'dddddd'});
   *          setYamlValue(yamlstring);
   *
   */
  setYamlValue(stringYaml) {
    const setscript =
      'return monaco.editor.getModels().find(model => model.id === document.querySelector("aui-code-editor [model-id]").attributes["model-id"].value).setValue(`' +
      stringYaml +
      '`)';
    browser.executeScript(setscript);
    return browser.sleep(500);
  }
}
