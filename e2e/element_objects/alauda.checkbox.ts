/**
 * CheckBox 按钮控件
 * Created by liuwei on 2018/5/6.
 */

import { $, by, element } from 'protractor';

export class AlaudaCheckBox {
  private _innerText;
  private _checkBoxSelector;

  /**
   * 构造函数
   * @param innerText checkbox 右侧文字
   * @param checkBoxSelector checkbox 选择器
   */
  constructor(innerText: string, checkBoxSelector: string = 'aui-checkbox') {
    this._innerText = innerText;
    this._checkBoxSelector = checkBoxSelector;
  }

  /**
   * 是否选中
   */
  isChecked() {
    return element(
      by.cssContainingText(
        `${this._checkBoxSelector} div[class*=isChecked]`,
        this._innerText,
      ),
    ).isPresent();
  }

  /**
   * 选中复选框
   */
  check() {
    this.isChecked().then(ispresent => {
      if (!ispresent) {
        element($(`${this._checkBoxSelector}`)).click();
      }
    });
  }

  /**
   * 取消选中复选框
   */
  uncheck() {
    this.isChecked().then(ispresent => {
      if (ispresent) {
        element($(`${this._checkBoxSelector}`)).click();
      }
    });
  }
}
