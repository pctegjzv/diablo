/**
 * AloSearch控件
 * Created by liuwei on 2018/8/10.
 */
import { ElementFinder, browser, by, promise } from 'protractor';

// import { CommonPage } from '../utility/common.page';

import { AuiSearch } from './alauda.auiSearch';
import { AlaudaDropdown } from './alauda.dropdown';

export class AloSearch {
  private _AloSearch;
  private _buttonSelector;
  private _itemSelector;
  private _auiSearchSelector;

  constructor(
    aloSearch: ElementFinder,
    buttonSelector: string = '.alo-search button',
    itemSelector: string = 'aui-menu-item button',
    auiSearchSelector: string = 'aui-search',
  ) {
    this._AloSearch = aloSearch;
    this._buttonSelector = buttonSelector;
    this._itemSelector = itemSelector;
    this._auiSearchSelector = auiSearchSelector;
  }

  /**
   * 检索框左侧的dropdwon 控件
   */
  get dropdown() {
    return new AlaudaDropdown(
      by.css(this._buttonSelector),
      by.css(this._itemSelector),
    );
  }

  /**
   * 检索框控件
   */
  get auiSearch() {
    return new AuiSearch(this._AloSearch.$(this._auiSearchSelector));
  }

  /**
   * 检索
   * @param selectItem 要检索的类型
   * @param searchItem 要检索的值
   */
  search(
    searchItem: string,
    selectItem: string = '名称',
  ): promise.Promise<void> {
    this.dropdown.select(selectItem);
    this.auiSearch.search(searchItem);
    return browser.sleep(100);
  }
}
