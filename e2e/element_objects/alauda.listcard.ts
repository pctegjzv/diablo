/**
 * 按钮控件
 * Created by liuwei on 2018/2/22.
 */

import { $, $$, ElementFinder, browser, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

// import { CommonPage } from '../utility/common.page';

class AppStatus {
  private _listCard;
  private _successSelector;
  private _errorSelector;
  private _tooltipSelector;
  private _resourceCountSelector;
  constructor(
    card: ElementFinder,
    successSelector: string = 'div[class*=status-gauge-container__item--success]',
    errorSelector: string = 'div[class*=status-gauge-container__item--error]',
    tooltipSelector: string = 'aui-tooltip div[class=status-gauge-tooltip] div[class=status-gauge-tooltip__label]',
    resourceCountSelector: string = 'div[class=app-info] span[class*=aviliable-data]',
  ) {
    this._listCard = card;
    this._successSelector = successSelector;
    this._errorSelector = errorSelector;
    this._resourceCountSelector = resourceCountSelector;
    this._tooltipSelector = tooltipSelector;
  }

  get success() {
    return this._listCard.$(this._successSelector).getCssValue('flex-grow');
  }

  get error() {
    return this._listCard.$(this._errorSelector).getCssValue('flex-grow');
  }
  _waitElementPresent(elem, timeout = 5000) {
    return browser.driver
      .wait(() => {
        return elem.isPresent().then(isPresent => isPresent);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error [' + err + ']');
          return false;
        },
      );
  }

  get successTooltip() {
    this._listCard.$(this._successSelector).click();
    const tooltipElem = $(`${this._tooltipSelector}`);
    this._waitElementPresent(tooltipElem);
    return tooltipElem.getText();
  }

  get errorTooltip() {
    this._listCard.$(this._errorSelector).click();
    const tooltipElem = $(`${this._tooltipSelector}`);
    this._waitElementPresent(tooltipElem);
    return tooltipElem.getText();
  }

  get resourceNum() {
    return this._listCard.$(this._resourceCountSelector);
  }
}

export class AlaudaListCard {
  private _parentSelector;
  private _listCardSelector;
  private _appnSelector;

  constructor(
    parentSelector: string = 'div[class*=list-card-container]',
    listCardSelector: string = 'alo-application-list-card',
    appnSelector: string = 'div[class=app-name]',
  ) {
    this._parentSelector = parentSelector;
    this._listCardSelector = listCardSelector;
    this._appnSelector = appnSelector;
  }

  _getListCardByName(name: string): ElementFinder {
    return $$(`${this._parentSelector} ${this._listCardSelector}`)
      .filter(elem => {
        return elem
          .$(this._appnSelector)
          .getText()
          .then(text => {
            return text === name;
          });
      })
      .first();
  }

  /**
   * 单击名称进入详情页
   * @param appName app 的名称
   */
  clickByAppName(appName: string): promise.Promise<void> {
    const appCard = this._getListCardByName(appName);
    return appCard.$(this._appnSelector).click();
  }

  /**
   * 展开list card
   * @param appName card 的名称
   * @param _chiledSelector card 下面子节点的selector
   */
  expandByAppName(
    appName: string,
    _buttonSelector: string = 'div[class=row-image] img',
    _chiledSelector: string = 'div[class*=resource-list-container]',
  ): promise.Promise<void> {
    const appCard = this._getListCardByName(appName);
    return appCard
      .$(_chiledSelector)
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          CommonPage.waitElementPresent(appCard.$(_buttonSelector));
          return appCard.$(_buttonSelector).click();
        }
      });
  }

  /**
   * 折叠list card
   * @param appName card 的名称
   * @param _chiledSelector card 下面子节点的selector
   */
  collapseByAppName(
    appName: string,
    _buttonSelector: string = 'div[class=row-image] img',
    _chiledSelector: string = 'div[class*=resource-list-container]',
  ): promise.Promise<void> {
    const appCard = this._getListCardByName(appName);
    return appCard
      .$(_chiledSelector)
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          return appCard.$(_buttonSelector).click();
        }
      });
  }

  getAppStatusByName(name: string): AppStatus {
    return new AppStatus(this._getListCardByName(name));
  }
}
