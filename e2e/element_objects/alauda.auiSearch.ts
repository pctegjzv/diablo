/**
 * auiSearch控件
 * Created by liuwei on 2018/8/10.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AuiSearch {
  private _auiSearch;
  private _inputboxSelector;
  private _clearIconSelector;
  private _searchButtonSelector;

  constructor(
    auiSearch: ElementFinder,
    inputboxSelector: string = '.aui-search input',
    clearIconSelector: string = '.aui-search__clear aui-icon',
    searchButtonSelector: string = '.aui-search__button aui-icon',
  ) {
    this._auiSearch = auiSearch;
    this._inputboxSelector = inputboxSelector;
    this._clearIconSelector = clearIconSelector;
    this._searchButtonSelector = searchButtonSelector;
  }

  /**
   * 文本框
   */
  get inputbox(): ElementFinder {
    return this._auiSearch.$(this._inputboxSelector);
  }

  /**
   * 清除的小图标
   */
  get iconClear(): ElementFinder {
    return this._auiSearch.$(this._clearIconSelector);
  }

  /**
   * 检索的小图标
   */
  get iconSearch(): ElementFinder {
    return this._auiSearch.$(this._searchButtonSelector);
  }

  /**
   * 在文本框中输入值
   * @param text 文本框中要输入的值
   */
  _input(text: string): promise.Promise<void> {
    this.inputbox.clear();
    this.inputbox.sendKeys(text);
    return browser.sleep(100);
  }

  /**
   * 获取文本框的值
   */
  get value(): promise.Promise<string> {
    return this.inputbox.getAttribute('value');
  }

  /**
   * 检索
   * @param value 要检索的值
   */
  search(value: string): promise.Promise<void> {
    CommonPage.waitElementPresent(this.inputbox).then(isPresent => {
      if (!isPresent) {
        console.log('aui-search 控件没有正常加载');
      }
    });
    this._input(value);
    this.iconSearch.click();
    return browser.sleep(100);
  }

  /**
   * 清空检索框
   */
  clear(): promise.Promise<void> {
    CommonPage.waitElementPresent(this.inputbox).then(isPresent => {
      if (!isPresent) {
        console.log('aui-search 控件没有正常加载');
      }
    });
    this.iconClear.click();
    return browser.sleep(100);
  }
}
