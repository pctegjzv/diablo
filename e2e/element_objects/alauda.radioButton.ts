/**
 * Ridio 按钮控件
 * Created by liuwei on 2018/7/4.
 */

import { $$, browser } from 'protractor';
export class AlaudaRadioButton {
  private _parentSelector;
  private _contentSelector;

  constructor(
    parentSelector: string = 'aui-radio-button',
    contentSelector: string = '.aui-radio-button__content',
  ) {
    this._parentSelector = parentSelector;
    this._contentSelector = contentSelector;
  }

  /**
   * 单击按钮
   */
  clickByName(name: string) {
    const contentList = $$(`${this._parentSelector} ${this._contentSelector}`)
      .filter(elem => {
        return elem.getText().then(text => {
          return text === name;
        });
      })
      .first();
    contentList.click();
    return browser.sleep(500);
  }
}
