/**
 * 封装包含key 是 aui-select, value 是input 的alo-key-value-form 控件
 * Created by liuwei on 2018/8/7.
 */

import { $, $$, ElementFinder, browser, by, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AuiSelect } from './alauda.auiSelect';
import { AlaudaButton } from './alauda.button';

export class KeyValueForm {
  private _addButtonSelector;
  private _keyValueRowSelector;
  private _keyParentSelector;

  /**
   * ddd
   * @param addButtonSelector 添加按钮的selector
   * @param keyValueRowSelector 行的selector
   * @param keyParentSelector 要操作的父亲节点的selector
   * @param valueSelector  ddd
   */
  constructor(
    addButtonSelector: string = '.add-button button',
    keyValueRowSelector: string = '.key-value-form .key-value-form-row',
    keyParentSelector: string = 'alo-key-value-inputs',
  ) {
    this._addButtonSelector = addButtonSelector;
    this._keyValueRowSelector = keyValueRowSelector;
    this._keyParentSelector = keyParentSelector;
  }

  private _getRow(startIndex: number = 1): promise.Promise<ElementFinder> {
    const rows = $$(this._keyValueRowSelector);
    return rows.count().then(count => {
      const index = count * 2 - 1 + startIndex;
      // 找到要操作的行
      const parent = $(`${this._keyValueRowSelector}:nth-child(${index})`);
      CommonPage.waitElementPresent(parent);
      return parent;
    });
  }

  private _fillValue(
    elem: ElementFinder,
    value: string,
    tagName: string,
    auiSelectItemSelector: string,
  ) {
    switch (tagName) {
      case 'input':
        elem.clear();
        elem.sendKeys(value);
        return browser.sleep(100);
      case 'aui-select':
        // 找到aui-select 控件
        const auiSelectElem = new AuiSelect(
          elem.$('aui-select input'),
          elem.$('aui-select span'),
          auiSelectItemSelector,
        );
        // 选择一个值
        auiSelectElem.select(value);
        return browser.sleep(100);
    }
  }

  /**
   * 添加按钮
   */
  get buttonAdd() {
    return new AlaudaButton(by.css(this._addButtonSelector));
  }

  /**
   * 新增一个值
   * @param key
   * @param value
   */
  newValue(
    arrayValue,
    startIndex = 1,
    auiSelectItemSelector: string = 'aui-tooltip aui-option div',
  ) {
    this._getRow(startIndex).then(parent => {
      // 遍历所有子节点
      parent.$$(`${this._keyParentSelector}>*`).each((elem, index) => {
        elem.getTagName().then(tagName => {
          this._fillValue(
            elem,
            arrayValue[index],
            tagName,
            auiSelectItemSelector,
          );
        });
      });
    });
  }

  newValues(arrayValue) {
    for (let i = 0; i < arrayValue.length; i++) {
      if (i % 2 === 0) {
        this.newValue([arrayValue[i], arrayValue[i + 1]]);
        this.buttonAdd.click();
      }
    }
  }
}
