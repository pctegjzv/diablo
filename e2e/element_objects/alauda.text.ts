/**
 * 文本控件
 * Created by zhangjiao on 2018/3/2.
 */

import { element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaText {
  private text;

  constructor(selector) {
    this.text = selector;
  }

  /**
   * 判断text是否显示
   */
  checkTextIsPresent() {
    CommonPage.waitElementPresent(element(this.text));
    return element(this.text).isPresent();
  }

  checkTextIsNotPresent() {
    CommonPage.waitElementNotPresent(element(this.text));
    return element(this.text).isPresent();
  }

  getTextElem() {
    return element(this.text);
  }
  /**
   * 获得文本
   */
  getText() {
    return element(this.text).getText();
  }

  getTextAll(index) {
    return element
      .all(this.text)
      .get(index)
      .getText();
  }

  getValue() {
    return element(this.text).getAttribute('value');
  }

  getTextAllMap() {
    return element.all(this.text).map(item => item.getText());
  }

  checkTextAllIsPresent(index) {
    CommonPage.waitElementPresent(element.all(this.text).get(index));
    return element
      .all(this.text)
      .get(index)
      .isPresent();
  }
  click() {
    CommonPage.waitElementPresent(element(this.text));
    element(this.text).click();
  }
}
