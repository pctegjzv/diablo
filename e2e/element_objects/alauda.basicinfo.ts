/**
 * 基本信息页面
 * Created by liuwei on 2018/3/15.
 */

import { by, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaBasicInfo {
  private _title_selector;
  private _content_selector;
  private _itemKey_css_selector;
  private _itemValue_css_selector;
  private _itemHint_css_selector;

  /**
   * 构造函数
   * @param content_selector 获得每个键 和 值所在的父节点的 selector
   * @param title_selector 获得基本信息的粗体title 的selector
   * @param itemkey_css_selector string，类型，获得左侧key文字的 css
   * @param itemValue_css_selector string，类型，获得右侧value文字的 css
   * @param itemHint_css_selector string类型，获得创建页面的错误提示
   */
  constructor(
    content_selector,
    title_selector = by.css('.rc-cluster-info__title'),
    itemkey_css_selector = '.rc-cluster-info__label',
    itemValue_css_selector = '.rc-cluster-info__value',
    itemHint_css_selector = '.aui-form-field__error-hint',
  ) {
    this._title_selector = title_selector;
    this._content_selector = content_selector;
    this._itemKey_css_selector = itemkey_css_selector;
    this._itemValue_css_selector = itemValue_css_selector;
    this._itemHint_css_selector = itemHint_css_selector;
  }

  checkInputboxIsPresent(labeltext) {
    const content = element
      .all(this._content_selector)
      .filter(elem => {
        return elem
          .$(this._itemKey_css_selector)
          .getText()
          .then(text => {
            return (
              text
                .replace(':', '')
                .replace('：', '')
                .replace('*', '')
                .trim() === labeltext
            );
          });
      })
      .first();
    CommonPage.waitElementPresent(content.$(this._itemValue_css_selector));
    return content.$(this._itemValue_css_selector).isPresent();
  }

  /**
   * 在基本信息页面上， 根据key,获得值元素， 例如获得名称后边的元素（k8s_staging_uie2e_cluster），
   * 基本信息页面的例子：
   名称 : k8s_staging_uie2e_cluster          云服务 ： 私有的
   Docker 版本 : 1.12.6                     Docker 安装目录 ： /var/lib/docker
   创建时间 ： 2018-03-14 17:36:21
   * @param labeltext item_key 的显示文本，例如， 名称，云服务，Docker 版本，Docker 安装目录
   * @example getElementByText(名称).getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(labeltext) {
    const content = element
      .all(this._content_selector)
      .filter(elem => {
        return elem
          .$(this._itemKey_css_selector)
          .getText()
          .then(text => {
            return (
              text
                .replace(':', '')
                .replace('：', '')
                .replace('*', '')
                .trim() === labeltext
            );
          });
      })
      .first();
    CommonPage.waitElementPresent(content);
    return content.$(this._itemValue_css_selector);
  }

  async inputByText(labeltext, inputValue) {
    const content = element
      .all(this._content_selector)
      .filter(elem => {
        return elem
          .$(this._itemKey_css_selector)
          .getText()
          .then(text => {
            return (
              text
                .replace(':', '')
                .replace('：', '')
                .replace('*', '')
                .trim() === labeltext
            );
          });
      })
      .first();
    let passed;
    for (let i = 0; i < 4; i++) {
      await content
        .$(this._itemValue_css_selector)
        .getAttribute('value')
        .then(text => {
          if (text !== inputValue) {
            content.$(this._itemValue_css_selector).clear();
            content.$(this._itemValue_css_selector).sendKeys(inputValue);
            passed = false;
            return;
          }
          passed = true;
          return;
        });
      if (passed) {
        break;
      }
    }
    return;
  }

  /**
   * 获得基本信息的所有keys值，例如 ：[ '名称', '云服务', 'Docker 版本', 'Docker 安装目录', '创建时间' ]
   * @example getAllKeyText().then((text) => { console.log(text); })
   */
  getAllKeyText() {
    return element
      .all(this._content_selector)
      .$$(this._itemKey_css_selector)
      .getText();
  }

  /**
   * 获得基本信息页的title 元素
   */
  getTitleText() {
    CommonPage.waitElementPresent(element(this._title_selector));
    return element(this._title_selector).getText();
  }

  /**
   * 在创建页面上， 根据key,获得对应的错误提示信息
   * 基本信息页面的例子：
   应用名称： inputbox
            必填项
   * 获得必填项
   */
  getHintByText(labeltext) {
    const content = element
      .all(this._content_selector)
      .filter(elem => {
        return elem
          .$(this._itemKey_css_selector)
          .getText()
          .then(text => {
            return (
              text
                .replace(':', '')
                .replace('：', '')
                .replace('*', '')
                .trim() === labeltext
            );
          });
      })
      .first();
    return content.$(this._itemHint_css_selector);
  }
}
