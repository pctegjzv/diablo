/**
 * Created by zhangjiao on 2018/3/2.
 */

import { browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaList {
  private nameLink;
  private operateAllButton;
  private operateButton;
  private okButton;

  constructor(
    namelink,
    operateAllButton = null,
    operateButton = null,
    okButton = null,
  ) {
    this.nameLink = element(namelink);
    this.operateAllButton = element(operateAllButton);
    this.operateButton = element(operateButton);
    this.okButton = element(okButton);
  }

  /**
   * 判断NameLink是否在列表中显示
   */
  checkNameInListPage() {
    CommonPage.waitElementPresent(this.nameLink);
    return this.nameLink.isPresent();
  }

  /**
   * 判断NameLink是否不在列表中显示
   */
  checkNameNotInListPage() {
    CommonPage.waitElementNotPresent(this.nameLink);
    return this.nameLink.isPresent();
  }

  clickNameInlistPage() {
    CommonPage.waitElementPresent(this.nameLink, 60000);
    this.nameLink.click();
    return browser.sleep(200);
  }

  clickOperateAllButton() {
    this.operateAllButton.click();
    return browser.sleep(200);
  }

  clickOperateButton() {
    this.operateButton.click();
    return browser.sleep(200);
  }

  clickOkButton() {
    this.okButton.click();
    return browser.sleep(200);
  }
}
