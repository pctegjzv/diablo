/**
 * toast 控件
 * Created by liuwei on 2018/4/10.
 */

import { $, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaToast {
  private _toast_selector;

  constructor(
    selector: string = 'aui-message-container .aui-message__content',
  ) {
    this._toast_selector = selector;
  }

  get message() {
    return $(this._toast_selector);
  }

  /**
   * 获取toast控件的message文本值, 并等待toast 控件消失
   */
  getMessage(): promise.Promise<string> {
    CommonPage.waitElementPresent(this.message, 60000);
    return this.message.isPresent().then(isPresent => {
      if (isPresent) {
        const text = this.message.getText();
        CommonPage.waitElementNotPresent(this.message);
        return text;
      }
    });
  }

  /**
   * 等待toast 消失
   */
  waitDisappear() {
    CommonPage.waitElementNotPresent(this.message);
  }
}
