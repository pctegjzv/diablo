/**
 * tabs控件
 * Created by liuwei on 2018/7/30.
 */

import { $, $$, browser } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaTabs {
  private _tabItemSelector;

  constructor(tabItemSelector: string = '.tabs li a') {
    this._tabItemSelector = tabItemSelector;
  }

  private _getTabItem(tabItemText: string) {
    const tabItem = $$(this._tabItemSelector)
      .filter(elem => {
        return elem.getText().then(text => {
          return text === tabItemText;
        });
      })
      .first();
    CommonPage.waitElementPresent(tabItem).then(isPresent => {
      if (!isPresent) {
        console.log(`查找的tabItem 【${tabItemText}】 不存在`);
      }
    });
    return tabItem;
  }

  /**
   * 单击tab
   * @param tabItemText tabItem 上显示的文本信息
   */
  click(tabItemText: string) {
    const tableItem = this._getTabItem(tabItemText);
    tableItem.click();
    browser.sleep(100);
  }

  /**
   * 获得所有tabItem 的值
   */
  getText() {
    return $$(this._tabItemSelector).getText();
  }

  /**
   * 获得选中的tabItem
   * @param activeTabSelector 选中的tabItem 选择器
   */
  getActiveTab(activeTabSelector: string = '.tabs li[class*="active"] a') {
    return $(activeTabSelector);
  }
}
