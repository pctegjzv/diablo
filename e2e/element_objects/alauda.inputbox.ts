/**
 * 文本框控件
 * Created by liuwei on 2018/2/22.
 */

import { browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaInputbox {
  private inputBox;
  private inputBox_all;

  constructor(selector) {
    this.inputBox = element(selector);
    this.inputBox_all = element.all(selector);
  }

  /**
   * 判断Inputbox是否显示
   */
  checkInputboxIsPresent() {
    CommonPage.waitElementPresent(this.inputBox);
    return this.inputBox.isPresent();
  }

  /**
   * 在文本框中输入一个值
   *
   * @parameter {inputValue} 要输入的值
   */
  async input(inputValue) {
    let passed;
    for (let i = 0; i < 4; i++) {
      await this.inputBox.getAttribute('value').then(text => {
        if (text !== inputValue) {
          this.inputBox.clear();
          this.inputBox.sendKeys(inputValue);
          passed = false;
          return;
        }
        passed = true;
        return;
      });
      if (passed) {
        break;
      }
    }
    browser.sleep(100);
    return;
  }

  /**
   * 获取文本框的值
   */
  getText() {
    return this.inputBox.getText();
  }

  /**
   * 获取文本框的值
   */
  get value() {
    return this.inputBox.getAttribute('value');
  }

  async input_index(inputValue, j) {
    let passed;
    for (let i = 0; i < 4; i++) {
      await this.inputBox_all
        .get(j)
        .getAttribute('value')
        .then(text => {
          if (text !== inputValue) {
            this.inputBox_all.get(j).clear();
            this.inputBox_all.get(j).sendKeys(inputValue);
            passed = false;
            return;
          }
          passed = true;
          return;
        });
      if (passed) {
        break;
      }
    }
    return;
  }
}
