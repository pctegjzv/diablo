const { config } = require('./protractor.conf');

exports.config = Object.assign({}, config, {
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 2,
    chromeOptions: {
      args: [
        '--headless',
        '--disable-gpu',
        '--window-size=2920,2440',
        '--ignore-certificate-errors',
        '--no-sandbox',
        "--proxy-server='direct://'",
        '--proxy-bypass-list=*',
      ],
    },
    acceptInsecureCerts: true,
  },
});
