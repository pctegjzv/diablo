/**
 * Created by liuwei on 2018/8/21.
 */

import { $ } from 'protractor';

import { AuiTableComponent } from '../../component/aui_table.component';

import { PageBase } from '../page.base';

export class ServicePage extends PageBase {
  /**
   * 服务表格的table
   */
  get serviceTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('alo-card-section'),
      '.alo-card__section-body aui-table',
      '.list-header',
    ); // 检索框的父元素);
  }
}
