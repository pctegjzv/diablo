/**
 * Created by liuwei on 2018/8/30.
 */

import { $ } from 'protractor';

import { AuiTableComponent } from '../../component/aui_table.component';
import { AuiMonacoEditor } from '../../element_objects_new/alauda.aui_monaco_editor';

import { PageBase } from '../page.base';

export class ConfigPage extends PageBase {
  /**
   * 服务表格的table
   */
  get ConfigTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('alo-card-section'),
      '.alo-card__section-body aui-table',
      '.list-header',
    ); // 检索框的父元素);
  }

  get auiMonacoEditor() {
    return new AuiMonacoEditor($('mat-dialog-content aui-monaco-editor'));
  }

  openConfigFile(key = ['asfdemo-client', 'asfdemo-client-test.yaml']) {
    this.ConfigTable.clickResourceNameByRow(key, '配置文件');
    this.waitElementPresent(
      this.auiMonacoEditor.auiMonacoEditor,
      '配置弹窗没有打开',
    );
  }
}
