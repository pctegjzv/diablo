/**
 * Created by liuwei on 2018/7/24.
 */

import { AlaudaButton } from '../../../element_objects/alauda.button';

import { $, $$, browser, by } from '../../../../node_modules/protractor';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaDropdown } from '../../../element_objects_new/alauda.dropdown';
import { PageBase } from '../../page.base';

import { CreatingPage } from './create.page';

class DialogUpdateBasicInfo extends PageBase {
  get name() {
    return $('.aui-form-item__content div');
  }

  get displayName() {
    return new AlaudaInputbox(by.css('aui-form-item .aui-form-item input'));
  }

  get describe() {
    return new AlaudaInputbox(by.css('aui-form-item .aui-form-item textarea'));
  }

  get buttonUpdate() {
    return new AlaudaButton(by.css('.form-actions button:nth-child(1)'));
  }

  updatBasicInfo(displayName: string, describe: string) {
    this.displayName.input(displayName);
    this.describe.input(describe);
    this.buttonUpdate.click();
    this.waitProgressBarNotPresent();
  }
}

export class DetailPage extends CreatingPage {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.alo-card__section-body .field',
      '.alo-card__section-body .field label',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname: string = 'span'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }

  get dropdownbutton(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('alo-menu-trigger button'),
      $$('aui-tooltip button'),
    );
  }

  get inputBoxMicEnvName() {
    return new AlaudaInputbox(by.css('alo-confirm-dialog input'));
  }

  /**
   * 更新微服务环境的基本信息
   * @param name 微服务的名称
   * @param displayName 微服务的显示名称
   * @param describe 微服务的描述
   */
  updateBasicInfo(name: string, displayName: string, describe: string) {
    this.dropdownbutton.select('更新基本信息');
    const dialog = new DialogUpdateBasicInfo();

    // browser.driver.wait(() => {
    //   return dialog.buttonUpdate.button.isPresent().then(isPresent => {
    //     if (!isPresent) {
    //       this.dropdownbutton.select('更新基本信息');
    //       return isPresent;
    //     } else {
    //       return isPresent;
    //     }
    //   });
    // }, 60000);
    console.log(`更新${name} 基本信息`);

    dialog.updatBasicInfo(displayName, describe);
    expect(this.toast.getMessage()).toBe(`微服务环境更新成功`);
  }

  /**
   * 删除微服务环境
   * @param microServiceEnvName 微服务环境的名
   */
  deleteMicroServiceEnv(microServiceEnvName: string) {
    this.dropdownbutton.select('删除微服务环境');
    this.inputBoxMicEnvName.input(microServiceEnvName);
    browser.sleep(1000);
    this.getButtonByText('删除微服务环境').click();
    this.waitProgressBarNotPresent();
  }
}
