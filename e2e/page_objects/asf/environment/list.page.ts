/**
 * Created by liuwei on 2018/7/24.
 */
import { $, $$, ElementFinder, promise } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AuiSearch } from '../../../element_objects_new/alauda.auiSearch';
import { PageBase } from '../../page.base';

class EnvironmentTable extends AuiTableComponent {
  constructor(
    root: ElementFinder = $('alo-card-section'), // 包含检索框和aui-table 控件的元素
    tableSelector = '.alo-card__section-body aui-table',
    searchBoxSelector = '.alo-search', // 检索框的父元素
  ) {
    super(root, tableSelector, searchBoxSelector);
  }

  /**
   * 表格右上角的检索框，子类不一样时需要重载
   */
  get aloSearch() {
    return new AuiSearch(
      $('.alo-card__section-header .alo-search'),
      '.aui-search input',
      '.aui-search__clear aui-icon',
      '.aui-search__icon aui-icon',
    );
  }
}

export class ListPage extends PageBase {
  /**
   * 微服务的环境的列表
   */
  get environmentTable() {
    return new EnvironmentTable($('alo-card-section'));
  }

  /**
   * 判断 Environment 是否存在列表中
   * @param name 微服务环境的名字
   */
  isExist(name): promise.Promise<boolean> {
    const applist = $$('aui-table-cell>div:nth-child(1)');
    const app = applist
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === name;
        });
      })
      .first();
    return app.isPresent();
  }
}
