/**
 * Created by liuwei on 2018/7/24.
 */

import { $, browser, by, promise } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaYamlEditor } from '../../../element_objects/alauda.ymaleditor';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

export class CreatingPage extends PageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item__label',
    );
  }

  /**
   * 获得 aui-code-editor
   */
  get codeEditor(): AlaudaYamlEditor {
    return new AlaudaYamlEditor();
  }

  /**
   * 获得安装按钮
   */
  get buttonInstall(): AlaudaButton {
    return new AlaudaButton(
      by.css('alo-microservice-component .mscomp-install-footer button'),
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname: string = 'input'): any {
    switch (text) {
      case '描述':
        return this.alaudaElement.getElementByText(text, 'textarea');
    }
    return this.alaudaElement.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname: string = 'input') {
    CommonPage.waitElementPresent(this.getElementByText(name, tagname));
    this.getElementByText(name, tagname).clear();
    this.getElementByText(name, tagname).sendKeys(value);
    browser.sleep(50);
  }

  /**
   * 填写表单
   * @param data 测试数据 { 名称: 'test', 显示名称: 'TEST'， 描述: 'describe 111' }
   */
  fillForm(data) {
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        this.enterValue(key, data[key]);
      }
    }
  }

  /**
   * 组件安装是否成功
   * @param timeout 超时时间
   */
  isInstallComponentSuccess(
    timeout: number = 120000,
  ): promise.Promise<boolean> {
    const elemSuccess = $('.mscomp-status-icon aui-icon[class*=success]');
    this.waitElementPresent(
      elemSuccess,
      '微服务组件安装的成功状态图标没有正常加载',
      timeout,
    );
    return elemSuccess.isPresent();
  }

  /**
   * 等待 eureka 应用程序正常启动
   */
  waitEurekaStart(): promise.Promise<boolean> {
    return browser.sleep(100).then(() => {
      const command = CommonKubectl.execKubectlCommand(
        `curl -i --insecure http://${process.env.ns}--eureka.${
          process.env.CLUSTER_MASTER_IP
        }`,
      );
      console.log(command);
      return String(command).includes('<b>EUREKA-EUREKA</b>');
    });
  }

  /**
   * eureka 服务发现demoservice
   */
  isRegistrySuccessByEureka(): promise.Promise<boolean> {
    return browser.sleep(100).then(() => {
      const command = CommonKubectl.execKubectlCommand(
        `curl -i --insecure http://${process.env.ns}--eureka.${
          process.env.CLUSTER_MASTER_IP
        }`,
      );
      console.log(command);
      return String(command).includes('<b>ASFDEMO-CLIENT</b>');
    });
  }

  /**
   * demoservce 部署成功后，等待 eureka 服务发现demoservice, 并注册成功
   */
  waitEurekaRegistryDemoService() {
    return browser
      .wait(
        this.isRegistrySuccessByEureka,
        300000,
        'demoservice 部署成功后，没有被erueka 服务发现成功',
      )
      .then(isSuccess => {
        if (!isSuccess) {
          throw new Error('demoservice 没有成功注册到 eureka');
        }
      });
  }

  /**
   * 等待安装中的提示消失
   * @param timeout 超时时间
   */
  waitInstall(
    name: string,
    ns: string,
    timeout: number = 300000,
  ): promise.Promise<boolean> {
    const elemProgress = $('.mscomp-installing .progress');
    return this.waitElementNotPresent(
      elemProgress,
      '安装中，已经完成[* %] 的提示没有消失',
      timeout,
    ).then(isPresent => {
      if (!isPresent) {
        console.log(
          CommonKubectl.execKubectlCommand(`kubectl get po -n ${ns}`),
        );
        // console.log(
        //   CommonKubectl.execKubectlCommand(`kubectl describe po eureka-eureka -n ${ns}`),
        // );
        throw new Error(`微服务基础组件[${name}]安装失败了`);
      } else {
        if (name === 'eureka') {
          process.env.ns = ns;
          browser
            .wait(
              this.waitEurekaStart,
              300000,
              'eureka 安装完成后，应用程序没有正常启动',
            )
            .then(isSuccess => {
              if (!isSuccess) {
                throw new Error(
                  `Erueka http://${process.env.ns}--eureka.${
                    process.env.CLUSTER_MASTER_IP
                  } 应用程序没有正常启动`,
                );
              }
            });
          return this.waitProgressBarNotPresent();
        } else {
          // 等待30秒
          browser.sleep(3000);
        }
      }
      return this.waitProgressBarNotPresent();
    });
  }
}
