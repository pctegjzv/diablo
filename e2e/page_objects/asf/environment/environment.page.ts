/**
 * Created by liuwei on 2018/7/20.
 */

import { readFileSync } from 'fs';
import { browser, promise } from 'protractor';

import { CommonMethod } from '../../../utility/common.method';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

import { CreatingPage } from './create.page';
import { DetailPage } from './detail.page';
import { ListPage } from './list.page';

export class EnvironmentPage extends PageBase {
  get detailPage() {
    return new DetailPage();
  }

  /**
   * 创建页面
   */
  get listPage() {
    return new ListPage();
  }

  /**
   * 列表页面
   */
  get creatingPage() {
    return new CreatingPage();
  }

  /**
   * 更新demoservice 的配置
   * @param microServiceEnvironment 微服务环境的名称
   * @param yamlName github 的yaml 配置
   */
  updateGithubEurekaYaml(
    microServiceEnvironment,
    yamlName = 'asfdemo-client-test.yaml',
  ) {
    CommonMethod.execCommand(`rm -rf ${process.cwd()}/asfdemo-client`);
    CommonMethod.execCommand(
      `git config remote.origin.url https://davidLiu2622:liuwei82@github.com/davidLiu2622/microServiceTest/asfdemo-client.git`,
    );
    CommonMethod.execCommand(
      `git clone https://github.com/microServiceTest/asfdemo-client.git`,
    );

    const filePath = `${process.cwd()}/asfdemo-client/`;
    const fileName = `${filePath}${yamlName}`;

    const eurekayaml = CommonMethod.parseYaml(String(readFileSync(fileName)));
    eurekayaml.eureka.client.serviceUrl.defaultZone = `http://eureka-eureka.${microServiceEnvironment}:8761/eureka/`;
    CommonMethod.dumpYaml(fileName, eurekayaml);

    CommonMethod.execCommand(`cd ${filePath} && git add .`);
    CommonMethod.execCommand(`cd ${filePath} && git commit -m 'update eureka'`);
    CommonMethod.execCommand(`cd ${filePath} && git push -f`);
    console.log(String(readFileSync(fileName)));
  }

  /**
   * 通过模版创建应用
   * @param testData 模版创建应用的测试数据
   */
  createAsfEnvironment(testData, ns): promise.Promise<void> {
    this.clickLeftNavByText('微服务环境');
    CommonPage.waitElementPresent(
      this.getButtonByText('部署微服务环境').button,
    );
    this.getButtonByText('部署微服务环境').click();
    this.waitProgressBarNotPresent();

    this.creatingPage.fillForm(testData);
    browser.sleep(100);
    this.getButtonByText('创建').click();
    this.waitProgressBarNotPresent();

    CommonPage.waitElementPresent(this.creatingPage.buttonInstall.button);
    // 安装zookeeper
    this.creatingPage.codeEditor.getYamlValue().then(yaml => {
      yaml = String(yaml).replace('memory: 2Gi', 'memory: 1Gi');
      yaml = String(yaml).replace('memory: 4Gi', 'memory: 1Gi');
      yaml = String(yaml).replace('storage: "5Gi"', 'storage: "1Gi"');

      this.creatingPage.codeEditor.setYamlValue(yaml);
      // 安装zookeeper
      this.creatingPage.buttonInstall.click();
      this.creatingPage.waitInstall('zookeeper', ns);
    });

    // 安装kafka
    this.creatingPage.codeEditor.getYamlValue().then(() => {
      this.creatingPage.buttonInstall.click();
      this.creatingPage.waitInstall('kafka', ns);
    });

    // 安装eureka
    this.creatingPage.codeEditor.getYamlValue().then(yaml => {
      yaml = String(yaml).replace(
        'ingressHost: ""',
        `ingressHost: "${process.env.CLUSTER_MASTER_IP}"`,
      );
      // eureka 程序正常启动需要 cpu: 1000m， memory: 2G
      yaml = String(yaml).replace('memory: 1G', `memory: 2G`);
      yaml = String(yaml).replace('memory: 1G', `memory: 2G`);
      yaml = String(yaml).replace('cpu: 100m', `cpu: 1000m`);
      yaml = String(yaml).replace('cpu: 100m', `cpu: 1000m`);

      console.log('\n\n********创建eureka时的yaml********\n\n');
      console.log(yaml);
      console.log('\n********************************\n\n');

      this.creatingPage.codeEditor.setYamlValue(yaml);
      this.creatingPage.buttonInstall.click();
      this.creatingPage.waitInstall('eureka', ns);
    });

    // 安装configServer
    this.creatingPage.codeEditor.getYamlValue().then(yaml => {
      console.log('\n\n********创建configServer时的yaml********\n\n');
      yaml = String(yaml).replace(
        'spring: {"cloud":{"config":{"server":{"git":{"uri":"https://github.com/sampleApp/{application}"}}}}}',
        `spring: {"cloud":{"config":{"server":{"git":{"uri":"https://github.com/microServiceTest/{application}", "username": "davidLiu2622", "password": "liuwei82"}}}}}`,
      );
      // yaml = String(yaml).replace('profiles: []', `profiles: ["test"]`);
      yaml = String(yaml).replace(
        'ingressHost: ""',
        `ingressHost: "${process.env.CLUSTER_MASTER_IP}"`,
      );
      console.log(yaml);
      this.creatingPage.codeEditor.setYamlValue(yaml);
      console.log('\n********************************\n\n');
      // 安装configServer
      this.creatingPage.buttonInstall.click();
      this.creatingPage.waitInstall('configServer', ns);
      // this.updateGithubEurekaYaml(ns);
    });
    return browser.sleep(10000);
  }
}
