/**
 * Created by zhangjiao on 2018/3/20.
 */

import { by } from 'protractor';

import { AlaudaElement } from '../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../element_objects/alauda.list';
import { AlaudaText } from '../../element_objects/alauda.text';
import { PageBase } from '../page.base';
export class HomePage extends PageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('div[class*=field]', 'div[class*=field] label');
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname: string = 'span'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }

  // -----------begin-----------Home page
  get homePage_createProject_Button() {
    return this.getButtonByText('创建项目');
  }

  get homePage_listNull_content() {
    return new AlaudaText(by.css('.list-status'));
  }

  get homePage_nameFilter_input() {
    return new AlaudaInputbox(by.css('.alo-search-input'));
  }

  homePage_projectName(projectName) {
    return new AlaudaList(
      by.xpath('//*[@href="#/workspace/' + projectName + '"]'),
    );
  }
  // -----------end-----------Home page

  get projectManagePage_projectName() {
    return new AlaudaText(by.xpath('//a[contains(@class, "switch-projects")]'));
  }

  projectManagePage_projectName1(projectName) {
    return new AlaudaText(
      by.xpath(
        '//div[contains(@class, "toolbar")]/a[contains(text(), "' +
          projectName +
          '")]',
      ),
    );
  }
}
