/**
 * Created by zhangjiao on 2018/3/1.
 */
import { $, by, element } from 'protractor';

import { TableComponent } from '../../../component/table.component';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaConfirmDialog } from '../../../element_objects/alauda.confirmdialog';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

import { SecretsCreatePage } from './create.page';

export class SecretsPage extends PageBase {
  get createPage() {
    return new SecretsCreatePage();
  }

  createSecrets(testData) {
    this.getButtonByText('创建 Secret').click();
    const createButton = $(
      `.aui-card__footer button[class*='aui-button--primary']`,
    );
    CommonPage.waitElementPresent(createButton);

    this.createPage.fillForm(testData);
    createButton.click();
    CommonPage.waitElementNotPresent(createButton);
  }

  get updateDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('mat-dialog-container[class]'),
      '.mat-dialog-header',
      'mat-dialog-actions button[aui-button=primary] span',
      `mat-dialog-actions button[aui-button='']`,
    );
  }

  get resourceTable() {
    const inputbox = new AlaudaInputbox(by.css('.list-header input'));
    return new TableComponent('alo-card', inputbox);
  }

  // -----------begin-----------secret list page

  navigationButton() {
    return this.clickLeftNavByText('凭据', false);
  }

  get listPage_createButton() {
    return this.getButtonByText('创建 Secret');
  }

  get listPage_nameFilter_input() {
    return new AlaudaInputbox(by.css('.alo-search-input'));
  }

  listPage_SecretName(project_name, secretName) {
    return new AlaudaList(
      by.xpath(
        '//*[@href="#/admin/secrets/' + project_name + '/' + secretName + '"]',
      ),
    );
  }
  // -----------end-----------secret list page

  // -----------begin-----------Create secret page
  get createPage_headerTitle_Text() {
    return new AlaudaText(by.css('.aui-card__header span'));
  }

  get createPage_secretName_inputbox() {
    return new AlaudaInputbox(by.name('name'));
  }

  get createPage_displayName_inputbox() {
    return new AlaudaInputbox(by.name('displayName'));
  }

  get createPage_project_dropdown() {
    return new AlaudaDropdown(by.name('namespace'), by.css('.aui-option'));
  }

  get createPage_description_inputbox() {
    return new AlaudaInputbox(by.name('description'));
  }

  get createPage_typeItem() {
    return element.all(by.css('.aui-radio-group .aui-radio-button'));
  }

  get createPage_createButton() {
    return this.getButtonByText('创建');
  }

  get createPage_cancelButton() {
    return this.getButtonByText('取消');
  }

  get createpage_input_content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      'input',
      '.aui-form-item__error',
    );
  }
  // -----------end-----------Create secret page

  // -----------begin-----------secret detail page
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.alo-card__container .field'),
      '.base-header',
      'label',
      'span',
    );
  }

  get detailPage_updateButton() {
    return this.getButtonByText('更新');
  }
  // -----------end-----------secret detail page

  // -----------begin-----------secret update page
  updateDialog_SecretName(secretname) {
    return new AlaudaText(
      by.xpath(
        '//div[@class="aui-form-item__content"]/span[contains(text(), "' +
          secretname +
          '")]',
      ),
    );
  }
  get updatePage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '',
      'label',
      '.aui-form-item__content .aui-form-item__control',
    );
  }
  get updatePage_updateButton() {
    return new AlaudaButton(by.xpath('//button[@aui-button="primary"]'));
  }
  // -----------end-----------secret update page
}
