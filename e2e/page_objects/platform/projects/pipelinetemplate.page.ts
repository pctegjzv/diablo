/**
 * Created by jiaozhang on 2018/8/13.
 */
import { by } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { PageBase } from '../../page.base';

import { CreatePage } from './create.page';

export class PipelineTemplate extends PageBase {
  get createPage() {
    return new CreatePage();
  }
  get detailTab_configButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button__content', '配置模板仓库'),
    );
  }
  get detailTab_syncing() {
    return new AlaudaButton(
      by.xpath('//span[contains(text(), "模板仓库同步中")]'),
    );
  }
  get detailTab_operateButton() {
    return new AlaudaButton(by.tagName('alo-menu-trigger'));
  }
  get detailTab_syncButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-menu-item', '同步模板仓库'),
    );
  }
  get detailTab_configButton_oprate() {
    return new AlaudaText(
      by.cssContainingText('.aui-menu-item', '配置模板仓库'),
    );
  }
  get configDialog_title() {
    return new AlaudaText(
      by.cssContainingText('.aui-dialog__header-title', '配置模板仓库'),
    );
  }

  configDialog_inputbox(testData) {
    this.createPage.fillForm(testData);
    this.configDialog_confirmButton.click();
    this.waitProgressBarNotPresent();
  }

  get configDialog_confirmButton() {
    return new AlaudaText(by.cssContainingText('.aui-button__content', '确定'));
  }
  get configDialog_cancelButton() {
    return new AlaudaText(by.cssContainingText('.aui-button__content', '取消'));
  }

  get detailTab_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .field'),
      '.base-header',
      'label',
      'span',
    );
  }

  get detailTab_successNum() {
    return new AlaudaText(by.xpath('//span[@class="color--success"]'));
  }
  get detailTab_failureNum() {
    return new AlaudaText(by.xpath('//span[@class="color--failure"]'));
  }
  get detailTab_skipNum() {
    return new AlaudaText(by.xpath('//span[@class="color--skip"]'));
  }
  get detailTab_resultLink() {
    return new AlaudaButton(by.css('.sync-result'));
  }

  get resultDialog_title() {
    return new AlaudaText(by.cssContainingText('.title', '同步结果'));
  }
  get resultDialog_close() {
    return new AlaudaButton(by.css('.aui-dialog__header-close'));
  }
  get detailTab_definedButton() {
    return new AlaudaButton(by.css('.header-action button'));
  }
  detailTab_definedName(name) {
    return new AlaudaButton(by.cssContainingText('.name', name));
  }
  get detailDialog_title() {
    return new AlaudaText(by.css('.aui-dialog__header-title .name'));
  }
  get detailDialog_close() {
    return new AlaudaButton(by.css('.aui-icon-close'));
  }
}
