/**
 * Created by liuwei on 2018/8/12.
 */

import { $, $$, browser, by, element, promise } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';

import { AuiSelect } from '../../../element_objects/alauda.auiSelect';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaTabs } from '../../../element_objects/alauda.tabs';
import { AlaudaDropdown } from '../../../element_objects_new/alauda.dropdown';
import { PageBase } from '../../page.base';
import { MemberPage } from '../../project/member/member.page';
class MicroServiceEnvironent extends PageBase {
  /**
   * 绑定微服务环境按钮
   */
  get buttonBindingEnv() {
    return new AlaudaButton(by.css('.unbinding-tip button'));
  }

  /**
   * 绑定微服务环境 dialog, 微服务环境 dropdownlist
   */
  get microServiceEnvSelect() {
    return new AuiSelect(
      element(by.css('aui-select aui-icon')),
      element(by.css('aui-select input')),
    );
  }

  /**
   *  绑定微服务环境 dialog, 绑定描述
   */
  get inputBoxDescription() {
    return new AlaudaInputbox(by.css('mat-dialog-content textarea'));
  }

  get dropdownbutton(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('alo-menu-trigger button'),
      $$('aui-tooltip button'),
    );
  }

  /**
   * 解绑微服务环境 dialog, 微服务环境名称输入框
   */
  get inputBoxMicEnvName() {
    return new AlaudaInputbox(by.css('mat-dialog-content input'));
  }

  /**
   * 绑定微服务环境
   * @param microServiceEnvName 微服务环境的名字
   * @param description 描述
   */
  bind(
    microServiceEnvName: string,
    description: string,
  ): promise.Promise<void> {
    this.buttonBindingEnv.click();
    this.microServiceEnvSelect.select(microServiceEnvName);
    this.inputBoxDescription.input(description);
    this.getButtonByText('绑定').click();
    expect(this.toast.getMessage()).toBe('微服务环境绑定成功');
    // 等待 3 分钟， 保证eureka 服务启动成功
    return browser.sleep(300);
  }

  /**
   * 解除绑定
   * @param microServiceEnvName 微服务环境的名
   */
  disBind(microServiceEnvName: string) {
    this.dropdownbutton.select('解绑微服务环境');
    this.waitProgressBarNotPresent();
    this.inputBoxMicEnvName.input(microServiceEnvName);
    browser.sleep(1000);
    this.getButtonByText('解绑').click();
    this.waitProgressBarNotPresent();
    expect(this.toast.getMessage()).toBe('微服务环境解绑成功');
  }

  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'alo-card-section .field',
      'alo-card-section .field label',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname: string = '.field>span'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }
}

export class ProjectDetailPage extends PageBase {
  constructor() {
    super();
  }

  /**
   * 成员列表页
   */
  get memberPage() {
    return new MemberPage();
  }

  /**
   * 详情页tabs
   */
  get tabs() {
    return new AlaudaTabs();
  }

  /**
   * 微服务环境
   */
  get microServiceEnvironent() {
    return new MicroServiceEnvironent();
  }
}

export class ProjectsDetailPage extends PageBase {
  /**
   * 详情页tabs
   */
  get tabs() {
    return new AlaudaTabs();
  }

  /**
   * 微服务环境
   */
  get microServiceEnvironent() {
    return new MicroServiceEnvironent();
  }
}
