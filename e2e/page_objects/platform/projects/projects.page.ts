/**
 * Created by zhangjiao on 2018/3/1.
 */
import { by, element } from 'protractor';

import { TableComponent } from '../../../component/table.component';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaTabItem } from '../../../element_objects/alauda.tabitem';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { PageBase } from '../../page.base';
import { ProjectDetailPage } from '../projects/projects.detail.page';

import { CreatePage } from './create.page';

export class ProjectsPage extends PageBase {
  get detailPage() {
    return new ProjectDetailPage();
  }
  get createPage() {
    return new CreatePage();
  }

  get languageButton() {
    return new AlaudaButton(by.css('.alo-switch-language'));
  }

  getLanguage() {
    return element(by.css('.alo-switch-language')).getText();
  }
  navigationButton() {
    return this.clickLeftNavByText('项目');
  }
  // -----------begin-----------projects list page
  get listPage_createButton() {
    return this.getButtonByText('创建项目');
  }

  get listPage_nameFilter_input() {
    return new AlaudaInputbox(by.css('input[placeholder="按名称搜索"]'));
  }

  get resourceTable() {
    const inputbox = new AlaudaInputbox(by.css('.list-header input'));
    return new TableComponent('aui-card', inputbox);
  }

  listPage_projectName(projectName) {
    return new AlaudaList(
      by.xpath('//*[@href="#/admin/projects/' + projectName + '"]'),
    );
  }
  listPage_projectManager(projectName) {
    return new AlaudaText(
      by.xpath(
        '//*[@href="#/admin/projects/' +
          projectName +
          '"]/parent::aui-table-cell/following-sibling::aui-table-cell[1]',
      ),
    );
  }
  // -----------end-----------projects list page

  // -----------begin-----------create project page
  get createPage_inputbox() {
    return new AlaudaBasicInfo(
      by.css('.alo-card__section-body .aui-form-field'),
      by.css('.ng-star-inserted .title'),
      '.aui-form-field__wrapper label',
      '.aui-form-field__control-elem input',
      '.aui-form-field__error-hint span',
    );
  }
  get createPage_description() {
    return new AlaudaInputbox(by.name('description'));
  }

  get createPage_createButton() {
    return this.getButtonByText('创建');
  }

  get createPage_cancelButton() {
    return this.getButtonByText('取消');
  }
  // -----------end-----------create project page

  // -----------begin-----------project detail page
  detailPage_TabItem(tabName) {
    return new AlaudaTabItem(
      by.xpath('//*[@class="tabs"]/li/a'),
      by.xpath('//*[@class="tab-label active"]/a'),
      by.xpath('//*[@class="tabs"]/li/a[contains(text(), "' + tabName + '")]'),
    );
  }

  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.alo-card__container .field'),
      '.base-header',
      'label',
      'span',
    );
  }

  get detailPage_updateButton() {
    return this.getButtonByText('更新');
  }

  get detailPage_deleteButton() {
    return this.getButtonByText('删除项目');
  }

  get detailPage_deleteButton_ok() {
    return new AlaudaButton(by.css('.aui-button--primary'));
  }

  get detailPage_Bind_button() {
    // return this.getButtonByText('绑定', by.css('button[disabled]'));
    return this.getButtonByText('绑定');
  }

  get detailPage_addSecret_button() {
    return this.getButtonByText('添加凭据');
  }

  detailPage_secretTable_secretName(projectName, secretName) {
    return new AlaudaList(
      by.xpath(
        '//*[@href="#/admin/secrets/' + projectName + '/' + secretName + '"]',
      ),
    );
  }

  get detailPage_addMember_button() {
    return this.getButtonByText('添加成员');
  }

  get detailPage_CodeRepo_Table() {
    return new TableComponent('aui-card');
  }
  // -----------end-----------project detail page

  // -----------begin-----------update project page
  get updatePage_header_text() {
    return new AlaudaText(by.css('.mat-dialog-header'));
  }
  get updatePage_projectName_Text() {
    return new AlaudaText(by.css('.aui-form-field__control-elem span'));
  }
  get updatePage_displayName_Text() {
    return new AlaudaInputbox(by.xpath('//*[@name="displayName"]'));
  }
  get updatePage_description_Text() {
    return new AlaudaInputbox(by.xpath('//*[@name="description"]'));
  }
  get updatePage__updateButton() {
    return new AlaudaButton(
      by.cssContainingText(
        '.mat-dialog-container .aui-button__content',
        '更新',
      ),
    );
  }
  // -----------end-----------update project page

  // -----------begin-----------bind service page
  get bindJenkins_inputcontent() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .aui-form-item'),
      by.css('.aui-dialog__header .aui-dialog__header-title'),
      '.aui-form-item__label-wrapper label',
      '.aui-form-item__content .aui-input',
      '.aui-form-item__error-wrapper .aui-form-item__error',
    );
  }

  bindJenkins_inputbox(testData) {
    this.createPage.fillForm(testData);
    this.bindJenkinsDialog_bindButton.click();
  }
  get bindJenkinsDialog_bindButton() {
    return new AlaudaButton(by.xpath('//button[@aui-button="primary"]'));
  }

  get bindJenkinsDialog_cancelButton() {
    return this.getButtonByText('取消');
  }
  // -----------end-----------bind service page

  // -----------begin-----------add secret dialog

  addSecretDialog_inputbox(testData) {
    this.createPage.fillForm(testData);
    this.addSecretDialog_addButton.click();
  }
  get addSecretDialog_addButton() {
    return new AlaudaButton(by.css('.aui-button--primary'));
  }

  get addSecretDialog_cancelButton() {
    return this.getButtonByText('取消');
  }

  get addSecretDialog_inputContent() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      by.css('.mat-dialog-header'),
      '.aui-form-item__label-wrapper label',
      '.aui-form-item__content .aui-input',
      '.aui-form-item__error-wrapper .aui-form-item__error',
    );
  }

  addSecretDialog_Project_Text(projectName) {
    return new AlaudaText(
      by.xpath(
        '//label[@class="aui-form-item__label" and contains(text(), "所属项目")]/../..//span[contains(text(), "' +
          projectName +
          '")]',
      ),
    );
  }

  addSecretDialog_typeItem() {
    return new AlaudaTabItem(by.css(''), by.css('+"typename"+'), by.css(''));
  }

  addSecretDialog_typeItem_dropdown() {
    return new AlaudaDropdown(
      by.xpath('//select[@name="secretType"]'),
      by.tagName('option'),
    );
  }

  get addSecretDialog_userName_inputbox() {
    return new AlaudaInputbox(by.xpath('//input[@name="username"]'));
  }

  get addSecretDialog_password_inputbox() {
    return new AlaudaInputbox(by.xpath('//input[@name="password"]'));
  }

  // -----------end-----------add secret dialog

  // -----------begin-----------add person tab
  get addMemberTab_email_inputbox() {
    return new AlaudaInputbox(by.name('email'));
  }

  get addMemberTab_role_dropdown() {
    return new AlaudaDropdown(
      by.xpath('//select[@name="role"]'),
      by.tagName('option'),
    );
  }

  get addMemberTab_add_button() {
    return this.getButtonByText('添加成员');
  }

  addMemberTab_memberName(memberEmail) {
    return new AlaudaList(
      by.xpath('//aui-table-cell[contains(text(), "' + memberEmail + '")]'),
    );
  }
  addMemberTab_memberOperate(memberEmail) {
    return new AlaudaList(
      by.xpath('//aui-table-cell[contains(text(), "' + memberEmail + '")]'),
      null,
      by.xpath(
        '//aui-table-cell[contains(text(), "' +
          memberEmail +
          '")]/..//a[contains(@class, "remove")]',
      ),
      by.xpath('//button[@aui-button="error"]'),
    );
  }
  // -----------end-----------add person tab
}
