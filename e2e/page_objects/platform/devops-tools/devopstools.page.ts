/**
 * Created by zhangjiao on 2018/3/1.
 */
import { by } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { PageBase } from '../../page.base';

export class DevopsToolsPage extends PageBase {
  // -----------begin-----------Devopstools list page
  navigationButton() {
    return this.clickLeftNavByText('DevOps 工具链');
  }

  get listPage_servicesTable() {
    return this.resourceTable;
  }

  get listPage_createButton() {
    return this.getButtonByText('集成');
  }

  get listPage_nameFilter_input() {
    return new AlaudaInputbox(by.css('.alo-search-input'));
  }

  listPage_serviceName(serviceName) {
    return new AlaudaList(by.xpath('//div[@title="' + serviceName + '"]/..'));
  }
  // -----------end-----------Devopstools list page

  // -----------begin-----------Integration dialog
  get createDialog_headerTitle_Text() {
    return new AlaudaText(by.xpath('//'));
  }
  createDialog_tools_type(tooltype) {
    return new AlaudaButton(
      by.cssContainingText(
        '.aui-dialog__content .alo-tool-type-bar .alo-tool-type-bar__label',
        tooltype,
      ),
    );
  }
  createDialog_tools_card(toolname) {
    return new AlaudaButton(
      by.xpath(
        '//div[@class="name" and contains(text(), "' + toolname + '")]/..',
      ),
    );
  }
  get createDialog_inputbox() {
    return new AlaudaBasicInfo(
      by.css('.integrate-form .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      '.aui-form-item__content .aui-input',
      '.aui-form-item__error-wrapper .aui-form-item__error',
    );
  }
  get createDialog_createButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-dialog .aui-button', '集成'),
    );
  }
  get createDialog_cancelButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-dialog .aui-button', '取消'),
    );
  }
  // -----------begin-----------Integration dialog

  // -----------begin-----------service detail page
  get detailPage_bind_table() {
    return this.resourceTable;
  }

  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card .aui-card__content .info-item'),
      '.header-title',
      '.label',
      '.value',
    );
  }
  get detailPage_errorIcon() {
    return new AlaudaButton(by.css('.info-header .header-title .aui-icon'));
  }
  get detailPage_svgButton() {
    return new AlaudaButton(by.css('.aui-button__content .aui-icon'));
  }
  detailPage_operateButton(operateName) {
    return this.getButtonByText(operateName);
  }
  get deleteConfirm_deleteButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-confirm-dialog .aui-button__content', '删除'),
    );
  }
  get deleteConfirm_cancelButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-confirm-dialog .aui-button__content', '取消'),
    );
  }
  get deleteConfirm_knowButton() {
    return new AlaudaButton(
      by.cssContainingText(
        '.aui-confirm-dialog .aui-button__content',
        '知道了',
      ),
    );
  }

  get detailPage_toProject_link() {
    return new AlaudaButton(by.xpath('//a[contains(text(), "前往项目")]'));
  }
  // -----------end-----------service detail page

  // -----------begin-----------service updateDialog
  get updateDialog_host_inputbox() {
    return new AlaudaInputbox(by.name('host'));
  }
  get updateDialog_updateButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-dialog .aui-button__content', '更新'),
    );
  }
  get updateDialog_cancelsButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-dialog .aui-button__content', '取消'),
    );
  }
  // -----------end-----------service updateDialog
}
