/**
 * Created by zhangjiao on 2018/3/1.
 */
import { $, by } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { PageBase } from '../../page.base';

export class DevopsBindPage extends PageBase {
  // -----------begin-----------bind dialog
  bindDialog_tools_type(tooltype) {
    return new AlaudaButton(
      by.cssContainingText(
        '.aui-dialog__content .alo-tool-type-bar .alo-tool-type-bar__label',
        tooltype,
      ),
    );
  }
  bindDialog_tools_card(toolname) {
    return new AlaudaButton(
      by.xpath(
        '//div[@class="title" and contains(text(), "' + toolname + '")]/..',
      ),
    );
  }
  // -----------end-----------bind dialog

  // -----------begin-----------bindlist page
  bindListPage_toolName(binName) {
    return new AlaudaList(by.xpath('//div[@title="' + binName + '"]/..'));
  }
  // bindlistPage_errorIcon(coderepoName) {
  //   return new AlaudaButton(
  //     by.xpath(
  //       '//*[@class="alo-code-service-card__title"]/span[contains(text(), "' +
  //         coderepoName +
  //         '")]//parent::div/following-sibling::aui-icon',
  //     ),
  //   );
  // }
  // -----------end-----------bindlist page

  // -----------begin-----------  bind page
  get bindPage_header_text() {
    return new AlaudaText(by.css('.ng-star-inserted .title'));
  }
  get bind_firstStep_inputbox() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      '.aui-form-item__content .aui-input',
      '.aui-form-item__error-wrapper .aui-form-item__error',
    );
  }
  get bind_firstStep_secretDropdown() {
    return new AlaudaDropdown(by.name('secret'), by.css('.aui-option'));
  }
  get bind_firstStep_bindButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button--primary', '绑定账号'),
    );
  }
  get bind_firstStep_secretType() {
    return new AlaudaRadioButton();
  }
  get bindPage_addSecret() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button__content', '添加凭据'),
    );
  }
  click_firstStep_bindButton() {
    this.bind_firstStep_bindButton.click();
    // 等待状态消失
    this.waitElementNotPresent(
      $('div .status aui-icon'),
      '【代码仓库获取中...】的提示没有消失',
      60000,
    );
  }

  get bindCode_secondStep_autoCheckbox() {
    return new AlaudaButton(
      by.cssContainingText('.aui-checkbox__content', '自动同步全部仓库'),
    );
  }
  get bindCode_secondStep_accounts() {
    return new AlaudaText(by.css('.alo-repository-selector__photo+span'));
  }
  bindCode_secondStep_selectCode(account) {
    return new AlaudaDropdown(
      by.xpath(
        '//span[contains(text(), "' +
          account +
          '")]/../../..//aui-multi-select/div',
      ),
      by.css('.aui-option'),
    );
  }
  get bindCode_secondStep_bindButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button--primary', '完成绑定'),
    );
  }
  get bindCode_secondStep_cancelButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button__content', '取消'),
    );
  }
  get bindCode_firstStep_erroralert() {
    return new AlaudaText(
      by.cssContainingText('.aui-notification__title', '代码仓库绑定失败'),
    );
  }
  get addSecretDialog_type() {
    return new AlaudaText(
      by.css('.aui-dialog__content .aui-radio-button__content'),
    );
  }
  get addSecretDialog_cancelButton() {
    return new AlaudaButton(
      by.cssContainingText('aui-dialog-footer .aui-button__content', '取消'),
    );
  }
  get bind_secondStep_createButton() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button__content', '分配仓库'),
    );
  }
  // -----------begin-----------  bind page

  // -----------begin-----------detail page
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.base-body .field'),
      '.base-header',
      'label',
      'span',
    );
  }

  get detailPage_toProject_link() {
    return new AlaudaButton(by.xpath('//a[contains(text(), "前往项目")]'));
  }
  // -----------end-----------detail page

  // ------------------------------------------------------------------------
  // -----------begin-----------  bindCodeRepoService detail page
  get bindCodedetailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .field'),
      '.base-header',
      'label',
      'span',
    );
  }
  get bindCodedetailPage_operate() {
    return new AlaudaButton(by.css('.aui-card__header .alo-menu-button'));
  }
  get bindCodedetailPage_update_button() {
    return new AlaudaButton(
      by.cssContainingText('.aui-menu-item', '更新基本信息'),
    );
  }
  get bindCodedetailPage_remove_button() {
    return new AlaudaButton(by.cssContainingText('.aui-menu-item', '解除绑定'));
  }
  get bindCodedetailPage_assign_button() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button__content', '分配代码仓库'),
    );
  }

  click_bindCodedetailPage_assign_button() {
    this.bindCodedetailPage_assign_button.click();
    // 等待状态消失
    this.waitElementNotPresent(
      $('div .status aui-icon'),
      '【代码仓库获取中...】的提示没有消失',
      60000,
    );
  }

  get bindCodedetailPage_removedialog_ok() {
    return new AlaudaButton(by.css('mat-dialog-actions button:nth-child(1)'));
  }

  get bindCodedetailPage_removedialog_know() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button__content', '知道了'),
    );
  }
  bindCodedetailPage_assignTable_repoName(account, reponame) {
    return new AlaudaText(
      by.xpath(
        '//span[contains(text(), "' + account + '"/"' + reponame + '")]',
      ),
    );
  }
  // -----------end-----------  bindCodeRepoService detail page

  // -----------end-----------  update bindCodeRepoService dialog
  bindUpdateDialog_name(name) {
    return new AlaudaText(
      by.cssContainingText('.aui-form-item__control', "'" + name + "'"),
    );
  }
  get bindUpdateDialog_content() {
    return new AlaudaBasicInfo(
      by.css('.alo-code-binding-basic-edit .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper .aui-form-item__label',
      '.aui-form-item__container span',
    );
  }
  get bindUpdateDialog_input() {
    return new AlaudaBasicInfo(
      by.css('.alo-code-binding-basic-edit .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper .aui-form-item__label',
      '.aui-form-item__container .aui-input',
    );
  }
  get bindUpdateDialog_updateButton() {
    return new AlaudaButton(by.css('.mat-dialog-actions .aui-button--primary'));
  }
  // -----------end-----------  update bindCodeRepoService dialog

  // -----------begin-----------  bindCodeRepoService assign CodeRepo dialog
  get bindCodePage_assigndialog_accounts() {
    return new AlaudaText(by.css('.alo-repository-selector__photo+span'));
  }
  bindCodePage_assigndialog_selectCode(account) {
    return new AlaudaDropdown(
      by.xpath(
        '//span[contains(text(), "' +
          account +
          '")]/../../..//aui-multi-select/div',
      ),
      by.css('.aui-option'),
    );
  }
  get bindCodePage_assigndialog_upate_button() {
    return new AlaudaButton(
      by.cssContainingText('.aui-button__content', '更新'),
    );
  }
  get bindCodePage_assigndialog_title() {
    return new AlaudaText(
      by.cssContainingText('.mat-dialog-header .title', '分配代码仓库'),
    );
  }
  // -----------end-----------  bindCodeRepoService assign CodeRepo dialog

  // -----------begin-----------  bindDockerRegistry detail page
  // -----------end-----------  bindDockerRegistry detail page
}
