/**
 * Created by zhangjiao on 2018/7/30.
 */

import { $, $$, ElementFinder, browser, by } from 'protractor';

import { VolumeMount } from '../../../component/volume-mount.component';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

export class Container extends PageBase {
  // private _spanSelector;

  private _getParent(card: ElementFinder, leftInnerText: string) {
    return card
      .$$('.field')
      .filter(elem => {
        return elem
          .$('label')
          .getText()
          .then(text => {
            return text.trim() === leftInnerText;
          });
      })
      .first();
  }

  /**
   * 添加存储卷挂载
   */
  get volumeMont() {
    return new VolumeMount();
  }

  /**
   * 根据左侧文字获得右侧控件
   * @param leftInnerText 左侧文字
   */
  getElemByleftText(card: ElementFinder, leftInnerText: string): ElementFinder {
    const parentElem = this._getParent(card, leftInnerText);

    let rightItem;
    if (leftInnerText === 'CPU' || leftInnerText === '内存') {
      rightItem = parentElem.$$('span');
    } else if (leftInnerText === '标签' || leftInnerText === '注解') {
      rightItem = parentElem.$$('.plain-container__label');
    } else if (leftInnerText === '类型') {
      rightItem = parentElem.$('span:nth-child(3)');
    } else {
      rightItem = parentElem.$$('span').first();
    }
    return rightItem;
  }

  /**
   * 单击 exec 按钮
   * @param execButtonSelector exec 按钮的selector
   */
  exec(
    card: ElementFinder,
    number: number,
    execButtonSelector: string = '.actions a:nth-child(1)',
  ) {
    this.waitElementPresent(card, '卡片没出现');
    this.waitElementPresent(
      card.$(execButtonSelector),
      '详情页 exec 按钮没出现',
    );
    card.$(execButtonSelector).click();
    const button = $(
      `.aui-menu-group__content aui-menu-item:nth-child(${number}) button`,
    );
    this.waitElementPresent(button, 'exec 下拉框按钮没找到');
    button.click();
    browser.sleep(1000);
  }

  /**
   * 单击日志按钮
   * @param logButtonSelector 日志按钮的选择器
   */
  viewLog(
    card: ElementFinder,
    logButtonSelector: string = '.actions a:nth-child(2)',
  ) {
    CommonPage.waitElementPresent(card.$(logButtonSelector));
    card.$(logButtonSelector).click();
    browser.sleep(100);
  }

  /**
   * 打开存储卷挂载
   */
  openVolumeMount() {
    const volumeDropdwon = new AlaudaDropdown(
      by.css('.actions a:nth-child(3)'),
      by.css('aui-tooltip button'),
    );

    volumeDropdwon.select('添加存储卷挂载');
  }

  /**
   * 打开存储卷挂载
   */
  option(
    card: ElementFinder,
    item: string,
    buttonSelector: string = '.actions a:nth-child(3)',
  ) {
    card.$(buttonSelector).click();
    $$('aui-tooltip button').each(elem => {
      elem.getText().then(text => {
        if (text.trim() === item) {
          elem.click();
          browser.sleep(100);
        }
      });
    });
  }
}
