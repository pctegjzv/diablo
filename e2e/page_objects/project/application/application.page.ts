/**
 * Created by zhangjiao on 2018/3/20.
 */

import { $$, ElementFinder, browser, by, promise } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { AlaudaYamlEditor } from '../../../element_objects/alauda.ymaleditor';
import { CommonMethod } from '../../../utility/common.method';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

import { ContainerUpdatePage } from './container.update.page';
import { ApplicationDetailPage } from './detail.page';
import { ApplicationListPage } from './list.page';
import { ResourceDetailPage } from './resource.detail.page';
import { TemplatePage } from './template.create.page';
import { TerminalPage } from './terminal.page';

export class ApplicationPage extends PageBase {
  /**
   * 应用的创建页面
   */
  get templateCreatePage() {
    return new TemplatePage();
  }

  /**
   * 应用的列表页面
   */
  get listPage() {
    return new ApplicationListPage();
  }

  /**
   * 应用的详情页
   */
  get detailPage() {
    return new ApplicationDetailPage();
  }

  /**
   * 应用下资源(D,DS,SS)的详情页
   */
  get resourceDetailPage() {
    return new ResourceDetailPage();
  }

  /**
   * 更新容器页面
   */
  get containerUpdatePage() {
    return new ContainerUpdatePage();
  }

  get terminalPage() {
    return new TerminalPage();
  }

  /**
   * 通过模版创建应用
   * @param testData 模版创建应用的测试数据
   * @param kind 模版种类
   */
  createAppByTemplate(testData, kind: string): promise.Promise<boolean> {
    this.clickLeftNavByText('应用');
    this.getButtonByText('创建应用').click();
    browser.sleep(1000);
    this.clickMiniCardByInnerText('模板创建');
    this.templateCreatePage.clickMiniCardByInnerText(kind);

    this.templateCreatePage.fillForm(testData);
    browser.sleep(100);
    this.getButtonByText('创建').click();
    return this.waitProgressBarNotPresent();
  }

  /**
   * 单击mini card
   * @param text mini card 上显示的文字
   */
  clickMiniCardByInnerText(text: string) {
    const buttonMiniCard = new AlaudaButton(
      by.xpath(
        '//a[@class="mini-card"]/span[contains(text(), "' + text + '")]',
      ),
    );
    CommonPage.waitElementPresent(buttonMiniCard.button);
    buttonMiniCard.click();
    this.waitProgressBarNotPresent();
    browser.sleep(100);
  }

  /**
   * 获得minicard 上的文字
   */
  getMiniCatdText(): promise.Promise<string> {
    return $$('a[class="mini-card"] span').getText();
  }

  /**
   * 通过YAML 创建应用
   */
  createAppByYaml(
    name: string,
    label: string,
    image: string,
  ): promise.Promise<boolean> {
    this.clickLeftNavByText('应用');
    this.getButtonByText('创建应用').click();
    browser.sleep(1000);
    this.clickMiniCardByInnerText('YAML 创建');
    // 将yaml文件拷贝至yaml编辑框
    const yamlFile = CommonMethod.readyamlfile('alauda.application.yaml', {
      '${NAME}': name,
      '${LABEL}': label,
      '${IMAGE}': image,
    });
    this.createPage_yaml_textarea.setYamlValue(yamlFile);
    this.getButtonByText('创建').click(); // 点击“创建”按钮
    return this.waitProgressBarNotPresent();
  }

  verifyResult(expectResult) {
    for (const cardName in expectResult) {
      if (expectResult.hasOwnProperty(cardName)) {
        const cardInfo = expectResult[cardName];
        for (const item in cardInfo) {
          if (cardInfo.hasOwnProperty(item)) {
            if (item !== 'name') {
              const temp = this.detailPage.getCardBodyItem(
                cardName,
                cardInfo['name'],
                item,
              );
              CommonPage.waitElementPresent(temp, 60000);
              if (item === 'CPU' || item === '内存') {
                expect(temp.getText()).toEqual(cardInfo[item]);
              } else {
                expect(temp.getText()).toBe(cardInfo[item]);
              }
            }
          }
        }
      }
    }
  }

  /**
   * 获得错误提示信息
   * @param leftText 左侧文字
   */
  getErrorHint(leftText: string): ElementFinder {
    return this.getElementByText(leftText, '.aui-form-field__error-hint');
  }

  // -----------begin-----------Application create page

  get createPage_inputbox() {
    return new AlaudaBasicInfo(
      by.css('.alo-card__container .aui-form-field'),
      '.base-header',
      'label',
      'input',
    );
  }

  get createPage_createButton() {
    return this.getButtonByText('创建');
  }

  get createPage_cancelButton() {
    return this.getButtonByText('取消');
  }

  get createPage_port_inputbox() {
    return new AlaudaInputbox(by.name('port'));
  }

  get createPage_env_inputbox() {
    return new AlaudaInputbox(by.xpath('//input[@type="text"]'));
  }

  get createPage_yaml_textarea() {
    return new AlaudaYamlEditor(
      by.css('.aui-monaco-editor-container'),
      by.xpath(
        '//following::div[contains(@class,"aui-code-editor-toolbar__control-button")]',
      ),
    );
  }

  get createPage_yaml_assertive() {
    return new AlaudaText(by.css('.cdk-visually-hidden'));
  }
  // -----------end-----------Application create page
}
