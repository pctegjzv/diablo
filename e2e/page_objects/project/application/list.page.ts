/**
 * Created by zhangjiao on 2018/3/20.
 */

import { $$, ElementFinder, browser, by, promise } from 'protractor';

import { AlaudaSearchBox } from '../../../element_objects/alauda.searchbox';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

export class ApplicationListPage extends PageBase {
  /**
   * 检索框
   */
  get searchBox(): AlaudaSearchBox {
    return new AlaudaSearchBox(
      by.css('aui-search input[placeholder="支持按名称搜索"]'),
      by.css('.aui-search__button-icon aui-icon'),
    );
  }

  /**
   * 获得当前页的app 数量
   */
  get appCount(): promise.Promise<number> {
    const applist = $$('alo-application-list-card div[class=app-name]');
    return applist.count();
  }

  /**
   * 按名称检索APP
   * @param name app 的名称
   */
  searchAppByName(name, rowCount) {
    this.searchBox.search(name);
    return CommonPage.waitRowcountTextChangeTo(
      $$('alo-application-list-card div[class=app-name]'),
      rowCount,
    );
  }

  /**
   * 判断 App 是否存在列表中
   * @param appName App 的名字
   */
  isExist(appName): promise.Promise<boolean> {
    const applist = $$('alo-application-list-card div[class=app-name]');
    const app = applist
      .filter(elem => {
        return elem.getText().then(text => {
          return appName === text;
        });
      })
      .first();
    return app.isPresent();
  }

  /**
   * 单击App 的名称进入详情也
   * @param appName  App 的名字
   */
  clickByAppName(appName) {
    const applist = $$('alo-application-list-card div[class=app-name]');
    CommonPage.waitElementPresent(applist.first());
    const app = applist
      .filter(elem => {
        return elem.getText().then(text => {
          return appName === text;
        });
      })
      .first();
    CommonPage.waitElementPresent(app);
    app.click();
    return CommonPage.waitElementNotPresent(app);
  }

  private _getAppListCardByName(appName) {
    const applist = $$('alo-application-list-card .card-container');
    const appListCard = applist
      .filter(elem => {
        const appElem = elem.$('.app-info .app-name a');
        this.waitElementPresent(
          appElem,
          `在应用列表页面，没有找到【${appName}】应用`,
        );
        return appElem.getText().then(text => {
          return appName === text.trim();
        });
      })
      .first();
    this.waitElementPresent(
      appListCard,
      `在应用列表页面，没有找到【${appName}】所在 alo-application-list-card`,
      10000,
    );
    return appListCard;
  }

  private _getResourceListItem(
    appName: string,
    resourceType: string,
    resourceName: string,
  ) {
    const appListCard = this._getAppListCardByName(appName);
    this.expandByAppName(appName);
    const resourceListItem = appListCard
      .$$('.resource-list-item')
      .filter(elem => {
        const iconElem = elem.$('.resource-list-item .resoure-icon');
        this.waitElementPresent(iconElem, `没有找到任何资源类型控件`);
        const nameElem = elem.$('.resource-list-item .resoure-name a');
        this.waitElementPresent(nameElem, `没有找到任何资源名称控件`, 60000);
        try {
          return iconElem.getText().then(text => {
            const hasType = resourceType === text;
            return nameElem.getText().then(name => {
              return name === resourceName && hasType;
            });
          });
        } catch {
          return iconElem.getText().then(text => {
            const hasType = resourceType === text;
            return nameElem.getText().then(name => {
              return name === resourceName && hasType;
            });
          });
        }
      })
      .first();
    this.waitElementPresent(
      resourceListItem,
      `根据 ${appName}-->${resourceType}-->${resourceName} 没有找到任何资源`,
    );
    return resourceListItem;
  }

  /**
   * 获得资源类型
   * @param appName 应用名称
   * @param resourceType 资源类型
   * @param resourceName 资源名称
   */
  clickByResourceName(
    appName: string,
    resourceType: string,
    resourceName: string,
  ) {
    let resourceItem: ElementFinder;
    try {
      resourceItem = this._getResourceListItem(
        appName,
        resourceType,
        resourceName,
      );
    } catch {
      resourceItem = this._getResourceListItem(
        appName,
        resourceType,
        resourceName,
      );
    }
    this.waitElementPresent(resourceItem, `没有找到资源${resourceName}`);
    const name = resourceItem.$('.resource-list-item .resoure-name a');
    this.waitElementPresent(name, '列表页，没有找到超链接');
    name.click();
    browser.sleep(100);
    this.waitProgressBarNotPresent();
    return browser.sleep(100);
  }

  /**
   * 获得资源类型
   * @param appName 应用名称
   * @param resourceType 资源类型
   * @param resourceName 资源名称
   */
  getResourceStatus(
    appName: string,
    resourceType: string,
    resourceName: string,
  ): ElementFinder {
    const resourceItem = this._getResourceListItem(
      appName,
      resourceType,
      resourceName,
    );
    return resourceItem.$('.resource-list-item .resoure-status span');
  }

  /**
   * 获得资源类型
   * @param appName 应用名称
   * @param resourceType 资源类型
   * @param resourceName 资源名称
   */
  getResourceImage(
    appName: string,
    resourceType: string,
    resourceName: string,
  ): ElementFinder {
    const resourceItem = this._getResourceListItem(
      appName,
      resourceType,
      resourceName,
    );
    return resourceItem.$('.resource-list-item .resoure-images');
  }

  /**
   * 获得资源类型
   * @param appName 应用名称
   * @param resourceType 资源类型
   * @param resourceName 资源名称
   */
  selectResourceAction(
    appName: string,
    resourceType: string,
    resourceName: string,
    menuItemText: string,
  ) {
    const resourceItem = this._getResourceListItem(
      appName,
      resourceType,
      resourceName,
    );
    resourceItem.$('.resource-list-item .resource-action a').click();
    const menuButton = $$('aui-tooltip aui-menu-item button')
      .filter(elem => {
        return elem.getText().then(text => {
          return text === menuItemText;
        });
      })
      .first();
    this.waitElementPresent(
      menuButton,
      `在 aui-tooltip aui-menu-item 的下拉框中没有找到${menuItemText}`,
    );
    menuButton.click();
    browser.sleep(100);
  }

  /**
   * 获得App 的状态
   * @param appName App 的名字
   */
  getAppStatus(appName) {
    const app = this._getAppListCardByName(appName);
    return app.$('.app-status .aviliable-data');
  }

  /**
   * 应用是否展开了
   * @param appName App 的名字
   */
  isExpand(appName) {
    const app = this._getAppListCardByName(appName);
    return app.$('.resource-list-container').isPresent();
  }

  /**
   * 展开应用
   * @param appName App 的名字
   */
  expandByAppName(appName) {
    const app = this._getAppListCardByName(appName);
    this.isExpand(appName).then(isPresent => {
      if (!isPresent) {
        app.$('.app-info .row-image').click(); // 找到展开按钮单击
        this.waitElementPresent(
          app.$('.resource-list-container'),
          `应用列表页面${appName} 没有正常展开`,
        );
        browser.sleep(100);
      }
    });
  }

  /**
   * 折叠应用
   * @param appName App 的名字
   */
  collapsByAppName(appName) {
    const app = this._getAppListCardByName(appName);
    this.isExpand(appName).then(isPresent => {
      if (isPresent) {
        app.$('.app-info .row-image').click(); // 找到折叠按钮单击
        this.waitElementNotPresent(
          app.$('.resource-list-container'),
          `应用列表页面${appName} 没有正常折叠`,
        );
        browser.sleep(100);
      }
    });
  }
}
