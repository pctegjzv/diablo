/**
 * Created by zhangjiao on 2018/3/20.
 */

import { $, ElementFinder, browser, by } from 'protractor';

import { KeyValueControl } from '../../../component/key_value.component';
import { Logviewer } from '../../../component/log-verer.component';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaConfirmDialog } from '../../../element_objects/alauda.confirmdialog';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaTabs } from '../../../element_objects/alauda.tabs';
import { AlaudaAuiTable } from '../../../element_objects_new/alauda.aui_table';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';
import { Container } from '../../project/application/container.page';
import { PodScaler } from '../../project/application/podScaler.page';

import { ConfigManager } from './configmanager.page';

export class ResourceDetailPage extends PageBase {
  constructor() {
    super();
  }

  /**
   * 根据Header 的显示文字找到card
   * @param name header 的显示文字
   * @param cardNameSelector header 选择器
   */
  private _getCard(name: string): ElementFinder {
    let card;
    if (name === '基本信息') {
      card = $('alo-k8s-resource-detail alo-k8s-resource-basic-info');
    } else {
      card = $('alo-k8s-resource-detail alo-card:nth-child(3) .alo-container');
    }
    this.waitElementPresent(card, `卡片 ${name} 没找到`);
    return card;
  }

  get container() {
    return new Container();
  }

  /**
   * 根据左侧文字，获得Container 卡片里的元素
   * @param cardName 卡片名称
   * @param itemName 左侧的名称
   */
  getCardBodyItem(cardName: string, itemName: string): any {
    const card = this._getCard(cardName);
    this.waitElementPresent(card, `卡片${cardName} 没找到`);
    return this.container.getElemByleftText(card, itemName);
  }

  /**
   *
   * @param cardName 卡片名称
   * @param containerName congcongcong
   * @param index 容器的index
   */
  exec(cardName: string, index: number = 1) {
    const card = this._getCard(cardName);
    return this.container.exec(card, index);
  }

  /**
   * 单击日志按钮
   * @param cardName 卡片名称
   */
  viewLog(cardName: string) {
    const card = this._getCard(cardName);
    return this.container.viewLog(card);
  }

  /**
   * 容器选择一个操作，（添加存储卷，更新容器）
   * @param cardName 卡片名称
   * @param actionName 动作名称
   */
  action(cardName: string, actionName: string) {
    const card = this._getCard(cardName);
    this.container.option(card, actionName);
    this.waitProgressBarNotPresent();
    return browser.sleep(100);
  }

  get buttonEditLabel() {
    return new AlaudaButton(
      by.css(
        'alo-k8s-resource-basic-info div .field:nth-child(4)  alo-resource-labels-field aui-icon',
      ),
    );
  }

  get buttonEditAnnotation() {
    return new AlaudaButton(
      by.css(
        'alo-k8s-resource-basic-info div .field:nth-child(5)  alo-resource-labels-field aui-icon',
      ),
    );
  }

  get keyValueControl() {
    return new KeyValueControl();
  }

  /**
   * 配置管理详情页
   */
  get configManager() {
    return new ConfigManager($('alo-pod-env-list'));
  }

  /**
   * 已经挂载的存储卷的表格
   */
  get tableVolumeMount() {
    return new AlaudaAuiTable(
      $('.volume-info-container>table'),
      '.volume-info-container>table>tbody>tr>th',
      '.volume-info-container>table>tbody>tr>td',
      '.volume-info-container>table>tbody>tr',
    );
  }

  /**
   * 确认对话框
   */
  get confirmDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('aui-confirm-dialog'),
      '.aui-confirm-dialog__title span:nth-child(2)',
      'aui-confirm-dialog button:nth-child(1)',
      `aui-confirm-dialog button:nth-child(2)`,
    );
  }

  get logViewer() {
    return new Logviewer();
  }

  get tabs() {
    return new AlaudaTabs();
  }

  getPodScaler(cardName: string) {
    CommonPage.waitElementPresent(this._getCard(cardName));
    return new PodScaler(this._getCard(cardName));
  }

  get option() {
    return new AlaudaDropdown(
      by.css('.page-header .aui-button__content'),
      by.css('aui-menu-item button'),
    );
  }

  verifyResult(expectResult) {
    for (const cardName in expectResult) {
      if (expectResult.hasOwnProperty(cardName)) {
        const cardInfo = expectResult[cardName];
        for (const item in cardInfo) {
          if (cardInfo.hasOwnProperty(item)) {
            const temp = this.getCardBodyItem(cardName, item);
            CommonPage.waitElementPresent(temp);
            if (
              item === 'CPU' ||
              item === '内存' ||
              item === '标签' ||
              item === '注解'
            ) {
              expect(temp.getText()).toEqual(cardInfo[item]);
            } else {
              expect(temp.getText()).toBe(cardInfo[item]);
            }
          }
        }
      }
    }
  }
}
