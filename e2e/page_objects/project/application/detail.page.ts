/**
 * Created by zhangjiao on 2018/3/20.
 */

import { $, $$, ElementFinder, browser, by } from 'protractor';

import { Logviewer } from '../../../component/log-verer.component';
import { TableComponent } from '../../../component/table.component';
import { AlaudaConfirmDialog } from '../../../element_objects/alauda.confirmdialog';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

class Container {
  private _card;
  private _itemSelector;
  private _labelSelector;
  private _spanSelector;
  constructor(
    card: ElementFinder,
    itemSelector: string = '.field',
    labelSelector: string = 'label',
    spanSelector: string = 'span',
  ) {
    this._card = card;
    this._itemSelector = itemSelector;
    this._labelSelector = labelSelector;
    this._spanSelector = spanSelector;
  }

  getElemByleftText(leftInnerText: string): ElementFinder {
    const item = this._card
      .$$(`${this._itemSelector}`)
      .filter(elem => {
        return elem
          .$(`${this._itemSelector} ${this._labelSelector}`)
          .getText()
          .then(text => {
            return text === leftInnerText;
          });
      })
      .first();
    CommonPage.waitElementPresent(item, 10000).then(isPresent => {
      if (!isPresent) {
        console.log(`验证的元素【${leftInnerText}】没出现`);
      }
    });
    if (leftInnerText === 'CPU' || leftInnerText === '内存') {
      return item.$$(this._spanSelector);
    } else {
      return item.$(this._spanSelector);
    }
  }

  /**
   * 单击 exec 按钮
   * @param execButtonSelector exec 按钮的selector
   */
  exec(number, execButtonSelector = '.actions a:nth-child(1)') {
    CommonPage.waitElementPresent(this._card.$(execButtonSelector), 60000);
    this._card.$(execButtonSelector).click();
    browser.sleep(100);
    CommonPage.waitElementPresent(
      $(`.aui-menu-group aui-menu-item:nth-child(${number})`),
    );
    $(`.aui-menu-group aui-menu-item:nth-child(${number})`).click();
  }

  /**
   * 单击日志按钮
   * @param logButtonSelector 日志按钮的选择器
   */
  viewLog(logButtonSelector = '.actions a:nth-child(2)') {
    CommonPage.waitElementPresent(this._card.$(logButtonSelector), 60000);
    this._card.$(logButtonSelector).click();
    browser.sleep(100);
  }
}

class PodScaler {
  private _card;
  private _podScaler_selector;
  constructor(
    card: ElementFinder,
    podScaler_selector: string = 'alo-pods-scaler',
  ) {
    this._podScaler_selector = podScaler_selector;
    this._card = card;
  }

  get count(): ElementFinder {
    CommonPage.waitElementPresent(this._card);
    return this._card.$(`${this._podScaler_selector} .count`);
  }
  get label(): ElementFinder {
    return this._card.$(`${this._podScaler_selector} .label`);
  }
  mins() {
    const buttonMins = this._card.$(
      `${this._podScaler_selector} aui-icon[icon="basic:minus"]`,
    );
    CommonPage.waitElementPresent(buttonMins);
    buttonMins.click();
    return browser.sleep(500);
  }

  plus() {
    const buttonPlus = this._card.$(
      `${this._podScaler_selector} aui-icon[icon="basic:plus"]`,
    );
    CommonPage.waitElementPresent(buttonPlus);
    buttonPlus.click();
    return browser.sleep(500);
  }
}

export class ApplicationDetailPage extends PageBase {
  private _card_selector;
  private _header_selector;

  constructor(
    card_selector: string = 'alo-card-section',
    header_selector: string = '.alo-card__section-header',
  ) {
    super();
    this._card_selector = card_selector;
    this._header_selector = header_selector;
  }

  /**
   * 根据Header 的显示文字找到card
   * @param name header 的显示文字
   * @param cardNameSelector header 选择器
   */
  private _getCard(name: string): ElementFinder {
    let cardNameSelector;
    if (name === '基本信息') {
      cardNameSelector = 'div';
    } else {
      cardNameSelector = 'div span[class="resoure-kind"]';
    }

    const elemCard = $$(`${this._card_selector}`)
      .filter(elem => {
        const card = elem.$(`${this._header_selector} ${cardNameSelector}`);
        return card.isPresent().then(isPresent => {
          if (isPresent) {
            return card.getText().then(text => {
              return text.trim() === name;
            });
          } else {
            return false;
          }
        });
      })
      .first();
    this.waitElementPresent(elemCard, '卡片没找到', 60000);
    return elemCard;
  }

  private _getContainer(
    cardName: string,
    containerName: string,
    containerSelector: string = 'alo-container .alo-container',
    containerNameSelector: string = 'div[class*="field full name"] span',
  ): ElementFinder {
    const card = this._getCard(cardName);
    return card
      .$$(containerSelector)
      .filter(elem => {
        return elem
          .$(`${containerSelector} ${containerNameSelector}`)
          .getText()
          .then(text => {
            return text.trim() === containerName;
          });
      })
      .first();
  }

  /**
   * 根据左侧文字，获得Container 卡片里的元素
   * @param cardName 卡片名称
   * @param containerName container 名称
   * @param itemName 左侧的名称
   */
  getCardBodyItem(
    cardName: string,
    containerName: string,
    itemName: string = '',
  ): any {
    if (cardName === '其它资源') {
      return new TableComponent();
    } else if (cardName === '基本信息') {
      const temp = new Container(
        this._getContainer(
          cardName,
          containerName,
          'alo-card:nth-child(2) .alo-card__section-body',
          '.base-body div:nth-child(1) span',
        ),
      );
      return temp.getElemByleftText(itemName);
    } else {
      const temp = new Container(
        this._getContainer(
          cardName,
          containerName,
          'alo-container div[class="base-body info"]',
          'div[class*="field full name"] span',
        ),
      );
      return temp.getElemByleftText(itemName);
    }
  }

  /**
   * 流水线table
   */
  get resourceTable() {
    return new TableComponent('alo-card');
  }

  /**
   *
   * @param cardName 卡片名称
   * @param containerName congcongcong
   * @param index 容器的index
   */
  exec(cardName: string, containerName: string, index: number = 1) {
    const temp = new Container(
      this._getContainer(
        cardName,
        containerName,
        'alo-container',
        'div[class*="field full name"] span',
      ),
    );
    return temp.exec(index);
  }

  /**
   * 单击日志按钮
   * @param cardName 卡片名称
   * @param containerName congcongcong
   */
  viewLog(cardName: string, containerName: string) {
    const temp = new Container(
      this._getContainer(
        cardName,
        containerName,
        'alo-container',
        'div[class*="field full name"] span',
      ),
    );
    return temp.viewLog();
  }

  /**
   * 确认对话框
   */
  get confirmDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('aui-confirm-dialog'),
      '.aui-confirm-dialog__title span:nth-child(2)',
      'aui-confirm-dialog button:nth-child(1)',
      `aui-confirm-dialog button:nth-child(2)`,
    );
  }

  get logViewer() {
    return new Logviewer();
  }

  clickTabByName(name: string, tabitemSelector: string = 'ul[class=tabs] a') {
    const button = $$(tabitemSelector)
      .filter(elem => {
        return elem.getText().then(text => {
          return text === name;
        });
      })
      .first();
    CommonPage.waitElementPresent(button);
    button.click();
    browser.sleep(1000);
    this.waitProgressBarNotPresent();
  }

  getPodScaler(cardName: string) {
    CommonPage.waitElementPresent(this._getCard(cardName));
    return new PodScaler(this._getCard(cardName));
  }

  get option() {
    return new AlaudaDropdown(
      by.css('.page-header .aui-button__content'),
      by.css('aui-menu-item button'),
    );
  }
}
