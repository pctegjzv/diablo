/**
 * Created by liuwei on 2018/8/9.
 */

import { $, ElementFinder, browser, by, promise } from 'protractor';

import { VolumeMount } from '../../../component/volume-mount.component';
import { AuiSelect } from '../../../element_objects/alauda.auiSelect';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaTable } from '../../../element_objects/alauda.table';
import { ArrayFormTable } from '../../../element_objects_new/alauda.arrayFormTable';
import { AuiMultiSelect } from '../../../element_objects_new/alauda.auiMultiSelect';
import { PageBase } from '../../page.base';

class AuiInputGroup {
  private _auiInputGroup;

  constructor(labelSelector: string) {
    this._auiInputGroup = $(labelSelector);
  }
  get label(): ElementFinder {
    return this._auiInputGroup.$('.size-title');
  }

  get labelText() {
    return this.label.getText();
  }

  get inputBox(): ElementFinder {
    return this._auiInputGroup.$('.aui-input-group div:nth-child(2) input');
  }

  input(value: string) {
    this.inputBox.clear();
    this.inputBox.sendKeys(value);
    browser.sleep(100);
  }

  select(value: string) {
    this.auiSelect.select(value);
  }

  /**
   * 获取文本框的值
   */
  get value(): promise.Promise<string> {
    return this.inputBox.getAttribute('value');
  }

  get auiSelect(): AuiSelect {
    return new AuiSelect(
      this._auiInputGroup.$('aui-icon'),
      this._auiInputGroup.$('aui-select input'),
      'aui-option div',
    );
  }
}

class ContainerSize {
  get memoryRequest() {
    return new AuiInputGroup('.memory-size aui-input-group:nth-child(2)');
  }
  get memoryLimit() {
    return new AuiInputGroup('.memory-size aui-input-group:nth-child(4)');
  }
  get cpuRequest() {
    return new AuiInputGroup('.cpu-size aui-input-group:nth-child(2)');
  }
  get cpuLimit() {
    return new AuiInputGroup('.cpu-size aui-input-group:nth-child(4)');
  }
}

class Storage extends PageBase {
  /**
   * 挂载存储的配置项table
   */
  get storageTable() {
    return new AlaudaTable(
      '.volume-info-container>table>tbody>tr>th',
      '.volume-info-container>table>tbody>tr>td',
      '.volume-info-container>table>tbody>tr',
    );
  }

  get volumeMountDialog() {
    return new VolumeMount();
  }

  /**
   * 打开编辑dialog 页面
   * @param containerPath 容器路径，唯一值
   */
  editStorageItem(containerPath) {
    this.storageTable
      .getRow([containerPath])
      .$('.action button:nth-child(1)')
      .click();
    browser.sleep(100);
  }

  /**
   * 删除一个配置项
   * @param containerPath 容器路径，唯一值
   */
  deleteStorageItem(containerPath) {
    this.storageTable
      .getRow([containerPath])
      .$('.action button:nth-child(2)')
      .click();
    browser.sleep(100);
  }

  /**
   * 单击更新按钮
   */
  clickUpdateButton() {
    this.getButtonByText('更新').click();
  }
}

export class ContainerUpdatePage extends PageBase {
  get imageAddress() {
    return new AlaudaInputbox(by.css('input[name=image]'));
  }
  get containerSize() {
    return new ContainerSize();
  }

  get env() {
    return new ArrayFormTable(
      $('alo-container-update-env alo-array-form-table'),
    );
  }

  get config() {
    return new AuiMultiSelect(
      $('alo-env-from-form aui-multi-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }

  get storage() {
    return new Storage();
  }

  /**
   * 添加存储卷挂载
   */
  get volumeMont() {
    return new VolumeMount();
  }

  get mountVolumeButton() {
    return new AlaudaButton(by.css('.add-volume button'));
  }

  updateContainerSize(size) {
    this.waitElementPresent(
      this.containerSize.memoryRequest.inputBox,
      '更新容器的页面没有正常加载',
    );
    for (const item in size) {
      if (size.hasOwnProperty(item)) {
        switch (item) {
          case '内存请求值':
            this.containerSize.memoryRequest.input(size[item][0]);
            this.containerSize.memoryRequest.select(size[item][1]);
            break;
          case '内存限制值':
            this.containerSize.memoryLimit.input(size[item][0]);
            this.containerSize.memoryLimit.select(size[item][1]);
            break;
          case 'CPU请求值':
            this.containerSize.cpuRequest.input(size[item][0]);
            this.containerSize.cpuRequest.select(size[item][1]);
            break;
          case 'CPU限制值':
            this.containerSize.cpuLimit.input(size[item][0]);
            this.containerSize.cpuLimit.select(size[item][1]);
            break;
        }
      }
    }
  }

  mount(type: string, source: string, containerPath: string) {
    this.mountVolumeButton.click();
    this.volumeMont.mount(type, source, containerPath);
  }

  refMount(type: string, source: string, arrayValue) {
    this.mountVolumeButton.click();
    this.volumeMont.refMount(type, source, arrayValue);
  }
}
