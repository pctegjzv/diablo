/**
 * Created by liuwei on 2018/8/14.
 */

import { $, $$, ElementFinder, browser, promise } from 'protractor';

import { ArrayFormTable } from '../../../element_objects_new/alauda.arrayFormTable';
import { AlaudaAuiTable } from '../../../element_objects_new/alauda.aui_table';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

interface Item {
  index: number;
  text: string;
}

class AuiCard {
  private _auiCard: ElementFinder;
  constructor(auiCard: ElementFinder) {
    this._auiCard = auiCard;
  }

  get auiCard() {
    CommonPage.waitElementPresent(this._auiCard);
    return this._auiCard;
  }

  get title(): ElementFinder {
    return this.auiCard.$('.aui-card__header');
  }

  get buttonEdit(): ElementFinder {
    return this.auiCard.$('.aui-card__header button');
  }

  /**
   * 单击编辑按钮
   */
  click(): promise.Promise<void> {
    CommonPage.waitElementPresent(this.buttonEdit);
    this.buttonEdit.click();
    return browser.sleep(100);
  }

  /**
   * dialog 页面更新环境变量的表格
   */
  get env() {
    return new ArrayFormTable($('aui-dialog-content alo-array-form-table'));
  }

  /**
   * 获得环境变量
   */
  get auiTable(): AlaudaAuiTable {
    return new AlaudaAuiTable(this.auiCard.$('.aui-card__content aui-table'));
  }

  /**
   * 获得配置引用
   */
  get envTags(): promise.Promise<string> {
    return this.auiCard.$$('.aui-card__content aui-tag').getText();
  }
}

export class ConfigManager extends PageBase {
  private _envList: ElementFinder;
  constructor(alo_pod_env_list: ElementFinder) {
    super();
    this._envList = alo_pod_env_list;
  }

  get envList() {
    this.waitElementPresent(this._envList, '配置管理页面没有正常加载');
    return this._envList;
  }

  private _getIndex(containerName: string): promise.Promise<number> {
    return this.envList.getTagName().then(tagName => {
      const list = this.envList.$$(`${tagName}>*`).map((elem, index) => {
        return {
          index: index,
          text: tagName === 'div' ? elem.getText() : 'empty',
        };
      });

      // 遍历表头，返回{containerName} 在第几行
      return list.then(containerList => {
        let index = 0;
        containerList.forEach((item: Item) => {
          if (item.text.trim() === containerName) {
            index = item.index;
            return;
          }
        });
        return index;
      });
    });
  }

  /**
   * 按照名称获得auicard
   * @param containerName 名称
   * @param type 类型，环境变量， 配置引用
   */
  getAuiCardByName(
    type: string,
    containerName: string = '',
  ): promise.Promise<AuiCard> {
    return this._getIndex(containerName).then(index => {
      if (containerName !== '') {
        index = index + 1;
      }
      return this.envList.getTagName().then(tagName => {
        switch (type) {
          case '环境变量':
            return new AuiCard($$(`${tagName}>*`).get(index));
          default:
            return new AuiCard($$(`${tagName}>*`).get(index + 1));
        }
      });
    });
  }
}
