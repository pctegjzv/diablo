/**
 * Created by liuwei on 2018/7/16.
 */

import { $ } from 'protractor';

import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

export class TerminalPage extends PageBase {
  constructor() {
    super();
  }

  /**
   * Exec 是连接状态么
   */
  get isGoodConnect() {
    const temp = 'alo-shell .status-indicator--goodConnection';
    CommonPage.waitElementPresent($(temp), 60000);
    return $(temp).isPresent();
  }
}
