/**
 * Created by liuwei on 2018/7/30.
 */

import { ElementFinder, browser } from 'protractor';

import { CommonPage } from '../../../utility/common.page';

export class PodScaler {
  private _card;
  private _podScaler_selector;
  constructor(
    card: ElementFinder,
    podScaler_selector: string = 'alo-pods-scaler',
  ) {
    this._podScaler_selector = podScaler_selector;
    this._card = card;
  }

  get count() {
    CommonPage.waitElementPresent(this._card);
    return this._card.$(`${this._podScaler_selector} .count`);
  }
  get label() {
    return this._card.$(`${this._podScaler_selector} .label`);
  }
  mins() {
    const buttonMins = this._card.$(
      `${this._podScaler_selector} aui-icon[icon="basic:minus"]`,
    );
    CommonPage.waitElementPresent(buttonMins);
    buttonMins.click();
    return browser.sleep(100);
  }

  plus() {
    const buttonPlus = this._card.$(
      `${this._podScaler_selector} aui-icon[icon="basic:plus"]`,
    );
    CommonPage.waitElementPresent(buttonPlus);
    buttonPlus.click();
    return browser.sleep(100);
  }
}
