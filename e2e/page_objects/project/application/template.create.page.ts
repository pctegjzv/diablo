/**
 * Created by zhangjiao on 2018/3/20.
 */

import { $, $$, browser, by, element, promise } from 'protractor';

import { AlaudaButton } from '../../../element_objects_new/alauda.button';
import { AlaudaDropdown } from '../../../element_objects_new/alauda.dropdown';
import { CommonPage } from '../../../utility/common.page';
import { PageBase } from '../../page.base';

export class TemplatePage extends PageBase {
  /**
   * 单击mini card
   * @param text mini card 上显示的文字
   */
  clickMiniCardByInnerText(text: string) {
    const buttonMiniCard = new AlaudaButton(
      element(
        by.xpath(
          '//a[@class="mini-card"]/span[contains(text(), "' + text + '")]',
        ),
      ),
    );
    CommonPage.waitElementPresent(buttonMiniCard.button);
    buttonMiniCard.click();
    this.waitProgressBarNotPresent();
    browser.sleep(100);
  }

  get buttonCloase() {
    return $('mat-dialog-container .close');
  }

  get buttonBack() {
    return $('.mat-dialog-header .back-title');
  }

  close(): promise.Promise<boolean> {
    this.buttonCloase.click();
    browser.sleep(100);
    return this.waitElementNotPresent(
      this.buttonCloase,
      '单击关闭按钮，页面没关闭',
    );
  }

  back(): promise.Promise<boolean> {
    this.buttonBack.click();
    browser.sleep(100);
    return this.waitElementNotPresent(
      this.buttonBack,
      '单击返回按钮，页面没关闭',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname: string = 'input'): any {
    switch (text) {
      case '镜像源证书':
        return new AlaudaDropdown(
          $('alo-card-section:nth-child(2) select'),
          $$('alo-card-section:nth-child(2) option'),
        );
      case 'Jenkins 服务':
        return new AlaudaDropdown(
          $('alo-card-section:nth-child(4) aui-form-field:nth-child(1) select'),
          $$(
            'alo-card-section:nth-child(4) aui-form-field:nth-child(1) option',
          ),
        );
      case '代码仓库证书':
        return new AlaudaDropdown(
          $('alo-card-section:nth-child(4) aui-form-field:nth-child(6) select'),
          $$(
            'alo-card-section:nth-child(4) aui-form-field:nth-child(6) option',
          ),
        );
      default:
        return super.getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname: string = 'input') {
    switch (name) {
      case '镜像源证书':
        this.getElementByText(name, tagname).select(value);
        break;
      case 'Jenkins 服务':
        this.getElementByText(name, tagname).select(value);
        break;
      case '代码仓库证书':
        this.getElementByText(name, tagname).select(value);
        break;
      default:
        const inputbox = this.getElementByText(name, tagname);
        CommonPage.waitElementPresent(inputbox);
        inputbox.clear();
        inputbox.sendKeys(value);
        browser.sleep(100);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        this.enterValue(key, data[key]);
      }
    }
  }
}
