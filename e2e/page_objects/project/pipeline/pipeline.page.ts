/**
 * Created by zhangjiao on 2018/3/28.
 */

import { by, element } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaTabItem } from '../../../element_objects/alauda.tabitem';
import { AlaudaTable } from '../../../element_objects/alauda.table';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { AlaudaYamlEditor } from '../../../element_objects/alauda.ymaleditor';
import { PageBase } from '../../page.base';

import { PipelineCreatePage } from './create.page';

export class PipelinePage extends PageBase {
  get createPage() {
    return new PipelineCreatePage();
  }
  navigationButton() {
    return this.clickLeftNavByText('流水线');
  }

  createPipiline_byscript_first(testData, projectName) {
    this.getButtonByText('创建流水线').click();
    this.listPage_createMode_card(projectName).checkButtonIsPresent();
    this.listPage_createMode_card(projectName).click();

    this.createPage_nextButton.checkButtonIsPresent();

    this.createPage.fillForm(testData);
    this.createPage_nextButton.click();
  }

  get createPipeline_select() {
    return new AlaudaButton(by.css('.select .select-button'));
  }
  createPipeline_sourceType(typename) {
    return new AlaudaButton(
      by.xpath(
        '//aui-radio-group[@name="sourceType"]//span[contains(text(), "' +
          typename +
          '")]',
      ),
    );
  }
  get createPipeline_repourl_input() {
    return new AlaudaInputbox(by.name('repo'));
  }
  get createPipeline_secret_dropdown() {
    return new AlaudaDropdown(by.name('secret'), by.css('.aui-option'));
  }
  get createPipeline_addSecretButton() {
    return this.getButtonByText('添加凭据');
  }
  createPipeline_byscript_input(code_address, jenkins_name_api) {
    this.createPipeline_select.click();
    this.createPipeline_sourceType('输入').click();
    this.createPipeline_addSecretButton.checkButtonIsPresent();
    this.createPipeline_repourl_input.input(code_address);
    this.createPipeline_secret_dropdown.select_item(jenkins_name_api);
    this.getButtonByText('确定').click();
  }
  createPipiline_byscript_second(testData) {
    this.createPage.fillForm(testData);
    // this.createPage_confirmButton.click(); // 选择代码仓库dialog中点击确认按钮
    this.createPage_nextButton.click();
  }
  get createPipeline_location() {
    return new AlaudaText(
      by.xpath(
        '//div[@class="aui-radio-button aui-radio-button--medium isChecked isPlain"]/label/span',
      ),
    );
  }

  // -----------begin-----------pipeline List page
  get listPage_createPipeline_Button() {
    return this.getButtonByText('创建流水线');
  }

  get listPage_nameFilter_input() {
    return new AlaudaInputbox(by.css('input[placeholder="按名称搜索"]'));
  }

  listPage_pipelineName(projectName, pipelineName) {
    return new AlaudaList(
      by.xpath(
        '//a[@href="#/workspace/' +
          projectName +
          '/pipelines/' +
          pipelineName +
          '"]',
      ),
    );
  }

  listPage_historyView(projectName, pipelineName) {
    return new AlaudaButton(
      by.xpath(
        '//a[@href="#/workspace/' +
          projectName +
          '/pipelines/' +
          pipelineName +
          '"]/../..//aui-icon[@class="alo-history-preview__overview"]',
      ),
    );
  }

  logView_dialog_header(pipelineName) {
    return new AlaudaText(
      by.xpath(
        '//div[@class="mat-dialog-header"]//span[contains(text(), "' +
          pipelineName +
          '")]',
      ),
    );
  }
  get logView_dialog_cancel() {
    return new AlaudaButton(by.css('.mat-dialog-header .mat-icon'));
  }
  listPage_pipelineOperate(projectName, pipelineName, operateName) {
    return new AlaudaList(
      by.xpath(
        '//a[@href="#/workspace/' +
          projectName +
          '/pipelines/' +
          pipelineName +
          '"]',
      ),
      by.xpath(
        '//a[@href="#/workspace/' +
          projectName +
          '/pipelines/' +
          pipelineName +
          '"]/../..//alo-menu-trigger',
      ),
      by.cssContainingText('.aui-menu-item', operateName),
      by.xpath('//button[@aui-button="error"]'),
    );
  }

  listPage_createMode_card(projectName) {
    return new AlaudaButton(
      by.xpath(
        '//*[@href="#/workspace/' +
          projectName +
          '/pipelines/create?method=jenkinsfile"]',
      ),
    );
  }

  listPage_confirmdelete_button() {
    return this.getButtonByText('确定');
  }

  // -----------end-----------pipeline List page

  // -----------begin-----------pipeline detail page
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.alo-card__container .field'),
      '.base-header',
      'label',
      'span',
    );
  }
  get detailPage_pipeline_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .field'),
      '.base-header',
      'label',
      'span',
    );
  }

  get detailPage_Content_trigger() {
    return new AlaudaButton(
      by.xpath(
        '//label[contains(text(), "触发器")]/..//*[@class="aui-icon basic-time"]',
      ),
    );
  }
  detailPage_TabItem(tabName) {
    return new AlaudaTabItem(
      by.xpath('//*[@class="tabs"]/li/a'),
      by.xpath('//*[@class="tab-label active"]/a'),
      by.xpath('//*[@class="tabs"]/li/a[contains(text(), "' + tabName + '")]'),
    );
  }

  detailPage_executeHistory_id(historyId) {
    return new AlaudaList(
      by.xpath('//a[contains(text(), "' + historyId + '")]'),
    );
  }
  get detailPage_executeHistory_getid() {
    return element.all(by.css('.aui-table__row .aui-table__cell')).get(1);
  }

  detailPage_executeHistory_operate(historyId, operateName) {
    return new AlaudaList(
      by.xpath('//a[contains(text(), "' + historyId + '")]'),
      by.xpath(
        '//a[contains(text(), "' +
          historyId +
          '")]/../..//a[contains(@class, "alo-menu-button")]',
      ),
      by.xpath('//button[contains(text(), "' + operateName + '")]'),
      by.xpath('//button[@aui-button="error"]'),
    );
  }

  get detailPage_executeHistory_Table() {
    return new AlaudaTable(
      'aui-table-header-cell',
      'aui-table-cell',
      'aui-table-row',
    );
  }
  detailPage_historyTable_operate(historyId, operateName) {
    return new AlaudaList(
      by.xpath('//a[contains(text(), "' + historyId + '")]'),
      by.xpath(
        '//a[contains(text(), "' + historyId + '")]/../../..//alo-menu-trigger',
      ),
      by.xpath('//button[contains(text(), "' + operateName + '")]'),
      by.xpath('//button[@aui-button="error"]'),
    );
  }

  get detailPage_historyTable_logview() {
    return new AlaudaButton(
      by.xpath('//aui-icon[@class="history-name__overview"]'),
    );
  }

  get detailPage_operate_all() {
    return this.getButtonByText('操作');
  }
  detailPage_operate_button(operateName) {
    return this.getButtonByText(operateName);
  }
  get detailPage_PipelineTab_jenkins() {
    return new AlaudaText(
      by.xpath('//div[contains(text(), "JENKINSFILE (只读) ")]'),
    );
  }
  // -----------end-----------pipeline detail page

  // -----------begin----------pipeline create page

  get createPage_fistStep_input() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '.base-header',
      'label',
      'input',
    );
  }

  get createPage_nextButton() {
    return this.getButtonByText('下一步');
  }
  get createPage_createButton() {
    return new AlaudaButton(by.xpath('//button[@aui-button="primary"]'));
  }
  get createPage_cancelButton() {
    return this.getButtonByText('取消创建');
  }

  get createPage_yaml_textarea() {
    return new AlaudaYamlEditor(
      by.css('.aui-monaco-editor-container'),
      by.xpath(
        '//following::div[contains(@class,"aui-code-editor-toolbar__control-button")]',
      ),
    );
  }

  // -----------begin-----------pipeline update page
  get updatePage_Content() {
    return new AlaudaBasicInfo(
      by.css('.alo-card__container .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper',
      '.aui-form-item__content',
    );
  }
  get updatePage_inputbox() {
    return new AlaudaBasicInfo(
      by.css('.alo-card__container .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper',
      '.aui-form-item__content input',
    );
  }

  updatePage_trigger(triggerName) {
    return new AlaudaButton(
      by.xpath(
        '//div[@class="title" and contains(text(), "' +
          triggerName +
          '")]/../..//div/aui-checkbox',
      ),
    );
  }
  get updatePage_trigger_inputbox() {
    return new AlaudaInputbox(by.css('input[placeholder="例如：H * * * *"]'));
  }
  get updatePage_updateButton() {
    return this.getButtonByText('更新');
  }
  get updatePage_cancelButton() {
    return this.getButtonByText('取消');
  }
  // -----------end-----------pipeline update page
}
