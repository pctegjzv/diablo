/**
 * Created by zhangjiao on 2018/4/8.
 */
import { by } from 'protractor';

import { TableComponent } from '../../../component/table.component';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { PageBase } from '../../page.base';

export class MemberPage extends PageBase {
  // -----------begin-----------member list page
  navigationButton() {
    return this.clickLeftNavByText('成员', false);
  }

  get resourceTable() {
    return new TableComponent('alo-card');
  }

  get addMember_baseHeader_Text() {
    return new AlaudaText(by.css('alo-card__section-header ng-star-inserted'));
  }

  get addMember_email_inputbox() {
    return new AlaudaInputbox(by.name('email'));
  }

  get addMember_role_dropdown() {
    return new AlaudaDropdown(
      by.xpath('//select[@name="role"]'),
      by.tagName('option'),
    );
  }

  get addMember_add_button() {
    return this.getButtonByText('添加成员');
  }

  listPage_memberName(memberEmail) {
    return new AlaudaList(
      by.xpath('//aui-table-cell[contains(text(), "' + memberEmail + '")]'),
    );
  }
  listPage_memberOperate(memberEmail) {
    return new AlaudaList(
      by.xpath('//aui-table-cell[contains(text(), "' + memberEmail + '")]'),
      null,
      by.xpath(
        '//aui-table-cell[contains(text(), "' +
          memberEmail +
          '")]/..//a[contains(@class, "remove")]',
      ),
      by.xpath('//button[@aui-button="error"]'),
    );
  }
  // -----------end-----------member list page
}
