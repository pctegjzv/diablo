/**
 * Created by zhangjiao on 2018/4/16.
 */

import {
  $,
  $$,
  ElementFinder,
  browser,
  by,
  element,
  promise,
} from 'protractor';

import { TableComponent } from '../component/table.component';
import { AlaudaBreadCrumb } from '../element_objects/alauda.breadcrumb';
import { AlaudaButton } from '../element_objects/alauda.button';
import { AlaudaConfirmDialog } from '../element_objects/alauda.confirmdialog';
import { AlaudaDropdown } from '../element_objects/alauda.dropdown';
import { AlaudaElement } from '../element_objects/alauda.element';
import { AlaudaToast } from '../element_objects/alauda.toast';
import { AuiDialog } from '../element_objects_new/alauda.aui_dialog';
import { CommonMethod } from '../utility/common.method';
import { CommonPage } from '../utility/common.page';

export class PageBase {
  /**
   * 面包屑下边的进度条
   */
  get progressbar(): ElementFinder {
    return $('div[class="global-loader loading"]');
  }

  /**
   * progressbar 出现后，等待消失
   */
  waitProgressBarNotPresent(timeout = 60000): promise.Promise<boolean> {
    return this.progressbar.isPresent().then(isPresent => {
      if (isPresent) {
        return this.waitElementNotPresent(
          this.progressbar,
          'progressbar 没消失',
          timeout,
        );
      }
    });
  }
  /**
   * 获得新窗口的
   * @param index 窗口的index, 从 0 开始
   */
  switchWindow(index: number) {
    browser.getAllWindowHandles().then(handles => {
      const newWindowHandle = handles[index];
      browser
        .switchTo()
        .window(newWindowHandle)
        .then(() => {
          browser.getCurrentUrl().then(url => {
            console.log('switch to new Window: ' + url);
          });
        });
    });
  }

  /**
   * 等待页面元素出现
   * @param elem 页面元素
   * @param errorMessage 错误提示信息
   * @param timeout 超时时间
   */
  waitElementPresent(
    elem: ElementFinder,
    errorMessage: string,
    timeout = 20000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementPresent(elem, timeout).then(isPresent => {
      if (!isPresent) {
        console.log(errorMessage);
      }
      return isPresent;
    });
  }

  /**
   * 等待页面元素不出现
   * @param elem 页面元素
   * @param errorMessage 错误提示
   * @param timeout 超时时间
   */
  waitElementNotPresent(
    elem: ElementFinder,
    errorMessage: string,
    timeout = 20000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementNotPresent(elem, timeout).then(
      isNotPresent => {
        if (!isNotPresent) {
          console.log(errorMessage);
        }
        return isNotPresent;
      },
    );
  }

  /**
   * 等待页面元素不显示
   * @param elem 页面元素
   * @param errorMessage 错误提示
   * @param timeout 超时时间
   */
  waitElementNotDisplay(
    elem: ElementFinder,
    errorMessage: string,
    timeout: number = 20000,
  ) {
    return CommonPage.waitElementNotDisplay(elem, timeout).then(
      isNotDisplay => {
        if (!isNotDisplay) {
          console.log(errorMessage);
        }
        return isNotDisplay;
      },
    );
  }

  /**
   * 单击左导航
   */
  clickLeftNavByText(text, isCheckCrumb = true) {
    CommonPage.clickLeftNavByText(text);
    if (isCheckCrumb) {
      expect(this.breadcrumb.getText()).toContain(text.replace(/\s/g, ''));
    }
    this.waitProgressBarNotPresent();
  }

  /**
   * 页面右上角创建资源的按钮
   */
  get iconCreateResource(): AlaudaButton {
    return new AlaudaButton(
      by.css(`alo-global-actions div[class='create-resource']`),
    );
  }

  /**
   * 页面右上角用户图标的按钮
   */
  get accountMenu(): AlaudaDropdown {
    return new AlaudaDropdown(
      by.css('alo-global-actions div[class=user-actions]'),
      by.css('aui-menu-item button'),
    );
  }

  /**
   * 页面右上角用户文字显示
   */
  get account() {
    return element(by.css('div[class=user-actions] span'));
  }

  /**
   * toast 控件的message信息
   */
  get toast(): AlaudaToast {
    return new AlaudaToast();
  }

  /**
   * 面包屑
   */
  get breadcrumb(): AlaudaBreadCrumb {
    return new AlaudaBreadCrumb();
  }

  /**
   * 确认对话框
   */
  get confirmDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('mat-dialog-container'),
      'div[class=title]',
      'mat-dialog-actions button[aui-button=primary] span',
      `mat-dialog-actions button[aui-button='']`,
    );
  }

  get auiDialog() {
    return new AuiDialog($('aui-dialog .aui-dialog'));
  }

  /**
   * 单击页面Button
   */
  getButtonByText(buttonText, enabled = null) {
    if (enabled === null) {
      return new AlaudaButton(
        by.xpath(
          `//button/descendant-or-self::*[normalize-space(text()) ='${buttonText}']`,
        ),
      );
    } else {
      return new AlaudaButton(
        by.xpath(
          `//button/descendant-or-self::*[normalize-space(text()) ='${buttonText}']`,
        ),
        enabled,
      );
    }
  }

  get resourceTable() {
    return new TableComponent('aui-card');
  }

  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-field[class]',
      'aui-form-field[class] label[class*=aui-form-field]',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname: string = 'input'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname: string = 'input') {
    this.getElementByText(name, tagname).clear();
    this.getElementByText(name, tagname).sendKeys(value);
    browser.sleep(100);
  }

  private _goToChinese() {
    const icon = $('alo-global-actions .user-actions a');
    CommonPage.waitElementPresent(icon);
    icon.click();
    browser.sleep(500);
    const item = element(by.xpath('//aui-menu-item/button[text()="中文"]'));
    item.isPresent().then(isPresent => {
      if (isPresent) {
        item.click();
      } else {
        icon.click();
      }
    });
  }

  /**
   * 进入项目管理，项目列表页面
   */
  enterProjectDashboard() {
    browser.get(browser.baseUrl);
    browser.sleep(1000);
    this._goToChinese();
    this.waitProgressBarNotPresent();
  }

  /**
   * 进入平台管理页面
   */
  enterAdminDashboard() {
    browser.get(browser.baseUrl);
    this._goToChinese();
    this.waitProgressBarNotPresent();
    this.accountMenu.select('平台管理');
    this.waitProgressBarNotPresent();
  }

  /**
   * 生成测试数据
   * @param prefix 前缀
   */
  getTestData(prefix: string): string {
    return CommonMethod.random_generate_testData(prefix);
  }

  waitTableDisplay() {
    CommonPage.waitElementPresent($$('aui-table-row').first());
  }
}
