// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

// JUnit style XML reports
const { JUnitXmlReporter } = require('jasmine-reporters');
var Recycler = require('./utility/clear.testdata');
var retry = require('protractor-retry').retry;
var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: ['./**/**/**/*.e2e-spec.ts'],
  suites: {
    asftest: './**/**/**/*.asf-spec.ts',
    fulltest: ['./**/**/**/*.asf-spec.ts', './**/**/**/*.e2e-spec.ts'],
  },
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 2,
  },
  directConnect: true,
  baseUrl: process.env.BASE_URL || 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 1800000,
    print: function() {},
  },

  onPrepare() {
    printEnv();
    require('ts-node').register({
      project: 'e2e/tsconfig.json',
    });

    jasmine
      .getEnv()
      .addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    jasmine.getEnv().addReporter(
      new JUnitXmlReporter({
        savePath: './e2e-reports',
        consolidateAll: true,
      }),
    );

    jasmine.getEnv().addReporter(
      new HtmlReporter({
        baseDirectory: './e2e-reports',
        screenshotsSubfolder: 'images',
        takeScreenShotsOnlyForFailedSpecs: true,
        docTitle: 'Devops测试报告',
      }).getJasmine2Reporter(),
    );
    retry.onPrepare();

    browser.waitForAngularEnabled(false);

    console.log('**********************************');
    console.log('****    ' + process.env.CLUSTER_MASTER_IP + '      *****');
    console.log('**********************************');

    if (Recycler.TestData.isOpenOIDC()) {
      browser.get(
        `${browser.baseUrl}/?locale=zh&id_token=${process.env.USER_TOKEN}`,
      );
      browser.sleep(1000);
    } else {
      browser.get(`${browser.baseUrl}/?locale=zh`);
      browser.sleep(1000);
    }

    // browser.get(`${browser.baseUrl}/?locale=zh`).then(() => {
    //   browser.executeScriptWithDescription(
    //     `window.localStorage.setItem('alauda_locale', 'zh')`,
    //     'Config locale to ZH',
    //   );
    // });
  },

  onCleanUp(results) {
    retry.onCleanUp(results);
  },

  beforeLaunch() {
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch (err) {
      // console.warn(err);
    }
  },

  afterLaunch() {
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch (err) {
      // console.warn(err);
    }
    return retry.afterLaunch(2);
  },
};

function printEnv() {
  console.log(
    '\n\n*********************UI Auto must set Env*******************************',
  );
  console.log(`*          BASE_URL: ${browser.baseUrl}`);
  console.log(
    `*         TESTIMAGE: ${process.env.TESTIMAGE ||
      'index.alauda.cn/alaudaorg/qaimages:helloworld'}`,
  );
  console.log(`*        USER_TOKEN: ${process.env.USER_TOKEN || 'false'}`);
  console.log(
    `*      JENKINSURL: ${process.env.JENKINSURL ||
      'http://10.105.83.243:8080'}`,
  );

  console.log(
    `*  CLUSTER_MASTER_IP: ${process.env.CLUSTER_MASTER_IP || '127.0.0.1'}`,
  );
  console.log(
    '************************************************************************\n\n',
  );
}
