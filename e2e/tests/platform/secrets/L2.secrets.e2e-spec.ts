/**
 * Created by zhangjiao on 2018/3/1.
 */

import { browser } from 'protractor';

import { SecretsPage } from '../../../page_objects/platform/secrets/secrets.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';

describe('Secret L2级别UI自动化case', () => {
  const page = new SecretsPage();

  const project_name = page.getTestData('projectl2-insecret-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'Secret自动化测试中添加一个project！';
  const secret_name = 'secretl2-api';
  const secret_name_api1 = page.getTestData('secretl2-api1-');
  const secret_displayname_api1 = 'AUTOSECRET-API1';
  const secret_description_api1 = 'use yaml create secret!first,first,first!';
  const secret_username_api1 = 'zhangjiao';
  const secret_password_api1 = 'zhangjiao';

  const secret_name_api2 = page.getTestData('secretl2-api2-');
  const secret_username_api2 = 'zhangjiao2';
  const secret_password_api2 = 'zhangjiao2';

  const secret_createSecret_text = '创建 Secret';

  beforeAll(() => {
    this.testdataProject = CommonKubectl.createResource(
      'alauda.project.secret.yaml',
      {
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
        '${NAME}': secret_name_api1,
        '${SECRETNAME}': secret_name_api1,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username_api1).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password_api1).toString('base64'),
      },
      'L2.secrets.project.testdata',
    );

    this.testdataSecret1 = CommonKubectl.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name_api2,
        '${SECRETNAME}': secret_name_api2,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username_api2).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password_api2).toString('base64'),
      },
      'L2.secrets.secret.testdata1',
    );
    browser.refresh();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.navigationButton();
  });
  afterAll(() => {
    CommonKubectl.deleteResourceByYmal(this.testdataProject);
    CommonKubectl.deleteResourceByYmal(this.testdataSecret1);
  });

  /**
   * 在Secret列表页，按名称搜索Secret
   */
  it('L2:Ald-2236:Secret管理列表页-搜索Secret', () => {
    expect(page.listPage_nameFilter_input.checkInputboxIsPresent).toBeTruthy(); // 检查搜索框是否存在

    page.resourceTable.searchByResourceName('zjsecret_bucunzai', 0); // 在搜索框输入不存在的Secret名称
    expect(page.resourceTable.getRowCount()).toBe(0);
    page.resourceTable.searchByResourceName(secret_name_api1, 1); // 在搜索框输入要检索的Secret名称
    expect(
      page
        .listPage_SecretName(project_name, secret_name_api1)
        .checkNameInListPage(),
    ).toBeTruthy();
    expect(page.resourceTable.getRowCount()).toBe(1);

    page.resourceTable.searchByResourceName(secret_name, 2); // 在搜索框输入模糊查询的Secret名称
    expect(
      page
        .listPage_SecretName(project_name, secret_name_api1)
        .checkNameInListPage(),
    ).toBeTruthy();
    expect(
      page
        .listPage_SecretName(project_name, secret_name_api2)
        .checkNameInListPage(),
    ).toBeTruthy();
    expect(page.resourceTable.getRowCount()).toBe(2);
    // page.resourceTable.clearSearchBox(); //清空检索框
    page.resourceTable
      .getCell('显示名称', [secret_name_api1])
      .then(function(elem) {
        expect(elem.getText()).toBe(secret_name_api1);
      });
    page.resourceTable
      .getCell('显示名称', [secret_name_api2])
      .then(function(elem) {
        expect(elem.getText()).toBe(secret_name_api2);
      });

    page
      .listPage_SecretName(project_name, secret_name_api1)
      .clickNameInlistPage(); // 到Secret详情页检查通过API创建的数据是否正确
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('Secret 名称'),
      secret_name_api1,
    );
    expect(
      page.detailPage_Content.getElementByText('Secret 名称').getText(),
    ).toBe(secret_name_api1);
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      '',
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe('');
    expect(page.detailPage_Content.getElementByText('类型').getText()).toBe(
      '用户名/密码',
    );
    expect(page.detailPage_Content.getElementByText('用户名').getText()).toBe(
      secret_username_api1,
    );
  });

  /**
   * 创建Secret, 各项值的合法验证。
   */
  it('L2:Ald-2219:Secret管理-创建Secret-创建Secret时的合法验证', () => {
    expect(page.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.listPage_createButton.click(); // 列表页点击'创建Secret'到创建Secret页

    expect(page.createPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.createPage_createButton.click(); // 点击"创建"按钮 （此时什么都没有输入）
    CommonPage.waitElementTextChangeTo(
      page.createpage_input_content.getHintByText('Secret 名称'),
      '必填项',
    );
    expect(
      page.createpage_input_content.getHintByText('Secret 名称').getText(),
    ).toBe('必填项');
    expect(
      page.createpage_input_content.getHintByText('所属项目').getText(),
    ).toBe('必填项');
    expect(
      page.createpage_input_content.getHintByText('用户名').getText(),
    ).toBe('必填项');
    expect(page.createpage_input_content.getHintByText('密码').getText()).toBe(
      '必填项',
    );

    page.createPage_secretName_inputbox.input('自动—secretname'); // Secret名称输入不合法，中文。
    expect(
      page.createpage_input_content.getHintByText('Secret 名称').getText(),
    ).toBe('Secret 名称必须为小写字母，数字或"-", 以字母或数字结束。');

    page.createPage_secretName_inputbox.input('SECRETNAME'); // Secret名称输入不合法，大写字母。
    expect(
      page.createpage_input_content.getHintByText('Secret 名称').getText(),
    ).toBe('Secret 名称必须为小写字母，数字或"-", 以字母或数字结束。');

    page.createPage_secretName_inputbox.input('secret_name'); // Secret名称输入不合法，有下划线。
    expect(
      page.createpage_input_content.getHintByText('Secret 名称').getText(),
    ).toBe('Secret 名称必须为小写字母，数字或"-", 以字母或数字结束。');
  });

  /**
   * 创建Secret页，点击取消按钮跳转回Secret列表页。
   */
  it('L2:Ald-2234:Secret管理-创建Secret页-取消创建-返回列表页', () => {
    expect(page.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.listPage_createButton.click();
    expect(page.createPage_headerTitle_Text.checkTextIsPresent()).toBeTruthy();
    expect(page.createPage_headerTitle_Text.getText()).toBe('创建 Secret');
    expect(page.createPage_cancelButton.checkButtonIsPresent()).toBeTruthy();
    page.createPage_cancelButton.click(); // 点击取消按钮
    expect(page.listPage_createButton.checkButtonIsPresent).toBeTruthy(); // 点击取消操作后返回列表页，判断列表页的创建Secret按钮是否存在。
    expect(page.listPage_createButton.getButtonText()).toBe(
      secret_createSecret_text,
    );
  });

  /**
   * 更新Secret页，修改显示名称和描述，点击取消按钮，页面返回至Secret详情页显示名称和描述还是原来的值。
   */
  it('L2:Ald-2235:Secret管理-更新Secret页-取消更新-返回到Secret详情页', () => {
    expect(
      page
        .listPage_SecretName(project_name, secret_name_api1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_SecretName(project_name, secret_name_api1)
      .clickNameInlistPage(); // 在Secret列表页找到SecretName，点击到详情页。
    page.detailPage_updateButton.click(); // 点击更新按钮
    expect(
      page.createPage_displayName_inputbox.checkInputboxIsPresent(),
    ).toBeTruthy();
    expect(
      page.updateDialog_SecretName(secret_name_api1).checkTextIsPresent(),
    ).toBeTruthy(); // 判断更新页的SecretName

    page.createPage_displayName_inputbox.input(secret_displayname_api1);
    page.createPage_description_inputbox.input(secret_description_api1);
    page.createPage_cancelButton.click();

    // 取消更新后，验证取消成功，并且到Secret详情页。
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('Secret 名称'),
      secret_name_api1,
    );
    expect(
      page.detailPage_Content.getElementByText('Secret 名称').getText(),
    ).toBe(secret_name_api1);
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      '',
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe('');
    expect(page.detailPage_Content.getElementByText('类型').getText()).toBe(
      '用户名/密码',
    );
    expect(page.detailPage_Content.getElementByText('用户名').getText()).toBe(
      secret_username_api1,
    );
  });
});
