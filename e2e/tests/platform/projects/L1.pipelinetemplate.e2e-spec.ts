/**
 * Created by zhangjiao on 2018/3/12.
 */

import { browser } from 'protractor';

import { PipelineTemplate } from '../../../page_objects/platform/projects/pipelinetemplate.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { alauda_type_project } from '../../../utility/resource.type.k8s';

describe('项目-流水线模板 L1级别UI自动化case', () => {
  const page = new ProjectsPage();
  const templatePage = new PipelineTemplate();
  const project_name = page.getTestData('project-intemplate-');
  const project_description = 'create a project in template!';

  const coderepo_url =
    'https://github.com/jiaozhang1/devops-templates-auto.git';

  beforeAll(() => {
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_name,
        '${PROJECT_DESCRIPTION}': project_description,
      },
      alauda_type_project,
      null,
    );
    browser.refresh();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
  });

  it('L1:AldDevops-2559:项目详情-流水线模板-未配置模板仓库时的详情页', () => {
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(page.detailPage_TabItem('').getActiveText()).toBe('详情信息'); // 判断选中的是否是详情信息tab
    page.detailPage_TabItem('流水线模板').click(); // 点击详情页的“流水线模板”tab
    // 新创建的项目，还未配置模板仓库地址
    expect(
      templatePage.detailTab_configButton.checkButtonIsPresent(),
    ).toBeTruthy(); // 还未配置时，检查"配置模板仓库"按钮是否存在
    templatePage.detailTab_configButton.click();
    expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();
    templatePage.configDialog_cancelButton.click();
    expect(templatePage.configDialog_title.checkTextIsNotPresent()).toBeFalsy();
  });

  it('L1:AldDevops-2561:项目详情-流水线模板-配置模板仓库-通过输入代码仓库地址配置-配置成功', () => {
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('流水线模板').click(); // 点击详情页的“流水线模板”tab
    templatePage.detailTab_operateButton.click();
    templatePage.detailTab_configButton_oprate.click();
    expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();

    const testData = {
      方式: '输入',
      代码仓库地址: coderepo_url,
      代码分支: 'master',
    };
    templatePage.configDialog_inputbox(testData);

    // 配置结果检查
    expect(
      templatePage.detailTab_Content.getElementByText('模板仓库地址').getText(),
    ).toBe(coderepo_url);
    expect(
      templatePage.detailTab_Content.getElementByText('代码分支').getText(),
    ).toBe('master');
    templatePage.waitElementNotPresent(
      templatePage.detailTab_syncing.button,
      '同步配置一直在同步中...',
      60000,
    );
    //检查同步结果
    expect(templatePage.detailTab_successNum.getText()).toBe('5');
    expect(templatePage.detailTab_failureNum.getText()).toBe('0');
    expect(templatePage.detailTab_skipNum.getText()).toBe('0');
  });

  it('L1:AldDevops-2563:项目详情-流水线模板-同步模板仓库-同步结果检查', () => {
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('流水线模板').click(); // 点击详情页的“流水线模板”tab
    templatePage.detailTab_operateButton.click();
    templatePage.detailTab_syncButton.click();
    templatePage.waitElementNotPresent(
      templatePage.detailTab_syncing.button,
      '同步配置一直在同步中...',
    );
    //检查同步结果
    expect(templatePage.detailTab_successNum.getText()).toBe('0');
    expect(templatePage.detailTab_failureNum.getText()).toBe('0');
    expect(templatePage.detailTab_skipNum.getText()).toBe('5');
  });

  it('L1:AldDevops-2562:项目详情-流水线模板-查看模板同步结果', () => {
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('流水线模板').click(); // 点击详情页的“流水线模板”tab
    templatePage.detailTab_resultLink.click();
    expect(templatePage.resultDialog_title.checkTextIsPresent()).toBeTruthy();
    templatePage.resultDialog_close.click();
  });

  it('L1:AldDevops-2569:项目详情-流水线模板-点击自定义模板名称-打开模板详情dialog', () => {
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('流水线模板').click(); // 点击详情页的“流水线模板”tab
    templatePage.detailTab_definedButton.click(); // 点击自定义
    templatePage.detailTab_definedName('BuildImage').click();
    expect(templatePage.detailDialog_title.getText()).toBe('BuildImage');
    templatePage.detailDialog_close.click();
    expect(templatePage.detailDialog_title.checkTextIsNotPresent()).toBeFalsy();
  });
});
