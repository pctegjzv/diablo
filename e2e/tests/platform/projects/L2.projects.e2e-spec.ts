/**
 * Created by zhangjiao on 2018/3/12.
 */
import { browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';

describe('项目 L2级别UI自动化case', () => {
  const page = new ProjectsPage();
  const bindPage = new DevopsBindPage();
  this.prefix = 'projectl2-';
  this.displayPrefix = 'PROJECTL2-';

  const project_name1 = page.getTestData('projectl2-1');
  const project_displayname1 = page.getTestData('PROJECTL2-1');
  const project_description1 = 'the first project!';

  const project_name2 = page.getTestData('projectl2-2');
  const project_displayname2 = page.getTestData('PROJECTL2-2');
  const project_description2 = 'the second project!';

  const jenkins_name = page.getTestData('jenkinsl2-inproject-');
  const jenkins_host = 'http://jenkinsaaa.com';

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name1,
        '${PROJECT_NAME}': project_name1,
        '${PROJECT_DISPLAYNAME}': project_displayname1,
        '${PROJECT_DESCRIPTION}': project_description1,
      },
      'L2.projects.testdata',
    );
    this.testdata1 = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name2,
        '${PROJECT_NAME}': project_name2,
        '${PROJECT_DISPLAYNAME}': project_displayname2,
        '${PROJECT_DESCRIPTION}': project_description2,
      },
      'L2.projects.testdata1',
    );
    this.testdata2 = CommonKubectl.createResource(
      'alauda.jenkins.yaml',
      {
        '${NAME}': jenkins_name,
        '${JENKINS_NAME}': jenkins_name,
        '${JENKINS_HOST}': jenkins_host,
      },
      'L2.projects.testdata2',
    );
    browser.refresh();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    CommonKubectl.deleteResourceByYmal(this.testdata);
    CommonKubectl.deleteResourceByYmal(this.testdata1);
    CommonKubectl.deleteResourceByYmal(this.testdata2);
  });

  /**
   * 项目列表页，搜索项目。
   */
  xit('L2:Ald-2207:项目列表页-搜索项目', () => {
    expect(page.listPage_nameFilter_input.checkInputboxIsPresent).toBeTruthy(); // 检查搜索框是否存在
    page.resourceTable.searchByResourceName('zjprojectbucunzai', 0); // 在搜索框输入不存在的项目名称
    expect(page.resourceTable.getRowCount()).toBe(0);

    page.resourceTable.searchByResourceName(project_name1, 1); // 在搜索框输入存在的项目名称
    expect(
      page.listPage_projectName(project_name1).checkNameInListPage(),
    ).toBeTruthy();
    expect(page.resourceTable.getRowCount()).toBe(1);

    page.resourceTable.searchByResourceName(this.prefix, 2); // 在搜索框输入模糊查询的项目名称
    expect(
      page.listPage_projectName(project_name1).checkNameInListPage(),
    ).toBeTruthy();
    expect(
      page.listPage_projectName(project_name2).checkNameInListPage(),
    ).toBeTruthy();
    expect(page.resourceTable.getRowCount()).toBe(2);

    page.listPage_projectName(project_name2).clickNameInlistPage(); // 到project详情页检查通过API创建的数据是否正确
    expect(
      page.detailPage_Content.checkInputboxIsPresent('项目名称'),
    ).toBeTruthy();
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name2,
    );
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      project_displayname2,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      project_description2,
    );
  });

  /**
   * 创建项目页，点击取消按钮跳转回项目列表页。
   */
  xit('L2:Ald-2203:创建项目-取消项目创建-返回列表页', () => {
    expect(page.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.listPage_createButton.click(); // 点击创建项目按钮
    expect(page.createPage_inputbox.getTitleText()).toBe('创建项目'); // 检查是否到创建项目页

    expect(page.createPage_cancelButton.checkButtonIsPresent()).toBeTruthy();
    page.createPage_cancelButton.click(); // 点击取消按钮
    expect(page.createPage_cancelButton.checkButtonIsNotPresent()).toBeFalsy(); // 点击取消操作后返回列表页，判断列表页的创建项目按钮是否存在。
  });

  /**
   * 创建项目，对每一个输入框进行校验。最后只输入项目名称点击提交。 显示名称记入的是项目名称。
   */
  it('L2:Ald-2202:创建项目-表单的合法验证', () => {
    expect(page.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.listPage_createButton.click(); // 点击创建项目按钮

    expect(page.createPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.createPage_createButton.click(); // 第一次点击创建按钮 (什么都不输入时点击"创建"按钮，需要提示项目名称不可以为空)
    expect(page.createPage_inputbox.getHintByText('项目名称').getText()).toBe(
      '必填项',
    );

    page.createPage_inputbox.inputByText('项目名称', '自动—项目名称'); // 项目名称输入不合法，中文。
    expect(page.createPage_inputbox.getHintByText('项目名称').getText()).toBe(
      '项目名称必须为小写字母，数字或"-", 以字母或数字结束。',
    );

    page.createPage_inputbox.inputByText('项目名称', 'DAXIE-projectname'); // 项目名称输入不合法，大写字母。
    expect(page.createPage_inputbox.getHintByText('项目名称').getText()).toBe(
      '项目名称必须为小写字母，数字或"-", 以字母或数字结束。',
    );

    // page.createPage_projectName_inputbox.input(project_name_api1); //项目名称输入输入已存在的项目名称。
    // expect(page.createPage_projectName_Error.getText()).toBe("项目名称已存在。");
  });

  /**
   * 更新项目，输入更新的内容后点击取消按钮，更新取消页面值还是原来的值。
   */
  it('L2:Ald-2206:更新项目-取消项目更新-返回到项目详情页', () => {
    expect(
      page.listPage_projectName(project_name1).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name1).clickNameInlistPage(); // 打开project详情页
    expect(page.detailPage_updateButton.checkButtonIsPresent()).toBeTruthy();
    page.detailPage_updateButton.click(); // 点击更新按钮
    expect(page.updatePage_header_text.checkTextIsPresent()).toBeTruthy();
    expect(page.updatePage_projectName_Text.getTextAll(0)).toBe(project_name1); // 判断更新页的ProjectName
    page.createPage_cancelButton.click();

    // 取消更新后，验证取消成功，并且到项目详情页。
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('项目名称'),
      project_name1,
    );
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name1,
    );
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      project_displayname1,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      project_description1,
    );
  });

  /**
   * 在"绑定Jenkins服务"对话框中点击取消按钮，取消绑定，页面返回到服务tab的列表页。
   */
  it('L2:AAldDevops-2212:项目详情页-Devops工具链-绑定持续集成(jenkins服务)-取消绑定', () => {
    expect(
      page.listPage_projectName(project_name1).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name1).clickNameInlistPage();
    expect(
      page.detailPage_Content.checkInputboxIsPresent('项目名称'),
    ).toBeTruthy();
    expect(
      page.detailPage_TabItem('DevOps 工具链').checkTabClickIsPresent(),
    ).toBeTruthy();
    page.detailPage_TabItem('DevOps 工具链').click();
    expect(page.detailPage_Bind_button.checkButtonIsPresent()).toBeTruthy();
    page.detailPage_Bind_button.click(); // 点击绑定按钮
    expect(page.bindJenkins_inputcontent.getTitleText()).toContain('绑定');

    bindPage.bindDialog_tools_type('持续集成').click();
    expect(
      bindPage.bindDialog_tools_card(jenkins_name).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(jenkins_name).click();

    expect(
      page.bindJenkinsDialog_cancelButton.checkButtonIsPresent(),
    ).toBeTruthy();
    page.bindJenkinsDialog_cancelButton.click(); // 点击取消按钮
    expect(
      page.bindJenkinsDialog_cancelButton.checkButtonIsNotPresent(),
    ).toBeFalsy();
  });

  /**
   * 在"添加Secret"页，取消添加，页面返回到资源tab的列表页。
   */
  it('L2:项目详情页-资源tab-添加凭据-取消添加凭据', () => {
    expect(
      page.listPage_projectName(project_name1).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name1).clickNameInlistPage();
    page.detailPage_TabItem('资源').click(); // 点击详情页的资源标签
    expect(
      page.detailPage_addSecret_button.checkButtonIsPresent(),
    ).toBeTruthy();
    page.detailPage_addSecret_button.click(); // 点击"添加凭据"按钮
    // 打开添加凭据dialog
    expect(
      page.addSecretDialog_Project_Text(project_name1).checkTextIsPresent(),
    ).toBeTruthy(); // 检查所属项目是否是当前添加的项目
    expect(page.addSecretDialog_inputContent.getTitleText()).toContain(
      '添加凭据',
    );
    page.addSecretDialog_cancelButton.click(); // 点击取消按钮
    expect(
      page.addSecretDialog_cancelButton.checkButtonIsNotPresent(),
    ).toBeFalsy();
  });
});
