/**
 * Created by zhangjiao on 2018/3/12.
 */

import { $, browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_type_jenkins,
  alauda_type_project,
  alauda_type_secret,
} from '../../../utility/resource.type.k8s';

const getEleValue = (ele, elename) => {
  const queryscript =
    'return document.querySelector("' + ele + '[name=' + elename + ']").value';
  return browser.executeScript(queryscript);
};

describe('项目 L1级别UI自动化case', () => {
  const page = new ProjectsPage();
  const bindPage = new DevopsBindPage();
  const project_name1 = page.getTestData('projectl1-1');
  const project_displayname1 = project_name1.toLocaleUpperCase();
  const project_description1 = 'auto_project the first description!!!';
  const project_name2 = page.getTestData('projectl1-2');
  const project_displayname2 = project_name2.toLocaleUpperCase();
  const project_description2 = 'auto_project the second description!!!';

  const jenkins_name_api = page.getTestData('jenkinsl1-inproject-');
  const jenkins_host_api = 'http://jenkinsaaa.com';
  const jenkins_description = 'At project detail page, bind Jenkins service';

  const secret_name_api = page.getTestData('secretapi-');
  const secret_name = page.getTestData('secret-');
  const secret_displayname = 'AUTOSECRET';
  const secret_description = 'auto add secret description!!! ';
  const secret_username = 'zhangjiao';
  const secret_password = 'zhangjiao';

  const person_email = 'zhangjiao521@alauda.io';
  const person_email1 = 'zhangjiao555@alauda.io';

  beforeAll(() => {
    browser.refresh();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(jenkins_name_api, alauda_type_jenkins, null);
    CommonApi.deleteResource(project_name1, alauda_type_project, null);
    CommonApi.deleteResource(project_name2, alauda_type_project, null);
  });

  /**
   * 创建项目，项目名称、显示名称、描述 输入合法点击提交，创建成功。
   */
  it('L1:Ald-2201: 创建项目-项目创建成功', () => {
    expect(page.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.listPage_createButton.click(); // 点击创建项目按钮
    expect(page.createPage_inputbox.getTitleText()).toBe('创建项目'); // 检查是否到创建项目页
    page.createPage_inputbox.inputByText('项目名称', project_name1);
    page.createPage_inputbox.inputByText('显示名称', project_displayname1);
    page.createPage_description.input(project_description1);
    page.createPage_createButton.click();

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('项目名称'),
      project_name1,
    );
    expect(
      page.detailPage_TabItem('详情信息').checkTabActiveIsPresent(),
    ).toBeTruthy();
    expect(page.detailPage_TabItem('详情信息').getActiveText()).toBe(
      '详情信息',
    ); // 判断选中的是否是详情信息tab
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name1,
    );
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      project_displayname1,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      project_description1,
    );
  });

  it('L1:Ald-2273: 创建项目-只输入项目名称-项目创建成功', () => {
    expect(page.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.listPage_createButton.click(); // 点击创建项目按钮
    page.createPage_inputbox.inputByText('项目名称', project_name2);
    page.createPage_createButton.click();

    // 创建后跳转到详情页，验证是否创建成功
    expect(
      page.detailPage_Content.checkInputboxIsPresent('项目名称'),
    ).toBeTruthy();
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('项目名称'),
      project_name2,
    );
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name2,
    );
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      '',
    ); // 显示名称显示为空
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(''); // 描述显示为空
  });

  /**
   * 在项目列表页，点击项目名称到项目详情页
   */
  it('L1:Ald-2277:项目列表页-点击项目名称到项目详情页', () => {
    page.resourceTable.clickResourceNameByRow([project_name1]); // 在列表页点击项目名称到项目详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('项目名称'),
      project_name1,
    );
    expect(page.detailPage_updateButton.checkButtonIsPresent()).toBeTruthy(); // 判断"更新"按钮是否存在
    expect(page.detailPage_deleteButton.checkButtonIsPresent()).toBeTruthy(); // 判断"删除项目"按钮是否存在

    expect(page.detailPage_TabItem('').getActiveText()).toBe('详情信息'); // 判断选中的是否是详情信息tab
    expect(
      page
        .detailPage_TabItem('')
        .getTabItem()
        .get(0)
        .getText(),
    ).toBe('详情信息');
    expect(
      page
        .detailPage_TabItem('')
        .getTabItem()
        .get(1)
        .getText(),
    ).toBe('DevOps 工具链');
    expect(
      page
        .detailPage_TabItem('')
        .getTabItem()
        .get(2)
        .getText(),
    ).toBe('流水线模板');
    expect(
      page
        .detailPage_TabItem('')
        .getTabItem()
        .get(3)
        .getText(),
    ).toBe('资源');
    expect(
      page
        .detailPage_TabItem('')
        .getTabItem()
        .get(4)
        .getText(),
    ).toBe('微服务环境');
    expect(
      page
        .detailPage_TabItem('')
        .getTabItem()
        .get(5)
        .getText(),
    ).toBe('项目成员');
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name1,
    );
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      project_displayname1,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      project_description1,
    );
  });

  /**
   * 更新项目，项目更新成功。
   */
  it('L1:Ald-2204:更新项目-项目更新成功', () => {
    // page.resourceTable.searchByResourceName(project_name2);
    page.resourceTable.clickResourceNameByRow([project_name2]); // 在列表页点击项目名称到项目详情页

    expect(page.detailPage_updateButton.checkButtonIsPresent()).toBeTruthy(); // 判断"更新"按钮是否存在
    page.detailPage_updateButton.click(); // 点击更新按钮
    expect(page.updatePage_header_text.checkTextIsPresent()).toBeTruthy();
    expect(page.updatePage_header_text.getText()).toContain('更新项目');
    expect(page.updatePage_projectName_Text.getText()).toBe(project_name2); // 判断更新页的ProjectName
    page.updatePage_displayName_Text.input(project_displayname2);
    page.updatePage_description_Text.input(project_description2);
    page.updatePage__updateButton.click();

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('显示名称'),
      project_displayname2,
    );
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('描述'),
      project_description2,
    );
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name2,
    );
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      project_displayname2,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      project_description2,
    );

    page.detailPage_updateButton.click(); // 再次点击更新按钮
    expect(page.updatePage_projectName_Text.getTextAll(0)).toBe(project_name2); // 判断更新页的ProjectName
    getEleValue('input', 'displayName').then(function(eleValue) {
      expect(eleValue).toBe(project_displayname2);
    });
    getEleValue('textarea', 'description').then(function(eleValue) {
      expect(eleValue).toBe(project_description2);
    });
    page.confirmDialog.clickCancel();
  });

  /**
   * 在项目详情页，四个tab页来回切换，保证数据正确。
   */
  it('L1:Ald-2208:项目详情页-tab页切换-数据正确', () => {
    page.resourceTable.clickResourceNameByRow([project_name1]); // 在列表页点击项目名称到项目详情页

    expect(page.detailPage_TabItem('').getActiveText()).toBe('详情信息'); // 判断选中的是否是详情信息tab
    page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的“DevOps工具链”tab
    expect(page.detailPage_Bind_button.checkButtonIsPresent).toBeTruthy(); // 检查"绑定"按钮是否存在

    page.detailPage_TabItem('资源').click(); // 点击详情页中资源tab
    expect(
      page.detailPage_addSecret_button.checkButtonIsPresent(),
    ).toBeTruthy(); // 检查"添加Secret"按钮是否存在

    page.detailPage_TabItem('项目成员').click(); // 点击详情页中项目成员tab
    expect(
      page.detailPage_addMember_button.checkButtonIsPresent(),
    ).toBeTruthy(); // 检查"添加成员"按钮是否存在

    page.detailPage_TabItem('详情信息').click(); // 点击详情页的详情信息tab
    expect(page.detailPage_updateButton.checkButtonIsPresent()).toBeTruthy(); // 检查是否有"更新"按钮
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name1,
    ); // 检查基本信息中项目名称是否正确

    page.detailPage_TabItem('项目成员').click(); // 点击详情页中项目成员tab
    expect(page.detailPage_addMember_button.checkButtonIsPresent).toBeTruthy(); // 检查"添加成员"按钮是否存在

    page.detailPage_TabItem('资源').click(); // 点击详情页中资源tab
    expect(
      page.detailPage_addSecret_button.checkButtonIsPresent(),
    ).toBeTruthy(); // 检查"添加Secret"按钮是否存在

    page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的“DevOps工具链”tab
    expect(page.detailPage_Bind_button.checkButtonIsPresent()).toBeTruthy(); // 检查"绑定jenkins服务按钮"是否存在

    page.detailPage_TabItem('详情信息').click(); // 点击详情页的详情信息tab
    expect(page.detailPage_updateButton.checkButtonIsPresent()).toBeTruthy(); // 检查是否有"更新"按钮
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name1,
    ); // 检查基本信息中项目名称是否正确
  });

  /**
   * 在项目详情页，点击"Devops 工具链"tab，点击"绑定"按钮。
   * 绑定Jenkins服务，绑定成功。
   */
  it('L1:AldDevops-2209:项目详情页-Devops 工具链-绑定持续集成(jenkins服务)-成功绑定', () => {
    // 通过YAML创建Jenkins和Secret
    CommonApi.createResource(
      'alauda.jenkins.yaml',
      {
        '${NAME}': jenkins_name_api,
        '${JENKINS_NAME}': jenkins_name_api,
        '${JENKINS_HOST}': jenkins_host_api,
      },
      alauda_type_jenkins,
      null,
    );

    CommonApi.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name_api,
        '${SECRETNAME}': secret_name_api,
        '${NAMESPACE}': project_name1,
        '${USERNAME}': Buffer.from(secret_username).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password).toString('base64'),
      },
      alauda_type_secret,
      project_name1,
    );
    page.resourceTable.clickResourceNameByRow([project_name1]); // 在列表页点击项目名称到项目详情页

    page.detailPage_TabItem('DevOps 工具链').click();
    expect(page.detailPage_Bind_button.checkButtonIsPresent()).toBeTruthy();
    page.detailPage_Bind_button.click(); // 点击绑定按钮
    expect(page.bindJenkins_inputcontent.getTitleText()).toContain('绑定');

    bindPage.bindDialog_tools_type('持续集成').click();
    expect(
      bindPage.bindDialog_tools_card(jenkins_name_api).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(jenkins_name_api).click();
    // 检查是否到绑定页
    expect(bindPage.bindPage_header_text.getText()).toContain('绑定 Jenkins');
    const testData = {
      名称: jenkins_name_api,
      描述: jenkins_description,
      凭据: secret_name_api,
    };
    page.bindJenkins_inputbox(testData);
    // 绑定成功后跳转到项目详情页
    expect(page.detailPage_TabItem('').getActiveText()).toBe('详情信息'); // 判断选中的是否是详情信息tab
    expect(page.detailPage_Content.getElementByText('项目名称').getText()).toBe(
      project_name1,
    ); // 检查基本信息中项目名称是否正确
  });

  /**
   * 在项目详情页，点击资源tab，点击"添加凭据"按钮。
   * 添加Secret，添加成功。
   */
  it('L1:Ald-2214:项目详情页-资源tab-添加凭据-添加成功', () => {
    page.resourceTable.clickResourceNameByRow([project_name1]); // 在列表页点击项目名称到项目详情页

    page.detailPage_TabItem('资源').click(); // 点击详情页的资源标签
    expect(
      page.detailPage_addSecret_button.checkButtonIsPresent(),
    ).toBeTruthy();
    page.detailPage_addSecret_button.click(); // 点击"添加凭据"按钮

    // 打开添加凭据dialog
    expect(
      page.addSecretDialog_Project_Text(project_name1).checkTextIsPresent(),
    ).toBeTruthy(); // 检查所属项目是否是当前添加的项目
    const testData = {
      'Secret 名称': secret_name,
      显示名称: secret_displayname,
      描述: secret_description,
      用户名: secret_username,
      密码: secret_password,
    };
    page.addSecretDialog_inputbox(testData);
    const temp = $(`a[href="#/admin/secrets/${project_name1}/${secret_name}"]`);
    page.waitElementPresent(temp, `新添加的${secret_name}没有显示在表格中`);
    expect(temp.isPresent()).toBeTruthy();
  });

  /**
   * 在项目详情页，点击项目成员tab
   */
  xit('L1:Ald-2275:项目详情页-成员tab-添加成员', () => {
    page.resourceTable.clickResourceNameByRow([project_name1]); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('项目成员').click(); // 点击详情页的项目成员标签

    expect(
      page.addMemberTab_email_inputbox.checkInputboxIsPresent(),
    ).toBeTruthy();
    browser.sleep(3000);
    page.addMemberTab_email_inputbox.input(person_email);
    page.addMemberTab_role_dropdown.select('项目成员');
    page.addMemberTab_add_button.click(); // 点击"添加成员"按钮
    expect(
      page.addMemberTab_memberName(person_email).checkNameInListPage(),
    ).toBeTruthy();

    page.addMemberTab_email_inputbox.input(person_email1);
    page.addMemberTab_role_dropdown.select('项目管理员');
    page.addMemberTab_add_button.click(); // 点击"添加成员"按钮
    expect(
      page.addMemberTab_memberName(person_email1).checkNameInListPage(),
    ).toBeTruthy();
    expect(page.detailPage.memberPage.resourceTable.getRowCount()).toBe(2);
    page.detailPage.memberPage.resourceTable
      .getCell('邮箱', [person_email])
      .then(elem => {
        expect(elem.getText()).toBe(person_email);
      });
    page.detailPage.memberPage.resourceTable
      .getCell('邮箱', [person_email1])
      .then(elem => {
        expect(elem.getText()).toBe(person_email1);
      });
    // 返回到项目列表页，检查项目列表页是否显示该项目的项目管理员
    page.navigationButton();
    // page.resourceTable.searchByResourceName(project_name1);
    CommonPage.waitElementTextChangeTo(
      page.listPage_projectManager(project_name1),
      person_email1,
    );
    expect(page.listPage_projectManager(project_name1).getText()).toBe(
      person_email1,
    );
  });

  xit('L1:Ald-2316:项目详情页-成员tab-删除已经添加的成员', () => {
    page.resourceTable.clickResourceNameByRow([project_name1]); // 在列表页点击项目名称到项目详情页
    expect(
      page.detailPage_TabItem('项目成员').checkTabClickIsPresent(),
    ).toBeTruthy();
    page.detailPage_TabItem('项目成员').click();
    expect(
      page.addMemberTab_memberName(person_email).checkNameInListPage(),
    ).toBeTruthy();
    page.addMemberTab_memberOperate(person_email).clickOperateButton();
    page.addMemberTab_memberOperate(person_email).clickOkButton();
    expect(
      page.addMemberTab_memberName(person_email).checkNameNotInListPage(),
    ).toBeFalsy();
  });

  it('L1:Ald-2221:项目详情页-删除项目-删除成功', () => {
    const rowkey = [project_name2];
    page.resourceTable.getCell('名称', rowkey).then(item => {
      CommonPage.waitElementPresent(item);
      page.resourceTable.clickResourceNameByRow(rowkey);

      page.detailPage_deleteButton.click(); // 点击"删除项目"按钮
      expect(
        page.detailPage_deleteButton_ok.checkButtonIsPresent(),
      ).toBeTruthy();
      page.detailPage_deleteButton_ok.click(); // 点击"删除"确认删除
      expect(
        page.listPage_nameFilter_input.checkInputboxIsPresent(),
      ).toBeTruthy();
      // 删除后在列表搜索这个项目看是不是已经删除掉
      // page.resourceTable.searchByResourceName(project_name2, 0); // 在搜索框输入不存在的项目名称
      // expect(page.resourceTable.getRowCount()).toBe(0);
    });
  });
});
