/**
 * Created by zhangjiao on 2018/8/20.
 */

import { browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/platform/devops-tools/devopstools.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';

import {
  alauda_image_registry,
  alauda_type_project,
} from '../../../utility/resource.type.k8s';

describe('Docker Registry L2级别UI自动化case', () => {
  const bindPage = new DevopsBindPage();
  const toolsPage = new DevopsToolsPage();
  const projectPage = new ProjectsPage();

  const registry_name = bindPage.getTestData('registryl2-error-');
  const registry_host = 'https://registry.com';

  const project_name = toolsPage.getTestData('projectl2-inregistry-');
  const project_displayname = toolsPage.getTestData('PROJECT-INCODE-API1-');
  const project_desc = 'the first project1!';
  beforeAll(() => {
    this.testdata1 = CommonKubectl.createResource(
      'alauda.docker-registry.yaml',
      {
        '${NAME}': registry_name,
        '${REGISTRY_NAME}': registry_name,
        '${REGISTRY_HOST}': registry_host,
      },
      'L2.dockerregistry.testdata1',
    );
    this.testdata2 = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_desc,
      },
      'L2.dockerregistry.testdata2',
    );
    browser.refresh();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {});

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(registry_name, alauda_image_registry, null);
  });

  it('L2:AldDevops-2578:集成-制品仓库-Docker Registry-对表单进行验证', () => {
    toolsPage.navigationButton();
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('制品仓库').click();
    toolsPage.createDialog_tools_card('Docker Registry').click();

    toolsPage.createDialog_createButton.click(); // 点击集成按钮
    expect(
      toolsPage.createDialog_inputbox.getHintByText('API 地址').getText(),
    ).toBe('必填项');
    toolsPage.createDialog_inputbox.inputByText('API 地址', 'aaaaaa'); // API地址不合法。
    expect(
      toolsPage.createDialog_inputbox.getHintByText('API 地址').getText(),
    ).toBe('格式错误');
    toolsPage.createDialog_cancelButton.click(); // 点击取消按钮
  });

  it('L2:AldDevops-2587:项目详情-DevOps工具链-绑定Docker Registry-绑定账号表单验证', () => {
    projectPage.navigationButton();
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('制品仓库').click();
    expect(
      bindPage.bindDialog_tools_card(registry_name).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(registry_name).click();

    // 表单验证
    // 点击[绑定账号]按钮时，必填项提示
    bindPage.bind_firstStep_bindButton.click();
    CommonPage.waitElementTextChangeTo(
      bindPage.bind_firstStep_inputbox.getHintByText('名称'),
      '必填项',
    );
    expect(
      bindPage.bind_firstStep_inputbox.getHintByText('名称').getText(),
    ).toBe('必填项');

    bindPage.bind_firstStep_inputbox.inputByText('名称', '汉字'); // Secret名称输入不合法，中文。
    expect(
      bindPage.bind_firstStep_inputbox.getHintByText('名称').getText(),
    ).toBe('名称必须为小写字母，数字或"-", 以字母或数字结束。');

    bindPage.bind_firstStep_inputbox.inputByText('名称', 'DAXIE'); // Secret名称输入不合法，大写字母。
    expect(
      bindPage.bind_firstStep_inputbox.getHintByText('名称').getText(),
    ).toBe('名称必须为小写字母，数字或"-", 以字母或数字结束。');
  });

  it('L2:AldDevops-2588:集成-制品仓库-Docker Registry-取消集成', () => {
    toolsPage.navigationButton();
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('制品仓库').click();
    toolsPage.createDialog_tools_card('Docker Registry').click();

    toolsPage.createDialog_cancelButton.click(); // 点击取消按钮
    expect(
      toolsPage.createDialog_cancelButton.checkButtonIsNotPresent(),
    ).toBeFalsy();
  });
});
