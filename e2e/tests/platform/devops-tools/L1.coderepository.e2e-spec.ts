/**
 * Created by zhangjiao on 2018/7/19.
 */

import { $, browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/platform/devops-tools/devopstools.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';

import {
  alauda_coderepo_service,
  alauda_type_project,
} from '../../../utility/resource.type.k8s';

describe('CodeRepo L1级别UI自动化case', () => {
  const toolsPage = new DevopsToolsPage();
  const bindPage = new DevopsBindPage();
  const projectPage = new ProjectsPage();

  const gitlab_name1 = bindPage.getTestData('gitlabl1-1-');
  const gitlab_host1 = 'https://api.gitlab.com';

  const gitlab_name2 = bindPage.getTestData('gitlabl1-2-');
  const gitlab_host2 = 'https://api.gitlaberror.com';
  const gitlab_host_valid = 'https://api.gitlab.com';
  const gitlab_bind_name2 = bindPage.getTestData('gitlabbindl1-2-');
  const gitlab_bind_desc2 = 'auto bind gitlab service description22222!!!';

  const github_name1 = bindPage.getTestData('githubl1-');
  const github_host1 = 'https://api.github.com';
  //绑定代码仓库服务参数
  const github_bind_name1 = bindPage.getTestData('githubbindl1-1-');
  const github_bind_desc1 = 'auto bind coderepo service description1111!!!';
  const github_bind_desc1_new = 'new auto bind coderepo service description1!';
  const github_bind_name2 = bindPage.getTestData('githubbindl1-2-');
  const github_bind_desc2 = 'auto bind coderepo service description2222!!!';
  const github_bind_nameerror = bindPage.getTestData('githubbindl1-error-');
  const github_bind_descerror = 'auto bind coderepo service description1111!!!';

  const project_name = bindPage.getTestData('projectl1-incode-');
  const project_displayname = 'AUTOPROJECT-INCODE-L1';
  const project_desc = 'the first project!';

  const secret_name1 = 'autosecretl1-github1';
  const secret_username1 = 'jiaozhang1';
  const secret_password1 = 'ZhangJiao123';

  const secret_name2 = 'autosecretl1-github2';
  const secret_username2 = 'alaudabot';
  const secret_password2 = 'b31d3fee2cc48600d9137ad321a47633c5ce7c1b';

  const secret_name_error = 'autosecretl1-github-error';
  const secret_username_error = 'jiaozhang';
  const secret_password_error = 'yiersansiwu';

  beforeAll(() => {
    this.testdata1 = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_desc,
      },
      'L1.coderepository.testdata1',
    );
    browser.sleep(1000);
    this.testdata2 = CommonKubectl.createResource(
      'alauda.coderepo-github.yaml',
      {
        '${NAME}': github_name1,
        '${GITHUB_NAME}': github_name1,
        '${GITHUB_HOST}': github_host1,
        '${GITHUB_HTML}': github_host1,
      },
      'L1.coderepository.testdata2',
    );
    this.testdata3 = CommonKubectl.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name1,
        '${SECRETNAME}': secret_name1,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username1).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password1).toString('base64'),
      },
      'L1.coderepository.testdata3',
    );
    this.testdata4 = CommonKubectl.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name2,
        '${SECRETNAME}': secret_name2,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username2).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password2).toString('base64'),
      },
      'L1.coderepository.testdata4',
    );
    this.testdata5 = CommonKubectl.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name_error,
        '${SECRETNAME}': secret_name_error,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username_error).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password_error).toString('base64'),
      },
      'L1.coderepository.testdata5',
    );
    browser.refresh();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {
    toolsPage.enterAdminDashboard();
    toolsPage.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    browser.sleep(1000);
    CommonApi.deleteResource(gitlab_name1, alauda_coderepo_service, null);
    CommonApi.deleteResource(gitlab_name2, alauda_coderepo_service, null);
    CommonApi.deleteResource(github_name1, alauda_coderepo_service, null);
  });

  it('L1:AldDevops-2412:集成-代码仓库-企业Gitlab-输入正确的名称和API地址-创建成功', () => {
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('代码仓库').click();
    toolsPage.createDialog_tools_card('Gitlab 企业版').click();

    toolsPage.createDialog_inputbox.inputByText('名称', gitlab_name1);
    toolsPage.createDialog_inputbox.inputByText('API 地址', gitlab_host1);
    toolsPage.createDialog_createButton.click(); // 点击集成按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      gitlab_name1,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(gitlab_name1);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(gitlab_host1);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Gitlab 企业版');
  });

  it('L1:AldDevops-2494:集成-代码仓库-企业Gitlab-输入正确的名称和错误的API地址-创建成功', () => {
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('代码仓库').click();
    toolsPage.createDialog_tools_card('Gitlab 企业版').click();

    toolsPage.createDialog_inputbox.inputByText('名称', gitlab_name2);
    toolsPage.createDialog_inputbox.inputByText('API 地址', gitlab_host2); // 错误的api地址
    toolsPage.createDialog_createButton.click(); // 点击集成按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      gitlab_name2,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(gitlab_name2);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(gitlab_host2);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Gitlab 企业版');
    // 详情页有错误标示，有errormessage
    // browser.sleep(2000);
    // expect(toolsPage.detailPage_errorIcon.checkButtonIsPresent()).toBeTruthy();
    // 返回到Devops 工具链首页，该Gitlab卡片上有errormessage
  });

  it('L1:AldDevops-2522:更新-代码仓库-企业Gitlab-修改为有效的地址-更新成功', () => {
    expect(
      toolsPage.listPage_serviceName(gitlab_name2).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(gitlab_name2).clickNameInlistPage(); // 在列表页点击一个gitlab name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      gitlab_name2,
    );
    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('更新').click(); // 点击更新按钮

    toolsPage.updateDialog_host_inputbox.input(gitlab_host_valid); // 输入有效的服务地址

    toolsPage.updateDialog_updateButton.click(); // 更新Jenkins页点击"更新"按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('API 地址'),
      gitlab_host_valid,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(gitlab_name2);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(gitlab_host_valid);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Gitlab 企业版');

    // 详情页验证没有错误标识，为可用状态
    // 返回到Devops 工具链首页，该Gitlab卡片上没有有errormessage
  });

  it('L1:AldDevops-2432:集成-代码仓库-企业Gitlab-未绑定账号-前往项目', () => {
    expect(
      toolsPage.listPage_serviceName(gitlab_name2).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(gitlab_name2).clickNameInlistPage(); // 在列表页点击一个gitlab name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      gitlab_name2,
    );

    expect(toolsPage.resourceTable.getRowCount()).toBe(0); // 未绑定过账号
    expect(
      toolsPage.detailPage_toProject_link.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.detailPage_toProject_link.click(); // 点击“前往项目”后跳转到项目列表页

    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('代码仓库').click();
    expect(
      bindPage.bindDialog_tools_card(gitlab_name2).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(gitlab_name2).click();

    // 开始绑定账号-第一步
    bindPage.bind_firstStep_inputbox.inputByText('名称', gitlab_bind_name2);
    bindPage.bind_firstStep_inputbox.inputByText('描述', gitlab_bind_desc2);
    bindPage.bind_firstStep_secretDropdown.select_item(secret_name_error); // 选择secret
    bindPage.bind_firstStep_bindButton.click();

    // 等待状态消失
    bindPage.waitElementNotPresent(
      $('div .status aui-icon'),
      '【代码仓库获取中...】的提示没有消失',
    );

    // 再点击左侧导航 Devops工具链，到集成详情页，检查绑定信息
    toolsPage.navigationButton();
    expect(
      toolsPage.listPage_serviceName(gitlab_name2).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(gitlab_name2).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      gitlab_name2,
    );
    // expect(toolsPage.detailPage_acount_table.getRowCount()).toBe(1);
    // 检查绑定的数据是否正确
  });

  it('L1:AldDevops-2434:项目详情-DevOps工具链-绑定Github-使用Token认证方式绑定-并成功分配代码仓库', () => {
    projectPage.clickLeftNavByText('项目');
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('代码仓库').click();
    expect(
      bindPage.bindDialog_tools_card(github_name1).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(github_name1).click();

    // 开始绑定账号-第一步
    bindPage.bind_firstStep_inputbox.inputByText('名称', github_bind_name1);
    bindPage.bind_firstStep_inputbox.inputByText('描述', github_bind_desc1);
    bindPage.bind_firstStep_secretDropdown.select_item(secret_name1); // 选择secret
    bindPage.bind_firstStep_bindButton.click();

    // 等待状态消失
    bindPage.waitElementNotPresent(
      $('div .status aui-icon'),
      '【代码仓库获取中...】的提示没有消失',
    );

    // 分配代码仓库-第二步
    expect(
      bindPage.bindCode_secondStep_accounts.checkTextAllIsPresent(0),
    ).toBeTruthy();
    bindPage.bindCode_secondStep_accounts
      .getTextAll(0)
      .then(function(elemText) {
        bindPage
          .bindCode_secondStep_selectCode(elemText)
          .select_item('hello-world-golang');
      });
    // codePage.bindCodePage_title.click();
    expect(
      bindPage.bindCode_secondStep_bindButton.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCode_secondStep_bindButton.click();

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    CommonPage.waitElementTextChangeTo(
      bindPage.bindCodedetailPage_Content.getElementByText('名称'),
      github_bind_name1,
    );
    expect(
      bindPage.bindCodedetailPage_Content
        .getElementByText('仓库类型')
        .getText(),
    ).toBe('Github');
    expect(
      bindPage.bindCodedetailPage_Content.getElementByText('凭据').getText(),
    ).toBe(secret_name1);
    expect(
      bindPage.bindCodedetailPage_Content.getElementByText('描述').getText(),
    ).toBe(github_bind_desc1);

    // 列表等已分配的代码仓库出现
    expect(
      bindPage.bindCodedetailPage_assignTable_repoName(
        secret_username1,
        'hello-world-golang',
      ).checkTextAllIsPresent,
    ).toBeTruthy();
  });

  it('L1:AldDevops-2436:项目详情-DevOps工具链-绑定Github-使用Token认证方式绑定-分配仓库选择自动同步全部仓库-分配成功', () => {
    projectPage.clickLeftNavByText('项目');
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('代码仓库').click();
    expect(
      bindPage.bindDialog_tools_card(github_name1).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(github_name1).click();

    // 开始绑定账号-第一步
    bindPage.bind_firstStep_inputbox.inputByText('名称', github_bind_name2);
    bindPage.bind_firstStep_inputbox.inputByText('描述', github_bind_desc2);
    bindPage.bind_firstStep_secretDropdown.select_item(secret_name2); // 选择secret
    bindPage.click_firstStep_bindButton();

    // 分配代码仓库-第二步，勾选自动同步全部仓库
    expect(
      bindPage.bindCode_secondStep_autoCheckbox.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCode_secondStep_autoCheckbox.click();
    expect(
      bindPage.bindCode_secondStep_bindButton.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCode_secondStep_bindButton.click();

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    CommonPage.waitElementTextChangeTo(
      bindPage.bindCodedetailPage_Content.getElementByText('名称'),
      github_bind_name2,
    );
    expect(
      bindPage.bindCodedetailPage_Content
        .getElementByText('仓库类型')
        .getText(),
    ).toBe('Github');
    expect(
      bindPage.bindCodedetailPage_Content.getElementByText('凭据').getText(),
    ).toBe(secret_name2);
    expect(
      bindPage.bindCodedetailPage_Content.getElementByText('描述').getText(),
    ).toBe(github_bind_desc2);
  });

  it('L1:AldDevops-2423:项目详情-DevOps工具链-已绑定Github详情页-更新基本信息-更新成功', () => {
    projectPage.clickLeftNavByText('项目');
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click();

    // 点击已经绑定过的一个代码仓库服务到详情页
    expect(
      bindPage.bindListPage_toolName(github_bind_name1).checkNameInListPage(),
    ).toBeTruthy();
    bindPage.bindListPage_toolName(github_bind_name1).clickNameInlistPage();

    // 先检查是否到详情页
    CommonPage.waitElementTextChangeTo(
      bindPage.bindCodedetailPage_Content.getElementByText('名称'),
      github_bind_name1,
    );
    bindPage.bindCodedetailPage_operate.click(); //点击操作
    expect(
      bindPage.bindCodedetailPage_update_button.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCodedetailPage_update_button.click(); // 点击更新操作

    // 更新dialog
    CommonPage.waitElementTextChangeTo(
      bindPage.bindUpdateDialog_content.getElementByText('名称'),
      github_bind_name1,
    );
    bindPage.bindUpdateDialog_input.inputByText('描述', github_bind_desc1_new);
    bindPage.bindUpdateDialog_updateButton.click();
    // 更新成功后验证
    CommonPage.waitElementTextChangeTo(
      bindPage.bindCodedetailPage_Content.getElementByText('描述'),
      github_bind_desc1_new,
    );
  });

  it('L1:AldDevops-2425:项目详情-DevOps工具链-已绑定Github详情页-分配代码仓库-成功', () => {
    projectPage.clickLeftNavByText('项目');
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    // 点击已经绑定过的一个代码仓库服务到详情页
    expect(
      bindPage.bindListPage_toolName(github_bind_name1).checkNameInListPage(),
    ).toBeTruthy();
    bindPage.bindListPage_toolName(github_bind_name1).clickNameInlistPage();

    // 先检查是否到详情页
    CommonPage.waitElementTextChangeTo(
      bindPage.bindCodedetailPage_Content.getElementByText('名称'),
      github_bind_name1,
    );
    expect(
      bindPage.bindCodedetailPage_assign_button.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.click_bindCodedetailPage_assign_button(); //点击分配代码仓库按钮

    expect(
      bindPage.bindCodePage_assigndialog_accounts.checkTextAllIsPresent(0),
    ).toBeTruthy();
    bindPage.bindCodePage_assigndialog_accounts
      .getTextAll(0)
      .then(function(elemText) {
        bindPage
          .bindCodePage_assigndialog_selectCode(elemText)
          .select_item('hello-world-java');
      });
    bindPage.bindCodePage_assigndialog_title.click();
    expect(
      bindPage.bindCodePage_assigndialog_upate_button.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCodePage_assigndialog_upate_button.click();
  });

  it('L1:AldDevops-2424:项目详情-DevOps工具链-已绑定Github详情页--解除绑定-成功', () => {
    projectPage.clickLeftNavByText('项目');
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    // 点击已经绑定过的一个代码仓库服务到详情页
    expect(
      bindPage.bindListPage_toolName(github_bind_name1).checkNameInListPage(),
    ).toBeTruthy();
    bindPage.bindListPage_toolName(github_bind_name1).clickNameInlistPage();

    // 先检查是否到详情页
    CommonPage.waitElementTextChangeTo(
      bindPage.bindCodedetailPage_Content.getElementByText('名称'),
      github_bind_name1,
    );
    bindPage.bindCodedetailPage_operate.click(); //点击操作
    expect(
      bindPage.bindCodedetailPage_remove_button.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCodedetailPage_remove_button.click(); // 点击解除绑定操作

    expect(
      bindPage.bindCodedetailPage_removedialog_ok.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCodedetailPage_removedialog_ok.click(); // 点击删除按钮
    expect(
      bindPage.bindCodedetailPage_removedialog_know.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindCodedetailPage_removedialog_know.click(); // 点击知道了按钮
  });

  it('L1:AldDevops-2438:项目详情-DevOps工具链-绑定Github-使用Token认证方式绑定-选择错误的凭据-分配仓库表单验证(异常)', () => {
    projectPage.clickLeftNavByText('项目');
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('代码仓库').click();
    expect(
      bindPage.bindDialog_tools_card(github_name1).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(github_name1).click();

    // 开始绑定账号-第一步
    bindPage.bind_firstStep_inputbox.inputByText('名称', github_bind_nameerror);
    bindPage.bind_firstStep_inputbox.inputByText('描述', github_bind_descerror);
    bindPage.bind_firstStep_secretDropdown.select_item(secret_name_error); // 选择secret
    bindPage.click_firstStep_bindButton();

    expect(
      bindPage.bindCode_firstStep_erroralert.checkTextIsPresent(),
    ).toBeTruthy();
    browser.sleep(6000);
    bindPage.bindCode_secondStep_cancelButton.click(); // 点击取消按钮
  });

  it('L1:AldDevops-2523:删除集成-代码仓库-公有Github-未绑定过-可以删', () => {
    expect(
      toolsPage.listPage_serviceName(gitlab_name1).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(gitlab_name1).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      gitlab_name1,
    );

    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('删除').click();
    expect(
      toolsPage.deleteConfirm_deleteButton.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.deleteConfirm_deleteButton.click();
  });

  it('L1:AldDevops-2524:删除集成-代码仓库-公有Github-已绑定过-不可以删', () => {
    expect(
      toolsPage.listPage_serviceName(github_name1).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(github_name1).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      github_name1,
    );

    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('删除').click();
    expect(
      toolsPage.deleteConfirm_knowButton.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.deleteConfirm_knowButton.click();
  });
});
