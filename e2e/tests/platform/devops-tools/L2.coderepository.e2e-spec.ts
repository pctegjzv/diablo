/**
 * Created by zhangjiao on 2018/6/11.
 */

import { browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/platform/devops-tools/devopstools.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';

import {
  alauda_coderepo_service,
  alauda_type_project,
} from '../../../utility/resource.type.k8s';

describe('CodeRepo L2级别UI自动化case', () => {
  const bindPage = new DevopsBindPage();
  const toolsPage = new DevopsToolsPage();
  const projectPage = new ProjectsPage();
  const github_nameerror = bindPage.getTestData('githubl2-error-');
  const github_hosterror = 'https://api.github11.com';

  const project_name = toolsPage.getTestData('projectl2-incode-');
  const project_displayname = 'AUTOPROJECT-INCODE-L2';
  const project_desc = 'the second project2!';
  beforeAll(() => {
    this.testdata1 = CommonKubectl.createResource(
      'alauda.coderepo-github.yaml',
      {
        '${NAME}': github_nameerror,
        '${GITHUB_NAME}': github_nameerror,
        '${GITHUB_HOST}': github_hosterror,
        '${GITHUB_HTML}': github_hosterror,
      },
      'L2.coderepository.testdata1',
    );
    this.testdata2 = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_desc,
      },
      'L2.coderepository.testdata2',
    );
    browser.refresh();
    bindPage.enterAdminDashboard();
  });

  beforeEach(() => {});

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(github_nameerror, alauda_coderepo_service, null);
  });

  it('L2:AldDevops-2428:集成-代码仓库-企业GitLab-表单合法验证', () => {
    toolsPage.navigationButton();
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('代码仓库').click();
    toolsPage.createDialog_tools_card('Gitlab 企业版').click();

    toolsPage.createDialog_createButton.click(); // 点击集成按钮
    expect(
      toolsPage.createDialog_inputbox.getHintByText('API 地址').getText(),
    ).toBe('必填项');
    toolsPage.createDialog_inputbox.inputByText('API 地址', 'aaaaaa'); // API地址不合法。
    expect(
      toolsPage.createDialog_inputbox.getHintByText('API 地址').getText(),
    ).toBe('格式错误');
    toolsPage.createDialog_cancelButton.click(); // 点击取消按钮
  });

  it('L2:AldDevops-2437:L2: 项目详情-DevOps工具链-绑定Github-绑定账号表单验证', () => {
    projectPage.navigationButton();
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('代码仓库').click();
    expect(
      bindPage.bindDialog_tools_card(github_nameerror).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(github_nameerror).click();

    // 表单验证
    // 点击[绑定账号]按钮时，必填项提示
    bindPage.bind_firstStep_bindButton.click();
    CommonPage.waitElementTextChangeTo(
      bindPage.bind_firstStep_inputbox.getHintByText('名称'),
      '必填项',
    );
    expect(
      bindPage.bind_firstStep_inputbox.getHintByText('名称').getText(),
    ).toBe('必填项');

    bindPage.bind_firstStep_inputbox.inputByText('名称', '汉字'); // Secret名称输入不合法，中文。
    expect(
      bindPage.bind_firstStep_inputbox.getHintByText('名称').getText(),
    ).toBe('名称必须为小写字母，数字或"-", 以字母或数字结束。');

    bindPage.bind_firstStep_inputbox.inputByText('名称', 'DAXIE'); // Secret名称输入不合法，大写字母。
    expect(
      bindPage.bind_firstStep_inputbox.getHintByText('名称').getText(),
    ).toBe('名称必须为小写字母，数字或"-", 以字母或数字结束。');
  });

  it('L2:AldDevops-2439:项目详情-DevOps工具链-绑定Github-绑定账号表单页-认证方式选择Token-点击添加凭据', () => {
    projectPage.navigationButton();
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('代码仓库').click();
    expect(
      bindPage.bindDialog_tools_card(github_nameerror).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(github_nameerror).click();
    expect(bindPage.bindPage_addSecret.checkButtonIsPresent()).toBeTruthy();
    bindPage.bindPage_addSecret.click();
    expect(bindPage.addSecretDialog_type.checkTextIsPresent()).toBeTruthy();
    expect(bindPage.addSecretDialog_type.getText()).toBe('用户名/密码');
    bindPage.addSecretDialog_cancelButton.click();
    expect(bindPage.addSecretDialog_type.checkTextIsNotPresent()).toBeFalsy();
  });

  it('L2:AldDevops-2551:项目详情-DevOps工具链-绑定Github-绑定账号表单页-认证方式选择OAuth-点击添加凭据', () => {
    projectPage.navigationButton();
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('代码仓库').click();
    expect(
      bindPage.bindDialog_tools_card(github_nameerror).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(github_nameerror).click();
    expect(bindPage.bindPage_addSecret.checkButtonIsPresent()).toBeTruthy();
    bindPage.bind_firstStep_secretType.clickByName('OAuth2');
    bindPage.bindPage_addSecret.click();
    expect(bindPage.addSecretDialog_type.checkTextIsPresent()).toBeTruthy();
    expect(bindPage.addSecretDialog_type.getText()).toBe('OAuth2');
    bindPage.addSecretDialog_cancelButton.click();
    expect(bindPage.addSecretDialog_type.checkTextIsNotPresent()).toBeFalsy();
  });

  it('L2:AldDevops-2429:集成-代码仓库-选择企业GitLab-取消集成', () => {
    toolsPage.navigationButton();
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('代码仓库').click();
    toolsPage.createDialog_tools_card('Gitlab 企业版').click();

    toolsPage.createDialog_cancelButton.click(); // 点击取消按钮
    expect(
      toolsPage.createDialog_cancelButton.checkButtonIsNotPresent(),
    ).toBeFalsy();
  });
});
