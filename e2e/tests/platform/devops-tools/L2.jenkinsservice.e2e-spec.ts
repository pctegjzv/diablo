/**
 * Created by zhangjiao on 2018/3/16.
 */

import { browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/platform/devops-tools/devopstools.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_type_jenkins,
  alauda_type_project,
} from '../../../utility/resource.type.k8s';

describe('Devops工具链-Jenkins L2级别UI自动化case', () => {
  const toolsPage = new DevopsToolsPage();
  const bindPage = new DevopsBindPage();
  const projectPage = new ProjectsPage();
  const jenkins_name1 = toolsPage.getTestData('jenkinsl2-1-');
  const jenkins_host_valid = 'http://10.97.222.85'; // 有效的服务地址

  const project_name = toolsPage.getTestData('projectl2-injenkins');
  const project_displayname = 'AUTOPROJECTL2-INJENKINS';
  const project_desc = 'the second project in jenkinsL2!';

  beforeAll(() => {
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_desc,
      },
      alauda_type_project,
      null,
    );
    CommonApi.createResource(
      'alauda.jenkins.yaml',
      {
        '${NAME}': jenkins_name1,
        '${JENKINS_NAME}': jenkins_name1,
        '${JENKINS_HOST}': jenkins_host_valid,
      },
      alauda_type_jenkins,
      null,
    );
    browser.refresh();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {
    toolsPage.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(jenkins_name1, alauda_type_jenkins, null);
  });

  it('L2:AldDevops-2217:集成-Jenkins-集成页表单合法验证', () => {
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('持续集成').click();
    toolsPage.createDialog_tools_card('Jenkins').click();

    toolsPage.createDialog_createButton.click(); // 点击集成按钮
    CommonPage.waitElementTextChangeTo(
      toolsPage.createDialog_inputbox.getHintByText('API 地址'),
      '必填项',
    );
    expect(
      toolsPage.createDialog_inputbox.getHintByText('API 地址').getText(),
    ).toBe('必填项');
    toolsPage.createDialog_inputbox.inputByText('名称', 'DAXIEZIMU'); // 输入不合法的服务名称
    expect(
      toolsPage.createDialog_inputbox.getHintByText('名称').getText(),
    ).toBe('格式错误');
    toolsPage.createDialog_inputbox.inputByText('名称', 'aaa_aaa'); // 输入不合法的服务名称
    expect(
      toolsPage.createDialog_inputbox.getHintByText('名称').getText(),
    ).toBe('格式错误');
    toolsPage.createDialog_inputbox.inputByText('API 地址', 'aaa_aaa'); // 输入不合法的API地址
    expect(
      toolsPage.createDialog_inputbox.getHintByText('API 地址').getText(),
    ).toBe('格式错误');
    toolsPage.createDialog_inputbox.inputByText('名称', jenkins_name1); // 输入合法的服务名称
    toolsPage.createDialog_inputbox.inputByText('API 地址', jenkins_host_valid); // 输入合法的API地址
    toolsPage.createDialog_cancelButton.click(); // 点击取消关闭当前弹窗
  });

  it('L2:AldDevops-2220:集成-jenkins-集成页取消-返回Devops工具链首页', () => {
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('持续集成').click();
    toolsPage.createDialog_tools_card('Jenkins').click();

    toolsPage.createDialog_cancelButton.click(); // 点击取消按钮
    expect(
      toolsPage.createDialog_cancelButton.checkButtonIsNotPresent(),
    ).toBeFalsy(); // 点击取消操作后返回列表页，弹窗关闭
  });

  it('L2:AldDevops-2224:更新-jenkins-取消更新API-返回详情页', () => {
    expect(
      toolsPage.listPage_serviceName(jenkins_name1).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name1).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name1,
    );

    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('更新').click();
    toolsPage.updateDialog_host_inputbox.input('http://hello.com'); // 输入API地址
    toolsPage.updateDialog_cancelsButton.click();
    expect(
      toolsPage.updateDialog_cancelsButton.checkButtonIsNotPresent(),
    ).toBeFalsy();

    // 取消更新后，验证取消成功，并且到项目详情页。
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('API 地址'),
      jenkins_host_valid,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(jenkins_name1);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(jenkins_host_valid);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Jenkins');
  });

  it('L2:AldDevops-2527:集成绑定-Jenkins-到项目绑定Jenkins-绑定Jenkins页-表单合法验证', () => {
    projectPage.navigationButton();
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页

    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('持续集成').click();
    expect(
      bindPage.bindDialog_tools_card(jenkins_name1).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(jenkins_name1).click();

    expect(
      projectPage.bindJenkinsDialog_bindButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.bindJenkinsDialog_bindButton.click(); //点击“绑定账号”按钮，验证必填项
    expect(
      projectPage.bindJenkins_inputcontent.getHintByText('名称').getText(),
    ).toBe('必填项');
    expect(
      projectPage.bindJenkins_inputcontent.getHintByText('凭据').getText(),
    ).toBe('必填项');
  });
});
