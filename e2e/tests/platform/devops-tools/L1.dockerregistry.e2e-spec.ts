/**
 * Created by zhangjiao on 2018/8/20.
 */

import { browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/platform/devops-tools/devopstools.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_image_registry,
  alauda_type_project,
} from '../../../utility/resource.type.k8s';

describe('Docker Registry L1级别UI自动化case', () => {
  const toolsPage = new DevopsToolsPage();
  const bindPage = new DevopsBindPage();
  const projectPage = new ProjectsPage();

  const registry_name1 = toolsPage.getTestData('registryl1-');
  const registry_host1 = 'http://registry.online:3001';
  const registry_host_valid = 'http://alauda.codreamer.online:3001';

  const registry_name2 = toolsPage.getTestData('registryl1-api-');
  const registry_host2 = 'http://registry.online:3001';

  const project_name = toolsPage.getTestData('projectl1-inregistry-');
  const project_displayname = 'AUTOPROJECTL1-REGISTRY';

  const registry_bind_name1 = toolsPage.getTestData('registryl1-bind1-');
  const registry_bind_desc1 = 'auto bind registry service description1111!!!';
  const registry_bind_name2 = toolsPage.getTestData('registryl1-bind2-');
  const registry_bind_desc2 = 'auto bind registry service description222!!!';

  const secret_name_right = 'autosecretl1-registry1';
  const secret_username1 = 'admin';
  const secret_password1 = 'password';

  const secret_name_error = 'autosecretl1-registry-error';
  const secret_username2 = 'jiaozhang';
  const secret_password2 = 'yiersansiwu';

  beforeAll(() => {
    this.testdata1 = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_displayname,
      },
      'L1.dockerregistry.testdata1',
    );
    this.testdata2 = CommonKubectl.createResource(
      'alauda.docker-registry.yaml',
      {
        '${NAME}': registry_name2,
        '${REGISTRY_NAME}': registry_name2,
        '${REGISTRY_HOST}': registry_host2,
      },
      'L1.dockerregistry.testdata2',
    );
    this.testdata3 = CommonKubectl.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name_right,
        '${SECRETNAME}': secret_name_right,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username1).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password1).toString('base64'),
      },
      'L1.dockerregistry.testdata3',
    );
    this.testdata4 = CommonKubectl.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name_error,
        '${SECRETNAME}': secret_name_error,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username2).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password2).toString('base64'),
      },
      'L1.dockerregistry.testdata4',
    );
    browser.refresh();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {
    toolsPage.enterAdminDashboard();
    toolsPage.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(registry_name1, alauda_image_registry, null);
    CommonApi.deleteResource(registry_name2, alauda_image_registry, null);
  });

  it('L1:AldDevops-2572:集成-制品仓库-Docker Registry-输入正确的API地址集成成功', () => {
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('制品仓库').click();
    toolsPage.createDialog_tools_card('Docker Registry').click();

    toolsPage.createDialog_inputbox.inputByText('名称', registry_name1);
    toolsPage.createDialog_inputbox.inputByText('API 地址', registry_host1);
    toolsPage.createDialog_createButton.click(); // 点击集成按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      registry_name1,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(registry_name1);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(registry_host1);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Docker');
  });

  it('L1:AldDevops-2579:集成-制品仓库-Docker Registry-到详情页更新API地址-更新成功', () => {
    expect(
      toolsPage.listPage_serviceName(registry_name1).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(registry_name1).clickNameInlistPage(); // 在列表页点击一个gitlab name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      registry_name1,
    );
    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('更新').click(); // 点击更新按钮

    toolsPage.updateDialog_host_inputbox.input(registry_host_valid); // 输入有效的服务地址

    toolsPage.updateDialog_updateButton.click(); // 更新Jenkins页点击"更新"按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('API 地址'),
      registry_host_valid,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(registry_name1);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(registry_host_valid);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Docker');
  });

  it('L1:AldDevops-2580:集成-制品仓库-Docker Registry-未绑定账号时详情页提示“前往项目”', () => {
    expect(
      toolsPage.listPage_serviceName(registry_name1).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(registry_name1).clickNameInlistPage(); // 在列表页点击一个gitlab name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      registry_name1,
    );

    expect(toolsPage.resourceTable.getRowCount()).toBe(0); // 未绑定过账号
    expect(
      toolsPage.detailPage_toProject_link.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.detailPage_toProject_link.click(); // 点击“前往项目”后跳转到项目列表页

    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('制品仓库').click();
    expect(
      bindPage.bindDialog_tools_card(registry_name1).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(registry_name1).click();

    // 开始绑定账号-第一步
    bindPage.bind_firstStep_inputbox.inputByText('名称', registry_bind_name1);
    bindPage.bind_firstStep_inputbox.inputByText('描述', registry_bind_desc1);
    bindPage.bind_firstStep_secretDropdown.select_item(secret_name_error); // 选择secret
    bindPage.bind_firstStep_bindButton.click();

    // 再点击左侧导航 Devops工具链，到Docker Registry集成详情页，检查绑定信息
    toolsPage.navigationButton();
    expect(
      toolsPage.listPage_serviceName(registry_name1).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(registry_name1).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      registry_name1,
    );
    // expect(toolsPage.detailPage_acount_table.getRowCount()).toBe(1);
    // 检查绑定的数据是否正确
  });

  it('L1:AldDevops-2584:项目详情-DevOps工具链-绑定Docker Registry-输入正确的绑定账号-成功分配镜像仓库', () => {
    projectPage.clickLeftNavByText('项目');
    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage,
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    expect(
      projectPage.detailPage_updateButton.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('制品仓库').click();
    expect(
      bindPage.bindDialog_tools_card(registry_name1).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(registry_name1).click();

    // 开始绑定账号-第一步
    bindPage.bind_firstStep_inputbox.inputByText('名称', registry_bind_name2);
    bindPage.bind_firstStep_inputbox.inputByText('描述', registry_bind_desc2);
    bindPage.bind_firstStep_secretDropdown.select_item(secret_name_right); // 选择secret
    bindPage.bind_firstStep_bindButton.click();

    // 分配镜像仓库-第二步
    expect(
      bindPage.bind_secondStep_createButton.checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bind_secondStep_createButton.click();

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    // 列表等已分配的仓库出现检查
  });

  it('L1:AldDevops-2582:集成-制品仓库-Docker Registry-未绑定过-可以删除', () => {
    expect(
      toolsPage.listPage_serviceName(registry_name2).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(registry_name2).clickNameInlistPage();
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      registry_name2,
    );

    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('删除').click();
    expect(
      toolsPage.deleteConfirm_deleteButton.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.deleteConfirm_deleteButton.click();
  });

  it('L1:AldDevops-2583:集成-制品仓库-Docker Registry-已绑定-不可以删除-提示先解绑', () => {
    expect(
      toolsPage.listPage_serviceName(registry_name1).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(registry_name1).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      registry_name1,
    );

    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('删除').click();
    expect(
      toolsPage.deleteConfirm_knowButton.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.deleteConfirm_knowButton.click();
  });
});
