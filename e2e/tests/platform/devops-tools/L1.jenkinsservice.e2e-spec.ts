/**
 * Created by zhangjiao on 2018/3/1.
 */
import { browser } from 'protractor';

import { DevopsBindPage } from '../../../page_objects/platform/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/platform/devops-tools/devopstools.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_type_jenkins,
  alauda_type_project,
  alauda_type_secret,
} from '../../../utility/resource.type.k8s';

describe('Devops工具链-Jenkins L1级别UI自动化case', () => {
  const toolsPage = new DevopsToolsPage();
  const bindPage = new DevopsBindPage();
  const projectPage = new ProjectsPage();
  const jenkins_name = toolsPage.getTestData('jenkinsl1-');
  const jenkins_host_valid = 'http://10.97.222.85'; // 有效的服务地址
  const jenkins_host_invalid = 'http://invalid.com'; // 无效的服务地址

  const jenkins_name_new = toolsPage.getTestData('jenkinsl1-new-');
  const jenkins_bind_name = 'autojenkins-new-bind1';
  const jenkins_bind_description = 'bindbindbinddbindjenkins';

  const project_name = toolsPage.getTestData('projectl1-injenkins-');
  const project_displayname = 'AUTOPROJECTL1-INJENKINS';
  const project_desc = 'the first project in jenkinsl1!';

  const secret_name_api1 = 'autosecretl1-jenkins1';
  const secret_username_api1 = 'admin';
  const secret_password_api1 = '123456';

  beforeAll(() => {
    CommonApi.clearTestData(alauda_type_jenkins, 'autojenkins-');
    browser.refresh();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {
    toolsPage.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    browser.sleep(1000);
    CommonApi.deleteResource(jenkins_name, alauda_type_jenkins, null);
    CommonApi.deleteResource(jenkins_name_new, alauda_type_jenkins, null);
  });

  it('L1:AldDevops-2215:集成-Jenkins-输入正确的名称和API地址，集成成功', () => {
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('持续集成').click();
    toolsPage.createDialog_tools_card('Jenkins').click();

    toolsPage.createDialog_inputbox.inputByText('名称', jenkins_name);
    toolsPage.createDialog_inputbox.inputByText('API 地址', jenkins_host_valid);
    toolsPage.createDialog_createButton.click(); // 点击集成按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(jenkins_name);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(jenkins_host_valid);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Jenkins');
  });

  it('L1:AldDevops-2274:集成-Jenkins-输入正确的名称，无效的API地址，集成成功', () => {
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('持续集成').click();
    toolsPage.createDialog_tools_card('Jenkins').click();

    toolsPage.createDialog_inputbox.inputByText('名称', jenkins_name_new);
    toolsPage.createDialog_inputbox.inputByText(
      'API 地址',
      jenkins_host_invalid,
    );
    toolsPage.createDialog_createButton.click(); // 点击集成按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name_new,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(jenkins_name_new);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(jenkins_host_invalid);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Jenkins');
    // 加错误图标验证，以及Message不为空
    // 返回列表页错误图标验证，以及Message不为空
  });

  it('L1:AldDevops-2223:更新-Jenkins-修改为有效地址-更新成功', () => {
    expect(
      toolsPage.listPage_serviceName(jenkins_name_new).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name_new).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name_new,
    );
    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('更新').click(); // 点击更新按钮

    toolsPage.updateDialog_host_inputbox.input(jenkins_host_valid); // 输入有效的服务地址

    toolsPage.updateDialog_updateButton.click(); // 更新Jenkins页点击"更新"按钮

    // 创建后跳转到详情页，验证是否创建成功
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('API 地址'),
      jenkins_host_valid,
    );
    expect(
      toolsPage.detailPage_Content.getElementByText('名称').getText(),
    ).toBe(jenkins_name_new);
    expect(
      toolsPage.detailPage_Content.getElementByText('API 地址').getText(),
    ).toBe(jenkins_host_valid);
    expect(
      toolsPage.detailPage_Content.getElementByText('类型').getText(),
    ).toBe('Jenkins');

    // 详情页验证Jenkins状态，为可用状态，没有ErrorMessage图标
    // 到列表页，验证service-jenkins的状态
    // page.navigateTo('/jenkins-services/list');
  });

  it('L1:AldDevops-2520:集成-Jenkins-详情页-未绑定账号-前往项目', () => {
    // 通过YAML创建Project和Secret
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_desc,
      },
      alauda_type_project,
      null,
    );

    CommonApi.createResource(
      'alauda.secret.yaml',
      {
        '${NAME}': secret_name_api1,
        '${SECRETNAME}': secret_name_api1,
        '${NAMESPACE}': project_name,
        '${USERNAME}': Buffer.from(secret_username_api1).toString('base64'),
        '${PASSWORD}': Buffer.from(secret_password_api1).toString('base64'),
      },
      alauda_type_secret,
      project_name,
    );

    expect(
      toolsPage.listPage_serviceName(jenkins_name_new).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name_new).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name_new,
    );

    expect(toolsPage.resourceTable.getRowCount()).toBe(0);

    expect(
      toolsPage.detailPage_toProject_link.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.detailPage_toProject_link.click(); // 点击“前往项目”后跳转到项目列表页

    expect(
      projectPage.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    projectPage.listPage_projectName(project_name).clickNameInlistPage();

    projectPage.detailPage_TabItem('DevOps 工具链').click();
    expect(
      projectPage.detailPage_Bind_button.checkButtonIsPresent(),
    ).toBeTruthy();
    projectPage.detailPage_Bind_button.click(); // 点击绑定按钮
    expect(projectPage.bindJenkins_inputcontent.getTitleText()).toContain(
      '绑定',
    );

    bindPage.bindDialog_tools_type('持续集成').click();
    expect(
      bindPage.bindDialog_tools_card(jenkins_name_new).checkButtonIsPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(jenkins_name_new).click();
    // 检查是否到绑定页
    expect(bindPage.bindPage_header_text.getText()).toContain('绑定 Jenkins');
    const testData = {
      名称: jenkins_bind_name,
      描述: jenkins_bind_description,
      凭据: secret_name_api1,
    };
    projectPage.bindJenkins_inputbox(testData);
    // 绑定成功后跳转到项目详情页
    expect(projectPage.detailPage_TabItem('').getActiveText()).toBe('详情信息'); // 判断选中的是否是详情信息tab
    // 点击左侧导航 Devops工具链，到集成详情页，检查绑定信息
    toolsPage.navigationButton();
    expect(
      toolsPage.listPage_serviceName(jenkins_name_new).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name_new).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name_new,
    );
    // expect(toolsPage.detailPage_acount_table.getRowCount()).toBe(1);
    // 检查绑定的数据是否正确
  });

  it('L1:AldDevops-2518:删除集成-Jenkins-未绑定过-成功删除', () => {
    expect(
      toolsPage.listPage_serviceName(jenkins_name).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name,
    );
    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('删除').click(); // 点击删除按钮

    expect(
      toolsPage.deleteConfirm_deleteButton.checkButtonIsPresent(),
    ).toBeTruthy(); // 检查确认删除弹窗
    toolsPage.deleteConfirm_deleteButton.click();

    // 删除成功后跳转到Devops工具链的首页
    expect(toolsPage.listPage_createButton.checkButtonIsPresent()).toBeTruthy();
    expect(
      toolsPage.listPage_serviceName(jenkins_name).checkNameNotInListPage(),
    ).toBeFalsy();
  });

  it('L1:AldDevops-2525:删除集成-Jenkins-已绑定过-不可以删', () => {
    expect(
      toolsPage.listPage_serviceName(jenkins_name_new).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name_new).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getElementByText('名称'),
      jenkins_name_new,
    );
    toolsPage.detailPage_svgButton.click();
    toolsPage.detailPage_operateButton('删除').click(); // 点击删除按钮
    expect(
      toolsPage.deleteConfirm_knowButton.checkButtonIsPresent(),
    ).toBeTruthy();
    toolsPage.deleteConfirm_knowButton.click();
  });
});
