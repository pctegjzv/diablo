// /**
//  * Created by zhangjiao on 2018/3/20.
//  */

// import { browser } from 'protractor';

// import { HomePage } from '../../page_objects/home/home.page';
// import { ProjectsPage } from '../../page_objects/platform/projects/projects.page';
// import { CommonApi } from '../../utility/common.api';
// import { CommonPage } from '../../utility/common.page';
// import { alauda_type_project } from '../../utility/resource.type.k8s';

// describe('Home页 L1级别UI自动化case', () => {
//   const page = new HomePage();
//   let projectPage: ProjectsPage;
//   const prefix = page.getTestData('autoproject-home');
//   const project_name_api1 = page.getTestData(prefix);
//   const project_displayname_api1 = project_name_api1.toLocaleUpperCase();
//   const project_description_api1 = 'the first auto project for homepage!';

//   const project_name_api2 = page.getTestData(prefix);
//   const project_displayname_api2 = project_name_api2.toLocaleUpperCase();
//   const project_description_api2 = 'the second auto project for homepage!';

//   beforeAll(() => {
//     CommonApi.createResource(
//       'alauda.project.yaml',
//       {
//         '${NAME}': project_name_api1,
//         '${PROJECT_NAME}': project_name_api1,
//         '${PROJECT_DISPLAYNAME}': project_displayname_api1,
//         '${PROJECT_DESCRIPTION}': project_description_api1,
//       },
//       alauda_type_project,
//       null,
//     );
//     CommonApi.createResource(
//       'alauda.project.yaml',
//       {
//         '${NAME}': project_name_api2,
//         '${PROJECT_NAME}': project_name_api2,
//         '${PROJECT_DISPLAYNAME}': project_displayname_api2,
//         '${PROJECT_DESCRIPTION}': project_description_api2,
//       },
//       alauda_type_project,
//       null,
//     );
//     projectPage = new ProjectsPage();
//     browser.sleep(1000);
//   });

//   beforeEach(() => {
//     browser.refresh();
//     page.waitTableDisplay();
//   });

//   afterAll(() => {
//     CommonApi.deleteResource(project_name_api1, alauda_type_project, null);
//     CommonApi.deleteResource(project_name_api2, alauda_type_project, null);
//   });

//   it('L1:Ald-2372:Home页-“创建项目”按钮', () => {
//     page
//       .resourceTable()
//       .getRowCount()
//       .then(function(count) {
//         if (count === 0) {
//           expect(
//             page.homePage_createProject_Button.checkButtonIsPresent(),
//           ).toBeTruthy(); // Home页应该有"创建项目"按钮
//           expect(page.homePage_listNull_content.getText()).toBe(
//             '暂无项目，你可以通过上方“创建项目“按钮或切换至平台管理下创建项目',
//           );

//           page.homePage_createProject_Button.click(); // 点击"创建项目"按钮
//           // 判断是否到创建项目页+左侧导航是否正确
//           expect(
//             projectPage.createPage_headerTitle_Text.checkTextIsPresent(),
//           ).toBeTruthy();
//           expect(projectPage.createPage_headerTitle_Text.getText()).toBe(
//             '创建项目',
//           );
//         } else {
//           console.log(
//             'Home页有项目，一共有' +
//               count +
//               '个项目，有项目时没有“创建项目”按钮！',
//           );
//         }
//       });
//   });

//   // it('L1:Ald-2370:Home页-点击项目名称-到这个项目的项目管理', () => {
//   //   page
//   //     .resourceTable()
//   //     .getCell('名称', [project_displayname_api1])
//   //     .then(item => {
//   //       item.$('a').click();
//   //       // CommonPage.waitElementPresent(page.getElementByText('项目名称'));
//   //       expect(page.getElementByText('项目名称').getText()).toBe(
//   //         project_name_api1,
//   //       );
//   //     });
//   // });

//   it('L1:Ald-2368:Home页-项目列表-列表数据正确', () => {
//     page
//       .resourceTable()
//       .getRowCount()
//       .then(count => {
//         if (count === 0) {
//           expect(
//             page.homePage_createProject_Button.checkButtonIsPresent(),
//           ).toBeTruthy(); // 判断Home页是否有"创建项目"按钮
//           expect(page.homePage_listNull_content.getText()).toBe(
//             '暂无项目，你可以通过上方“创建项目“按钮或切换至平台管理下创建项目',
//           );
//         } else {
//           console.log(
//             'Home页有项目，一共有' +
//               count +
//               '个项目，有项目时没有“暂无项目”提示！',
//           );
//           page
//             .resourceTable()
//             .getCell('名称', [project_displayname_api1])
//             .then(function(elem) {
//               CommonPage.waitElementPresent(elem);
//               expect(elem.getText()).toContain(project_displayname_api1);
//             });
//           page
//             .resourceTable()
//             .getCell('名称', [project_displayname_api2])
//             .then(function(elem) {
//               CommonPage.waitElementPresent(elem);
//               expect(elem.getText()).toContain(project_displayname_api2);
//             });
//         }
//       });
//   });

//   it('L1:Ald-2369:Home页-按名称搜索项目', () => {
//     page.resourceTable().searchByResourceName('aotuprojectbucunzai', 0); // 在搜索框输入不存在的项目名称
//     expect(page.resourceTable().getRowCount()).toBe(0);
//     page.resourceTable().clearSearchBox();
//     // 在搜索框输入存在的项目名称
//     page.resourceTable().searchByResourceName(project_name_api1, 1);
//     expect(page.resourceTable().getRowCount()).toBe(1);
//     page.resourceTable().clearSearchBox();
//     page.resourceTable().searchByResourceName(prefix, 2); // 在搜索框输入模糊查询的项目名称
//     expect(page.resourceTable().getRowCount()).toBe(2);
//     page.resourceTable().clearSearchBox();
//   });
// });
