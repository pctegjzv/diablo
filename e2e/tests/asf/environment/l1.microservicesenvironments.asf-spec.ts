import { $, $$, browser } from 'protractor';

// import { ServerConf } from '../../../config/serverConf';
import { ConfigPage } from '../../../page_objects/asf/config.page';
import { EnvironmentPage } from '../../../page_objects/asf/environment/environment.page';
import { ServicePage } from '../../../page_objects/asf/service.page';
import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonMethod } from '../../../utility/common.method';

describe('MicroServiceEnvironment L1级别UI自动化case', () => {
  const page = new EnvironmentPage();
  const projectPage = new ProjectsPage();
  const servicePage = new ServicePage();
  const configPage = new ConfigPage();
  const prefix = 'asf-auto-';
  const projectName = CommonMethod.random_generate_testData(prefix);
  const name = CommonMethod.random_generate_testData(prefix);
  const disaplyName = CommonMethod.random_generate_testData(prefix);
  const describe = CommonMethod.random_generate_testData(prefix);
  const bindDescribe = CommonMethod.random_generate_testData(prefix);
  let isReady = true;

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${PROJECT_NAME}': projectName,
        '${PROJECT_DISPLAYNAME}': '',
        '${PROJECT_DESCRIPTION}': describe,
      },
      'L1.microservicesenvironments',
    );
    browser.refresh();
    page.enterAdminDashboard();
    const result = String(
      CommonKubectl.execKubectlCommand('kubectl get chart'),
    ).toLowerCase();
    isReady =
      isReady &&
      result.includes('zookeeper') &&
      result.includes('kafka') &&
      result.includes('config-server') &&
      result.includes('eureka') &&
      // result.includes('hystrix-dashboard') &&
      result.includes('hbase');
    // result.includes('turbine') &&
    // result.includes('zuul')
    // process.env.CLUSTER_MASTER_IP = 'asf-dev.alauda.cn';
  });

  beforeEach(() => {});

  afterAll(() => {
    // 删除项目
    CommonKubectl.execKubectlCommand(`kubectl delete project ${projectName}`);
    // 删除微服务环境
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservicesenvironments ${name}`,
    );
  });

  it('L1:AldDevops-2489: 进入平台管理，单击左导航微服务环境， 创建微服务环境', () => {
    if (isReady) {
      // 微服务相关的chart 准备成功， 开始测试
      const testData = {
        名称: name,
        显示名称: disaplyName,
        描述: describe,
      };
      // 创建微服务环境
      page.createAsfEnvironment(testData, name);
      // 创建成功后，微服务列表页面会新增一条新创建的微服务环境
      page.enterAdminDashboard();
      page.clickLeftNavByText('微服务环境');
      expect(page.listPage.isExist(name)).toBeTruthy();
    } else {
      throw new Error(
        '没有发现 chart(zookeeper, kafka, config-server, eureka, eureka, hystrix-dashboard, hbase, turbine, zuul)',
      );
    }
  });

  it('L1:AldDevops-2545: 进入平台管理, 单击左导航项目, 进入详情页，单击微服务环境tab, 绑定微服务环境', () => {
    if (isReady) {
      page.clickLeftNavByText('项目');
      // 检索新创建的微服务环境
      projectPage.resourceTable.clickResourceNameByRow([projectName]);
      projectPage.waitProgressBarNotPresent();
      expect(projectPage.detailPage.tabs.getText()).toEqual([
        '详情信息',
        'DevOps 工具链',
        '流水线模板',
        '资源',
        '微服务环境',
        '项目成员',
      ]);
      projectPage.detailPage.tabs.click('微服务环境');
      projectPage.detailPage.microServiceEnvironent
        .bind(`${name}(${disaplyName})`, bindDescribe)
        .then(() => {
          console.log('绑定完成了:\n');
          console.log(
            CommonKubectl.execKubectlCommand(
              `curl -i --insecure https://${process.env.ns}--eureka.${
                process.env.CLUSTER_MASTER_IP
              }`,
            ),
          );
        });

      // 验证绑定成功
      expect(
        projectPage.detailPage.microServiceEnvironent
          .getElementByText('名称')
          .getText(),
      ).toBe(name);

      expect(
        projectPage.detailPage.microServiceEnvironent
          .getElementByText('显示名称')
          .getText(),
      ).toBe(disaplyName);

      expect(
        projectPage.detailPage.microServiceEnvironent
          .getElementByText('描述')
          .getText(),
      ).toBe(describe);

      expect($$('.component-icons .component-icon-success').count()).toBe(4);
    } else {
      throw new Error(
        '没有发现 chart(zookeeper, kafka, config-server, eureka, eureka, hystrix-dashboard, hbase, turbine, zuul)',
      );
    }
  });

  it('L1:AldDevops-2594:进入项目管理，单击项目名称，进入项目，单击微服务，服务，验证eureka 服务发现列表正确', () => {
    if (isReady) {
      page.enterProjectDashboard();
      projectPage.resourceTable.clickResourceNameByRow([projectName]);
      projectPage.clickLeftNavByText('服务');
      servicePage.serviceTable.searchByResourceName('EUREKA-EUREKA');

      // 精确检索，只能检索到一条数据
      expect(servicePage.serviceTable.getRowCount()).toBe(1);
    } else {
      throw new Error(
        '没有发现 chart(zookeeper, kafka, config-server, eureka, eureka, hystrix-dashboard, hbase, turbine, zuul)',
      );
    }
  });

  xit('L1:AldDevops-2598:进入项目管理，单击项目名称，进入项目，单击微服务，配置，验证eureka 能读出demoservice 的配置', () => {
    CommonKubectl.createResource(
      'alauda.asf.demoservice.yaml',
      {
        '${PROJECT_NAME}': projectName,
        '${microservices_env}': name,
      },
      'L1.microservicesenvironments.demoservice',
    );
    page.creatingPage.waitEurekaRegistryDemoService();
    projectPage.clickLeftNavByText('配置');
    projectPage.waitProgressBarNotPresent(600000);
    const empty = $('.list-status-empty');
    empty.isPresent().then(isPresent => {
      if (isPresent) {
        projectPage.clickLeftNavByText('服务');
        projectPage.waitProgressBarNotPresent(600000);
        projectPage.clickLeftNavByText('配置');
        projectPage.waitProgressBarNotPresent(600000);
      }
    });
    empty.isPresent().then(isPresent => {
      if (isPresent) {
        projectPage.clickLeftNavByText('服务');
        projectPage.waitProgressBarNotPresent(600000);
        projectPage.clickLeftNavByText('配置');
        projectPage.waitProgressBarNotPresent(600000);
      } else {
        expect(configPage.ConfigTable.getRowCount()).toBe(4);
        configPage.openConfigFile();
        configPage.auiMonacoEditor.getText().then(text => {
          expect(text.includes(`eureka-eureka.${name}:8761`)).toBeTruthy();
        });
      }
    });
  });

  it('L1:AldDevops-2544: 进入平台管理, 单击左导航项目, 进入详情页，单击微服务环境tab, 解绑微服务环境', () => {
    if (isReady) {
      // 微服务相关的chart 准备成功， 开始测试
      page.enterAdminDashboard();
      page.clickLeftNavByText('项目');
      // 检索新创建的微服务环境
      projectPage.resourceTable.clickResourceNameByRow([projectName]);

      projectPage.detailPage.tabs.click('微服务环境');
      projectPage.detailPage.microServiceEnvironent.disBind(name);
      // 验证解绑成功
      expect(
        projectPage.detailPage.microServiceEnvironent.buttonBindingEnv.checkButtonIsPresent(),
      ).toBeTruthy();
    } else {
      throw new Error(
        '没有发现 chart(zookeeper, kafka, config-server, eureka, eureka, hystrix-dashboard, hbase, turbine, zuul)',
      );
    }
  });
});
