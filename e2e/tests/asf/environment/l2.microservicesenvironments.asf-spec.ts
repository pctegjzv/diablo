import { $$, browser } from 'protractor';

// import { ServerConf } from '../../../config/serverConf';
// import { AloSearch } from '../../../element_objects_new/alauda.aloSearch';
import { EnvironmentPage } from '../../../page_objects/asf/environment/environment.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonMethod } from '../../../utility/common.method';

describe('MicroServiceEnvironment L2级别UI自动化case', () => {
  const page = new EnvironmentPage();
  const prefix = 'asf-auto-';
  const projectName = CommonMethod.random_generate_testData(prefix);
  const name = CommonMethod.random_generate_testData(prefix);
  const disaplyName = CommonMethod.random_generate_testData(prefix);
  const describe = CommonMethod.random_generate_testData(prefix);
  let isReady = true;

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'alauda.asf.environment.yaml',
      {
        '${projectName}': projectName,
        '${name}': name,
        '${displayName}': disaplyName,
        '${description}': describe,
      },
      'L2.microservicesenvironments',
    );
    browser.refresh();
    page.enterAdminDashboard();
    const result = String(
      CommonKubectl.execKubectlCommand('kubectl get chart'),
    ).toLowerCase();
    isReady =
      isReady &&
      result.includes('zookeeper') &&
      result.includes('kafka') &&
      result.includes('config-server') &&
      result.includes('eureka') &&
      // result.includes('hystrix-dashboard') &&
      result.includes('hbase');
    // result.includes('turbine') &&
    // result.includes('zuul');
  });

  beforeEach(() => {});

  afterAll(() => {
    // 删除微服务环境
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservicesenvironments ${name}`,
    );
    CommonKubectl.execKubectlCommand(`kubectl delete project ${projectName}`);
  });

  it('L2:AldDevops-2490: 进入平台管理，单击左导航微服务环境， 查询微服务环境', () => {
    if (isReady) {
      page.clickLeftNavByText('微服务环境');
      // 检索新创建的微服务环境
      page.listPage.environmentTable.searchByResourceName(name);

      // 精确检索，只能检索到一条数据
      expect(page.listPage.environmentTable.getRowCount()).toBe(1);
      expect(page.listPage.isExist(name)).toBeTruthy();
    } else {
      throw new Error(
        '没有发现 chart(zookeeper, kafka, config-server, eureka, eureka, hystrix-dashboard, hbase, turbine, zuul)',
      );
    }
  });

  it('L2:AldDevops-2546: 进入平台管理, 单击左导航微服务环境, 进入详情页，更新基本信息', () => {
    if (isReady) {
      // 微服务相关的chart 准备成功， 开始测试
      page.clickLeftNavByText('微服务环境');
      // 检索新创建的微服务环境
      page.listPage.environmentTable.clickResourceNameByRow([name]);
      const disaplyName_update = CommonMethod.random_generate_testData(prefix);
      const describe_update = CommonMethod.random_generate_testData(prefix);
      page.detailPage.updateBasicInfo(
        name,
        disaplyName_update,
        describe_update,
      );

      page.clickLeftNavByText('微服务环境');

      // 检索新创建的微服务环境
      page.listPage.environmentTable.clickResourceNameByRow([name]);
      page.waitElementPresent(
        page.creatingPage.buttonInstall.button,
        '微服务环境的详情页没能打开',
      );

      // 验证更新成功
      expect(page.detailPage.getElementByText('显示名称').getText()).toBe(
        disaplyName_update,
      );
      expect(page.detailPage.getElementByText('描述').getText()).toBe(
        describe_update,
      );
    } else {
      throw new Error(
        '没有发现 chart(zookeeper, kafka, config-server, eureka, eureka, hystrix-dashboard, hbase, turbine, zuul)',
      );
    }
  });

  it('L2:AldDevops-2547: 进入平台管理, 单击左导航微服务环境, 详情页，删除微服务环境', () => {
    if (isReady) {
      // 微服务相关的chart 准备成功， 开始测试
      page.clickLeftNavByText('微服务环境');

      page.listPage.environmentTable.clickResourceNameByRow([name]);
      // 检索新创建的微服务环境
      page.waitElementPresent(
        page.creatingPage.buttonInstall.button,
        '微服务环境的详情页没能打开',
      );
      page.detailPage.deleteMicroServiceEnv(name);

      // 验证删除成功
      page.clickLeftNavByText('微服务环境');
      page.listPage.environmentTable.searchByResourceName(name, 0);
      // 精确检索，检索不到数据
      const row = $$('aui-table-row').first();
      expect(row.isPresent()).toBeFalsy();
    } else {
      throw new Error(
        '没有发现 chart(zookeeper, kafka, config-server, eureka, eureka, hystrix-dashboard, hbase, turbine, zuul)',
      );
    }
  });
});
