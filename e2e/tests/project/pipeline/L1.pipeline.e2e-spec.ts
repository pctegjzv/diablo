/**
 * Created by zhangjiao on 2018/3/28.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { HomePage } from '../../../page_objects/home/home.page';
import { PipelinePage } from '../../../page_objects/project/pipeline/pipeline.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonMethod } from '../../../utility/common.method';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_type_deploy,
  alauda_type_jenkins,
  alauda_type_jenkinsbinding,
  alauda_type_project,
  alauda_type_secret,
} from '../../../utility/resource.type.k8s';

describe('流水线 L1级别UI自动化case', () => {
  const page = new PipelinePage();
  const homePage = new HomePage();

  const project_name = page.getTestData('projectl1-inpipeline-');
  const project_desc = 'create a project in pipeline!';
  const jenkins_name = page.getTestData('jenkinsl1-inpipeline-');
  const jenkins_host = ServerConf.JENKINSURL;

  const pipeline_name1 = page.getTestData('pipelinel1-1-');
  const pipeline_displayName1 = page.getTestData('Pipeline-1-');
  const pipeline_displayName1_new = page.getTestData('PipelineL1-New-');

  const pipeline_name2 = page.getTestData('pipelinel1-2-');
  const pipeline_displayName2 = page.getTestData('Pipeline-2-');
  const pipeline_displayName2_new = page.getTestData('PipelineL1-');
  const code_address = 'https://github.com/jiaozhang1/logtest.git';

  const pipeline_name3 = page.getTestData('pipelinel1-3-');
  const pipeline_displayName3 = page.getTestData('Pipeline-3-');

  const application_name_api = 'autoapplication-api1';
  const application_container_api = 'autocontainer-api1';
  beforeAll(() => {
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_name,
        '${PROJECT_DESCRIPTION}': project_desc,
      },
      alauda_type_project,
      null,
    );
    browser.sleep(3000); // 加等待时间，是防止namespace还是creating状态

    CommonApi.createResource(
      'alauda.jenkins-secret.yaml',
      {
        '${NAME}': jenkins_name + '-secret',
        '${JENKINS_NAME}': jenkins_name,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_secret,
      null,
    );
    CommonApi.createResource(
      'alauda.git-secret.yaml',
      {
        '${NAME}': jenkins_name + '-git',
        '${JENKINS_NAME}': jenkins_name,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_secret,
      null,
    );

    CommonApi.createResource(
      'alauda.jenkins.yaml',
      {
        '${NAME}': jenkins_name,
        '${JENKINS_NAME}': jenkins_name,
        '${JENKINS_HOST}': jenkins_host,
      },
      alauda_type_jenkins,
      null,
    );

    CommonApi.createResource(
      'alauda.jenkinsbinding.yaml',
      {
        '${NAME}': jenkins_name,
        '${JENKINS_NAME}': jenkins_name,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_jenkinsbinding,
      null,
    );
    browser.sleep(2000);
    CommonApi.createResource(
      'alauda.deployment.yaml',
      {
        '${NAME}': application_name_api,
        '${APP_NAME}': application_name_api,
        '${CONTAINER_NAME}': application_container_api,
        '${PROJECT}': project_name,
      },
      alauda_type_deploy,
      project_name,
    );
    browser.refresh();
    homePage.enterProjectDashboard();
    homePage.resourceTable.clickResourceNameByRow([project_name]);
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(jenkins_name, alauda_type_jenkins, null);
  });

  // ---------begin------创建相关测试用例
  it('L1:AldDevops-2484:脚本创建流水线-通过页面编写Jenkinsfile-成功创建流水线', () => {
    const testData = {
      名称: pipeline_name1,
      显示名称: pipeline_displayName1,
      'Jenkins 实例': jenkins_name,
      所属应用: application_name_api,
      'Jenkinsfile 位置': '页面编写',
    };
    page.createPipiline_byscript_first(testData, project_name);
    page.createPage_yaml_textarea.checkEditorIsPresent();

    // 将yaml文件拷贝至yaml编辑框
    expect(page.createPage_yaml_textarea.checkEditorIsPresent()).toBeTruthy();
    const yamlFile = CommonMethod.readyamlfile('alauda.jenkinsfile.yaml');
    page.createPage_yaml_textarea.setYamlValue(yamlFile);
    page.createPage_nextButton.click();
    // 第三步骤-直接点击创建按钮
    page.createPage_createButton.click();

    // 创建成功过后详情页检查
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('流水线名称'),
      pipeline_name1,
    );
    expect(
      page.detailPage_Content.getElementByText('流水线名称').getText(),
    ).toBe(pipeline_name1);
    expect(
      page.detailPage_Content.getElementByText('Jenkins 实例').getText(),
    ).toBe(jenkins_name);
  });

  it('L1:AldDevops-2475:脚本创建流水线-通过代码仓库位置选择Jenkinsfile(输入仓库地址)-成功创建流水线', () => {
    const testData = {
      名称: pipeline_name2,
      显示名称: pipeline_displayName2,
      'Jenkins 实例': jenkins_name,
      所属应用: application_name_api,
      'Jenkinsfile 位置': '代码仓库',
    };
    page.createPipiline_byscript_first(testData, project_name);
    page.createPipeline_byscript_input(code_address, jenkins_name + '-git');
    // 选择代码仓库
    const testData1 = {
      代码分支: 'master',
      脚本路径: 'Jenkinsfile',
    };
    page.createPipiline_byscript_second(testData1);
    // 第三步骤-直接点击创建按钮
    page.createPage_createButton.click();

    // 创建成功过后详情页检查
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('流水线名称'),
      pipeline_name2,
    );
    expect(
      page.detailPage_Content.getElementByText('流水线名称').getText(),
    ).toBe(pipeline_name2);
    expect(
      page.detailPage_Content.getElementByText('Jenkins 实例').getText(),
    ).toBe(jenkins_name);
  });

  it('L1:AldDevops-2533:脚本创建流水线-通过代码仓库位置选择Jenkinsfile(选择仓库地址)-成功创建流水线', () => {
    const testData = {
      名称: pipeline_name3,
      显示名称: pipeline_displayName3,
      'Jenkins 实例': jenkins_name,
      所属应用: application_name_api,
      'Jenkinsfile 位置': '代码仓库',
    };
    page.createPipiline_byscript_first(testData, project_name);
  });

  // --------begin--------详情页的相关用例
  it('L1:AldDevops-2465:详情页-更新-脚本创建的流水线-通过页面编写Jenkinsfile的流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name1)
      .clickNameInlistPage();

    page.detailPage_operate_all.click();
    page.detailPage_operate_button('更新').click();
    // 到更新页
    CommonPage.waitElementTextChangeTo(
      page.updatePage_Content.getElementByText('名称'),
      pipeline_name1,
    );
    expect(page.updatePage_Content.getElementByText('名称').getText()).toBe(
      pipeline_name1,
    );
    expect(
      page.updatePage_Content.getElementByText('Jenkins 实例').getText(),
    ).toBe(jenkins_name);

    page.updatePage_inputbox.inputByText('显示名称', pipeline_displayName1_new);
    page.updatePage_trigger('定时触发器').click();
    page.updatePage_trigger_inputbox.input('H/30 * * * *');
    page.updatePage_updateButton.click();
    // 详情页检查是否更新成功
    expect(page.detailPage_Content_trigger.checkButtonIsPresent()).toBeTruthy();
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      pipeline_displayName1_new,
    );
  });

  it('L1:AldDevops-2531:详情页-复制-脚本创建的流水线-通过页面编写Jenkinsfile的流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name1)
      .clickNameInlistPage();
    page.detailPage_operate_all.click();
    page.detailPage_operate_button('复制').click();
    // 到复制页，Jenkinsfile默认选中页面编写
    expect(page.createPipeline_location.getText()).toBe('页面编写'); // 判断选中的是否是“页面编写”
  });

  it('L1:AldDevops-2464:详情页-执行-执行记录检查', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name1)
      .clickNameInlistPage();

    page.detailPage_operate_all.click();
    page.detailPage_operate_button('执行').click();
    // 执行成功后跳转到执行记录详情页，检查
  });

  xit('L1:AldDevops-2468:详情页-流水线设计-脚本创建的流水线-通过页面编写Jenkinsfile的流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name1)
      .clickNameInlistPage();
    expect(
      page.detailPage_TabItem('流水线设计').checkTabClickIsPresent(),
    ).toBeTruthy();
    page.detailPage_TabItem('流水线设计').click(); // 点击详情页的Jenkinsfile的tab
    // 流水线设计展示Jenkinsfile
    expect(
      page.detailPage_PipelineTab_jenkins.checkTextIsPresent(),
    ).toBeTruthy();
  });

  it('L1:AldDevops-2534:详情页-流水线设计-脚本创建的流水线-通过代码仓库位置选择Jenkinsfile的流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name2)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name2)
      .clickNameInlistPage();
    expect(
      page.detailPage_TabItem('流水线设计').checkTabClickIsPresent(),
    ).toBeTruthy();
    page.detailPage_TabItem('流水线设计').click(); // 点击详情页的Jenkinsfile的tab
    // 流水线设计展示代码仓库位置
    expect(
      page.detailPage_pipeline_Content.getElementByText('代码仓库').getText(),
    ).toBe(code_address);
  });
  // --------end--------详情页的相关用例

  // --------begin--------列表页的相关用例
  it('L1:AldDevops-2455:列表页-更新-脚本创建的流水线-通过代码仓库位置选择Jenkinsfile的流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name2)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '更新')
      .clickOperateAllButton(); // 列表页更新操作
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '更新')
      .clickOperateButton();
    // 到更新页
    CommonPage.waitElementTextChangeTo(
      page.updatePage_Content.getElementByText('名称'),
      pipeline_name2,
    );
    expect(page.updatePage_Content.getElementByText('名称').getText()).toBe(
      pipeline_name2,
    );
    expect(
      page.updatePage_Content.getElementByText('Jenkins 实例').getText(),
    ).toBe(jenkins_name);

    page.updatePage_inputbox.inputByText('显示名称', pipeline_displayName2_new);
    page.updatePage_trigger('定时触发器').click();
    page.updatePage_trigger_inputbox.input('H/30 * * * *');
    page.updatePage_updateButton.click();
    // 详情页检查是否更新成功
    expect(page.detailPage_Content_trigger.checkButtonIsPresent()).toBeTruthy();
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      pipeline_displayName2_new,
    );
  });

  it('L1:AldDevops-2456:列表页-复制-脚本创建的流水线-通过代码仓库位置选择Jenkinsfile的流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name2)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '复制')
      .clickOperateAllButton(); // 列表页更新操作
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '复制')
      .clickOperateButton();
    // 到复制页，Jenkinsfile默认选中代码仓库
    expect(page.createPipeline_location.getText()).toBe('代码仓库'); // 判断选中的是否是“代码仓库”
  });

  it('L1:AldDevops-2454:列表页-执行-执行记录检查', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name2)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '执行')
      .clickOperateAllButton(); // 执行操作
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '执行')
      .clickOperateButton();
    // 执行成功后跳转到详情页，基本信息检查
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('流水线名称'),
      pipeline_name2,
    );
    expect(
      page.detailPage_Content.getElementByText('流水线名称').getText(),
    ).toBe(pipeline_name2);
    expect(
      page.detailPage_Content.getElementByText('Jenkins 实例').getText(),
    ).toBe(jenkins_name);

    expect(page.detailPage_executeHistory_Table.getRowCount()).toBe(1);

    // 这里要加等待执行成功，然后再删掉这个执行。
    // 删除
    // page.detailPage_executeHistory_getid.getText().then(function (historyId1) {
    //   expect(page.detailPage_executeHistory_id(historyId1).checkNameInListPage()).toBeTruthy();
    //   page.detailPage_executeHistory_operate(historyId1, '删除').clickOperateAllButton();  // 删除操作
    //   page.detailPage_executeHistory_operate(historyId1, '删除').clickOperateButton();
    //   page.detailPage_executeHistory_operate(historyId1, '删除').clickOkButton();
    //   expect(page.detailPage_executeHistory_id(historyId1).checkNameNotInListPage()).toBeFalsy();
    // });
  });

  it('L1:AldDevops-2539:详情页-执行记录-查看执行记录详情概览-关闭', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name1)
      .clickNameInlistPage();
    expect(
      page.detailPage_historyTable_logview.checkButtonIsPresent(),
    ).toBeTruthy();
    page.detailPage_historyTable_logview.click();
    expect(
      page.logView_dialog_header(pipeline_name1).checkTextIsPresent(),
    ).toBeTruthy();
    page.logView_dialog_cancel.click();
  });

  it('L1:AldDevops-2474:列表页-执行记录-查看执行记录详情概览-关闭', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name2)
        .checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_historyView(project_name, pipeline_name2).click();
    expect(
      page.logView_dialog_header(pipeline_name2).checkTextIsPresent(),
    ).toBeTruthy();
    page.logView_dialog_cancel.click();
  });
  // --------end--------列表页的相关用例

  it('L1:AldDevops-2457:列表页-删除流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name2)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '删除')
      .clickOperateAllButton(); // 列表页删除操作
    page
      .listPage_pipelineOperate(project_name, pipeline_name2, '删除')
      .clickOperateButton();
    page.listPage_confirmdelete_button().click();
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name2)
        .checkNameNotInListPage(),
    ).toBeFalsy();
  });

  it('L1:AldDevops-2467:详情页-删除流水线', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name1)
      .clickNameInlistPage();

    page.detailPage_operate_all.click();
    page.detailPage_operate_button('删除').click();
    page.listPage_confirmdelete_button().click();
    expect(
      page.listPage_nameFilter_input.checkInputboxIsPresent(),
    ).toBeTruthy(); // 删除后返回列表页，检查搜索框是否存在
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameNotInListPage(),
    ).toBeFalsy();
  });
});
