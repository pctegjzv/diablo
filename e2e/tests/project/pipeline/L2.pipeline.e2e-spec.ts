/**
 * Created by zhangjiao on 2018/3/28.
 */
import { HomePage } from '../../../page_objects/home/home.page';
import { PipelinePage } from '../../../page_objects/project/pipeline/pipeline.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_type_jenkins,
  alauda_type_jenkinsbinding,
  alauda_type_pipeline,
  alauda_type_project,
  alauda_type_secret,
} from '../../../utility/resource.type.k8s';

describe('流水线 L2级别UI自动化case', () => {
  const page = new PipelinePage();
  const homePage = new HomePage();

  const project_name = page.getTestData('projectl2-inpipeline-');
  const project_description = 'create a project in pipeline!';
  const jenkins_name_api = page.getTestData('jenkinsl2-inpipeline-');
  const jenkins_host_api = 'http://10.96.131.136:8080';

  const pipeline_name_prefix = page.getTestData('pipelinetest');
  const pipeline_name_api1 = page.getTestData(pipeline_name_prefix);
  const pipeline_name_api2 = page.getTestData(pipeline_name_prefix);

  const pipeline_sourceGit_api1 =
    'https://github.com/jiaozhang1/hello-world-java.git';

  beforeAll(() => {
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_name,
        '${PROJECT_DESCRIPTION}': project_description,
      },
      alauda_type_project,
      null,
    );
    CommonApi.createResource(
      'alauda.jenkins-secret.yaml',
      {
        '${NAME}': jenkins_name_api + '-secret',
        '${JENKINS_NAME}': jenkins_name_api,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_secret,
      null,
    );
    CommonApi.createResource(
      'alauda.git-secret.yaml',
      {
        '${NAME}': jenkins_name_api + '-git',
        '${JENKINS_NAME}': jenkins_name_api,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_secret,
      null,
    );

    CommonApi.createResource(
      'alauda.jenkins.yaml',
      {
        '${NAME}': jenkins_name_api,
        '${JENKINS_NAME}': jenkins_name_api,
        '${JENKINS_HOST}': jenkins_host_api,
      },
      alauda_type_jenkins,
      null,
    );

    CommonApi.createResource(
      'alauda.jenkinsbinding.yaml',
      {
        '${NAME}': jenkins_name_api,
        '${JENKINS_NAME}': jenkins_name_api,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_jenkinsbinding,
      null,
    );
    CommonApi.createResource(
      'alauda.pipeline.yaml',
      {
        '${NAME}': pipeline_name_api1,
        '${PROJECT_NAME}': project_name,
        '${JENKINS_NAME}': jenkins_name_api,
        '${PIPELINE_NAME}': pipeline_name_api1,
        '${SOURCEGIT}': pipeline_sourceGit_api1,
      },
      alauda_type_pipeline,
      null,
    );
    CommonApi.createResource(
      'alauda.pipeline.yaml',
      {
        '${NAME}': pipeline_name_api2,
        '${PROJECT_NAME}': project_name,
        '${JENKINS_NAME}': jenkins_name_api,
        '${PIPELINE_NAME}': pipeline_name_api2,
        '${SOURCEGIT}': pipeline_sourceGit_api1,
      },
      alauda_type_pipeline,
      null,
    );
    homePage.enterProjectDashboard();
    homePage.resourceTable.clickResourceNameByRow([project_name]);
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(jenkins_name_api, alauda_type_jenkins, null);
  });
  it('L2:AldDevops-2453:列表页-搜索功能-按名称搜索', () => {
    expect(
      page.listPage_nameFilter_input.checkInputboxIsPresent(),
    ).toBeTruthy(); // 检查搜索框是否存在

    page.resourceTable.searchByResourceName('autopipeline_bucunzai', 0); // 在搜索框输入不存在的流水线名称
    expect(page.resourceTable.getRowCount()).toBe(0);
    page.resourceTable.aloSearch.auiSearch.clear(); // 清空检索框
    page.resourceTable.searchByResourceName(pipeline_name_api1, 1); // 在搜索框输入要检索的流水线名称
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name_api1)
        .checkNameInListPage(),
    ).toBeTruthy();
    expect(page.resourceTable.getRowCount()).toBe(1);
    page.resourceTable.aloSearch.auiSearch.clear(); // 清空检索框
    page.resourceTable.searchByResourceName(pipeline_name_prefix, 2); // 在搜索框输入模糊查询的流水线名称

    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name_api1)
        .checkNameInListPage(),
    ).toBeTruthy();
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name_api2)
        .checkNameInListPage(),
    ).toBeTruthy();
    expect(page.resourceTable.getRowCount()).toBe(2);
    page.resourceTable.aloSearch.auiSearch.clear(); // 清空检索框

    // 创建后跳转到详情页，验证Yaml创建的流水线是否创建成功
    page
      .listPage_pipelineName(project_name, pipeline_name_api1)
      .clickNameInlistPage();
    // 基本信息检查
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('流水线名称'),
      pipeline_name_api1,
    );
    expect(
      page.detailPage_Content.getElementByText('流水线名称').getText(),
    ).toBe(pipeline_name_api1);
    expect(
      page.detailPage_Content.getElementByText('Jenkins 实例').getText(),
    ).toBe(jenkins_name_api);

    // 判断如果有流水线执行历史就删除
    page.detailPage_executeHistory_Table.getRowCount().then(function(count) {
      if (count >= 1) {
        page
          .detailPage_executeHistory_operate(pipeline_name_api1 + '-1', '删除')
          .clickOperateAllButton(); // 删除操作
        page
          .detailPage_executeHistory_operate(pipeline_name_api1 + '-1', '删除')
          .clickOperateButton();
        page
          .detailPage_executeHistory_operate(pipeline_name_api1 + '-1', '删除')
          .clickOkButton();
        expect(
          page
            .detailPage_executeHistory_id(pipeline_name_api1 + '-1')
            .checkNameNotInListPage(),
        ).toBeFalsy();
      }
    });
  });

  /* TODO: disabled for not pipeline history status check.
    it('L2:Ald-2355:流水线详情-再次执行-取消执行', () => {
        expect(page.listPage_pipelineName(project_name, pipeline_name_api1).checkNameInListPage()).toBeTruthy();
        page.listPage_pipelineName(project_name, pipeline_name_api1).clickNameInlistPage();
        page.detailPage_executeHistory_getid.getText().then(function (historyId1) {
            expect(page.detailPage_executeHistory_id(historyId1).checkNameInListPage()).toBeTruthy();
            page.detailPage_executeHistory_operate(historyId1, '再次执行').clickOperateAllButton();  // 执行操作
            page.detailPage_executeHistory_operate(historyId1, '再次执行').clickOperateButton();
        });
        // 再次执行后，判断是否有新的历史记录生成
        page.detailPage_executeHistory_getid.getText().then(function (historyId2) {
            expect(page.detailPage_executeHistory_id(historyId2).checkNameInListPage()).toBeTruthy();
            // 取消执行
            page.detailPage_executeHistory_operate(historyId2, '取消执行').clickOperateAllButton();  // 执行操作
            page.detailPage_executeHistory_operate(historyId2, '取消执行').clickOperateButton();
            page.detailPage_executeHistory_operate(historyId2, '取消执行').clickOkButton(); // 点击确认取消执行
        });
    });*/

  it('L2:AldDevops-2535:脚本创建流水线-取消创建-返回列表页', () => {
    expect(
      page.listPage_createPipeline_Button.checkButtonIsPresent(),
    ).toBeTruthy();
    page.listPage_createPipeline_Button.click();
    page.listPage_createMode_card(project_name).click();
    expect(page.createPage_cancelButton.checkButtonIsPresent()).toBeTruthy();
    page.createPage_cancelButton.click();
    // 取消创建后返回列表页
    expect(
      page.listPage_createPipeline_Button.checkButtonIsPresent(),
    ).toBeTruthy();
  });

  it('L2:AldDevops-2483:更新脚本创建的流水线-取消更新-返回详情页', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name_api1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name_api1)
      .clickNameInlistPage();

    expect(
      page.detailPage_TabItem('流水线设计').checkTabClickIsPresent(),
    ).toBeTruthy();
    page.detailPage_TabItem('流水线设计').click(); // 点击详情页的Jenkinsfile的tab
    page.detailPage_operate_all.click();
    page.detailPage_operate_button('更新').click();
    expect(page.updatePage_cancelButton.checkButtonIsPresent()).toBeTruthy();
    page.updatePage_cancelButton.click(); // 点击“取消”按钮
    // 取消更新后返回详情页
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('流水线名称'),
      pipeline_name_api1,
    );
    expect(
      page.detailPage_Content.getElementByText('流水线名称').getText(),
    ).toBe(pipeline_name_api1);
  });
});
