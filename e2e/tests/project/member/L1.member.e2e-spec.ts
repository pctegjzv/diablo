/**
 * Created by zhangjijao on 2018/4/8.
 */

import { browser } from 'protractor';

import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { MemberPage } from '../../../page_objects/project/member/member.page';
import { CommonApi } from '../../../utility/common.api';
import { alauda_type_project } from '../../../utility/resource.type.k8s';

describe('成员 L1级别UI自动化case', () => {
  const projectPage = new ProjectsPage();
  const page = new MemberPage();
  const project_name = page.getTestData('project-inmember-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'Member自动化测试中添加一个project！';

  const person_email = 'zhangjiao521@alauda.io';
  const person_email1 = 'zhangjiao555@alauda.io';

  beforeAll(() => {
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
      },
      alauda_type_project,
      null,
    );
    browser.refresh();
    page.enterProjectDashboard();
    projectPage.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
  });

  xit('L1: 添加成员', () => {
    expect(page.addMember_email_inputbox.checkInputboxIsPresent()).toBeTruthy();
    browser.sleep(1000);
    page.addMember_email_inputbox.input(person_email);
    page.addMember_role_dropdown.select('开发测试');
    page.addMember_add_button.click(); // 点击"添加成员"按钮
    expect(
      page.listPage_memberName(person_email).checkNameInListPage(),
    ).toBeTruthy();

    page.addMember_email_inputbox.input(person_email1);
    page.addMember_role_dropdown.select('项目经理');
    page.addMember_add_button.click(); // 点击"添加成员"按钮
    expect(
      page.listPage_memberName(person_email1).checkNameInListPage(),
    ).toBeTruthy();
    expect(page.resourceTable.getRowCount()).toBe(2);
    page.resourceTable.getCell('邮箱', [person_email]).then(function(elem) {
      expect(elem.getText()).toBe(person_email);
    });
    page.resourceTable.getCell('邮箱', [person_email1]).then(function(elem) {
      expect(elem.getText()).toBe(person_email1);
    });
  });

  xit('L2:删除已经添加的成员', () => {
    expect(
      page.listPage_memberName(person_email).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_memberOperate(person_email).clickOperateButton();
    page.listPage_memberOperate(person_email).clickOkButton();
    expect(
      page.listPage_memberName(person_email).checkNameNotInListPage(),
    ).toBeFalsy();
  });
});
