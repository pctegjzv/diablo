/**
 * Created by zhangjiao on 2018/3/22.
 */

// import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { ApplicationPage } from '../../../page_objects/project/application/application.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('应用 L1级别UI自动化case', () => {
  const page = new ApplicationPage();
  // const pipelinePage = new PipelinePage();
  const project_name = page.getTestData('project-app1-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'the first auto project for application!';

  const application_name = page.getTestData('application-yaml');

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'alauda.appcore.yaml',
      {
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
        '${NAME}': application_name,
        '${LABEL}': application_name,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.deployment.detail.application',
    );
    page.enterProjectDashboard();
    page.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(`kubectl delete project ${project_name}`);
  });

  it('L1:AldDevops-2297:应用列表展示', () => {
    page.clickLeftNavByText('应用');
    // 默认应用是折叠的
    expect(page.listPage.isExpand(application_name)).toBeFalsy();
    // 展开应用
    page.listPage.expandByAppName(application_name);
    expect(page.listPage.isExpand(application_name)).toBeTruthy();

    // 折叠应用
    page.listPage.collapsByAppName(application_name);
    expect(page.listPage.isExpand(application_name)).toBeFalsy();

    // 展开应用
    page.listPage.expandByAppName(application_name);
    expect(
      page.listPage
        .getResourceImage(application_name, 'D', application_name)
        .getText(),
    ).toBe(`镜像:\n${ServerConf.TESTIMAGE}`);

    expect(
      page.listPage
        .getResourceImage(application_name, 'SS', application_name)
        .getText(),
    ).toBe(`镜像:\n${ServerConf.TESTIMAGE}\n共 2 条`);

    expect(
      page.listPage
        .getResourceImage(application_name, 'DS', application_name)
        .getText(),
    ).toBe(`镜像:\n${ServerConf.TESTIMAGE}`);

    expect(page.listPage.getAppStatus(application_name).getText()).toBe('3');
  });

  it('L1:AldDevops-2529: 应用-Deployment详情页-基本信息，容器内容显示正确', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'D', application_name);

    const expectResult = {
      基本信息: {
        名称: application_name,
        类型: 'Deployment',
        更新策略: 'RollingUpdate',
        标签: ['alauda.test: true', `app: ${application_name}`],
        注解: ['deployment.kubernetes.io/revision: 1'],
        访问地址: '-',
      },
      容器: {
        容器: 'qatest',
        镜像: ServerConf.TESTIMAGE,
        端口: 'TCP/30300,TCP/22',
        执行命令: '-',
        内存: ['请求值:10Mi', '限制值:11Mi'],
        CPU: ['请求值:10m', '限制值:11m'],
        参数: '-',
      },
    };

    // 进入到详情页检查显示信息正确
    page.resourceDetailPage.verifyResult(expectResult);
    expect(page.resourceDetailPage.tabs.getText()).toEqual([
      '详情信息',
      'YAML',
      '配置管理',
      '日志',
      '事件',
    ]);
  });

  it('L1:AldDevops-2530: 应用-StatefulSet详情页-查看日志', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'DS', application_name);

    page.resourceDetailPage.viewLog('容器');

    // 容器组，容器自动被选择成对应的选项
    expect(page.detailPage.logViewer.containerGroup.getText()).toContain(
      application_name,
    );
    expect(page.detailPage.logViewer.container.getText()).toBe('qavolume');

    page.detailPage.logViewer.waitLogDisplay('hehe.txt');
    // 验证日志的自动更新功能
    page.detailPage.logViewer.logqueryTime.then(logTime => {
      page.detailPage.logViewer.getToolbarButton('自动更新').check();
      //等待5秒，日志会自动刷新，时间会自动更新成最新的时间
      // browser.sleep(6000);
      page.detailPage.logViewer.waitLogRefresh();
      page.detailPage.logViewer.logqueryTime.then(latestTime => {
        expect(latestTime !== logTime).toBeTruthy();
      });
    });

    // 验证日志的日间，夜间模式
    expect(page.detailPage.logViewer.isNightMode()).toBeFalsy(); // 默认日间模式
    page.detailPage.logViewer.getToolbarButton('自动更新').check();

    // 验证日志的查找
    page.detailPage.logViewer.getToolbarButton('查找').click();
    page.detailPage.logViewer.finder_inputbox.input('hehe.txt');
    page.detailPage.logViewer.resultfinder.getText().then(result => {
      expect(result.trim() !== '无结果');
    });
  });

  xit('L1:AldDevops-2548:L1:Deployment详情页-容器exec功能', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'D', application_name);

    page.resourceDetailPage.exec(application_name);
    page.switchWindow(1);
    expect(page.terminalPage.isGoodConnect).toBeTruthy();
    page.terminalPage.breadcrumb.getText().then(innerText => {
      expect(innerText.includes(application_name)).toBeTruthy();
    });
    page.switchWindow(0);
  });
});
