/**
 * Created by zhangjiao on 2018/3/22.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { ApplicationPage } from '../../../page_objects/project/application/application.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_type_jenkins,
  alauda_type_jenkinsbinding,
  alauda_type_pipeline,
  alauda_type_project,
  alauda_type_secret,
} from '../../../utility/resource.type.k8s';

describe('应用 L1级别UI自动化case', () => {
  const page = new ApplicationPage();
  const project_name = page.getTestData('project-app1-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'the first auto project for application!';

  const application_name_php = page.getTestData('application-php');
  const application_image_php = 'index.alauda.cn/alaudak8s/hello-php';
  const application_image_php_tag = 'latest';
  const application_codeUrl_php =
    'https://github.com/jiaozhang1/hello-world-php.git';

  const application_name_yaml = page.getTestData('application-yaml');

  const application_name_registry = page.getTestData(
    'autoapplication-registry',
  );
  const application_image_registry =
    'index.alauda.cn/alauda/hello-world:latest';
  const application_port_registry = '8091';
  // const application_ports_retistry = 'TCP/8091';
  const application_domain_registry = 'indexzj.com';
  const application_envkey_registry = 'KeyTest';
  const application_envvalue_registry = 'valueTest';
  const application_name_registry_new = page.getTestData(
    'application-registry-new',
  );
  const application_image_registry_new =
    'index.alauda.cn/alauda/hello-world:latest';

  const pipeline_name_api1 = page.getTestData('pipeline-app1');
  const pipeline_name_api2 = page.getTestData('pipeline-app2');
  const image_secret = 'autoalaudak8s-secret';

  const jenkins_name_api1 = page.getTestData('jenkins-');
  const jenkins_host_api1 = 'http://10.96.131.136:8080';

  beforeAll(() => {
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
      },
      alauda_type_project,
      null,
    );
    // 创建一个jenkins,secret,jenkinsBinding
    CommonApi.createResource(
      'alauda.jenkins-secret.yaml',
      {
        '${NAME}': jenkins_name_api1 + '-secret',
        '${JENKINS_NAME}': jenkins_name_api1,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_secret,
      null,
    );
    CommonApi.createResource(
      'alauda.jenkins.yaml',
      {
        '${NAME}': jenkins_name_api1,
        '${JENKINS_NAME}': jenkins_name_api1,
        '${JENKINS_HOST}': jenkins_host_api1,
      },
      alauda_type_jenkins,
      null,
    );
    CommonApi.createResource(
      'alauda.jenkinsbinding.yaml',
      {
        '${NAME}': jenkins_name_api1,
        '${JENKINS_NAME}': jenkins_name_api1,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_jenkinsbinding,
      null,
    );
    CommonApi.createResource(
      'alauda.image-secret.yaml',
      {
        '${NAME}': image_secret,
        '${SECRET_NAME}': image_secret,
        '${NAMESPACE}': project_name,
      },
      alauda_type_secret,
      null,
    );
    CommonApi.createResource(
      'alauda.application-pipeline.yaml',
      {
        '${NAME}': pipeline_name_api1,
        '${JENKINS_NAME}': jenkins_name_api1,
        '${NAMESPACE_NAME}': project_name,
        '${PIPELINE_NAME}': pipeline_name_api1,
        '${APPLICATION_NAME}': application_name_registry_new,
      },
      alauda_type_pipeline,
      null,
    );

    CommonApi.createResource(
      'alauda.application-pipeline.yaml',
      {
        '${NAME}': pipeline_name_api2,
        '${JENKINS_NAME}': jenkins_name_api1,
        '${NAMESPACE_NAME}': project_name,
        '${PIPELINE_NAME}': pipeline_name_api2,
        '${APPLICATION_NAME}': application_name_registry_new,
      },
      alauda_type_pipeline,
      null,
    );
    page.enterProjectDashboard();
    page.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(jenkins_name_api1, alauda_type_jenkins, null);
  });

  it('L1:Ald-2293:创建应用-yaml创建', () => {
    page.clickLeftNavByText('应用');
    page.createAppByYaml(
      application_name_yaml,
      application_name_yaml,
      ServerConf.TESTIMAGE,
    );

    // 创建后跳转到详情页，验证是否创建成功
    const expectResult = {
      基本信息: {
        name: application_name_yaml,
        名称: application_name_yaml,
        访问地址: '-',
        资源状态: '3',
      },
      'Deployment (1 个)': {
        name: 'qatest',
        容器: 'qatest',
        镜像: ServerConf.TESTIMAGE,
        端口: 'TCP/30300,TCP/22',
        内存: ['请求值:10Mi', '限制值:11Mi'],
        CPU: ['请求值:10m', '限制值:11m'],
      },
      'StatefulSet (1 个)': {
        name: 'qatest',
        容器: 'qatest',
        镜像: ServerConf.TESTIMAGE,
        端口: 'TCP/80',
        内存: ['请求值:10Mi', '限制值:11Mi'],
        CPU: ['请求值:10m', '限制值:11m'],
      },
      'DaemonSet (1 个)': {
        name: 'qavolume',
        容器: 'qavolume',
        镜像: ServerConf.TESTIMAGE,
        端口: 'TCP/80',
        内存: ['请求值:10Mi', '限制值:11Mi'],
        CPU: ['请求值:10m', '限制值:11m'],
      },
    };
    page.verifyResult(expectResult);
    expect(
      page.detailPage.getPodScaler('Deployment (1 个)').count.isPresent(),
    ).toBeTruthy();
  });

  /**
   * 包含了应用详情页的检查
   */
  it('L1:AldDevops-2394:应用详情页-流水线列表-点击流水线名称到流水线详情页', () => {
    const testData = {
      应用名称: application_name_php,
      // 镜像设置
      镜像仓库地址: application_image_php,
      镜像版本: application_image_php_tag,

      // 流水线设置
      'Jenkins 服务': jenkins_name_api1,
      代码仓库地址: application_codeUrl_php,
    };

    const expectResult = {
      基本信息: {
        name: application_name_php,
        名称: application_name_php,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: application_name_php,
        容器: application_name_php,
        镜像: `${application_image_php}:${application_image_php_tag}`,
        端口: 'TCP/8080',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.createAppByTemplate(testData, 'PHP');
    expect(page.toast.getMessage()).toBe(
      `应用 ${application_name_php} 创建成功`,
    );
    page.toast.waitDisappear();
    // 模板创建应用的详情页检查
    page.verifyResult(expectResult);

    // 检查详情页中的流水线历史数据是否正确
    page.detailPage.clickTabByName('流水线');
    expect(page.detailPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '执行记录',
      '触发器',
      '',
    ]);

    // 点击流水线名称到流水线详情页
    page.detailPage.resourceTable.clickResourceNameByRow([
      application_name_php,
    ]);

    // 判断是否到流水线详情页
    // TODO
    // const expectPiplineResult = {
    //   基本信息: {
    //     name: application_name_php,
    //     名称: application_name_php,
    //     // 流水线名称: application_name_php,
    //     // 代码仓库: application_codeUrl_php,
    //     // 触发器: '代码仓库',
    //     // 所属应用: application_name_php,
    //   },
    // };
    // verifyResult(expectPiplineResult);
  });

  it('L1: Ald-2312:创建应用-镜像创建', () => {
    page.clickLeftNavByText('应用');
    page.getButtonByText('创建应用').click();
    page.clickMiniCardByInnerText('镜像创建'); // 点击镜像创建

    expect(
      page.createPage_inputbox.checkInputboxIsPresent('名称'),
    ).toBeTruthy(); // 判断页面元素是否出现
    page.createPage_inputbox.inputByText('名称', application_name_registry);
    page.createPage_inputbox.inputByText(
      '镜像地址',
      application_image_registry,
    );
    page.createPage_port_inputbox.input(application_port_registry);
    page.createPage_inputbox.inputByText('域名', application_domain_registry);
    page.createPage_env_inputbox.input_index(application_envkey_registry, 0);
    page.createPage_env_inputbox.input_index(application_envvalue_registry, 1);
    page.createPage_createButton.click(); // 点击创建按钮
    browser.sleep(1000);
    page.toast.waitDisappear();

    // 创建后跳转到详情页，验证是否创建成功
    const expectResult = {
      基本信息: {
        name: application_name_registry,
        名称: application_name_registry,
        访问地址: `http://${application_domain_registry}`,
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: application_name_registry,
        容器: application_name_registry,
        镜像: application_image_registry,
        端口: 'TCP/8091',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.verifyResult(expectResult);
    expect(page.detailPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '类型',
    ]);
  });

  it('L1:AldDevops-2375:创建应用-镜像创建-只输入必填项创建应用', () => {
    page.getButtonByText('创建应用').click();
    page.clickMiniCardByInnerText('镜像创建'); // 点击镜像创建

    expect(
      page.createPage_inputbox.checkInputboxIsPresent('名称'),
    ).toBeTruthy(); // 判断页面元素是否出现
    page.createPage_inputbox.inputByText('名称', application_name_registry_new);
    page.createPage_inputbox.inputByText(
      '镜像地址',
      application_image_registry_new,
    );
    page.createPage_createButton.click(); // 点击创建按钮
    browser.sleep(1000);

    // 创建后跳转到详情页，验证是否创建成功
    const expectResult = {
      基本信息: {
        name: application_name_registry_new,
        名称: application_name_registry_new,
        访问地址: `-`,
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: application_name_registry_new,
        容器: application_name_registry_new,
        镜像: application_image_registry_new,
        端口: '',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.verifyResult(expectResult);
  });

  it('L1:AldDevops-2345:应用详情页-流水线列表-验证列表数据是否正确', () => {
    page.clickLeftNavByText('应用'); // 点击左侧应用导航
    page.listPage.clickByAppName(application_name_registry_new); // 点击列表页的应用名称

    page.detailPage.clickTabByName('流水线');

    // 检查详情页中的流水线历史数据是否正确
    expect(page.detailPage.resourceTable.getRowCount()).toBe(2);
    expect(page.detailPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '执行记录',
      '触发器',
      '',
    ]);
  });

  it('L1:AldDevops-2381:应用详情页-流水线列表-删除流水线', () => {
    page.clickLeftNavByText('应用'); // 点击左侧应用导航
    page.listPage.clickByAppName(application_name_registry_new); // 点击列表页的应用名称
    page.detailPage.clickTabByName('流水线');

    // 检查详情页中的流水线历史是否有要删除的流水线
    expect(page.detailPage.resourceTable.getRowCount()).toBe(2);
    page.detailPage.resourceTable.clickOperationButtonByRow(
      [pipeline_name_api2],
      '删除',
    );
    page.detailPage.confirmDialog.clickConfirm();
    expect(page.toast.getMessage()).toBe('流水线删除成功');
    page.toast.waitDisappear();
    // page.confirmDialog.clickConfirm();
    expect(page.detailPage.resourceTable.getRowCount()).toBe(1); // 删掉一条数据后，列表有一条数据
  });

  it('L1:AldDevops-2528: 应用详情页-容器日志功能', () => {
    page.clickLeftNavByText('应用');
    page.listPage.clickByAppName(application_name_yaml);
    page.detailPage.viewLog('Deployment (1 个)', 'qatest');
    // 验证资源名称，容器组，容器自动被选择成对应的选项
    expect(page.detailPage.logViewer.resourceName.getText()).toContain(
      `${application_name_yaml}`,
    );
    expect(page.detailPage.logViewer.containerGroup.getText()).toContain(
      application_name_yaml,
    );
    expect(page.detailPage.logViewer.container.getText()).toBe('qatest');
    page.detailPage.clickTabByName('详情信息');
    page.detailPage.viewLog('StatefulSet (1 个)', 'qatest1');
    // 验证资源名称，容器组，容器自动被选择成对应的选项
    expect(page.detailPage.logViewer.resourceName.getText()).toContain(
      `${application_name_yaml}`,
    );
    expect(page.detailPage.logViewer.containerGroup.getText()).toContain(
      application_name_yaml,
    );
    expect(page.detailPage.logViewer.container.getText()).toBe('qatest1');

    page.detailPage.logViewer.waitLogDisplay('hehe.txt');
    // 验证日志的自动更新功能
    page.detailPage.logViewer.logqueryTime.then(logTime => {
      page.detailPage.logViewer.getToolbarButton('自动更新').check();
      //等待5秒，日志会自动刷新，时间会自动更新成最新的时间
      browser.sleep(6000);
      page.detailPage.logViewer.logqueryTime.then(latestTime => {
        expect(latestTime !== logTime).toBeTruthy();
      });
    });

    // 验证日志的日间，夜间模式
    expect(page.detailPage.logViewer.isNightMode()).toBeFalsy(); // 默认日间模式
    page.detailPage.logViewer.getToolbarButton('自动更新').check();

    // 验证日志的查找
    page.detailPage.logViewer.getToolbarButton('查找').click();
    page.detailPage.logViewer.finder_inputbox.input('hehe.txt');
    page.detailPage.logViewer.resultfinder.getText().then(result => {
      expect(result.trim() !== '无结果');
    });
  });

  it('L1: AldDevops-2488:应用详情页-容器exec功能', () => {
    page.clickLeftNavByText('应用');
    page.listPage.clickByAppName(application_name_registry);
    page.detailPage.exec('Deployment (1 个)', application_name_registry);
    page.switchWindow(1);
    expect(page.terminalPage.isGoodConnect).toBeTruthy();
    page.terminalPage.breadcrumb.getText().then(innerText => {
      expect(innerText.includes(application_name_registry)).toBeTruthy();
    });
    page.switchWindow(0);
  });

  it('L1:AldDevops-2378:应用详情页-增减POD', () => {
    page.listPage.clickByAppName(application_name_registry); // 点击列表页的应用名称

    const scaler = page.detailPage.getPodScaler('Deployment (1 个)');

    // 应用详情页，检查当前POD数
    expect(scaler.count.getText()).toBe('1');

    // 应用详情页，POD 扩容容到1
    scaler.plus();
    CommonPage.waitElementTextChangeTo(scaler.count.getText(), '2');
    expect(scaler.count.getText()).toBe('2');

    // 应用详情页，POD 缩容容容到1
    scaler.mins();
    CommonPage.waitElementTextChangeTo(scaler.count.getText(), '1');
    expect(scaler.count.getText()).toBe('1');

    // 应用详情页，POD 缩容容容到0, 有确认对话框
    scaler.mins();
    expect(page.detailPage.confirmDialog.confirmTitle.getText()).toBe(
      `确定要将${application_name_registry}缩容到0吗？`,
    );
    page.detailPage.confirmDialog.clickConfirm();
    CommonPage.waitElementTextChangeTo(scaler.count.getText(), '0');
    expect(scaler.count.getText()).toBe('0');
  });
  /**
    it('L1:Ald-2377:应用详情页-YAML更新', () => {
        page.listPage_applicationName(project_name_api1, application_name_registry).clickNameInlistPage();  // 点击列表页的应用名称
        expect(page.detailPage_yamlUpdate_link.checkButtonIsPresent()).toBeTruthy();
        page.detailPage_yamlUpdate_link.click();  // 点击YAML更新link
        // 判断是否打开YAML更新页
        expect(page.updateYaml_dialog.checkTextIsPresent()).toBeTruthy();
        expect(page.updateYaml_updateButton.checkButtonIsPresent()).toBeTruthy();
        expect(page.updateYaml_dialog.getText()).toBe('Yaml 更新');
        page.updateYaml_updateContent('replicas: 5');
        page.updateYaml_updateButton.click();
        // page.updateYaml_update_result.getTextAllMap().then((a) => {
        //     const isFailed = a.find(item => item.indexOf('更新失败'));
        // });
        page.updateYaml_knowButton.click();
        CommonPage.waitElementTextChangeTo(page.detailPage_pod_count.getText(), '5');
        expect(page.detailPage_pod_count.getText()).toBe('5');

        page.detailPage_yamlUpdate_link.click();  // 点击YAML更新link
        browser.sleep(1000);
        expect(page.updateYaml_dialog.checkTextIsPresent()).toBeTruthy();
        expect(page.updateYaml_updateButton.checkButtonIsPresent()).toBeTruthy();
        page.updateYaml_updateContent('replicas: 1');
        page.updateYaml_addContainers('containers:\n          - name: bbbb\n            image: '
        + application_image_php); // 更新yaml，增加container
        page.updateYaml_updateButton.click();
        page.updateYaml_knowButton.click();
        CommonPage.waitElementTextChangeTo(page.detailPage_pod_count.getText(), '2');
        expect(page.detailPage_pod_count.getText()).toBe('2');
        expect(page.detailPage_containers_Content(1).getElementByText('容器').getText()).toBe('bbbb');
        expect(page.detailPage_containers_Content(1).getElementByText('Image').getText()).toBe(application_image_php);
        expect(page.detailPage_containers_Content(2).getElementByText('容器').getText()).toBe(application_name_registry);
        expect(page.detailPage_containers_Content(2).getElementByText('Image').getText()).toBe(application_image_registry);
    }); */

  it('L1:AldDevops-2407:应用详情页-删除应用', () => {
    page.listPage.clickByAppName(application_name_registry); // 点击列表页的应用名称
    page.detailPage.option.select_item('删除应用'); // 点击删除按钮
    page.confirmDialog.clickConfirm(); // 确认框点击删除按钮
    browser.refresh();
    page.clickLeftNavByText('应用');
    // 删除后返回列表页
    page.listPage.searchAppByName(application_name_registry, 0);
    expect(page.listPage.isExist(application_name_registry)).toBeFalsy();
  });
});
