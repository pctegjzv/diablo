/**
 * Created by zhangjiao on 2018/3/22.
 */

import { browser } from 'protractor';

import { AlaudaInputbox } from '../../../element_objects_new/alauda.inputbox';
import { ApplicationPage } from '../../../page_objects/project/application/application.page';
import { CommonApi } from '../../../utility/common.api';
import {
  alauda_type_deploy,
  alauda_type_jenkins,
  alauda_type_project,
  k8s_type_namespaces,
} from '../../../utility/resource.type.k8s';

describe('应用 L2级别UI自动化case', () => {
  const page = new ApplicationPage();

  const project_name = page.getTestData('project-app2-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'the second auto project for application!';

  const application_name_api1 = 'autoapplication-api1';
  const application_name_api2 = 'autoapplication-api2';
  const application_name_api = 'autoapplication-api';

  const application_container_api1 = 'autocontainer-api1';
  const application_container_api2 = 'autocontainer-api2';
  const application_container_api = 'autocontainer-api';

  const jenkins_name_api = 'autojenkinsbinding-app2';

  beforeAll(() => {
    CommonApi.createResource(
      'alauda.namespace-project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
      },
      alauda_type_project,
      null,
    );
    browser.sleep(2000);

    CommonApi.createResource(
      'alauda.deployment.yaml',
      {
        '${NAME}': application_name_api1,
        '${APP_NAME}': application_name_api1,
        '${CONTAINER_NAME}': application_container_api1,
        '${PROJECT}': project_name,
      },
      alauda_type_deploy,
      project_name,
    );
    browser.sleep(2000);

    CommonApi.createResource(
      'alauda.deployments.yaml',
      {
        '${NAME}': application_name_api2,
        '${APP_NAME}': application_name_api2,
        '${CONTAINER_NAME}': application_container_api,
        '${PROJECT}': project_name,
      },
      alauda_type_deploy,
      project_name,
    );
    page.enterProjectDashboard();
    page.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.clickLeftNavByText('应用'); // 点击左侧导航
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, k8s_type_namespaces, null);
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(jenkins_name_api, alauda_type_jenkins, null);
  });

  it('L2:AldDevops-2376:创建应用-点击创建应用后的几种创建模式的mini-card的检查', () => {
    page.getButtonByText('创建应用').click();

    // 检查4个mini-card是不是 YAML创建 模板创建 镜像创建
    expect(page.getMiniCatdText()).toEqual([
      'YAML 创建',
      '模板创建',
      '镜像创建',
    ]);

    page.clickMiniCardByInnerText('模板创建'); // 点击模板创建
    // 检查4个mini-card是不是Java Golang Python PHP
    expect(page.getMiniCatdText()).toEqual(['Java', 'Golang', 'Python', 'PHP']);

    page.templateCreatePage.back(); // 点击返回按钮
    expect(page.getMiniCatdText()).toEqual([
      'YAML 创建',
      '模板创建',
      '镜像创建',
    ]);
    page.templateCreatePage.close(); // 选择模式dialog点击‘关闭’
    expect(page.templateCreatePage.buttonCloase.isPresent()).toBeFalsy();
  });

  it('L2:AldDevops-2298:应用列表-按名称搜索', () => {
    page.clickLeftNavByText('应用');
    // expect(page.listPage.searchBox.isPresent()).toBeTruthy(); // 检查搜索框是否存在

    page.listPage.searchAppByName('autoapplication_bucunzai', 0).then(() => {
      expect(page.listPage.appCount).toBe(0);
    }); // 在搜索框输入不存在的项目名称

    page.listPage.searchAppByName(application_name_api1, 1).then(() => {
      expect(page.listPage.isExist(application_name_api1)).toBeTruthy();
      expect(page.listPage.appCount).toBe(1);
    }); // 在搜索框输入要检索的应用名称

    page.listPage.searchAppByName(application_name_api, 2).then(() => {
      expect(page.listPage.isExist(application_name_api1)).toBeTruthy();
      expect(page.listPage.isExist(application_name_api2)).toBeTruthy();
      expect(page.listPage.appCount).toBe(2);
    }); // // 在搜索框输入模糊查询的应用名称

    page.listPage.clickByAppName(application_name_api2);
    browser.sleep(1000);
    // 创建后跳转到详情页，验证Yaml创建的Application是否创建成功
    const expectResult = {
      基本信息: {
        name: application_name_api2,
        名称: application_name_api2,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: application_container_api1,
        容器: application_container_api1,
        镜像: `danielfbm/gitea:zeon1`,
        端口: 'TCP/30300,TCP/22',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.verifyResult(expectResult);
    const expectResult1 = {
      'Deployment (1 个)': {
        name: application_container_api2,
        容器: application_container_api2,
        镜像: `danielfbm/gitea:zeon2`,
        端口: 'TCP/30301,TCP/22',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.verifyResult(expectResult1);
  });

  it('L2:AldDevops-2393:创建应用-模板创建-PHP-表单的合法性校验', () => {
    page.clickLeftNavByText('应用'); // 点击左侧应用导航
    page.getButtonByText('创建应用').click();
    page.clickMiniCardByInnerText('模板创建'); // 点击模板创建
    page.clickMiniCardByInnerText('PHP'); // 点击PHP创建

    expect(
      page.createPage_inputbox.checkInputboxIsPresent('应用名称'),
    ).toBeTruthy(); // 判断页面元素是否出现
    page.createPage_createButton.click(); // 点击“创建”按钮
    expect(page.getErrorHint('应用名称').getText()).toBe('必填项');
    expect(page.getErrorHint('镜像仓库地址').getText()).toBe('必填项');
    expect(page.getErrorHint('Jenkins 服务').getText()).toBe('必填项');
  });

  it('L2:Ald-2303:创建应用-yaml创建-表单的合法性校验', () => {
    page.clickLeftNavByText('应用'); // 点击左侧应用导航
    page.getButtonByText('创建应用').click();
    page.clickMiniCardByInnerText('YAML 创建'); // 点击YAML创建
    expect(page.createPage_yaml_textarea.checkEditorIsPresent()).toBeTruthy();
    expect(page.createPage_createButton.checkButtonIsPresent()).toBeTruthy();
    page.createPage_createButton.click(); // 点击“创建”按钮

    // 提示Yaml格式不正常，无法找到应用名称
    expect(page.createPage_yaml_assertive.checkTextIsPresent()).toBeTruthy();
  });

  it('L2:AldDevops-2379:创建应用-镜像创建-表单的合法性校验', () => {
    page.clickLeftNavByText('应用'); // 点击左侧应用导航
    page.getButtonByText('创建应用').click();
    page.clickMiniCardByInnerText('镜像创建'); // 点击模板创建
    expect(
      page.createPage_inputbox.checkInputboxIsPresent('名称'),
    ).toBeTruthy(); // 判断页面元素是否出现
    page.createPage_createButton.click(); // 点击“创建“按钮 提示必填项
    expect(page.getErrorHint('名称').getText()).toBe('必填项');
    expect(page.getErrorHint('镜像地址').getText()).toBe('必填项');

    const inputbox = new AlaudaInputbox(page.getElementByText('名称'));
    inputbox.input('AUTO-APPLICATIONNAME'); // 输入大写字母
    expect(page.getErrorHint('名称').getText()).toBe('格式错误');

    inputbox.input('auto_applicationname'); // 输入带有下划线
    expect(page.getErrorHint('名称').getText()).toBe('格式错误');
  });

  it('L2:AldDevops-2304:创建应用-取消', () => {
    page.clickLeftNavByText('应用'); // 点击左侧应用导航
    page.getButtonByText('创建应用').click(); // 点击“创建应用”按钮
    page.clickMiniCardByInnerText('镜像创建'); // 点击镜像创建
    page.createPage_cancelButton.click(); // 应用创建页点击“取消”按钮
    //验证创建页面关闭
    expect(
      page.getButtonByText('创建应用').checkButtonIsPresent(),
    ).toBeTruthy();

    page.getButtonByText('创建应用').click(); // 点击“创建应用”按钮
    page.clickMiniCardByInnerText('YAML 创建'); // 点击YAML创建
    browser.sleep(1000);
    expect(page.createPage_yaml_textarea.checkEditorIsPresent()).toBeTruthy();
    expect(page.createPage_cancelButton.checkButtonIsPresent()).toBeTruthy();
    page.createPage_cancelButton.click(); // YAML创建页点击“取消”按钮
    //验证创建页面关闭
    expect(
      page.getButtonByText('创建应用').checkButtonIsPresent(),
    ).toBeTruthy();

    page.getButtonByText('创建应用').click(); // 点击“创建应用”按钮
    page.clickMiniCardByInnerText('模板创建'); // 点击YAML创建
    page.templateCreatePage.clickMiniCardByInnerText('PHP'); // 点击YAML创建
    expect(page.getButtonByText('创建').checkButtonIsPresent()).toBeTruthy();
    page.createPage_cancelButton.click(); // 模板创建页点击“取消”按钮
    //验证创建页面关闭
    expect(
      page.getButtonByText('创建应用').checkButtonIsPresent(),
    ).toBeTruthy();
  });
});
