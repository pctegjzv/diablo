/**
 * Created by liuwei on 2018/8/7.
 */

// import { browser } from 'protractor';

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { ApplicationPage } from '../../../page_objects/project/application/application.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('AppCore 更新容器 L1级别UI自动化case', () => {
  const page = new ApplicationPage();
  const project_name = page.getTestData('autoproject-app1-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'the first auto project for application!';

  const application_name = page.getTestData('autoapplication-yaml');

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'alauda.appcore.yaml',
      {
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
        '${NAME}': application_name,
        '${LABEL}': application_name,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.appcore.volumeMount',
    );
    page.enterProjectDashboard();
    page.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(`kubectl delete project ${project_name}`);
  });
  it('L2:AldDevops-2564: StatefulSet 详情页，更新容器页面，更新容器大小', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击StatefulSet名称, 进入StatefulSet 的详细页
    page.listPage.clickByResourceName(application_name, 'SS', application_name);
    page.resourceDetailPage.tabs.click('qatest1');
    page.resourceDetailPage.action('容器', '更新容器');
    page.containerUpdatePage.updateContainerSize({
      内存请求值: [10, 'Mi'],
      内存限制值: [11, 'Mi'],
      CPU请求值: [10, 'm'],
      CPU限制值: [11, 'm'],
    });
    page.containerUpdatePage.getButtonByText('更新').click();
    // 验证更新容器的dialog 的内容提示正确
    expect(page.containerUpdatePage.auiDialog.content.getText()).toBe(
      `更新后，${application_name} 将重新部署`,
    );
    // 单击确定按钮
    page.containerUpdatePage.auiDialog.clickConfirm();
    page.containerUpdatePage.toast.getMessage().then(message => {
      console.log(message);
    });

    // 页面返回详情页，单击【qatest1】容器 tab
    page.resourceDetailPage.tabs.click('qatest1');
    // 验证容器的信息显示正确
    let expectResult = {
      容器: {
        镜像: ServerConf.TESTIMAGE,
        端口: 'TCP/80',
        执行命令: '-',
        内存: ['请求值:10Mi', '限制值:11Mi'],
        CPU: ['请求值:10m', '限制值:11m'],
        参数: '-',
      },
    };
    page.resourceDetailPage.verifyResult(expectResult);

    // 页面返回详情页，单击【qatest1】容器 tab
    page.resourceDetailPage.tabs.click('qatest');
    // 验证容器的信息显示正确
    expectResult = {
      容器: {
        镜像: ServerConf.TESTIMAGE,
        端口: 'TCP/80',
        执行命令: '-',
        内存: ['请求值:10Mi', '限制值:11Mi'],
        CPU: ['请求值:10m', '限制值:11m'],
        参数: '-',
      },
    };
    page.resourceDetailPage.verifyResult(expectResult);
  });

  it('L2:AldDevops-2565: AldDevops-2564: Depliyment 详情页，更新容器页面，更新环境变量', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'D', application_name);
    page.resourceDetailPage.action('容器', '更新容器');

    // 单击添加按钮，更新环境变量
    page.containerUpdatePage.env.click('添加');
    page.containerUpdatePage.env.fill([
      // 第一个环境变量
      'qatest1',
      '11111',
      'false',
      // 第2个环境变量
      'qatest2',
      '22222',
      'false',
    ]);

    // 单击更新按钮，在dialog 页单击确定按钮
    page.containerUpdatePage.getButtonByText('更新').click();
    // 验证更新容器的dialog 的内容提示正确
    expect(page.containerUpdatePage.auiDialog.content.getText()).toBe(
      `更新后，${application_name} 将重新部署`,
    );
    // 单击确定按钮
    page.containerUpdatePage.auiDialog.clickConfirm();
    page.containerUpdatePage.toast.getMessage().then(message => {
      console.log(message);
    });

    // 详情页点击配置管理tab
    page.resourceDetailPage.tabs.click('配置管理');

    // 验证配置管理更新成功
    page.resourceDetailPage.configManager
      .getAuiCardByName('环境变量')
      .then(auicard => {
        // 验证title 是环境变量
        expect(auicard.title.getText()).toBe('环境变量');
        // 验证表格的header 显示正确
        expect(auicard.auiTable.getHeaderText()).toEqual(['名称', '值']);
        // 验证更新的环境变量在配置管理下的环境变量表格里面显示
        expect(auicard.auiTable.getRowByIndex(0).getText()).toBe(
          'qatest1\n11111',
        );
        expect(auicard.auiTable.getRowByIndex(1).getText()).toBe(
          'qatest2\n22222',
        );
      });
  });

  xit('L2:AldDevops-2566: StatefulSet 详情页，更新容器页面，更新环境变量引用方式', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击StatefulSet名称, 进入StatefulSet 的详细页
    page.listPage.clickByResourceName(application_name, 'SS', application_name);
    page.resourceDetailPage.tabs.click('配置管理');
    // 在弹出的dialog 页面上修改环境变量
    page.resourceDetailPage.configManager
      .getAuiCardByName('环境变量', 'qatest')
      .then(auiCard => {
        auiCard.click();
        browser.sleep(100);
        // 单击添加按钮，更新环境变量
        auiCard.env.click('引用');
        auiCard.env.fill([
          // 第一个环境变量
          'HTTP_PORT',
          '30603',
          'false',
          // 第2个环境变量
          'qatest1',
          ['配置字典', application_name],
          'app.ini',
        ]);
      });

    // 单击确定按钮
    page.containerUpdatePage.auiDialog.clickConfirm();
    page.containerUpdatePage.toast.getMessage().then(message => {
      console.log(message);
    });

    // 验证配置管理更新成功
    page.resourceDetailPage.configManager
      .getAuiCardByName('环境变量', 'qatest')
      .then(auicard => {
        // 验证更新的环境变量在配置管理下的环境变量表格里面显示
        expect(auicard.auiTable.getRowByIndex(0).getText()).toBe(
          'HTTP_PORT\n30603',
        );
        expect(auicard.auiTable.getRowByIndex(1).getText()).toBe(
          `qatest1\n配置字典 ${application_name}: app.ini`,
        );
      });
  });

  it('L2:AldDevops-2567: DaemonSet 详情页，更新容器页面，更新配置引用', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击StatefulSet名称, 进入StatefulSet 的详细页
    page.listPage.clickByResourceName(application_name, 'DS', application_name);
    page.resourceDetailPage.action('容器', '更新容器');
    // 在更新容器页面上修改配置引用
    page.containerUpdatePage.config.select('配置字典', application_name);
    page.containerUpdatePage.config.select('保密字典', application_name);

    // 单击更新按钮
    page.containerUpdatePage.getButtonByText('更新').click();

    // 单击确定按钮
    page.containerUpdatePage.auiDialog.clickConfirm();
    page.containerUpdatePage.toast.getMessage().then(message => {
      console.log(message);
    });

    // 单击配置管理tab
    page.resourceDetailPage.tabs.click('配置管理');

    // 验证配置管理更新成功
    page.resourceDetailPage.configManager
      .getAuiCardByName('配置引用')
      .then(auicard => {
        // 验证更新的配置引用在配置管理下的配置引用表格里面显示
        expect(auicard.envTags).toEqual([
          `配置字典 ${application_name}`,
          `保密字典 ${application_name}`,
        ]);
      });
  });

  it('L2:AldDevops-2568: 详情页，更新容器页面，更新挂载存储', () => {
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'DS', application_name);
    page.resourceDetailPage.action('容器', '更新容器');

    // 更新容器页面打开添加存储卷挂载的dialog 页面
    page.containerUpdatePage.mountVolumeButton.click();

    const containerPath = '/var/temp/test';
    const containerPath1 = '/var/temp/test1';
    //获得volumeMount page
    const volumeMountPage = page.containerUpdatePage.volumeMont;
    // 单击保密字典tab
    volumeMountPage.volumeType.clickByName('保密字典');
    // 在保密字典中选择一个保密字典
    volumeMountPage.secret.select(application_name);
    // 打开单独引用
    volumeMountPage.switch.open();
    // 输入值
    volumeMountPage.keyValueForm.newValue(['username', containerPath]);
    volumeMountPage.keyValueForm.buttonAdd.click();
    volumeMountPage.keyValueForm.newValue(['password', containerPath1]);
    // 单击添加按钮
    volumeMountPage.buttonAdd.click();

    // 在更新页面单击更新按钮
    page.containerUpdatePage.getButtonByText('更新').click();
    // 单击确定按钮
    page.containerUpdatePage.auiDialog.clickConfirm();
    page.containerUpdatePage.toast.getMessage().then(message => {
      console.log(message);
    });

    // 验证添加成功
    expect(page.resourceDetailPage.tableVolumeMount.getHeaderText()).toEqual([
      '类型',
      '名称/路径',
      '容器路径',
    ]);
    expect(
      page.resourceDetailPage.tableVolumeMount.getRowByIndex(1).getText(),
    ).toBe(
      `保密字典 volume-0\n键\n文件路径\nusername ${containerPath}\npassword ${containerPath1}`,
    );
  });
});
