/**
 * Created by zhangjiao on 2018/3/22.
 */
import { ApplicationPage } from '../../../page_objects/project/application/application.page';
import { CommonApi } from '../../../utility/common.api';
import {
  alauda_type_jenkins,
  alauda_type_jenkinsbinding,
  alauda_type_project,
  alauda_type_secret,
} from '../../../utility/resource.type.k8s';

describe('应用 L0级别UI自动化case', () => {
  const page = new ApplicationPage();

  const project_name = page.getTestData('project-app1-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'the first auto project for application!';

  const hello_label = 'all';

  // onst application_ports = 'TCP/8080';
  const app_name_java_default = page.getTestData('java-default-');
  const app_name_java = page.getTestData('java-');
  const application_imageAddress_java = 'index.alauda.cn/alaudak8s/hello-java';
  const application_imageTag_java = 'latest';
  const application_image_java = 'index.alauda.cn/alaudak8s/hello-java:latest';
  const application_codeUrl_java =
    'https://github.com/jiaozhang1/hello-world-java.git';
  const application_buildDir_java = 'examples/test-app-gradle/';

  const app_name_golang_default = page.getTestData('golang-default');
  const app_name_golang = page.getTestData('golang');
  const application_imageAddress_golang =
    'index.alauda.cn/alaudak8s/hello-golang';
  const application_imageTag_golang = 'latest';
  const application_image_golang =
    'index.alauda.cn/alaudak8s/hello-golang:latest';
  const application_codeUrl_golang =
    'https://github.com/jiaozhang1/hello-world-golang.git';
  const application_buildDir_golang = 'examples/import-with-vendor-app/app/';
  const application_startDir_golang = 'examples/import-with-vendor-app/app/';

  const app_name_python_default = page.getTestData('python-default-');
  const app_name_python = page.getTestData('python-');
  const application_imageAddress_python =
    'index.alauda.cn/alaudak8s/hello-python';
  const application_imageTag_python = 'latest';
  const application_image_python =
    'index.alauda.cn/alaudak8s/hello-python:latest';
  const application_codeUrl_python =
    'https://github.com/jiaozhang1/hello-world-python.git';
  const application_buildDir_python = 'examples/pipenv-test-app/';

  const app_name_php_default = page.getTestData('php-default');
  const app_name_php = page.getTestData('php-');
  const application_imageAddress_php = 'index.alauda.cn/alaudak8s/hello-php';
  const application_imageTag_php = 'latest';
  const application_image_php = 'index.alauda.cn/alaudak8s/hello-php:latest';
  const application_codeUrl_php =
    'https://github.com/jiaozhang1/hello-world-php.git';
  const application_buildDir_php = 'examples/test-app/';

  const application_shellScript_sh = 'app.sh';
  const application_pythonScript_sh = 'app.py';
  const application_gunicorModule =
    'APP_MODULE=$(MODULE_NAME):$(VARIABLE_NAME)';
  const image_secret = 'autoalaudak8s-secret';

  const jenkins_name_api = page.getTestData('jenkins-');
  const jenkins_host_api1 = 'http://10.96.131.136:8080';

  beforeAll(() => {
    CommonApi.createResource(
      'alauda.project.yaml',
      {
        '${NAME}': project_name,
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
      },
      alauda_type_project,
      null,
    );

    // 创建一个jenkins,secret,jenkinsBinding
    CommonApi.createResource(
      'alauda.jenkins-secret.yaml',
      {
        '${NAME}': jenkins_name_api + '-secret',
        '${JENKINS_NAME}': jenkins_name_api,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_secret,
      null,
    );
    CommonApi.createResource(
      'alauda.jenkins.yaml',
      {
        '${NAME}': jenkins_name_api,
        '${JENKINS_NAME}': jenkins_name_api,
        '${JENKINS_HOST}': jenkins_host_api1,
      },
      alauda_type_jenkins,
      null,
    );

    CommonApi.createResource(
      'alauda.jenkinsbinding.yaml',
      {
        '${NAME}': jenkins_name_api,
        '${JENKINS_NAME}': jenkins_name_api,
        '${PROJECT_NAME}': project_name,
      },
      alauda_type_jenkinsbinding,
      null,
    );
    CommonApi.createResource(
      'alauda.image-secret.yaml',
      {
        '${NAME}': image_secret,
        '${SECRET_NAME}': image_secret,
        '${NAMESPACE}': project_name,
      },
      alauda_type_secret,
      null,
    );
    page.enterProjectDashboard();
    page.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });

  afterAll(() => {
    CommonApi.deleteResource(project_name, alauda_type_project, null);
    CommonApi.deleteResource(jenkins_name_api, alauda_type_jenkins, null);
  });

  it('L1:AldDevops-2395:创建应用-模版创建-Java-全部使用默认选项', () => {
    const testData = {
      应用名称: app_name_java_default,
      // 镜像设置
      镜像仓库地址: application_imageAddress_java,
      镜像版本: application_imageTag_java,

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
      代码仓库地址: application_codeUrl_java,
      构建目录: application_buildDir_java,
    };

    const expectResult = {
      基本信息: {
        name: app_name_java_default,
        名称: app_name_java_default,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_java_default,
        // 容器: app_name_java_default,
        镜像: application_image_java,
        端口: 'TCP/8080',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.createAppByTemplate(testData, 'Java');
    expect(page.toast.getMessage()).toBe(
      `应用 ${app_name_java_default} 创建成功`,
    );
    page.toast.waitDisappear();
    // 创建后返回到详情页检查是否创建成功
    page.verifyResult(expectResult);
    // 检查详情页中的流水线历史数据是否正确
    // TODO
  });

  it('L1:AldDevops-2398:创建应用-模版创建-Java-改变默认选项', () => {
    const testData = {
      应用名称: app_name_java,
      // 镜像设置
      镜像仓库地址: application_imageAddress_java,
      镜像版本: application_imageTag_java,
      镜像源证书: '不使用',

      // 网络设置
      访问端口: '9000',

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
      'Jenkins 节点标签': hello_label,
      代码仓库地址: application_codeUrl_java,
      构建目录: application_buildDir_java,
      代码仓库证书: '不使用',
    };

    const expectResult = {
      基本信息: {
        name: app_name_java,
        名称: app_name_java,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_java,
        容器: app_name_java,
        镜像: application_image_java,
        端口: 'TCP/9000',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.createAppByTemplate(testData, 'Java');
    expect(page.toast.getMessage()).toBe(`应用 ${app_name_java} 创建成功`);
    page.toast.waitDisappear();
    // 创建后返回到详情页检查是否创建成功
    page.verifyResult(expectResult);

    // 检查详情页中的流水线历史数据是否正确
    // TODO
  });

  it('L1:AldDevops-2396:创建应用-模版创建-Golang-全部使用默认选项', () => {
    const testData = {
      应用名称: app_name_golang_default,
      // 镜像设置
      镜像仓库地址: application_imageAddress_golang,
      镜像版本: application_imageTag_golang,

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
      代码仓库地址: application_codeUrl_golang,
      构建目录: application_buildDir_golang,
    };
    page.createAppByTemplate(testData, 'Golang');
    const expectResult = {
      基本信息: {
        name: app_name_golang_default,
        名称: app_name_golang_default,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_golang_default,
        容器: app_name_golang_default,
        镜像:
          application_imageAddress_golang + ':' + application_imageTag_golang,
        端口: 'TCP/8080',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    // 创建后返回到详情页检查是否创建成功
    expect(page.toast.getMessage()).toBe(
      `应用 ${app_name_golang_default} 创建成功`,
    );
    page.toast.waitDisappear();
    page.verifyResult(expectResult);

    // 检查详情页中的流水线历史数据是否正确
    //TODO
  });

  it('L1:AldDevops-2399:创建应用-模版创建-Golang-改变默认选项', () => {
    const testData = {
      应用名称: app_name_golang,
      // 镜像设置
      镜像仓库地址: application_imageAddress_golang,
      镜像版本: application_imageTag_golang,
      镜像源证书: '不使用',

      // 网络设置
      访问端口: '9000',

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
      'Jenkins 节点标签': hello_label,
      代码仓库地址: application_codeUrl_golang,
      构建目录: application_buildDir_golang,
      代码仓库证书: '不使用',
      启动文件的路径: application_startDir_golang,
    };

    page.createAppByTemplate(testData, 'Golang');
    const expectResult = {
      基本信息: {
        name: app_name_golang,
        名称: app_name_golang,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_golang,
        容器: app_name_golang,
        镜像: application_image_golang,
        端口: 'TCP/9000',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };

    // 创建后返回到详情页检查是否创建成功
    expect(page.toast.getMessage()).toBe(`应用 ${app_name_golang} 创建成功`);
    page.toast.waitDisappear();
    page.verifyResult(expectResult);
    // 检查详情页中的流水线历史数据是否正确
    //TODO
  });

  it('L1:AldDevops-2397:创建应用-模版创建-Python-全部使用默认选项', () => {
    const testData = {
      应用名称: app_name_python_default,
      // 镜像设置
      镜像仓库地址: application_imageAddress_python,

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
    };

    const expectResult = {
      基本信息: {
        name: app_name_python_default,
        名称: app_name_python_default,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_python_default,
        容器: app_name_python_default,
        镜像: `${application_imageAddress_python}:${application_imageTag_python}`,
        端口: 'TCP/8080',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };

    page.createAppByTemplate(testData, 'Python');
    expect(page.toast.getMessage()).toBe(
      `应用 ${app_name_python_default} 创建成功`,
    );
    page.toast.waitDisappear();

    // 创建后返回到详情页检查是否创建成功
    page.verifyResult(expectResult);
    // 检查详情页中的流水线历史数据是否正确
  });

  it('L1:AldDevops-2400:创建应用-模版创建-Python-改变默认选项', () => {
    const testData = {
      应用名称: app_name_python,
      // 镜像设置
      镜像仓库地址: application_imageAddress_python,
      镜像版本: application_imageTag_python,
      镜像源证书: '不使用',

      // 网络设置
      访问端口: '9000',

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
      'Jenkins 节点标签': hello_label,
      代码仓库地址: application_codeUrl_python,
      构建目录: application_buildDir_python,
      代码仓库证书: '不使用',

      // 应用启动设置
      'Shell 文件启动应用': application_shellScript_sh,
      'Python 文件启动应用': application_pythonScript_sh,
      'gunicorn 启动应用': application_gunicorModule,
    };

    const expectResult = {
      基本信息: {
        name: app_name_python,
        名称: app_name_python,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_python,
        容器: app_name_python,
        镜像: application_image_python,
        端口: 'TCP/9000',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };

    page.createAppByTemplate(testData, 'Python');
    expect(page.toast.getMessage()).toBe(`应用 ${app_name_python} 创建成功`);
    page.toast.waitDisappear();

    // 创建后返回到详情页检查是否创建成功
    page.verifyResult(expectResult);

    // 检查详情页中的流水线历史数据是否正确
    // TODO
  });

  it('L1:AldDevops-2294:创建应用-模版创建-PHP-全部使用默认选项', () => {
    const testData = {
      应用名称: app_name_php_default,
      // 镜像设置
      镜像仓库地址: application_imageAddress_php,

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
    };
    page.createAppByTemplate(testData, 'PHP');
    expect(page.toast.getMessage()).toBe(
      `应用 ${app_name_php_default} 创建成功`,
    );
    page.toast.waitDisappear();

    const expectResult = {
      基本信息: {
        name: app_name_php_default,
        名称: app_name_php_default,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_php_default,
        容器: app_name_php_default,
        镜像: `${application_imageAddress_php}:${application_imageTag_php}`,
        端口: 'TCP/8080',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };

    // 创建后返回到详情页检查是否创建成功
    page.verifyResult(expectResult);

    // 检查详情页中的流水线历史数据是否正确
  });

  it('L1:AldDevops-2401:创建应用-模版创建-PHP-改变默认选项', () => {
    const testData = {
      应用名称: app_name_php,
      // 镜像设置
      镜像仓库地址: application_imageAddress_php,
      镜像版本: application_imageTag_php,
      镜像源证书: '不使用',

      // 网络设置
      访问端口: '9000',

      // 流水线设置
      'Jenkins 服务': jenkins_name_api,
      'Jenkins 节点标签': hello_label,
      代码仓库地址: application_codeUrl_php,
      构建目录: application_buildDir_php,
      代码仓库证书: '不使用',
    };
    const expectResult = {
      基本信息: {
        name: app_name_php,
        名称: app_name_php,
        访问地址: '-',
        资源状态: '1',
      },
      'Deployment (1 个)': {
        name: app_name_php,
        容器: app_name_php,
        镜像: application_image_php,
        // 端口: 'TCP/9000',
        内存: ['请求值: -', '限制值: -'],
        CPU: ['请求值: -', '限制值: -'],
      },
    };
    page.createAppByTemplate(testData, 'PHP');
    // 创建后返回到详情页检查是否创建成功
    expect(page.toast.getMessage()).toBe(`应用 ${app_name_php} 创建成功`);
    page.verifyResult(expectResult);
  });
});
