/**
 * Created by liuwei on 2018/8/7.
 */

import { ServerConf } from '../../../config/serverConf';
import { ApplicationPage } from '../../../page_objects/project/application/application.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('AppCore volumeMount L1级别UI自动化case', () => {
  const page = new ApplicationPage();
  const project_name = page.getTestData('project-app1-');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'the first auto project for application!';

  const application_name = page.getTestData('application-yaml');

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'alauda.appcore.yaml',
      {
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
        '${NAME}': application_name,
        '${LABEL}': application_name,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.appcore.volumeMount',
    );
    page.enterProjectDashboard();
    page.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(`kubectl delete project ${project_name}`);
  });

  it('L1:AldDevops-2552: volumeMount, 使用配置字典，容器路径挂载存储卷', () => {
    const containerPath = page.getTestData('/var/temp/test');
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'D', application_name);
    page.resourceDetailPage.container.openVolumeMount();
    //获得volumeMount page
    const volumeMountPage = page.resourceDetailPage.container.volumeMont;
    volumeMountPage.configmap.select(application_name);
    volumeMountPage.containerPath.input(containerPath);
    volumeMountPage.buttonAdd.click();
    expect(volumeMountPage.tableMount.getHeaderText()).toEqual([
      '类型',
      '名称/路径',
      '容器路径',
    ]);
    expect(volumeMountPage.tableMount.getRow([containerPath]).getText()).toBe(
      `配置字典 volume-0 ${containerPath}`,
    );
  });
  it('L1:AldDevops-2553: volumeMount, 使用配置字典，单独引用挂载存储卷', () => {
    const containerPath = page.getTestData('/var/temp/test');
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'D', application_name);
    page.resourceDetailPage.container.openVolumeMount();
    //获得volumeMount page
    const volumeMountPage = page.resourceDetailPage.container.volumeMont;
    volumeMountPage.configmap.select(application_name);
    volumeMountPage.switch.open();
    volumeMountPage.keyValueForm.newValue(['app.ini', containerPath]);

    volumeMountPage.buttonAdd.click();
    expect(volumeMountPage.tableMount.getRow([containerPath]).getText()).toBe(
      `配置字典 volume-0\n键\n文件路径\napp.ini ${containerPath}`,
    );
  });

  it('L1:AldDevops-2554: volumeMount, 使用保密字典，容器路径挂载存储卷', () => {
    const containerPath = page.getTestData('/var/temp/test');
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'SS', application_name);
    page.resourceDetailPage.container.openVolumeMount();
    //获得volumeMount page
    const volumeMountPage = page.resourceDetailPage.container.volumeMont;
    volumeMountPage.volumeType.clickByName('保密字典');
    volumeMountPage.secret.select(application_name);
    volumeMountPage.containerPath.input(containerPath);
    volumeMountPage.buttonAdd.click();
    expect(volumeMountPage.tableMount.getHeaderText()).toEqual([
      '类型',
      '名称/路径',
      '容器路径',
    ]);
    expect(volumeMountPage.tableMount.getRow([containerPath]).getText()).toBe(
      `保密字典 volume-0 ${containerPath}`,
    );
  });

  it('L1:AldDevops-2555: volumeMount, 使用保密字典，单独引用挂载存储卷', () => {
    const containerPath = page.getTestData('/var/temp/test');
    const containerPath1 = page.getTestData('/var/temp/test');
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'DS', application_name);
    page.resourceDetailPage.container.openVolumeMount();
    //获得volumeMount page
    const volumeMountPage = page.resourceDetailPage.container.volumeMont;
    volumeMountPage.volumeType.clickByName('保密字典');
    volumeMountPage.secret.select(application_name);
    volumeMountPage.switch.open();
    volumeMountPage.keyValueForm.newValue(['username', containerPath]);
    volumeMountPage.keyValueForm.buttonAdd.click();
    volumeMountPage.keyValueForm.newValue(['password', containerPath1]);

    volumeMountPage.buttonAdd.click();
    expect(volumeMountPage.tableMount.getRow([containerPath]).getText()).toBe(
      `保密字典 volume-0\n键\n文件路径\nusername ${containerPath}\npassword ${containerPath1}`,
    );
  });

  it('L1:AldDevops-2556:volumeMount, 使用主机路径，挂载存储卷', () => {
    const hostPath = page.getTestData('/var/temp/hosttest');
    const containerPath = page.getTestData('/var/temp/test');
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'DS', application_name);
    page.resourceDetailPage.container.openVolumeMount();
    //获得volumeMount page
    const volumeMountPage = page.resourceDetailPage.container.volumeMont;
    volumeMountPage.volumeType.clickByName('主机路径');

    volumeMountPage.hostPath.input(hostPath);
    volumeMountPage.containerPath.input(containerPath);

    volumeMountPage.buttonAdd.click();
    expect(volumeMountPage.tableMount.getRow([containerPath]).getText()).toBe(
      `主机路径 ${hostPath} ${containerPath}`,
    );
  });

  it('L1:ldDevops-2557: 详情页更新标签', () => {
    const labelKey = page.getTestData('labelkey');
    const labelValue = page.getTestData('labelvalue');
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'DS', application_name);
    page.resourceDetailPage.buttonEditLabel.click();
    page.resourceDetailPage.keyValueControl.newValue(labelKey, labelValue);
    page.resourceDetailPage.keyValueControl.clickUpdate();
    page.toast.getMessage().then(message => {
      console.log(message);
    });

    expect(
      page.resourceDetailPage.getCardBodyItem('基本信息', '标签').getText(),
    ).toEqual([
      'alauda.test: true',
      `app: ${application_name}`,
      `${labelKey}: ${labelValue}`,
    ]);
  });

  it('L1:AldDevops-2558: 详情页更新注解', () => {
    const labelKey = page.getTestData('labelkey');
    const labelValue = page.getTestData('labelvalue');
    page.clickLeftNavByText('应用');
    // 展开应用
    page.listPage.expandByAppName(application_name);
    // 单击Depliyment名称, 进入Deployment 的详细页
    page.listPage.clickByResourceName(application_name, 'D', application_name);
    page.resourceDetailPage.buttonEditAnnotation.click();
    page.resourceDetailPage.keyValueControl.newValue(labelKey, labelValue);
    page.resourceDetailPage.keyValueControl.clickUpdate();
    page.toast.getMessage().then(message => {
      console.log(message);
    });

    expect(
      page.resourceDetailPage.getCardBodyItem('基本信息', '注解').getText(),
    ).toEqual([
      'deployment.kubernetes.io/revision: 3',
      `${labelKey}: ${labelValue}`,
    ]);
  });
});
