/**
 * Created by zhangjijao on 2018/4/8.
 */

import { browser } from 'protractor';

import { ProjectsPage } from '../../../page_objects/platform/projects/projects.page';
import { SecretsPage } from '../../../page_objects/platform/secrets/secrets.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonMethod } from '../../../utility/common.method';
import { CommonPage } from '../../../utility/common.page';

describe('项目管理中的Secret L1级别UI自动化case', () => {
  const projectPage = new ProjectsPage();
  const page = new SecretsPage();
  const project_name = page.getTestData('projectl1-secret1');
  const project_displayname = project_name.toLocaleUpperCase();
  const project_description = 'Secret自动化测试中添加一个project！';

  const secret_name_user = page.getTestData(`secret-user`);
  const secret_displayname_user = secret_name_user.toLocaleUpperCase();
  const secret_description_user = 'secret description, type is user/password!!';
  const secret_username_user = 'zhangjiao';
  const secret_password_user = 'zhangjiao';

  const secret_displayname_user_update = `${secret_displayname_user}-0`;
  const secret_description_user_update =
    'secret description, type is user/password update update!!';
  const secret_username_user_update = 'zhangjiao_update';
  const secret_password_user_update = 'zhangjiao_update';
  const secret_displayname_user_update1 = `${secret_displayname_user}-1`;
  const secret_description_user_update1 =
    'secret description, type is user/password update update1!!';
  const secret_username_user_update1 = 'zhangjiao_update1';
  const secret_password_user_update1 = 'zhangjiao_update1';

  const secret_name_registry = CommonMethod.random_generate_testData(
    `${this.prefix}-reg`,
  );
  const secret_displayname_registry = secret_name_registry.toLocaleUpperCase();
  const secret_description_registry = 'secret description, type is registry!!';
  const secret_address_registry = 'inputregistry.com';
  const secret_username_registry = 'inputregistryusername';
  const secret_password_registry = 'inputregistrypassword';
  const secret_email_registry = 'input@email.com';

  const secret_name_opaque = CommonMethod.random_generate_testData(`opaque-`);
  const secret_displayname_opaque = secret_name_opaque.toLocaleUpperCase();
  const secret_description_opaque = 'secret description, type is Opaques!!';
  const secret_key_opaque = 'inputkey';
  const secret_value_opaque = 'inputvalue';

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'alauda.project.yaml',
      {
        '${PROJECT_NAME}': project_name,
        '${PROJECT_DISPLAYNAME}': project_displayname,
        '${PROJECT_DESCRIPTION}': project_description,
      },
      'L1.secrets-Inproject.testdata',
    );
    browser.refresh();
    page.enterProjectDashboard();
    projectPage.resourceTable.clickResourceNameByRow([project_displayname]);
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  /**
   * 创建Secret,类型选择"用户名／密码"，其余各项条件输入合法，创建成功。
   */

  it('L1:Ald-2382:Secret-创建Secret-Secret创建成功(类型选择用户名/密码)', () => {
    const testData = {
      'Secret 名称': secret_name_user,
      显示名称: secret_displayname_user,
      // 所属项目: project_name_api1,
      描述: secret_description_user,
      类型: '用户名/密码',
      用户名: secret_username_user,
      密码: secret_password_user,
    };
    page.createSecrets(testData);

    // 创建后跳转到详情页，验证是否创建成功
    expect(
      page.detailPage_Content.checkInputboxIsPresent('Secret 名称'),
    ).toBeTruthy();
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('Secret 名称'),
      secret_name_user,
    );
    expect(
      page.detailPage_Content.getElementByText('Secret 名称').getText(),
    ).toBe(secret_name_user);
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      secret_displayname_user,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      secret_description_user,
    );
    expect(page.detailPage_Content.getElementByText('类型').getText()).toBe(
      '用户名/密码',
    );
    expect(page.detailPage_Content.getElementByText('用户名').getText()).toBe(
      secret_username_user,
    );
  });

  /**
   * 创建Secret,类型选择"Opaque"，其余各项条件输入合法，创建成功。
   */
  it('L1:Ald-2384:Secret-创建Secret-Secret创建成功(类型选择Opaque)', () => {
    const testData = {
      'Secret 名称': secret_name_opaque,
      显示名称: secret_displayname_opaque,
      // 所属项目: project_name_api1,
      描述: secret_description_opaque,
      类型: 'Opaque',
      Opaque: { inputkey: secret_value_opaque },
    };
    page.createSecrets(testData);

    // 创建后跳转到详情页，验证是否创建成功
    expect(
      page.detailPage_Content.checkInputboxIsPresent('Secret 名称'),
    ).toBeTruthy();
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('Secret 名称'),
      secret_name_opaque,
    );
    expect(
      page.detailPage_Content.getElementByText('Secret 名称').getText(),
    ).toBe(secret_name_opaque);
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      secret_displayname_opaque,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      secret_description_opaque,
    );
    expect(page.detailPage_Content.getElementByText('类型').getText()).toBe(
      'Opaque',
    );
    expect(
      page.detailPage_Content.getElementByText(secret_key_opaque).getText(),
    ).toBe(secret_value_opaque);
  });

  /**
   * 创建Secret,类型选择"镜像服务"，其余各项条件输入合法，创建成功。
   */
  it('L1:Ald-2383:Secret-创建Secret-Secret创建成功(类型选择镜像服务)', () => {
    const testData = {
      'Secret 名称': secret_name_registry,
      显示名称: secret_displayname_registry,
      // 所属项目: project_name_api1,
      描述: secret_description_registry,
      类型: '镜像服务',
      镜像服务地址: secret_address_registry,
      用户名: secret_username_registry,
      密码: secret_password_registry,
      邮箱地址: secret_email_registry,
    };
    page.createSecrets(testData);

    // 创建后跳转到详情页，验证是否创建成功
    expect(
      page.detailPage_Content.checkInputboxIsPresent('Secret 名称'),
    ).toBeTruthy();
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('Secret 名称'),
      secret_name_registry,
    );
    expect(
      page.detailPage_Content.getElementByText('Secret 名称').getText(),
    ).toBe(secret_name_registry);
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      secret_displayname_registry,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      secret_description_registry,
    );
    expect(page.detailPage_Content.getElementByText('类型').getText()).toBe(
      '镜像服务',
    );
    expect(
      page.detailPage_Content.getElementByText('镜像服务地址').getText(),
    ).toBe(secret_address_registry);
    expect(page.detailPage_Content.getElementByText('用户名').getText()).toBe(
      secret_username_registry,
    );
    expect(page.detailPage_Content.getElementByText('邮箱地址').getText()).toBe(
      secret_email_registry,
    );
  });

  /**
   * 详情页更新Secret，Secret更新成功。
   */
  it('L1:Ald-2385:Secret-详情页更新Secret-更新成功', () => {
    page.resourceTable.clickResourceNameByRow([secret_displayname_user]);
    page.getButtonByText('更新').click();

    const testData = {
      显示名称: secret_displayname_user_update,
      描述: secret_description_user_update,
      用户名: secret_username_user_update,
      密码: secret_password_user_update,
    };
    page.createPage.fillForm(testData);
    page.updateDialog.clickConfirm(); // 点击更新按钮
    page.toast.waitDisappear();

    // 更新后跳转到详情页，验证是否创建成功
    expect(
      page.detailPage_Content.checkInputboxIsPresent('Secret 名称'),
    ).toBeTruthy();
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('Secret 名称'),
      secret_name_user,
    );
    expect(
      page.detailPage_Content.getElementByText('Secret 名称').getText(),
    ).toBe(secret_name_user);
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      secret_displayname_user_update,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      secret_description_user_update,
    );
    expect(page.detailPage_Content.getElementByText('类型').getText()).toBe(
      '用户名/密码',
    );
    expect(page.detailPage_Content.getElementByText('用户名').getText()).toBe(
      secret_username_user_update,
    );
  });

  /**
   * 列表页更新Secret，Secret更新成功。
   */
  it('L1:Ald-2386:Secret-列表页更新Secret-更新成功', () => {
    page.resourceTable.clickOperationButtonByRow(
      [secret_displayname_user_update],
      '更新',
      'mat-icon',
      'div[class=mat-menu-content] button',
    );

    const testData = {
      显示名称: secret_displayname_user_update1,
      描述: secret_description_user_update1,
      用户名: secret_username_user_update1,
      密码: secret_password_user_update1,
    };
    page.createPage.fillForm(testData);
    page.updateDialog.clickConfirm(); // 点击更新按钮

    page.resourceTable.clickResourceNameByRow([
      secret_displayname_user_update1,
    ]);

    // 更新后跳转到详情页，验证是否创建成功
    expect(
      page.detailPage_Content.checkInputboxIsPresent('Secret 名称'),
    ).toBeTruthy();
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getElementByText('Secret 名称'),
      secret_name_user,
    );
    expect(
      page.detailPage_Content.getElementByText('Secret 名称').getText(),
    ).toBe(secret_name_user);
    expect(page.detailPage_Content.getElementByText('显示名称').getText()).toBe(
      secret_displayname_user_update1,
    );
    expect(page.detailPage_Content.getElementByText('描述').getText()).toBe(
      secret_description_user_update1,
    );
    expect(page.detailPage_Content.getElementByText('类型').getText()).toBe(
      '用户名/密码',
    );
    expect(page.detailPage_Content.getElementByText('用户名').getText()).toBe(
      secret_username_user_update1,
    );
  });
});
