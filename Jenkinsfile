// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
def deployment
// image can be used for promoting...
def IMAGE
def IMAGE_E2E
def CURRENT_VERSION
def RELEASE_VERSION
def RELEASE_BUILD
def CHANGE_TARGET
def CHANGE_TITLE
def code_data
def DEBUG = false
pipeline {
    // 运行node条件
    // 为了扩容jenkins的功能一般情况会分开一些功能到不同的node上面
    // 这样每个node作用比较清晰，并可以并行处理更多的任务量
    agent {
        label 'all'
    }

    // (optional) 流水线全局设置
    options {
        // 保留多少流水线记录（建议不放在jenkinsfile里面）
        buildDiscarder(logRotator(numToKeepStr: '10'))

        // 不允许并行执行
        disableConcurrentBuilds()
    }
    //(optional) 环境变量
    environment {
        // def root = tool name: 'go1.9', type: 'go'
        // GOROOT = "${root}"
        // GOPATH = '/go'
        // PATH = "${root}/bin:${GOPATH}/bin:$PATH"
        FOLDER = "$GOPATH/src/alauda.io/diablo"

        // for building an scanning
        REPOSITORY = "diablo"
        OWNER = "mathildetech"
        // sonar feedback user
        // needs to change together with the credentialsID
        BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
        SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
        NAMESPACE = "alauda-system"
        DEPLOYMENT = "diablo"
        CONTAINER = "backend"
        DINGDING_BOT = "devops-chat-bot"
        ASF_DINGDING_BOT = "asf-robot"
        TAG_CREDENTIALS = "alaudabot-bitbucket"
        E2E_IMAGE = "index.alauda.cn/alaudak8s/diablo-e2e"
        TEST_REPORT_FOLDER = "test_report"
    }
    // stages
    stages {
        stage('Checkout') {
            steps {
                script {
                    // checkout code
                    def scmVars = checkout scm
                    // extract git information
                    env.GIT_COMMIT = scmVars.GIT_COMMIT
                    env.GIT_BRANCH = scmVars.GIT_BRANCH
                    CHANGE_TARGET = "${env.CHANGE_TARGET}"
                    CHANGE_TITLE = "${env.CHANGE_TITLE}"
                    GIT_COMMIT = "${scmVars.GIT_COMMIT}"
                    GIT_BRANCH = "${scmVars.GIT_BRANCH}"
                    RELEASE_BUILD = "${env.BUILD_NUMBER}"
                    RELEASE_VERSION = readFile('.version').trim()
                    RELEASE_BUILD = "${RELEASE_VERSION}.${env.BUILD_NUMBER}"
                    if (GIT_BRANCH != "master") {
                        def branch = GIT_BRANCH.replace("/","-").replace("_", "-")
                        RELEASE_BUILD = "${RELEASE_VERSION}.${branch}.${env.BUILD_NUMBER}"
                }
            }
            // moving project code for the specified folder
            sh """
                mkdir ${TEST_REPORT_FOLDER}
                rm -rf ${FOLDER}
                mkdir -p ${FOLDER}/e2e-reports
                cp -R . ${FOLDER}
                cp -R .git ${FOLDER}/.git
            """
            // installing golang coverage and report tools
            sh """
                go get -u github.com/alauda/gitversion
            """
            script {
                if (GIT_BRANCH == "master") {
                    sh "gitversion patch ${RELEASE_VERSION} > patch"
                    RELEASE_BUILD = readFile("patch").trim()
                }
                echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
            }
        }
    }

      stage('CI'){
        failFast true
        parallel {
          stage('Build') {
            //   agent { label "all" }
              //(optional) 环境变量
            // environment {
            //     def root = tool name: 'go1.9', type: 'go'
            //     GOROOT = "${root}"
            //     GOPATH = '/go'
            //     PATH = "${root}/bin:${GOPATH}/bin:$PATH"
            //     FOLDER = "$GOPATH/src/alauda.io/diablo"

            // }
              steps {
                  script {
                    // build
                    sh """
                        cd ${FOLDER}
                        yarn install
                        yarn build
                    """
                    // currently is building code inside the container
                    IMAGE = deploy.dockerBuild(
                        "${FOLDER}/dist/Dockerfile", //Dockerfile
                        "${FOLDER}/dist", // build context
                        "index.alauda.cn/alaudak8s/diablo", // repo address
                        "${RELEASE_BUILD}", // tag
                        "alaudak8s", // credentials for pushing
                    )
                    IMAGE.setArg("commit_id", "${GIT_COMMIT}").setArg("app_version", "${RELEASE_BUILD}")
                    // start and push
                    IMAGE.start().push().push("${GIT_COMMIT}")

                    // building e2e image
                    IMAGE_E2E = deploy.dockerBuild(
                        "${FOLDER}/e2e/Dockerfile",
                        "${FOLDER}",
                        E2E_IMAGE,
                        "${RELEASE_BUILD}",
                        "alaudak8s",
                    )
                    IMAGE_E2E.start().push().push("${GIT_COMMIT}")
                  }
              }
          }
          stage('Code Scan') {
            steps {
                script {
                    sh """
                      cd ${FOLDER}/src/backend
                      make test-result
                      make cover-result
                      echo 'sonar.projectVersion=${RELEASE_BUILD}' >> ${FOLDER}/sonar-project.properties
                    """

                    try{
                      // scanning
                      deploy.scan(
                          REPOSITORY,
                          GIT_BRANCH,
                          SONARQUBE_BITBUCKET_CREDENTIALS,
                          FOLDER,
                          DEBUG,
                          OWNER,
                          BITBUCKET_FEEDBACK_ACCOUNT).start()
                    } catch (Exception exc) {
                      echo "scan in sonar failed: ${exc}"
                    }
                }
            }
          }
          stage('Test') {
            agent { label 'chrome' }
            environment {
                def root = tool name: 'go1.10', type: 'go'
                GOROOT = "${root}"
                GOPATH = '/go'
                PATH = "${root}/bin:${GOPATH}/bin:$PATH"
                FOLDER = "$GOPATH/src/alauda.io/diablo"
            }
            steps {
                script {
                    checkout scm
                    sh """
                        mkdir ${TEST_REPORT_FOLDER}
                        rm -rf ${FOLDER}
                        mkdir -p ${FOLDER}/e2e-reports
                        cp -R . ${FOLDER}
                        cp -R .git ${FOLDER}/.git
                    """

                    sh "cd ${FOLDER} && yarn install && mkdir -p ${FOLDER}/e2e-reports "
                    sh "cd ${FOLDER} && yarn test:ci"
                    // setup kubectl
                    def token
                    if (GIT_BRANCH == "master") {
                        // master is already merged
                        env.CLUSTER_MASTER_IP = "alaudak8s-staging.alauda.cn"
                        token = deploy.setupStaging()

                    } else {
                        // pull-requests
                        env.CLUSTER_MASTER_IP = "alaudak8s-int.alauda.cn"
                        token = deploy.setupInt()
                    }
                    try {
                        if (GIT_BRANCH == "master") {
                            sh "cd ${FOLDER} && export USER_TOKEN=${token} && yarn e2e:full-integration"
                        } else {
                            if (CHANGE_TITLE.toLowerCase().contains("/asf")) {
                                sh "cd ${FOLDER} && export USER_TOKEN=${token} && yarn e2e:asf-integration"
                            } else {
                                sh "cd ${FOLDER} && export USER_TOKEN=${token} && yarn e2e:integration"
                            }
                        }
                        
                        sh "ls -la ${FOLDER}/e2e-reports/ || true"
                        sh "cp -R ${FOLDER}/e2e-reports/* ${TEST_REPORT_FOLDER}/ || true"
                        sh "ls -la ${TEST_REPORT_FOLDER}/"
                        archiveArtifacts artifacts: "${TEST_REPORT_FOLDER}/**", fingerprint: true
                    } catch (Exception exc) {
                      echo "test failed: ${exc}"
                      sh "ls -la ${FOLDER}/e2e-reports/ || true"
                      sh "cp -R ${FOLDER}/e2e-reports/* ${TEST_REPORT_FOLDER}/ || true"
                      sh "ls -la ${TEST_REPORT_FOLDER}/"
                      archiveArtifacts artifacts: "${TEST_REPORT_FOLDER}/**", fingerprint: true
                      throw exc
                    }
                    sh "rm -rf ${FOLDER}/src/backend/dist"
                }
            }
          }
        }

      }
      // after build it should start deploying
      stage('Deploying') {
          steps {
              echo "here we can start to deploy"
              script {
                  // setup kubectl
                  if (GIT_BRANCH == "master") {
                      // master is already merged
                      deploy.setupStaging()
                  } else {
                      // pull-requests
                      deploy.setupInt()
                  }
                  // saving current state
                  CURRENT_VERSION = deploy.getDeployment(NAMESPACE, DEPLOYMENT)
                  // starts deploying
                  deploy.updateDeployment(
                      NAMESPACE,
                      DEPLOYMENT,
                      // just built image
                      IMAGE.getImage(),
                      CONTAINER,
                  )
              }
          }
      }
      stage('Manual test') {
          when {
              expression {
                // GIT_BRANCH == "master"
                // remove manual test result for now
                false
              }
          }
          steps {
              script {
                  if (CHANGE_TITLE.toLowerCase().contains("/asf")) {
                    deploy.notificationTest(DEPLOYMENT, ASF_DINGDING_BOT, "注意：等待手工测试反馈！", RELEASE_BUILD)
                  } else {
                    deploy.notificationTest(DEPLOYMENT, DINGDING_BOT, "注意：等待手工测试反馈！", RELEASE_BUILD)
                  }
              }
              input message: 'Accepted?'
          }
      }
      stage('Promoting') {
          // limit this stage to master only
          when {
              expression {
                  GIT_BRANCH == "master"
              }
          }
          steps {
              script {
                  // setup kubectl
                  deploy.setupProd()

                  // promote to release
                  IMAGE.push("release")

                  // update production environment
                  deploy.updateDeployment(
                      NAMESPACE,
                      DEPLOYMENT,
                      // just built image
                      IMAGE.getImage(),
                      CONTAINER,
                  )
                  // adding tag to the current commit
                  withCredentials([usernamePassword(credentialsId: TAG_CREDENTIALS, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                    sh "git tag -l | xargs git tag -d" // clean local tags
                    sh """
                        git config --global user.email "alaudabot@alauda.io"
                        git config --global user.name "Alauda Bot"
                    """
                    def repo = "https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${OWNER}/${REPOSITORY}.git"
                    sh "git fetch --tags ${repo}" // retrieve all tags
                    sh("git tag -a ${RELEASE_BUILD} -m 'auto add release tag by jenkins'")
                    sh("git push ${repo} --tags")
                }
                build job: '../../charts-pipeline', parameters: [
                  [$class: 'StringParameterValue', name: 'CHART', value: 'devops'],
                  [$class: 'StringParameterValue', name: 'VERSION', value: RELEASE_VERSION],
                  [$class: 'StringParameterValue', name: 'COMPONENT', value: 'diablo'],
                  [$class: 'StringParameterValue', name: 'IMAGE_TAG', value: RELEASE_BUILD],
                  [$class: 'BooleanParameterValue', name: 'DEBUG', value: false],
                  [$class: 'StringParameterValue', name: 'ENV', value: ''],
                ], wait: false
              }
          }
      }

    }

    // (optional)
    // happens at the end of the pipeline
    post {
        // 成功
        success {
          echo "Horay!"
          script {
            if (GIT_BRANCH == "master") {
                deploy.notificationSuccess(DEPLOYMENT, ASF_DINGDING_BOT, "上线啦！", RELEASE_BUILD)
                deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "上线啦！", RELEASE_BUILD)
            } else {
                if (CHANGE_TITLE.toLowerCase().contains("/asf")) {
                    deploy.notificationSuccess(DEPLOYMENT, ASF_DINGDING_BOT, "流水线完成了", RELEASE_BUILD)
                } else {
                    deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "流水线完成了", RELEASE_BUILD)
                }
            }
          }
        }

        // 失败
        failure {
            echo "damn!"
            // check the npm log
            // fails lets check if it
            script {
                if (CURRENT_VERSION != null) {
                  if (GIT_BRANCH == "master") {
                    deploy.setupStaging()
                  } else {
                    deploy.setupInt()
                  }
                    deploy.rollbackDeployment(
                        NAMESPACE,
                        DEPLOYMENT,
                        CONTAINER,
                        CURRENT_VERSION,
                    )
                }

                if (GIT_BRANCH == "master") {
                    deploy.notificationFailed(DEPLOYMENT, ASF_DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
                    deploy.notificationFailed(DEPLOYMENT, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
                } else {
                    if (CHANGE_TITLE.toLowerCase().contains("/asf")) {
                        deploy.notificationFailed(DEPLOYMENT, ASF_DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
                    } else {
                        deploy.notificationFailed(DEPLOYMENT, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
                    }
                }
            }
        }
        // 取消的
        aborted {
          echo "aborted!"
        }
    }
}
