import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApplicationApiService } from '@app/api/application/application-api.service';
import { Pagination } from '@app/types';
import { filterBy, getQuery, sortBy } from '@app/utils/query-builder';
import { get, head } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  API_GROUP_VERSION,
  MicroserviceComponentName,
} from '../asf.constants';

export interface MicroserviceEnvironmentModel {
  name: string;
  displayName: string;
  description: string;
}

export interface MicroserviceEnvironmentBindModel {
  name: string;
  namespace: string;
  description: string;
}

interface Metadata {
  name: string;
  namespace?: string;
  annotations?: {
    [key: string]: string;
  };
  labels?: {
    [key: string]: string;
  };
  creationTimestamp?: string;
}

export interface MicroserviceEnvironmentPayload {
  apiVersion: string;
  kind: string;
  metadata: Metadata;
}

export interface MicroserviceComponent {
  microservicesComponent: {
    objectMeta: Metadata;
    typeMeta: {
      kind: string;
    };
    spec: {
      microservicesEnvironmentName: string;
      releaseName: string;
      type: number;
      order: number;
      rawValues: string;
    };
    status: {
      status: string;
      currentInstallStep: number;
      totalInstallSteps: number;
      podControllerInfo: {
        pending: number;
        desired: number;
        unavailable: number;
        available: number;
      };
      conditions?: {
        type: string;
        status: string;
        lastProbeTime: string;
        lastTransitionTime: string;
        reason: string;
        message: string;
      }[];
    };
  };
  chart: {
    objectMeta: Metadata;
    typeMeta: {
      kind: string;
    };
    spec: {
      chart: {
        name: string;
        version: string;
      };
      values: string;
    };
  };
}

export interface MicroserviceComponentRef {
  name: MicroserviceComponentName;
  status: string;
  hostpath?: string;
}

export interface MicroserviceEnvironmentBase {
  objectMeta: Metadata;
  typeMeta: {
    kind: string;
  };
  bindingProjects?: { name: string }[];
  spec: {
    microserviceComponentRefs: MicroserviceComponentRef[];
    namespace: {
      name: string;
      type: string;
    };
  };
  status: {
    status: string;
    namespace: {
      name: string;
      status: string;
    };
    components: {
      status: string;
    };
  };
}

export interface MicroserviceEnvironment {
  microservicesEnvironment: MicroserviceEnvironmentBase;
  microservicesComponents?: MicroserviceComponent[];
  microservicesEnvironmentBindings: {
    apiVersion: string;
    kind: string;
    objectMeta: Metadata;
    spec: {
      microservicesEnvironmentRef: {
        name: string;
      };
    };
  }[];
}

export interface InstallProgress {
  currentStep: number;
  allSteps: number;
  status: string;
  hostpath: string;
  podControllerInfo: {
    [key: string]: number;
  };
}

export interface PostResponse {
  create_messages: {
    success: boolean;
    resource?: string;
    message: string;
  }[];
  total_resource_count?: number;
  success_resource_count?: number;
  failed_resource_count?: number;
}

export interface ListResponse {
  listMeta: {
    totalItems: number;
  };
  microservicesEnvironments: MicroserviceEnvironmentBase[];
}

export interface ListParams {
  keywords?: string;
  searchBy?: string;
  sort?: string;
  direction?: string;
}

@Injectable()
export class MicroserviceEnvironmentService {
  constructor(
    private http: HttpClient,
    private appService: ApplicationApiService,
  ) {}

  create(model: MicroserviceEnvironmentModel): Observable<PostResponse> {
    const payload = this.toResource(model);
    return this.http.post<PostResponse>('api/v1/others', [payload]);
  }

  update(model: MicroserviceEnvironmentModel): Observable<boolean> {
    const payload = this.toResource(model);
    return this.http
      .put(
        `api/v1/others/${API_GROUP_VERSION}/MicroservicesEnvironment/_/${
          model.name
        }`,
        payload,
      )
      .pipe(
        map(() => {
          return true;
        }),
      );
  }

  delete(name: string): Observable<boolean> {
    return this.http
      .delete(
        `api/v1/others/${API_GROUP_VERSION}/MicroservicesEnvironment/_/${name}`,
      )
      .pipe(
        map(() => {
          return true;
        }),
      );
  }

  install(
    namespace: string,
    name: string,
    yamlValue: string,
  ): Observable<boolean> {
    return this.http
      .post(`api/v1/microservicescomponent/${namespace}/${name}`, {
        rawValues: yamlValue,
      })
      .pipe(map(() => true));
  }

  updateComponent(
    namespace: string,
    name: string,
    yamlValue: string,
  ): Observable<boolean> {
    return this.http
      .put(`api/v1/microservicescomponent/${namespace}/${name}`, {
        rawValues: yamlValue,
      })
      .pipe(map(() => true));
  }

  bind(model: MicroserviceEnvironmentBindModel): Observable<PostResponse> {
    const payload = {
      apiVersion: API_GROUP_VERSION,
      kind: 'MicroservicesEnvironmentBinding',
      metadata: {
        name: model.name,
        namespace: model.namespace,
        annotations: {
          [ANNOTATION_DESCRIPTION]: model.description,
        },
      },
      spec: {
        microservicesEnvironmentRef: {
          name: model.name,
        },
      },
    };
    return this.http.post<PostResponse>(`api/v1/others`, [payload]);
  }

  unbindEnabled(projectName: string): Observable<boolean> {
    return this.appService
      .findApplications({
        name: '',
        pageIndex: 1,
        itemsPerPage: 20,
        project: projectName,
      })
      .pipe(
        map((p: Pagination<any>) => {
          return !p.items.length;
        }),
      );
  }

  unbind(name: string, projectName: string): Observable<boolean> {
    return this.http
      .delete(
        `api/v1/others/${API_GROUP_VERSION}/MicroservicesEnvironmentBinding/${projectName}/${name}`,
      )
      .pipe(map(() => true));
  }

  getList(
    params: ListParams = {
      keywords: '',
      searchBy: '',
      sort: '',
      direction: '',
    },
  ): Observable<MicroserviceEnvironmentBase[]> {
    return this.http
      .get(`api/v1/microservicesenvironments`, {
        params: getQuery(
          filterBy(params.searchBy, params.keywords),
          sortBy(params.sort, params.direction === 'desc'),
        ),
      })
      .pipe(
        map((ret: ListResponse) => {
          return ret.microservicesEnvironments;
        }),
      );
  }

  getBindingDetail(
    projectName: string,
  ): Observable<MicroserviceEnvironmentBase> {
    return this.http
      .get(`api/v1/microservicesenvironments`, {
        params: {
          project_name: projectName,
        },
      })
      .pipe(map((res: ListResponse) => head(res.microservicesEnvironments)));
  }

  getOptions(): Observable<string[]> {
    return this.getList().pipe(
      map(items => {
        return items
          .filter(
            item =>
              !item.bindingProjects.length &&
              get(item, 'status.components.status') === 'BasicReady',
          )
          .map(item => {
            const displayName = get(
              item,
              'objectMeta.annotations["alauda.io/displayName"]',
            );
            return `${item.objectMeta.name}${
              displayName ? `(${displayName})` : ''
            }`;
          });
      }),
    );
  }

  getInstallProgress(
    envName: string,
    compName: string,
  ): Observable<InstallProgress> {
    return this.getDetail(envName).pipe(
      map((env: MicroserviceEnvironment) => {
        const comp = env.microservicesComponents.find(
          c => c.microservicesComponent.objectMeta.name === compName,
        );
        const ref = env.microservicesEnvironment.spec.microserviceComponentRefs.find(
          item => item.name === compName,
        );
        return {
          currentStep: comp.microservicesComponent.status.currentInstallStep,
          allSteps: comp.microservicesComponent.status.totalInstallSteps,
          status: comp.microservicesComponent.status.status,
          hostpath: ref.hostpath,
          podControllerInfo:
            comp.microservicesComponent.status.podControllerInfo,
        };
      }),
    );
  }

  getDetail(name: string): Observable<MicroserviceEnvironment> {
    return this.http.get<MicroserviceEnvironment>(
      `api/v1/microservicesenvironments/${name}`,
    );
  }

  toResource(
    model: MicroserviceEnvironmentModel,
  ): MicroserviceEnvironmentPayload {
    return {
      apiVersion: API_GROUP_VERSION,
      kind: 'MicroservicesEnvironment',
      metadata: {
        name: model.name,
        annotations: {
          [ANNOTATION_DISPLAY_NAME]: model.displayName,
          [ANNOTATION_DESCRIPTION]: model.description,
        },
      },
    };
  }
}
