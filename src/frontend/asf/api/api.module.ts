import { NgModule } from '@angular/core';

import { MicroserviceService } from './microservice-api.service';
import { MicroserviceEnvironmentService } from './microservice-environment-api.service';

@NgModule({
  providers: [MicroserviceEnvironmentService, MicroserviceService],
})
export class ApiModule {}
