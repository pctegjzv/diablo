import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { filterBy, getQuery, pageBy, sortBy } from '@app/utils/query-builder';
import { PodsCount } from '@asf/features/microservice-environments/components/pods-status/pods-status.component';
import { countBy } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface ListParams {
  searchBy?: string;
  keywords?: string;
  sort?: string;
  direction?: string;
  pageIndex?: number;
  pageSize?: number;
  project: string;
}

export interface ServiceListResponse {
  listMeta: {
    totalItems: number;
  };
  apps: {
    name: string;
    status: string;
    instance: {
      instanceId: string;
      hostName: string;
      app: string;
      status: string;
    }[];
  }[];
  errors: any[];
}

export interface Service {
  name: string;
  status: string;
  podsCount: PodsCount;
  __original?: any;
}

export interface ServiceList {
  total: number;
  errors: any[];
  services: Service[];
}

export interface ConfigListResponse {
  listMeta: {
    totalItems: number;
  };
  apps: {
    name: string;
    profile: string;
    label: string;
    fileName: string;
    source: {
      [key: string]: number | string | boolean;
    };
  }[];
  errors: any[];
}

export interface ConfigList {
  total: number;
  errors: any[];
  configs: {
    name: string;
    profile: string;
    label: string;
    fileName: string;
    fileContents: string[];
    __original?: any;
  }[];
}

@Injectable()
export class MicroserviceService {
  constructor(private http: HttpClient) {}

  getConfigs(params: ListParams): Observable<ConfigList> {
    return this.http
      .get<ConfigListResponse>(`api/v1/microservicesconfigs`, {
        params: {
          ...getQuery(
            filterBy(params.searchBy, params.keywords),
            sortBy(params.sort, params.direction === 'desc'),
            pageBy(params.pageIndex, params.pageSize),
          ),
          project_name: params.project,
        },
      })
      .pipe(
        map(res => {
          return {
            total: res.listMeta.totalItems,
            errors: res.errors,
            configs: res.apps.map(config => {
              return {
                name: config.name,
                profile: config.profile,
                label: config.label,
                fileName: (() => {
                  const pos = config.fileName.lastIndexOf('/');
                  return config.fileName.substring(
                    pos + 1,
                    config.fileName.length,
                  );
                })(),
                fileContents: Object.keys(config.source).map((key: string) => {
                  return `${key}: ${config.source[key]}`;
                }),
                __original: config,
              };
            }),
          };
        }),
      );
  }

  getServices(params: ListParams): Observable<ServiceList> {
    return this.http
      .get<ServiceListResponse>(`api/v1/microservicesapps`, {
        params: {
          ...getQuery(
            filterBy(params.searchBy, params.keywords),
            sortBy(params.sort, params.direction === 'desc'),
            pageBy(params.pageIndex, params.pageSize),
          ),
          project_name: params.project,
        },
      })
      .pipe(
        map(res => {
          return {
            total: res.listMeta.totalItems,
            errors: res.errors,
            services: res.apps.map(service => {
              return {
                name: service.name,
                status: service.status,
                podsCount: this.getPodsCount(service.instance),
                __original: service,
              };
            }),
          };
        }),
      );
  }

  private getPodsCount(instance: any) {
    const count = countBy(instance, 'status');
    return {
      desired: instance.length,
      pending: 0,
      available: count.UP || 0,
      unavailable: count.DOWN || 0,
    };
  }
}
