export const ANNOTATION_PREFIX = 'alauda.io';
export const API_GROUP = 'asf.alauda.io';
export const API_VERSION = 'v1alpha1';
export const API_GROUP_VERSION = `${API_GROUP}/${API_VERSION}`;
export const ANNOTATION_DISPLAY_NAME = `${ANNOTATION_PREFIX}/displayName`;
export const ANNOTATION_DESCRIPTION = `${ANNOTATION_PREFIX}/description`;
export const ANNOTATION_PRODUCT = `${ANNOTATION_PREFIX}/product`;
export enum MicroserviceComponentName {
  Zookeeper = 'zookeeper',
  Kafka = 'kafka',
  ConfigServer = 'config-server',
  Eureka = 'eureka',
  Zuul = 'zuul',
  Turbine = 'turbine',
  HystrixDashboard = 'hystrix-dashboard',
  PinpointHbase = 'pinpoint-hbase',
}
