export const en = {
  microservice_environment: 'Microservice Environment',
  install: 'Install',
  reinstall: 'Reinstall',
  cancel_install: 'Cancel Install',
  stop: 'Stop',
  launch: 'Launch',
  complete: 'Complete',
  bind: 'Bind',
  unbind: 'Unbind',
  components: 'Components',
};
export const zh = {
  microservice_environment: '微服务环境',
  install: '安装',
  reinstall: '重新安装',
  cancel_install: '取消安装',
  stop: '停止',
  launch: '启动',
  complete: '完成',
  bind: '绑定',
  unbind: '解绑',
  components: '组件',
};

export default {
  en,
  zh,
};
