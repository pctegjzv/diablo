import { NgModule } from '@angular/core';
import { TranslateService } from '@app/translate';

import { ApiModule } from './api/api.module';

import i18n from './i18n';

@NgModule({
  imports: [ApiModule],
  providers: [],
  exports: [ApiModule],
})
export class ASFModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('asf', i18n);
  }
}
