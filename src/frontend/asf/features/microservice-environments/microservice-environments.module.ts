import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';

import { ConfirmDialogModule } from '../microservice-environments/components/confirm-dialog/confirm-dialog.module';
import { WarningDialogModule } from '../microservice-environments/components/warning-dialog/warning-dialog.module';

import { MicroserviceComponentInstallComponent } from './components/microservice-component-install/microservice-component-install.component';
import { MicroserviceComponentUpdateDialogComponent } from './components/microservice-component-update-dialog/microservice-component-update-dialog.component';
import { MicroserviceComponentComponent } from './components/microservice-component/microservice-component.component';
import { MicroserviceEnvironmentFormComponent } from './components/microservice-environment-form/microservice-environment-form.component';
import { MicroserviceEnvironmentUpdateDialogComponent } from './components/microservice-environment-update-dialog/microservice-environment-update-dialog.component';
import { PodsStatusModule } from './components/pods-status/pods-status.module';
import { StepsComponent } from './components/steps/steps.component';
import { MicroserviceEnvironmentCreateComponent } from './create/microservice-environment-create.component';
import { MicroserviceEnvironmentDetailComponent } from './detail/microservice-environment-detail.component';
import i18n from './i18n';
import { MicroserviceEnvironmentListComponent } from './list/microservice-environment-list.component';
import { MicroserviceEnvironmentRoutingModule } from './microservice-environments-routing.module';
import { MicroserviceEnvironmentSetupComponent } from './setup/microservice-environment-setup.component';

@NgModule({
  declarations: [
    MicroserviceEnvironmentListComponent,
    MicroserviceEnvironmentCreateComponent,
    MicroserviceEnvironmentSetupComponent,
    MicroserviceEnvironmentDetailComponent,
    MicroserviceEnvironmentFormComponent,
    MicroserviceComponentComponent,
    StepsComponent,
    MicroserviceComponentInstallComponent,
    MicroserviceEnvironmentUpdateDialogComponent,
    MicroserviceComponentUpdateDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    WarningDialogModule,
    ConfirmDialogModule,
    MicroserviceEnvironmentRoutingModule,
    PodsStatusModule,
  ],
  providers: [],
  entryComponents: [
    MicroserviceEnvironmentUpdateDialogComponent,
    MicroserviceComponentUpdateDialogComponent,
  ],
})
export class MicroserviceEnvironmentsModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('microservice_environments', i18n);
  }
}
