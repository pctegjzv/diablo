import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@app/translate';
import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
} from '@asf/asf.constants';
import { ToastService } from 'alauda-ui';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  MicroserviceEnvironment,
  MicroserviceEnvironmentService,
} from '../../../api/microservice-environment-api.service';
import { ConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';
import { MicroserviceEnvironmentUpdateDialogComponent } from '../components/microservice-environment-update-dialog/microservice-environment-update-dialog.component';
import { WarningDialogComponent } from '../components/warning-dialog/warning-dialog.component';

@Component({
  templateUrl: './microservice-environment-detail.component.html',
  styleUrls: ['./microservice-environment-detail.component.scss'],
})
export class MicroserviceEnvironmentDetailComponent
  implements OnInit, OnDestroy {
  detail: MicroserviceEnvironment;
  statusMap = {
    Running: 'success',
    Stopped: 'info',
    Failed: 'error',
    Pending: 'primary',
  };
  name = '';
  subs: Subscription[] = [];
  get hasProject() {
    return this.detail && this.detail.microservicesEnvironmentBindings.length;
  }
  constructor(
    private api: MicroserviceEnvironmentService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private toast: ToastService,
  ) {}

  ngOnInit() {
    this.subs.push(
      this.route.paramMap
        .pipe(map(paramMap => paramMap.get('name')))
        .subscribe(name => {
          this.name = name;
        }),
    );
    this.fetchData();
  }

  fetchData() {
    this.subs.push(
      this.api
        .getDetail(this.name)
        .subscribe((detail: MicroserviceEnvironment) => {
          this.detail = detail;
          this.cdr.markForCheck();
        }),
    );
  }

  update() {
    const dialogRef = this.dialog.open(
      MicroserviceEnvironmentUpdateDialogComponent,
      {
        width: '800px',
        data: {
          name: this.name,
          displayName: this.detail.microservicesEnvironment.objectMeta
            .annotations[ANNOTATION_DISPLAY_NAME],
          description: this.detail.microservicesEnvironment.objectMeta
            .annotations[ANNOTATION_DESCRIPTION],
        },
      },
    );
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.toast.messageSuccess(
          this.translate.get('microservice_environments.update_success'),
        );
        this.fetchData();
      }
    });
  }

  delete() {
    const installing = this.detail.microservicesComponents.some(
      item => item.microservicesComponent.status.status === 'Installing',
    );
    if (installing) {
      this.dialog.open(WarningDialogComponent, {
        width: '600px',
        data: this.translate.get(
          'microservice_environments.delete_installing_warning_tip',
        ),
      });
    } else if (this.hasProject) {
      this.dialog.open(WarningDialogComponent, {
        width: '600px',
        data: this.translate.get(
          'microservice_environments.delete_project_warning_tip',
        ),
      });
    } else {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '800px',
        data: {
          field: this.name,
          title: this.translate.get('microservice_environments.delete'),
          hint: this.translate.get(
            'microservice_environments.delete_confirm_content',
            {
              name: this.name,
            },
          ),
          confirm: this.translate.get('microservice_environments.delete'),
          errorText: this.translate.get(
            'microservice_environments.delete_name_pattern',
          ),
        },
      });
      dialogRef.componentInstance.confirmed.subscribe(() => {
        this.api.delete(this.name).subscribe(result => {
          if (result) {
            this.router.navigate(['../'], {
              relativeTo: this.route,
            });
          }
        });
        dialogRef.componentInstance.submitting = false;
        dialogRef.close();
      });
    }
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
