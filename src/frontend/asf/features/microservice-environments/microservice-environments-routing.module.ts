import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MicroserviceEnvironmentCreateComponent } from './create/microservice-environment-create.component';
import { MicroserviceEnvironmentDetailComponent } from './detail/microservice-environment-detail.component';
import { MicroserviceEnvironmentListComponent } from './list/microservice-environment-list.component';
import { MicroserviceEnvironmentSetupComponent } from './setup/microservice-environment-setup.component';

const routes: Routes = [
  { path: '', component: MicroserviceEnvironmentListComponent },
  { path: 'create', component: MicroserviceEnvironmentCreateComponent },
  { path: ':name', component: MicroserviceEnvironmentDetailComponent },
  { path: ':name/setup', component: MicroserviceEnvironmentSetupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MicroserviceEnvironmentRoutingModule {}
