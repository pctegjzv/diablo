import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@app/translate/translate.service';
import {
  MicroserviceComponent,
  MicroserviceEnvironment,
  MicroserviceEnvironmentService,
} from '@asf/api/microservice-environment-api.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { concatMap, delay, tap } from 'rxjs/operators';

import { MicroserviceComponentInstallComponent } from '../components/microservice-component-install/microservice-component-install.component';
import { Step } from '../components/steps/steps.component';

@Component({
  templateUrl: './microservice-environment-setup.component.html',
  styleUrls: ['./microservice-environment-setup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MicroserviceEnvironmentSetupComponent
  implements OnInit, OnDestroy {
  @ViewChild(MicroserviceComponentInstallComponent)
  installRef: MicroserviceComponentInstallComponent;
  name = '';
  finished = false;
  steps: Step[] = [
    {
      index: 1,
      active: false,
      complete: true,
      text: this.translate.get('microservice_environments.create'),
    },
    {
      index: 2,
      active: true,
      complete: false,
      text: this.translate.get('microservice_environments.install'),
    },
  ];
  components: MicroserviceComponent[] = [];
  subs: Subscription[] = [];
  pollingSub: Subscription;
  detail: MicroserviceEnvironment;
  constructor(
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    private api: MicroserviceEnvironmentService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.subs.push(
      this.route.paramMap.subscribe(params => (this.name = params.get('name'))),
    );
    this.startPolling();
  }

  startPolling() {
    this.pollingSub = this.get().subscribe(detail => {
      if (
        detail.microservicesComponents &&
        detail.microservicesComponents.length === 8
      ) {
        this.detail = detail;
        this.cdr.detectChanges();
        this.pollingSub.unsubscribe();
      }
    });
  }

  get() {
    const pull$ = new BehaviorSubject<void>(null);
    return pull$.pipe(
      concatMap(() => {
        return this.api.getDetail(this.name);
      }),
      delay(1000),
      tap(detail => {
        // 8: total components number
        // TODO: optimize
        if (
          !detail.microservicesComponents ||
          detail.microservicesComponents.length < 8
        ) {
          pull$.next(null);
        }
      }),
    );
  }

  installComplete(result: boolean) {
    this.finished = result;
  }

  complete() {
    this.router.navigate([`../../`], {
      relativeTo: this.route,
    });
  }

  ngOnDestroy() {
    if (this.pollingSub) {
      this.pollingSub.unsubscribe();
    }
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
