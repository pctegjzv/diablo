import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isEqual } from 'lodash';
import { BehaviorSubject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

import {
  ListParams,
  MicroserviceEnvironmentService,
} from '@asf/api/microservice-environment-api.service';

@Component({
  templateUrl: './microservice-environment-list.component.html',
  styleUrls: [
    '../../../asf.common.scss',
    './microservice-environment-list.component.scss',
  ],
})
export class MicroserviceEnvironmentListComponent {
  columns = ['name', 'binding_project', 'components'];
  loading = false;
  statusIconMap: { [key: string]: string } = {
    Unknow: 'default',
    Running: 'success',
    Stopped: 'default',
    Failed: 'error',
    Pending: 'primary',
    UnCreate: 'default',
    Created: 'default',
  };

  selectedRef: { name: string; status: string };
  params$ = combineLatest(this.route.paramMap, this.route.queryParamMap).pipe(
    map(([, queryParams]) => {
      this.loading = true;
      return {
        sort: queryParams.get('sort') || 'name',
        direction: queryParams.get('direction') || 'asc',
      };
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  sort$ = this.params$.pipe(
    map(params => {
      return {
        active: params.sort,
        direction: params.direction,
      };
    }),
    publishReplay(1),
    refCount(),
  );

  filter$ = new BehaviorSubject<string>('');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: MicroserviceEnvironmentService,
  ) {}

  fetch = (params: ListParams) => {
    return combineLatest(this.api.getList(params), this.filter$).pipe(
      map(([list, keyword]) => {
        return list.filter(item => item.objectMeta.name.includes(keyword));
      }),
    );
  };

  sortChange(event: { active: string; direction: string }) {
    this.router.navigate([], {
      queryParams: { sort: event.active, direction: event.direction },
      queryParamsHandling: 'merge',
    });
  }

  getIconName(ref: { name: string; status: string }) {
    return `${ref.name}-${this.statusIconMap[ref.status]}`;
  }
}
