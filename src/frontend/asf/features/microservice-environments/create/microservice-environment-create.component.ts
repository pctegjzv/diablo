import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@app/translate';
import { Subscription } from 'rxjs';

import { MicroserviceEnvironmentFormComponent } from '../components/microservice-environment-form/microservice-environment-form.component';
import { Step } from '../components/steps/steps.component';

@Component({
  templateUrl: './microservice-environment-create.component.html',
  styleUrls: ['./microservice-environment-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MicroserviceEnvironmentCreateComponent implements OnDestroy {
  @ViewChild('form') form: MicroserviceEnvironmentFormComponent;
  subs: Subscription[] = [];
  steps: Step[] = [
    {
      index: 1,
      active: true,
      complete: false,
      text: this.translate.get('microservice_environments.create'),
    },
    {
      index: 2,
      complete: false,
      active: false,
      text: this.translate.get('microservice_environments.install'),
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
  ) { }

  complete(name: string) {
    if (name) {
      this.router.navigate([`../${name}/setup`], {
        relativeTo: this.route,
      });
    } else {
      this.router.navigate([`../`], {
        relativeTo: this.route,
      });
    }
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
