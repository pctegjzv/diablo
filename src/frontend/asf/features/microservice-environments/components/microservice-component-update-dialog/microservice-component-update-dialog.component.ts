import { Component, Inject, OnInit } from '@angular/core';
import { TranslateService } from '@app/translate';
import { MicroserviceEnvironmentService } from '@asf/api/microservice-environment-api.service';
import { DIALOG_DATA, DialogRef, ToastService } from 'alauda-ui';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './microservice-component-update-dialog.component.html',
  styleUrls: ['./microservice-component-update-dialog.component.scss'],
})
export class MicroserviceComponentUpdateDialogComponent implements OnInit {
  editorOptions = {
    language: 'yaml',
  };
  editorConfig = {
    recover: false,
    clear: false,
    diffMode: false,
    format: false,
    find: false,
    copy: false,
  };
  newValue = '';
  updateSubscription: Subscription;
  constructor(
    @Inject(DIALOG_DATA)
    public data: { name: string; namespace: string; value: string },
    private dialog: DialogRef<MicroserviceComponentUpdateDialogComponent>,
    private api: MicroserviceEnvironmentService,
    private toast: ToastService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    this.newValue = this.data.value;
  }

  update() {
    this.updateSubscription = this.api
      .updateComponent(this.data.name, this.data.namespace, this.data.value)
      .subscribe(result => {
        if (result) {
          this.toast.messageSuccess(
            this.translate.get(
              'microservice_environments.component_update_success',
            ),
          );
        }
      });
    this.dialog.close(true);
  }
}
