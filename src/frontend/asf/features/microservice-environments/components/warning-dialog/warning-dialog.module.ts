import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material';
import { TranslateModule } from '@app/translate';
import { ButtonModule, IconModule } from 'alauda-ui';

import { WarningDialogComponent } from './warning-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    ButtonModule,
    IconModule,
    TranslateModule,
  ],
  exports: [WarningDialogComponent],
  declarations: [WarningDialogComponent],
  entryComponents: [WarningDialogComponent],
})
export class WarningDialogModule {}
