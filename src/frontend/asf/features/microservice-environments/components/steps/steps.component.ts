import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

export interface Step {
  index: number;
  active: boolean;
  text: string;
  complete: boolean;
}

@Component({
  selector: 'alo-steps',
  templateUrl: 'steps.component.html',
  styleUrls: ['./steps.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StepsComponent implements OnInit {
  @Input() steps: Step[] = [];
  constructor() {}

  ngOnInit() {}
}
