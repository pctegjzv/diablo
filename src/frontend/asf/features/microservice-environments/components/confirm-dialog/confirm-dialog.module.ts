import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { TranslateModule } from '@app/translate';
import { ButtonModule, FormModule, IconModule, InputModule } from 'alauda-ui';

import { ConfirmDialogComponent } from './confirm-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    ButtonModule,
    IconModule,
    TranslateModule,
    FormModule,
    InputModule,
  ],
  exports: [ConfirmDialogComponent],
  declarations: [ConfirmDialogComponent],
  entryComponents: [ConfirmDialogComponent],
})
export class ConfirmDialogModule {
  constructor() {}
}
