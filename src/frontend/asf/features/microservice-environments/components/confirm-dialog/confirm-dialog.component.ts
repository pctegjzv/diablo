import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'alo-confirm-dialog',
  templateUrl: 'confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
  @ViewChild(NgForm) form: NgForm;
  @Output() confirmed = new EventEmitter<boolean>();
  confirmField = '';
  submitting = false;
  get fieldPattern() {
    return new RegExp(`${this.data.field}`);
  }
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {
      field: string;
      title: string;
      hint: string;
      confirm: string;
      errorText: string;
    } = {
      field: '',
      title: '',
      hint: '',
      confirm: '',
      errorText: '',
    },
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
  ) {}

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    this.confirmed.next(true);
    this.confirmed.complete();
  }

  close() {
    this.dialogRef.close();
  }
}
