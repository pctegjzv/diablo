import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

import {
  MicroserviceComponent,
  MicroserviceEnvironmentBase,
} from '@asf/api/microservice-environment-api.service';
import { MicroserviceComponentName } from '@asf/asf.constants';

@Component({
  selector: 'alo-microservice-component-install',
  templateUrl: './microservice-component-install.component.html',
  styleUrls: ['./microservice-component-install.component.scss'],
})
export class MicroserviceComponentInstallComponent implements OnChanges {
  @Input() components: MicroserviceComponent[] = [];
  @Input() environment: MicroserviceEnvironmentBase;
  @Output() complete = new EventEmitter<boolean>();
  state: {
    name: string;
    prev: string;
    actived: boolean;
  }[];
  extensionEnabled = false;

  get basicComponents() {
    return this.components
      ? this.components
          .filter(c => c.microservicesComponent.spec.type === 0)
          .sort((prev, next) => {
            return (
              prev.microservicesComponent.spec.order -
              next.microservicesComponent.spec.order
            );
          })
      : [];
  }
  get extensionComponents() {
    return this.components
      ? this.components.filter(c => c.microservicesComponent.spec.type === 1)
      : [];
  }
  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.components.currentValue &&
      changes.components.currentValue.length
    ) {
      this.initState();
    }
  }

  initState() {
    this.state = this.basicComponents.map((c, i) => {
      const prev =
        i === 0
          ? ''
          : this.basicComponents[i - 1].microservicesComponent.objectMeta.name;
      let ret;
      if (
        ['Failed', 'UnCreate', 'Installing'].includes(
          c.microservicesComponent.status.status,
        )
      ) {
        ret = {
          name: c.microservicesComponent.objectMeta.name,
          prev,
          actived: false,
        };
        if (
          !prev ||
          !['Failed', 'UnCreate', 'Installing'].includes(
            this.basicComponents[i - 1].microservicesComponent.status.status,
          )
        ) {
          ret.actived = true;
        }
      } else {
        ret = {
          name: c.microservicesComponent.objectMeta.name,
          prev,
          actived: true,
        };
      }
      return ret;
    });
  }

  checkExtensionEnabled() {
    return this.state.every(item => item.actived);
  }

  getRef(name: MicroserviceComponentName) {
    return this.environment.spec.microserviceComponentRefs.find(
      ref => ref.name === name,
    );
  }

  finish(name: string) {
    const next = this.state.find(item => item.prev === name);
    if (next) {
      next.actived = true;
    }
  }
}
