import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';

import { MicroserviceEnvironmentModel } from '../../../../api/microservice-environment-api.service';
import { MicroserviceEnvironmentFormComponent } from '../microservice-environment-form/microservice-environment-form.component';

@Component({
  selector: 'alo-microservice-environment-update-dialog',
  templateUrl: './microservice-environment-update-dialog.component.html',
  styleUrls: ['./microservice-environment-update-dialog.component.scss'],
})
export class MicroserviceEnvironmentUpdateDialogComponent {
  @ViewChild(MicroserviceEnvironmentFormComponent)
  form: MicroserviceEnvironmentFormComponent;
  subs: Subscription[] = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: MicroserviceEnvironmentModel,
    private dialogRef: MatDialogRef<
      MicroserviceEnvironmentUpdateDialogComponent
    >,
  ) {}

  complete(name: string) {
    if (name) {
      this.close(true);
    } else {
      this.close();
    }
  }

  close(result: boolean = false) {
    this.dialogRef.close(result);
  }
}
