import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';
import { Subscription } from 'rxjs';

import {
  MicroserviceEnvironmentBase,
  MicroserviceEnvironmentService,
} from '../../../../api/microservice-environment-api.service';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';
import { WarningDialogComponent } from '../../components/warning-dialog/warning-dialog.component';
import { MicroserviceEnvironmentBindDialogComponent } from '../microservice-environment-bind-dialog/microservice-environment-bind-dialog.component';

@Component({
  selector: 'alo-project-microservice-environment',
  templateUrl: './project-microservice-environment.component.html',
  styleUrls: ['./project-microservice-environment.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProjectMicroserviceEnvironmentComponent
  implements OnInit, OnDestroy {
  @ViewChild(NgForm) form: NgForm;
  @Input() project: string;
  binding = false;
  detail: MicroserviceEnvironmentBase;
  statusIconMap: { [key: string]: string } = {
    Running: 'success',
    Stopped: 'default',
    Failed: 'error',
    Pending: 'primary',
    UnCreate: 'default',
    Created: 'default',
  };
  selectedRef: { name: string; status: string };

  unbindEnabled = false;
  confirmName = '';
  subs: Subscription[] = [];
  confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;
  get name() {
    return this.detail ? this.detail.objectMeta.name : '';
  }
  get namePattern() {
    return new RegExp(`${this.name}`);
  }
  constructor(
    private dialog: MatDialog,
    private api: MicroserviceEnvironmentService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private toast: ToastService,
  ) {}

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.subs.push(
      this.api.getBindingDetail(this.project).subscribe(
        (detail: MicroserviceEnvironmentBase | null) => {
          this.binding = !!detail;
          this.detail = detail;
          this.cdr.detectChanges();
        },
        error => {
          if (error.error) {
            this.toast.alertError(error.error);
          }
        },
      ),
    );
  }

  bind() {
    this.subs.push(
      this.dialog
        .open(MicroserviceEnvironmentBindDialogComponent, {
          width: '800px',
          data: this.project,
        })
        .afterClosed()
        .subscribe(result => {
          if (result) {
            this.fetchData();
          }
        }),
    );
  }

  unbind() {
    this.subs.push(
      this.api.unbindEnabled(this.project).subscribe(result => {
        if (!result) {
          this.dialog.open(WarningDialogComponent, {
            width: '600px',
            data: this.translate.get(
              'microservice_environments.unbind_has_app_title',
            ),
          });
        } else {
          this.confirmDialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '800px',
            data: {
              field: this.name,
              title: this.translate.get('microservice_environments.unbind'),
              hint: this.translate.get(
                'microservice_environments.unbind_confirm_content',
                {
                  name: this.name,
                },
              ),
              confirm: this.translate.get('asf.unbind'),
              errorText: this.translate.get(
                'microservice_environments.unbind_name_pattern',
              ),
            },
          });
          this.confirmDialogRef.componentInstance.confirmed.subscribe(() => {
            this.confirm();
          });
        }
      }),
    );
  }

  confirm() {
    this.subs.push(
      this.api.unbind(this.name, this.project).subscribe(result => {
        this.confirmDialogRef.componentInstance.submitting = false;
        if (result) {
          this.dialog.closeAll();
          this.toast.messageSuccess(
            this.translate.get('microservice_environments.unbind_success'),
          );
          this.fetchData();
          this.cdr.detectChanges();
        } else {
          this.toast.alertError(
            this.translate.get('microservice_environments.unbind_failed'),
          );
        }
      }),
    );
  }

  getIconName(ref: { name: string; status: string }) {
    return `${ref.name}-${this.statusIconMap[ref.status]}`;
  }

  cancel() {
    this.dialog.closeAll();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
    this.cancel();
  }
}
