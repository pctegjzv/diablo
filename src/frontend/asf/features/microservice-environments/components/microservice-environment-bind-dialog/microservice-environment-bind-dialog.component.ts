import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';
import { Subscription } from 'rxjs';

import {
  MicroserviceEnvironmentBindModel,
  MicroserviceEnvironmentService,
} from '../../../../api/microservice-environment-api.service';

@Component({
  templateUrl: './microservice-environment-bind-dialog.component.html',
  styleUrls: ['./microservice-environment-bind-dialog.component.scss'],
})
export class MicroserviceEnvironmentBindDialogComponent
  implements OnInit, OnDestroy {
  @ViewChild(NgForm) form: NgForm;
  subs: Subscription[] = [];
  model: MicroserviceEnvironmentBindModel = {
    name: '',
    namespace: '',
    description: '',
  };
  options: string[] = [];
  saving = false;
  loading = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    private api: MicroserviceEnvironmentService,
    private toast: ToastService,
    private translate: TranslateService,
    private dialogRef: MatDialogRef<MicroserviceEnvironmentBindDialogComponent>,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.loading = true;
    this.subs.push(
      this.api.getOptions().subscribe((options: string[]) => {
        this.options = options;
        this.loading = false;
      }),
    );
  }

  close(bound = false) {
    this.dialogRef.close(bound);
  }

  bind() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.model.namespace = this.data;
    this.model.name = this.model.name.split('(')[0];
    this.saving = true;
    this.subs.push(
      this.api.bind(this.model).subscribe(result => {
        if (result.create_messages[0].success) {
          this.toast.messageSuccess({
            content: this.translate.get(
              'microservice_environments.bind_success',
            ),
          });
        } else {
          this.toast.alertError({
            content: result.create_messages[0].message,
          });
        }
        this.saving = false;
        this.dialogRef.close(result);
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
