import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { TranslateService } from '@app/translate';
import {
  InstallProgress,
  MicroserviceComponent,
  MicroserviceComponentRef,
  MicroserviceEnvironmentService,
} from '@asf/api/microservice-environment-api.service';
import { MicroserviceComponentName } from '@asf/asf.constants';
import { MicroserviceComponentUpdateDialogComponent } from '@asf/features/microservice-environments/components/microservice-component-update-dialog/microservice-component-update-dialog.component';
import { COMPONENT_STATUS } from '@asf/features/microservice-environments/microservice-environments.constant';
import { ConfirmType, DialogService, DialogSize } from 'alauda-ui';

import { BehaviorSubject, Subscription, of } from 'rxjs';

import { catchError, delay, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'alo-microservice-component',
  templateUrl: './microservice-component.component.html',
  styleUrls: ['./microservice-component.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MicroserviceComponentComponent implements OnInit, OnDestroy {
  @ViewChild('stopConfirmTemplate')
  @Input()
  component: MicroserviceComponent;
  @Input() ref: MicroserviceComponentRef;
  @Input() actived = false;
  @Input() type = 'basic';
  @Output() finish = new EventEmitter<string>();
  editorOptions = {
    language: 'yaml',
  };
  editorConfig = {
    recover: false,
    clear: false,
    diffMode: false,
    format: false,
    find: false,
    copy: false,
  };
  currentYaml = '';
  expanded = false;
  statusMap = COMPONENT_STATUS;
  currentStatus = COMPONENT_STATUS.UnCreate;
  subscriptions: Subscription[] = [];
  progress: InstallProgress = {
    currentStep: 0,
    allSteps: 0,
    status: this.statusMap.UnCreate,
    hostpath: undefined,
    podControllerInfo: {},
  };
  podsInfo = {
    available: 0,
    desired: 0,
    pending: 0,
    unavailable: 0,
  };
  hostpath = '';
  pollingSub: Subscription;

  reinstall$ = new BehaviorSubject(false);

  get name() {
    return this.component.microservicesComponent.objectMeta.name;
  }

  get percent() {
    return this.progress.allSteps === 0
      ? '0%'
      : `${Math.round(
          (this.progress.currentStep / this.progress.allSteps) * 100,
        )}%`;
  }

  get stopped() {
    return this.currentStatus === this.statusMap.Stopped;
  }

  get failed() {
    return this.currentStatus === this.statusMap.Failed;
  }

  get pending() {
    return this.currentStatus === this.statusMap.Pending;
  }

  get installed() {
    return [
      this.statusMap.Stopped,
      this.statusMap.Running,
      this.statusMap.Pending,
    ].includes(this.currentStatus);
  }

  get uninstalled() {
    return [this.statusMap.Failed, this.statusMap.UnCreate].includes(
      this.currentStatus,
    );
  }

  get copyText() {
    return `[${this.translate.get('click_to_copy')}] ${this.hostpath ||
      this.ref.hostpath}`;
  }

  constructor(
    private api: MicroserviceEnvironmentService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private dialogService: DialogService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    this.currentStatus = this.statusMap[
      this.component.microservicesComponent.status.status
    ];
    if (this.currentStatus === this.statusMap.Installing) {
      this.startPolling();
    }
    this.currentYaml = this.component.chart.spec.values;
    const podControllerInfo = this.component.microservicesComponent.status
      .podControllerInfo;
    this.podsInfo = {
      available: podControllerInfo.available,
      desired: podControllerInfo.desired,
      unavailable: podControllerInfo.unavailable,
      pending: podControllerInfo.pending,
    };
  }

  install() {
    this.subscriptions.push(
      this.api
        .install(
          this.component.microservicesComponent.objectMeta.namespace,
          this.name,
          this.currentYaml,
        )
        .subscribe(result => {
          if (result) {
            this.currentStatus = this.statusMap.Installing;
            this.cdr.detectChanges();
            this.startPolling();
          }
        }),
    );
  }

  reinstall() {
    this.reinstall$.next(true);
  }

  canCopyHostpath(name: MicroserviceComponentName) {
    return ![
      MicroserviceComponentName.PinpointHbase,
      MicroserviceComponentName.Turbine,
      MicroserviceComponentName.Zuul,
    ].includes(name);
  }

  showCopy() {
    return (
      this.currentStatus === this.statusMap.Running &&
      (this.hostpath || this.ref.hostpath) &&
      this.canCopyHostpath(this.ref.name)
    );
  }

  startPolling() {
    this.pollingSub = this.pollingProgress().subscribe(value => {
      this.progress = value || this.progress;
      if (
        value &&
        value.currentStep === value.allSteps &&
        value.hostpath !== undefined
      ) {
        this.currentStatus = this.statusMap[value.status];
        this.hostpath = value.hostpath;
        this.podsInfo = value.podControllerInfo;
        this.finish.emit(this.name);
        this.pollingSub.unsubscribe();
      }
      this.cdr.markForCheck();
    });
  }

  pollingProgress() {
    const pull$ = new BehaviorSubject<void>(null);
    return pull$.pipe(
      switchMap(() => {
        return this.api
          .getInstallProgress(
            this.component.microservicesComponent.spec
              .microservicesEnvironmentName,
            this.name,
          )
          .pipe(catchError(() => of(null)));
      }),
      delay(3000),
      tap(value => {
        if (
          !value ||
          value.currentStep < value.allSteps ||
          value.hostpath === undefined
        ) {
          pull$.next(null);
        }
      }),
    );
  }

  expand() {
    if (!this.actived) {
      return;
    }
    this.expanded = !this.expanded;
  }

  stop() {
    this.dialogService
      .confirm({
        title: this.translate.get(
          'microservice_environments.component_stop_confirm_title',
          {
            name: this.name,
          },
        ),
        content: this.translate.get(
          'microservice_environments.component_stop_confirm_content',
        ),
        confirmType: ConfirmType.Primary,
      })
      .then(() => {})
      .catch(() => {});
  }

  launch() {
    // TODO
  }

  update() {
    const dialogRef = this.dialogService.open(
      MicroserviceComponentUpdateDialogComponent,
      {
        size: DialogSize.Large,
        data: {
          name: this.name,
          namespace: this.component.microservicesComponent.objectMeta.namespace,
          value: this.component.microservicesComponent.spec.rawValues,
        },
      },
    );
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  stopConfirm() {
    // TODO
  }

  cancel() {
    this.dialog.closeAll();
  }

  ngOnDestroy() {
    if (this.pollingSub) {
      this.pollingSub.unsubscribe();
    }
    this.subscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
