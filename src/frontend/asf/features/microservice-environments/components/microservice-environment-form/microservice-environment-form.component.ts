import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastService } from 'alauda-ui';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  MicroserviceEnvironmentModel,
  MicroserviceEnvironmentService,
} from '../../../../api/microservice-environment-api.service';

@Component({
  selector: 'alo-microservice-environment-form',
  templateUrl: './microservice-environment-form.component.html',
  styleUrls: ['./microservice-environment-form.component.scss'],
})
export class MicroserviceEnvironmentFormComponent implements OnInit, OnDestroy {
  @Input() data: MicroserviceEnvironmentModel;
  @Output() complete = new EventEmitter<string>();
  @ViewChild(NgForm) form: NgForm;
  model: MicroserviceEnvironmentModel = {
    name: '',
    displayName: '',
    description: '',
  };
  saving = false;
  subs: Subscription[] = [];
  get updating() {
    return !!this.data;
  }
  constructor(
    public http: HttpClient,
    private api: MicroserviceEnvironmentService,
    private toast: ToastService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.updating) {
      this.model = { ...this.data };
    }
  }

  save() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.saving = true;
    this.updating
      ? this.subs.push(
          this.api
            .update(this.model)
            .pipe(
              map(result => {
                return {
                  result,
                };
              }),
            )
            .subscribe(this.postObserver.bind(this)),
        )
      : this.subs.push(
          this.api
            .create(this.model)
            .pipe(
              map(result => {
                return {
                  result: result.create_messages[0].success,
                  message: result.create_messages[0].message,
                };
              }),
            )
            .subscribe(this.postObserver.bind(this)),
        );
  }
  private postObserver(result: { result: boolean; message?: string }) {
    this.saving = false;
    this.cdr.detectChanges();
    if (result.result) {
      this.complete.emit(this.model.name);
    } else {
      this.toast.alertError({
        content: result.message,
      });
    }
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
