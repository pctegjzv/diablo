import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { TranslateModule } from '@app/translate';
import { TooltipModule } from 'alauda-ui';

import { PodsStatusComponent } from './pods-status.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    TooltipModule,
    TranslateModule,
  ],
  exports: [PodsStatusComponent],
  declarations: [PodsStatusComponent],
})
export class PodsStatusModule {
  constructor() {}
}
