import { Component, Input, TemplateRef } from '@angular/core';

export interface PodsCount {
  desired: number;
  pending: number;
  available: number;
  unavailable: number;
}

@Component({
  selector: 'alo-pods-status',
  templateUrl: './pods-status.component.html',
  styleUrls: ['./pods-status.component.scss'],
})
export class PodsStatusComponent {
  @Input() pods: PodsCount;
  @Input() tooltipTpl: TemplateRef<any>;
  constructor() {}
}
