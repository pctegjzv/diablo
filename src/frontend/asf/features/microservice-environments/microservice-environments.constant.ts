export const COMPONENT_STATUS: { [key: string]: string } = {
  UnCreate: 'uncreate',
  Created: 'created',
  Installing: 'installing',
  Failed: 'failed',
  Running: 'running',
  Stopped: 'stopped',
  Pending: 'pending',
};
