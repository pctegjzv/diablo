export const en = {};
export const zh = {
  configs: '配置',
  services: '服务',
  belong_service: '所属服务',
  config_file: '配置文件',
  status: '状态',
  pod_status: '实例状态',
  referenced_resource: '关联资源',
  service_online: '在线',
  service_offline: '离线',
  pod_normal: '正常',
  pod_abnormal: '异常',
  search_by_belong_service_placeholder: '按所属服务搜索',
};

export default {
  en,
  zh,
};
