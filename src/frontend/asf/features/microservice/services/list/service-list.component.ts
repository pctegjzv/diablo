import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ListParams,
  MicroserviceService,
} from '@asf/api/microservice-api.service';
import { PodsCount } from '@asf/features/microservice-environments/components/pods-status/pods-status.component';
import { isEqual } from 'lodash';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

@Component({
  templateUrl: './service-list.component.html',
  styleUrls: [
    '../../../../../asf/asf.common.scss',
    './service-list.component.scss',
  ],
})
export class ServiceListComponent implements OnInit {
  columns: string[] = [
    'name',
    'status',
    'pod_status',
    // 'referenced_resource',
  ];
  loading = false;
  searchBy = 'name';
  podsCount: PodsCount[] = [];
  params$ = combineLatest(this.route.paramMap, this.route.queryParamMap).pipe(
    map(([params, queryParams]) => {
      return {
        keywords: queryParams.get('keywords') || '',
        searchBy: queryParams.get('search_by') || '',
        sort: queryParams.get('sort') || 'name',
        direction: queryParams.get('direction') || 'asc',
        pageIndex: +(queryParams.get('page') || '1') - 1,
        itemsPerPage: +(queryParams.get('page_size') || '10'),
        project: params.get('project'),
      };
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  sort$ = this.params$.pipe(
    map(params => {
      return {
        active: params.sort,
        direction: params.direction,
      };
    }),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  pageIndex$ = this.params$.pipe(
    map(params => params.pageIndex),
    publishReplay(1),
    refCount(),
  );

  itemsPerPage$ = this.params$.pipe(
    map(params => params.itemsPerPage),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: MicroserviceService,
  ) {}

  ngOnInit() {}

  fetch = (params: ListParams) => this.api.getServices(params);

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: {
        search_by: this.searchBy,
        keywords,
        page: 1,
      },
      queryParamsHandling: 'merge',
    });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    this.router.navigate([], {
      queryParams: { page: event.pageIndex + 1, page_size: event.pageSize },
      queryParamsHandling: 'merge',
    });
  }

  sortChange(event: { active: string; direction: string }) {
    this.router.navigate([], {
      queryParams: { sort: event.active, direction: event.direction },
      queryParamsHandling: 'merge',
    });
  }

  getStatus(status: string) {
    return status === 'UP'
      ? 'microservice.service_online'
      : 'microservice.service_offline';
  }
}
