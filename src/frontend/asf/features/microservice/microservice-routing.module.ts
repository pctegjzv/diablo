import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConfigListComponent } from './configs/list/config-list.component';
import { ServiceListComponent } from './services/list/service-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'config',
    pathMatch: 'full',
  },
  {
    path: 'configs',
    component: ConfigListComponent,
  },
  {
    path: 'services',
    component: ServiceListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MicroserviceRoutingModule {}
