import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ListParams,
  MicroserviceService,
} from '@asf/api/microservice-api.service';
import { isEqual } from 'lodash';
import * as monaco from 'monaco-editor';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

@Component({
  templateUrl: './config-list.component.html',
  styleUrls: [
    '../../../../../asf/asf.common.scss',
    './config-list.component.scss',
  ],
})
export class ConfigListComponent {
  @ViewChild('fileContent') fileContent: TemplateRef<any>;
  columns: string[] = ['name', 'config_file', 'label', 'profile'];
  loading = false;
  configs: any;
  searchBy = 'name';
  monacoOptions: monaco.editor.IEditorConstructionOptions = {
    wordWrap: 'on',
    readOnly: true,
    renderLineHighlight: 'none',
    lineNumbers: 'off',
  };
  activedFile: {
    name: string;
    content: string;
  };
  params$ = combineLatest(this.route.paramMap, this.route.queryParamMap).pipe(
    map(([params, queryParams]) => {
      return {
        searchBy: queryParams.get('search_by') || '',
        keywords: queryParams.get('keywords') || '',
        sort: queryParams.get('sort') || 'name',
        direction: queryParams.get('direction') || 'asc',
        pageIndex: +(queryParams.get('page') || '1') - 1,
        itemsPerPage: +(queryParams.get('page_size') || '10'),
        project: params.get('project') || '',
      };
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  sort$ = this.params$.pipe(
    map(params => {
      return {
        active: params.sort,
        direction: params.direction,
      };
    }),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.name),
    publishReplay(1),
    refCount(),
  );

  pageIndex$ = this.params$.pipe(
    map(params => params.pageIndex),
    publishReplay(1),
    refCount(),
  );

  itemsPerPage$ = this.params$.pipe(
    map(params => params.itemsPerPage),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: MicroserviceService,
    private dialog: MatDialog,
  ) {}

  fetch = (params: ListParams) => this.api.getConfigs(params);

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: {
        search_by: this.searchBy,
        keywords,
        page: 1,
      },
      queryParamsHandling: 'merge',
    });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    this.router.navigate([], {
      queryParams: { page: event.pageIndex + 1, page_size: event.pageSize },
      queryParamsHandling: 'merge',
    });
  }

  sortChange(event: { active: string; direction: string }) {
    this.router.navigate([], {
      queryParams: { sort: event.active, direction: event.direction },
      queryParamsHandling: 'merge',
    });
  }

  displayFileContent(name: string, contents: string[]) {
    this.activedFile = {
      name,
      content: contents.join('\n'),
    };
    this.dialog.open(this.fileContent, {
      width: '960px',
      disableClose: true,
    });
  }

  close() {
    this.dialog.closeAll();
  }
}
