import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';
import { PodsStatusModule } from '@asf/features/microservice-environments/components/pods-status/pods-status.module';

import { ConfigListComponent } from './configs/list/config-list.component';
import i18n from './i18n';
import { MicroserviceRoutingModule } from './microservice-routing.module';
import { ServiceListComponent } from './services/list/service-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    MicroserviceRoutingModule,
    PodsStatusModule,
  ],
  exports: [],
  declarations: [ConfigListComponent, ServiceListComponent],
  providers: [],
})
export class MicroserviceModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('microservice', i18n);
  }
}
