import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PipelineApiService } from '@app/api/pipeline/pipeline-api.service';
import { BasicInfoComponent } from '@app/modules/pipeline/components/basic-info/basic-info.component';
import { PipelineCreateContainerComponent } from '@app/modules/pipeline/components/create/pipeline-create-container.component';
import { PipelineHistoriesComponent } from '@app/modules/pipeline/components/histories/histories.component';
import { PipelineHistoryBasicInfoComponent } from '@app/modules/pipeline/components/history-basic-info/history-basic-info.component';
import { PipelineHistoryDetailLogComponent } from '@app/modules/pipeline/components/history-detail-log/history-detail-log.component';
import { HistoryPreviewComponent } from '@app/modules/pipeline/components/history-preview/history-preview.component';
import { PipelineHistoryStepComponent } from '@app/modules/pipeline/components/history-step/history-step.component';
import { LogsComponent } from '@app/modules/pipeline/components/logs/logs.component';
import { PipelineDesignContainerComponent } from '@app/modules/pipeline/components/pipeline-design/pipeline-design-container.component';
import { PipelineDesignSourceComponent } from '@app/modules/pipeline/components/pipeline-design/source/pipeline-design-source.component';
import { PipelineListComponent } from '@app/modules/pipeline/components/pipeline-list/pipeline-list.component';
import { PipelineTemplateBasicInfoComponent } from '@app/modules/pipeline/components/template/basic-info/pipeline-template-basic-info.component';
import { PipelineTemplateDetailParameterTableComponent } from '@app/modules/pipeline/components/template/detail-parameter-table/pipeline-template-detail-parameter-table.component';
import { PipelineTemplateDetailComponent } from '@app/modules/pipeline/components/template/detail/pipeline-template-detail.component';
import { PipelineTemplateListCardComponent } from '@app/modules/pipeline/components/template/list-card//pipeline-template-list-card.component';
import { PipelineTemplateListComponent } from '@app/modules/pipeline/components/template/list/pipeline-template-list.component';
import { PipelineTemplateComponent } from '@app/modules/pipeline/components/template/pipeline-template/pipeline-template.component';
import { PipelineTemplateSettingComponent } from '@app/modules/pipeline/components/template/setting/pipeline-template-setting.component';
import { PipelineTemplateSyncReportComponent } from '@app/modules/pipeline/components/template/sync-report/pipeline-template-sync-report.component';
import { SecretModule } from '@app/modules/secret';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';
import { PipelineDiagramModule } from 'alauda-ui-pipeline';

import {
  PipelineBasicFormComponent,
  PipelineRepositoryFormComponent,
  PipelineScriptFormComponent,
  PipelineTriggerFormComponent,
  RepositorySelectorComponent,
} from './components/forms';
import { PipelineHistoryTableComponent } from './components/history-table/history-table.component';
import { ModeSelectComponent } from './components/mode-select/mode-select.component';
import { StepsComponent } from './components/steps/steps.component';
import { PipelineUpdateContainerComponent } from './components/update/pipeline-update-container.component';
import i18n from './i18n';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    PipelineDiagramModule,
    SecretModule,
  ],
  declarations: [
    PipelineListComponent,
    HistoryPreviewComponent,
    LogsComponent,
    ModeSelectComponent,
    PipelineCreateContainerComponent,
    StepsComponent,
    PipelineBasicFormComponent,
    PipelineRepositoryFormComponent,
    PipelineTriggerFormComponent,
    PipelineScriptFormComponent,
    RepositorySelectorComponent,
    PipelineUpdateContainerComponent,
    PipelineListComponent,
    HistoryPreviewComponent,
    LogsComponent,
    BasicInfoComponent,
    PipelineHistoryTableComponent,
    PipelineHistoriesComponent,
    PipelineDesignContainerComponent,
    PipelineDesignSourceComponent,
    PipelineHistoryDetailLogComponent,
    PipelineHistoryBasicInfoComponent,
    PipelineHistoryStepComponent,
    PipelineTemplateSettingComponent,
    PipelineTemplateListComponent,
    PipelineTemplateDetailComponent,
    PipelineTemplateBasicInfoComponent,
    PipelineTemplateComponent,
    PipelineTemplateListCardComponent,
    PipelineTemplateSyncReportComponent,
    PipelineTemplateDetailParameterTableComponent,
  ],
  exports: [
    PipelineListComponent,
    ModeSelectComponent,
    PipelineCreateContainerComponent,
    RepositorySelectorComponent,
    PipelineUpdateContainerComponent,
    LogsComponent,
    BasicInfoComponent,
    PipelineHistoriesComponent,
    PipelineDesignContainerComponent,
    PipelineHistoryDetailLogComponent,
    PipelineHistoryBasicInfoComponent,
    PipelineTemplateSettingComponent,
    PipelineTemplateListComponent,
    PipelineTemplateDetailComponent,
    PipelineTemplateBasicInfoComponent,
    PipelineTemplateComponent,
    PipelineTemplateListCardComponent,
    PipelineTemplateSyncReportComponent,
    PipelineTemplateDetailParameterTableComponent,
  ],
  providers: [PipelineApiService],
  entryComponents: [
    ModeSelectComponent,
    RepositorySelectorComponent,
    LogsComponent,
    PipelineTemplateSettingComponent,
    PipelineTemplateDetailComponent,
    PipelineTemplateSyncReportComponent,
  ],
})
export class PipelineModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('pipeline', i18n);
  }
}
