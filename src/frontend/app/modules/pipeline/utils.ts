export function mapTriggerTranslateKey(triggerType: string) {
  switch (triggerType) {
    case 'cron':
      return 'pipeline_trigger_cron';
    case 'codeChange':
      return 'pipeline_trigger_code_change';
    case 'imageChange':
      return 'pipeline_trigger_image_change';
    default:
      return 'unknown';
  }
}

export function mapTriggerIcon(triggerType: string) {
  switch (triggerType) {
    case 'cron':
      return 'basic:time';
    case 'codeChange':
      return 'basic:code';
    case 'imageChange':
    // TODO: 添加代码仓库图标
    default:
      return 'unknown';
  }
}

const historyStatusIconMap: { [key: string]: string } = {
  Queued: 'basic:hourglass_half_circle_s',
  Pending: 'basic:play_circle_s',
  Running: 'basic:sync_circle_s',
  Failed: 'basic:close_circle_s',
  Complete: 'check_circle_s',
  Cancelled: 'basic:minus_circle_s',
  Aborted: 'basic:paused_circle_s',
  Unknown: 'basic:question_circle_s',
};

const historyStatusTranslateMap: { [key: string]: string } = {
  Queued: 'pipeline.history_queued',
  Pending: 'pipeline.history_pending',
  Running: 'pipeline.history_running',
  Failed: 'pipeline.history_failed',
  Complete: 'pipeline.history_complete',
  Cancelled: 'pipeline.history_cancelled',
  Aborted: 'pipeline.history_aborted',
  Unknown: 'pipeline.history_unknown',
};

export function getHistoryStatus(phase: string) {
  return {
    icon: historyStatusIconMap[phase] || historyStatusIconMap['Unknown'],
    translateKey:
      historyStatusTranslateMap[phase] || historyStatusTranslateMap['Unknown'],
  };
}

export const PIPELINE_ALL_STATUS = [
  'Queued',
  'Pending',
  'Running',
  'Failed',
  'Complete',
  'Cancelled',
  'Aborted',
  'Unknown',
];
