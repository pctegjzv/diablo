import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChange,
  ViewChild,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PipelineApiService, PipelineConfig } from '@app/api';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';
import { extend, get } from 'lodash';

import { MODEL } from '../create/model.constant';

@Component({
  selector: 'alo-pipeline-update-container',
  templateUrl: './pipeline-update-container.component.html',
  styleUrls: ['./pipeline-update-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineUpdateContainerComponent implements OnChanges {
  form: FormGroup;

  @Input() name: string;
  @Input() namespace: string;
  @Input() loading: boolean;
  @Input() pipelineConfig: PipelineConfig;

  @ViewChild('updateForm') updateForm: NgForm;

  get sourceType() {
    return this.form.get('basic').get('source').value;
  }

  constructor(
    private api: PipelineApiService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private toast: ToastService,
  ) {
    this.form = this.formBuilder();
  }

  ngOnChanges({ pipelineConfig }: { pipelineConfig: SimpleChange }) {
    if (pipelineConfig && pipelineConfig.currentValue) {
      const type = get(pipelineConfig.currentValue, 'basic.source', 'repo');
      this.form.patchValue(pipelineConfig.currentValue);
      if (type === 'repo') {
        this.form.removeControl('editor_script');
      } else if (type === 'script') {
        this.form.removeControl('jenkinsfile');
      }
    }
  }

  submit() {
    if (this.submitValid()) {
      const data = {
        ...this.form.value,
        jenkinsfile: {
          ...this.form.value.jenkinsfile,
          ...(this.form.value.jenkinsfile && this.form.value.jenkinsfile.repo),
        },
      };
      this.api
        .updatePipelineConfig({
          namespace: this.namespace,
          name: this.name,
          data: data,
        })
        .subscribe(
          () => {
            this.toast.messageSuccess(
              this.translate.get('pipeline.update_success'),
            );
            this.router.navigate(
              ['../../', `${get(this.form.value, 'basic.name')}`],
              {
                relativeTo: this.route,
              },
            );
          },
          error => {
            this.toast.alertError({
              title: this.translate.get('pipeline.update_fail'),
              content: error.error || error.message,
            });
          },
        );
    }
  }

  getFormArrayControl(name: string) {
    return (<FormArray>this.form.get(name)).controls;
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  private formBuilder() {
    return this.fb.group({
      basic: this.buildGroup(MODEL.basicModel),
      jenkinsfile: this.buildGroup(MODEL.jenkinsfileModel),
      editor_script: this.buildGroup(MODEL.scriptModel),
      triggers: this.fb.array([
        this.buildGroup(
          extend(
            MODEL.triggerModel,
            get(this.pipelineConfig, 'triggers.[0]', ''),
          ),
        ),
        this.buildGroup(
          extend(
            MODEL.triggerModel,
            get(this.pipelineConfig, 'triggers.[1]', ''),
          ),
        ),
      ]),
    });
  }

  private buildGroup(Model: any) {
    return this.fb.group(Model);
  }

  private submitValid() {
    this.updateForm.onSubmit(null);
    return this.updateForm.valid;
  }
}
