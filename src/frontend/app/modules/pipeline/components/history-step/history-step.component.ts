import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import {
  PipelineApiService,
  PipelineHistoryLog,
  PipelineHistoryStep,
} from '@app/api';

import { ToastService } from 'alauda-ui';
import { Subject, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'alo-pipeline-history-step',
  templateUrl: './history-step.component.html',
  styleUrls: ['./history-step.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryStepComponent {
  more = false;
  errorCount = 0;
  next = 0;
  active = false;
  text: string[] = [];
  @Input() step: PipelineHistoryStep;
  @Input() namespace: string;
  @Input() historyName: string;
  @Input() stageId: string;

  params$ = new Subject<boolean>();
  constructor(
    private api: PipelineApiService,
    private cdr: ChangeDetectorRef,
    private toast: ToastService,
  ) {}

  stepLogs() {
    this.params$.next(!this.active);
    this.active = !this.active;
  }

  fetchData = (params: boolean) => {
    if (!params) {
      return of(null);
    }
    return this.api
      .getPipelineHistoryStepLog(this.namespace, this.historyName, {
        start: this.next,
        stage: this.stageId,
        step: this.step.id,
      })
      .pipe(
        tap((res: PipelineHistoryLog) => {
          this.next = res.nextStart;
          this.more = res.more;
          if (res.text) {
            this.text = this.text.concat(res.text.split('\n'));
          }
          this.cdr.detectChanges();
        }),
        catchError(error => {
          if (this.errorCount > 5) {
            this.toast.alertError({
              content: error.error || error.message,
            });
            this.more = false;
            this.cdr.detectChanges();
          }
          this.errorCount++;
          throw error;
        }),
      );
  };

  getStatusIcon() {
    switch (
      this.step.result !== 'UNKNOWN' ? this.step.result : this.step.state
    ) {
      case 'SUCCESS':
        return 'check_s';
      case 'RUNNING':
        return 'basic:sync_circle_s';
      case 'FAILURE':
        return 'basic:close_circle_s';
      case 'ABORTED':
        return 'basic:minus';
      default:
        return 'check_s';
    }
  }

  trackLogs(index: number) {
    return index;
  }
}
