import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { PipelineApiService, PipelineHistory } from '@app/api';
import {
  getHistoryStatus,
  mapTriggerIcon,
  mapTriggerTranslateKey,
} from '@app/modules/pipeline/utils';
import { TranslateService } from '@app/translate';
import { ConfirmType, DialogService, ToastService } from 'alauda-ui';

@Component({
  selector: 'alo-pipeline-history-basic-info',
  templateUrl: './history-basic-info.component.html',
  styleUrls: [
    './history-basic-info.component.scss',
    '../history-table/history-table.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryBasicInfoComponent {
  @Input() history: PipelineHistory;
  @Input() namespace: string;
  @Output() statusChange = new EventEmitter<string>();

  mapTriggerTranslateKey: Function = mapTriggerTranslateKey;
  mapTriggerIcon = mapTriggerIcon;

  constructor(
    private auiDialog: DialogService,
    private api: PipelineApiService,
    private translate: TranslateService,
    private toast: ToastService,
    private router: Router,
  ) {}

  getHistoryStatusIcon(phase: string) {
    return getHistoryStatus(phase);
  }

  getDuration(status: any) {
    if (!status || !status.finishedAt) {
      return 0;
    }
    return (
      new Date(status.finishedAt).getTime() -
      new Date(status.startedAt).getTime()
    );
  }

  delete() {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline.delete_history_confirm', {
          name: this.history.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('delete'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.deletePipeline(this.namespace, this.history.name).subscribe(
            () => {
              this.successNotification('history_delete_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, 'history_delete_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.router.navigate(['../']);
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  replay() {
    this.api.retryPipeline(this.namespace, this.history.name).subscribe(
      (result: PipelineHistory) => {
        this.successNotification('history_replay_succ');
        this.statusChange.emit(result.name);
      },
      (err: any) => {
        this.errorMessage(err, 'history_replay_fail');
      },
    );
  }

  cancel() {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline.cancel_history_confirm', {
          name: this.history.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline.cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.abortPipeline(this.namespace, this.history.name).subscribe(
            () => {
              this.successNotification('history_cancel_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, 'history_cancel_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.statusChange.emit();
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  private errorMessage(err: any, translationKey: string) {
    this.toast.alertError({
      title: this.translate.get(`pipeline.${translationKey}`, {
        name: this.history.name,
      }),
      content: err.error || err.message,
    });
  }

  private successNotification(translationKey: string) {
    this.toast.messageSuccess({
      content: this.translate.get(`pipeline.${translationKey}`, {
        name: this.history.name,
      }),
    });
  }
}
