import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChange,
} from '@angular/core';
import { PipelineConfig, PipelineRepositorySource } from '@app/api';
import { RepositoryInfo } from '@app/modules/pipeline/components/pipeline-design/source/pipeline-design-source.component';
import { get, merge } from 'lodash';

@Component({
  selector: 'alo-pipeline-design-container',
  templateUrl: './pipeline-design-container.component.html',
  styleUrls: ['./pipeline-design-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineDesignContainerComponent implements OnChanges {
  @Input()
  pipelineConfig: PipelineConfig & {
    type: string;
    source: RepositoryInfo | string;
  };

  ngOnChanges({ pipelineConfig }: { pipelineConfig: SimpleChange }) {
    if (pipelineConfig && pipelineConfig.currentValue) {
      this.pipelineConfig = this.mapPipelineConfig(this.pipelineConfig);
    }
  }

  private mapPipelineConfig(pipelineConfig: PipelineConfig) {
    const type =
      get(pipelineConfig, 'source.git.uri', '') ||
      get(pipelineConfig, 'source.codeRepository.name', '')
        ? 'repo'
        : 'script';
    const jenkinsfile: string = get(
      pipelineConfig,
      'strategy.jenkins.jenkinsfile',
      '',
    );
    const repository: PipelineRepositorySource = get(pipelineConfig, 'source');
    const path: string = get(
      pipelineConfig,
      'strategy.jenkins.jenkinsfilePath',
      '',
    );
    const source =
      type === 'repo'
        ? {
            path,
            repo:
              get(repository, 'git.uri', '') ||
              get(repository, 'codeRepository.name', ''),
            branch:
              get(repository, 'git.ref', '') ||
              get(repository, 'codeRepository.ref', ''),
          }
        : jenkinsfile;
    return merge(pipelineConfig, { type, source });
  }
}
