import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
} from '@angular/core';

import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'alo-mode-select',
  templateUrl: './mode-select.component.html',
  styleUrls: ['./mode-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModeSelectComponent implements OnDestroy {
  path: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialog: MatDialog,
  ) {
    this.path = this.data.path;
  }

  hideCreateMethodDialog() {
    this.closeAll();
  }

  ngOnDestroy() {
    this.closeAll();
  }

  closeAll() {
    this.dialog.closeAll();
  }
}
