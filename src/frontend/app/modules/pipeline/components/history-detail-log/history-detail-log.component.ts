import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChange,
} from '@angular/core';
import {
  PipelineApiService,
  PipelineHistory,
  PipelineHistoryLog,
  PipelineHistoryStep,
} from '@app/api';
import { ToastService } from 'alauda-ui';
import { Stage } from 'alauda-ui-pipeline';
import { find, get } from 'lodash';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'alo-pipeline-history-detail-log',
  templateUrl: './history-detail-log.component.html',
  styleUrls: ['./history-detail-log.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryDetailLogComponent implements OnChanges {
  steps: any[];
  fullLog = false;
  selectedStage: Stage;

  next = 0;
  more = true;
  errorCount = 0;
  text = '';

  @Input() history: PipelineHistory;
  @Input() namespace: string;
  constructor(
    private api: PipelineApiService,
    private cdr: ChangeDetectorRef,
    private toast: ToastService,
  ) {}

  ngOnChanges({ history }: { history: SimpleChange }) {
    // default to select stages, running > fail > first
    if (
      history &&
      (get(history.currentValue, 'jenkins.stages') || []).length &&
      !this.selectedStage
    ) {
      const defaultTarget = (find(history.currentValue.jenkins.stages, {
        status: 'RUNNING',
      }) ||
        find(history.currentValue.jenkins.stages, {
          status: 'FINISHED',
          result: 'FAILURE',
        }) ||
        get(history, 'currentValue.jenkins.stages[0]')) as Stage;
      this.selectedStage = defaultTarget;
    }
    // refresh steps when history is refreshed
    if (history && history.currentValue && this.selectedStage) {
      this.getSteps();
    }
  }

  selectedChange(event: Stage) {
    if (this.fullLog) {
      return;
    }
    this.selectedStage = event;
    this.getSteps();
  }

  getSteps() {
    this.api
      .getPipelineHistorySteps(
        this.namespace,
        this.history.name,
        this.selectedStage.id,
      )
      .subscribe(res => {
        this.steps = res.tasks;
        this.cdr.detectChanges();
      });
  }

  fetchLogs = () => {
    if (!this.history) {
      return of('');
    }
    if (!this.more) {
      this.next = 0;
      this.text = '';
    }
    return this.api
      .getPipelineHistoryLog(this.namespace, this.history.name, {
        start: this.next,
      })
      .pipe(
        tap((res: PipelineHistoryLog) => {
          this.next = res.nextStart;
          this.more = res.more;
          this.text = this.text + res.text;
          this.cdr.detectChanges();
        }),
        catchError(error => {
          if (this.errorCount > 5) {
            this.toast.alertError({
              content: error.error || error.message,
            });
            this.more = false;
            this.cdr.detectChanges();
          }
          this.errorCount++;
          throw error;
        }),
      );
  };

  trackById(_: number, item: PipelineHistoryStep) {
    return item.id;
  }
}
