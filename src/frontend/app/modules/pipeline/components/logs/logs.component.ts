import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import {
  PipelineApiService,
  PipelineHistory,
  PipelineHistoryLog,
} from '@app/api';
import { ToastService } from 'alauda-ui';
import { catchError, tap } from 'rxjs/operators';

@Component({
  templateUrl: 'logs.component.html',
  styleUrls: ['logs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogsComponent {
  editorOptions = { language: 'Logs', readOnly: true };
  logs = '';
  next = 0;
  more = true;
  errorCount = 0;
  text = '';

  constructor(
    private toast: ToastService,
    private pipelineApi: PipelineApiService,
    private dialogRef: MatDialogRef<LogsComponent>,
    private cdr: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public history: PipelineHistory,
  ) {}

  fetchLogs = () => {
    return this.pipelineApi
      .getPipelineHistoryLog(this.history.namespace, this.history.name, {
        start: this.next,
      })
      .pipe(
        tap((res: PipelineHistoryLog) => {
          this.next = res.nextStart;
          this.more = res.more;
          this.text = this.text + res.text;
          this.cdr.detectChanges();
        }),
        catchError(error => {
          if (this.errorCount > 5) {
            this.toast.alertError({
              content: error.error || error.message,
            });
            this.more = false;
            this.cdr.detectChanges();
          }
          this.errorCount++;
          throw error;
        }),
      );
  };

  cancel() {
    this.dialogRef.close();
  }
}
