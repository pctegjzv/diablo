import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
} from '@angular/core';
import { PipelineApiService, PipelineHistory, PipelineParams } from '@app/api';
import { isEmpty } from 'lodash';
import { Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { filterBy, getQuery, pageBy, sortBy } from '@app/utils/query-builder';

@Component({
  selector: 'alo-pipeline-histories',
  templateUrl: './histories.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoriesComponent implements AfterViewInit {
  historySearchKey = '';
  historyPageSize = 10;
  historyPageIndex = 0;
  historySortBy = 'creationTimestamp';
  historyDesc = true;
  histories: PipelineHistory[];
  historyParams$: Subject<{
    filterBy?: string;
    sortBy?: string;
    page?: string;
    itemsPerPage?: string;
  }> = new Subject();

  @Input() namespace: string;
  @Input() name: string;
  constructor(private pipelineApi: PipelineApiService) {}

  fetchData = (params: any) => {
    if (!params) {
      return of({ total: 0, histories: [] });
    }
    return this.pipelineApi.getPipelineHistories(this.namespace, params).pipe(
      map(historiesList => {
        historiesList.histories = historiesList.histories.map(
          this.mapPipelineHistories,
        );
        return historiesList;
      }),
    );
  };

  historyParamsChanged(params: {
    pageIndex?: number;
    pageSize?: number;
    searchKey?: string;
    active?: string;
    desc?: boolean;
  }) {
    if (isEmpty(params)) {
      this.historyRefresh();
    } else {
      const { pageIndex, pageSize, searchKey, active, desc } = params;
      this.historyPageIndex =
        pageIndex === undefined ? this.historyPageIndex : pageIndex;
      this.historyPageSize = pageSize || this.historyPageSize;
      this.historySearchKey =
        searchKey === undefined ? this.historySearchKey : searchKey;
      this.historySortBy = active === undefined ? this.historySortBy : active;
      this.historyDesc = desc === undefined ? this.historyDesc : desc;
      this.historyRefresh();
    }
  }

  historyRefresh(reset?: boolean) {
    if (reset) {
      this.historySearchKey = '';
      this.historyPageIndex = 0;
      this.historyPageSize = 10;
      this.historyDesc = true;
      this.historySortBy = 'creationTimestamp';
    }
    this.historyParams$.next(this.queryBuilder());
  }

  private queryBuilder(): PipelineParams {
    return getQuery(
      filterBy('label', `pipelineConfig:${this.name}`),
      filterBy('name', this.historySearchKey),
      sortBy(this.historySortBy, this.historyDesc),
      pageBy(this.historyPageIndex, this.historyPageSize),
    );
  }

  private mapPipelineHistories(history: PipelineHistory) {
    return {
      ...history.status,
      ...history,
      duration:
        new Date(history.status.finishedAt).getTime() -
        new Date(history.status.startedAt).getTime(),
    };
  }

  ngAfterViewInit() {
    this.historyParams$.next(this.queryBuilder());
  }
}
