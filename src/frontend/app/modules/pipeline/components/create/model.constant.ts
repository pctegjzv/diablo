import { FormControl, Validators } from '@angular/forms';

const basicModel = {
  name: [
    '',
    [
      Validators.required,
      Validators.maxLength(36),
      Validators.pattern('[a-z]([-a-z0-9]*[a-z0-9])?'),
    ],
  ],
  display_name: ['', [Validators.maxLength(100)]],
  jenkins_instance: ['', Validators.required],
  app: '',
  source: 'repo',
};

const jenkinsfileModel = {
  repo: [
    { repo: '', secret: '', bindingRepository: '' },
    [Validators.required, codeRepositoryValidator],
  ],
  branch: ['master', Validators.required],
  path: ['Jenkinsfile', Validators.required],
};

const scriptModel = {
  script: ['', Validators.required],
};

const triggerModel = {
  enabled: false,
  cron_string: ['', Validators.maxLength(20)],
};

export const MODEL: any = {
  basicModel,
  jenkinsfileModel,
  scriptModel,
  triggerModel,
};

function codeRepositoryValidator(
  control: FormControl,
): null | { [key: string]: any } {
  const value = control.value;
  if (!value.repo && !value.bindingRepository) {
    return { needRepositoryValue: { value: control.value } };
  } else {
    return null;
  }
}
