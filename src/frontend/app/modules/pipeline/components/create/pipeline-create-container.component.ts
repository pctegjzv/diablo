import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChange,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PipelineApiService, PipelineConfigModel } from '@app/api';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';
import { get } from 'lodash';

import { MODEL } from './model.constant';
interface MODEL {
  basicStep: FormGroup;
  repoStep: FormGroup;
  scriptStep: FormGroup;
  triggerStep: FormGroup;
}

export enum STEPS {
  basic,
  jenkinsfile,
  trigger,
}
@Component({
  selector: 'alo-pipeline-create-container',
  templateUrl: './pipeline-create-container.component.html',
  styleUrls: ['./pipeline-create-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineCreateContainerComponent implements OnChanges {
  model: MODEL;
  stepIndex: STEPS = STEPS.basic;
  stepConfigs = ['basic_info', 'jenkinsfile', 'trigger'];
  jenkinsInstances: any[];

  triggerStep: FormGroup;

  forms: any[] = [];

  @Input() method: 'template' | 'jenkinsfile';
  @Input() templateID = '';
  @Input() namespace: string;
  @Input() type: 'copy' | 'create';
  @Input() name: string;

  constructor(
    private api: PipelineApiService,
    private router: Router,
    private toast: ToastService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
  ) {
    this.model = this.formBuilder();
  }

  ngOnChanges({ type, name }: { type: SimpleChange; name: SimpleChange }) {
    if (type && type.currentValue && name && name.currentValue) {
      if (type.currentValue !== 'copy') {
        return;
      } else {
        this.api
          .getPipelineConfigToModel(this.namespace, name.currentValue)
          .subscribe(
            (pipelineConfig: PipelineConfigModel) => {
              pipelineConfig.basic.name = pipelineConfig.basic.name + '-copy';
              this.model.basicStep.patchValue({ basic: pipelineConfig.basic });
              this.model.repoStep.patchValue({
                jenkinsfile: pipelineConfig.jenkinsfile,
              });
              this.model.scriptStep.patchValue({
                editor_script: pipelineConfig.editor_script,
              });
              this.model.triggerStep.patchValue({
                triggers: pipelineConfig.triggers,
              });
            },
            () => {},
          );
      }
    }
  }

  step(operation: 'next' | 'prev', form?: NgForm) {
    if ((form && !form.onSubmit(null) && form.valid) || operation === 'prev') {
      operation === 'prev' ? this.stepIndex-- : this.stepIndex++;
    }
  }

  submit(triggerForm: NgForm) {
    triggerForm.onSubmit(null);
    if (triggerForm.valid) {
      const jenkinsfile = {
        ...get(this.model, 'repoStep.value.jenkinsfile.repo', ''),
        branch: get(this.model, 'repoStep.value.jenkinsfile.branch', ''),
        path: get(this.model, 'repoStep.value.jenkinsfile.path', ''),
      };
      this.api
        .createPipelineConfig(this.namespace, {
          ...this.model.basicStep.value,
          ...this.model.scriptStep.value,
          ...this.model.triggerStep.value,
          jenkinsfile: jenkinsfile,
        })
        .subscribe(
          () => {
            this.toast.messageSuccess(
              this.translate.get('pipeline.create_success'),
            );
            this.router.navigate(
              ['../', `${get(this.model.basicStep.value, 'basic.name')}`],
              {
                relativeTo: this.route,
              },
            );
          },
          error => {
            this.toast.alertError({
              title: this.translate.get('pipeline.create_fail'),
              content: error.error || error.message,
            });
          },
        );
    }
  }

  cancel() {
    this.router.navigate(['../'], {
      relativeTo: this.route,
    });
  }

  jenkinsChanged(values: any[]) {
    this.jenkinsInstances = values;
  }

  private formBuilder() {
    return {
      basicStep: this.fb.group({ basic: this.buildGroup(MODEL.basicModel) }),
      repoStep: this.fb.group({
        jenkinsfile: this.buildGroup(MODEL.jenkinsfileModel),
      }),
      scriptStep: this.fb.group({
        editor_script: this.buildGroup(MODEL.scriptModel),
      }),
      triggerStep: this.fb.group({
        triggers: this.fb.array([
          this.buildGroup(MODEL.triggerModel),
          this.buildGroup(MODEL.triggerModel),
        ]),
      }),
    };
  }

  private buildGroup(Model: any) {
    return this.fb.group(Model);
  }
}
