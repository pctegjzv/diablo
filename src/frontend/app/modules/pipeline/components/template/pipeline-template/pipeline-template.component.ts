import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { PipelineApiService, PipelineTemplate } from '@app/api';
import { filterBy, getQuery } from '@app/utils/query-builder';
import { get } from 'lodash';
import { Observable, Subject, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'alo-pipeline-template',
  templateUrl: './pipeline-template.component.html',
  styleUrls: ['./pipeline-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateComponent implements OnInit {
  type = 'custom';
  searchKey: string;
  params$ = new Subject<{ search: string }>();
  clusterTemplateCount = 0;
  templateCount = 0;
  @Input() project: string;
  constructor(private pipelineApi: PipelineApiService) {}

  ngOnInit() {
    this.getTemplateList().subscribe(() => {}, () => {});
    this.getClusterTemplateList().subscribe(() => {}, () => {});
  }

  fetchData = (params: { search: string }) => {
    if (!params) {
      of();
    }
    if (this.type === 'official') {
      return this.getClusterTemplateList(params);
    } else {
      return this.getTemplateList(params);
    }
  };

  getClusterTemplateList(params?: any): Observable<any> {
    return this.pipelineApi
      .clusterTemplateList(
        getQuery(filterBy('name', get(params, 'search', ''))),
      )
      .pipe(
        tap((ctemplate: PipelineTemplate[]) => {
          this.clusterTemplateCount = ctemplate.length;
        }),
      );
  }

  getTemplateList(params?: any): Observable<any> {
    return this.pipelineApi
      .templateList(
        this.project,
        getQuery(filterBy('name', get(params, 'search', ''))),
      )
      .pipe(
        tap((template: PipelineTemplate[]) => {
          this.templateCount = template.length;
          if (!this.templateCount) {
            this.type = 'official';
            this.refetchList();
          }
        }),
      );
  }

  search(event: string) {
    this.searchKey = event;
    this.refetchList();
  }

  typeChange(type: string) {
    this.type = type;
    this.refetchList();
  }

  syncChange() {
    this.type = 'custom';
    this.refetchList();
  }

  private refetchList() {
    this.params$.next({ search: this.searchKey });
  }
}
