import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { PipelineTemplate } from '@app/api';
import { templateStagesConvert } from '@app/api/pipeline/utils';
import { PipelineTemplateDetailComponent } from '@app/modules/pipeline/components/template/detail/pipeline-template-detail.component';
import { DialogService, DialogSize } from 'alauda-ui';

@Component({
  selector: 'alo-pipeline-template-list-card',
  templateUrl: './pipeline-template-list-card.component.html',
  styleUrls: ['./pipeline-template-list-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateListCardComponent implements OnInit {
  customLabelIndex = ['sonarqube'];
  @Input() template: PipelineTemplate;
  constructor(private dialog: DialogService) {}

  ngOnInit() {
    this.template.stages = templateStagesConvert(this.template.stages);
  }

  detail() {
    this.dialog.open(PipelineTemplateDetailComponent, {
      size: DialogSize.Large,
      data: {
        template: this.template,
      },
    });
  }
}
