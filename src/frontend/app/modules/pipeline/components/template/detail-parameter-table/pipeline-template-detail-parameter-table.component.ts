import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TemplateArgumentField } from '@app/api';
import { TranslateService } from '@app/translate/translate.service';
import { get } from 'lodash';

@Component({
  selector: 'alo-pipeline-template-detail-parameter-table',
  templateUrl: './pipeline-template-detail-parameter-table.component.html',
  styleUrls: ['./pipeline-template-detail-parameter-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateDetailParameterTableComponent {
  columns = ['parameter', 'description'];
  @Input() parameterField: TemplateArgumentField;
  constructor(private translate: TranslateService) {}

  getValue(path: string, target?: { [key: string]: any }) {
    const currentLang = this.translate.currentLang;
    return get(
      target || this.parameterField,
      `${path}.${currentLang === 'en' ? 'en' : 'zh-CN'}`,
      '',
    );
  }
}
