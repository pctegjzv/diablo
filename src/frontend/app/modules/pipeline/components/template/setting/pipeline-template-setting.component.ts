import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import {
  CodeApiService,
  CodeRepository,
  PipelineApiService,
  PipelineTemplateSync,
  PipelineTemplateSyncConfig,
  Secret,
  SecretApiService,
  SecretTypes,
} from '@app/api';
import { SecretCreateDialogComponent } from '@app/modules/secret';
import { TranslateService } from '@app/translate';
import { Pagination } from '@app/types';
import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
  ToastService,
} from 'alauda-ui';
import { get } from 'lodash';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: './pipeline-template-setting.component.html',
  styleUrls: ['./pipeline-template-setting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateSettingComponent implements OnInit {
  model: any;
  sourceType = 'select';
  secretOptions: Secret[];
  codeOptions: CodeRepository[];

  namespace: string;
  setting: PipelineTemplateSync;
  constructor(
    @Inject(DIALOG_DATA)
    public data: { setting: PipelineTemplateSync; namespace: string },
    private dialogRef: DialogRef<PipelineTemplateSettingComponent>,
    private pipelineApi: PipelineApiService,
    private secretApi: SecretApiService,
    private dialog: DialogService,
    private codeApi: CodeApiService,
    private toast: ToastService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {
    ({ namespace: this.namespace, setting: this.setting } = data);
    this.model = {
      codeRepository: get(data, 'setting.codeRepository.name', ''),
      repo: get(data, 'setting.git.uri', ''),
      branch:
        get(data, 'setting.codeRepository.ref', '') ||
        get(data, 'setting.git.ref', ''),
      secret: get(data, 'setting.secret.name', ''),
    };
    if (this.model.codeRepository) {
      this.model.repo = '';
      this.model.secret = '';
    }
    if (this.model.repo) {
      this.sourceType = 'input';
    }
  }

  ngOnInit() {
    this.fetchData().subscribe(
      ([secrets, codes]) => {
        this.secretOptions = secrets || [];
        this.codeOptions = codes || [];
        this.cdr.detectChanges();
      },
      (error: any) => {
        this.toast.alertError({
          title: this.translate.get('pipeline.api_error'),
          content: error.error || error.message,
        });
      },
    );
  }

  hideDialog() {
    this.dialog.closeAll();
  }

  addSecret() {
    const ref = this.dialog.open(SecretCreateDialogComponent, {
      size: DialogSize.Large,
      data: {
        namespace: this.namespace,
        types: [SecretTypes.basicAuth],
      },
    });
    ref.afterClosed().subscribe((secret: any) => {
      if (secret) {
        this.model.secret = secret.name;
        this.secrets();
      }
    });
  }

  onSubmit(value: any) {
    let data: PipelineTemplateSyncConfig;
    if (this.sourceType === 'select') {
      data = {
        codeRepository: {
          name: value.codeRepository,
          ref: value.branch,
        },
      };
    } else {
      data = {
        git: {
          uri: value.repo,
          ref: value.branch,
        },
      };
      if (value.secret) {
        data['secret'] = {
          name: value.secret,
        };
      }
    }
    if (!this.setting) {
      this.pipelineApi.templateSetting(this.namespace, data).subscribe(
        (result: PipelineTemplateSync) => {
          this.dialogRef.close(result);
          this.toast.messageSuccess({
            content: this.translate.get('pipeline.template_sync_setting_succ'),
          });
        },
        (err: any) => {
          this.toast.alertError({
            title: this.translate.get('pipeline.template_sync_setting_fail'),
            content: err.error || err.message,
          });
        },
      );
    } else {
      this.pipelineApi
        .updateTemplateSetting(this.namespace, this.setting.name, data)
        .subscribe(
          (result: PipelineTemplateSync) => {
            this.dialogRef.close(result);
            this.toast.messageSuccess({
              content: this.translate.get(
                'pipeline.template_sync_setting_update_succ',
              ),
            });
          },
          (err: any) => {
            this.toast.alertError({
              title: this.translate.get(
                'pipeline.template_sync_setting_update_fail',
              ),
              content: err.error || err.message,
            });
          },
        );
    }
  }

  private fetchData() {
    return forkJoin(
      this.secrets().pipe(map(res => res.items)),
      this.codes().pipe(map(res => res.items)),
    );
  }

  private secrets(): Observable<{ items: Secret[]; length: number }> {
    return this.secretApi.find({}, this.namespace);
  }

  private codes(): Observable<Pagination<CodeRepository>> {
    return this.codeApi.findCodeRepositories({ project: this.namespace });
  }
}
