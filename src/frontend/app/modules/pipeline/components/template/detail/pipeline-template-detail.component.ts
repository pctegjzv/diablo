import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { PipelineTemplate } from '@app/api';
import { TranslateService } from '@app/translate';
import { DIALOG_DATA } from 'alauda-ui';
import { get } from 'lodash';

@Component({
  selector: 'alo-pipeline-template-detail',
  templateUrl: './pipeline-template-detail.component.html',
  styleUrls: ['./pipeline-template-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateDetailComponent {
  customLabelIndex = ['sonarqube'];
  template: PipelineTemplate;
  constructor(
    @Inject(DIALOG_DATA) public data: { template: PipelineTemplate },
    private translate: TranslateService,
  ) {
    this.template = data.template;
  }

  getDescription() {
    const currentLang = this.translate.currentLang;
    const t = get(
      this.template,
      ['description', `${currentLang === 'en' ? 'en' : 'zh-CN'}`],
      '',
    );
    return t;
  }
}
