import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PipelineConfig } from '@app/api';
import {
  mapTriggerIcon,
  mapTriggerTranslateKey,
} from '@app/modules/pipeline/utils';

@Component({
  selector: 'alo-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['../../shared-style/fields.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasicInfoComponent {
  @Input() pipeline: PipelineConfig;
  mapTriggerTranslateKey: Function = mapTriggerTranslateKey;
  mapTriggerIcon = mapTriggerIcon;
  constructor() {}
}
