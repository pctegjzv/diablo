import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PipelineApiService, PipelineConfig, PipelineTrigger } from '@app/api';
import {
  mapTriggerIcon,
  mapTriggerTranslateKey,
} from '@app/modules/pipeline/utils';
import { TranslateService } from '@app/translate';
import { ConfirmType, DialogService, ToastService } from 'alauda-ui';

@Component({
  selector: 'alo-pipeline-list',
  templateUrl: 'pipeline-list.component.html',
  styleUrls: ['pipeline-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineListComponent {
  @Input() pipelines: PipelineConfig[] = [];
  @Input()
  columns = ['name', 'history', 'belongs_application', 'triggers', 'actions'];
  @Output() started = new EventEmitter<PipelineConfig>();
  @Output() deleted = new EventEmitter<PipelineConfig>();
  @Output()
  sortChange = new EventEmitter<{ active: string; direction: string }>();

  mapTriggerIcon = mapTriggerIcon;

  constructor(
    private translate: TranslateService,
    private dialog: DialogService,
    private toast: ToastService,
    private router: Router,
    private route: ActivatedRoute,
    private pipelineApi: PipelineApiService,
  ) {}

  getTriggerHint(trigger: PipelineTrigger) {
    return `${this.translate.get(
      trigger.enabled ? 'enabled' : 'not_enabled',
    )} ${this.translate.get(
      mapTriggerTranslateKey(trigger.type),
    )} ${this.translate.get('pipeline_trigger')}, ${this.translate.get(
      'pipeline.trigger_rules',
    )}: ${trigger.rule}`;
  }

  pipelineIdentity(_: number, pipeline: PipelineConfig) {
    return `${pipeline.namespace}/${pipeline.name}`;
  }

  start(pipeline: PipelineConfig) {
    this.pipelineApi
      .triggerPipeline(pipeline.namespace, pipeline.name)
      .subscribe(
        () => {
          this.toast.messageSuccess({
            content: this.translate.get('pipeline_start_succ'),
          });
          this.started.emit(pipeline);
        },
        (err: any) => {
          this.toast.alertError({
            title: this.translate.get('pipeline_start_fail'),
            content: err.error || err.message,
          });
        },
      );
  }

  sortChanged(event: { active: string; direction: string }) {
    this.sortChange.emit(event);
  }

  delete(pipeline: PipelineConfig) {
    this.dialog
      .confirm({
        title: this.translate.get('pipeline_delete_confirm', {
          name: pipeline.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline.sure'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.pipelineApi
            .deletePipelineConfig(pipeline.namespace, pipeline.name)
            .subscribe(
              () => {
                this.toast.messageSuccess({
                  content: this.translate.get('pipeline_delete_succ'),
                });
                resolve();
              },
              (err: any) => {
                this.toast.alertError({
                  title: this.translate.get('pipeline_delete_fail'),
                  content: err.error || err.message,
                });
                reject();
              },
            );
        },
      })
      .then(() => {
        this.deleted.emit(pipeline);
      })
      .catch(() => {});
  }

  update(item: PipelineConfig) {
    this.router.navigate(['./', item.name, 'update'], {
      relativeTo: this.route,
    });
  }

  copy(item: PipelineConfig) {
    this.router.navigate(['./', 'create'], {
      relativeTo: this.route,
      queryParams: {
        type: 'copy',
        name: item.name,
      },
    });
  }
}
