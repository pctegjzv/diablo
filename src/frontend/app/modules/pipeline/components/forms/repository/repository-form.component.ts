import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'alo-pipeline-repository-form',
  templateUrl: './repository-form.component.html',
  styleUrls: ['./repository-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineRepositoryFormComponent {
  @Input() form: FormGroup;
  @Input() type: 'create' | 'update' = 'create';
  @Input() namespace: string;

  @ViewChild('repoForm') formControl: NgForm;

  get values() {
    return this.form.value;
  }
}
