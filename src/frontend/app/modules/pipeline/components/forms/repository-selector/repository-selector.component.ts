import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Optional,
  Output,
  TemplateRef,
  ViewChild,
  forwardRef,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeRepository as CodeRepo } from '@app/api/code/code-api.types';
import {
  Secret,
  SecretApiService,
  SecretTypes,
} from '@app/api/secret-api.service';
import { SecretCreateDialogComponent } from '@app/modules/secret/components/create-dialog/create-dialog.component';
import { DialogService, DialogSize } from 'alauda-ui';

export interface CodeRepository {
  repo: string;
  secret: string;
  bindingRepository: string;
}
@Component({
  selector: 'alo-repository-selector',
  templateUrl: './repository-selector.component.html',
  styleUrls: ['./repository-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RepositorySelectorComponent),
      multi: true,
    },
    RepositorySelectorComponent,
  ],
})
export class RepositorySelectorComponent implements ControlValueAccessor {
  repositoryDisplay: string;
  modelGroup: FormGroup;
  sourceType: string;
  secretOptions: Secret[];
  codeOptions: CodeRepo[];

  @Input() namespace: string;
  @Output() repoChanged = new EventEmitter();
  @ViewChild('repo') repoRef: TemplateRef<any>;

  constructor(
    private dialog: DialogService,
    private secretApi: SecretApiService,
    private cdr: ChangeDetectorRef,
    private codeApi: CodeApiService,
    private fb: FormBuilder,
    @Optional() private parentForm: FormGroupDirective,
  ) {
    this.modelGroup = this.fb.group({
      repo: '',
      secret: '',
      bindingRepository: '',
    });
  }

  open() {
    this.dialog.open(this.repoRef);
    this.onTouched();
    this.secrets();
    this.codes();
  }

  hideDialog() {
    this.dialog.closeAll();
  }

  addSecret() {
    const ref = this.dialog.open(SecretCreateDialogComponent, {
      size: DialogSize.Large,
      data: {
        namespace: this.namespace,
        types: [SecretTypes.basicAuth],
      },
    });
    ref.afterClosed().subscribe((secret: any) => {
      if (secret) {
        this.secrets();
        this.modelGroup.controls.secret.patchValue(secret.name);
      }
    });
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    return (
      this.parentForm && this.parentForm.submitted && this.parentForm.invalid
    );
  }

  getSubmitDisabled() {
    return !(
      (this.sourceType === 'input' && this.modelGroup.controls.repo.valid) ||
      (this.sourceType === 'select' &&
        this.modelGroup.controls.bindingRepository.valid)
    );
  }

  writeValue(value: CodeRepository) {
    if (!value) {
      return;
    }
    this.modelGroup.patchValue(value);
    if (value.repo) {
      this.sourceType = 'input';
      this.repositoryDisplay = value.repo;
    } else {
      this.sourceType = 'select';
      this.repositoryDisplay = value.bindingRepository;
    }
    this.cdr.detectChanges();
  }

  valueChange = (_: any) => {};

  onTouched = () => {};

  registerOnChange(fn: any) {
    this.valueChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  onSubmit() {
    const data = {
      repo: '',
      bindingRepository: '',
      secret: '',
    };
    if (this.sourceType === 'input') {
      this.repositoryDisplay = this.getControlValue('repo');
      data.repo = this.getControlValue('repo');
      data.secret = this.getControlValue('secret');
    } else {
      this.repositoryDisplay = this.getControlValue('bindingRepository');
      data.bindingRepository = this.getControlValue('bindingRepository');
    }
    this.valueChange(data);
    this.dialog.closeAll();
    this.cdr.detectChanges();
  }

  private getControlValue(controlName: string): string {
    return this.modelGroup.controls[controlName].value;
  }

  private secrets() {
    this.secretApi.find({}, this.namespace).subscribe(res => {
      this.secretOptions = res.items || [];
    });
  }

  private codes() {
    this.codeApi
      .findCodeRepositories({ project: this.namespace })
      .subscribe(res => {
        this.codeOptions = res.items;
      });
  }
}
