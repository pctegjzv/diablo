import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApplicationApiService } from '@app/api/application/application-api.service';
import { JenkinsApiService } from '@app/api/jenkins/jenkins-api.service';
import { JenkinsBinding } from '@app/api/jenkins/jenkins-api.types';
import { ToastService } from 'alauda-ui';
import { forkJoin, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'alo-pipeline-basic-form',
  templateUrl: './basic-form.component.html',
  styleUrls: ['./basic-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineBasicFormComponent implements OnInit {
  jenkinsfielSourceType = [
    { value: 'repo', name: 'pipeline.from_repo' },
    { value: 'script', name: 'pipeline.from_script' },
  ];

  apps: any[];
  jenkins: JenkinsBinding[];

  @Input() form: FormGroup;

  @Input() namespace: string;

  @Input() type: 'create' | 'update' = 'create';

  @Output() jenkinsChanged = new EventEmitter<any>();

  get values() {
    return this.form.value;
  }

  constructor(
    private jenkinsApi: JenkinsApiService,
    private appApi: ApplicationApiService,
    private toast: ToastService,
  ) {}

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    forkJoin(this.getJenkins(), this.getApps())
      .pipe(
        tap(([instanceResult, appResult]: any[]) => {
          if (instanceResult.error || appResult.error) {
            this.errorHandler(instanceResult.content || appResult.content);
          }
        }),
      )
      .subscribe();
  }

  getApps() {
    return this.appApi.find(this.namespace).pipe(
      tap(result => {
        this.apps = result.items;
      }),
      catchError(error => {
        this.apps = [];
        return of({ content: error, error: true });
      }),
    );
  }

  getJenkins() {
    return this.jenkinsApi.findBindingsByNamespace(this.namespace, {}).pipe(
      tap(result => {
        this.jenkins = result.items;
        this.jenkinsChanged.emit(this.jenkins);
      }),
      catchError(error => {
        this.jenkins = [];
        return of({ content: error, error: true });
      }),
    );
  }

  private errorHandler(error: any) {
    this.toast.alertError({
      content: error.error || error.message,
    });
  }
}
