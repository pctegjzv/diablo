import {
  ChangeDetectionStrategy,
  Component,
  Input,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'alo-pipeline-script-form',
  templateUrl: './script-form.component.html',
  styleUrls: ['./script-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineScriptFormComponent {
  exampleConfigs = { language: 'Jenkinsfile', readOnly: true };
  options = {
    language: 'Jenkinsfile',
    recover: false,
    diffMode: false,
  };
  originalYaml = `
  pipeline{
    agent any
     stages{
        stage("Clone"){
           steps{
             git url:"https://github.com/example/example.git"
           }
        }
     }
  }
  `;

  @Input() form: FormGroup;

  @ViewChild('example') example: TemplateRef<any>;
  constructor(private dialog: MatDialog) {}

  viewExample() {
    this.dialog.open(this.example, { width: '960px' });
  }

  hideCreateMethodDialog() {
    this.closeAll();
  }

  closeAll() {
    this.dialog.closeAll();
  }
}
