import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'alo-pipeline-trigger-form',
  templateUrl: './trigger-form.component.html',
  styleUrls: ['./trigger-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTriggerFormComponent {
  codeCheckOptions = [
    { name: 'pipeline.trigger_every_2_minute', value: 'H/2 * * * *' },
    { name: 'pipeline.trigger_every_5_minute', value: 'H/5 * * * *' },
    { name: 'pipeline.trigger_every_30_minute', value: 'H/30 * * * *' },
    { name: 'pipeline.trigger_every_hour', value: 'H * * * *' },
  ];

  @Input() form: FormGroup;

  @Input() index: number;

  formValue(name: string) {
    return !!this.form.get(name).value;
  }
}
