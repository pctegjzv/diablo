import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { PipelineApiService, PipelineHistory } from '@app/api';
import { LogsComponent } from '@app/modules/pipeline/components/logs/logs.component';
import { getHistoryStatus } from '@app/modules/pipeline/utils';

import { mapTriggerTranslateKey } from '@app/modules/pipeline/utils';
import { TranslateService } from '@app/translate';
import { ConfirmType, DialogService, ToastService } from 'alauda-ui';
import { interval } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
@Component({
  selector: 'alo-pipeline-history-table',
  templateUrl: './history-table.component.html',
  styleUrls: [
    './history-table.component.scss',
    '../../shared-style/fields.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryTableComponent {
  columns = ['name', 'status', 'startedAt', 'time', 'cause', 'actions'];

  mapTriggerTranslateKey: (_: string) => {} = mapTriggerTranslateKey;

  @Input() historyList: { total: number; histories: PipelineHistory[] };
  @Input() namespace: string;
  @Input() loading: boolean;
  @Output()
  paramsChanged: EventEmitter<{
    pageIndex?: number;
    pageSize?: number;
    searchKey?: string;
    active?: string;
    desc?: boolean;
  }> = new EventEmitter();

  currentDate = interval(2000).pipe(
    map(() => new Date().getTime()),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private dialog: MatDialog,
    private api: PipelineApiService,
    private auiDialog: DialogService,
    private translate: TranslateService,
    private toast: ToastService,
  ) {}

  getHistoryStatusIcon(phase: string) {
    return getHistoryStatus(phase);
  }

  openLogs(history: PipelineHistory) {
    this.dialog.open(LogsComponent, {
      width: '900px',
      data: history,
    });
  }

  search(searchKey: string) {
    this.paramsChanged.emit({ searchKey: searchKey });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    this.paramsChanged.emit({ ...event });
  }

  getDateTimes(dateString: string) {
    return new Date(dateString).getTime();
  }

  sortChange(event: { active: string; direction: string }) {
    this.paramsChanged.emit({
      active: event.active,
      desc: event.direction === 'desc',
    });
  }

  delete(item: PipelineHistory) {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline.delete_history_confirm', {
          name: item.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('delete'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.deletePipeline(this.namespace, item.name).subscribe(
            () => {
              this.successNotification(item, 'history_delete_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, item, 'history_delete_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.paramsChanged.emit({});
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  replay(item: PipelineHistory) {
    this.api.retryPipeline(this.namespace, item.name).subscribe(
      () => {
        this.paramsChanged.emit({});
        this.successNotification(item, 'history_replay_succ');
      },
      (err: any) => {
        this.errorMessage(err, item, 'history_replay_fail');
      },
    );
  }

  cancel(item: PipelineHistory) {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline.cancel_history_confirm', {
          name: item.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline.cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.abortPipeline(this.namespace, item.name).subscribe(
            () => {
              this.successNotification(item, 'history_cancel_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, item, 'history_cancel_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.paramsChanged.emit({});
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  private errorMessage(
    err: any,
    item: PipelineHistory,
    translationKey: string,
  ) {
    this.toast.alertError({
      title: this.translate.get(`pipeline.${translationKey}`, {
        name: item.name,
      }),
      content: err.error || err.message,
    });
  }

  private successNotification(item: PipelineHistory, translationKey: string) {
    this.toast.messageSuccess({
      content: this.translate.get(`pipeline.${translationKey}`, {
        name: item.name,
      }),
    });
  }
}
