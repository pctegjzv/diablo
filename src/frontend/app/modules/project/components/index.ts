export * from './list/list.component';
export * from './detail/detail.component';

// TODO: migrate to modules
export * from '../../../features/projects/secrets/project-secrets.component';
export * from '../../../features/projects/jenkins-bindings/project-jenkins-bindings.component';
