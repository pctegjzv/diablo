import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatDialog } from '@angular/material';

import { Project } from '@app/api';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeBinding } from '@app/api/code/code-api.types';
import { BindingKind } from '@app/api/tool-chain/utils';
import { ProjectUpdateComponent } from '@app/features/projects/update/project-update.component';
import { CodeServiceSelectDialogComponent } from '@app/modules/code';
import { map } from 'rxjs/operators';

@Component({
  selector: 'alo-project-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectDetailComponent {
  @Input() data: Project;
  @Input() disabled: boolean;

  @Output() updated = new EventEmitter();

  activeTab = 'base';

  constructor(private dialog: MatDialog, private codeApi: CodeApiService) {}

  changeTab(tab: string) {
    this.activeTab = tab;
  }

  updateProject() {
    const dialogRef = this.dialog.open(ProjectUpdateComponent, {
      width: '900px',
      data: {
        name: this.data.name,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updated.emit();
      }
    });
  }

  fetchProjectCodeBindings = (project: string) =>
    this.codeApi
      .findBindingsByNamespace(project, null)
      .pipe(map(result => result.items));

  codeBindingRoute = (item: CodeBinding) => [
    '/admin/projects',
    item.namespace,
    BindingKind.CodeRepo,
    item.name,
  ];

  openCodeBindingDialog() {
    this.dialog.open(CodeServiceSelectDialogComponent, {
      data: this.data.name,
      width: '900px',
    });
  }
}
