import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CodeModule } from '@app/modules/code';
import { PipelineModule } from '@app/modules/pipeline/pipeline.module';
import { ToolBindingsSectionComponent } from '@app/modules/project/components/tool-bindings-section/tool-bindings-section.component';
import { SecretModule } from '@app/modules/secret';
import { ToolChainCommonModule } from '@app/modules/tool-chain/tool-chain-common.module';
import { TranslateService } from '@app/translate';

import { MicroserviceEnvironmentService } from '../../../asf/api/microservice-environment-api.service';
import { ConfirmDialogModule } from '../../../asf/features/microservice-environments/components/confirm-dialog/confirm-dialog.module';
import { MicroserviceEnvironmentBindDialogComponent } from '../../../asf/features/microservice-environments/components/microservice-environment-bind-dialog/microservice-environment-bind-dialog.component';
import { ProjectMicroserviceEnvironmentComponent } from '../../../asf/features/microservice-environments/components/project-microservice-environment/project-microservice-environment.component';
import { WarningDialogModule } from '../../../asf/features/microservice-environments/components/warning-dialog/warning-dialog.module';
import { EditJenkinsBindingModule } from '../../features/shared/edit-jenkins-binding';
import { ProjectMembersModule } from '../../features/shared/project-members';
import { SharedModule } from '../../shared';

import {
  ProjectDetailComponent,
  ProjectJenkinsBindingsComponent,
  ProjectListComponent,
  ProjectSecretsComponent,
} from './components';
import i18n from './i18n';

const Components = [
  ProjectListComponent,
  ProjectDetailComponent,
  ProjectSecretsComponent,
  ProjectJenkinsBindingsComponent,
  ProjectMicroserviceEnvironmentComponent,
  MicroserviceEnvironmentBindDialogComponent,
  ToolBindingsSectionComponent,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    SecretModule,
    EditJenkinsBindingModule,
    ProjectMembersModule,
    CodeModule,
    WarningDialogModule,
    ConfirmDialogModule,
    ToolChainCommonModule,
    PipelineModule,
  ],
  providers: [MicroserviceEnvironmentService],
  declarations: Components,
  exports: Components,
  entryComponents: [MicroserviceEnvironmentBindDialogComponent],
})
export class ProjectModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('project', i18n);
  }
}
