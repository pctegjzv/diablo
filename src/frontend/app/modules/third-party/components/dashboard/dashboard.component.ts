import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ThirdPartyApiService, ThirdPartyContent } from '@app/api';

@Component({
  selector: 'alo-third-party-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThirdPartyDashboardComponent {
  @Input() items: ThirdPartyContent[];

  icons = {
    zentao: 'assets/icons/zentao@2x.png',
    mayun: 'assets/icons/gitee@2x.png',
    mayun_project: 'assets/icons/gitee@2x.png',
  };

  schemas: any = {
    zentao: null,
    mayun: {
      columns: {
        human_name: {
          propertyPath: 'human_name',
          translateKey: 'third_party.project_name',
        },
        html_url: {
          propertyPath: 'html_url',
          translateKey: 'third_party.project_path',
        },
        owner_name: {
          propertyPath: 'owner.name',
          translateKey: 'third_party.project_owner',
        },
        pushed_at: {
          propertyPath: 'pushed_at',
          translateKey: 'third_party.project_latest_commit_at',
          type: 'time',
        },
        description: {
          propertyPath: 'description',
          translateKey: 'third_party.project_description',
        },
      },
    },
    mayun_project: {
      columns: {
        project_name: {
          propertyPath: 'repository.full_name',
          translateKey: 'third_party.project_name',
        },
        title: {
          propertyPath: 'title',
          translateKey: 'third_party.task_name',
        },
        state: {
          propertyPath: 'state',
          translateKey: 'third_party.task_status',
        },
        scheduled_time: {
          propertyPath: 'scheduled_time',
          translateKey: 'third_party.scheduled_time',
        },
        created_at: {
          propertyPath: 'third_party.created_at',
          translateKey: 'third_party.created_at',
        },
        started_at: {
          propertyPath: 'third_party.started_at',
          translateKey: 'third_party.started_at',
        },
        finished_at: {
          propertyPath: 'third_party.finished_at',
          translateKey: 'third_party.finished_at',
        },
        owner: {
          propertyPath: 'user.name',
          translateKey: 'third_party.owner',
        },
      },
    },
  };

  constructor(private api: ThirdPartyApiService) {}

  contentTracker(_: number, content: ThirdPartyContent) {
    return content.type;
  }

  contentListFetcher = (type: string) => this.api.get(type);
}
