import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { get } from 'lodash';

@Component({
  selector: 'alo-third-party-content-list',
  templateUrl: 'content-list.component.html',
  styleUrls: ['content-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: 'alo-third-party-content-list',
})
export class ThirdPartyContentListComponent {
  @Input() data: any[] = [];

  @Input() schema: any;

  get columns() {
    return Object.keys(this.schema.columns);
  }

  getTranslateKey(column: string) {
    return this.schema.columns[column].translateKey;
  }

  getCellValue(column: string, item: any) {
    return get(item, this.schema.columns[column].propertyPath) || '';
  }

  getCellType(column: string) {
    return this.schema.columns[column].type || 'text';
  }
}
