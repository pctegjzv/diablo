import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared';
import { TranslateService } from '../../translate';

import {
  ThirdPartyContentListComponent,
  ThirdPartyDashboardComponent,
} from './components';
import i18n from './i18n';

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule],
  declarations: [ThirdPartyDashboardComponent, ThirdPartyContentListComponent],
  exports: [ThirdPartyDashboardComponent],
})
export class ThirdPartyModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('third_party', i18n);
  }
}
