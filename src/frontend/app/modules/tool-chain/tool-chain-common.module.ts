import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BindingListComponent } from '@app/modules/tool-chain/components/binding-list/binding-list.component';
import { IntegrateToolComponent } from '@app/modules/tool-chain/components/integrate-tool/integrate-tool.component';
import { ProjectBindingListComponent } from '@app/modules/tool-chain/components/project-binding-list/project-binding-list.component';
import { SelectServiceComponent } from '@app/modules/tool-chain/components/select-service/select-service.component';
import { ServiceListComponent } from '@app/modules/tool-chain/components/service-list/service-list.component';
import { ToolListComponent } from '@app/modules/tool-chain/components/tool-list/tool-list.component';
import { ToolTypeBarComponent } from '@app/modules/tool-chain/components/tool-type-bar/tool-type-bar.component';
import { UpdateToolComponent } from '@app/modules/tool-chain/components/update-tool/update-tool.component';
import i18n from '@app/modules/tool-chain/i18n';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule, SharedModule],
  declarations: [
    ToolTypeBarComponent,
    ToolListComponent,
    ServiceListComponent,
    BindingListComponent,
    IntegrateToolComponent,
    UpdateToolComponent,
    ProjectBindingListComponent,
    SelectServiceComponent,
  ],
  exports: [
    ToolTypeBarComponent,
    ToolListComponent,
    ServiceListComponent,
    BindingListComponent,
    IntegrateToolComponent,
    UpdateToolComponent,
    ProjectBindingListComponent,
    SelectServiceComponent,
  ],
  entryComponents: [
    IntegrateToolComponent,
    UpdateToolComponent,
    SelectServiceComponent,
  ],
})
export class ToolChainCommonModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('tool_chain', i18n);
  }
}
