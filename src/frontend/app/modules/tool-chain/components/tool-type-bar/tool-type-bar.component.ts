import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  ToolBinding,
  ToolService,
  ToolType,
} from '@app/api/tool-chain/tool-chain-api.types';
import { TranslateService } from '@app/translate';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'alo-tool-type-bar',
  templateUrl: 'tool-type-bar.component.html',
  styleUrls: ['tool-type-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolTypeBarComponent {
  @Input() selectedType = 'all';
  @Input()
  set types(val: ToolType[]) {
    if (val) {
      this.types$$.next(val);
    }
  }
  @Input() resources: Array<ToolService | ToolBinding>;
  @Output() selectedTypeChange = new EventEmitter<string>();

  private types$$ = new BehaviorSubject<ToolType[]>([]);

  types$ = combineLatest(this.types$$, this.translate.currentLang$).pipe(
    map(([types, lang]) =>
      types.map(type => ({
        name: type.name,
        displayName: type.displayName[lang],
      })),
    ),
  );

  constructor(private translate: TranslateService) {}

  handleLabelClicked(type: string) {
    if (type !== this.selectedType) {
      this.selectedTypeChange.emit(type);
    }
  }

  getResourcesCount(type: ToolType) {
    return this.resources.filter(item => {
      const toolType =
        (item as ToolService).toolType || (item as ToolBinding).tool.toolType;
      return toolType === type.name;
    }).length;
  }
}
