import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Tool } from '@app/api/tool-chain/tool-chain-api.types';
import { TranslateService } from '@app/translate';
import { snakeCase } from 'lodash';
import { map } from 'rxjs/operators';

@Component({
  selector: 'alo-tool-list',
  templateUrl: 'tool-list.component.html',
  styleUrls: ['tool-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolListComponent {
  @Input() tools: Tool[];
  @Input()
  @Input()
  showType: boolean;
  @Output() selectedChange = new EventEmitter<Tool>();

  selectedTool = '';

  @Input() tool: Tool;
  @Input() toolType: string;
  @Input() isSelected: boolean;

  snakeCase = snakeCase;

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `assets/icons/enterprise_${lang}.svg`),
  );

  constructor(public translate: TranslateService) {}

  clearSelection() {
    this.selectedTool = '';
    this.selectedChange.emit(null);
  }

  handleCardClicked(tool: Tool) {
    this.selectedTool = tool.name;
    this.selectedChange.emit(tool);
  }

  trackByName(_: number, item: Tool) {
    return item.name;
  }
}
