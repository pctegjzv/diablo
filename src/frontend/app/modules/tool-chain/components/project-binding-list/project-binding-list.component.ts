import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ToolBinding } from '@app/api/tool-chain/tool-chain-api.types';
import { TranslateService } from '@app/translate';
import { snakeCase } from 'lodash';
import { map } from 'rxjs/operators';

@Component({
  selector: 'alo-project-binding-list',
  templateUrl: 'project-binding-list.component.html',
  styleUrls: ['project-binding-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectBindingListComponent {
  @Input() bindings: ToolBinding[];
  @Input() showTag = true;
  @Output() cardClick = new EventEmitter<ToolBinding>();

  snakeCase = snakeCase;

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `assets/icons/enterprise_${lang}.svg`),
  );

  constructor(private translate: TranslateService) {}

  trackByName(_: number, item: ToolBinding) {
    return item.name;
  }
}
