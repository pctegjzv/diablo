import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToolChainApiService } from '@app/api/tool-chain/tool-chain-api.service';
import { Tool, ToolType } from '@app/api/tool-chain/tool-chain-api.types';
import { TranslateService } from '@app/translate';
import {
  DIALOG_DATA,
  DialogRef,
  MessageService,
  NotificationService,
} from 'alauda-ui';

@Component({
  selector: 'alo-integrate-tool',
  templateUrl: 'integrate-tool.component.html',
  styleUrls: ['integrate-tool.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IntegrateToolComponent {
  selectedType = 'all';
  selectedTool: Tool;
  formData: { name: string; host: string } = { name: '', host: '' };
  loading = false;

  @ViewChild('form') formRef: NgForm;

  private allTools: Tool[] = [];

  constructor(
    @Inject(DIALOG_DATA) public toolTypes: ToolType[],
    private dialogRef: DialogRef<
      IntegrateToolComponent,
      { kind: string; name: string }
    >,
    private toolChainApi: ToolChainApiService,
    private message: MessageService,
    private notifaction: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {
    this.toolTypes.forEach(type => {
      this.allTools.push(...type.items);
    });
  }

  getCurrentTools() {
    if (this.selectedType === 'all') {
      return this.allTools;
    } else {
      return this.toolTypes.find(type => type.name === this.selectedType).items;
    }
  }

  handleSelectedToolChange(tool: Tool) {
    this.selectedTool = tool;
    if (tool) {
      this.formData.name = tool.name;
      this.formData.host = tool.host;
    } else {
      this.formData = { name: '', host: '' };
    }
  }

  integrate() {
    this.formRef.onSubmit(null);
    if (this.formRef.invalid) {
      return;
    }
    this.loading = true;
    const currentTool = this.selectedTool;
    const currentData = this.formData;
    this.toolChainApi
      .integrateTool(currentTool.kind, {
        ...currentData,
        type: currentTool.type,
        public: currentTool.public,
      })
      .subscribe(
        () => {
          this.loading = false;
          this.cdr.markForCheck();
          this.message.success(
            `${currentData.name} ${this.translate.get(
              'tool_chain.integrate_successful',
            )}`,
          );
          this.dialogRef.close({
            kind: currentTool.kind,
            name: currentData.name,
          });
        },
        (error: any) => {
          this.loading = false;
          this.cdr.markForCheck();
          this.notifaction.error({
            title: this.translate.get('tool_chain.integrate_failed'),
            content: error.error || error.message,
          });
        },
      );
  }
}
