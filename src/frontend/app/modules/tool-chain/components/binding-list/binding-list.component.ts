import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ToolBinding } from '@app/api/tool-chain/tool-chain-api.types';

@Component({
  selector: 'alo-binding-list',
  templateUrl: 'binding-list.component.html',
  styleUrls: ['binding-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BindingListComponent {
  @Input() bindings: ToolBinding[];
  columns = ['name', 'project', 'secret', 'bind_at'];

  bindingName(_: number, binding: ToolBinding) {
    return binding.name;
  }
}
