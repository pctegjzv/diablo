import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToolChainApiService } from '@app/api/tool-chain/tool-chain-api.service';
import { ToolService } from '@app/api/tool-chain/tool-chain-api.types';
import { TranslateService } from '@app/translate';
import {
  DIALOG_DATA,
  DialogRef,
  MessageService,
  NotificationService,
} from 'alauda-ui';

@Component({
  selector: 'alo-update-tool',
  templateUrl: 'update-tool.component.html',
  styleUrls: ['update-tool.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateToolComponent {
  loading = false;
  @ViewChild('form') formRef: NgForm;

  formData = {
    host: this.tool.host,
  };

  constructor(
    @Inject(DIALOG_DATA) private tool: ToolService,
    private toolChainApi: ToolChainApiService,
    private dialogRef: DialogRef<
      UpdateToolComponent,
      { [key: string]: string }
    >,
    private message: MessageService,
    private notifaction: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  update() {
    this.formRef.onSubmit(null);
    if (this.formRef.invalid) {
      return;
    }
    this.loading = true;
    const currentData = this.formData;
    this.toolChainApi
      .updateTool(this.tool.kind, {
        name: this.tool.name,
        type: this.tool.type,
        public: this.tool.public,
        host: currentData.host,
      })
      .subscribe(
        () => {
          this.loading = false;
          this.cdr.markForCheck();
          this.message.success(
            `${this.tool.name} ${this.translate.get(
              'tool_chain.update_successful',
            )}`,
          );
          this.dialogRef.close(currentData);
        },
        (error: any) => {
          this.loading = false;
          this.cdr.markForCheck();
          this.notifaction.error({
            title: this.translate.get('tool_chain.update_failed'),
            content: error.error || error.message,
          });
        },
      );
  }
}
