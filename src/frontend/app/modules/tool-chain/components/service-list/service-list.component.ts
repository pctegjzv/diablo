import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ToolService } from '@app/api/tool-chain/tool-chain-api.types';
import { TranslateService } from '@app/translate';
import { snakeCase } from 'lodash';
import { map } from 'rxjs/operators';

@Component({
  selector: 'alo-service-list',
  templateUrl: 'service-list.component.html',
  styleUrls: ['service-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListComponent {
  @Input() services: ToolService[];
  @Input() showTag = true;
  @Output() cardClick = new EventEmitter<ToolService>();

  snakeCase = snakeCase;

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `assets/icons/enterprise_${lang}.svg`),
  );

  constructor(private translate: TranslateService) {}

  trackByName(_: number, item: ToolService) {
    return item.name;
  }
}
