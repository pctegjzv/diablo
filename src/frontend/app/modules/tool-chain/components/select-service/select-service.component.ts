import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ToolChainApiService } from '@app/api/tool-chain/tool-chain-api.service';
import {
  ToolService,
  ToolType,
} from '@app/api/tool-chain/tool-chain-api.types';
import { DIALOG_DATA, DialogRef } from 'alauda-ui';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  selector: 'alo-select-service',
  templateUrl: 'select-service.component.html',
  styleUrls: ['select-service.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectServiceComponent {
  selectedType = 'all';

  selectedType$$ = new BehaviorSubject<string>(this.selectedType);
  allServices$ = this.toolChainApi.getToolServices().pipe(
    publishReplay(1),
    refCount(),
  );

  filteredServices$ = combineLatest(
    this.allServices$,
    this.selectedType$$,
  ).pipe(
    map(([tools, selectedType]) =>
      tools.filter(
        tool => selectedType === 'all' || tool.toolType === selectedType,
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  hasServices$ = this.allServices$.pipe(
    map(ins => ins && ins.length),
    publishReplay(1),
    refCount(),
  );

  constructor(
    @Inject(DIALOG_DATA) public data: { types: ToolType[] },
    private toolChainApi: ToolChainApiService,
    private dialogRef: DialogRef<SelectServiceComponent, ToolService>,
  ) {}

  selectService(ins: ToolService) {
    this.dialogRef.close(ins);
  }

  closeSelf() {
    this.dialogRef.close();
  }
}
