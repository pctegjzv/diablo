import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApplicationApiService } from '@app/api';
import { TranslateService } from '@app/translate';
import { DIALOG_DATA, DialogRef, ToastService } from 'alauda-ui';

enum volumeMountTypes {
  PVC = 'PVC',
  ConfigMap = 'ConfigMap',
  Secret = 'Secret',
  HostPath = 'HostPath',
}

@Component({
  selector: 'alo-volume-mount',
  templateUrl: './volume-mount-dialog.component.html',
  styleUrls: ['./volume-mount-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumeMountDialogComponent implements OnInit {
  selectedType: volumeMountTypes;
  pvcParams = {
    pvc: '',
    subpath: '',
    containerpath: '',
  };
  hostpathParams = {
    hostpath: '',
    containerpath: '/',
  };
  configmapParams = {
    configmap: '',
    separatereference: false,
    containerpath: '/',
    key: [['', '']],
  };
  secretParams = {
    secret: '',
    separatereference: false,
    containerpath: '/',
    key: [['', '']],
  };
  title = '';
  isSubmit = false;
  isErrorKey = false;
  submitting = false;
  configmapOptions: any[] = [];
  secretOptions: any[] = [];
  configmapKeyOptions: string[] = [];
  secretKeyOptions: string[] = [];
  @ViewChild('form') form: NgForm;

  constructor(
    private dialogRef: DialogRef<VolumeMountDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      title: string;
      resourceKind: string;
      resourceName: string;
      containerName: string;
      namespace: string;
      isEdit?: boolean;
      editData?: any;
    },
    private api: ApplicationApiService,
    private toast: ToastService,
    private cdr: ChangeDetectorRef,
    public translate: TranslateService,
  ) {
    this.title = data.title;
  }

  ngOnInit() {
    if (this.data.isEdit && this.data.editData) {
      this.selectedType = this.data.editData.type;
      this.handleEditData();
    } else {
      this.selectedType = volumeMountTypes.ConfigMap;
      this.getVolumeMountSelect(volumeMountTypes.ConfigMap);
    }
  }

  typechange(type: volumeMountTypes) {
    this.selectedType = type;
    if (
      type === volumeMountTypes.ConfigMap ||
      type === volumeMountTypes.Secret
    ) {
      this.getVolumeMountSelect(type);
    }
  }

  configmapChange(name: string) {
    const selectedConfigmap = this.configmapOptions.find(
      configmap => name === configmap.objectMeta.name,
    );
    this.configmapKeyOptions = selectedConfigmap.keys;
  }

  secretChange(name: string) {
    const selectedSecret = this.secretOptions.find(
      secret => name === secret.objectMeta.name,
    );
    this.secretKeyOptions = selectedSecret.keys;
  }

  getVolumeMountSelect(type: volumeMountTypes) {
    this.api.getVolumeMount(type, this.data.namespace).subscribe(
      (result: any) => {
        if (type === volumeMountTypes.ConfigMap) {
          this.configmapOptions = result.items;
        } else if (type === volumeMountTypes.Secret) {
          this.secretOptions = result.secrets;
        }
        this.cdr.detectChanges();
      },
      (error: any) => {
        this.toast.alertWarning({
          content: error.error || error.message,
        });
      },
    );
  }

  handleEditData() {
    switch (this.selectedType) {
      case volumeMountTypes.HostPath:
        this.hostpathParams.hostpath = this.data.editData.hostPath;
        this.hostpathParams.containerpath = this.data.editData.volumeMountInfos[0].mountPath;
        break;
      case volumeMountTypes.ConfigMap:
        this.getVolumeMountSelect(this.selectedType);
        this.configmapParams.configmap = this.data.editData.resourceName;
        this.configmapChange(this.data.editData.resourceName);
        if (
          this.data.editData.volumeMountInfos &&
          this.data.editData.volumeMountInfos[0].subPath
        ) {
          this.configmapParams.separatereference = true;
          this.configmapParams.key = this.data.editData.volumeMountInfos.map(
            (volumeMount: any) => {
              return [volumeMount.subPath, volumeMount.mountPath];
            },
          );
        } else {
          this.configmapParams.containerpath = this.data.editData.volumeMountInfos[0].mountPath;
        }
        break;
      case volumeMountTypes.Secret:
        this.getVolumeMountSelect(this.selectedType);
        this.secretParams.secret = this.data.editData.resourceName;
        this.secretChange(this.data.editData.resourceName);
        if (
          this.data.editData.volumeMountInfos &&
          this.data.editData.volumeMountInfos[0].subPath
        ) {
          this.secretParams.separatereference = true;
          this.secretParams.key = this.data.editData.volumeMountInfos.map(
            (volumeMount: any) => {
              return [volumeMount.subPath, volumeMount.mountPath];
            },
          );
        } else {
          this.secretParams.containerpath = this.data.editData.volumeMountInfos[0].mountPath;
        }
        break;
    }
    this.cdr.detectChanges();
  }

  getTypetranlate(type: volumeMountTypes) {
    switch (type) {
      case volumeMountTypes.HostPath:
        return this.translate.get('hostpath');
      case volumeMountTypes.ConfigMap:
        return this.translate.get('application.configmap');
      case volumeMountTypes.Secret:
        return this.translate.get('application.secret');
      case volumeMountTypes.PVC:
        return this.translate.get('persistent_volume_claim');
    }
  }

  checkKeyValid(type: volumeMountTypes) {
    if (
      type === volumeMountTypes.Secret &&
      this.secretParams.separatereference
    ) {
      return this.checkKeyValueData(this.secretParams.key);
    }
    if (
      type === volumeMountTypes.ConfigMap &&
      this.configmapParams.separatereference
    ) {
      return this.checkKeyValueData(this.configmapParams.key);
    }
  }

  checkKeyValueData(keyParams: any[]) {
    if (keyParams.length === 0) {
      this.isErrorKey = true;
      return true;
    } else {
      let hasEmptyValue = false;
      keyParams.forEach(item => {
        if (!item[0] || !item[1]) {
          hasEmptyValue = true;
        }
      });
      if (hasEmptyValue) {
        this.isErrorKey = true;
        return true;
      } else {
        this.isErrorKey = false;
        return false;
      }
    }
  }

  async save() {
    this.isSubmit = true;
    this.form.onSubmit(null);
    this.checkKeyValid(this.selectedType);
    if (this.form.invalid || this.isErrorKey) {
      return;
    }
    const payload = this.handleMountInfos();
    if (this.data.isEdit) {
      this.dialogRef.close(payload);
      return;
    }
    this.submitting = true;
    this.api
      .createVolumeMount(
        this.data.resourceKind,
        this.data.namespace,
        this.data.resourceName,
        this.data.containerName,
        payload,
      )
      .subscribe(
        () => {
          this.dialogRef.close(true);
        },
        (error: any) => {
          this.submitting = false;
          this.toast.alertError({
            title: this.translate.get('add_fail'),
            content: error.error || error.message,
          });
        },
      );
  }

  handleMountInfos() {
    switch (this.selectedType) {
      case volumeMountTypes.HostPath:
        return {
          type: this.selectedType,
          hostPath: this.hostpathParams.hostpath,
          volumeMountInfos: [{ mountPath: this.hostpathParams.containerpath }],
        };
      case volumeMountTypes.ConfigMap:
        return {
          type: this.selectedType,
          resourceName: this.configmapParams.configmap,
          volumeMountInfos: this.configmapParams.separatereference
            ? this.handleKeyValue(this.configmapParams.key)
            : [{ mountPath: this.configmapParams.containerpath }],
        };
      case volumeMountTypes.Secret:
        return {
          type: this.selectedType,
          resourceName: this.secretParams.secret,
          volumeMountInfos: this.secretParams.separatereference
            ? this.handleKeyValue(this.secretParams.key)
            : [{ mountPath: this.secretParams.containerpath }],
        };
    }
  }

  handleKeyValue(params: any) {
    return params.map((item: any) => {
      return { subPath: item[0], mountPath: item[1] };
    });
  }
}
