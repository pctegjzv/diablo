import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'alo-resource-labels-field',
  templateUrl: './resource-labels-field.component.html',
  styleUrls: ['./resource-labels-field.component.scss'],
})
export class ResourceLabelsFieldComponent {
  @Input() resource: any;
  @Input() kind = 'label';
  @Output() updated = new EventEmitter();

  showUpdate() {
    this.updated.next();
  }
}
