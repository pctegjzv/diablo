import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import {
  ApplicationApiService,
  Container,
  ContainerSize,
  ResourceIdentity,
  VolumeInfo,
} from '@app/api';
import { DIALOG_DATA, DialogRef, ToastService } from 'alauda-ui';
import { cloneDeep } from 'lodash';

import { TranslateService } from '@app/translate';

@Component({
  templateUrl: './container-update-differ-dialog.component.html',
  styleUrls: ['./container-update-differ-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerUpdateDifferDialogComponent implements OnInit {
  saving = false;

  collapseDetail = true;

  constructor(
    private api: ApplicationApiService,
    private cdr: ChangeDetectorRef,
    private toast: ToastService,
    private translate: TranslateService,
    private dialogRef: DialogRef,
    @Inject(DIALOG_DATA)
    public data: {
      resourceName: string;
      params: ResourceIdentity;
      kind: string;
      container: Container;
      volumeInfo: VolumeInfo[];
    },
  ) {}

  ngOnInit() {}

  save() {
    this.saving = true;
    const resources = this.handleResource(this.data.container.resources);
    const payload = {
      container: {
        image: this.data.container.image,
        resources: resources,
        env: this.data.container.env,
        envFrom: this.data.container.envFrom,
      },
      VolumeInfo: this.data.volumeInfo,
    };
    this.api
      .putContainer(
        this.data.kind,
        this.data.params.namespace,
        this.data.params.resourceName,
        this.data.container.name,
        payload,
      )
      .subscribe(
        () => {
          this.saving = false;
          this.toast.messageSuccess({
            content: this.translate.get('application.container_update_success'),
          });
          this.cdr.detectChanges();
          this.dialogRef.close(true);
        },
        (error: any) => {
          this.toast.alertError({
            title: this.translate.get('application.container_update_fail'),
            content: error.error || error.message,
          });
          this.saving = false;
          this.cdr.detectChanges();
        },
      );
  }

  handleResource(resources: ContainerSize) {
    const res = cloneDeep(resources);
    if (res.limits.cpu || res.limits.memory) {
      if (!res.limits.cpu) {
        delete res.limits.cpu;
      }
      if (!res.limits.memory) {
        delete res.limits.memory;
      }
    } else {
      delete res.limits;
    }
    if (res.requests.cpu || res.requests.memory) {
      if (!res.requests.cpu) {
        delete res.requests.cpu;
      }
      if (!res.requests.memory) {
        delete res.requests.memory;
      }
    } else {
      delete res.requests;
    }
    return res;
  }

  cancel() {
    this.dialogRef.close();
  }
}
