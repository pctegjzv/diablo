import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { ContainerSize } from '@app/api';
import { get } from 'lodash';

@Component({
  selector: 'alo-container-update-size',
  templateUrl: './container-update-size.component.html',
  styleUrls: ['./container-update-size.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ContainerUpdateSizeComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ContainerUpdateSizeComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerUpdateSizeComponent
  implements ControlValueAccessor, Validator {
  memUnits = ['G', 'M', 'Ti', 'Gi', 'Mi', 'K', 'T', 'P', 'E', 'Ki', 'Pi', 'Ei'];
  cpuUnits = ['m', 'c'];
  memRequestUnit = 'G';
  memRequestValue: number;
  memLimitUnit = 'G';
  memLimitValue: number;
  cpuRequestUnit = 'm';
  cpuRequestValue: number;
  cpuLimitUnit = 'm';
  cpuLimitValue: number;
  resources: ContainerSize;
  isDisabled = false;
  propagateChange = (_: any) => {};

  constructor(private cdr: ChangeDetectorRef) {}

  writeValue(resources: ContainerSize): void {
    this.resources = resources;
    if (resources) {
      this.initResource();
      this.cdr.detectChanges();
    }
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(): void {}

  validate(): ValidationErrors {
    return null;
  }

  inputChange() {
    this.propagateChange(this.resourcesToValue());
  }

  resourcesToValue() {
    const resources: ContainerSize = {
      requests: { cpu: '', memory: '' },
      limits: { cpu: '', memory: '' },
    };
    if (this.memRequestValue) {
      resources.requests.memory = this.memRequestValue + this.memRequestUnit;
    }
    if (this.memLimitValue) {
      resources.limits.memory = this.memLimitValue + this.memLimitUnit;
    }
    if (this.cpuRequestValue) {
      resources.requests.cpu =
        this.cpuRequestValue +
        (this.cpuRequestUnit === 'c' ? '' : this.cpuRequestUnit);
    }
    if (this.cpuLimitValue) {
      resources.limits.cpu =
        this.cpuLimitValue +
        (this.cpuLimitUnit === 'c' ? '' : this.cpuLimitUnit);
    }
    return resources;
  }

  initResource() {
    if (get(this.resources, 'requests.memory')) {
      const { value, unit } = this.handleResourceMemorySize(
        get(this.resources, 'requests.memory'),
      );
      this.memRequestValue = value;
      this.memRequestUnit = unit;
    }
    if (get(this.resources, 'limits.memory')) {
      const { value, unit } = this.handleResourceMemorySize(
        get(this.resources, 'limits.memory'),
      );
      this.memLimitValue = value;
      this.memLimitUnit = unit;
    }
    if (get(this.resources, 'requests.cpu')) {
      const { value, unit } = this.handleResourceCpuSize(
        get(this.resources, 'requests.cpu'),
      );
      this.cpuRequestValue = value;
      this.cpuRequestUnit = unit;
    }
    if (get(this.resources, 'limits.cpu')) {
      const { value, unit } = this.handleResourceCpuSize(
        get(this.resources, 'limits.cpu'),
      );
      this.cpuLimitValue = value;
      this.cpuLimitUnit = unit;
    }
  }

  handleResourceMemorySize(value: string) {
    let resource_value: number;
    let resource_unit: string;
    if (value.slice(-1) === 'i') {
      resource_unit = value.slice(-2);
      resource_value = parseFloat(value.slice(0, -2));
    } else {
      resource_unit = value.slice(-1);
      resource_value = parseFloat(value.slice(0, -1));
    }
    return { value: resource_value, unit: resource_unit };
  }

  handleResourceCpuSize(value: string) {
    let resource_value: number;
    let resource_unit: string;
    resource_unit = value.slice(-1) === 'm' ? 'm' : 'c';
    resource_value =
      resource_unit === 'm'
        ? parseFloat(value.slice(0, -1))
        : parseFloat(value);
    return { value: resource_value, unit: resource_unit };
  }
}
