import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContainerParams, VolumeInfo } from '@app/api';

@Component({
  selector: 'alo-container-update-form',
  templateUrl: './container-update-form.component.html',
  styleUrls: ['./container-update-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerUpdateFormComponent {
  @Input() container: any;
  @Input() volumeInfo: VolumeInfo[];
  @Input() params: ContainerParams;

  @ViewChild('form') form: NgForm;

  constructor() {}

  vilidate() {
    return !this.form || this.form.valid;
  }

  changed() {
    return this.form.dirty;
  }
}
