import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { get } from 'lodash';

import { OtherResource } from '@app/api';

interface Sort {
  active: keyof OtherResource | '';
  direction: 'desc' | 'asc' | '';
}

@Component({
  selector: 'alo-application-other-list',
  templateUrl: 'other-list.component.html',
  styleUrls: ['other-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationOtherListComponent implements OnInit, OnChanges {
  @Input() keywords: string;

  @Input() data: OtherResource[];

  items: OtherResource[];

  sort: Sort = { active: '', direction: '' };

  columns = ['name', 'kind'];

  constructor(private cdr: ChangeDetectorRef) {}

  itemIdentity(_: number, item: OtherResource) {
    return item.name;
  }

  ngOnInit() {
    this.setItems();
  }

  ngOnChanges() {
    this.setItems();
  }

  setItems() {
    const comparator =
      this.sort.direction === 'desc'
        ? <T>(a: T, b: T) => (a === b ? 0 : a < b ? 1 : -1)
        : <T>(a: T, b: T) => (a === b ? 0 : a > b ? 1 : -1);

    const items = (this.data || []).filter(
      item => item.name.indexOf(this.keywords || '') >= 0,
    );

    if (this.sort.active) {
      this.items = [...items].sort((a, b) =>
        comparator(get(a, this.sort.active), get(b, this.sort.active)),
      );
    } else {
      this.items = items;
    }

    this.cdr.detectChanges();
  }

  sortChange(sort: Sort) {
    this.sort = sort;
    this.setItems();
  }
}
