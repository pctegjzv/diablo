import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { PipelineConfig, PipelineIdentity } from '@app/api';
import { K8sResourceMap } from '@app/api';
import {
  AppK8sResource,
  Application,
  ApplicationApiService,
  ApplicationIdentity,
  ApplicationLogParams,
} from '@app/api';
import { ToastService } from 'alauda-ui';

import { TranslateService } from '../../../../translate';
import { ApplicationDeleteDialogComponent } from '../application-delete/application-delete-dialog.component';
import { ApplicationYamlEditDialogComponent } from '../yaml-edit/yaml-edit-dialog.component';

@Component({
  selector: 'alo-application-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailComponent implements OnInit {
  @Input() params: ApplicationIdentity;

  @Input() data: Application;

  @Output() updated = new EventEmitter<void>();
  @Output() deleted = new EventEmitter<void>();

  othersKeywords = '';
  activeTab = 'base';
  yaml = '';
  displayOptions = { language: 'yaml', readOnly: true };
  resourceKinds: string[];
  logParams: ApplicationLogParams;

  changeTab(tab: string) {
    this.activeTab = tab;
    if (tab === 'log') {
      this.logParams = { application: this.data };
    }
  }

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private api: ApplicationApiService,
  ) {
    this.resourceKinds = K8sResourceMap;
  }

  ngOnInit() {
    this.api.getYaml(this.params).subscribe(
      (result: string) => {
        this.yaml = result;
      },
      () => {
        this.toast.alertError({
          content: this.translate.get('yaml_load_fail'),
        });
      },
    );
  }

  tracker(_: number, item: AppK8sResource) {
    return item.name;
  }

  updateByYaml() {
    const dialogRef = this.dialog.open(ApplicationYamlEditDialogComponent, {
      width: '900px',
      data: this.params,
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.updated.emit();
        this.toast.messageSuccess({
          content: this.translate.get(
            'application.application_name_update_success',
            {
              name: this.data.name,
            },
          ),
        });
      }
    });
  }

  confirmDelete() {
    const dialogRef = this.dialog.open(ApplicationDeleteDialogComponent, {
      width: '600px',
      data: {
        application: this.data,
        params: this.params,
      },
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.toast.messageSuccess({
          content: this.translate.get(
            'application.application_name_delete_success',
            {
              name: this.data.name,
            },
          ),
        });
        this.router.navigate(['../'], {
          relativeTo: this.route,
        });
      }
    });
  }

  onPipelineStart(id: PipelineIdentity) {
    this.router.navigate(['../../pipelines', id.name], {
      relativeTo: this.route,
    });
  }

  showLogs(event: {
    resourceName?: string;
    containerName?: string;
    kind?: string;
  }) {
    this.changeTab('log');
    this.logParams = { ...this.logParams, ...event };
  }

  pipelineRoute(pipeline: PipelineConfig) {
    return ['../../pipelines', pipeline.name];
  }
}
