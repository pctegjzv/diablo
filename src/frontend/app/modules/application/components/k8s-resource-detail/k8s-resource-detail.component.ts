import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import {
  ApplicationApiService,
  ApplicationIdentity,
  Container,
  ContainerParams,
  K8sResourceDetail,
  ResourceLogParams,
  StringMap,
} from '@app/api';
import { DialogService, ToastService } from 'alauda-ui';
import { safeDump } from 'js-yaml';
import { get } from 'lodash';

import { TranslateService } from '../../../../translate';
import { UpdateLabelsDialogComponent } from '../update-labels/update-labels-dialog.component';

@Component({
  selector: 'alo-k8s-resource-detail',
  templateUrl: 'k8s-resource-detail.component.html',
  styleUrls: ['k8s-resource-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8sResourceDetailComponent implements OnChanges {
  @Input() params: ApplicationIdentity;
  @Input() data: any;
  @Output() updated = new EventEmitter<void>();
  activeTab = 'base';
  yaml = '';
  selectedContainer = '';
  folded = true;
  logParams: ResourceLogParams;
  displayOptions = { language: 'yaml', readOnly: true };
  result: K8sResourceDetail;
  containerParams: ContainerParams;

  constructor(
    private toast: ToastService,
    private translate: TranslateService,
    private api: ApplicationApiService,
    private cdr: ChangeDetectorRef,
    private dialog: DialogService,
  ) {}

  ngOnChanges() {
    if (this.data) {
      this.initParams();
    }
  }

  initParams() {
    this.result = this.data.data;
    if (!this.selectedContainer) {
      this.selectedContainer = this.result.containers[0].name;
    }
    this.logParams = {
      namespace: this.params.namespace,
      pods: this.result.podInfo.pods,
      containers: this.result.containers,
    };
    this.containerParams = {
      name: this.result.objectMeta.name,
      kind: this.params.kind,
      namespace: this.params.namespace,
      podInfo: this.result.podInfo,
    };
    this.api.getJsonYaml(this.params).subscribe(
      (result: any) => {
        if (this.params.kind) {
          this.yaml = safeDump(
            result.find((resource: any) => {
              return (
                resource.kind.toLocaleLowerCase() ===
                this.params.kind.toLocaleLowerCase()
              );
            }),
          );
          this.cdr.markForCheck();
        }
      },
      () => {
        this.toast.alertError({
          content: this.translate.get('yaml_load_fail'),
        });
      },
    );
    this.cdr.detectChanges();
  }

  get ready() {
    return true;
  }

  get multiContainer() {
    return get(this.result, 'containers', []).length > 1;
  }

  changeTab(tab: string) {
    this.activeTab = tab;
  }

  changeContainer(name: string) {
    this.selectedContainer = name;
  }

  onDesiredChange(event: any) {
    console.log(event);
  }

  containerSelectedLogs() {}

  updateContainerImage() {}

  containerIdentity(_: number, item: Container) {
    return item.name;
  }

  showLogs(event: {
    resourceName?: string;
    containerName?: string;
    kind?: string;
  }) {
    this.logParams.selectedContainerName = event.containerName;
    this.changeTab('log');
  }

  refreshResourceDetail() {
    this.updated.emit();
  }

  toggleFold() {
    this.folded = !this.folded;
    this.cdr.detectChanges();
  }

  updateLabels() {
    const dialogRef = this.dialog.open(UpdateLabelsDialogComponent);
    dialogRef.componentInstance.labels = this.result.objectMeta.labels;
    dialogRef.componentInstance.title = this.translate.get(
      'application.update_labels',
    );
    dialogRef.componentInstance.onUpdate = async (labels: StringMap) => {
      try {
        this.api
          .patchField(
            {
              apiVersion: this.result.data.apiVersion,
              kind: this.params.kind,
              namespace: this.params.namespace,
              name: this.params.resourceName,
            },
            'labels',
            labels,
          )
          .subscribe(() => {
            this.updateSuccess(dialogRef);
          });
      } catch (error) {
        this.toast.alertError({ content: error.error || error });
      }
    };
  }

  updateAnnotations() {
    const dialogRef = this.dialog.open(UpdateLabelsDialogComponent);
    dialogRef.componentInstance.labels = this.result.objectMeta.annotations;
    dialogRef.componentInstance.title = this.translate.get(
      'application.update_annotations',
    );
    dialogRef.componentInstance.onUpdate = async (annotations: StringMap) => {
      try {
        this.api
          .patchField(
            {
              apiVersion: this.result.data.apiVersion,
              kind: this.params.kind,
              namespace: this.params.namespace,
              name: this.params.resourceName,
            },
            'annotations',
            annotations,
          )
          .subscribe(() => {
            this.updateSuccess(dialogRef);
          });
      } catch (error) {
        this.toast.alertError({ content: error.error || error });
      }
    };
  }

  updateSuccess(dialogRef: any) {
    dialogRef.close();
    this.toast.messageSuccess({
      content: this.translate.get('update_succeeded'),
    });
    this.updated.emit();
  }
}
