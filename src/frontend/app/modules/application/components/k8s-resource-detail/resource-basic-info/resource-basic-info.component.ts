import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ApplicationIdentity, K8sResourceDetail } from '@app/api';
import { ApplicationApiService } from '@app/api';
import { TranslateService } from '@app/translate';
import { ConfirmType, DialogService, ToastService } from 'alauda-ui';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'alo-k8s-resource-basic-info',
  templateUrl: './resource-basic-info.component.html',
  styleUrls: ['./resource-basic-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8sResourceBasicInfoComponent {
  @Input() result: K8sResourceDetail;
  @Input() data: ApplicationIdentity;
  @Output() updateLabelsEvent = new EventEmitter();
  @Output() updateAnnotationsEvent = new EventEmitter();
  @Output() updatedreplicasEvent = new EventEmitter<void>();

  scaling = false;

  constructor(
    private dialog: DialogService,
    private translate: TranslateService,
    private toast: ToastService,
    private api: ApplicationApiService,
    private cdr: ChangeDetectorRef,
  ) {}

  updateLabels() {
    this.updateLabelsEvent.next();
  }

  updateAnnotations() {
    this.updateAnnotationsEvent.next();
  }

  onDesiredChange(replicas: number) {
    if (replicas < 0) {
      return;
    }

    if (replicas === 0) {
      this.dialog
        .confirm({
          title: this.translate.get('scale_down_confirm', {
            name: this.data.resourceName,
          }),
          cancelText: this.translate.get('cancel'),
          confirmText: this.translate.get('scale_down'),
          confirmType: ConfirmType.Danger,
          content: this.translate.get('scale_down_confirm_description'),
        })
        .then(() => {
          this.scale(replicas, this.data.kind);
        })
        .catch(() => {});
    } else {
      this.scale(replicas, this.data.kind);
    }
  }

  scale(replicas: number, kind: string) {
    this.scaling = true;
    this.cdr.detectChanges();
    this.api
      .scaleK8sResource(
        this.data.resourceName,
        this.data.namespace,
        kind,
        replicas,
      )
      .pipe(delay(1000))
      .subscribe(
        () => {
          this.scaling = false;
          this.updatedreplicasEvent.emit();
          this.cdr.detectChanges();
        },
        () => {
          this.toast.messageError({
            content: this.translate.get('scale_fail'),
          });
          this.scaling = false;
          this.cdr.detectChanges();
        },
      );
  }
}
