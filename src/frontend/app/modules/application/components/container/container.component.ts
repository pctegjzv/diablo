import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Container, ContainerParams } from '@app/api';
import { TranslateService } from '@app/translate';
import { DialogService } from 'alauda-ui';

import { TerminalService } from '../../../../services';
import { VolumeMountDialogComponent } from '../volume-mount/volume-mount-dialog.component';

import { ImageAddressUpdateDialogComponent } from './image-address/image-address-dialog.component';

@Component({
  selector: 'alo-container',
  templateUrl: 'container.component.html',
  styleUrls: ['container.component.scss'],
})
export class ContainerComponent {
  @Input() resource: ContainerParams;
  @Input() container: Container;
  @Input() displayAdvanced = false;
  @Input() displayContainerName = true;
  @Output() selecteLogs = new EventEmitter<any>();
  @Output() updated = new EventEmitter<void>();
  selectedPodName = '';

  constructor(
    private terminal: TerminalService,
    private dialog: DialogService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  showLogs() {
    this.selecteLogs.next({
      resourceName: this.resource.name,
      containerName: this.container.name,
      kind: this.resource.kind,
    });
  }

  onContainerSelected(podName: string) {
    this.selectedPodName = podName;
    this.showEXEC();
  }

  showEXEC() {
    this.terminal.openTerminal({
      pod: this.selectedPodName,
      container: this.container.name,
      namespace: this.resource.namespace,
      // Optional
      resourceName: this.resource.name,
      resourceKind: this.resource.kind,
    });
  }

  volumeMount() {
    const dialogRef = this.dialog.open(VolumeMountDialogComponent, {
      data: {
        title: this.translate.get('application.add_volume_mounts'),
        resourceKind: this.resource.kind,
        resourceName: this.resource.name,
        containerName: this.container.name,
        namespace: this.resource.namespace,
      },
    });
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.updated.emit();
      }
    });
  }

  updateContainer() {
    this.router.navigate([`../../${this.resource.name}`], {
      relativeTo: this.route,
      queryParams: {
        kind: this.resource.kind,
        name: this.container.name,
      },
    });
  }

  showImageUpdate() {
    const dialogRef = this.dialog.open(ImageAddressUpdateDialogComponent, {
      data: {
        container_name: this.container.name,
        images_address: this.container.image,
        resource: this.resource,
      },
    });
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.updated.emit();
      }
    });
  }
}
