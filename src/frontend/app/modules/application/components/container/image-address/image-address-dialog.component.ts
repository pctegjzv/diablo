import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApplicationApiService, ContainerParams } from '@app/api';
import { TranslateService } from '@app/translate';
import { DIALOG_DATA, DialogRef, ToastService } from 'alauda-ui';

@Component({
  selector: 'alo-image-address-update',
  templateUrl: './image-address-dialog.component.html',
  styleUrls: ['./image-address-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageAddressUpdateDialogComponent implements OnInit {
  containerImageAddress = '';
  isSubmit = false;
  isErrorKey = false;
  @ViewChild('form') form: NgForm;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      container_name: string;
      images_address: string;
      resource: ContainerParams;
    },
    private api: ApplicationApiService,
    private toast: ToastService,
    private translate: TranslateService,
    private dialogRef: DialogRef<ImageAddressUpdateDialogComponent>,
  ) {}

  ngOnInit() {
    this.containerImageAddress = this.data.images_address;
  }

  async save() {
    this.isSubmit = true;
    this.form.onSubmit(null);
    if (this.form.invalid || this.isErrorKey) {
      return;
    }
    this.api
      .putImage(
        this.data.resource.kind,
        this.data.resource.namespace,
        this.data.resource.name,
        this.data.container_name,
        this.form.form.value,
      )
      .subscribe(
        () => {
          this.toast.messageSuccess({
            content: this.translate.get('application.container_update_success'),
          });
          this.dialogRef.close(true);
        },
        (error: any) => {
          this.toast.alertError({
            title: this.translate.get('application.container_update_fail'),
            content: error.error || error.message,
          });
        },
      );
  }
}
