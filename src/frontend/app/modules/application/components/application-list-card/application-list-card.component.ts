import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { ApplicationInfo } from '@app/api';

import { TranslateService } from '@app/translate';

@Component({
  selector: 'alo-application-list-card',
  templateUrl: 'application-list-card.component.html',
  styleUrls: ['application-list-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationListCardComponent implements OnInit, OnChanges {
  @Input() appInfo: ApplicationInfo;
  displayResource = false;
  selectedImages: string[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
  ) {}

  ngOnInit() {}

  ngOnChanges() {
    this.setItems();
  }

  get addressesNum() {
    return this.appInfo.visitAddresses.length;
  }

  setItems() {
    this.cdr.detectChanges();
  }

  viewResource() {
    return false;
  }

  statusMap(status: string) {
    switch (status) {
      case 'Succeeded':
        return 'success';
      case 'Failed':
        return 'error';
      case 'Pending':
        return 'pending';
    }
  }

  getResoureStatus(current: number, desired: number) {
    return `${current} / ${desired}`;
  }

  displayImageAddr(addr: string) {
    if (addr.length <= 45) {
      return addr;
    } else {
      return `${addr.slice(0, 20)}...${addr.slice(-20)}`;
    }
  }

  displayImges(images: string[]) {
    this.selectedImages = images;
  }

  toggleResourceList() {
    if (this.appInfo.resourceList.length > 0) {
      this.displayResource = !this.displayResource;
    }
  }

  getImagesTotal(length: number) {
    return this.translate.get('paginator.total_records', {
      length: length,
    });
  }
}
