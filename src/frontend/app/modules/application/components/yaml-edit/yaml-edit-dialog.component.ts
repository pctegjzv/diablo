import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {
  ApplicationApiService,
  ApplicationIdentity,
  Report,
  toReports,
} from '@app/api';
import { ToastService } from 'alauda-ui';
import { isEmpty } from 'lodash';

import { TranslateService } from '../../../../translate';

@Component({
  templateUrl: 'yaml-edit-dialog.component.html',
  styleUrls: ['yaml-edit-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationYamlEditDialogComponent implements OnInit {
  editorOptions = { language: 'yaml', readOnly: false };
  yaml = '';
  originalYaml = '';

  updating = false;
  updateError = false;
  updatePartSuccess = false;
  reports: Report[];

  constructor(
    private cdr: ChangeDetectorRef,
    private api: ApplicationApiService,
    private toast: ToastService,
    private translate: TranslateService,
    private dialogRef: MatDialogRef<ApplicationYamlEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ApplicationIdentity,
  ) {}

  ngOnInit() {
    this.api.getYaml(this.data).subscribe(
      (result: string) => {
        this.yaml = this.originalYaml = result;
        this.cdr.detectChanges();
      },
      () => {
        this.toast.alertError({
          content: this.translate.get('yaml_load_fail'),
        });
      },
    );
  }

  save() {
    this.updating = true;
    this.updateError = false;
    this.updatePartSuccess = false;
    this.dialogRef.updateSize('400px');
    this.api.putYaml(this.data, this.yaml).subscribe(
      (results: any[]) => {
        this.reports = toReports(results);
        const errorReports = this.reports.filter(report => report.error);
        if (isEmpty(errorReports)) {
          this.dialogRef.close(true);
          return;
        } else if (errorReports.length === this.reports.length) {
          this.updateError = true;
        } else {
          this.updatePartSuccess = true;
        }
        this.cdr.detectChanges();
        this.dialogRef.updateSize('800px');
      },
      (error: any) => {
        this.dialogRef.close(false);
        this.toast.alertError({
          title: this.translate.get('application.application_update_fail'),
          content: error.error || error.message,
        });
      },
    );
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
