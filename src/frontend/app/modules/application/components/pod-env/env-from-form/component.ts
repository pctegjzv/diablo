import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
  forwardRef,
} from '@angular/core';
import { FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AppConfigMap, AppSecret, EnvFromSource, ResourceList } from '@app/api';
import { TranslateService } from '@app/translate';
import { OptionComponent } from 'alauda-ui';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, publishReplay, refCount, switchMap } from 'rxjs/operators';

import { BaseResourceFormComponent } from '../../abstract';
import {
  ENV_FROM_SOURCE_TYPE_TO_KIND,
  KIND_TO_SUPPORTED_ENV_FROM_TYPES,
  SupportedEnvFromSourceKind,
  getEnvFromSource,
  getEnvFromSourceType,
} from '../utils/env-from';

interface EnvFromSourceFormModel {
  kind?: SupportedEnvFromSourceKind;
  name?: string;
}

@Component({
  selector: 'alo-env-from-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EnvFromFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => EnvFromFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvFromFormComponent
  extends BaseResourceFormComponent<EnvFromSource[], EnvFromSourceFormModel[]>
  implements OnInit {
  @Input()
  set namespace(namespace: string) {
    this.namespaceChanged.next(namespace);
  }

  configMaps$: Observable<AppConfigMap[]>;
  secrets$: Observable<AppSecret[]>;

  private namespaceChanged = new BehaviorSubject<string>(this.namespace);

  constructor(
    private httpClient: HttpClient,
    private translate: TranslateService,
    injector: Injector,
  ) {
    super(injector);
  }

  // We will take care merge by ourself in adaptFormModel
  getResourceMergeStrategy() {
    return false;
  }

  ngOnInit() {
    super.ngOnInit();

    this.configMaps$ = this.namespaceChanged.pipe(
      switchMap(namespace =>
        this.getNamespaceResources$(namespace, 'configMap'),
      ),
      map((list: any) => list.items),
      publishReplay(1),
      refCount(),
    );

    this.secrets$ = this.namespaceChanged.pipe(
      switchMap(namespace => this.getNamespaceResources$(namespace, 'secret')),
      map((list: any) => list.secrets),
      publishReplay(1),
      refCount(),
    );
  }

  createForm() {
    return new FormControl([]);
  }

  getDefaultFormModel(): EnvFromSourceFormModel[] {
    return [];
  }

  adaptResourceModel(
    envFromSources: EnvFromSource[],
  ): EnvFromSourceFormModel[] {
    if (!envFromSources || envFromSources.length === 0) {
      return this.getDefaultFormModel();
    }

    // Fill in keyRefObj when applied:
    return envFromSources.map((envFrom: EnvFromSource) => {
      const sourceObj = getEnvFromSource(envFrom);
      const kind = ENV_FROM_SOURCE_TYPE_TO_KIND[getEnvFromSourceType(envFrom)];
      return {
        name: sourceObj.name,
        kind,
      };
    });
  }

  adaptFormModel(
    envFromSourceFormModels: EnvFromSourceFormModel[],
  ): EnvFromSource[] {
    return envFromSourceFormModels.map(({ kind, name }) => {
      const refType = KIND_TO_SUPPORTED_ENV_FROM_TYPES[kind];

      return {
        [refType]: {
          name,
        },
      };
    });
  }

  getRefObj(obj: AppConfigMap | AppSecret): EnvFromSourceFormModel {
    return {
      name: obj.objectMeta.name,
      kind: obj.typeMeta.kind as SupportedEnvFromSourceKind,
    };
  }

  refObjTrackByFn = (refObj: EnvFromSourceFormModel) => {
    return refObj && refObj.kind
      ? `${this.translate.get(`application.${refObj.kind.toLowerCase()}`)}: ${
          refObj.name
        }`
      : '';
  };

  refObjFilterFn = (filterString: string, option: OptionComponent) => {
    return option.value.name.includes(filterString);
  };

  private getNamespaceResources$(namespace: string, resourceName: string) {
    return this.httpClient
      .get<ResourceList>(
        `api/v1/${resourceName.toLocaleLowerCase()}/${namespace || ''}`,
      )
      .pipe(
        publishReplay(1),
        refCount(),
      );
  }
}
