import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  ApplicationApiService,
  Container,
  EnvFromSource,
  EnvVar,
} from '@app/api';
import { TranslateService } from '@app/translate';
import { DialogService, DialogSize, ToastService } from 'alauda-ui';
import { safeDump } from 'js-yaml';
import { cloneDeep } from 'lodash';

import { EnvFromDialogComponent } from '../env-from-dialog/component';
import { EnvDialogComponent } from '../env-var-dialog/component';
import { getEnvFromSource, getEnvFromSourceKind } from '../utils/env-from';
import {
  getEnvVarSource,
  getEnvVarSourceKind,
  isEnvVarSourceMode,
  isEnvVarSourceSupported,
} from '../utils/env-var';

@Component({
  selector: 'alo-pod-env-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class PodEnvListComponent implements OnInit {
  @Input() containers: Container[] = [];

  @Input() kind: string;

  @Input() resourceName: string;

  @Input() namespace: string;

  @Input() noUpdate: boolean;

  @Output() update = new EventEmitter<any>();

  readonly envListColumnDefs = ['name', 'config_value'];

  constructor(
    private dialog: DialogService,
    private api: ApplicationApiService,
    private toast: ToastService,
    private translate: TranslateService,
  ) {}

  ngOnInit(): void {}

  envVarViewMode(envVar: EnvVar): 'value' | 'valueFrom' | 'yaml' {
    if (!isEnvVarSourceMode(envVar)) {
      return 'value';
    } else if (isEnvVarSourceSupported(envVar)) {
      return 'valueFrom';
    } else {
      return 'yaml';
    }
  }

  envVarResource(
    envVar: EnvVar,
  ): { kind: string; name: string; key: string; namespace: string } {
    const source = getEnvVarSource(envVar);

    return {
      kind: `application.${getEnvVarSourceKind(envVar.valueFrom)}`,
      name: source.name,
      namespace: this.namespace,
      key: source.key,
    };
  }

  envFromResource(
    envFrom: EnvFromSource,
  ): { kind: string; name: string; namespace: string } {
    const source = getEnvFromSource(envFrom);

    return {
      kind: `application.${getEnvFromSourceKind(envFrom)}`,
      name: source.name,
      namespace: this.namespace,
    };
  }

  async editEnvFrom(container: Container) {
    const newEnvFrom = await this.dialog
      .open(EnvFromDialogComponent, {
        data: {
          envFrom: cloneDeep(container.envFrom || []),
          namespace: this.namespace,
        },
      })
      .afterClosed()
      .toPromise();

    if (newEnvFrom) {
      const newContainer = cloneDeep(container);
      newContainer.envFrom = newEnvFrom;
      const payload = { env: container.env, envFrom: newEnvFrom };

      this.api
        .putEnvAndEnvFrom(
          this.kind,
          this.namespace,
          this.resourceName,
          container.name,
          payload,
        )
        .subscribe(
          () => {
            this.handleUpdateSuccess('env_from');
          },
          error => {
            this.handleUpdateError(error);
          },
        );
    }
  }

  async editEnv(container: Container) {
    const newEnv = await this.dialog
      .open(EnvDialogComponent, {
        data: {
          env: cloneDeep(container.env || []),
          namespace: this.namespace,
        },
        size: DialogSize.Large,
      })
      .afterClosed()
      .toPromise();

    if (newEnv) {
      const newContainer = cloneDeep(container);
      newContainer.env = newEnv;

      const putEnv = newEnv.map((env: any) => {
        return { name: env.name, value: env.value, valueFrom: env.valueFrom };
      });
      const payload = { env: putEnv, envFrom: container.envFrom };
      this.api
        .putEnvAndEnvFrom(
          this.kind,
          this.namespace,
          this.resourceName,
          container.name,
          payload,
        )
        .subscribe(
          () => {
            this.handleUpdateSuccess('env');
          },
          error => {
            this.handleUpdateError(error);
          },
        );
    }
  }

  getYaml(json: any) {
    return safeDump(json).trim();
  }

  handleUpdateSuccess(type: string) {
    this.toast.messageSuccess({
      content: `${this.translate.get(type)}${this.translate.get(
        'update_succeeded',
      )}`,
    });
    this.update.emit();
  }

  handleUpdateError(error: any) {
    this.toast.alertError({
      title: `${this.translate.get('configmap')}${this.translate.get(
        'update_failed',
      )}`,
      content: error.error || error.message,
    });
  }
}
