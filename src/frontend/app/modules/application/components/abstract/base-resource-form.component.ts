import {
  AfterViewInit,
  ChangeDetectorRef,
  EventEmitter,
  Injector,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

import {
  ControlValueAccessor,
  FormArray,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { cloneDeep, isEqual, merge } from 'lodash';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import {
  OnFormArrayResizeFn,
  setFormByResource,
  setResourceByForm,
} from '@app/utils/form';

// Base form component for Resources.
// <T> refers the type of the resource.
export abstract class BaseResourceFormComponent<
  R extends Object = any,
  F extends Object = R
> implements OnInit, ControlValueAccessor, Validator, OnDestroy, AfterViewInit {
  private formValueSub: Subscription;
  private parentFormSub: Subscription;
  private adaptedResource: F;
  private statusChangeSub: Subscription;
  readonly cdr: ChangeDetectorRef;

  @ViewChild(NgForm) ngFormDir: NgForm;

  @ViewChild(FormGroupDirective) ngFormGroupDirective: FormGroupDirective;

  @Output() blur = new EventEmitter();

  disabled = false;

  // Based on scenarios, the form can be a single form control, array or a complex group.
  form: FormControl | FormGroup | FormArray;

  /**
   * Method to create the default form
   */
  abstract createForm(): FormControl | FormGroup | FormArray;

  /**
   * The default form model
   */
  abstract getDefaultFormModel(): F;

  /**
   * Returns the embedded form value
   */
  get formModel(): F {
    return this.form.value;
  }

  /**
   * Adapts the resource to form Model.
   *
   * Will be called in [writeValue]
   */
  adaptResourceModel(resource: R): F {
    return resource as any;
  }

  /**
   * Adapts the form model to the resource Model
   *
   * Will be called in [onChange]
   */
  adaptFormModel(formModel: F): R {
    return formModel as any;
  }

  /**
   * Provide an optional function to be used when the given form is not with
   * correct size.
   */
  getOnFormArrayResizeFn(): OnFormArrayResizeFn {
    return () => new FormControl();
  }

  /**
   * Whether or not to merge the form with the input resource.
   */
  getResourceMergeStrategy() {
    return !(this.form instanceof FormArray);
  }

  /**
   * Wrapps the ControlValueAccessor onChange (cvaConChange) to let
   * the user do some hack before calling onChange
   */
  onChange(formValue: R) {
    this.onCvaChange(formValue);
  }

  onCvaChange = (_: R) => {};
  onCvaTouched = () => {};

  /**
   * To be bound to the template.
   */
  onBlur() {
    this.onCvaTouched();
    this.blur.emit();
  }

  registerOnChange(fn: (value: R) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(resource: R) {
    let formModel = (this.adaptedResource = this.adaptResourceModel(resource));

    // We need to unsub the form value change before setting the form value
    // because the form may emit events when setFormByResource is called.
    this.deregisterObservables();

    this.setupForm();

    formModel = this.getResourceMergeStrategy()
      ? Object.assign(cloneDeep(this.getDefaultFormModel()), formModel)
      : formModel;

    setFormByResource(this.form, formModel, this.getOnFormArrayResizeFn());

    this.registerObservables();

    this.cdr.markForCheck();
  }

  /**
   * We skipped the form control, but checks the embedded form instead.
   */
  validate(_c: FormControl): ValidationErrors | null {
    if (this.form && this.form.invalid) {
      return { invalid: true };
    }

    return null;
  }

  ngOnInit() {
    this.setupForm();
  }

  ngAfterViewInit() {
    const parentForm: NgForm | FormGroupDirective =
      this.getInjectable(NgForm) || this.getInjectable(FormGroupDirective);
    if (parentForm) {
      this.form.setParent(parentForm.form);

      this.parentFormSub = parentForm.ngSubmit.subscribe((event: Event) => {
        if (this.ngFormDir) {
          this.ngFormDir.onSubmit(event);
        } else if (this.ngFormGroupDirective) {
          this.ngFormGroupDirective.onSubmit(event);
        }
        this.form.updateValueAndValidity();
      });
    }
  }

  ngOnDestroy() {
    this.deregisterObservables();
    if (this.parentFormSub) {
      this.parentFormSub.unsubscribe();
    }
  }

  private getInjectable<Token>(token: Function & { prototype: Token }) {
    try {
      return this.injector.get(token);
    } catch {}
  }

  protected setupForm() {
    if (!this.form) {
      this.form = this.createForm();
    }
  }

  protected registerObservables() {
    this.deregisterObservables();

    this.statusChangeSub = this.form.statusChanges.subscribe(() => {
      this.form.updateValueAndValidity({ emitEvent: false });
    });

    this.formValueSub = this.form.valueChanges
      .pipe(
        map(value =>
          setResourceByForm(
            this.form,
            this.getResourceMergeStrategy()
              ? merge(cloneDeep(this.adaptedResource), value)
              : value,
          ),
        ),
        distinctUntilChanged(isEqual),
      )
      .subscribe(value => this.onChange(this.adaptFormModel(value)));
  }

  protected deregisterObservables() {
    if (this.formValueSub) {
      this.formValueSub.unsubscribe();
      this.formValueSub = undefined;
    }

    if (this.statusChangeSub) {
      this.statusChangeSub.unsubscribe();
      this.statusChangeSub = undefined;
    }
  }

  constructor(public injector: Injector) {
    this.cdr = this.getInjectable(ChangeDetectorRef);
  }
}
