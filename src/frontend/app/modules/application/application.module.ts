import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ApplicationApiService, ContainerLogApiService } from '@app/api';
import { PipelineModule } from '@app/modules/pipeline/pipeline.module';
import { DialogModule } from 'alauda-ui';

import { CreateApplicationModule } from '../../features/shared/create-application/create-application.module';
import { SharedModule } from '../../shared';
import { TranslateService } from '../../translate';

import {
  ApplicationDeleteDialogComponent,
  ApplicationDetailComponent,
  ApplicationListCardComponent,
  ApplicationLogComponent,
  ApplicationOtherListComponent,
  ApplicationResourceReportComponent,
  ApplicationStatusGaugeBarComponent,
  ApplicationYamlEditDialogComponent,
  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
  ContainerComponent,
  ContainerUpdateComponent,
  ContainerUpdateDifferDialogComponent,
  ContainerUpdateEnvComponent,
  ContainerUpdateEnvFromComponent,
  ContainerUpdateFormComponent,
  ContainerUpdateSizeComponent,
  ContainerUpdateVolumeComponent,
  EnvDialogComponent,
  EnvFormComponent,
  EnvFromDialogComponent,
  EnvFromFormComponent,
  FoldableBarComponent,
  ImageAddressUpdateDialogComponent,
  K8sResourceBasicInfoComponent,
  K8sResourceComponent,
  K8sResourceDetailComponent,
  K8sResourceIconComponent,
  KeyPathFromComponent,
  PodEnvListComponent,
  PodsScalerComponent,
  ResourceLabelsFieldComponent,
  ResourceYamlPreviewDialogComponent,
  TagsLabelComponent,
  UpdateLabelsDialogComponent,
  VolumeInfoComponent,
  VolumeMountDialogComponent,
  ZeroStateComponent,
} from './components';
import i18n from './i18n';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    PipelineModule,
    CreateApplicationModule,
    DialogModule,
  ],
  declarations: [
    ApplicationDetailComponent,
    K8sResourceComponent,
    K8sResourceDetailComponent,
    K8sResourceIconComponent,
    K8sResourceBasicInfoComponent,
    ContainerComponent,
    PodsScalerComponent,
    ApplicationYamlEditDialogComponent,
    ApplicationOtherListComponent,
    ContainerUpdateComponent,
    ContainerUpdateFormComponent,
    ContainerUpdateEnvComponent,
    ContainerUpdateEnvFromComponent,
    ResourceYamlPreviewDialogComponent,
    ContainerUpdateDifferDialogComponent,
    ApplicationDeleteDialogComponent,
    ApplicationResourceReportComponent,
    ApplicationListCardComponent,
    ApplicationStatusGaugeBarComponent,
    ApplicationLogComponent,
    ResourceLabelsFieldComponent,
    TagsLabelComponent,
    UpdateLabelsDialogComponent,
    VolumeInfoComponent,
    VolumeMountDialogComponent,
    FoldableBarComponent,
    ImageAddressUpdateDialogComponent,
    ContainerUpdateSizeComponent,
    ContainerUpdateVolumeComponent,
    KeyPathFromComponent,
    PodEnvListComponent,
    EnvDialogComponent,
    EnvFromDialogComponent,
    EnvFormComponent,
    EnvFromFormComponent,
    ZeroStateComponent,
    ArrayFormTableComponent,
    ArrayFormTableFooterDirective,
    ArrayFormTableHeaderDirective,
    ArrayFormTableRowControlDirective,
    ArrayFormTableRowDirective,
    ArrayFormTableZeroStateDirective,
  ],
  exports: [
    ApplicationDetailComponent,
    ContainerUpdateComponent,
    ApplicationListCardComponent,
    K8sResourceDetailComponent,
  ],
  providers: [ApplicationApiService, ContainerLogApiService],
  entryComponents: [
    ApplicationYamlEditDialogComponent,
    ResourceYamlPreviewDialogComponent,
    ContainerUpdateDifferDialogComponent,
    ApplicationDeleteDialogComponent,
    UpdateLabelsDialogComponent,
    VolumeMountDialogComponent,
    ImageAddressUpdateDialogComponent,
    EnvDialogComponent,
    EnvFromDialogComponent,
  ],
})
export class ApplicationModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('application', i18n);
  }
}
