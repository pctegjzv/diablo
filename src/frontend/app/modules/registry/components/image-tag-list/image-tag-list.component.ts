import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ImageTag } from '@app/api/registry/registry-api.types';

@Component({
  selector: 'alo-iamge-tag-list',
  templateUrl: 'image-tag-list.component.html',
  styleUrls: ['image-tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageTagListComponent {
  @Input() tags: ImageTag[] = [];

  trackByName(_: number, item: ImageTag) {
    return item.name;
  }
}
