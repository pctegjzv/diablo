import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Repository } from '@app/api/registry/registry-api.types';
import { Sort } from 'alauda-ui';

@Component({
  selector: 'alo-repository-list',
  templateUrl: 'repository-list.component.html',
  styleUrls: ['repository-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RepositoryListComponent {
  private _repositories: Repository[] = [];
  @Input()
  get repositories() {
    return this._repositories;
  }
  set repositories(val) {
    if (!val) {
      return;
    }
    this._repositories = this.sortData(val, this.currentSort);
  }

  currentSort: Sort = {
    direction: 'asc',
    active: 'address',
  };

  onSortChange(sort: Sort) {
    this.currentSort = sort;
    this.repositories = this.sortData(this.repositories, sort);
  }

  sortData(data: Repository[], sort: Sort) {
    return [
      ...data.sort((a, b) => {
        return sort.direction === 'asc'
          ? a.image.localeCompare(b.image)
          : b.image.localeCompare(a.image);
      }),
    ];
  }

  trackByName(_: number, item: Repository) {
    return item.name;
  }
}
