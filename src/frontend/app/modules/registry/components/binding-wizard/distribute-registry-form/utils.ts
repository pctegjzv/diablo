import { TreeNode } from 'alauda-ui';

export function mapRepositoriesToTreeNodes(repositories: string[] | null) {
  if (!repositories) {
    return [];
  }
  return [
    {
      label: '/',
      value: ['/'],
      expanded: true,
      icon: 'basic:file_catalog_fold',
      expandedIcon: 'basic:file_catalog_open',
      children: repositories
        .map(items =>
          items
            .split('/')
            .map((item, index, arr) => ({
              label: item,
              value: arr.slice(0, index + 1),
              icon:
                index < arr.length - 1
                  ? 'basic:file_catalog_fold'
                  : 'basic:file',
              expandedIcon: 'basic:file_catalog_open',
            }))
            .reduceRight((prev, current) => {
              return { ...current, children: [prev] };
            }),
        )
        .reduce(mergeTreeBranch, []),
    },
  ];
}

export function mergeTreeBranch(
  prev: TreeNode[],
  current: TreeNode,
): TreeNode[] {
  const exitNodeIndex = prev.findIndex(item => item.label === current.label);
  if (exitNodeIndex >= 0) {
    return prev.map((item, index) => {
      if (index === exitNodeIndex) {
        const mergedChildren = item.children
          .concat(current.children)
          .reduce(mergeTreeBranch, [])
          .sort((a, b) => a.label.localeCompare(b.label));
        return { ...item, children: mergedChildren };
      } else {
        return item;
      }
    });
  } else {
    return prev.concat(current).sort((a, b) => a.label.localeCompare(b.label));
  }
}
