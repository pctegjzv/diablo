import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { SecretApiService, SecretTypes } from '@app/api';
import { RegistryApiService } from '@app/api/registry/registry-api.service';
import { RegistryBinding } from '@app/api/registry/registry-api.types';
import { SecretCreateDialogComponent } from '@app/modules/secret';
import { TranslateService } from '@app/translate';
import { DialogService, DialogSize, NotificationService } from 'alauda-ui';
import { ReplaySubject, Subject, combineLatest } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

@Component({
  selector: 'alo-bind-account-form',
  templateUrl: 'bind-account-form.component.html',
  styleUrls: ['bind-account-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BindAccountFormComponent {
  private _namespace: string;
  @Input()
  get namespace() {
    return this._namespace;
  }
  set namespace(val: string) {
    this._namespace = val;
    this.namespace$$.next(val);
  }
  @Input() service: string;
  @Output() statusChange = new EventEmitter<boolean>();
  @Output() bound = new EventEmitter<RegistryBinding>();

  @ViewChild('form') ngForm: NgForm;

  private secretsUpdated$$ = new Subject<void>();
  private namespace$$ = new ReplaySubject<string>(1);

  secrets$ = combineLatest(
    this.namespace$$,
    this.secretsUpdated$$.pipe(startWith(null)),
  ).pipe(
    switchMap(([namespace]) => this.secretApi.find(null, namespace)),
    map(res =>
      res.items
        .filter(item => item.type === SecretTypes.basicAuth)
        .map(item => item.name),
    ),
    publishReplay(1),
    refCount(),
  );

  formData = {
    name: '',
    secret: '',
    description: '',
  };

  hasAuth = true;

  constructor(
    private dialog: DialogService,
    private secretApi: SecretApiService,
    private registryApi: RegistryApiService,
    private notifaction: NotificationService,
    private translate: TranslateService,
  ) {}

  addSecret() {
    this.dialog
      .open(SecretCreateDialogComponent, {
        size: DialogSize.Large,
        data: {
          namespace: this.namespace,
          types: [SecretTypes.basicAuth],
        },
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.formData.secret = result.name;
          this.secretsUpdated$$.next();
        }
      });
  }

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    this.statusChange.emit(true);
    this.registryApi
      .createBinding({
        namespace: this.namespace,
        service: this.service,
        name: this.formData.name,
        description: this.formData.description,
        secret: this.hasAuth ? this.formData.secret : '',
      })
      .subscribe(
        binding => {
          this.statusChange.emit(false);
          this.bound.emit(binding);
        },
        (error: any) => {
          this.notifaction.error({
            title: this.translate.get('registry.bind_account_failed'),
            content: error.error || error.message,
          });
          this.statusChange.emit(false);
        },
      );
  }
}
