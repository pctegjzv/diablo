import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { SecretApiService, SecretTypes } from '@app/api';
import { RegistryApiService } from '@app/api/registry/registry-api.service';
import { RegistryBinding } from '@app/api/registry/registry-api.types';
import { SecretCreateDialogComponent } from '@app/modules/secret';
import { TranslateService } from '@app/translate';
import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from 'alauda-ui';
import { Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

@Component({
  templateUrl: 'update-binding.component.html',
  styleUrls: ['update-binding.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateRegistryBindingComponent {
  formData = {
    description: this.binding.description,
    secret: this.binding.secret,
  };

  hasAuth = !!this.binding.secret;
  loading = false;

  private secretsUpdated$$ = new Subject<void>();

  secrets$ = this.secretsUpdated$$.pipe(startWith(null)).pipe(
    switchMap(() => this.secretApi.find(null, this.binding.namespace)),
    map(res =>
      res.items
        .filter(item => item.type === SecretTypes.basicAuth)
        .map(item => item.name),
    ),
    publishReplay(1),
    refCount(),
  );

  @ViewChild('ngForm') ngForm: NgForm;

  constructor(
    @Inject(DIALOG_DATA) public binding: RegistryBinding,
    private dialogRef: DialogRef,
    private dialog: DialogService,
    private secretApi: SecretApiService,
    private registryApi: RegistryApiService,
    private cdr: ChangeDetectorRef,
    private message: MessageService,
    private notifaction: NotificationService,
    private translate: TranslateService,
  ) {}

  addSecret() {
    this.dialog
      .open(SecretCreateDialogComponent, {
        size: DialogSize.Large,
        data: {
          namespace: this.binding.namespace,
          types: [SecretTypes.basicAuth],
        },
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.formData.secret = result.name;
          this.secretsUpdated$$.next();
        }
      });
  }

  update() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    this.loading = true;
    this.registryApi
      .updateBinding({
        ...this.binding,
        description: this.formData.description,
        secret: this.hasAuth ? this.formData.secret : '',
      })
      .subscribe(
        () => {
          this.dialogRef.close(true);
          this.message.success(
            this.translate.get('registry.update_registry_binding_succ'),
          );
        },
        error => {
          this.notifaction.error({
            title: this.translate.get(
              'registry.update_registry_binding_failed',
            ),
            content: error.error || error.message,
          });
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }
}
