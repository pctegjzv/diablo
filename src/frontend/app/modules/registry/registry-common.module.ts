import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AssignRepositoryComponent } from '@app/modules/registry/components/assign-repository/assign-repository.component';
import { BindAccountFormComponent } from '@app/modules/registry/components/binding-wizard/bind-account-form/bind-account-form.component';
import { RegistryBindingWizardComponent } from '@app/modules/registry/components/binding-wizard/binding-wizard.component';
import { DistributeRegistryFormComponent } from '@app/modules/registry/components/binding-wizard/distribute-registry-form/distribute-registry-form.component';
import { ImageTagListComponent } from '@app/modules/registry/components/image-tag-list/image-tag-list.component';
import { RepositoryListComponent } from '@app/modules/registry/components/repository-list/repository-list.component';
import { UpdateRegistryBindingComponent } from '@app/modules/registry/components/update-binding/update-binding.component';
import i18n from '@app/modules/registry/i18n';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';

@NgModule({
  imports: [CommonModule, FormsModule, RouterModule, SharedModule],
  declarations: [
    BindAccountFormComponent,
    DistributeRegistryFormComponent,
    RegistryBindingWizardComponent,
    UpdateRegistryBindingComponent,
    RepositoryListComponent,
    AssignRepositoryComponent,
    ImageTagListComponent,
  ],
  exports: [
    RegistryBindingWizardComponent,
    RepositoryListComponent,
    ImageTagListComponent,
  ],
  entryComponents: [UpdateRegistryBindingComponent, AssignRepositoryComponent],
})
export class RegistryCommonModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('registry', i18n);
  }
}
