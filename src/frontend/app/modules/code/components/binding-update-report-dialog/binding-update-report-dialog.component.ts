import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CodeRepoRelatedResource } from '@app/api/code/code-api.types';

@Component({
  templateUrl: 'binding-update-report-dialog.component.html',
  styleUrls: ['binding-update-report-dialog.component.scss'],
})
export class CodeBindingUpdateReportDialogComponent {
  columns = ['name', 'type'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: CodeRepoRelatedResource[],
  ) {}

  resourceIdentity(_: number, item: CodeRepoRelatedResource) {
    return `${item.kind}/${item.namespace}/${item.name}`;
  }
}
