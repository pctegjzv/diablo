import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CodeRepository } from '@app/api/code/code-api.types';

@Component({
  selector: 'alo-code-repository-list',
  templateUrl: 'repository-list.component.html',
  styleUrls: ['repository-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeRepositoryListComponent {
  @Input() data: CodeRepository[];

  columns = ['name', 'organization', 'address', 'capacity', 'commit', 'type'];

  trackByFn(_index: number, item: CodeRepository) {
    return `${item.namespace}/${item.name}`;
  }
}
