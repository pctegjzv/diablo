import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { CodeApiService } from '@app/api/code/code-api.service';
import {
  CodeBinding,
  CodeRepoOwner,
  RemoteRepositoryOwner,
} from '@app/api/code/code-api.types';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';
import { forkJoin } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HttpErrorResponse } from '../../../../../../../node_modules/@angular/common/http';
import { CodeBindingUpdateReportDialogComponent } from '../binding-update-report-dialog/binding-update-report-dialog.component';

@Component({
  selector: 'alo-code-repository-assign',
  templateUrl: 'repository-assign.component.html',
  styleUrls: ['repository-assign.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: 'alo-code-repository-assign',
})
export class CodeRepositoryAssignComponent implements OnChanges {
  @Input() namespace: string;
  @Input() name: string;

  @Output() saved = new EventEmitter<void>();

  personalAccounts: RemoteRepositoryOwner[];

  teamAccounts: RemoteRepositoryOwner[];

  hasError = false;

  owners: {
    [name: string]: CodeRepoOwner;
  };

  initialized = false;

  loading = false;

  saving = false;

  private original: CodeBinding;

  constructor(
    private codeApi: CodeApiService,
    private cdr: ChangeDetectorRef,
    private toast: ToastService,
    private dialog: MatDialog,
    private translate: TranslateService,
  ) {}

  ngOnChanges() {
    this.loadForm();
  }

  refresh() {
    this.loadForm(true);
  }

  submit() {
    this.saving = true;
    const owners = Object.keys(this.owners)
      .filter(
        key => this.owners[key].all || this.owners[key].repositories.length,
      )
      .map(
        key =>
          this.owners[key].all
            ? { ...this.owners[key], repositories: [] }
            : this.owners[key],
      );
    this.saving = true;
    this.codeApi
      .updateBinding({
        ...this.original,
        owners,
      })
      .subscribe(
        result => {
          this.saving = false;
          this.cdr.markForCheck();
          this.saved.emit();
          if (result.items && result.items.length) {
            this.dialog.open(CodeBindingUpdateReportDialogComponent, {
              width: '900px',
              data: result.items,
            });
          }
        },
        (error: any) => {
          this.saving = false;
          this.cdr.markForCheck();
          this.toast.alertError({
            title: 'code.repository_assign_fail',
            content: error.error || error.message || undefined,
          });
        },
      );
  }

  getMatched(account: RemoteRepositoryOwner) {
    if (!this.owners[`${account.type}/${account.name}`]) {
      this.owners[`${account.type}/${account.name}`] = {
        type: account.type,
        name: account.name,
        all: false,
        repositories: [],
      };
    }

    return this.owners[`${account.type}/${account.name}`];
  }

  updateSelected(account: RemoteRepositoryOwner, repositories: string[] = []) {
    const matched = this.getMatched(account);
    this.owners[`${account.type}/${account.name}`] = {
      ...matched,
      repositories,
    };
  }

  updateAutoSync(account: RemoteRepositoryOwner, all = false) {
    const matched = this.getMatched(account);
    this.owners[`${account.type}/${account.name}`] = {
      ...matched,
      all,
    };
  }

  private loadForm(force = false) {
    if ((!this.initialized || force) && this.name && this.namespace) {
      this.loading = true;
      this.hasError = false;
      forkJoin(this.getOwners(), this.getRemoteRepository()).subscribe(
        ([owners, { personal, team }]) => {
          this.personalAccounts = personal;
          this.teamAccounts = team;
          this.owners = owners;
          this.initialized = true;
          this.loading = false;
          this.cdr.markForCheck();
        },
        (error: { title: string; content: string }) => {
          this.personalAccounts = [];
          this.teamAccounts = [];
          this.hasError = true;
          this.owners = {};
          this.initialized = true;
          this.loading = false;
          this.cdr.markForCheck();
          this.toast.alertWarning(error);
        },
      );
    }
  }

  private getRemoteRepository() {
    return this.codeApi
      .getBindingRemoteRepositories(this.name, this.namespace)
      .pipe(
        map(result => {
          return (result.owners || []).reduce(
            (accum, item) => {
              return {
                ...accum,
                personal:
                  item.type === 'User'
                    ? [
                        ...accum.personal,
                        { ...item, repositories: item.repositories || [] },
                      ]
                    : accum.personal,
                team:
                  item.type === 'Org'
                    ? [
                        ...accum.team,
                        { ...item, repositories: item.repositories || [] },
                      ]
                    : accum.team,
              };
            },
            {
              personal: [] as RemoteRepositoryOwner[],
              team: [] as RemoteRepositoryOwner[],
            },
          );
        }),
        catchError(
          this.throwFriendlyError('code.remote_repositories_load_fail'),
        ),
      );
  }

  private getOwners() {
    return this.codeApi.getBinding(this.name, this.namespace).pipe(
      tap(result => (this.original = result)),
      map(result =>
        result.owners.reduce(
          (accum, item) => ({ ...accum, [`${item.type}/${item.name}`]: item }),
          {},
        ),
      ),
      catchError(this.throwFriendlyError('code.code_binding_not_found')),
    );
  }

  private throwFriendlyError(translateKey: string) {
    return (error: HttpErrorResponse) => {
      throw {
        title: this.translate.get(translateKey),
        content: error.error || error.message || undefined,
      };
    };
  }
}
