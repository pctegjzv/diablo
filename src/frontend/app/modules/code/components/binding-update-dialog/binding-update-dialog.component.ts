import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  templateUrl: 'binding-update-dialog.component.html',
  styleUrls: ['binding-update-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeBindingUpdateDialogComponent {
  status = '';

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { name: string; namespace: string; service: string },
    private dialogRef: MatDialogRef<CodeBindingUpdateDialogComponent>,
    private cdr: ChangeDetectorRef,
  ) {}

  close(saved = false) {
    this.dialogRef.close(saved);
    this.cdr.detectChanges();
  }

  onStatusChange(status: string) {
    this.status = status;
  }
}
