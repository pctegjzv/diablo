import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeRepoRelatedResource } from '@app/api/code/code-api.types';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';

@Component({
  templateUrl: 'service-delete-dialog.component.html',
  styleUrls: ['service-delete-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeServiceDeleteDialogComponent {
  deleting = false;

  deleted = false;

  resourceColumns = ['name', 'type'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { name: string },
    private codeApi: CodeApiService,
    private toast: ToastService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private dialogRef: MatDialogRef<CodeServiceDeleteDialogComponent>,
  ) {}

  resourceIdentity(_: number, item: CodeRepoRelatedResource) {
    return `${item.kind}/${item.namespace}/${item.name}`;
  }

  close(deleted = false) {
    this.dialogRef.close(deleted);
  }

  fetchRelatedResources = () => {
    return this.codeApi.findServiceRelatedResources(this.data.name);
  };

  fetchRelatedSecrets = () => {
    return this.codeApi.findServiceRelatedSecrets(this.data.name);
  };

  delete() {
    this.deleting = true;
    this.codeApi.deleteService(this.data.name).subscribe(
      () => {
        this.deleting = false;
        this.deleted = true;
        this.cdr.markForCheck();
        this.toast.messageSuccess({
          content: this.translate.get('code.delete_service_succ'),
        });
      },
      (error: HttpErrorResponse) => {
        this.deleting = false;
        this.cdr.markForCheck();
        this.toast.alertError({
          title: this.translate.get('code.delete_service_fail'),
          content: error.error || error.message || undefined,
        });
      },
    );
  }
}
