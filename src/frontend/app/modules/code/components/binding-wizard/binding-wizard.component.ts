import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BindingKind } from '@app/api/tool-chain/utils';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';

@Component({
  selector: 'alo-code-binding-wizard',
  templateUrl: 'binding-wizard.component.html',
  styleUrls: ['binding-wizard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeBindingWizardComponent {
  @Input() namespace: string;
  @Input() service: string;

  codeBinding = '';
  status = {
    bindingAccount: '',
    assignRepository: '',
  };

  get step() {
    return this._steps[this._stepIndex];
  }
  private _steps = ['bindAccount', 'assignRepository'];
  private _stepIndex = 0;

  constructor(
    private router: Router,
    private toast: ToastService,
    private translate: TranslateService,
    private location: Location,
  ) {}

  nextStep(event: any) {
    if (this.step === 'bindAccount') {
      this.codeBinding = event.name;
    }
    if (this.step === 'assignRepository') {
      this.toast.messageSuccess({
        content: this.translate.get('code.create_binding_succ'),
      });
      this.router.navigate([
        '/admin/projects',
        this.namespace,
        BindingKind.CodeRepo,
        this.codeBinding,
      ]);
    }
    if (this._stepIndex < this._steps.length - 1) {
      this._stepIndex++;
    }
  }

  cancel() {
    this.location.back();
  }

  onBindingAccountStatusChange(status: string) {
    this.status.bindingAccount = status;
  }
}
