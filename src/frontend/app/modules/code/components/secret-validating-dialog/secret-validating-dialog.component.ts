import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { SecretApiService } from '@app/api';
import { OAuthSecretValidatorService } from '@app/services';
import { TranslateService } from '@app/translate';
import { DIALOG_DATA, DialogRef, NotificationService } from 'alauda-ui';
import { Subject, of } from 'rxjs';
import {
  catchError,
  concatMap,
  delay,
  skipWhile,
  startWith,
  switchMap,
  take,
  tap,
  timeout,
} from 'rxjs/operators';

const WATCH_SECRET_DELAY = 2 * 1000;
const OAUTH_INTERACTIVE_TIMEOUT = 60 * 1000;

@Component({
  templateUrl: 'secret-validating-dialog.component.html',
  styleUrls: ['secret-validating-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretValidatingDialogComponent {
  validating = false;

  constructor(
    private oauthSecretValidator: OAuthSecretValidatorService,
    private secretApi: SecretApiService,
    private notifacation: NotificationService,
    private translate: TranslateService,
    private dialogRef: DialogRef<SecretValidatingDialogComponent>,
    @Inject(DIALOG_DATA)
    private data: {
      url: string;
      namespace: string;
      secret: string;
      service: string;
    },
  ) {}

  private watchSecret() {
    const { namespace, secret } = this.data;
    const fetchSecret$ = new Subject<void>();

    return fetchSecret$.pipe(
      startWith(null),
      switchMap(() =>
        this.secretApi
          .get({
            namespace,
            name: secret,
          })
          .pipe(catchError(() => of(null))),
      ),
      delay(WATCH_SECRET_DELAY),
      tap(res => {
        if (!res || !res.accessToken) {
          fetchSecret$.next();
        }
      }),
      skipWhile(res => !res.accessToken),
      take(1),
    );
  }

  validate() {
    this.validating = true;
    const { namespace, secret, service, url } = this.data;

    const sub = this.oauthSecretValidator
      .validate(url)
      .pipe(
        concatMap(code =>
          this.oauthSecretValidator.callback(namespace, secret, service, code),
        ),
        concatMap(() => this.watchSecret()),
        take(1),
        timeout(OAUTH_INTERACTIVE_TIMEOUT),
      )
      .subscribe(
        () => {
          this.dialogRef.close(true);
          sub.unsubscribe();
        },
        (error: any) => {
          if (error && error.name === 'TimeoutError') {
            this.notifacation.error({
              title: this.translate.get('code.oauth_authorization_timeout'),
            });
          }
          this.dialogRef.close();
          sub.unsubscribe();
        },
      );
  }
}
