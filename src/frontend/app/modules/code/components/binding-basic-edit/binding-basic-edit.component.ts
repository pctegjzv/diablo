import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Secret, SecretApiService, SecretTypes } from '@app/api';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeBinding } from '@app/api/code/code-api.types';
import { SecretValidatingDialogComponent } from '@app/modules/code/components/secret-validating-dialog/secret-validating-dialog.component';
import { SecretCreateDialogComponent } from '@app/modules/secret';
import { TranslateService } from '@app/translate';
import { DialogService, DialogSize, NotificationService } from 'alauda-ui';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export type Status = 'normal' | 'loading' | 'saving';

const defaultModel = (): CodeBinding => ({
  name: '',
  description: '',
  secretType: SecretTypes.basicAuth,
  secret: '',
});
@Component({
  selector: 'alo-code-binding-basic-edit',
  templateUrl: 'binding-basic-edit.component.html',
  styleUrls: ['binding-basic-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: 'alo-code-binding-basic-edit',
})
export class CodeBindingBasicEditComponent implements OnInit {
  @Input() mode: 'create' | 'update' = 'create';

  @Input() service: string;

  @Input() namespace: string;

  @Input() name = '';

  @Output()
  saved = new EventEmitter<{
    name: string;
    namespace: string;
  }>();

  @Output() statusChange = new EventEmitter<Status>();

  get callbackUrl() {
    return `${window.location.protocol}//${window.location.host}`;
  }

  @ViewChild('form') form: NgForm;

  secrets: Secret[] = [];

  secretTypes = SecretTypes;

  serviceUrl = '';

  createAppUrl = '';

  model = defaultModel();

  constructor(
    private secretApi: SecretApiService,
    private codeApi: CodeApiService,
    private dialog: DialogService,
    private auiDialog: DialogService,
    private notification: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.prepareForm();
  }

  private prepareForm() {
    this.statusChange.emit('loading');
    this.getModel()
      .pipe(
        concatMap(model => {
          return forkJoin(
            this.getSecrets(model.secretType),
            this.getService(this.service || model.service),
          ).pipe(map(([secrets, service]) => ({ model, secrets, service })));
        }),
      )
      .subscribe(
        ({ secrets, model, service }) => {
          this.secrets = secrets;
          this.model = model;
          this.serviceUrl = service.html;
          this.createAppUrl = service.createAppUrl;
          this.statusChange.emit('normal');
          this.cdr.markForCheck();
        },
        (error: { title: string; content: string }) => {
          this.secrets = [];
          this.statusChange.emit('normal');
          this.cdr.markForCheck();
          this.notification.warning(error);
        },
      );
  }

  submit() {
    (this.form as any).submitted = true;
    this.form.ngSubmit.emit();
  }

  save() {
    if (this.form.valid) {
      this.statusChange.emit('saving');
      const model = {
        ...this.model,
        namespace: this.namespace,
        service: this.service,
      };
      const request =
        this.mode === 'create'
          ? this.codeApi.createBinding(model)
          : this.codeApi.updateBinding(model);

      request
        .pipe(
          catchError((res: HttpErrorResponse) => {
            if (res.status === 402 && model.secretType === SecretTypes.oauth2) {
              return this.authSecret(res.error.authorizeUrl).pipe(
                concatMap(authed => (authed ? request : of(null))),
              );
            }
            throw res;
          }),
        )
        .subscribe(
          (result: any) => {
            this.statusChange.emit('normal');
            if (!result) {
              return;
            }
            this.saved.emit({
              name: this.model.name,
              namespace: this.namespace,
            });
            this.cdr.detectChanges();
          },
          (error: any) => {
            this.statusChange.emit('normal');
            this.cdr.detectChanges();
            this.notification.error({
              title:
                this.mode === 'create'
                  ? this.translate.get('code.create_binding_fail')
                  : this.translate.get('code.update_binding_fail'),
              content: error.error || error.message || undefined,
            });
          },
        );
    }
  }

  private authSecret(url: string): Observable<boolean> {
    const dialogRef = this.auiDialog.open(SecretValidatingDialogComponent, {
      size: DialogSize.Small,
      data: {
        url,
        namespace: this.namespace,
        secret: this.model.secret,
        service: this.service || this.model.service,
      },
    });

    return dialogRef.afterClosed();
  }

  openCreateSecretDialog() {
    const dialogRef = this.dialog.open(SecretCreateDialogComponent, {
      size: DialogSize.Large,
      data: {
        namespace: this.namespace,
        types: [this.model.secretType],
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getSecrets(this.model.secretType).subscribe(secrets => {
          this.secrets = secrets;
          this.model.secret = result.name;
          this.cdr.markForCheck();
        });
      }
    });
  }

  onSecretTypeChange() {
    this.getSecrets(this.model.secretType).subscribe(
      secrets => {
        this.secrets = secrets;
        this.model.secret = null;
        this.cdr.markForCheck();
      },
      () => {
        this.secrets = [];
        this.model.secret = null;
        this.cdr.markForCheck();
      },
    );
  }

  private getService(name: string) {
    return this.codeApi
      .getService(name)
      .pipe(catchError(this.throwFriendlyError('code.binding_edit_load_fail')));
  }

  private getModel(): Observable<CodeBinding> {
    if (this.mode === 'create') {
      return of(this.model);
    }

    const throwNotFound = this.throwFriendlyError(
      'code.binding_edit_not_found',
    );
    const throwLoadFail = this.throwFriendlyError(
      'code.binding_edit_load_fail',
    );

    return this.codeApi.getBinding(this.name, this.namespace).pipe(
      catchError(
        (error: HttpErrorResponse): Observable<CodeBinding> => {
          if (error.status === 404) {
            return throwNotFound(error);
          }

          return throwLoadFail(error);
        },
      ),
    );
  }

  private getSecrets(type: SecretTypes): Observable<Secret[]> {
    return this.secretApi.find(null, this.namespace).pipe(
      map(result => result.items),
      map(items => items.filter(item => item.type === type)),
      catchError(this.throwFriendlyError('code.binding_edit_load_fail')),
    );
  }

  private throwFriendlyError(translateKey: string) {
    return (error: HttpErrorResponse) => {
      throw {
        title: this.translate.get(translateKey),
        content: error.error || error.message || undefined,
      };
    };
  }
}
