import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeRepoServiceType, CodeService } from '@app/api/code/code-api.types';
import { ToolIntegrateParams } from '@app/api/tool-chain/tool-chain-api.types';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Component({
  templateUrl: 'service-edit-dialog.component.html',
  styleUrls: ['service-edit-dialog.component.scss'],
})
export class CodeServiceEditDialogComponent implements OnInit {
  currentLang$ = this.translate.currentLang$;

  serviceTypeGroups: {
    public: CodeRepoServiceType[];
    private: CodeRepoServiceType[];
  } = {
    public: [],
    private: [],
  };

  model = {
    type: '',
    name: '',
    host: '',
    public: false,
  };

  loading = false;

  saving = false;

  @ViewChild(NgForm) form: NgForm;

  constructor(
    private codeApi: CodeApiService,
    private translate: TranslateService,
    private toast: ToastService,
    private dialogRef: MatDialogRef<CodeServiceEditDialogComponent>,
    private cdr: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      mode: 'create' | 'upate';
      name: string;
    },
  ) {}

  ngOnInit() {
    this.prepareForm();
  }

  prepareForm() {
    this.loading = true;
    forkJoin(this.getModel(), this.getServiceTypeGroups()).subscribe(
      ([model, serviceTypeGroups]) => {
        if (model) {
          this.model = model;
        }
        this.serviceTypeGroups = serviceTypeGroups;
        this.loading = false;
        this.cdr.markForCheck();
      },
      error => {
        this.loading = false;
        this.cdr.markForCheck();
        this.toast.alertError(error);
      },
    );
  }

  getModel(): Observable<CodeService> {
    if (this.data.mode === 'create') {
      return of(null);
    }

    return this.codeApi
      .getService(this.data.name)
      .pipe(catchError(this.throwFriendlyError('code.service_load_fail')));
  }

  getServiceTypeGroups() {
    return this.codeApi.findCodeServiceTypes().pipe(
      map(result => {
        return result.reduce(
          (accum, item) => {
            return {
              ...accum,
              public: item.public ? [...accum.public, item] : accum.public,
              private: !item.public ? [...accum.private, item] : accum.private,
            };
          },
          { public: [], private: [] },
        );
      }),
      catchError(this.throwFriendlyError('code.service_type_load_fail')),
    );
  }

  selectServiceType(item: CodeRepoServiceType) {
    if (this.model.type !== item.type || this.model.public !== item.public) {
      this.model.name = item.name;
      this.model.host = item.host;
    }
    this.model.type = item.type;
    this.model.public = item.public;
  }

  submit() {
    (this.form as any).submitted = true;
    this.form.ngSubmit.emit();
  }

  save() {
    if (this.form.valid) {
      const succTitle =
        this.data.mode === 'create'
          ? 'code.create_service_succ'
          : 'code.update_service_succ';
      const failTitle =
        this.data.mode === 'create'
          ? 'code.create_service_fail'
          : 'code.update_service_fail';

      const request =
        this.data.mode === 'create'
          ? this.codeApi.createService(this.model as ToolIntegrateParams)
          : this.codeApi.updateService(this.model as ToolIntegrateParams);
      this.saving = true;
      request.subscribe(
        () => {
          this.saving = false;
          this.cdr.markForCheck();
          this.toast.messageSuccess({
            content: this.translate.get(succTitle),
          });
          this.dialogRef.close(this.model.name);
        },
        (error: HttpErrorResponse) => {
          this.saving = false;
          this.cdr.markForCheck();
          this.toast.alertError({
            title: this.translate.get(failTitle),
            content: error.error || error.message || undefined,
          });
        },
      );
    }
  }

  close() {
    this.dialogRef.close();
  }

  private throwFriendlyError(translateKey: string) {
    return (error: HttpErrorResponse) => {
      throw {
        title: this.translate.get(translateKey),
        content: error.error || error.message || undefined,
      };
    };
  }
}
