import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  templateUrl: 'repository-assign-dialog.component.html',
  styleUrls: ['repository-assign-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeRepositoryAssignDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { name: string; namespace: string },
    private dialogRef: MatDialogRef<CodeRepositoryAssignDialogComponent>,
  ) {}

  close(saved = false) {
    this.dialogRef.close(saved);
  }
}
