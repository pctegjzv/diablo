import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeRepoRelatedResource } from '@app/api/code/code-api.types';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';

@Component({
  templateUrl: 'binding-delete-dialog.component.html',
  styleUrls: ['binding-delete-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeBindingDeleteDialogComponent {
  deleting = false;

  deleted = false;

  resourceColumns = ['name', 'type'];

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { name: string; namespace: string; service: string },
    private codeApi: CodeApiService,
    private toast: ToastService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private dialogRef: MatDialogRef<CodeBindingDeleteDialogComponent>,
  ) {}

  resourceIdentity(_: number, item: CodeRepoRelatedResource) {
    return `${item.kind}/${item.namespace}/${item.name}`;
  }

  close(deleted = false) {
    this.dialogRef.close(deleted);
  }

  fetchRelatedResources = () => {
    return this.codeApi.findBindingRelatedResources(
      this.data.name,
      this.data.namespace,
    );
  };

  fetchRelatedSecrets = () => {
    return this.codeApi.findBindingRelatedSecrets(
      this.data.name,
      this.data.namespace,
    );
  };

  delete() {
    this.deleting = true;
    this.codeApi.deleteBinding(this.data.name, this.data.namespace).subscribe(
      () => {
        this.deleting = false;
        this.deleted = true;
        this.cdr.markForCheck();
        this.toast.messageSuccess({
          content: this.translate.get('code.delete_binding_succ'),
        });
      },
      (error: HttpErrorResponse) => {
        this.deleting = false;
        this.cdr.markForCheck();
        this.toast.alertError({
          title: this.translate.get('code.delete_binding_fail'),
          content: error.error || error.message || undefined,
        });
      },
    );
  }
}
