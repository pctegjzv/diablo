import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeBinding, CodeService } from '@app/api/code/code-api.types';
import { BindingKind } from '@app/api/tool-chain/utils';
import { CodeServiceEditDialogComponent } from '@app/modules/code/components/service-edit-dialog/service-edit-dialog.component';
import { filterBy, getQuery } from '@app/utils/query-builder';
import { map } from 'rxjs/operators';

import { CodeServiceDeleteDialogComponent } from '../service-delete-dialog/service-delete-dialog.component';

@Component({
  selector: 'alo-code-service-detail',
  templateUrl: 'service-detail.component.html',
  styleUrls: ['service-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeServiceDetailComponent {
  @Input() data: CodeService;

  @Output() updated = new EventEmitter<void>();
  @Output() deleted = new EventEmitter<void>();

  constructor(private codeApi: CodeApiService, private dialog: MatDialog) {}

  codeBindingRoute = (item: CodeBinding) => [
    '/admin/projects',
    item.namespace,
    BindingKind.CodeRepo,
    item.name,
  ];

  fetchCodeBindings = (codeService: string) =>
    this.codeApi
      .findBindings(
        getQuery(filterBy('label', `codeRepoService:${codeService}`)),
      )
      .pipe(map(result => result.items));

  update() {
    const dialogRef = this.dialog.open(CodeServiceEditDialogComponent, {
      width: '960px',
      minWidth: '960px',
      data: {
        mode: 'update',
        name: this.data.name,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updated.emit();
      }
    });
  }

  delete() {
    const dialogRef = this.dialog.open(CodeServiceDeleteDialogComponent, {
      width: '960px',
      minWidth: '960px',
      data: {
        name: this.data.name,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleted.emit();
      }
    });
  }
}
