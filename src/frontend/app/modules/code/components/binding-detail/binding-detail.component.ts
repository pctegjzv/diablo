import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { SecretTypes } from '@app/api';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeBinding } from '@app/api/code/code-api.types';
import { CodeBindingDeleteDialogComponent } from '@app/modules/code/components/binding-delete-dialog/binding-delete-dialog.component';
import { CodeBindingUpdateDialogComponent } from '@app/modules/code/components/binding-update-dialog/binding-update-dialog.component';
import { CodeRepositoryAssignDialogComponent } from '@app/modules/code/components/repository-assign-dialog/repository-assign-dialog.component';

@Component({
  selector: 'alo-code-binding-detail',
  templateUrl: 'binding-detail.component.html',
  styleUrls: ['binding-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeBindingDetailComponent {
  @Input() data: CodeBinding;

  @Output() updated = new EventEmitter<void>();

  secretTypes = SecretTypes;

  constructor(private codeApi: CodeApiService, private dialog: MatDialog) {}

  fetchCodeRepositories = (identity: { name: string; namespace: string }) => {
    return this.codeApi.findCodeRepositoriesByBinding(
      identity.name,
      identity.namespace,
    );
  };

  update() {
    const dialogRef = this.dialog.open(CodeBindingUpdateDialogComponent, {
      width: '900px',
      data: {
        name: this.data.name,
        namespace: this.data.namespace,
        service: this.data.service,
      },
    });

    dialogRef.afterClosed().subscribe(this.emitUpdated());
  }

  delete() {
    const dialogRef = this.dialog.open(CodeBindingDeleteDialogComponent, {
      width: '900px',
      data: {
        name: this.data.name,
        namespace: this.data.namespace,
        service: this.data.service,
      },
    });

    dialogRef.afterClosed().subscribe(this.emitUpdated());
  }

  assignRepository() {
    const dialogRef = this.dialog.open(CodeRepositoryAssignDialogComponent, {
      width: '900px',
      data: {
        name: this.data.name,
        namespace: this.data.namespace,
      },
    });

    dialogRef.afterClosed().subscribe(this.emitUpdated());
  }

  private emitUpdated() {
    return (result: any) => {
      if (result) {
        this.updated.emit();
      }
    };
  }
}
