import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeService } from '@app/api/code/code-api.types';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: 'service-select-dialog.component.html',
  styleUrls: ['service-select-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeServiceSelectDialogComponent {
  constructor(
    private api: CodeApiService,
    @Inject(MAT_DIALOG_DATA) private data: string,
    private dialogRef: MatDialogRef<CodeServiceSelectDialogComponent>,
  ) {}

  fetchCodeServices = () => {
    return this.api.findServices().pipe(map(result => result.items));
  };

  createBindingRoute = () => [
    '/admin/projects',
    this.data,
    'create-code-binding',
  ];

  createBindingQueryParams = (item: CodeService) => ({ service: item.name });

  close = () => this.dialogRef.close();
}
