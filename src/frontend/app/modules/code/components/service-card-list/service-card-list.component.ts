import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { CodeService } from '@app/api/code/code-api.types';

@Component({
  selector: 'alo-code-service-card-list',
  templateUrl: 'service-card-list.component.html',
  styleUrls: ['service-card-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeServiceCardListComponent {
  @Input() data: CodeService[] = [];
  @Output() toggleItem = new EventEmitter<CodeService>();

  @Input() itemRoute = (item: CodeService) => ['./', item.name];
  @Input() itemQueryParams = (): any => null;

  codeServiceIdentity(_: number, item: CodeService) {
    return item.name;
  }
}
