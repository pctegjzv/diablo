import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';

import {
  CodeBindingBasicEditComponent,
  CodeBindingDeleteDialogComponent,
  CodeBindingDetailComponent,
  CodeBindingListComponent,
  CodeBindingUpdateDialogComponent,
  CodeBindingUpdateReportDialogComponent,
  CodeBindingWizardComponent,
  CodeRemoteRepositorySelectorComponent,
  CodeRepositoryAssignComponent,
  CodeRepositoryAssignDialogComponent,
  CodeRepositoryListComponent,
  CodeServiceCardListComponent,
  CodeServiceDeleteDialogComponent,
  CodeServiceDetailComponent,
  CodeServiceEditDialogComponent,
  CodeServiceSelectDialogComponent,
  SecretValidatingDialogComponent,
} from './components';
import i18n from './i18n';

@NgModule({
  imports: [SharedModule, RouterModule, FormsModule],
  declarations: [
    CodeServiceCardListComponent,
    CodeServiceDetailComponent,
    CodeBindingListComponent,
    CodeBindingDetailComponent,
    CodeRepositoryListComponent,
    CodeBindingWizardComponent,
    CodeBindingBasicEditComponent,
    CodeRepositoryAssignComponent,
    CodeServiceSelectDialogComponent,
    CodeRemoteRepositorySelectorComponent,
    CodeBindingUpdateDialogComponent,
    CodeRepositoryAssignDialogComponent,
    CodeBindingDeleteDialogComponent,
    CodeBindingUpdateReportDialogComponent,
    CodeServiceEditDialogComponent,
    CodeServiceDeleteDialogComponent,
    SecretValidatingDialogComponent,
  ],
  exports: [
    CodeServiceCardListComponent,
    CodeServiceDetailComponent,
    CodeBindingListComponent,
    CodeBindingDetailComponent,
    CodeRepositoryListComponent,
    CodeBindingWizardComponent,
    CodeBindingBasicEditComponent,
    CodeRepositoryAssignComponent,
    CodeServiceSelectDialogComponent,
    CodeServiceEditDialogComponent,
    CodeServiceDeleteDialogComponent,
  ],
  entryComponents: [
    CodeServiceSelectDialogComponent,
    CodeBindingUpdateDialogComponent,
    CodeRepositoryAssignDialogComponent,
    CodeBindingDeleteDialogComponent,
    CodeBindingUpdateReportDialogComponent,
    CodeServiceEditDialogComponent,
    CodeServiceDeleteDialogComponent,
    SecretValidatingDialogComponent,
  ],
})
export class CodeModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('code', i18n);
  }
}
