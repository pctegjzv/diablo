export const en = {
  enterprise: 'Enterprise',
  select_service: 'Select Code Repository Service',
  create_service: 'Integration Code Service',
  delete_service: 'Delete Code Service',
  update_service_address: 'Update Code Service Address',
  repo_type: 'Repository Type',
  integrate_by: 'Integration By',
  integrate_at: 'Integration At',
  service_address: 'Service Address',
  binding: 'Binding Accounts',
  update_binding: 'Update Basic Info',
  delete_binding: 'Unbind',
  project: 'Project',
  account: 'Account',
  bind_by: 'Bind By',
  bind_at: 'Bind At',
  update_at: 'Update At',
  secret: 'Secret',
  repository_name: 'Repository Name',
  repository_address: 'Code Repository Address',
  repository_url: 'Repository Url',
  repository_team: 'Affiliated Team',
  repository_used: 'Used Capacity',
  repository_commit: 'Latest Commit',
  repository_account: 'Account',
  repository_no_data: 'No Repository Available',
  assign_repository: 'Assign Repository',
  no_binding_prefix: "Current code service haven't bind account, please ",
  no_binding_prefix_for_tips: 'You can ',
  no_binding_link: 'go to projects',
  no_binding_suffix:
    ', toggle "Project Detail - Services" tab, [Binding Account]',
  service_connect_fail: 'Connection failed',
  create_binding_fail: 'Bind code repository service failed',
  update_binding_fail: 'Update code repository binding failed',
  binding_edit_not_found: 'Code repository binding not found',
  binding_edit_load_fail: 'Load code repository binding failed',
  create_binding_succ: 'Bind code repository service successed',
  repository_assign_fail: 'Assign code repository failed',
  remote_repositories_load_fail: 'Load remote repositories failed',
  code_binding_not_found: 'code repository binding not found',
  token: 'Token',
  goto_service: 'go to {{ service }}',
  goto_service_create_token: ' generate Token, add as Secret',
  goto_service_create_oauth_app:
    ' create OAuth2 application，add Client information as Secret(use {{ url }} as callback url)',
  please_select_secret: 'Please Select Secret',
  no_secrets: 'No Secret',
  create_secret: 'Create Secret',
  bind_account: 'Bind Account',
  complete_bind: 'Complete Bind',
  bind_account_succ:
    'Account bind successed! Please assign repository for project',
  remote_repository_loading: 'Loading Code repository...',
  remote_repository_load_fail_text: 'Code repository load failed，please',
  personal_account: 'Personal',
  team_account: 'Organization',
  please_select_repository: 'Please Select Repository',
  repository_auto_sync: 'Auto Sync Repositories',
  update_binding_tips:
    'Remove repositories been used，add repositories not used by other code repository services，repository become "Wait for delete" state, repository will removed when specified references removed',
  delete_binding_tips:
    'Specified code repository service binding {{ service }} been used by this resources，please try unbind after delete these resources',
  delete_binding_confirm:
    'Are you sure to unbind code repository service {{ service }}?',
  get_binding_related_fail:
    'Load code repository binding related resources failed, please try later',
  binding_related_loading:
    'Checking code repository binding related resources...',
  delete_binding_succ: 'Unbind code repository service successed',
  delete_binding_fail: 'Unbind code repository service failed',
  delete_binding_remains:
    'Unbind code repository service successed, these Secret must deleted for related resources take effect',
  delete_binding_clean:
    'Unbind code repository service successed, no related resources',
  update_report: 'Update Report',
  update_report_tips:
    'Update affected code repositories used by these resources, repositories become "Wait for delete" state, repositories will remove when specified references removed',
  public: 'Public',
  private: 'Private',
  service_host: 'Code Repository Url',
  integrate: 'Integrate',
  service_name_pattern:
    'Code repository service name must consist lower case alphanumeric characters or "-", and end with a alphanumeric character.',
  create_service_succ: 'Integrate code repository service successed',
  create_service_fail: 'Integrate code repository service failed',
  delete_service_tips:
    'Specified code repository service binding {{ service }} been used by this resources，please try unbind after delete these resources',
  delete_service_confirm:
    'Are you sure to delete code repository service {{ service }}?',
  get_service_related_fail:
    'Load code repository service related resources failed, please try later',
  service_related_loading:
    'Checking code repository service related resources...',
  delete_service_succ: 'Delete code repository service successed',
  delete_service_fail: 'Delete code repository service failed',
  delete_service_remains:
    'Delete code repository service successed, these Secret must deleted for related resources take effect',
  delete_service_clean:
    'Delete code repository service successed, no related resources',
  no_service_prefix: 'No code repository service for now, please ',
  no_service_prefix_for_tips: 'You can ',
  no_service_link: 'go to code repository services',
  no_service_suffix: ' integrate',
  oauth: 'OAuth2',
  secret_validating: 'Third party platform validating, please wait...',
  secret_validating_cancel: 'click cancel to stop validating',
  oauth_authorization_timeout: 'OAuth2 authorization timeout',
  secret_validate_description: 'Need authorization by secret provider',
  secret_validate: 'Goto secret provider',
};

export const zh = {
  enterprise: '企业版',
  select_service: '选择代码仓库服务',
  create_service: '集成代码仓库服务',
  delete_service: '删除代码仓库服务',
  update_service_address: '更新代码仓库服务地址',
  repo_type: '仓库类型',
  integrate_by: '集成人',
  integrate_at: '集成时间',
  service_address: '代码仓库服务地址',
  binding: '绑定账号',
  update_binding: '更新基本信息',
  delete_binding: '解除绑定',
  project: '项目',
  account: '账号',
  bind_by: '绑定人',
  bind_at: '绑定时间',
  update_at: '更新时间',
  secret: '凭据',
  repository_name: '仓库名称',
  repository_address: '代码仓库地址',
  repository_url: '代码仓库地址',
  repository_team: '所属团队',
  repository_used: '已用容量',
  repository_commit: '最新提交',
  repository_account: '账号',
  repository_no_data: '无可用代码仓库',
  assign_repository: '分配代码仓库',
  no_binding_prefix: '当前代码仓库服务未绑定账号，请 ',
  no_binding_prefix_for_tips: '您可以 ',
  no_binding_link: '前往项目',
  no_binding_suffix: '，进入“项目详情-服务”下[绑定账号]',
  service_connect_fail: '连接失败',
  create_binding_fail: '代码仓库绑定失败',
  update_binding_fail: '更新代码仓库绑定失败',
  binding_edit_not_found: '代码仓库绑定未找到',
  binding_edit_load_fail: '代码仓库绑定加载失败',
  create_binding_succ: '代码仓库绑定成功',
  repository_assign_fail: '分配代码仓库失败',
  remote_repositories_load_fail: '获取账号仓库信息失败',
  code_binding_not_found: '代码仓库绑定未找到',
  token: 'Token',
  goto_service: '前往 {{ service }}',
  goto_service_create_token: ' 生成 Token, 并将 Token 信息添加到凭据',
  goto_service_create_oauth_app:
    ' 创建OAuth2 应用，并将 Client 信息添加到凭据（回调地址请填写 {{ url }}）',
  please_select_secret: '请选择凭据',
  no_secrets: '无可用凭据',
  create_secret: '添加凭据',
  bind_account: '绑定账号',
  complete_bind: '完成绑定',
  bind_account_succ: '账号绑定成功！请为项目分配代码仓库',
  remote_repository_loading: '代码仓库获取中...',
  remote_repository_load_fail_text: '代码仓库获取失败，请',
  personal_account: '个人账号',
  team_account: '组织账号',
  please_select_repository: '请选择仓库',
  repository_auto_sync: '自动同步全部仓库',
  update_binding_tips:
    '若去除某些正在被引用的仓库，并且仓库没有被其它代码仓库服务所引用，仓库会变成“待删除”状态，删除这些引用后，仓库会自动删除',
  delete_binding_tips:
    '该代码仓库服务绑定 {{ service }} 被以下资源使用，请删除以下资源后再解除绑定',
  delete_binding_confirm: '确定要解除代码仓库服务 {{ service }} 吗？',
  get_binding_related_fail: '获取代码仓库绑定相关资源失败，请稍后重试',
  binding_related_loading: '检查代码仓库绑定相关资源...',
  delete_binding_succ: '解除代码仓库绑定成功',
  delete_binding_fail: '解除代码仓库绑定失败',
  delete_binding_remains:
    '解除绑定成功，若需保证相关资源不可用，需删除下列 Secret',
  delete_binding_clean: '解除绑定成功，无相关资源需删除',
  update_report: '更新报告',
  update_report_tips:
    '更新操作移除代码仓库被以下服务所引用，仓库会变成“待删除”状态，删除这些引用后，仓库会自动删除',
  public: '公有',
  private: '私有',
  service_host: '代码仓库服务地址',
  integrate: '集成',
  service_name_pattern:
    '代码仓库服务名称必须为小写字母，数字或"-", 以字母或数字结束。',
  create_service_succ: '集成代码仓库服务成功',
  create_service_fail: '集成代码仓库服务失败',
  delete_service_tips:
    '该代码仓库服务 {{ service }} 被以下资源使用，请删除以下资源后再删除服务',
  delete_service_confirm: '确定要删除代码仓库服务 {{ service }} 吗？',
  get_service_related_fail: '获取代码仓库服务相关资源失败，请稍后重试',
  service_related_loading: '检查代码仓库服务相关资源...',
  delete_service_succ: '代码仓库服务删除成功',
  delete_service_fail: '代码仓库服务删除失败',
  delete_service_remains:
    '删除代码仓库服务成功，若需保证相关资源不可用，需删除下列凭据',
  delete_service_clean: '删除代码仓库服务成功，无相关资源需删除',
  no_service_prefix: '当前未集成代码仓库服务，请 ',
  no_service_prefix_for_tips: '您可以 ',
  no_service_link: '前往代码仓库服务',
  no_service_suffix: ' 集成',
  oauth: 'OAuth2',
  secret_validating: '第三方平台授权中，请稍后…',
  secret_validating_cancel: '如需终止操作，可点击',
  oauth_authorization_timeout: '第三方平台凭据授权超时',
  secret_validate_description: '需要跳转到第三方平台授权',
  secret_validate: '立即前往',
};

export default { en, zh };
