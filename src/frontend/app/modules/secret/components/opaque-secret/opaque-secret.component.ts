import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export interface OpaqueSecretPair {
  key: string;
  value: string;
}
const defaultValue = (): OpaqueSecretPair => ({ key: '', value: '' });

@Component({
  selector: 'alo-opaque-secret',
  templateUrl: 'opaque-secret.component.html',
  styleUrls: ['opaque-secret.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => OpaqueSecretComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OpaqueSecretComponent implements ControlValueAccessor {
  pairs: OpaqueSecretPair[] = [defaultValue()];
  isDisabled = false;

  private propagateChange = (_: any) => {};

  constructor(private cdr: ChangeDetectorRef) {}

  writeValue(obj: OpaqueSecretPair[]): void {
    this.pairs = obj && obj.length ? obj : [{ key: '', value: '' }];
    this.cdr.detectChanges();
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(): void {}

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this.cdr.detectChanges();
  }

  add() {
    this.pairs.push(defaultValue());
  }

  remove(index: number) {
    this.pairs.splice(index, 1);
    this.cdr.detectChanges();
    this.propagateChange(pairsToValue(this.pairs));
  }

  inputChange() {
    this.cdr.detectChanges();
    this.propagateChange(pairsToValue(this.pairs));
  }
}

function pairsToValue(pairs: OpaqueSecretPair[]) {
  const result = pairs.filter(pair => pair.key !== '' && pair.value !== '');

  return result.length ? result : null;
}
