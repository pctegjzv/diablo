import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import {
  Project,
  ProjectApiService,
  Secret,
  SecretApiService,
  SecretIdentity,
  SecretTypes,
} from '@app/api';
import { TranslateService } from '@app/translate';
import { MessageService, NotificationService } from 'alauda-ui';
import { head } from 'lodash';
import { Observable, forkJoin, of, throwError } from 'rxjs';
import { catchError, concatMap, map, tap } from 'rxjs/operators';

const defaultModel = (): Secret => ({
  name: '',
  namespace: '',
  displayName: '',
  description: '',
  type: null,
  ownerReferences: [],
});

@Component({
  selector: 'alo-secret-edit',
  templateUrl: 'edit.component.html',
  styleUrls: ['edit.component.scss'],
  exportAs: 'alo-secret-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretEditComponent implements OnInit {
  @Input() name = '';

  @Input() namespace = '';

  @Input() mode: 'create' | 'update' = 'create';

  @Input()
  types: SecretTypes[] = [
    SecretTypes.basicAuth,
    SecretTypes.dockerConfig,
    SecretTypes.opaque,
    SecretTypes.oauth2,
  ];

  @Output() saved = new EventEmitter<SecretIdentity>();

  SecretTypes = SecretTypes;

  loading = false;
  saving = false;

  model = defaultModel();

  @ViewChild('form') form: NgForm;

  projects: Project[] = [];

  belongsProject: Project;

  showPassword: boolean;

  showClientSecret: boolean;

  showDockerPassword: boolean;

  error: any;

  get notReady() {
    if (this.model.ownerReferences.length) {
      return true;
    }

    if (this.namespace) {
      return this.loading || this.error;
    } else {
      return this.loading || this.error || this.projects.length === 0;
    }
  }

  constructor(
    private api: SecretApiService,
    private projectApi: ProjectApiService,
    private notification: NotificationService,
    private message: MessageService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.prepareForm();
  }

  prepareForm() {
    this.loading = true;
    this.error = null;

    this.getModel()
      .pipe(
        tap(model => {
          this.model = model;
          this.form.resetForm(this.model);
        }),
        concatMap(() => {
          return forkJoin(this.getAllProjects(), this.getBelongsProject());
        }),
      )
      .subscribe(
        ([projects, belongsProject]) => {
          if (projects) {
            this.projects = projects;
          }
          if (belongsProject) {
            this.belongsProject = belongsProject;
          }
          this.loading = false;
          this.cdr.markForCheck();
        },
        (error: { title: string; content: string; data: any }) => {
          this.loading = false;
          this.error = error;
          this.cdr.markForCheck();

          this.notification.warning({
            title: this.translate.get(error.title),
            content: this.translate.get(error.content, error.data),
          });
        },
      );
  }

  save() {
    if (!this.form.valid) {
      return;
    }

    const handler =
      this.mode === 'create'
        ? this.api.post(this.model)
        : this.api.put(this.model);
    const succMessage =
      this.mode === 'create' ? 'secret.create_succ' : 'secret.update_succ';
    const errorMessage =
      this.mode === 'create' ? 'secret.create_fail' : 'secret.update_fail';

    this.saving = true;
    handler.subscribe(
      () => {
        this.saving = false;
        this.cdr.markForCheck();
        this.saved.emit({
          name: this.model.name,
          namespace: this.model.namespace,
        });
        this.message.success({
          content: this.translate.get(succMessage),
        });
      },
      (error: any) => {
        this.saving = false;
        this.cdr.markForCheck();
        this.notification.error({
          title: this.translate.get(errorMessage),
          content: error.error || error.message || undefined,
        });
      },
    );
  }

  submit() {
    (this.form as any).submitted = true;
    this.form.ngSubmit.emit();
  }

  createProject() {}

  private getModel(): Observable<Secret> {
    if (this.mode === 'update' && (!this.name || !this.namespace)) {
      return throwError({
        title: 'secret.edit_input_invalid',
        content: 'scret.edit_input_invalid_content',
        data: {},
      });
    }

    if (!this.name || !this.namespace) {
      return of({
        ...defaultModel(),
        namespace: this.namespace || '',
        type: head(this.types),
      });
    }

    return this.api.get({ name: this.name, namespace: this.namespace }).pipe(
      catchError(
        (error: HttpErrorResponse): Observable<Secret> => {
          if (error.status === 404) {
            throw {
              title: 'secret.edit_not_found',
              content: 'secret.edit_not_found_content',
              data: {
                name: this.name,
              },
            };
          }

          throw {
            title: 'secret.edit_load_fail',
            content: 'secret.edit_load_fail_content',
            data: {
              name: this.name,
            },
          };
        },
      ),
    );
  }

  private getBelongsProject() {
    if (!this.namespace) {
      return of(null);
    }
    return this.projectApi.get(this.namespace).pipe(
      catchError(
        (error: HttpErrorResponse): Observable<Project> => {
          if (error.status === 404) {
            throw {
              title: 'secret.edit_belongs_project_not_found',
              content: 'secret.edit_belongs_project_not_found_content',
              data: {
                name: this.namespace,
              },
            };
          }

          throw {
            title: 'secret.edit_belongs_project_load_fail',
            content: 'secret.edit_belongs_project_load_fail_content',
            data: {
              name: this.namespace,
            },
          };
        },
      ),
    );
  }

  private getAllProjects(): Observable<Project[]> {
    if (this.namespace) {
      return of([]);
    }

    return this.projectApi
      .find({
        searchBy: 'name',
        keywords: '',
        sort: '',
        direction: 'asc',
        pageIndex: 0,
        pageSize: 0,
      })
      .pipe(
        map(result => result.items),
        catchError(
          (): Observable<Project[]> => {
            throw {
              title: 'secret.edit_projects_load_fail',
              content: 'secret.edit_projects_load_fail_content',
            };
          },
        ),
      );
  }
}
