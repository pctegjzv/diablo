import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { DIALOG_DATA, DialogRef } from 'alauda-ui';

@Component({
  templateUrl: 'create-dialog.component.html',
  styleUrls: ['create-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretCreateDialogComponent {
  constructor(
    @Inject(DIALOG_DATA) public data: { namespace: string; types: string[] },
    private dialogRef: DialogRef<SecretCreateDialogComponent>,
  ) {}

  close(created = '') {
    this.dialogRef.close(created);
  }
}
