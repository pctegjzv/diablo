import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ApiModule } from '@app/api';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';

import {
  OpaqueSecretComponent,
  SecretCreateDialogComponent,
  SecretEditComponent,
} from './components';
import i18n from './i18n';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ApiModule],
  declarations: [
    SecretEditComponent,
    OpaqueSecretComponent,
    SecretCreateDialogComponent,
  ],
  exports: [SecretEditComponent, SecretCreateDialogComponent],
  entryComponents: [SecretCreateDialogComponent],
})
export class SecretModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('secret', i18n);
  }
}
