import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ASFModule } from '@asf/asf.module';
import {
  CodeEditorIntl,
  CodeEditorModule,
  MonacoProviderService,
  PaginatorIntl,
  TooltipCopyIntl,
} from 'alauda-ui';

import { AppPaginatorIntl } from './app-paginator-intl';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  CustomCodeEditorIntlService,
  CustomTooltipIntlService,
  ServicesModule,
} from './services';
import { CustomMonacoProviderService } from './services/custom-monaco-provider';
import { SharedModule } from './shared';
import { GlobalTranslateModule } from './translate';

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    GlobalTranslateModule,
    ServicesModule,
    SharedModule,
    // App routing module should stay at the bottom
    AppRoutingModule,
    ASFModule,
    CodeEditorModule.forRoot({
      baseUrl: 'lib',
      defaultOptions: {
        fontSize: 12,
        folding: true,
        scrollBeyondLastLine: false,
        minimap: { enabled: false },
        find: { seedSearchStringFromSelection: false },
      },
    }),
  ],
  providers: [
    {
      provide: MonacoProviderService,
      useClass: CustomMonacoProviderService,
    },
    {
      provide: PaginatorIntl,
      useClass: AppPaginatorIntl,
    },
    {
      provide: TooltipCopyIntl,
      useClass: CustomTooltipIntlService,
    },
    {
      provide: CodeEditorIntl,
      useClass: CustomCodeEditorIntlService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
