import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule, NavModule } from 'alauda-ui';

import { SharedModule } from '../../shared/shared.module';
import { GlobalActionsModule } from '../shared/global-actions';

import { WorkspaceRoutingModule } from './workspace-routing.module';
import { WorkspaceComponent } from './workspace.component';

@NgModule({
  imports: [
    CommonModule,
    PortalModule,
    SharedModule,
    LayoutModule,
    GlobalActionsModule,
    NavModule,
    WorkspaceRoutingModule,
  ],
  declarations: [WorkspaceComponent],
  providers: [],
})
export class WorkspaceModule {}
