import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, RouterOutlet } from '@angular/router';
import {
  getRouteConfigPathFromRoot,
  routerTransition,
} from '@app/router-transition';
import {
  ApiGroup,
  NavControlService,
  SwitchableProjectsService,
  TemplateHolderType,
  UiStateService,
} from '@app/services';
import { TranslateService } from '@app/translate';
import { MicroserviceComponentName } from '@asf/asf.constants';
import { Subscription, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

interface NavItem {
  label: string;
  path?: string;
  icon?: string;
  routerLink?: string[];
  items?: NavItem[] | MicroserviceSubNavItem[];
  resources?: string[];
  expanded?: boolean;
  apiGroup?: ApiGroup;
  enabled: boolean;
}

interface MicroserviceSubNavItem extends NavItem {
  component: MicroserviceComponentName;
}

const DEFAULT_NAV_CONFIG: NavItem[] = [
  // {
  //   icon: 'bar_chart_s',
  //   label: 'nav_dashboard',
  // },
  // {
  //   icon: 'application_s',
  //   label: 'nav_applications_manage',
  //   items: [
  //     {
  //       label: 'nav_applications',
  //       routerLink: ['applications'],
  //     },
  //     {
  //       label: 'nav_configs',
  //     },
  //     {
  //       label: 'nav_networks',
  //     },
  //     {
  //       label: 'nav_storage',
  //     },
  //   ],
  // },
  {
    icon: 'application_s',
    label: 'nav_applications',
    routerLink: ['applications'],
    enabled: false,
  },
  {
    icon: 'devops',
    label: 'nav_devops',
    items: [
      {
        label: 'nav_pipelines',
        routerLink: ['pipelines'],
        enabled: true,
      },
      {
        label: 'nav_code_repos',
        routerLink: ['code-repositories'],
        enabled: true,
      },
      {
        label: 'nav_artifact_repository',
        routerLink: ['artifact-repositories'],
        enabled: true,
      },
    ],
    apiGroup: ApiGroup.DevOps,
    enabled: false,
  },
  {
    icon: 'micro_service',
    label: 'nav_microservice',
    items: [
      {
        label: 'nav_services',
        component: MicroserviceComponentName.Zookeeper,
        routerLink: ['microservice/services'],
        enabled: false,
      },
      {
        label: 'nav_configs',
        component: MicroserviceComponentName.ConfigServer,
        routerLink: ['microservice/configs'],
        enabled: false,
      },
      {
        label: 'nav_monitors',
        component: MicroserviceComponentName.Turbine,
        enabled: false,
      },
      {
        label: 'nav_callchains',
        component: MicroserviceComponentName.PinpointHbase,
        enabled: false,
      },
    ],
    apiGroup: ApiGroup.ASF,
    enabled: false,
  },
  {
    icon: 'member_s',
    label: 'nav_members',
    routerLink: ['members'],
    enabled: false,
  },
  {
    icon: 'secrets_s',
    label: 'nav_secrets',
    routerLink: ['secrets'],
    enabled: false,
  },
];

@Component({
  templateUrl: 'workspace.component.html',
  styleUrls: ['workspace.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [routerTransition],
})
export class WorkspaceComponent implements OnInit, OnDestroy {
  config = DEFAULT_NAV_CONFIG;
  project: string;
  apiGroupNames: ApiGroup[] = [];
  subs: Subscription[] = [];

  pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
    TemplateHolderType.PageHeaderContent,
  ).templatePortal$;

  get language() {
    return this.translate.currentLang;
  }

  showLoadingBar$ = this.uiState.showLoadingBar$;

  currentProject$ = combineLatest(
    this.route.paramMap.pipe(map(paramMap => paramMap.get('project'))),
    this.projects.items$,
  ).pipe(
    map(
      ([name, items]) =>
        (items || []).find((item: any) => item.name === name) || { name },
    ),
    map(
      item => `${item.name}${item.displayName ? `(${item.displayName})` : ''}`,
    ),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private uiState: UiStateService,
    private translate: TranslateService,
    public projects: SwitchableProjectsService,
    private route: ActivatedRoute,
    private navControl: NavControlService,
  ) {}

  ngOnInit() {
    this.subs.push(
      this.currentProject$
        .pipe(
          map(project => project.split('(')[0]),
          tap(project => {
            this.project = project;
          }),
          switchMap(() => this.navControl.getApiGroupNames()),
        )
        .subscribe(names => {
          this.apiGroupNames = names;
          this.initNavState();
        }),
    );
  }

  initNavState() {
    this.config.forEach(parent => {
      if (parent.apiGroup) {
        parent.enabled =
          this.apiGroupNames.includes(parent.apiGroup) &&
          (!parent.items || !!parent.items.length);
        // sub items
        if (parent.items && parent.items.length) {
          this.initSubItemsState(parent);
        }
      } else {
        parent.enabled = !parent.items || !!parent.items.length;
      }
    });
  }

  initSubItemsState(nav: NavItem) {
    switch (nav.apiGroup) {
      case ApiGroup.ASF:
        this.subs.push(
          this.navControl
            .getAsfSubMenus(this.project)
            .pipe(
              map(refs =>
                refs
                  .filter(ref => ref.status === 'Running' && ref.hostpath)
                  .map(ref => ref.name),
              ),
            )
            .subscribe(refs => {
              if (!refs.length) {
                nav.enabled = false;
                return;
              }
              (nav.items as MicroserviceSubNavItem[]).forEach(subNav => {
                subNav.enabled = refs.includes(subNav.component);
              });
            }),
        );
        break;
      default:
        break;
    }
  }

  onNavItemClick(currentItem: NavItem) {
    const expanded = currentItem.expanded;
    this.config.forEach(item => {
      item.expanded = false;
    });
    if (currentItem.items) {
      currentItem.expanded = !expanded;
    }
  }

  toggleLanguage() {
    this.translate.changeLanguage(this.translate.otherLang);
  }

  shouldDisplay(nav: NavItem) {
    return (
      !nav.apiGroup ||
      (this.apiGroupNames && this.apiGroupNames.includes(nav.apiGroup))
    );
  }

  getPage(outlet: RouterOutlet) {
    return getRouteConfigPathFromRoot(outlet.activatedRoute);
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
