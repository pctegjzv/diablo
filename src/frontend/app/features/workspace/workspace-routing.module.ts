import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WorkspaceComponent } from './workspace.component';

const routes: Routes = [
  {
    path: ':project',
    component: WorkspaceComponent,
    children: [
      { path: '', redirectTo: 'applications', pathMatch: 'full' },
      {
        path: 'applications',
        loadChildren: '../applications/applications.module#ApplicationsModule',
      },
      {
        path: 'pipelines',
        loadChildren: '../pipelines/pipelines.module#PipelinesModule',
      },
      {
        path: 'microservice',
        loadChildren:
          '@asf/features/microservice/microservice.module#MicroserviceModule',
      },
      {
        path: 'secrets',
        loadChildren: '../secrets/for-workspace.module#ForWorkspaceModule',
      },
      {
        path: 'code-repositories',
        loadChildren:
          '../code-repositories/code-repositories.module#CodeRepositoriesModule',
      },
      {
        path: 'artifact-repositories',
        loadChildren:
          '../artifact-repositories/artifact-repositories.module#ArtifactRepositoriesModule',
      },
      {
        path: 'members',
        loadChildren: '../members/members.module#MembersModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkspaceRoutingModule {}
