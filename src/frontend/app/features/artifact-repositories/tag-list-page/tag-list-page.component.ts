import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegistryApiService } from '@app/api/registry/registry-api.service';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';

@Component({
  templateUrl: 'tag-list-page.component.html',
  styleUrls: ['tag-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagListPageComponent {
  loading = false;

  namespace$ = this.activatedRoute.paramMap.pipe(
    map(param => param.get('project')),
  );
  repoName$ = this.activatedRoute.paramMap.pipe(
    map(param => param.get('name')),
  );

  keyword$$ = new BehaviorSubject<string>('');

  repo$ = combineLatest(this.namespace$, this.repoName$).pipe(
    tap(() => {
      this.loading = true;
    }),
    switchMap(([namespace, name]) =>
      this.registryApi.getRepository(namespace, name),
    ),
    tap(() => {
      this.loading = false;
    }),
    publishReplay(1),
    refCount(),
  );

  tags$ = combineLatest(
    this.repo$.pipe(map(repo => repo.tags)),
    this.keyword$$,
  ).pipe(
    map(([tags, keyword]) => tags.filter(tag => tag.name.includes(keyword))),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private registryApi: RegistryApiService,
    private activatedRoute: ActivatedRoute,
  ) {}
}
