import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegistryApiService } from '@app/api/registry/registry-api.service';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent {
  loading = false;

  namespace$ = this.activeatedRoute.paramMap.pipe(
    map(param => param.get('project')),
  );

  repositoriesFilter$$ = new BehaviorSubject<string>('');

  repositories$ = this.namespace$.pipe(
    tap(() => {
      this.loading = true;
    }),
    switchMap(namespace =>
      this.registryApi.findRepositoriesByNamespace(namespace, null),
    ),
    map(list => list.items),
    tap(() => {
      this.loading = false;
    }),
    publishReplay(1),
    refCount(),
  );

  filteredRepositories$ = combineLatest(
    this.repositories$,
    this.repositoriesFilter$$,
  ).pipe(
    map(([items, keyword]) =>
      items.filter(item => item.image.includes(keyword)),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private activeatedRoute: ActivatedRoute,
    private registryApi: RegistryApiService,
  ) {}
}
