import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {
  getRouteConfigPathFromRoot,
  routerTransition,
} from '@app/router-transition';
import {
  ApiGroup,
  NavControlService,
  TemplateHolderType,
  UiStateService,
} from '@app/services';
import { TranslateService } from '@app/translate';
import { Subscription } from 'rxjs';

interface NavItem {
  label: string;
  path?: string;
  icon?: string;
  routerLink?: string[];
  items?: NavItem[];
  resources?: string[];
  apiGroup?: ApiGroup;
}

const DEFAULT_NAV_CONFIG: NavItem[] = [
  {
    icon: 'project_s',
    label: 'nav_projects',
    routerLink: ['projects'],
    apiGroup: ApiGroup.DevOps,
  },
  {
    icon: 'devops_tools_s',
    label: 'nav_tool_chain',
    routerLink: ['tool-chain'],
    apiGroup: ApiGroup.DevOps,
  },
  {
    icon: 'micro_service',
    label: 'nav_microservice_environments',
    routerLink: ['asf/microservice-environments'],
    apiGroup: ApiGroup.ASF,
  },
  {
    icon: 'secrets_s',
    label: 'nav_secrets',
    routerLink: ['secrets'],
  },
];

@Component({
  templateUrl: 'admin.component.html',
  styleUrls: ['admin.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [routerTransition],
})
export class AdminComponent implements OnInit, OnDestroy {
  config = DEFAULT_NAV_CONFIG;
  apiGroupNames: ApiGroup[] = [];
  sub: Subscription;

  expandedPath = '';

  pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
    TemplateHolderType.PageHeaderContent,
  ).templatePortal$;

  get language() {
    return this.translate.currentLang;
  }

  showLoadingBar$ = this.uiState.showLoadingBar$;

  constructor(
    private uiState: UiStateService,
    private translate: TranslateService,
    private navControl: NavControlService,
  ) {}

  ngOnInit() {
    this.sub = this.navControl.getApiGroupNames().subscribe(names => {
      this.apiGroupNames = names;
    });
  }

  shouldDisplay(nav: NavItem) {
    return (
      !nav.apiGroup ||
      (this.apiGroupNames && this.apiGroupNames.includes(nav.apiGroup))
    );
  }

  onNavItemClick(path: string) {
    this.expandedPath = this.expandedPath === path ? '' : path;
  }

  isPathExpanded(path: string) {
    return this.expandedPath === path;
  }

  toggleLanguage() {
    this.translate.changeLanguage(this.translate.otherLang);
  }

  getPage(outlet: RouterOutlet) {
    return getRouteConfigPathFromRoot(outlet.activatedRoute);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
