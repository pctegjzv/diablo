import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from '@app/features/admin/admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'projects', pathMatch: 'full' },
      {
        path: 'projects',
        loadChildren: '../projects/projects.module#ProjectsModule',
      },
      {
        path: 'secrets',
        loadChildren: '../secrets/for-admin.module#ForAdminModule',
      },
      {
        path: 'tool-chain',
        loadChildren: '../tool-chain/tool-chain.module#ToolChainModule',
      },
      {
        path: 'asf/microservice-environments',
        loadChildren:
          '@asf/features/microservice-environments/microservice-environments.module#MicroserviceEnvironmentsModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
