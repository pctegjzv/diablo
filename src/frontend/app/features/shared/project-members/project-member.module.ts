import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../../../shared';
import { TranslateModule } from '../../../translate';

import { ProjectMembersComponent } from './project-members.component';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, TranslateModule],
  declarations: [ProjectMembersComponent],
  exports: [ProjectMembersComponent],
})
export class ProjectMembersModule {}
