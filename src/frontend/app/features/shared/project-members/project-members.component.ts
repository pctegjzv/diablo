import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { find, head } from 'lodash';
import { Subject, Subscription, merge, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  scan,
  shareReplay,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import {
  ANNOTATION_ROLE_NAME,
  LABEL_IS_SHOWN,
  LABEL_ROLE_LEVEL,
} from '../../../constants';
import { TranslateService } from '../../../translate';
import { filterBy, getQuery, sortBy } from '../../../utils/query-builder';

import {
  ActionTypes,
  Actions,
  FetchError,
  FetchFormDependencyError,
  FetchFormDependencyReceived,
  FetchReceived,
  InputChange,
  ProjectChange,
  Search,
  SortChange,
  initialState,
  reducer,
} from './project-members.state';

@Component({
  selector: 'alo-project-members',
  templateUrl: 'project-members.component.html',
  styleUrls: ['project-members.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectMembersComponent implements OnInit, OnChanges, OnDestroy {
  roles: any;
  private subscriptions: Subscription[] = [];
  private actions$ = new Subject<Actions>();

  private state$ = this.actions$.pipe(
    scan(reducer, initialState),
    shareReplay(1),
  );

  public list$ = this.state$.pipe(
    distinctUntilChanged(),
    map(state =>
      state.list.map(item => ({
        name: item.objectMeta.name,
        user: item.objectMeta.name.split(':')[1],
        role: (
          state.roles.find(
            role => role.name === item.objectMeta.name.split(':')[0],
          ) || {}
        ).displayName,
        creationTimestamp: item.objectMeta.creationTimestamp,
      })),
    ),
    shareReplay(1),
  );

  public roles$ = this.state$.pipe(
    map(state => state.roles),
    distinctUntilChanged(),
    shareReplay(1),
  );

  public loading$ = this.state$.pipe(
    map(state => state.loading),
    distinctUntilChanged(),
    shareReplay(1),
  );

  public inputValue$ = this.state$.pipe(
    map(state => state.inputValue),
    distinctUntilChanged(),
    shareReplay(1),
  );

  public sort$ = this.state$.pipe(
    map(state => state.sort),
    distinctUntilChanged(),
    shareReplay(1),
  );

  public desc$ = this.state$.pipe(
    map(state => state.desc),
    distinctUntilChanged(),
    shareReplay(1),
  );

  private projectChange$ = this.actions$.pipe(
    filter<ProjectChange>(action => action.type === ActionTypes.ProjectChange),
    map(() =>
      getQuery(
        filterBy('kind', 'RoleBinding'),
        filterBy('namespace', this.project),
      ),
    ),
  );

  private fetchFormDependencyEffect$ = this.actions$.pipe(
    filter<ProjectChange>(action => action.type === ActionTypes.ProjectChange),
    switchMap(() =>
      this.http.get('api/v1/rbac/role').pipe(
        map((result: any) =>
          result.items
            .filter((item: any) => {
              const labels = item.objectMeta.labels || {};
              return (
                (labels[LABEL_ROLE_LEVEL] === 'namespace' ||
                  labels[LABEL_ROLE_LEVEL] === 'project') &&
                labels[LABEL_IS_SHOWN] === 'true'
              );
            })
            .map((item: any) => ({
              name: item.objectMeta.name,
              displayName: {
                en: item.objectMeta.annotations[`${ANNOTATION_ROLE_NAME}-en`],
                zh: item.objectMeta.annotations[`${ANNOTATION_ROLE_NAME}-zh`],
              },
            })),
        ),
        tap((items: any) => {
          this.roles = items;
        }),
        map(roles => new FetchFormDependencyReceived([], roles)),
        catchError(error => of(new FetchFormDependencyError(error))),
      ),
    ),
    tap(action => {
      if (action.type === ActionTypes.FetchFormDependencyError) {
        return;
      }
      this.model = {
        email: '',
        role: (action.roles.length && head(action.roles).name) || '',
      };
      this.ngForm.reset(this.model);
    }),
  );

  private search$ = this.actions$.pipe(
    filter<Search>(action => action.type === ActionTypes.Search),
    withLatestFrom(this.state$),
    map(([, state]) =>
      getQuery(
        filterBy('kind', 'RoleBinding'),
        filterBy('namespace', this.project),
        filterBy('name', state.inputValue),
        filterBy('displayName', state.inputValue),
      ),
    ),
  );

  private sortChange$ = this.actions$.pipe(
    filter<SortChange>(action => action.type === ActionTypes.SortChange),
    withLatestFrom(this.state$),
    map(([action, state]) =>
      getQuery(
        filterBy('kind', 'RoleBinding'),
        filterBy('namespace', this.project),
        filterBy('name', state.keywords),
        filterBy('displayName', state.keywords),
        sortBy(action.sort, action.desc),
      ),
    ),
  );

  private inputEffect$ = this.actions$.pipe(
    filter<InputChange>(action => action.type === ActionTypes.InputChange),
    debounceTime(500),
    map(() => new Search()),
  );

  private fetchEffect$ = merge(
    this.projectChange$,
    this.search$,
    this.sortChange$,
  ).pipe(
    switchMap(params =>
      this.http.get('api/v1/others', { params }).pipe(
        map(
          (result: any) =>
            new FetchReceived(
              result.resources.filter(
                (item: any) => item.objectMeta.namespace === this.project,
              ),
            ),
        ),
        catchError(error => of(new FetchError(error))),
      ),
    ),
  );

  @Input() project = '';

  @ViewChild('createDialog') createDialog: TemplateRef<any>;
  @ViewChild(NgForm) ngForm: NgForm;

  @ViewChild('deleteConfirmDialog')
  private deleteConfirmDialog: TemplateRef<any>;

  deletingItem: any = null;

  model = { email: '', role: '' };

  constructor(
    private http: HttpClient,
    private dialog: MatDialog,
    private translate: TranslateService,
    private snackBar: MatSnackBar,
  ) {
    this.subscriptions.push(
      this.fetchEffect$.subscribe(this.actions$),
      this.fetchFormDependencyEffect$.subscribe(this.actions$),
      this.inputEffect$.subscribe(this.actions$),
    );
  }

  ngOnInit() {
    if (this.project) {
      this.actions$.next(new ProjectChange(this.project));
    }
  }

  ngOnChanges({ project }: SimpleChanges) {
    if (project && !project.firstChange && project.currentValue) {
      this.actions$.next(new ProjectChange(project.currentValue));
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  inputChange(value: string) {
    this.actions$.next(new InputChange(value));
  }

  search() {
    this.actions$.next(new Search());
  }

  getDisplayName(role: any) {
    return (
      role[this.translate.currentLang] ||
      role.displayName[this.translate.currentLang]
    );
  }

  sortChange(event: { active: string; direction: string }) {
    this.actions$.next(
      new SortChange(event.active, event.direction === 'desc'),
    );
  }

  showCreate() {
    this.dialog.open(this.createDialog, { width: '900px' });
  }

  hideCreate() {
    this.dialog.closeAll();
  }

  save() {
    if (this.ngForm.valid) {
      this.http
        .post(`api/v1/others`, [
          toResource(this.model, this.project, this.roles),
        ])
        .pipe(
          map((result: any) => result.failed_resource_count === 0),
          catchError(() => of(false)),
          withLatestFrom(
            this.translate.stream(`member_add_succ`),
            this.translate.stream(`member_add_fail`),
          ),
        )
        .subscribe(([flag, succ, fail]) => {
          if (flag) {
            this.snackBar.open(succ, null, {
              duration: 2000,
            });
            this.actions$.next(new Search());
          } else {
            this.snackBar.open(fail, null, {
              duration: 2000,
            });
          }
        });
    }
  }

  showDeleteConfirm(item: any) {
    this.deletingItem = item;
    this.dialog.open(this.deleteConfirmDialog, {
      width: '420px',
    });
  }

  delete() {
    this.dialog.closeAll();
    this.http
      .delete(
        `api/v1/others/rbac.authorization.k8s.io/v1/RoleBinding/${
          this.project
        }/${this.deletingItem.name}`,
      )
      .pipe(
        map(() => true),
        catchError(() => of(false)),
        withLatestFrom(
          this.translate.stream('member_delete_succ'),
          this.translate.stream('member_delete_fail'),
        ),
      )
      .subscribe(([flag, succ, fail]) => {
        if (flag) {
          this.snackBar.open(succ, null, { duration: 2000 });
          this.actions$.next(new Search());
        } else {
          this.snackBar.open(fail, null, { duration: 2000 });
        }
      });
  }

  cancelDelete() {
    this.dialog.closeAll();
  }
}

function toResource(model: any, project: string, roles: any[]): any {
  const targetRole: any = find(roles, { name: model.role });
  return {
    apiVersion: 'rbac.authorization.k8s.io/v1',
    kind: 'RoleBinding',
    metadata: {
      name: `${model.role}:${model.email}`,
      namespace: project,
      annotations: {
        ['devops.alauda.io_displayName_zh']: targetRole.displayName['zh'],
        ['devops.alauda.io_displayName_en']: targetRole.displayName['en'],
      },
      labels: {
        [`alauda.io/system-rolebinding`]: 'true',
      },
    },
    roleRef: {
      apiGroup: 'rbac.authorization.k8s.io',
      kind: 'ClusterRole',
      name: model.role,
    },
    subjects: [
      {
        apiGroup: 'rbac.authorization.k8s.io',
        kind: 'User',
        name: model.email,
      },
    ],
  };
}
