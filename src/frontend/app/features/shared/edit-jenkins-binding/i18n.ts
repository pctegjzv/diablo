export const en = {
  service: 'Jenkins Service',
  secret: 'Secret',
  create_succ: 'Jenkins Binding Failed',
  create_fail: 'Jenkins Binding Successed',
};

export const zh = {
  service: 'Jenkins 服务',
  secret: 'Secret',
  create_succ: 'Jenkins 绑定成功',
  create_fail: 'Jenkins 绑定失败',
};

export default {
  en,
  zh,
};
