import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../../../shared';
import { TranslateService } from '../../../translate';

import { EditJenkinsBindingComponent } from './edit-jenkins-binding.component';
import i18n from './i18n';

@NgModule({
  imports: [FormsModule, SharedModule],
  declarations: [EditJenkinsBindingComponent],
  exports: [EditJenkinsBindingComponent],
})
export class EditJenkinsBindingModule {
  constructor(public translate: TranslateService) {
    this.translate.setTranslations('edit_jenkins_binding', i18n);
  }
}
