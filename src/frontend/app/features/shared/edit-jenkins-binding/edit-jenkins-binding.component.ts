import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { head } from 'lodash';
import { Observable, Subject, Subscription, forkJoin, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  scan,
  shareReplay,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_PRODUCT,
  API_GROUP_VERSION,
  PRODUCT_NAME,
} from '../../../constants';
import { TranslateService } from '../../../translate';
import { getQuery } from '../../../utils/query-builder';

import {
  ActionTypes,
  Actions,
  FetchDependencyError,
  FetchDependencyReceived,
  ProjectChange,
  initialState,
  reducer,
} from './edit-jenkins-binding.state';

@Component({
  selector: 'alo-edit-jenkins-binding',
  templateUrl: 'edit-jenkins-binding.component.html',
  styleUrls: ['edit-jenkins-binding.component.scss'],
  exportAs: 'alo-edit-jenkins-binding',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditJenkinsBindingComponent
  implements OnInit, OnChanges, OnDestroy {
  model: any = {};
  subscriptions: Subscription[] = [];
  actions$ = new Subject<Actions>();
  state$ = this.actions$.pipe(
    scan(reducer, initialState),
    distinctUntilChanged(),
    shareReplay(1),
  );

  initializing$ = this.state$.pipe(
    map(state => state.initializing),
    distinctUntilChanged(),
    shareReplay(1),
  );

  jenkinsOptions$ = this.state$.pipe(
    map(state => state.jenkins),
    distinctUntilChanged(),
    shareReplay(1),
  );

  secretOptions$ = this.state$.pipe(
    map(state => state.secrets),
    distinctUntilChanged(),
    shareReplay(1),
  );

  saving$ = this.state$.pipe(
    map(state => state.saving),
    distinctUntilChanged(),
    shareReplay(1),
  );

  private projectChangeEffect$ = this.actions$.pipe(
    filter<ProjectChange>(action => action.type === ActionTypes.ProjectChange),
    switchMap(
      (): Observable<Actions> => {
        return forkJoin([
          this.http.get('api/v1/jenkinses'),
          this.http.get(`api/v1/secret/${this.project}`, {
            params: getQuery(),
            // TODO: need filter only basicAuth
          }),
        ]).pipe(
          map(
            ([jenkins, secrets]: any[]) =>
              new FetchDependencyReceived(
                jenkins.jenkins,
                secrets.secrets.filter(
                  (item: any) => item.type === 'kubernetes.io/basic-auth',
                ),
              ),
          ),
          catchError(error => of(new FetchDependencyError(error))),
        );
      },
    ),
    tap(action => {
      if (action.type === ActionTypes.FetchDependencyError) {
        return;
      }

      const { jenkins, secrets } = action as FetchDependencyReceived;

      this.model = {
        jenkins: jenkins.length ? head(jenkins).objectMeta.name : '',
        secret: secrets.length ? head(secrets).objectMeta.name : '',
        description: '',
      };
      this.ngForm.reset(this.model);
    }),
  );

  @ViewChild(NgForm) ngForm: NgForm;

  @Input() project = '';

  @Output() created = new EventEmitter<string>();

  constants = { ANNOTATION_DESCRIPTION, ANNOTATION_DISPLAY_NAME };

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private snackBar: MatSnackBar,
  ) {
    this.subscriptions.push(this.projectChangeEffect$.subscribe(this.actions$));
  }

  ngOnInit() {
    this.actions$.next(new ProjectChange(this.project || ''));
  }

  ngOnChanges({ project }: SimpleChanges) {
    if (project && !project.firstChange) {
      this.actions$.next(new ProjectChange(this.project || ''));
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  submit() {
    this.ngForm.ngSubmit.emit();
    (this.ngForm as any).submitted = true;
  }

  save() {
    if (this.ngForm.valid) {
      this.http
        .post('api/v1/others', [toResource(this.model, this.project)])
        .pipe(
          map((result: any) => result.failed_resource_count === 0),
          catchError(() => of(false)),
          withLatestFrom(
            this.translate.stream('edit_jenkins_binding.create_succ'),
            this.translate.stream('edit_jenkins_binding.create_fail'),
          ),
        )
        .subscribe(([flag, succ, fail]) => {
          if (flag) {
            this.snackBar.open(succ, null, {
              duration: 2000,
            });
            // TODO: need by result
            this.created.next(this.model.name);
          } else {
            this.snackBar.open(fail, null, {
              duration: 2000,
            });
          }
        });
    }
  }

  fieldError(field: string, mapper: (key: string) => string = key => key) {
    const control = this.ngForm.controls[field];

    if (!control) {
      return null;
    }

    if ((this.ngForm.submitted || control.dirty) && !control.valid) {
      const errorKey = Object.keys(control.errors || {}).find(
        key => control.errors[key],
      );

      return mapper(errorKey);
    }
  }
}

function toResource(model: any, project: string): any {
  return {
    apiVersion: API_GROUP_VERSION,
    kind: 'JenkinsBinding',
    metadata: {
      name: model.jenkins,
      annotations: {
        [ANNOTATION_DESCRIPTION]: model.description,
        [ANNOTATION_PRODUCT]: PRODUCT_NAME,
      },
      namespace: project,
    },
    spec: {
      jenkins: {
        name: model.jenkins,
      },
      account: {
        secret: {
          name: model.secret,
          usernameKey: 'username',
          apiTokenKey: 'password',
        },
      },
    },
  };
}
