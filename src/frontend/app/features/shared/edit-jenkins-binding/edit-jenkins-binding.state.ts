import { Action } from '../../../utils/state-adapters';

export interface State {
  saving: boolean;
  initializing: boolean;
  project: string;
  jenkins: any[];
  secrets: any[];
  error: any;
}

export const initialState: State = {
  saving: false,
  initializing: false,
  project: '',
  jenkins: [],
  secrets: [],
  error: null,
};

export enum ActionTypes {
  ProjectChange = 'ProjectChange',
  FetchDependencyReceived = 'FetchDependencyReceived',
  FetchDependencyError = 'FetchDependencyError',
}

export class ProjectChange implements Action {
  public readonly type = ActionTypes.ProjectChange;
  constructor(public project: string) {}
}

export class FetchDependencyReceived implements Action {
  public readonly type = ActionTypes.FetchDependencyReceived;
  constructor(public jenkins: any[], public secrets: any[]) {}
}

export class FetchDependencyError implements Action {
  public readonly type = ActionTypes.FetchDependencyError;
  constructor(public error: any) {}
}

export type Actions =
  | ProjectChange
  | FetchDependencyReceived
  | FetchDependencyError;

const projectChange = (_state: State, { project }: ProjectChange): State => ({
  ...initialState,
  project,
  initializing: true,
});

const fetchDependencyReceived = (
  state: State,
  { jenkins, secrets }: FetchDependencyReceived,
): State => ({
  ...state,
  jenkins,
  secrets,
  initializing: false,
});

const fetchDependencyError = (
  state: State,
  { error }: FetchDependencyError,
): State => ({
  ...state,
  error,
  initializing: false,
});

export function reducer(state: State, action: Actions): State {
  switch (action.type) {
    case ActionTypes.ProjectChange:
      return projectChange(state, action);
    case ActionTypes.FetchDependencyReceived:
      return fetchDependencyReceived(state, action);
    case ActionTypes.FetchDependencyError:
      return fetchDependencyError(state, action);
    default:
      return state;
  }
}
