import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { safeLoadAll } from 'js-yaml';
import { get } from 'lodash';
import { of } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';

import { TranslateService } from '../../../translate';

@Component({
  selector: 'alo-create-application-by-yaml',
  templateUrl: 'by-yaml.component.html',
  styleUrls: ['by-yaml.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateApplicationByYamlComponent {
  @Input() project = '';
  @Output() saved = new EventEmitter<string>();
  @Output() canceled = new EventEmitter<void>();
  yaml = '';
  editorOptions = { language: 'yaml' };
  saving = false;

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private snackBar: MatSnackBar,
    private cdr: ChangeDetectorRef,
  ) {}

  create() {
    const resources = safeLoadAll(this.yaml).map((resource: any) => ({
      ...resource,
      metadata: {
        ...resource.metadata,
        namespace: this.project,
      },
    }));
    const app = resources.find(
      (resource: any) =>
        (resource.kind === 'Deployment' &&
          get(resource, 'metadata.labels.app')) ||
        false,
    );

    if (!app) {
      this.snackBar.open(this.translate.get('application_yaml_invalid'), null, {
        duration: 2000,
      });

      return;
    }

    const appName = get(app, 'metadata.labels.app');

    this.saving = true;
    this.cdr.detectChanges();

    this.http
      .post('api/v1/others', resources)
      .pipe(
        map((result: any) => result.failed_resource_count === 0),
        catchError(() => of(false)),
        withLatestFrom(
          this.translate.stream('application_create_succ'),
          this.translate.stream('application_create_fail'),
        ),
      )
      .subscribe(([flag, succ, fail]) => {
        this.saving = false;
        this.cdr.detectChanges();
        if (flag) {
          this.snackBar.open(succ, null, {
            duration: 2000,
          });
          this.saved.next(appName);
        } else {
          this.snackBar.open(fail, null, {
            duration: 2000,
          });
        }
      });
  }
}
