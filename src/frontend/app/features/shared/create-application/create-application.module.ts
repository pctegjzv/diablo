import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import {
  ButtonModule,
  CardModule as AUICardModule,
  CodeEditorModule,
  FormFieldModule,
  IconModule,
} from 'alauda-ui';

import { CardModule } from '../../../shared/components/card/card.module';
import { TranslateModule } from '../../../translate';

import { CreateApplicationByImageComponent } from './by-image.component';
import { CreateApplicationByTemplateComponent } from './by-template.component';
import { CreateApplicationByYamlComponent } from './by-yaml.component';
import { EnvVarsComponent } from './env-vars.component';
import { ApplicationResourceReportDetailComponent } from './resource-report-detail.component';
import { TemplateResourceReportComponent } from './template-resource-report.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CodeEditorModule,
    ButtonModule,
    CardModule,
    IconModule,
    AUICardModule,
    TranslateModule,
    FormFieldModule,
    MatDialogModule,
  ],
  declarations: [
    CreateApplicationByImageComponent,
    CreateApplicationByTemplateComponent,
    CreateApplicationByYamlComponent,
    EnvVarsComponent,
    TemplateResourceReportComponent,
    ApplicationResourceReportDetailComponent,
  ],
  exports: [
    CreateApplicationByImageComponent,
    CreateApplicationByTemplateComponent,
    CreateApplicationByYamlComponent,
    ApplicationResourceReportDetailComponent,
  ],
})
export class CreateApplicationModule {}
