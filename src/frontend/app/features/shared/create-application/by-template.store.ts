import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { flatMap, get } from 'lodash';
import { Subject, Subscription, forkJoin, from, of } from 'rxjs';
import {
  catchError,
  concatMap,
  delay,
  distinctUntilChanged,
  filter,
  map,
  scan,
  shareReplay,
  skipWhile,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';

import {
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_PRODUCT,
  PRODUCT_NAME,
} from '../../../constants';
import { Action } from '../../../utils/state-adapters';

import { TranslateService } from './../../../translate/translate.service';

export interface TemplateGroup {
  displayName: {
    en: string;
    'zh-CN': string;
  };
  items: TemplateItem[];
}

export interface TemplateItem {
  key: string;
  Default: string;
  display: {
    type: string;
    name: {
      en: string;
      'zh-CN': string;
    };
    description: {
      en: string;
      'zh-CN': string;
    };
  };
  required: boolean;
  schema: {
    type: string;
  };
  validation: {
    pattern: string;
    maxLength: number;
  };
}

export interface Option {
  text: string;
  value: string;
}

export interface Inputs {
  project: string;
  template: string;
}

export interface State {
  project: string;
  template: string;
  templateValues: TemplateGroup[];
  loading: boolean;
  loadingOptions: boolean;
  saving: boolean;
  options: {
    [field: string]: Option[];
  };
}

export const initialState: State = {
  project: '',
  template: '',
  templateValues: [],
  loading: false,
  loadingOptions: false,
  saving: false,
  options: {},
};

export enum ActionTypes {
  InputsChange = 'InputsChange',
  FetchChartReceived = 'FetchChartReceived',
  FetchChartError = 'FetchChartError',
  FetchOptionsReceived = 'FetchOptionsReceived',
  FetchOptionsError = 'FetchOptionsError',
  FetchOptionsComplete = 'FetchOptionsComplete',
  Save = 'Save',
  SaveSucc = 'SaveSucc',
  SaveFail = 'SaveFail',
  FetchRelease = 'FetchRelease',
}

export class InputsChange implements Action {
  public readonly type = ActionTypes.InputsChange;
  constructor(public inputs: Inputs, public initial: boolean) {}
}

export class FetchChartReceived implements Action {
  public readonly type = ActionTypes.FetchChartReceived;
  constructor(public data: any) {}
}

export class FetchChartError implements Action {
  public readonly type = ActionTypes.FetchChartError;
  constructor(public error: any) {}
}

export class FetchOptionsReceived implements Action {
  public readonly type = ActionTypes.FetchOptionsReceived;
  constructor(public field: string, public options: Option[]) {}
}

export class FetchOptionsError implements Action {
  public readonly type = ActionTypes.FetchOptionsError;
  constructor(public error: string) {}
}

export class FetchOptionsComplete implements Action {
  public readonly type = ActionTypes.FetchOptionsComplete;
}

export class Save implements Action {
  public readonly type = ActionTypes.Save;
  constructor(public data: any) {}
}

export class SaveSucc implements Action {
  public readonly type = ActionTypes.SaveSucc;
  constructor(public data: any) {}
}

export class SaveFail implements Action {
  public readonly type = ActionTypes.SaveFail;
  constructor(public error: any) {}
}

export class FetchRelease implements Action {
  public readonly type = ActionTypes.FetchRelease;
  constructor(public name: string) {}
}

export type Actions =
  | InputsChange
  | FetchChartReceived
  | FetchChartError
  | FetchOptionsReceived
  | FetchOptionsError
  | FetchOptionsComplete
  | Save
  | SaveSucc
  | SaveFail
  | FetchRelease;

function reducer(state: State, action: Actions): State {
  switch (action.type) {
    case ActionTypes.InputsChange:
      return {
        ...initialState,
        project: action.inputs.project,
        template: action.inputs.template,
        loading: true,
        loadingOptions: true,
      };
    case ActionTypes.FetchChartReceived:
      return {
        ...state,
        templateValues: get(action.data, 'data.spec.values', []),
        loading: false,
      };
    case ActionTypes.FetchChartError:
      return {
        ...state,
        loading: false,
      };
    case ActionTypes.FetchOptionsReceived:
      return {
        ...state,
        options: {
          ...state.options,
          [action.field]: action.options,
        },
      };
    case ActionTypes.FetchOptionsError:
    case ActionTypes.FetchOptionsComplete:
      return {
        ...state,
        loadingOptions: false,
      };
    case ActionTypes.Save:
    case ActionTypes.FetchRelease:
      return {
        ...state,
        saving: true,
      };
    case ActionTypes.SaveSucc:
    case ActionTypes.SaveFail:
      return {
        ...state,
        saving: false,
      };
    default:
      return state;
  }
}

@Injectable()
export class CreateApplicationByTemplateStore {
  private subscriptions: Subscription[] = [];
  actions$ = new Subject<Actions>();
  state$ = this.actions$.pipe(
    scan(reducer, initialState),
    shareReplay(1),
  );

  fetchEffect$ = this.actions$.pipe(
    filter<InputsChange>(action => action.type === ActionTypes.InputsChange),
    skipWhile(action => !action.initial),
    switchMap(action =>
      this.http
        .get(
          `api/v1/others/catalog.alauda.io/v1alpha1/Chart/_/${
            action.inputs.template
          }`,
        )
        .pipe(
          map((result: any) => new FetchChartReceived(result)),
          catchError(error => of(new FetchChartError(error))),
        ),
    ),
  );

  fetchOptionsEffect$ = this.actions$.pipe(
    filter<FetchChartReceived>(
      action => action.type === ActionTypes.FetchChartReceived,
    ),
    map(action => get(action.data, 'data.spec.values', [])),
    map((values: TemplateGroup[]) => flatMap(values.map(value => value.items))),
    map((items: TemplateItem[]) =>
      items.filter(item => isSelect(item.display.type)),
    ),
    withLatestFrom(this.state$),
    switchMap(([items, state]) =>
      forkJoin(items.map(getSelectOptions(this.http, state.project))).pipe(
        catchError(error => of([new FetchOptionsError(error)])),
      ),
    ),
    concatMap(results => {
      if (
        results.length === 1 &&
        results[0].type === ActionTypes.FetchOptionsError
      ) {
        return from([...results]);
      } else {
        return from([...results, new FetchOptionsComplete()]);
      }
    }),
  );

  saveEffect$ = this.actions$.pipe(
    filter<Save>(action => action.type === ActionTypes.Save),
    withLatestFrom(this.state$),
    switchMap(([action, state]) =>
      this.http.post('api/v1/releases', [toResource(action.data, state)]).pipe(
        map((results: any[]) => {
          if (results.length === 1 && !results[0].error) {
            return new FetchRelease(get(results[0], 'data.metadata.name'));
          } else {
            if (results.length === 1) {
              return new SaveFail({
                applicationName: action.data.name,
                error: results[0].error.ErrStatus.message,
              });
            }
            return new SaveFail({
              applicationName: action.data.name,
              error: this.translateService.get('unknow_error'),
            });
          }
        }),
        catchError(error =>
          of(
            new SaveFail({
              applicationName: action.data.name,
              error: error.error || error.message,
            }),
          ),
        ),
      ),
    ),
  );

  fetchRelease$ = this.actions$.pipe(
    filter<FetchRelease>(action => action.type === ActionTypes.FetchRelease),
    withLatestFrom(this.state$),
    delay(2000),
    switchMap(([action, state]) =>
      this.http.get(`api/v1/releases/${state.project}/${action.name}`).pipe(
        map((result: any) => {
          if (result && result.status && result.status.status) {
            return new SaveSucc(result);
          } else {
            return new FetchRelease(action.name);
          }
        }),
        catchError(error =>
          of(
            new SaveFail({
              applicationName: action.name,
              error: error.error || error.message,
            }),
          ),
        ),
      ),
    ),
  );

  constructor(
    private http: HttpClient,
    private translateService: TranslateService,
  ) {
    this.subscriptions.push(
      this.fetchEffect$.subscribe(this.actions$),
      this.saveEffect$.subscribe(this.actions$),
      this.fetchOptionsEffect$.subscribe(this.actions$),
      this.fetchRelease$.subscribe(this.actions$),
    );
  }

  select<T>(fn: (state: State) => T, comparator?: (a: T, b: T) => boolean) {
    return this.state$.pipe(
      map(fn),
      distinctUntilChanged(...(comparator ? [comparator] : [])),
      shareReplay(1),
    );
  }

  on<TAction extends Actions>(
    type: ActionTypes,
    fn: ((action: TAction, state: State) => void) = () => {},
  ) {
    this.subscriptions.push(
      this.actions$
        .pipe(
          filter<TAction>(action => action.type === type),
          withLatestFrom(this.state$),
        )
        .subscribe(([action, state]) => fn(action, state)),
    );
  }

  destroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  inputsChange(inputs: Inputs, initial: boolean = false) {
    this.actions$.next(new InputsChange(inputs, initial));
  }

  save(data: any) {
    this.actions$.next(new Save(data));
  }
}

function toResource(model: any, state: State) {
  return {
    apiVersion: 'catalog.alauda.io/v1alpha1',
    kind: 'Release',
    metadata: {
      name: model.name,
      namespace: state.project,
      annotations: {
        [ANNOTATION_PRODUCT]: PRODUCT_NAME,
        'alauda.io/chart.type': 'Application',
      },
      labels: {
        app: model.name,
      },
    },
    spec: {
      chart: {
        name: state.template,
        version: '0.1',
      },
      values: state.templateValues.map(group => ({
        displayName: group.displayName,
        items: group.items.map(item => ({
          ...item,
          value: (model[item.key] || '').toString(),
        })),
      })),
    },
  };
}

function getSelectOptions(http: HttpClient, namespace: string) {
  return function(item: TemplateItem) {
    switch (item.display.type) {
      case 'devops.alauda.io/v1alpha1/jenkinsbinding':
        return http.get(`api/v1/jenkinsbinding/${namespace}`).pipe(
          map((result: any) => result.jenkinsbindings),
          map((jenkinsbindings: any) => jenkinsbindings.map(toOption)),
          map(options => new FetchOptionsReceived(item.key, options)),
        );
      case 'v1/secret-basic-auth':
        return http.get(`api/v1/secret/${namespace}`).pipe(
          map((result: any) => result.secrets),
          map((secrets: any) =>
            secrets.filter(
              (secret: any) =>
                getAnnotation(secret, ANNOTATION_PRODUCT) === PRODUCT_NAME &&
                secret.type === 'kubernetes.io/basic-auth',
            ),
          ),
          map((secrets: any) => secrets.map(toOption)),
          map(secrets => new FetchOptionsReceived(item.key, secrets)),
        );
      case 'v1/secret-dockerconfigjson':
        return http.get(`api/v1/secret/${namespace}`).pipe(
          map((result: any) => result.secrets),
          map((secrets: any) =>
            secrets.filter(
              (secret: any) =>
                getAnnotation(secret, ANNOTATION_PRODUCT) === PRODUCT_NAME &&
                secret.type === 'kubernetes.io/dockerconfigjson',
            ),
          ),
          map((secrets: any) => secrets.map(toOption)),
          map(secrets => new FetchOptionsReceived(item.key, secrets)),
        );
      case 'v1/secret-opaque':
        return http.get(`api/v1/secret/${namespace}`).pipe(
          map((result: any) => result.secrets),
          map((secrets: any) =>
            secrets.filter(
              (secret: any) =>
                getAnnotation(secret, ANNOTATION_PRODUCT) === PRODUCT_NAME &&
                secret.type === 'Opaque',
            ),
          ),
          map((secrets: any) => secrets.map(toOption)),
          map(secrets => new FetchOptionsReceived(item.key, secrets)),
        );
      default:
        return of(new FetchOptionsReceived(item.key, []));
    }
  };
}

function toOption(resource: any) {
  const displayName = getAnnotation(resource, ANNOTATION_DISPLAY_NAME);
  const name = get(resource, 'objectMeta.name');

  return {
    text: displayName || name,
    value: name,
  };
}

function getAnnotation(resource: any, name: string) {
  const annotations = get(resource, 'objectMeta.annotations') || {};
  return annotations[name] || '';
}

export function isSelect(type: string) {
  return [
    'v1/secret-basic-auth',
    'v1/secret-opaque',
    'v1/secret-dockerconfigjson',
    'devops.alauda.io/v1alpha1/jenkinsbinding',
  ].includes(type);
}
