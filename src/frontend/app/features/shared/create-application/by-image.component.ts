import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { isInteger } from 'lodash';
import { Subject, of } from 'rxjs';
import { catchError, map, takeUntil, withLatestFrom } from 'rxjs/operators';

import {
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_PRODUCT,
  PRODUCT_NAME,
} from '../../../constants';
import { TranslateService } from '../../../translate';

@Component({
  selector: 'alo-create-application-by-image',
  templateUrl: 'by-image.component.html',
  styleUrls: ['by-image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateApplicationByImageComponent implements OnInit, OnDestroy {
  @Input() project = '';
  @Output() saved = new EventEmitter<string>();
  @Output() canceled = new EventEmitter<void>();
  destroy$ = new Subject<void>();
  secrets: any[];
  model: any = {
    name: '',
    imageAddress: '',
    secret: '',
    protocol: 'TCP',
    port: null,
    domain: '',
    envVars: [] as any[],
  };
  loading = false;
  saving = false;

  constants = {
    ANNOTATION_DISPLAY_NAME,
    ANNOTATION_PRODUCT,
  };

  @ViewChild('form') form: NgForm;

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private snackBar: MatSnackBar,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.loading = true;
    this.http
      .get(`api/v1/secret/${this.project}`)
      .pipe(
        map((res: any) => res.secrets),
        map((secrets: any[]) =>
          secrets.filter(
            item =>
              item.type === 'kubernetes.io/dockerconfigjson' &&
              item.objectMeta &&
              item.objectMeta.annotations &&
              item.objectMeta.annotations[ANNOTATION_PRODUCT] === PRODUCT_NAME,
          ),
        ),
        map(secrets =>
          secrets.map(item => ({
            name: item.objectMeta.name,
            displayName:
              item.objectMeta.annotations[ANNOTATION_DISPLAY_NAME] ||
              item.objectMeta.name,
          })),
        ),
        map(secrets => [null, secrets]),
        catchError(error => of([error, []])),
        withLatestFrom(
          this.translate.stream('form_intialize_fail'),
          (result, failMessage) => [...result, failMessage],
        ),
        takeUntil(this.destroy$),
      )
      .subscribe(([error, secrets, failMessage]) => {
        this.secrets = secrets;
        this.loading = false;
        this.cdr.detectChanges();
        if (error) {
          this.snackBar.open(this.translate.get(failMessage), null, {
            duration: 2000,
          });
          this.canceled.next();
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  create() {
    if (this.form.valid) {
      this.saving = true;
      this.cdr.detectChanges();
      this.http
        .post('api/v1/appdeployment', toResource(this.model, this.project))
        .pipe(
          map((res: any) => [null, res]),
          catchError(error => of([error, null])),
          withLatestFrom(
            this.translate.stream('application_create_succ'),
            this.translate.stream('application_create_fail'),
            (result, succMessage, failMessage) => [
              ...result,
              succMessage,
              failMessage,
            ],
          ),
        )
        .subscribe(([error, _res, succMessage, failMessage]) => {
          this.saving = false;
          this.cdr.detectChanges();
          this.snackBar.open(error ? failMessage : succMessage, null, {
            duration: 2000,
          });
          if (!error) {
            this.saved.next(this.model.name);
          }
        });
    }
  }

  submit() {
    (this.form.submitted as any) = true;
    this.form.ngSubmit.emit();
  }

  fieldError(field: string, mapper: (key: string) => string = key => key) {
    const control = this.form.controls[field];

    if (!control) {
      return null;
    }

    if ((this.form.submitted || control.dirty) && !control.valid) {
      const errorKey = Object.keys(control.errors || {}).find(
        key => control.errors[key],
      );

      return mapper(errorKey);
    }
  }
}

function toResource(model: any, namespace: string): any {
  const secret: any = {};
  if (model.secret) {
    secret['imagePullSecret'] = model.secret;
  }
  return {
    name: model.name,
    containerImage: model.imageAddress,
    portMappings: isInteger(model.port)
      ? [
          {
            port: model.port,
            targetPort: model.port,
            protocol: model.protocol,
          },
        ]
      : [],
    variables: model.envVars,
    replicas: 1,
    isExternal: true,
    namespace,
    domain: model.domain,
    labels: [
      {
        key: 'app',
        value: model.name,
      },
    ],
    ...secret,
  };
}
