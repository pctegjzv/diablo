import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ApplicationIdentity, Report, toReports } from '@app/api';
import { get, isEmpty } from 'lodash';
import { combineLatest } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { TranslateService } from '../../../translate';

import {
  ActionTypes,
  CreateApplicationByTemplateStore,
  FetchChartReceived,
  SaveFail,
  SaveSucc,
  isSelect,
} from './by-template.store';

@Component({
  selector: 'alo-create-application-by-template',
  templateUrl: 'by-template.component.html',
  styleUrls: ['by-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [CreateApplicationByTemplateStore],
})
export class CreateApplicationByTemplateComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input() project = '';
  @Input() template = '';
  @Output() saved = new EventEmitter<string>();
  @Output() canceled = new EventEmitter<void>();
  @Output() detail = new EventEmitter<string>();
  @Output() deleted = new EventEmitter<ApplicationIdentity>();

  @ViewChild('form') form: NgForm;
  @ViewChild('savingDialog') savingDialog: TemplateRef<any>;

  model: any = {};

  saving$ = this.store.select(state => state.saving);
  savingDialogRef: MatDialogRef<any>;

  createError = false;
  createPartSuccess = false;
  reports: Report[];
  errorMessage: string;
  applicationName: string;

  MAX_LENGTH = 10000;

  values$ = combineLatest(
    this.store.select(state => state.templateValues),
    this.store.select(state => state.options),
    this.translate.currentLang$,
  ).pipe(
    map(([values, options, lang]) => {
      return values.map(value => ({
        ...value,
        displayName:
          lang === 'zh' ? value.displayName['zh-CN'] : value.displayName['en'],
        items: value.items.map(item => ({
          ...item,
          type: isSelect(item.display.type) ? 'select' : item.display.type,
          displayName:
            lang === 'zh'
              ? item.display.name['zh-CN']
              : item.display.name['en'],
          description:
            lang === 'zh'
              ? item.display.description['zh-CN']
              : item.display.description['en'],
          options: options[item.key] || [],
          validation: item.validation || {},
        })),
      }));
    }),
    shareReplay(1),
  );

  constructor(
    public store: CreateApplicationByTemplateStore,
    private dialog: MatDialog,
    public translate: TranslateService,
  ) {
    this.store.on(
      ActionTypes.FetchChartReceived,
      (action: FetchChartReceived) => {
        this.model = get(action.data, 'data.spec.values', []).reduce(
          (accum: any, group: any) => {
            return {
              ...accum,
              ...group.items.reduce(
                (fields: any, field: any) => ({
                  ...fields,
                  [field.key]: field.Default || null,
                }),
                {},
              ),
            };
          },
          { name: '' },
        );
        this.form.reset(this.model);
      },
    );
    this.store.on(ActionTypes.SaveSucc, (action: SaveSucc) => {
      this.applicationName = get(action.data, 'metadata.name');
      this.reports = toReports(action.data.ops);
      const errorReports = this.reports.filter(report => report.error);
      if (isEmpty(errorReports)) {
        this.saved.emit(this.applicationName);
        return;
      } else if (errorReports.length === this.reports.length) {
        this.createError = true;
      } else {
        this.createPartSuccess = true;
      }
    });
    this.store.on(ActionTypes.SaveFail, (action: SaveFail) => {
      this.createError = true;
      this.applicationName = get(action, 'error.applicationName');
      this.errorMessage = get(action, 'error.error');
    });
  }

  ngOnInit() {
    this.store.inputsChange(
      {
        project: this.project,
        template: this.template,
      },
      true,
    );

    this.saving$.subscribe(saving => {
      if (saving) {
        this.savingDialogRef = this.dialog.open(this.savingDialog, {
          width: '400px',
        });
      } else {
        if (this.savingDialogRef) {
          this.savingDialogRef.close();
        }
      }
    });
  }

  ngOnChanges() {
    this.store.inputsChange({
      project: this.project,
      template: this.template,
    });
  }

  ngOnDestroy() {
    this.store.destroy();
  }

  fieldError(field: string, mapper: (key: string) => string = key => key) {
    const control = this.form.controls[field];

    if (!control) {
      return null;
    }

    if ((this.form.submitted || control.dirty) && !control.valid) {
      const errorKey = Object.keys(control.errors || {}).find(
        key => control.errors[key],
      );

      return mapper(errorKey);
    }
  }

  save() {
    this._initData();
    if (this.form.valid) {
      this.store.save(this.model);
    }
  }
  cancel() {
    this.canceled.emit();
  }

  delete() {
    this.deleted.emit({
      name: this.applicationName,
      namespace: this.project,
    });
  }

  viewDetail() {
    this.detail.emit(this.applicationName);
  }

  private _initData() {
    this.createError = false;
    this.createPartSuccess = false;
    this.reports = null;
    this.errorMessage = '';
    this.applicationName = '';
  }
}
