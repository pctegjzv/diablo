import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfigService, AuthService } from '@app/services';
import { TranslateService } from '@app/translate';
import { ToastService } from 'alauda-ui';
import { safeLoadAll } from 'js-yaml';
import { head } from 'lodash';

@Component({
  selector: 'alo-global-actions',
  templateUrl: 'global-actions.component.html',
  styleUrls: ['global-actions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlobalActionsComponent {
  get otherLanguage() {
    return this.translate.otherLang;
  }

  get isWorkspace() {
    if (!this.route.snapshot.parent) {
      return false;
    }

    const urlSegment = head(this.route.snapshot.parent.url) || { path: '' };

    return urlSegment.path === 'workspace';
  }

  get thirdPartyPortalEnabled() {
    return this.appConfig.getThirdPartyPortalEnabled();
  }

  editorOptions = { language: 'yaml' };
  yaml = '';
  originalYaml = '';
  isAsfPath = true;

  @ViewChild('yamlCreateDialog') private yamlCreateDialog: TemplateRef<any>;

  constructor(
    private appConfig: AppConfigService,
    private translate: TranslateService,
    private dialog: MatDialog,
    private http: HttpClient,
    private route: ActivatedRoute,
    private toast: ToastService,
    public auth: AuthService,
    private router: Router,
  ) {}

  ngOnInit() {
    // Temporary solution to ServiceAccount menu bug in the ASF pages.
    // Remove this after latest feat/asf merged in.
    this.isAsfPath = /^\/asf\/.*$/.test(this.router.routerState.snapshot.url);
  }

  toggleLanguage() {
    this.translate.changeLanguage(this.translate.otherLang);
  }

  openYamlCreateDialog() {
    this.dialog.open(this.yamlCreateDialog, {
      width: '900px',
      disableClose: true,
      maxWidth: '95vw',
      maxHeight: '95vh',
    });
  }

  createResource() {
    let resources;
    try {
      resources = safeLoadAll(this.yaml);
    } catch (error) {
      this.toast.alertError({
        title: this.translate.get('yaml_invalid'),
      });
    }
    this.http.post('api/v1/others', resources).subscribe(
      (res: any) => {
        this.toast.messageSuccess({
          content: this.translate.get('create_resource_succ', {
            succ: res.success_resource_count,
            fail: res.failed_resource_count,
          }),
        });
      },
      (error: any) => {
        this.toast.alertError({
          title: this.translate.get('create_resource_fail'),
          content: error.error || error.message,
        });
      },
    );
  }
}
