import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import {
  ButtonModule,
  CodeEditorModule,
  DropdownModule,
  IconModule,
  ToastModule,
} from 'alauda-ui';

import { TranslateModule } from '../../../translate';

import { GlobalActionsComponent } from './global-actions.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MatDialogModule,
    CodeEditorModule,
    ToastModule,
    ButtonModule,
    TranslateModule,
    IconModule,
    DropdownModule,
  ],
  declarations: [GlobalActionsComponent],
  exports: [GlobalActionsComponent],
})
export class GlobalActionsModule {}
