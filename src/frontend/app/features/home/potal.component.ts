import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectApiService } from '@app/api';
import { UiStateService } from '@app/services';
import { map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  templateUrl: 'portal.component.html',
  styleUrls: ['home.component.scss', 'portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortalComponent {
  thirdPartyData$ = this.route.data.pipe(
    map(data => data.thirdParty),
    publishReplay(1),
    refCount(),
  );

  showLoadingBar$ = this.uiState.showLoadingBar$;

  constructor(
    private route: ActivatedRoute,
    private projectApi: ProjectApiService,
    private uiState: UiStateService,
  ) {}

  fetchProjects = () =>
    this.projectApi.find({
      searchBy: 'name',
      keywords: '',
      sort: '',
      direction: '',
      pageIndex: 0,
      pageSize: 3,
    });
}
