import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { ThirdPartyConfigResolver } from './portal.resolver';
import { PortalComponent } from './potal.component';

const routes: Routes = [
  {
    path: '',
    component: PortalComponent,
    resolve: {
      thirdParty: ThirdPartyConfigResolver,
    },
  },
  {
    path: 'projects',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
