import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'alauda-ui';

import { ProjectModule } from '../../modules/project';
import { ThirdPartyModule } from '../../modules/third-party';
import { SharedModule } from '../../shared';
import { GlobalActionsModule } from '../shared/global-actions';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ThirdPartyConfigResolver } from './portal.resolver';
import { PortalComponent } from './potal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    SharedModule,
    ButtonModule,
    GlobalActionsModule,
    ProjectModule,
    ThirdPartyModule,
  ],
  declarations: [HomeComponent, PortalComponent],
  providers: [ThirdPartyConfigResolver],
})
export class HomeModule {}
