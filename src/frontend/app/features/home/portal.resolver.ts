import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { ThirdPartyContentFindResult } from '@app/api';
import { AppConfigService } from '@app/services';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ThirdPartyConfigResolver implements Resolve<any> {
  constructor(private router: Router, private appConfig: AppConfigService) {}

  resolve(): Observable<ThirdPartyContentFindResult> {
    return this.appConfig.getThirdPartyConfig().pipe(
      tap(result => {
        if (!result.error && result.data.length === 0) {
          this.router.navigate(['/home/projects']);
        }
      }),
    );
  }
}
