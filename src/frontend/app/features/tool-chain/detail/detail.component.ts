import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToolChainApiService } from '@app/api/tool-chain/tool-chain-api.service';
import { ToolKind } from '@app/api/tool-chain/utils';
import { UpdateToolComponent } from '@app/modules/tool-chain/components/update-tool/update-tool.component';
import { TranslateService } from '@app/translate';
import {
  ConfirmType,
  DialogService,
  MessageService,
  NotificationService,
} from 'alauda-ui';
import { EMPTY, Subject, combineLatest } from 'rxjs';
import {
  catchError,
  first,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

@Component({
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailPageComponent {
  detail$ = this.activatedRoute.paramMap.pipe(
    switchMap(params =>
      this.toolChainApi.getToolService(
        params.get('kind') as ToolKind,
        params.get('name'),
      ),
    ),
    catchError(error => {
      this.notifaction.error({
        title: this.translate.get('tool_chain.not_exist', {
          name: this.activatedRoute.snapshot.paramMap.get('name'),
        }),
        content: error.error || error.message,
      });
      this.router.navigateByUrl('/admin/tool-chain');
      return EMPTY;
    }),
    publishReplay(1),
    refCount(),
  );
  bindings$ = this.activatedRoute.paramMap.pipe(
    switchMap(params =>
      this.toolChainApi.getBindingsByToolKind(
        params.get('kind') as ToolKind,
        params.get('name'),
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  private update$ = new Subject<{ [key: string]: string }>();

  info$ = combineLatest(this.detail$, this.update$.pipe(startWith({}))).pipe(
    map(([detail, update]) => ({ ...detail, ...update })),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private toolChainApi: ToolChainApiService,
    private dialog: DialogService,
    private translate: TranslateService,
    private router: Router,
    private message: MessageService,
    private notifaction: NotificationService,
  ) {}

  update() {
    this.info$.pipe(first()).subscribe(data => {
      const dialogRef = this.dialog.open(UpdateToolComponent, { data });
      dialogRef.afterClosed().subscribe(update => {
        if (update) {
          this.update$.next(update);
        }
      });
    });
  }

  delete() {
    combineLatest(this.info$, this.bindings$)
      .pipe(first())
      .subscribe(([info, bindings]) => {
        if (bindings.length) {
          this.dialog.confirm({
            title: this.translate.get('tool_chain.has_binding_alert'),
            confirmText: this.translate.get('i_know'),
            cancelButton: false,
          });
        } else {
          this.dialog
            .confirm({
              title: this.translate.get('tool_chain.delete_confirm', {
                name: info.name,
              }),
              confirmText: this.translate.get('delete'),
              confirmType: ConfirmType.Danger,
              cancelText: this.translate.get('cancel'),
              beforeConfirm: (resolve, reject) => {
                this.toolChainApi
                  .deleteTool(info.kind, info.name)
                  .subscribe(resolve, (error: any) => {
                    this.notifaction.error({
                      title: this.translate.get('tool_chain.delete_failed'),
                      content: error.error || error.message,
                    });
                    reject();
                  });
              },
            })
            .then(() => {
              this.message.success(
                this.translate.get('tool_chain.delete_successful'),
              );
              this.router.navigateByUrl('/admin/tool-chain');
            })
            .catch(() => {});
        }
      });
  }
}
