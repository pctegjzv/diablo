import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToolChainApiService } from '@app/api/tool-chain/tool-chain-api.service';
import { ToolService } from '@app/api/tool-chain/tool-chain-api.types';
import { IntegrateToolComponent } from '@app/modules/tool-chain/components/integrate-tool/integrate-tool.component';
import { DialogService, DialogSize } from 'alauda-ui';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map, publishReplay, refCount, take } from 'rxjs/operators';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent {
  selectedType = 'all';

  selectedType$$ = new BehaviorSubject<string>(this.selectedType);
  toolTypes$ = this.toolChainApi.getToolChains().pipe(
    map(types => types.filter(type => type.enabled)),
    publishReplay(1),
    refCount(),
  );
  allServices$ = this.toolChainApi.getToolServices().pipe(
    publishReplay(1),
    refCount(),
  );

  filteredServices$ = combineLatest(
    this.allServices$,
    this.selectedType$$,
  ).pipe(
    map(([tools, selectedType]) =>
      tools.filter(
        tool => selectedType === 'all' || tool.toolType === selectedType,
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  hasServices$ = this.allServices$.pipe(
    map(ins => ins && ins.length),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private toolChainApi: ToolChainApiService,
    private dialog: DialogService,
    private router: Router,
  ) {}

  integrateTool() {
    combineLatest(
      this.toolTypes$,
      this.allServices$.pipe(
        map(items => items.filter(item => item.public).map(item => item.type)),
      ),
    )
      .pipe(
        take(1),
        map(([allTypes, excludeTypes]) =>
          allTypes.map(toolType => ({
            ...toolType,
            items: toolType.items.filter(
              tool => !tool.public || !excludeTypes.includes(tool.type),
            ),
          })),
        ),
      )
      .subscribe(toolTypes => {
        const dialogRef = this.dialog.open(IntegrateToolComponent, {
          data: toolTypes,
          size: DialogSize.Large,
        });
        dialogRef.afterClosed().subscribe(data => {
          if (data) {
            this.router.navigate(['/admin/tool-chain', data.kind, data.name]);
          }
        });
      });
  }

  navigateToDetail(ins: ToolService) {
    this.router.navigate(['/admin/tool-chain', ins.kind, ins.name]);
  }
}
