import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ApplicationIdentity } from '@app/api';
import { ToastService } from 'alauda-ui';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, shareReplay } from 'rxjs/operators';

import { ApplicationDeleteDialogComponent } from '../../../modules/application';
import { TranslateService } from '../../../translate';

@Component({
  templateUrl: 'application-create.component.html',
  styleUrls: ['application-create.component.scss'],
})
export class ApplicationCreateComponent {
  project$ = selectParam(this.route.parent.parent.paramMap, 'project');
  method$ = selectParam(this.route.queryParamMap, 'method', 'yaml');
  template$ = selectParam(this.route.queryParamMap, 'template');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastService,
    private translate: TranslateService,
    private dialog: MatDialog,
  ) {}

  onCreated(name: string) {
    this.toast.messageSuccess({
      content: this.translate.get(
        'application.application_name_create_success',
        { name: name },
      ),
    });
    this.router.navigate(['../', name], {
      relativeTo: this.route,
    });
  }

  onCanceled() {
    this.router.navigate(['../'], {
      relativeTo: this.route,
    });
  }

  onDetail(applicationName: string) {
    this.router.navigate(['../', applicationName], {
      relativeTo: this.route,
    });
  }

  onDelete(applicationIdentity: ApplicationIdentity) {
    const dialogRef = this.dialog.open(ApplicationDeleteDialogComponent, {
      width: '600px',
      data: {
        params: applicationIdentity,
      },
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.toast.messageSuccess({
          content: this.translate.get(
            'application.application_name_delete_success',
            {
              name: applicationIdentity.name,
            },
          ),
        });
        this.onCanceled();
      }
    });
  }
}

function selectParam(
  paramMap$: Observable<ParamMap>,
  name: string,
  defaultValue: string = '',
) {
  return paramMap$.pipe(
    map(paramMap => paramMap.get(name) || defaultValue),
    distinctUntilChanged(),
    shareReplay(1),
  );
}
