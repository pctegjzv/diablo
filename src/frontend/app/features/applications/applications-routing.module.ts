import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ApplicationCreateComponent } from './create/application-create.component';
import { ContainerUpdatePageComponent } from './detail-page/container-update-page/container-update-page.component';
import { ApplicationDetailPageComponent } from './detail-page/detail-page.component';
import { ApplicationListComponent } from './list/application-list.component';
import { ResourceDetailComponent } from './resource-detail/resource-detail.component';

const routes: Routes = [
  { path: '', component: ApplicationListComponent },
  { path: 'create', component: ApplicationCreateComponent },
  { path: ':name', component: ApplicationDetailPageComponent },
  { path: ':name/:resourceName', component: ContainerUpdatePageComponent },
  { path: ':name/:kind/:resourceName', component: ResourceDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationsRoutingModule {}
