import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ApplicationModule } from '../../modules/application';
import { SharedModule } from '../../shared';
import { TranslateService } from '../../translate';
import { CreateApplicationModule } from '../shared/create-application';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationCreateComponent } from './create/application-create.component';
import { ContainerUpdatePageComponent } from './detail-page/container-update-page/container-update-page.component';
import { ApplicationDetailPageComponent } from './detail-page/detail-page.component';
import i18n from './i18n';
import { ApplicationListComponent } from './list/application-list.component';
import { ResourceDetailComponent } from './resource-detail/resource-detail.component';

@NgModule({
  declarations: [
    ApplicationListComponent,
    ApplicationCreateComponent,
    ApplicationDetailPageComponent,
    ContainerUpdatePageComponent,
    ResourceDetailComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    ApplicationModule,
    CreateApplicationModule,
    ApplicationsRoutingModule,
  ],
  providers: [],
})
export class ApplicationsModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('applications', i18n);
  }
}
