import {
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationApiService, ApplicationsFindParams } from '@app/api';
import { isEqual } from 'lodash';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

@Component({
  templateUrl: 'application-list.component.html',
  styleUrls: ['application-list.component.scss'],
})
export class ApplicationListComponent implements OnInit, OnDestroy {
  params$ = combineLatest(
    this.route.parent.parent.paramMap,
    this.route.queryParamMap,
  ).pipe(
    map(([params, queryParams]) => ({
      pageIndex: +(queryParams.get('page') || '1') - 1,
      itemsPerPage: +(queryParams.get('page_size') || '10'),
      project: params.get('project'),
      name: queryParams.get('keywords') || '',
    })),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  pageIndex$ = this.params$.pipe(
    map(params => params.pageIndex),
    publishReplay(1),
    refCount(),
  );

  itemsPerpage$ = this.params$.pipe(
    map(params => params.itemsPerPage),
    publishReplay(1),
    refCount(),
  );

  @ViewChild('createMethodDialog') createMethodDialog: TemplateRef<any>;

  useTemplate = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private api: ApplicationApiService,
  ) {}

  ngOnInit() {}

  ngOnDestroy() {
    this.dialog.closeAll();
  }

  trackByFn(index: number) {
    return index;
  }

  selectCreateMethod() {
    this.dialog.open(this.createMethodDialog, { width: '952px' });
  }

  hideCreateMethodDialog() {
    this.dialog.closeAll();
    this.useTemplate = false;
  }

  toggleTemplate() {
    this.useTemplate = !this.useTemplate;
  }

  fetchApplications = (params: ApplicationsFindParams) =>
    this.api.findApplications(params);

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords, page: 1 },
      queryParamsHandling: 'merge',
    });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    this.router.navigate([], {
      queryParams: { page: event.pageIndex + 1, page_size: event.pageSize },
      queryParamsHandling: 'merge',
    });
  }
}
