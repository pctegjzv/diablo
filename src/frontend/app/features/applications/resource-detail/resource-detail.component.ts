import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApplicationApiService, ApplicationIdentity } from '@app/api';
import { K8sResourceDetailComponent } from '@app/modules/application/components/k8s-resource-detail/k8s-resource-detail.component';
import { combineLatest } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  templateUrl: 'resource-detail.component.html',
  styleUrls: ['resource-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceDetailComponent implements OnInit {
  @ViewChild('detailRef') detailComp: K8sResourceDetailComponent;
  identity$ = combineLatest(
    this.route.parent.parent.paramMap.pipe(
      map(paramMap => paramMap.get('project')),
    ),
    this.route.paramMap,
  ).pipe(
    map(([namespace, paramMap]) => ({
      namespace,
      name: paramMap.get('name'),
      kind: paramMap.get('kind'),
      resourceName: paramMap.get('resourceName'),
    })),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private route: ActivatedRoute,
    private api: ApplicationApiService,
  ) {}

  ngOnInit() {}

  fetchResource = (identity: ApplicationIdentity) =>
    this.api.getK8sResource(
      {
        namespace: identity.namespace,
        resourceName: identity.resourceName,
      },
      identity.kind,
    );

  updateByYaml() {}
}
