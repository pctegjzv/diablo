import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDetailComponent } from '@app/modules/application/components/detail/detail.component';
import { combineLatest } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { ApplicationApiService, ApplicationIdentity } from '@app/api';

@Component({
  templateUrl: 'detail-page.component.html',
  styleUrls: ['detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailPageComponent {
  @ViewChild('detailRef') detailComp: ApplicationDetailComponent;
  identity$ = combineLatest(
    this.route.parent.parent.paramMap.pipe(
      map(paramMap => paramMap.get('project')),
    ),
    this.route.paramMap,
  ).pipe(
    map(([namespace, paramMap]) => ({
      namespace,
      name: paramMap.get('name'),
    })),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApplicationApiService,
  ) {}

  fetchApplication = (identity: ApplicationIdentity) => this.api.get(identity);

  onDeleted() {
    this.router.navigate(['../'], {
      relativeTo: this.route,
    });
  }

  updateByYaml() {
    this.detailComp.updateByYaml();
  }

  confirmDelete() {
    this.detailComp.confirmDelete();
  }
}
