import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  selector: 'alo-container-update-page',
  templateUrl: './container-update-page.component.html',
  styleUrls: ['./container-update-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerUpdatePageComponent {
  constructor(private route: ActivatedRoute) {}

  identity$ = combineLatest(
    this.route.parent.parent.paramMap.pipe(
      map(paramMap => paramMap.get('project')),
    ),
    this.route.paramMap,
  ).pipe(
    map(([namespace, paramMap]) => ({
      namespace,
      applicationName: paramMap.get('name'),
      resourceName: paramMap.get('resourceName'),
    })),
    publishReplay(1),
    refCount(),
  );
}
