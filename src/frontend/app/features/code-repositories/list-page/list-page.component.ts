import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CodeApiService } from '@app/api/code/code-api.service';
import { CodeRepositoriesFindParams } from '@app/api/code/code-api.types';
import { isEqual } from 'lodash';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeRepositoryListPageComponent {
  params$ = combineLatest(
    this.route.parent.parent.paramMap,
    this.route.queryParamMap,
  ).pipe(
    map(([params, queryParams]) => ({
      pageIndex: +(queryParams.get('page') || '1') - 1,
      itemsPerPage: +(queryParams.get('page_size') || '20'),
      project: params.get('project'),
      name: queryParams.get('keywords') || '',
    })),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  pageIndex$ = this.params$.pipe(
    map(params => params.pageIndex),
    publishReplay(1),
    refCount(),
  );

  itemsPerpage$ = this.params$.pipe(
    map(params => params.itemsPerPage),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: CodeApiService,
  ) {}

  fetchCodeRepositories = (params: CodeRepositoriesFindParams) =>
    this.api.findCodeRepositories(params);

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords, page: 1 },
      queryParamsHandling: 'merge',
    });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    this.router.navigate([], {
      queryParams: { page: event.pageIndex + 1, page_size: event.pageSize },
      queryParamsHandling: 'merge',
    });
  }
}
