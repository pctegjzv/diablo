import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared';
import { ProjectMembersModule } from '../shared/project-members';

import { MemberListComponent } from './list/member-list.component';
import { MembersRoutingModule } from './members-routing.module';

@NgModule({
  declarations: [MemberListComponent],
  imports: [SharedModule, ProjectMembersModule, MembersRoutingModule],
})
export class MembersModule {}
