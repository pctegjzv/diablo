import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  templateUrl: 'member-list.component.html',
  styleUrls: ['member-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MemberListComponent {
  project$ = this.route.parent.parent.paramMap.pipe(
    map(paramMap => paramMap.get('project')),
    shareReplay(1),
  );

  constructor(private route: ActivatedRoute) {}
}
