import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProjectJenkinsBindingCreatePageComponent } from '@app/features/projects/jenkins-binding-create-page/jenkins-binding-create-page.component';
import { RegistryBindingCreatePageComponent } from '@app/features/projects/registry-binding-create-page/registry-binding-create-page.component';
import { RegistryBindingDetailPageComponent } from '@app/features/projects/registry-binding-detail-page/registry-binding-detail-page.component';
import { TagListPageComponent } from '@app/features/projects/registry-tag-list-page/tag-list-page.component';
import { CodeModule } from '@app/modules/code';
import { ProjectModule } from '@app/modules/project';
import { RegistryCommonModule } from '@app/modules/registry/registry-common.module';
import { SharedModule } from '@app/shared';
import { TranslateService } from '@app/translate';

import { ProjectCodeBindingCreatePageComponent } from './code-binding-create-page/code-binding-create-page.component';
import { ProjectCodeBindingDetailPageComponent } from './code-binding-detail-page/code-binding-detail-page.component';
import { ProjectCreateComponent } from './create/project-create.component';
import { ProjectDetailPageComponent } from './detail-page/detail-page.component';
import i18n from './i18n';
import { ProjectListPageComponent } from './list-page/list-page.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectUpdateComponent } from './update/project-update.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    ProjectsRoutingModule,
    ProjectModule,
    CodeModule,
    RegistryCommonModule,
  ],
  declarations: [
    ProjectListPageComponent,
    ProjectCreateComponent,
    ProjectDetailPageComponent,
    ProjectUpdateComponent,
    ProjectCodeBindingDetailPageComponent,
    ProjectCodeBindingCreatePageComponent,
    ProjectJenkinsBindingCreatePageComponent,
    RegistryBindingCreatePageComponent,
    RegistryBindingDetailPageComponent,
    TagListPageComponent,
  ],
  providers: [],
  entryComponents: [ProjectUpdateComponent],
})
export class ProjectsModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('projects', i18n);
  }
}
