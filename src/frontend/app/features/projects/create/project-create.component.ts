import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';

import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_PRODUCT,
  API_GROUP_VERSION,
  PRODUCT_NAME,
} from '../../../constants';
import { SwitchableProjectsService } from '../../../services';
import { TranslateService } from '../../../translate';

interface ProjectModel {
  name: string;
  displayName: string;
  description: string;
}

@Component({
  templateUrl: 'project-create.component.html',
  styleUrls: ['project-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectCreateComponent {
  model: ProjectModel = { name: '', displayName: '', description: '' };

  @ViewChild(NgForm) ngForm: NgForm;

  get nameError(): string {
    const control = this.ngForm.controls['name'];
    if (!control) {
      return null;
    }

    if ((this.ngForm.submitted || control.dirty) && !control.valid) {
      const errorkey = Object.keys(control.errors || {}).find(
        key => control.errors[key],
      );
      return errorkey === 'pattern' ? 'projects.name_pattern' : errorkey;
    }
  }

  constructor(
    public http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private projects: SwitchableProjectsService,
  ) {}

  save() {
    if (!this.ngForm.valid) {
      return;
    }

    this.http
      .post('api/v1/others', [toResource(this.model)])
      .pipe(
        map((result: any) => result.failed_resource_count === 0),
        catchError(() => of(false)),
        withLatestFrom(
          this.translate.stream('projects.create_succ'),
          this.translate.stream('projects.create_fail'),
        ),
      )
      .subscribe(([flag, succ, fail]) => {
        if (flag) {
          this.snackBar.open(succ, null, {
            duration: 2000,
          });
          // TODO: need redirect by result
          this.router.navigate(['../', this.model.name], {
            relativeTo: this.route,
          });
          this.projects.reload();
        } else {
          this.snackBar.open(fail, null, {
            duration: 2000,
          });
        }
      });
  }

  cancel() {
    this.router.navigate(['../'], {
      relativeTo: this.route,
    });
  }
}

function toResource({ name, displayName, description }: ProjectModel) {
  return {
    apiVersion: API_GROUP_VERSION,
    kind: 'Project',
    metadata: {
      name,
      annotations: {
        [ANNOTATION_DISPLAY_NAME]: displayName,
        [ANNOTATION_DESCRIPTION]: description,
        [ANNOTATION_PRODUCT]: PRODUCT_NAME,
      },
    },
  };
}
