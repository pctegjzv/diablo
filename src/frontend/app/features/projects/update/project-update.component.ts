import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { Subject, of } from 'rxjs';
import {
  catchError,
  map,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  API_GROUP_VERSION,
} from '../../../constants';
import { TranslateService } from '../../../translate';

interface ProjectModel {
  name: string;
  displayName: string;
  description: string;
}

@Component({
  templateUrl: 'project-update.component.html',
  styleUrls: ['project-update.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectUpdateComponent implements OnInit, OnDestroy {
  model: ProjectModel = { name: '', displayName: '', description: '' };
  destroy$ = new Subject<void>();
  private original: any;

  @ViewChild(NgForm) ngForm: NgForm;

  get nameError(): string {
    const control = this.ngForm.controls['name'];
    if (!control) {
      return null;
    }

    if ((this.ngForm.submitted || control.dirty) && !control.valid) {
      const errorkey = Object.keys(control.errors || {}).find(
        key => control.errors[key],
      );
      return errorkey === 'pattern' ? 'projects.name_pattern' : errorkey;
    }
  }

  constructor(
    public http: HttpClient,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private dialogRef: MatDialogRef<ProjectUpdateComponent>,
    private cdr: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) {}

  ngOnInit() {
    this.http
      .get(`api/v1/others/${API_GROUP_VERSION}/Project/_/${this.data.name}`)
      .pipe(
        takeUntil(this.destroy$),
        tap((data: any) => {
          this.model = {
            name: data.objectMeta.name,
            displayName: (data.objectMeta.annotations || {})[
              ANNOTATION_DISPLAY_NAME
            ],
            description: (data.objectMeta.annotations || {})[
              ANNOTATION_DESCRIPTION
            ],
          };
          this.original = data;
          this.cdr.detectChanges();
        }),
        catchError(() => null),
        tap(data => {
          if (!data) {
            this.dialogRef.close();
          }
        }),
      )
      .subscribe(() => {});
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  save() {
    if (!this.ngForm.valid) {
      return;
    }

    this.http
      .put(
        `api/v1/others/${API_GROUP_VERSION}/Project/_/${this.data.name}`,
        patch(this.original, this.model),
      )
      .pipe(
        map(() => true),
        catchError(() => of(false)),
        withLatestFrom(
          this.translate.stream('projects.update_succ'),
          this.translate.stream('projects.update_fail'),
        ),
      )
      .subscribe(([flag, succ, fail]) => {
        if (flag) {
          this.snackBar.open(succ, null, {
            duration: 2000,
          });
          // TODO: need redirect by result
          this.dialogRef.close(true);
        } else {
          this.snackBar.open(fail, null, {
            duration: 2000,
          });
        }
      });
  }

  cancel() {
    this.dialogRef.close();
  }
}

function patch(original: any, model: ProjectModel) {
  return {
    ...original.data,
    metadata: {
      ...original.data.metadata,
      annotations: {
        ...original.data.metadata.annotations,
        [ANNOTATION_DISPLAY_NAME]: model.displayName,
        [ANNOTATION_DESCRIPTION]: model.description,
      },
    },
  };
}
