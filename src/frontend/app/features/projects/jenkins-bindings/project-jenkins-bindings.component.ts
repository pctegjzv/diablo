import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { get } from 'lodash';
import { Subject, Subscription, merge, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  scan,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';

import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
} from '../../../constants';
import { filterBy, getQuery, sortBy } from '../../../utils/query-builder';

import {
  ActionTypes,
  Actions,
  FetchError,
  FetchReceived,
  InputChange,
  ProjectChange,
  Search,
  SortChange,
  initialState,
  reducer,
} from './project-jenkins-bindings.state';

@Component({
  selector: 'alo-project-jenkins-bindings',
  templateUrl: 'project-jenkins-bindings.component.html',
  styleUrls: ['project-jenkins-bindings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectJenkinsBindingsComponent
  implements OnInit, OnChanges, OnDestroy {
  private subscriptions: Subscription[] = [];
  private actions$ = new Subject<Actions>();

  private state$ = this.actions$.pipe(
    scan(reducer, initialState),
    publishReplay(1),
    refCount(),
  );

  public list$ = this.state$.pipe(
    map(state => state.list),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
    map(list => list.map(toModel)),
  );

  public loading$ = this.state$.pipe(
    map(state => state.loading),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  public inputValue$ = this.state$.pipe(
    map(state => state.inputValue),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  public sort$ = this.state$.pipe(
    map(state => state.sort),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  public desc$ = this.state$.pipe(
    map(state => state.desc),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  private projectChange$ = this.actions$.pipe(
    filter<ProjectChange>(action => action.type === ActionTypes.ProjectChange),
    map(() => getQuery()),
  );

  private search$ = this.actions$.pipe(
    filter<Search>(action => action.type === ActionTypes.Search),
    withLatestFrom(this.state$),
    map(([, state]) =>
      getQuery(
        filterBy('name', state.inputValue),
        filterBy('displayName', state.inputValue),
      ),
    ),
  );

  private sortChange$ = this.actions$.pipe(
    filter<SortChange>(action => action.type === ActionTypes.SortChange),
    withLatestFrom(this.state$),
    map(([action, state]) =>
      getQuery(
        filterBy('name', state.keywords),
        filterBy('displayName', state.keywords),
        sortBy(action.sort, action.desc),
      ),
    ),
  );

  private inputEffect$ = this.actions$.pipe(
    filter<InputChange>(action => action.type === ActionTypes.InputChange),
    debounceTime(500),
    map(() => new Search()),
  );

  private fetchEffect$ = merge(
    this.projectChange$,
    this.search$,
    this.sortChange$,
  ).pipe(
    switchMap(params =>
      this.http.get(`api/v1/jenkinsbinding/${this.project}`, { params }).pipe(
        map(
          (result: any) =>
            new FetchReceived(
              result.listMeta.totalItems,
              result.jenkinsbindings,
            ),
        ),
        catchError(error => of(new FetchError(error))),
      ),
    ),
  );

  @Input() project = '';

  @ViewChild('createDialog') createDialog: TemplateRef<any>;

  constructor(private http: HttpClient, private dialog: MatDialog) {
    this.subscriptions.push(
      this.fetchEffect$.subscribe(this.actions$),
      this.inputEffect$.subscribe(this.actions$),
    );
  }

  ngOnInit() {
    if (this.project) {
      this.actions$.next(new ProjectChange(this.project));
    }
  }

  ngOnChanges({ project }: SimpleChanges) {
    if (project && !project.firstChange && project.currentValue) {
      this.actions$.next(new ProjectChange(project.currentValue));
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  inputChange(value: string) {
    this.actions$.next(new InputChange(value));
  }

  search() {
    this.actions$.next(new Search());
  }

  sortChange(event: { active: string; direction: string }) {
    this.actions$.next(
      new SortChange(event.active, event.direction === 'desc'),
    );
  }

  showCreate() {
    this.dialog.open(this.createDialog, { width: '900px' });
  }

  hideCreate() {
    this.dialog.closeAll();
  }

  jenkinsBindingCreated() {
    this.actions$.next(new Search());
    this.dialog.closeAll();
  }
}

function toModel(resource: any) {
  const name = get(resource, 'objectMeta.name') || '';
  const creationTimestamp =
    get(resource, 'objectMeta.creationTimestamp') || null;
  const annotations = get(resource, 'objectMeta.annotations') || {};
  const displayName = annotations[ANNOTATION_DISPLAY_NAME];
  const description = annotations[ANNOTATION_DESCRIPTION];
  const secret = get(resource, 'spec.account.secret.name');
  const jenkins = get(resource, 'spec.jenkins.name');

  return {
    name,
    displayName,
    creationTimestamp,
    description,
    secret,
    jenkins,
  };
}
