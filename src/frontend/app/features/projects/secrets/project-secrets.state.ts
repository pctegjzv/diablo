import { Action } from '../../../utils/state-adapters';

export interface State {
  project: string;
  list: any[];
  loading: boolean;
  inputValue: string;
  keywords: string;
  sort: string;
  desc: boolean;
  error: any;
}

export const initialState: State = {
  project: '',
  list: [],
  loading: false,
  inputValue: '',
  keywords: '',
  sort: '',
  desc: false,
  error: null,
};

export enum ActionTypes {
  ProjectChange = 'ProjectChange',
  InputChange = 'InputChange',
  Search = 'Search',
  SortChange = 'SortChange',
  FetchReceived = 'FetchSecrets,',
  FetchError = 'FetchError',
}

export class ProjectChange implements Action {
  readonly type = ActionTypes.ProjectChange;
  constructor(public project: string) {}
}

export class InputChange implements Action {
  readonly type = ActionTypes.InputChange;
  constructor(public inputValue: string) {}
}

export class Search implements Action {
  readonly type = ActionTypes.Search;
}

export class SortChange implements Action {
  readonly type = ActionTypes.SortChange;
  constructor(public sort: string, public desc = false) {}
}

export class FetchReceived implements Action {
  readonly type = ActionTypes.FetchReceived;
  constructor(public length: number, public list: any) {}
}

export class FetchError implements Action {
  readonly type = ActionTypes.FetchError;
  constructor(public error: any) {}
}

export type Actions =
  | ProjectChange
  | InputChange
  | Search
  | SortChange
  | FetchReceived
  | FetchError;

const projectChange = (_state: State, { project }: ProjectChange): State => ({
  ...initialState,
  project,
  loading: true,
});

const inputChange = (state: State, { inputValue }: InputChange): State => ({
  ...state,
  inputValue,
});

const search = (state: State): State => ({
  ...state,
  keywords: state.inputValue,
  loading: true,
});

const sortChange = (state: State, { sort, desc }: SortChange): State => ({
  ...state,
  sort,
  desc,
  loading: true,
});

const fetchReceived = (state: State, { list }: FetchReceived): State => {
  return {
    ...state,
    list,
    loading: false,
  };
};

const fetchError = (state: State, action: FetchError): State => ({
  ...state,
  loading: false,
  error: action.error,
});

export function reducer(state: State = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.ProjectChange:
      return projectChange(state, action);
    case ActionTypes.Search:
      return search(state);
    case ActionTypes.InputChange:
      return inputChange(state, action);
    case ActionTypes.SortChange:
      return sortChange(state, action);
    case ActionTypes.FetchReceived:
      return fetchReceived(state, action);
    case ActionTypes.FetchError:
      return fetchError(state, action);
    default:
      return state;
  }
}
