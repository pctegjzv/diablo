import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BindingKind, ToolKind } from '@app/api/tool-chain/utils';
import { ProjectJenkinsBindingCreatePageComponent } from '@app/features/projects/jenkins-binding-create-page/jenkins-binding-create-page.component';
import { RegistryBindingCreatePageComponent } from '@app/features/projects/registry-binding-create-page/registry-binding-create-page.component';
import { RegistryBindingDetailPageComponent } from '@app/features/projects/registry-binding-detail-page/registry-binding-detail-page.component';
import { TagListPageComponent } from '@app/features/projects/registry-tag-list-page/tag-list-page.component';

import { ProjectCodeBindingCreatePageComponent } from './code-binding-create-page/code-binding-create-page.component';
import { ProjectCodeBindingDetailPageComponent } from './code-binding-detail-page/code-binding-detail-page.component';
import { ProjectCreateComponent } from './create/project-create.component';
import { ProjectDetailPageComponent } from './detail-page/detail-page.component';
import { ProjectListPageComponent } from './list-page/list-page.component';

const routes: Routes = [
  { path: '', component: ProjectListPageComponent },
  { path: 'create', component: ProjectCreateComponent },
  { path: ':name', component: ProjectDetailPageComponent },
  {
    path: `:name/create-binding/${ToolKind.CodeRepo}/:service`,
    component: ProjectCodeBindingCreatePageComponent,
  },
  {
    path: `:name/create-binding/${ToolKind.Jenkins}/:service`,
    component: ProjectJenkinsBindingCreatePageComponent,
  },
  {
    path: `:name/create-binding/${ToolKind.Registry}/:service`,
    component: RegistryBindingCreatePageComponent,
  },
  {
    path: `:name/${BindingKind.CodeRepo}/:codeBindingName`,
    component: ProjectCodeBindingDetailPageComponent,
  },
  {
    path: `:name/${BindingKind.Registry}/:bindingName`,
    component: RegistryBindingDetailPageComponent,
  },
  {
    path: `:name/${BindingKind.Registry}/:bindingName/:repoName`,
    component: TagListPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectsRoutingModule {}
