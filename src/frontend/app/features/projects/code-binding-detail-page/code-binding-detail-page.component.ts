import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CodeApiService } from '@app/api/code/code-api.service';
import { catchError, map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  templateUrl: 'code-binding-detail-page.component.html',
  styleUrls: ['code-binding-detail-page.component.html'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectCodeBindingDetailPageComponent {
  identity$ = this.route.paramMap.pipe(
    map(paramMap => ({
      name: paramMap.get('codeBindingName'),
      namespace: paramMap.get('name'),
    })),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private route: ActivatedRoute,
    private codeApi: CodeApiService,
    private router: Router,
  ) {}

  fetchCodeBinding = (identity: { name: string; namespace: string }) => {
    return this.codeApi.getBinding(identity.name, identity.namespace).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error && error.status === 404) {
          this.router.navigate(['../../'], {
            relativeTo: this.route,
          });
        }
        throw error;
      }),
    );
  };
}
