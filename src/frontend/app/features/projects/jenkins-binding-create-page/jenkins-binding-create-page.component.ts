import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SecretApiService, SecretTypes } from '@app/api';
import { SecretCreateDialogComponent } from '@app/modules/secret';
import { TranslateService } from '@app/translate';
import { Subject, combineLatest } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { Location } from '@angular/common';
import { JenkinsApiService } from '@app/api/jenkins/jenkins-api.service';
import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from 'alauda-ui';

@Component({
  templateUrl: 'jenkins-binding-create-page.component.html',
  styleUrls: ['jenkins-binding-create-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectJenkinsBindingCreatePageComponent {
  secretsUpdated$$ = new Subject<void>();
  project$ = this.activatedRoute.paramMap.pipe(
    map(params => params.get('name')),
    publishReplay(1),
    refCount(),
  );

  secrets$ = combineLatest(
    this.project$,
    this.secretsUpdated$$.pipe(startWith(null)),
  ).pipe(
    switchMap(([namespace]) => this.secretApi.find(null, namespace)),
    map(res =>
      res.items
        .filter(item => item.type === SecretTypes.basicAuth)
        .map(item => item.name),
    ),
    publishReplay(1),
    refCount(),
  );

  formData = {
    name: '',
    secret: '',
    description: '',
  };

  @ViewChild('form') form: NgForm;
  loading = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private secretApi: SecretApiService,
    private jenkinsApi: JenkinsApiService,
    private dialog: DialogService,
    private router: Router,
    private message: MessageService,
    private notifaction: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private location: Location,
  ) {}

  addSecret() {
    this.dialog
      .open(SecretCreateDialogComponent, {
        size: DialogSize.Large,
        data: {
          namespace: this.activatedRoute.snapshot.paramMap.get('name'),
          types: [SecretTypes.basicAuth],
        },
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.formData.secret = result.name;
          this.secretsUpdated$$.next();
        }
      });
  }

  onSubmit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const namespace = this.activatedRoute.snapshot.paramMap.get('name');
    const service = this.activatedRoute.snapshot.paramMap.get('service');
    this.jenkinsApi
      .createBinding({
        ...this.formData,
        service,
        namespace,
      })
      .subscribe(
        () => {
          this.message.success(
            this.translate.get('projects.created_successfully'),
          );
          this.router.navigate(['/admin/projects', namespace]);
        },
        error => {
          this.notifaction.error({
            title: this.translate.get('projects.creation_failed'),
            content: error.error || error.message,
          });
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.location.back();
  }
}
