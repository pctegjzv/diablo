import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegistryApiService } from '@app/api/registry/registry-api.service';
import { AssignRepositoryComponent } from '@app/modules/registry/components/assign-repository/assign-repository.component';
import { UpdateRegistryBindingComponent } from '@app/modules/registry/components/update-binding/update-binding.component';
import { TranslateService } from '@app/translate';
import {
  ConfirmType,
  DialogService,
  DialogSize,
  NotificationService,
} from 'alauda-ui';
import {
  BehaviorSubject,
  EMPTY,
  ReplaySubject,
  combineLatest,
  from,
  timer,
} from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  switchMapTo,
  take,
} from 'rxjs/operators';

@Component({
  templateUrl: 'registry-binding-detail-page.component.html',
  styleUrls: ['registry-binding-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistryBindingDetailPageComponent {
  namespace$ = this.activatedRoute.paramMap.pipe(
    map(params => params.get('name')),
  );

  bindingName$ = this.activatedRoute.paramMap.pipe(
    map(params => params.get('bindingName')),
  );

  updated$$ = new ReplaySubject<void>(1);
  repositoriesFilter$$ = new BehaviorSubject<string>('');

  binding$ = combineLatest(
    this.namespace$,
    this.bindingName$,
    this.updated$$.pipe(startWith(null)),
  ).pipe(
    switchMap(([namespace, bindingName]) =>
      this.registryApi.getBinding(namespace, bindingName),
    ),
    publishReplay(1),
    refCount(),
  );

  repositories$ = combineLatest(
    this.namespace$,
    this.bindingName$,
    this.updated$$.pipe(
      startWith(null),
      switchMapTo(timer(0, 10000)),
    ),
  ).pipe(
    switchMap(([namespace, binding]) =>
      this.registryApi.getRepositoriesByRegistryBinding(namespace, binding),
    ),
    map(list => list.items),
    publishReplay(1),
    refCount(),
  );

  filteredRepositories$ = combineLatest(
    this.repositories$,
    this.repositoriesFilter$$,
  ).pipe(
    map(([items, keyword]) =>
      items.filter(item => item.image.includes(keyword)),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private registryApi: RegistryApiService,
    private dialog: DialogService,
    private translate: TranslateService,
    private notifaction: NotificationService,
    private router: Router,
  ) {}

  assignRepository() {
    this.binding$
      .pipe(
        take(1),
        switchMap(binding =>
          this.dialog
            .open(AssignRepositoryComponent, {
              size: DialogSize.Large,
              data: { binding },
            })
            .afterClosed(),
        ),
      )
      .subscribe(result => {
        if (result) {
          this.updated$$.next();
        }
      });
  }

  updateBind() {
    this.binding$.pipe(take(1)).subscribe(binding => {
      this.dialog
        .open(UpdateRegistryBindingComponent, {
          size: DialogSize.Large,
          data: binding,
        })
        .afterClosed()
        .subscribe((updated: boolean) => {
          if (updated) {
            this.updated$$.next();
          }
        });
    });
  }

  unbind() {
    this.binding$
      .pipe(
        take(1),
        switchMap(binding => {
          if (binding.repositories.length) {
            return from(
              this.dialog.confirm({
                title: this.translate.get(
                  'projects.delete_registry_binding_hint',
                ),
                confirmText: this.translate.get('i_know'),
                cancelButton: false,
              }),
            ).pipe(switchMapTo(EMPTY));
          } else {
            return from(
              this.dialog.confirm({
                title: this.translate.get(
                  'projects.delete_registry_binding_confirm',
                  { name: binding.name },
                ),
                confirmText: this.translate.get('projects.unbind'),
                confirmType: ConfirmType.Danger,
                cancelText: this.translate.get('cancel'),
                beforeConfirm: (reslove, reject) => {
                  this.registryApi
                    .deleteBinding(binding.namespace, binding.name)
                    .subscribe(reslove, error => {
                      this.notifaction.error({
                        title: this.translate.get('projects.unbind_failed'),
                        content: error.error || error.message,
                      });
                      reject();
                    });
                },
              }),
            ).pipe(catchError(() => EMPTY));
          }
        }),
      )
      .subscribe(() => {
        this.router.navigate([
          '/admin/projects',
          this.activatedRoute.snapshot.paramMap.get('name'),
        ]);
      });
  }
}
