import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ProjectApiService } from '@app/api';
import { TranslateService } from '@app/translate';

@Component({
  templateUrl: 'detail-page.component.html',
  styleUrls: ['detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectDetailPageComponent implements OnInit {
  @ViewChild('deleteConfirmDialog')
  private deleteConfirmDialog: TemplateRef<any>;

  name: string;
  deleting: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private api: ProjectApiService,
    private dialog: MatDialog,
    private translate: TranslateService,
    private snackBar: MatSnackBar,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => (this.name = params.get('name')));
  }

  fetchProject = () =>
    this.api.get(this.name).pipe(
      catchError(() => {
        this.location.back();
        return of(false);
      }),
    );

  deleteProject() {
    this.dialog.open(this.deleteConfirmDialog, {
      width: '420px',
    });
  }

  cancel() {
    this.dialog.closeAll();
  }

  async delete() {
    this.dialog.closeAll();

    this.deleting = true;
    this.cdr.detectChanges();

    try {
      await this.api.delete(this.name).toPromise();
    } catch (e) {
      return this.snackBar.open(
        this.translate.get('projects.delete_fail'),
        null,
        {
          duration: 2000,
        },
      );
    } finally {
      this.deleting = false;
      this.cdr.detectChanges();
    }

    this.router.navigate(['/admin/projects'], {
      replaceUrl: true,
    });
  }
}
