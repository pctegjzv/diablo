import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { PipelineApiService, PipelineHistory } from '@app/api';
import { filterBy, getQuery } from '@app/utils/query-builder';
import { get, head } from 'lodash';
import { map, publishReplay, refCount, tap } from 'rxjs/operators';

@Component({
  selector: 'alo-pipeline-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryDetailComponent {
  namespace: string;
  running = true;
  params$ = this.route.paramMap.pipe(
    map((params: ParamMap) => {
      return {
        namespace: params.get('project'),
        name: params.get('name'),
        historyName: params.get('historyName'),
      };
    }),
    tap(param => {
      this.namespace = param.namespace;
    }),
    publishReplay(1),
    refCount(),
  );

  fetchData = (params: {
    namespace: string;
    name: string;
    historyName: string;
  }) =>
    this.api
      .getPipelineHistories(
        params.namespace,
        getQuery(
          filterBy('label', `pipelineConfig:${params.name}`),
          filterBy('name', params.historyName),
        ),
      )
      .pipe(
        map((result: any) => {
          return head(result.histories);
        }),
        tap((history: PipelineHistory) => {
          const currentStatus = get(history, 'status.phase', '');
          this.running = ['Queued', 'Pending', 'Running'].includes(
            currentStatus,
          );
          this.cdr.detectChanges();
        }),
      );
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: PipelineApiService,
    private cdr: ChangeDetectorRef,
  ) {}

  pipelineStatusChange(pipelineName: string) {
    this.router.navigate(['../', pipelineName], { relativeTo: this.route });
  }
}
