import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { PipelineApiService, PipelineConfig, PipelineParams } from '@app/api';
import { ModeSelectComponent } from '@app/modules/pipeline/components/mode-select/mode-select.component';
import { filterBy, getQuery, pageBy, sortBy } from '@app/utils/query-builder';
import { isEqual } from 'lodash';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  pluck,
  publishReplay,
  refCount,
  tap,
} from 'rxjs/operators';
@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent {
  project$ = this.route.paramMap.pipe(
    map(param => param.get('project')),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );
  params$ = combineLatest(this.project$, this.route.queryParamMap).pipe(
    map(([project, queryParams]) => {
      const params = getQuery(
        filterBy(
          queryParams.get('search_by') === 'display_name'
            ? 'displayName'
            : 'name',
          queryParams.get('keywords'),
        ),
        pageBy(
          +(queryParams.get('page') || '1') - 1,
          +(queryParams.get('page_size') || 20),
        ),
        sortBy(
          queryParams.get('sort'),
          queryParams.get('direction') === 'desc',
        ),
      );
      return [project, params];
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  pageIndex$ = this.params$.pipe(
    pluck('1', 'page'),
    publishReplay(1),
    refCount(),
  );

  itemsPerPage$ = this.params$.pipe(
    pluck('1', 'itemsPerPage'),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.route.queryParamMap.pipe(
    tap(params => {
      this.searchBy =
        params.get('search_by') === 'display_name' ? 'display_name' : 'name';
      this.cdr.markForCheck();
    }),
    map(params => params.get('keywords')),
    publishReplay(1),
    refCount(),
  );

  searchBy = 'name';

  findPipelines = ([project, params]: [string, PipelineParams]) =>
    this.pipelineApi.findPipelineConfigs(project, params);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private pipelineApi: PipelineApiService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
  ) {}

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords, page: 1, search_by: this.searchBy },
      queryParamsHandling: 'merge',
    });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    this.router.navigate([], {
      queryParams: { page: event.pageIndex + 1, page_size: event.pageSize },
      queryParamsHandling: 'merge',
    });
  }

  sortByChanged(event: { active: string; direction: string }) {
    this.router.navigate(['./'], {
      relativeTo: this.route,
      queryParams: {
        sort: event.active,
        direction: event.direction,
      },
    });
  }

  onPipelineStarted(pipeline: PipelineConfig) {
    this.router.navigate(['./', pipeline.name], { relativeTo: this.route });
  }

  modeSelect() {
    this.dialog.open(ModeSelectComponent, {
      width: '600px',
      data: {
        path: this.router.url.split('?')[0],
      },
    });
  }
}
