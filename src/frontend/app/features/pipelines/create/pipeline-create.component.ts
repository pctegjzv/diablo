import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { isEqual } from 'lodash';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

@Component({
  selector: 'alo-pipeline-create',
  templateUrl: './pipeline-create.component.html',
  styleUrls: ['./pipeline-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineCreateComponent {
  params$ = combineLatest(this.route.paramMap, this.route.queryParamMap).pipe(
    map(([params, queryParams]: ParamMap[]) => {
      return {
        namespace: params.get('project'),
        method: params.get('method') || 'jenkinsfile',
        type: queryParams.get('type') || '',
        name: queryParams.get('name') || '',
      };
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );
  constructor(private route: ActivatedRoute) {}
}
