import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PipelineApiService } from '@app/api';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'alo-pipeline-update',
  templateUrl: './pipeline-update.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineUpdateComponent {
  namespace: string;
  name: string;
  param$ = this.route.paramMap.pipe(
    map((params: ParamMap) => {
      return {
        namespace: params.get('project'),
        name: params.get('name') || '',
        method: params.get('method') || 'jenkinsfile',
      };
    }),
    tap(params => {
      this.namespace = params.namespace;
      this.name = params.name;
    }),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  @HostBinding('class.pipeline-update')
  get cls() {
    return true;
  }

  constructor(
    private route: ActivatedRoute,
    private pipelineApi: PipelineApiService,
  ) {}

  getPipelineConfig = () =>
    this.pipelineApi.getPipelineConfigToModel(this.namespace, this.name);
}
