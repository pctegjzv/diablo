import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PipelineHistoryDetailComponent } from '@app/features/pipelines/history-detail/history-detail.component';

import { PipelineCreateComponent } from './create/pipeline-create.component';
import { PipelineDetailComponent } from './detail/pipeline-detail.component';
import { ListPageComponent } from './list/list-page.component';
import { PipelineUpdateComponent } from './update/pipeline-update.component';

const routes: Routes = [
  { path: '', component: ListPageComponent },
  { path: 'create', component: PipelineCreateComponent },
  { path: ':name/update', component: PipelineUpdateComponent },
  { path: ':name', component: PipelineDetailComponent },
  {
    path: ':name/:historyName',
    component: PipelineHistoryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PipelinesRoutingModule {}
