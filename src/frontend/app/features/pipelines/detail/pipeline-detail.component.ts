import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';

import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { PipelineApiService, PipelineConfig } from '@app/api';
import { PipelineHistoriesComponent } from '@app/modules/pipeline/components/histories/histories.component';
import { TranslateService } from '@app/translate';
import { ConfirmType, DialogService, ToastService } from 'alauda-ui';
import { get } from 'lodash';
import { publishReplay, refCount, switchMap, tap } from 'rxjs/operators';
@Component({
  templateUrl: 'pipeline-detail.component.html',
  styleUrls: ['pipeline-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineDetailComponent {
  activeTab: 'base' | 'jenkinsfile' = 'base';
  orignalJenkinsfile: string;
  name: string;
  namespace: string;

  pipeline$ = this.route.paramMap.pipe(
    tap(param => {
      this.name = param.get('name');
      this.namespace = param.get('project');
    }),
    switchMap(param =>
      this.pipelineApi.getPipelineConfig(
        param.get('project'),
        param.get('name'),
      ),
    ),
    tap((pipeline: PipelineConfig) => {
      this.orignalJenkinsfile = get(
        pipeline,
        'strategy.jenkins.jenkinsfile',
        '',
      );
    }),
    publishReplay(1),
    refCount(),
  );

  @ViewChild(PipelineHistoriesComponent) histories: PipelineHistoriesComponent;

  constructor(
    private pipelineApi: PipelineApiService,
    private route: ActivatedRoute,
    private location: Location,
    private toast: ToastService,
    private translate: TranslateService,
    private router: Router,
    private auiDialog: DialogService,
  ) {}

  triggerPipeline() {
    this.pipelineApi.triggerPipeline(this.namespace, this.name).subscribe(
      () => {
        this.toast.messageSuccess({
          content: this.translate.get('pipeline_start_succ'),
        });
        this.histories.historyRefresh(true);
      },
      (err: any) => {
        this.toast.alertError({
          title: this.translate.get('pipeline_start_fail'),
          content: err.error || err.message,
        });
      },
    );
  }

  deletePipeline() {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline.delete_pipelineconfig_confirm', {
          name: this.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline.sure'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.pipelineApi
            .deletePipelineConfig(this.namespace, this.name)
            .subscribe(
              () => {
                this.toast.messageSuccess({
                  content: this.translate.get(
                    'pipeline.pipelineconfig_delete_succ',
                    { name: this.name },
                  ),
                });
                resolve();
                this.location.back();
              },
              (err: any) => {
                this.toast.alertError({
                  title: this.translate.get(
                    'pipeline.pipelineconfig_delete_failed',
                    { name: this.name },
                  ),
                  content: err.error || err.message,
                });
                reject();
              },
            );
        },
      })
      .then(() => {})
      .catch(() => {});
  }

  updatePipeline() {
    this.router.navigate(['./', 'update'], { relativeTo: this.route });
  }

  copyPipeline() {
    this.router.navigate(['../', 'create'], {
      relativeTo: this.route,
      queryParams: {
        type: 'copy',
        name: this.name,
      },
    });
  }

  changeTab(tabName: 'base' | 'jenkinsfile') {
    this.activeTab = tabName;
  }
}
