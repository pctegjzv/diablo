import { NgModule } from '@angular/core';

import { ForAdminRoutingModule } from './for-admin-routing.module';
import { FOR_ADMIN, MODULE_ENV } from './module-env';
import { SecretsModule } from './secrets.module';

@NgModule({
  imports: [SecretsModule, ForAdminRoutingModule],
  providers: [{ provide: MODULE_ENV, useValue: FOR_ADMIN }],
})
export class ForAdminModule {}
