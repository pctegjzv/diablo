import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SecretModule } from '@app/modules/secret';

import { SharedModule } from '../../shared';
import { TranslateService } from '../../translate';

import { SecretCreateComponent } from './create/secret-create.component';
import { SecretDetailComponent } from './detail/secret-detail.component';
import i18n from './i18n';
import { SecretListComponent } from './list/secret-list.component';

@NgModule({
  declarations: [
    SecretListComponent,
    SecretDetailComponent,
    SecretCreateComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    SecretModule,
  ],
  providers: [],
})
export class SecretsModule {
  constructor(translate: TranslateService) {
    translate.setTranslations('secrets', i18n);
  }
}
