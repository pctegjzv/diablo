import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash';
import { Subscription, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { ANNOTATION_DISPLAY_NAME } from '../../../constants';
import { shallowEqual } from '../../../utils/shallow-equal';
import { MODULE_ENV, ModuleEnv } from '../module-env';

import {
  ActionTypes,
  Edit,
  SecretListStore,
  SortChange,
  selectCurrentPage,
} from './secret-list.store';

@Component({
  templateUrl: 'secret-list.component.html',
  styleUrls: ['secret-list.component.scss'],
  providers: [SecretListStore],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretListComponent implements OnDestroy {
  private subscriptions: Subscription[] = [];
  private params$ = getParams(this.route, this.env);

  inputValue$ = this.store.select(state => state.inputValue);
  secrets$ = this.store
    .select(selectCurrentPage)
    .pipe(map(items => (items || []).map(secretToModel)));
  active$ = this.store.select(state => state.sort);
  direction$ = this.store.select(state => (state.desc ? 'desc' : 'asc'));
  editing$ = this.store.select(state => state.editing, shallowEqual);

  @ViewChild('updateDialog') updateDialog: TemplateRef<any>;
  columns =
    this.env === 'admin'
      ? ['name', 'project', 'type', 'creationTimestamp', 'actions']
      : ['name', 'type', 'creationTimestamp', 'actions'];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    public store: SecretListStore,
    @Inject(MODULE_ENV) public env: ModuleEnv,
  ) {
    this.store.on(ActionTypes.Search, (_, state) => {
      this.router.navigate([], {
        queryParamsHandling: 'merge',
        queryParams: {
          keywords: state.inputValue,
        },
      });
    });
    this.store.on(ActionTypes.SortChange, (action: SortChange) => {
      this.router.navigate([], {
        queryParamsHandling: 'merge',
        queryParams: {
          sort: action.sort,
          direction: action.desc ? 'desc' : 'asc',
        },
      });
    });
    this.store.on(ActionTypes.Edit, (_action: Edit) => {
      this.dialog.open(this.updateDialog, { width: '900px' });
    });
    this.subscriptions.push(
      this.params$.subscribe(params => this.store.paramsChange(params)),
    );
  }

  secretIdentity(_: number, secret: any) {
    return `${secret.namespace},${secret.name}`;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  closeDialog() {
    this.dialog.closeAll();
  }

  sortChange(event: any) {
    this.store.sortChange(event.active, event.direction === 'desc');
  }

  updated() {
    this.store.refetch();
    this.dialog.closeAll();
  }
}

function getParams(route: ActivatedRoute, env: ModuleEnv) {
  if (env === 'admin') {
    return route.queryParamMap.pipe(
      map(paramMap => ({
        project: null,
        keywords: paramMap.get('keywords') || '',
        sort: paramMap.get('sort') || '',
        desc: paramMap.get('direction') === 'desc',
        pageIndex: (+paramMap.get('page') || 1) - 1,
        pageSize: +paramMap.get('page_size') || 20,
      })),
    );
  } else {
    return combineLatest(
      route.queryParamMap.pipe(
        map(paramMap => ({
          keywords: paramMap.get('keywords') || '',
          sort: paramMap.get('sort') || '',
          desc: paramMap.get('direction') === 'desc',
          pageIndex: (+paramMap.get('page') || 1) - 1,
          pageSize: +paramMap.get('page_size') || 20,
        })),
      ),
      route.parent.parent.paramMap,
    ).pipe(
      map(([params, paramMap]) => ({
        project: paramMap.get('project'),
        ...params,
      })),
    );
  }
}

function secretToModel(resource: any) {
  const name = get(resource, 'objectMeta.name');
  const namespace = get(resource, 'objectMeta.namespace');
  const annotations = get(resource, 'objectMeta.annotations') || {};
  const displayName = annotations[ANNOTATION_DISPLAY_NAME] || '';
  const creationTimestamp = get(resource, 'objectMeta.creationTimestamp');

  return {
    name,
    displayName: displayName || name,
    namespace,
    type: resource.type,
    creationTimestamp,
  };
}
