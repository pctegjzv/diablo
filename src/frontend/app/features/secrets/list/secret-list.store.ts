import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Subscription, merge, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  scan,
  shareReplay,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';

import { ANNOTATION_PRODUCT } from '../../../constants';
import { filterBy, getQuery, sortBy } from '../../../utils/query-builder';
import {
  Action,
  PaginatedListState,
  createPaginatedListAdapter,
  selectors,
} from '../../../utils/state-adapters';

export interface State extends PaginatedListState<any> {
  inputValue: string;
  project: string;
  keywords: string;
  sort: string;
  desc: boolean;
  loading: boolean;
  error: any;
  sketch?: any[];
  editing: any;
}

export interface Params {
  project: string;
  keywords: string;
  sort: string;
  desc: boolean;
  pageIndex: number;
  pageSize: number;
}

const pipelineListAdapter = createPaginatedListAdapter<any>();

export const initialState: State = pipelineListAdapter.getInitialState({
  project: '',
  inputValue: '',
  keywords: '',
  loading: false,
  sort: '',
  desc: false,
  error: null,
  sketch: [],
  editing: null,
});

export enum ActionTypes {
  ParamsChange = 'ParamsChange',
  InputChange = 'InputChange',
  Search = 'Search',
  Refetch = 'Refetch',
  SortChange = 'SortChange',
  PageChange = 'PageChange',
  FetchReceived = 'FetchReceived',
  FetchError = 'FetchError',
  Edit = 'Edit',
}

export class ParamsChange implements Action {
  readonly type = ActionTypes.ParamsChange;
  constructor(public params: Params) {}
}

export class InputChange implements Action {
  readonly type = ActionTypes.InputChange;
  constructor(public inputValue: string) {}
}

export class Search implements Action {
  readonly type = ActionTypes.Search;
  constructor() {}
}

export class Refetch implements Action {
  readonly type = ActionTypes.Refetch;
  constructor() {}
}

export class SortChange implements Action {
  readonly type = ActionTypes.SortChange;
  constructor(public sort: string, public desc = false) {}
}

export class PageChange implements Action {
  readonly type = ActionTypes.PageChange;
  constructor(public pageIndex: number, public pageSize: number) {}
}

export class FetchReceived implements Action {
  readonly type = ActionTypes.FetchReceived;
  constructor(public length: number, public list: any) {}
}

export class FetchError implements Action {
  readonly type = ActionTypes.FetchError;
  constructor(public error: any) {}
}

export class Edit implements Action {
  readonly type = ActionTypes.Edit;
  constructor(public item: any) {}
}

export type Actions =
  | ParamsChange
  | InputChange
  | Search
  | Refetch
  | SortChange
  | PageChange
  | FetchReceived
  | FetchError
  | Edit;

const paramsChange = (state: State, { params }: ParamsChange): State => ({
  ...initialState,
  ...params,
  inputValue: params.keywords,
  loading: true,
  sketch: selectors.selectCurrentPage(state),
});

const inputChange = (state: State, { inputValue }: InputChange): State => ({
  ...state,
  inputValue,
});

const search = (state: State): State => ({
  ...state,
  keywords: state.inputValue,
});

const refetch = (state: State): State => ({
  ...state,
  inputValue: state.keywords,
  loading: true,
  sketch: selectors.selectCurrentPage(state),
});

const sortChange = (state: State, { sort, desc }: SortChange): State => ({
  ...state,
  sort,
  desc,
});

const pageChange = (
  state: State,
  { pageIndex, pageSize }: PageChange,
): State => {
  return {
    ...state,
    pageIndex,
    pageSize,
  };
};

const fetchReceived = (
  state: State,
  { type: _type, ...rest }: FetchReceived,
): State => {
  return {
    ...pipelineListAdapter.patch<State>(state, rest),
    loading: false,
    sketch: [],
  };
};

const fetchError = (state: State, action: FetchError): State => ({
  ...state,
  loading: false,
  error: action.error,
  sketch: [],
});

const edit = (state: State, action: Edit): State => ({
  ...state,
  editing: action.item,
});

export function reducer(state: State = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.ParamsChange:
      return paramsChange(state, action);
    case ActionTypes.InputChange:
      return inputChange(state, action);
    case ActionTypes.Search:
      return search(state);
    case ActionTypes.Refetch:
      return refetch(state);
    case ActionTypes.SortChange:
      return sortChange(state, action);
    case ActionTypes.PageChange:
      return pageChange(state, action);
    case ActionTypes.FetchReceived:
      return fetchReceived(state, action);
    case ActionTypes.FetchError:
      return fetchError(state, action);
    case ActionTypes.Edit:
      return edit(state, action);
    default:
      return state;
  }
}

@Injectable()
export class SecretListStore {
  private subscriptions: Subscription[] = [];
  private actions$ = new Subject<Actions>();
  private state$ = this.actions$.pipe(
    scan(reducer, initialState),
    shareReplay(1),
  );

  private paramsChange$ = this.actions$.pipe(
    filter<ParamsChange>(action => action.type === ActionTypes.ParamsChange),
    map(
      ({ params: { project, ...params } }): [string, any] => [
        project,
        getQuery(
          filterBy('name', params.keywords),
          sortBy(params.sort, params.desc),
          // pageBy(fromRoute.pageIndex, fromRoute.pageSize),
        ),
      ],
    ),
  );

  private refetch$ = this.actions$.pipe(
    filter<Refetch>(action => action.type === ActionTypes.Refetch),
    withLatestFrom(this.state$),
    map(
      ([, state]): [string, any] => [
        state.project,
        getQuery(
          filterBy('name', state.keywords),
          sortBy(state.sort, state.desc),
          // pageBy(state.pageIndex, state.pageSize),
        ),
      ],
    ),
  );

  private fetchEffect$ = merge(this.paramsChange$, this.refetch$).pipe(
    switchMap(([project, params]) =>
      this.http
        .get(project ? `api/v1/secret/${project}` : 'api/v1/secret', { params })
        .pipe(
          map(
            (result: any) =>
              new FetchReceived(
                result.listMeta.totalItems,
                result.secrets.filter(
                  (item: any) =>
                    item.objectMeta &&
                    (item.objectMeta.annotations || {})[ANNOTATION_PRODUCT] &&
                    item.objectMeta.namespace !== 'default',
                ),
              ),
          ),
          catchError(error => of(new FetchError(error))),
        ),
    ),
  );

  private inputEffect$ = this.actions$.pipe(
    filter<InputChange>(action => action.type === ActionTypes.InputChange),
    debounceTime(500),
    map(() => new Search()),
  );

  constructor(private http: HttpClient) {
    this.subscriptions.push(
      this.fetchEffect$.subscribe(this.actions$),
      this.inputEffect$.subscribe(this.actions$),
    );
  }

  destroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  select<T>(
    fn: (state: State) => T,
    comparator: (a: T, b: T) => boolean = (a, b) => a === b,
  ) {
    return this.state$.pipe(
      map(fn),
      distinctUntilChanged(comparator),
      shareReplay(1),
    );
  }

  on<TAction extends Actions>(
    type: ActionTypes,
    fn: ((action: TAction, state: State) => void) = () => {},
  ) {
    this.subscriptions.push(
      this.actions$
        .pipe(
          filter<TAction>(action => action.type === type),
          withLatestFrom(this.state$),
        )
        .subscribe(([action, store]) => fn(action, store)),
    );
  }

  paramsChange(params: Params) {
    this.actions$.next(new ParamsChange(params));
  }

  inputChange(inputValue: string) {
    this.actions$.next(new InputChange(inputValue));
  }

  sortChange(sort: string, desc: boolean) {
    this.actions$.next(new SortChange(sort, desc));
  }

  pageChange(pageIndex: number, pageSize: number) {
    this.actions$.next(new PageChange(pageIndex, pageSize));
  }

  search() {
    this.actions$.next(new Search());
  }

  refetch() {
    this.actions$.next(new Refetch());
  }

  edit(item: any) {
    this.actions$.next(new Edit(item));
  }
}

export const selectCurrentPage = (state: State) => {
  return selectors.selectCurrentPage(state) || state.sketch;
};
