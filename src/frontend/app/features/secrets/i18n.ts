export const en = {
  create: 'Create Secret',
  belongs_project: 'Belongs Project',
  type: 'Type',
  name: 'Secret Name',
  detail: 'Secret',
  username_password: 'Username/Passowrd',
  docker_registry: 'Docker Registry',
  opaque: 'Opaque',
  docker_address: 'Docker Service Address',
  docker_username: 'Username',
  docker_password: 'Password',
  docker_email: 'Email',
  list: 'Secrets',
};

export const zh = {
  create: '创建 Secret',
  belongs_project: '所属项目',
  type: '类型',
  name: 'Secret 名称',
  detail: 'Secret',
  username_password: '用户名/密码',
  docker_registry: '镜像服务',
  opaque: 'Opaque',
  docker_address: '镜像服务地址',
  docker_username: '用户名',
  docker_password: '密码',
  docker_email: '邮箱地址',
  list: 'Secret',
};

export default {
  en,
  zh,
};
