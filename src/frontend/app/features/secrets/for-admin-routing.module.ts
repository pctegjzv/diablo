import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SecretCreateComponent } from './create/secret-create.component';
import { SecretDetailComponent } from './detail/secret-detail.component';
import { SecretListComponent } from './list/secret-list.component';

const routes: Routes = [
  { path: '', component: SecretListComponent },
  { path: 'create', component: SecretCreateComponent },
  { path: ':project/:name', component: SecretDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForAdminRoutingModule {}
