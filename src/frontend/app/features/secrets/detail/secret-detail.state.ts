import { Action } from '../../../utils/state-adapters';

export interface State {
  data: any;
  loading: boolean;
  error: any;
  tab: string;
  deleting: boolean;
  updating: boolean;
}

export const initialState: State = {
  data: null,
  loading: false,
  error: null,
  tab: 'base',
  deleting: false,
  updating: false,
};

export enum ActionTypes {
  Init = 'Init',
  FetchReceived = 'FetchReceived',
  FetchError = 'FetchError',
  ChangeTab = 'ChangeTab',
  Delete = 'Delete',
  DeleteSucc = 'DeleteSucc',
  DeleteError = 'DeleteError',
}

export class Init implements Action {
  readonly type = ActionTypes.Init;
  constructor(public project: string, public name: string) {}
}

export class FetchReceived implements Action {
  readonly type = ActionTypes.FetchReceived;
  constructor(public data: any) {}
}

export class FetchError implements Action {
  readonly type = ActionTypes.FetchError;
  constructor(public error: any) {}
}

export class ChangeTab implements Action {
  readonly type = ActionTypes.ChangeTab;
  constructor(public tab: string) {}
}

export class Delete implements Action {
  readonly type = ActionTypes.Delete;
}

export class DeleteSucc implements Action {
  readonly type = ActionTypes.DeleteSucc;
}

export class DeleteError implements Action {
  readonly type = ActionTypes.DeleteError;
}

export type Actions =
  | Init
  | Delete
  | FetchReceived
  | FetchError
  | DeleteSucc
  | DeleteError;

const init = (state: State, _init: Init): State => ({
  ...state,
  loading: true,
});

const fetchReceived = (state: State, { data }: FetchReceived): State => ({
  ...state,
  data,
  loading: false,
});

const fetchError = (state: State, { error }: FetchError): State => ({
  ...state,
  error,
  loading: false,
});

const setDeleting = (state: State): State => ({
  ...state,
  deleting: true,
});

const setNotDeleting = (state: State): State => ({
  ...state,
  deleting: false,
});

export function reducer(state: State = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.Init:
      return init(state, action);
    case ActionTypes.FetchReceived:
      return fetchReceived(state, action);
    case ActionTypes.FetchError:
      return fetchError(state, action);
    case ActionTypes.Delete:
      return setDeleting(state);
    case ActionTypes.DeleteSucc:
    case ActionTypes.DeleteError:
      return setNotDeleting(state);
    default:
      return state;
  }
}
