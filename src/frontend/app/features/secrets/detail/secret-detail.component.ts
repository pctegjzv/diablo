import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { head, mapValues } from 'lodash';
import { Subject, Subscription, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  scan,
  shareReplay,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
} from '../../../constants';
import { MODULE_ENV, ModuleEnv } from '../module-env';

import {
  ActionTypes,
  Actions,
  FetchError,
  FetchReceived,
  Init,
  initialState,
  reducer,
} from './secret-detail.state';

@Component({
  templateUrl: 'secret-detail.component.html',
  styleUrls: ['secret-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretDetailComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private destroy$ = new Subject<void>();
  private actions$ = new Subject<Actions>();
  private state$ = this.actions$.pipe(
    scan(reducer, initialState),
    distinctUntilChanged(),
    shareReplay(1),
  );

  data$ = this.state$.pipe(
    map(state => state.data),
    distinctUntilChanged(),
    map(data => (data && toModel(data)) || {}),
    shareReplay(1),
  );

  name$ = this.route.paramMap.pipe(
    map(paramMap => paramMap.get('name')),
    distinctUntilChanged(),
    shareReplay(1),
  );

  displayName$ = this.state$.pipe(
    filter(state => !!state.data),
    map(state => state.data),
    map(
      data =>
        data.data.metadata.annotations['alauda.io/displayName'] ||
        data.objectMeta.name,
    ),
    distinctUntilChanged(),
    shareReplay(1),
  );

  private initEffect$ = this.actions$.pipe(
    filter<Init>(action => action.type === ActionTypes.Init),
    switchMap(({ project, name }) =>
      this.http.get(`api/v1/secret/${project}/${name}`).pipe(
        map((result: any) => new FetchReceived(result)),
        catchError(error => of(new FetchError(error))),
      ),
    ),
    takeUntil(this.destroy$),
    tap(action => {
      if (action.type === ActionTypes.FetchError) {
        if (this.env === 'admin') {
          this.router.navigate(['../../'], {
            relativeTo: this.route,
          });
        } else {
          this.router.navigate(['../'], {
            relativeTo: this.route,
          });
        }
      }
    }),
  );

  @ViewChild('updateDialog') private updateDialog: TemplateRef<any>;

  get project() {
    return this.env === 'admin'
      ? this.route.snapshot.paramMap.get('project')
      : this.route.snapshot.parent.parent.paramMap.get('project');
  }

  get name() {
    return this.route.snapshot.paramMap.get('name');
  }

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    @Inject(MODULE_ENV) public env: ModuleEnv,
  ) {
    this.subscriptions.push(this.initEffect$.subscribe(this.actions$));
  }

  ngOnInit() {
    this.actions$.next(new Init(this.project, this.name));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  showUpdate() {
    this.dialog.open(this.updateDialog, {
      width: '900px',
    });
  }

  updated() {
    this.dialog.closeAll();
    this.actions$.next(new Init(this.project, this.name));
  }

  cancel() {
    this.dialog.closeAll();
  }
}

function toModel(resource: any) {
  const annotations = resource.metadata.annotations || {};
  const type = resource.type;
  const data = resource.data || {};

  const secretFields: {
    [key: string]: string;
  } =
    type !== 'kubernetes.io/dockerconfigjson'
      ? mapValues(data, value => atob(value))
      : parseDockerJson(JSON.parse(atob(data['.dockerconfigjson'])));

  return {
    name: resource.metadata.name,
    displayName: annotations[ANNOTATION_DISPLAY_NAME] || '',
    project: resource.metadata.namespace,
    description: annotations[ANNOTATION_DESCRIPTION] || '',
    type,
    username: data['username'] || '',
    password: data['password'] || '',
    creationTimestamp: resource.metadata.creationTimestamp,
    ...(type === 'Opaque'
      ? {
          generic: Object.keys(secretFields).map(key => ({
            key,
            value: secretFields[key],
          })),
        }
      : secretFields),
  };
}

function parseDockerJson(data: any) {
  const dockerAddress = head(Object.keys(data.auths || {}));

  return {
    dockerAddress,
    dockerUsername: (dockerAddress && data.auths[dockerAddress].username) || '',
    dockerPassword: (dockerAddress && data.auths[dockerAddress].password) || '',
    dockerEmail: (dockerAddress && data.auths[dockerAddress].email) || '',
  };
}
