import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MODULE_ENV, ModuleEnv } from '../module-env';

@Component({
  templateUrl: 'secret-create.component.html',
  styleUrls: ['secret-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretCreateComponent {
  get project() {
    if (this.env === 'admin') {
      return null;
    }

    return this.route.snapshot.parent.parent.paramMap.get('project');
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    @Inject(MODULE_ENV) public env: ModuleEnv,
  ) {}

  toDetail({ name, namespace }: { name: string; namespace: string }) {
    this.router.navigate(
      this.env === 'admin' ? ['../', namespace, name] : ['../', name],
      {
        relativeTo: this.route,
      },
    );
  }
}
