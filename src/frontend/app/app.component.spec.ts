import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CodeEditorModule } from 'alauda-ui';

import { AppComponent } from './app.component';
import { ServicesModule } from './services';
import { SharedModule } from './shared/index';
import { GlobalTranslateModule } from './translate';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        GlobalTranslateModule,
        HttpClientModule,
        SharedModule,
        ServicesModule,
        CodeEditorModule.forRoot({
          baseUrl: 'lib',
          defaultOptions: {
            fontSize: 12,
            folding: true,
            scrollBeyondLastLine: false,
            minimap: { enabled: false },
            find: { seedSearchStringFromSelection: false },
          },
        }),
      ],
      declarations: [AppComponent],
    });
  });

  it('should create the app', () => {
    // const fixture = TestBed.createComponent(AppComponent);
    // const app = fixture.debugElement.componentInstance;
    // expect(app).toBeTruthy();
  });
});
