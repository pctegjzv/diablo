import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './services';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  // List lazy loading modules here:
  {
    path: 'home',
    loadChildren: './features/home/home.module#HomeModule',
    canActivate: [AuthGuardService],
  },
  {
    path: 'admin',
    loadChildren: './features/admin/admin.module#AdminModule',
    canActivate: [AuthGuardService],
    data: { admin: true },
  },
  {
    path: 'workspace',
    loadChildren: './features/workspace/workspace.module#WorkspaceModule',
    canActivate: [AuthGuardService],
  },
  {
    path: 'terminal',
    loadChildren: 'app/terminal/module#TerminalModule',
    canActivate: [AuthGuardService],
    data: {
      auth: true,
    },
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      preloadingStrategy: PreloadAllModules,
      paramsInheritanceStrategy: 'always',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
