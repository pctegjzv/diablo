import { Directive } from '@angular/core';

@Directive({ selector: '[aloCardSectionHeader]' })
export class CardSectionHeaderDirective {}

@Directive({ selector: '[aloCardSectionBody]' })
export class CardSectionBodyDirective {}

@Directive({ selector: '[aloCardSectionFooter]' })
export class CardSectionFooterDirective {}
