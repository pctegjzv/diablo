import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  HostBinding,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { style } from './bem';
import {
  CardSectionBodyDirective,
  CardSectionFooterDirective,
  CardSectionHeaderDirective,
} from './helper-directives';

@Component({
  selector: 'alo-card-section',
  templateUrl: 'card-section.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardSectionComponent {
  styles = style('alo-card');

  @Input() type: 'list' | 'detail' | 'edit' = 'list';

  @HostBinding('class')
  get className() {
    return this.styles.element('section', this.type);
  }

  @ContentChild(CardSectionHeaderDirective) header: CardSectionHeaderDirective;
  @ContentChild(CardSectionBodyDirective) body: CardSectionBodyDirective;
  @ContentChild(CardSectionFooterDirective) footer: CardSectionFooterDirective;
}
