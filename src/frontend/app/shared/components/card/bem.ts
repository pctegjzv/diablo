// TODO: alauda-ui remove exports
export class BemClassName {
  constructor(private namespace: string) {}

  block(...modifiers: Array<{ [name: string]: boolean } | string>) {
    const activitedModifiers = getActivitedModifiers(modifiers);
    if (activitedModifiers.length) {
      return `${this.namespace} ${activitedModifiers
        .map(modifier => `${this.namespace}--${modifier}`)
        .join(' ')}`;
    }
    return `${this.namespace}`;
  }

  modifier(name: string) {
    return `${this.namespace}--${name}`;
  }

  element(
    name: string,
    ...modifiers: Array<{ [name: string]: boolean } | string>
  ) {
    const activitedModifiers = getActivitedModifiers(modifiers);

    if (activitedModifiers.length) {
      return `${this.namespace}__${name} ${activitedModifiers
        .map(modifier => `${this.namespace}__${name}--${modifier}`)
        .join(' ')}`;
    }

    return `${this.namespace}__${name}`;
  }
}

function getActivitedModifiers(
  modifiers: Array<{ [name: string]: boolean } | string>,
) {
  return modifiers
    .filter(modifier => !!modifier)
    .map(
      modifier =>
        typeof modifier === 'string'
          ? [modifier]
          : getActivitedModifiersFromObject(modifier),
    )
    .reduce((prev: string[], modifier) => prev.concat(modifier), []);
}

function getActivitedModifiersFromObject(modifiers: {
  [name: string]: boolean;
}) {
  return Object.keys(modifiers).filter(key => modifiers[key]);
}

export function style(namespace: string) {
  return new BemClassName(namespace);
}
