import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  ViewEncapsulation,
} from '@angular/core';

import { style } from './bem';
// import { CardSectionComponent } from './card-section.component';

@Component({
  selector: 'alo-card',
  templateUrl: 'card.component.html',
  styleUrls: ['card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {
  styles = style('alo-card');

  @HostBinding('class') block = this.styles.block();

  // @ContentChildren(CardSectionComponent) sections: QueryList<CardSectionComponent>;
}
