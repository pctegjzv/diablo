import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardSectionComponent } from './card-section.component';
import { CardComponent } from './card.component';
import {
  CardSectionBodyDirective,
  CardSectionFooterDirective,
  CardSectionHeaderDirective,
} from './helper-directives';

@NgModule({
  imports: [CommonModule],
  declarations: [
    CardComponent,
    CardSectionComponent,
    CardSectionHeaderDirective,
    CardSectionBodyDirective,
    CardSectionFooterDirective,
  ],
  exports: [
    CardComponent,
    CardSectionComponent,
    CardSectionHeaderDirective,
    CardSectionBodyDirective,
    CardSectionFooterDirective,
  ],
})
export class CardModule {}
