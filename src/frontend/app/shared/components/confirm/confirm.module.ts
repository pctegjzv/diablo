import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material';
import { ButtonModule, IconModule } from 'alauda-ui';

import { TranslateModule } from '../../../translate';

import { ConfirmComponent } from './confirm.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    IconModule,
    MatDialogModule,
    TranslateModule,
  ],
  declarations: [ConfirmComponent],
  exports: [ConfirmComponent],
  entryComponents: [ConfirmComponent],
})
export class ConfirmModule {}
