import { NgModule } from '@angular/core';
import { MenuTriggerComponent } from '@app/shared/components/menu-trigger/menu-trigger.component';
import { ButtonModule, DropdownModule, IconModule } from 'alauda-ui';

@NgModule({
  imports: [ButtonModule, DropdownModule, IconModule],
  declarations: [MenuTriggerComponent],
  exports: [MenuTriggerComponent],
})
export class MenuTriggerModule {}
