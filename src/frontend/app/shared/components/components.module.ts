import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MenuTriggerModule } from '@app/shared/components/menu-trigger/menu-trigger.module';

import { BreadcrumbModule } from './breadcrumb/breadcrumb.module';
import { CardModule } from './card/card.module';
import { ConfirmModule } from './confirm';
import { ErrorViewsModule } from './error-views';
import { KeyValueFormModule } from './key-value-form/key-value-form.module';
import { LogViewModule } from './log-view';
import { LogoModule } from './logo/logo.module';
import { PasswordModule } from './password';
import { ShellModule } from './shell/shell.module';
import { StatusIconModule } from './status-icon/status-icon.module';

@NgModule({
  imports: [
    CommonModule,
    CardModule,
    PasswordModule,
    LogoModule,
    BreadcrumbModule,
    ErrorViewsModule,
    ConfirmModule,
    LogViewModule,
    MenuTriggerModule,
    StatusIconModule,
    KeyValueFormModule,
  ],
  declarations: [],
  exports: [
    CardModule,
    PasswordModule,
    LogoModule,
    BreadcrumbModule,
    ErrorViewsModule,
    ConfirmModule,
    LogViewModule,
    MenuTriggerModule,
    ShellModule,
    StatusIconModule,
    KeyValueFormModule,
  ],
})
export class ComponentsModule {}
