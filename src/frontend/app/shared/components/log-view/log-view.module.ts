import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CheckboxModule, CodeEditorModule, IconModule } from 'alauda-ui';

import { TranslateModule } from '../../../translate';

import { LogViewComponent } from './log-view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CodeEditorModule,
    IconModule,
    CheckboxModule,
    TranslateModule,
  ],
  declarations: [LogViewComponent],
  exports: [LogViewComponent],
  entryComponents: [LogViewComponent],
})
export class LogViewModule {}
