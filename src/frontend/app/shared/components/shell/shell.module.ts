import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from 'alauda-ui';
import { TooltipModule } from 'alauda-ui';

import { TranslateModule } from '../../../translate';
import { StatusIconModule } from '../status-icon/status-icon.module';

import { ShellComponent } from './shell.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    TranslateModule,
    StatusIconModule,
    TooltipModule,
  ],
  declarations: [ShellComponent],
  exports: [ShellComponent],
})
export class ShellModule {}
