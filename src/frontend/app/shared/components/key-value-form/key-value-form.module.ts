import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KeyValueInputsValidatorDirective } from '@app/shared/components/key-value-form/inputs-validator.directive';
import { ButtonModule, IconModule, InputModule, SelectModule } from 'alauda-ui';

import { TranslateModule } from '../../../translate';

import { KeyValueFormComponent } from './form.component';
import { KeyValueInputsComponent } from './inputs.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ButtonModule,
    IconModule,
    InputModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
  ],
  declarations: [
    KeyValueFormComponent,
    KeyValueInputsValidatorDirective,
    KeyValueInputsComponent,
  ],
  exports: [KeyValueFormComponent, KeyValueInputsComponent],
})
export class KeyValueFormModule {}
