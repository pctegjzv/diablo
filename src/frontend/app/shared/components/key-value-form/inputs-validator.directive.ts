import { Directive, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: 'alo-key-value-inputs[aloKeyValueInputsValidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: KeyValueInputsValidatorDirective,
      multi: true,
    },
  ],
})
export class KeyValueInputsValidatorDirective implements Validator {
  @Input() previousKeys: string[] = [];
  validate(c: FormControl): { [key: string]: any } {
    if (!c.value) {
      return null;
    }

    const [key, value] = c.value;

    const duplicateKey = this.previousKeys.includes(key);
    const keyIsMissing = !!value && !key;

    if (duplicateKey || keyIsMissing) {
      const error: any = {};
      if (duplicateKey) {
        error.duplicateKey = key;
      }

      if (keyIsMissing) {
        error.keyIsMissing = keyIsMissing;
      }
      return error;
    } else {
      return null;
    }
  }
}
