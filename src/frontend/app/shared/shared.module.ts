import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatDialogModule,
  MatIconModule,
  MatMenuModule,
  MatSnackBarModule,
} from '@angular/material';
import {
  ButtonModule,
  CardModule as AUICardModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormFieldModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  MessageModule,
  NotificationModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  SwitchModule,
  TableModule,
  TabsModule,
  TagModule,
  ToastModule,
  TooltipModule,
  TreeSelectModule,
} from 'alauda-ui';

import { TranslateModule } from '../translate';

import { ComponentsModule } from './components/components.module';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';

const EXPORTABLE_IMPORTS = [
  // Vendor modules:
  CommonModule,
  TranslateModule,
  FlexLayoutModule,

  // Material imports
  MatIconModule,
  MatDialogModule,
  MatMenuModule,
  MatSnackBarModule,

  // AUI imports
  TableModule,
  ToastModule,
  SortModule,
  SwitchModule,
  ButtonModule,
  FormFieldModule,
  CodeEditorModule,
  AUICardModule,
  InputModule,
  FormModule,
  TooltipModule,
  TagModule,
  SelectModule,
  RadioModule,
  DropdownModule,
  IconModule,
  InlineAlertModule,
  CheckboxModule,
  PaginatorModule,
  DialogModule,
  NotificationModule,
  MessageModule,
  TreeSelectModule,
  TabsModule,

  // App shared modules:
  ComponentsModule,
  PipesModule,
  DirectivesModule,
];

@NgModule({
  imports: [...EXPORTABLE_IMPORTS],
  exports: [...EXPORTABLE_IMPORTS],
})
export class SharedModule {}
