import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AsyncDataDirective } from './async-data.directive';
import { PageHeaderContentDirective } from './page-header-content.directive';

@NgModule({
  imports: [CommonModule, PortalModule],
  declarations: [PageHeaderContentDirective, AsyncDataDirective],
  exports: [PageHeaderContentDirective, AsyncDataDirective],
})
export class DirectivesModule {}
