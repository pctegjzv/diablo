import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

import { TranslateService } from '../../translate';

@Pipe({ name: 'aloDuration' })
export class DurationPipe implements PipeTransform {
  constructor(private translate: TranslateService) {}
  transform(ms: any): string {
    if (!ms) {
      return '-';
    }

    let message = '';
    if (ms >= 1000) {
      const duration = moment.duration(ms);

      const days = duration.days();
      const hours = duration.hours();
      const minutes = duration.minutes();
      const seconds = duration.seconds();

      message += days ? days + this.translate.get('day') : '';
      message += hours ? hours + this.translate.get('hour') : '';
      message += minutes ? minutes + this.translate.get('minute') : '';
      message += seconds ? seconds + this.translate.get('second') : '';
    } else if (ms > 0) {
      message = this.translate.get('less_then_a_second');
    } else {
      message = '-';
    }

    return message;
  }
}
