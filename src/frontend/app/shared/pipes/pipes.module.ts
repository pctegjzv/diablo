import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DurationPipe } from './duration.pipe';
import { RelativeTimePipe } from './relative-time.pipe';
import { TimePipe } from './time.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [RelativeTimePipe, TimePipe, DurationPipe],
  exports: [RelativeTimePipe, TimePipe, DurationPipe],
})
export class PipesModule {}
