import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import * as jwtDecode from 'jwt-decode';
import { get } from 'lodash';
import { Observable, Subject, Subscription, of } from 'rxjs';
import {
  catchError,
  concatMap,
  map,
  publishReplay,
  refCount,
  tap,
} from 'rxjs/operators';

import { TranslateService } from '../translate';

interface LoginMeta {
  settings: {
    enabled: boolean;
    client_id: string;
    client_secret: string;
    redirect_uri: string;
    auth_issuer: string;
    auth_namespace: string;
    root_ca_filepath: string;
    scopes: string[];
    response_type: string;
    custom_redirect_uri: {
      devops: string;
      dashboard: string;
    };
  };
  auth_url: string;
  errors: any[];
}

interface AuthState {
  error: any;
  account: any;
  accessToken: string;
  enabled: boolean;
  dashboardHost: string;
  portalHost: string;
  authUrl: string;
}

const initialState: AuthState = {
  error: null,
  account: null,
  accessToken: '',
  enabled: true,
  dashboardHost: '',
  portalHost: '',
  authUrl: '',
};

@Injectable()
export class AuthService implements OnDestroy {
  private subscriptions: Subscription[] = [];

  private login$ = new Subject<any>();

  state$: Observable<AuthState> = this.login$.pipe(
    concatMap(params => {
      return this.http.get<LoginMeta>('api/v1/login').pipe(
        tap(result => {
          if (result.errors && result.errors.length) {
            throw result.errors;
          }

          if (result.settings && result.settings.enabled && !result.auth_url) {
            throw Error(`auth_url must provided`);
          }
        }),
        map(result => ({
          ...initialState,
          dashboardHost:
            get(result, 'settings.custom_redirect_uri.dashboard') || '',
          portalHost: get(result, 'settings.custom_redirect_uri.portal') || '',
          enabled: get(result, 'settings.enabled') || false,
          authUrl: result.auth_url,
        })),
        catchError((error: any) => of({ ...initialState, error })),
        concatMap(state => {
          if (state.error || !state.enabled) {
            return of(state);
          }

          if (params.id_token) {
            const account = this.decodeToken(params.id_token);
            const accessToken = params.access_token;
            return of({ ...state, account, accessToken });
          }

          if (params.code) {
            return this.http
              .get(`api/v1/login/callback?code=${params.code}`)
              .pipe(
                map((res: any) => {
                  const token = get(res, 'id_token');
                  const accessToken = get(res, 'access_token');
                  const account = this.decodeToken(token);
                  return {
                    ...state,
                    account,
                    accessToken,
                  };
                }),
                catchError(() => of(state)),
              );
          }

          return of({
            ...state,
            account: this.decodeToken(localStorage.getItem('token')),
            accessToken: localStorage.getItem('access_token'),
          });
        }),
        tap(state => {
          if (state.error) {
            return;
          }
          this.snapshot = state;

          if (!state.enabled) {
            return;
          }

          if (state.account && state.account.token) {
            localStorage.setItem('token', state.account.token);
            localStorage.setItem('access_token', state.accessToken);
          } else {
            this.logout();
          }
        }),
      );
    }),
    publishReplay(1),
    refCount(),
  );

  snapshot: AuthState = initialState;

  authedOrNoNeed$ = this.state$.pipe(
    map(state => !!state.account || !state.enabled),
    publishReplay(1),
    refCount(),
  );

  constructor(private http: HttpClient, private translate: TranslateService) {
    this.subscriptions.push(this.state$.subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
  login(params: any) {
    this.login$.next(params);
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('access_token');
    if (this.snapshot.authUrl && window) {
      window.location.href = this.snapshot.authUrl;
    }
  }

  toDashboard() {
    if (window && this.snapshot.dashboardHost) {
      const queryParams = get(this.snapshot.account, 'token')
        ? `?locale=${this.translate.currentLang}&id_token=${get(
            this.snapshot.account,
            'token',
          )}`
        : '';
      window.location.href = this.snapshot.dashboardHost + queryParams;
    }
  }

  toPortal() {
    if (window && this.snapshot.portalHost) {
      const queryParams = this.snapshot.account.token
        ? `?locale=${this.translate.currentLang}&id_token=${
            this.snapshot.account.token
          }`
        : '';
      window.location.href = this.snapshot.portalHost + queryParams;
    }
  }

  private decodeToken(token: string) {
    try {
      const decoded = jwtDecode(token);
      return {
        token: token,
        name: get(decoded, 'name') || '',
        email: get(decoded, 'email') || '',
      };
    } catch (err) {
      return null;
    }
  }
}
