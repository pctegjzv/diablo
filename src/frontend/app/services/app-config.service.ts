// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ThirdPartyApiService, ThirdPartyContentFindResult } from '@app/api';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

export interface AppConfig {
  serverTime: number;
}

@Injectable()
export class AppConfigService {
  private readonly _configPath = 'api/appConfig.json';
  private _config: AppConfig;
  private _initTime: number;
  private _thirdPartyConfig: ThirdPartyContentFindResult;

  constructor(
    private readonly http: HttpClient,
    private thirdPartyApi: ThirdPartyApiService,
  ) {
    this.init();
  }

  init(): void {
    this.getAppConfig().subscribe(config => {
      // Set init time when response from the backend will arrive.
      this._config = config;
      this._initTime = new Date().getTime();
    });
    this.getThirdPartyConfig().subscribe();
  }

  getAppConfig(): Observable<AppConfig> {
    return this.http.get<AppConfig>(this._configPath);
  }

  getServerTime(): Date {
    if (this._config && this._config.serverTime) {
      const elapsed = new Date().getTime() - this._initTime;
      return new Date(this._config.serverTime + elapsed);
    } else {
      return new Date();
    }
  }

  getThirdPartyConfig() {
    if (this._thirdPartyConfig) {
      return of(this._thirdPartyConfig);
    }

    return this.thirdPartyApi
      .all()
      .pipe(tap(result => (this._thirdPartyConfig = result)));
  }

  getThirdPartyPortalEnabled(): boolean {
    return (
      this._thirdPartyConfig &&
      !this._thirdPartyConfig.error &&
      this._thirdPartyConfig.data.length > 0
    );
  }
}
