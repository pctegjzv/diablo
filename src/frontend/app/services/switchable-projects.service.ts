import { Injectable } from '@angular/core';
import { ProjectApiService } from '@app/api';
import { Subject, of } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  switchMap,
} from 'rxjs/operators';

@Injectable()
export class SwitchableProjectsService {
  private load$ = new Subject<void>();
  items$ = this.load$.pipe(
    switchMap(() =>
      this.api
        .find({
          searchBy: 'name',
          keywords: '',
          sort: 'creationTimestamp',
          direction: 'desc',
          pageIndex: 0,
          pageSize: 10,
        })
        .pipe(
          map(result => result.items),
          catchError(() => of([])),
        ),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(private api: ProjectApiService) {
    this.items$.subscribe(() => {});
    this.load$.next();
  }

  reload() {
    this.load$.next();
  }
}
