import { Injectable } from '@angular/core';
import { CodeEditorIntl } from 'alauda-ui';

@Injectable()
export class CustomCodeEditorIntlService extends CodeEditorIntl {
  getLanguageLabel(lang: string) {
    if (lang && lang.toLowerCase() === 'jenkinsfile') {
      return 'Jenkinsfile';
    }
    return super.getLanguageLabel(lang);
  }
}
