import { Injectable } from '@angular/core';
import * as distanceInWordsStrict from 'date-fns/distance_in_words_strict';
import * as format from 'date-fns/format';
import * as zh_cn from 'date-fns/locale/zh_cn/index.js';

import { TranslateService } from '../translate';

import { AppConfigService } from './app-config.service';

const milisecondsInAWeek = 1000 * 60 * 60 * 24 * 7;

@Injectable()
export class TimeService {
  translatedUnits: { [key: string]: string[] };

  constructor(
    private readonly config: AppConfigService,
    private readonly translate: TranslateService,
  ) {}

  transformRelative(value: string, ...args: any[]): string {
    if (value == null) {
      return '-';
    }
    // Current server time.
    const serverTime = this.config.getServerTime();

    // Current and given times in miliseconds.
    const currentTime = this.getCurrentTime_(serverTime);
    const givenTime = new Date(value).getTime();

    if (currentTime - givenTime > milisecondsInAWeek) {
      return format(value, 'YYYY-MM-DD');
    } else {
      return distanceInWordsStrict(currentTime, givenTime, {
        addSuffix: args[0],
        locale: this.translate.currentLang === 'zh' ? zh_cn : 'en',
      });
    }
  }

  transform(value: Date | string | number): string {
    if (value === null) {
      return '-';
    }

    return format(value, 'YYYY-MM-DD HH:mm:ss');
  }

  /**
   * Returns current time. If appConfig.serverTime is provided then it will be returned, otherwise
   * current client time will be used.
   */
  private getCurrentTime_(serverTime: Date): number {
    return serverTime ? serverTime.getTime() : new Date().getTime();
  }
}
