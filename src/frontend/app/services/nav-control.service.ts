import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { MicroserviceEnvironmentService } from '../../asf/api/microservice-environment-api.service';

export enum ApiGroup {
  DevOps = 'devops.alauda.io',
  ASF = 'asf.alauda.io',
  Apps = 'apps',
}

export interface ApiGroupsResponse {
  groups: {
    name: ApiGroup;
    preferredVersion: {
      groupVersion: string;
      version: string;
    };
    versions: {
      groupVersion: string;
      version: string;
    }[];
    serverAddressByClientCIDRs: any;
  }[];
}

@Injectable()
export class NavControlService {
  constructor(
    private http: HttpClient,
    private microserviceEnvironmentApi: MicroserviceEnvironmentService,
  ) {}

  getApiGroups() {
    return this.http.get<ApiGroupsResponse>(`api/v1/apis`);
  }

  getApiGroupNames(): Observable<ApiGroup[]> {
    return this.getApiGroups().pipe(
      map(result => {
        return result.groups.map(api => api.name).filter(name => !!name);
      }),
    );
  }

  getAsfSubMenus(project: string) {
    return this.microserviceEnvironmentApi.getBindingDetail(project).pipe(
      map(env => {
        return env ? env.spec.microserviceComponentRefs : [];
      }),
    );
  }
}
