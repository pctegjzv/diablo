import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Inject, NgModule, Optional, SkipSelf } from '@angular/core';
import { ApiModule } from '@app/api';
import { IconRegistryService, MonacoProviderService } from 'alauda-ui';
import * as basicIconsUrl from 'alauda-ui/assets/basic-icons.svg';

import { TranslateService } from '@app/translate';

import { AppConfigService } from '@app/services/app-config.service';
import { AuthGuardService } from '@app/services/auth-guard.service';
import { AuthInterceptor } from '@app/services/auth.interceptor';
import { AuthService } from '@app/services/auth.service';
import { GlobalLoadingInterceptor } from '@app/services/global-loading.interceptor';
import { IconService } from '@app/services/icon.service';
import { NavControlService } from '@app/services/nav-control.service';
import { OAuthSecretValidatorService } from '@app/services/oauth-secret-validator.service';
import { SwitchableProjectsService } from '@app/services/switchable-projects.service';
import { TerminalService } from '@app/services/terminal.service';
import { TimeService } from '@app/services/time.service';
import { UiStateService } from '@app/services/ui-state.service';

/**
 * Services in Angular App is static and singleton over the whole system,
 * so we will import them into the root module.
 *
 * This is to replace the Core module, which we believe is somewhat duplicates with the
 * purpose of global service module.
 */
@NgModule({
  imports: [ApiModule],
  providers: [
    AppConfigService,
    TimeService,
    IconService,
    MonacoProviderService,
    AuthService,
    AuthGuardService,
    UiStateService,
    SwitchableProjectsService,
    TerminalService,
    OAuthSecretValidatorService,
    NavControlService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalLoadingInterceptor,
      multi: true,
    },
  ],
})
export class ServicesModule {
  /* Make sure ServicesModule is imported only by one NgModule the RootModule */
  constructor(
    @Inject(ServicesModule)
    @Optional()
    @SkipSelf()
    parentModule: ServicesModule,
    monacoProvider: MonacoProviderService,
    iconRegistryService: IconRegistryService,
    // Inject the following to make sure they are loaded ahead of other components.
    _appConfig: AppConfigService,
    _iconService: IconService,
    _translate: TranslateService,
  ) {
    if (parentModule) {
      throw new Error(
        'ServicesModule is already loaded. Import only in AppModule.',
      );
    }

    monacoProvider.initMonaco();
    iconRegistryService.registrySvgSymbolsByUrl(basicIconsUrl);
    iconRegistryService.registrySvgSymbolsByUrl(
      'assets/icons/diablo-icons.svg',
    );
  }
}
