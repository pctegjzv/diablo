import { Inject, Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * A map of MatIcon name to SVG path. The following will be
 * registered into MatIcon, so to be used with
 * <mat-icon svgIcon="iconName"></mat-icon>
 *
 * TODO: use a script to generate icons automatically.
 */
export const SvgIcons = {
  'app-logo': 'app-logo.svg',
  'app-logo-text': 'app-logo-text.svg',
  danger: '16x16/exclamation_solid.svg',
  close: '16x16/close_solid.svg',
  remove: '16x16/minus_circle_s.svg',
  fullscreen: '16x16/expand_solid.svg',
  'fullscreen-exit': '16x16/compress_solid.svg',
  'caret-down': '16x16/caret_down_solid.svg',
  actions: '16x16/ellipsis_v_solid.svg',
  account: '16x16/account_circle_s.svg',
  plus: '16x16/plus_line.svg',
  'history-pending': '16x16/play_circle_s.svg',
  'history-queued': '16x16/hourglass_half_circle_s.svg',
  'history-running': '16x16/sync_circle_s.svg',
  'history-aborted': '16x16/paused_circle_s.svg',
  'history-cancelled': '16x16/minus_circle_s.svg',
  'history-complete': '16x16/check_circle_s.svg',
  'history-failed': '16x16/close_circle_s.svg',
  'history-unknown': '16x16/question_circle_s.svg',
  projects: '16x16/project_solid.svg',
  applications: '16x16/application_s.svg',
  'jenkins-services': '16x16/jenkins_reverse_solid.svg',
  secrets: '16x16/secrets_solid.svg',
  pipelines: '16x16/wrench_solid.svg',
  members: '16x16/member_solid.svg',
  eye: '16x16/eye_s.svg',
  'microservice-environment-empty':
    'microservice/microservice_environment_empty.svg',
  'eureka-primary': 'microservice/logo/eureka-primary.svg',
  'eureka-success': 'microservice/logo/eureka-success.svg',
  'eureka-error': 'microservice/logo/eureka-error.svg',
  'eureka-default': 'microservice/logo/eureka-default.svg',
  'config-server-primary': 'microservice/logo/config-server-primary.svg',
  'config-server-success': 'microservice/logo/config-server-success.svg',
  'config-server-error': 'microservice/logo/config-server-error.svg',
  'config-server-default': 'microservice/logo/config-server-default.svg',
  'hystrix-dashboard-primary':
    'microservice/logo/hystrix-dashboard-primary.svg',
  'hystrix-dashboard-success':
    'microservice/logo/hystrix-dashboard-success.svg',
  'hystrix-dashboard-error': 'microservice/logo/hystrix-dashboard-error.svg',
  'hystrix-dashboard-default':
    'microservice/logo/hystrix-dashboard-default.svg',
  'kafka-primary': 'microservice/logo/kafka-primary.svg',
  'kafka-success': 'microservice/logo/kafka-success.svg',
  'kafka-error': 'microservice/logo/kafka-error.svg',
  'kafka-default': 'microservice/logo/kafka-default.svg',
  'pinpoint-hbase-primary': 'microservice/logo/pinpoint-hbase-primary.svg',
  'pinpoint-hbase-success': 'microservice/logo/pinpoint-hbase-success.svg',
  'pinpoint-hbase-error': 'microservice/logo/pinpoint-hbase-error.svg',
  'pinpoint-hbase-default': 'microservice/logo/pinpoint-hbase-default.svg',
  'turbine-primary': 'microservice/logo/turbine-primary.svg',
  'turbine-success': 'microservice/logo/turbine-success.svg',
  'turbine-error': 'microservice/logo/turbine-error.svg',
  'turbine-default': 'microservice/logo/turbine-default.svg',
  'zookeeper-primary': 'microservice/logo/zookeeper-primary.svg',
  'zookeeper-success': 'microservice/logo/zookeeper-success.svg',
  'zookeeper-error': 'microservice/logo/zookeeper-error.svg',
  'zookeeper-default': 'microservice/logo/zookeeper-default.svg',
  'zuul-primary': 'microservice/logo/zuul-primary.svg',
  'zuul-success': 'microservice/logo/zuul-success.svg',
  'zuul-error': 'microservice/logo/zuul-error.svg',
  'zuul-default': 'microservice/logo/zuul-default.svg',
};

@Injectable()
export class IconService {
  private readonly iconPath = 'assets/icons';

  constructor(
    @Inject(MatIconRegistry) private iconRegistry: MatIconRegistry,
    @Inject(DomSanitizer) private sanitizer: DomSanitizer,
  ) {
    /**
     * Register all SVG icon images. Once registered, your svg icon can be used
     * with <mat-icon svgIcon="iconName"></mat-icon> element
     */
    Object.entries(SvgIcons).forEach(([name, path]) => {
      this.iconRegistry.addSvgIcon(
        name,
        this.sanitizer.bypassSecurityTrustResourceUrl(
          `${this.iconPath}/${path}`,
        ),
      );
    });
  }
}
