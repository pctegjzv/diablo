import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { get } from 'lodash';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const authReq =
      req.url.startsWith('api/v1') && this.auth.snapshot.account
        ? req.clone({
            setHeaders: {
              Authorization: `Bearer ${this.auth.snapshot.account.token}`,
              Token: this.auth.snapshot.accessToken || '',
            },
          })
        : req;

    return next.handle(authReq).pipe(
      tap(
        (result: any) => {
          const errors = get(result, 'body.errors') || [];
          const unauthorized = errors.find(
            (error: any) => get(error, 'ErrStatus.code') === 401,
          );

          if (unauthorized) {
            this.auth.logout();
          }
        },
        (err: any) => {
          if (err && err.status === 401) {
            this.auth.logout();
          }
        },
      ),
    );
  }
}
