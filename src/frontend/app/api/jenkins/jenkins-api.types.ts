import { ResourceBinding, ResourceService } from '@app/api/api.types';

export type JenkinsService = ResourceService;

export interface JenkinsBinding extends ResourceBinding {
  host: string;
}
