import { BindingParams, K8SResource, ListResult } from '@app/api/api.types';
import {
  JenkinsBinding,
  JenkinsService,
} from '@app/api/jenkins/jenkins-api.types';
import { ToolIntegrateParams } from '@app/api/tool-chain/tool-chain-api.types';
import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_PRODUCT,
  API_GROUP_VERSION,
  PRODUCT_NAME,
} from '@app/constants';
import { get } from 'lodash';

export function mapResourceToJenkinsBinding(
  resource: K8SResource,
): JenkinsBinding {
  return {
    name: get(resource, 'objectMeta.name', ''),
    namespace: get(resource, 'objectMeta.namespace', ''),
    description: get(resource, ['objectMeta', ANNOTATION_DESCRIPTION], ''),
    creationTimestamp: get(resource, 'objectMeta.creationTimestamp', ''),
    secret: get(resource, 'spec.account.secret.name', ''),
    service: get(resource, 'spec.jenkins.name', ''),
    host: get(resource, 'spec.http.host', ''),
    status: {
      phase: get(resource, 'status.status', ''),
      message: get(resource, 'status.message', ''),
    },
    __original: resource,
  };
}

export function mapFindBindingResponseToList(
  res: any,
): ListResult<JenkinsBinding> {
  return {
    total: get(res, 'listMeta.totalItems'),
    items: res.jenkinsbindings.map(mapResourceToJenkinsBinding),
  };
}

export function toCreateBindingResource(model: BindingParams): any {
  return {
    apiVersion: API_GROUP_VERSION,
    kind: 'JenkinsBinding',
    metadata: {
      name: model.name,
      annotations: {
        [ANNOTATION_DESCRIPTION]: model.description,
        [ANNOTATION_PRODUCT]: PRODUCT_NAME,
      },
      namespace: model.namespace,
    },
    spec: {
      jenkins: {
        name: model.service,
      },
      account: {
        secret: {
          name: model.secret,
          usernameKey: 'username',
          apiTokenKey: 'password',
        },
      },
    },
  };
}

export function mapResourceToJenkinsService(resource: any): JenkinsService {
  return {
    name: get(resource, 'metadata.name', ''),
    creationTimestamp: get(resource, 'metadata.creationTimestamp', ''),
    host: get(resource, 'spec.http.host', ''),
    html: get(resource, 'spec.http.html', ''),
    status: resource.status,
    __original: resource,
  };
}

export function mapIntegrateConfigToJenkinsPayload(data: ToolIntegrateParams) {
  return {
    metadata: {
      name: data.name,
    },
    spec: {
      http: {
        host: data.host,
      },
    },
  };
}
