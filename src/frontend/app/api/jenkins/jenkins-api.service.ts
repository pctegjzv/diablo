import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BindingParams, ListResult, ResourceService } from '@app/api/api.types';
import { JenkinsBinding } from '@app/api/jenkins/jenkins-api.types';
import {
  mapFindBindingResponseToList,
  mapIntegrateConfigToJenkinsPayload,
  mapResourceToJenkinsService,
  toCreateBindingResource,
} from '@app/api/jenkins/utils';
import { ToolIntegrateParams } from '@app/api/tool-chain/tool-chain-api.types';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class JenkinsApiService {
  constructor(private http: HttpClient) {}

  findBindings(query: {
    [key: string]: string;
  }): Observable<ListResult<JenkinsBinding>> {
    return this.http
      .get(`api/v1/jenkinsbinding`, { params: query })
      .pipe(map(mapFindBindingResponseToList));
  }

  findBindingsByNamespace(
    namespace: string,
    query: { [key: string]: string },
  ): Observable<ListResult<JenkinsBinding>> {
    return this.http
      .get(`api/v1/jenkinsbinding/${namespace}`, { params: query })
      .pipe(map(mapFindBindingResponseToList));
  }

  createBinding(model: BindingParams) {
    return this.http.post('api/v1/others', [toCreateBindingResource(model)]);
  }

  getService(name: string): Observable<ResourceService> {
    return this.http
      .get(`api/v1/jenkinses/${name}`)
      .pipe(map(mapResourceToJenkinsService));
  }

  createService(data: ToolIntegrateParams) {
    return this.http.post(
      'api/v1/jenkinses',
      mapIntegrateConfigToJenkinsPayload(data),
    );
  }

  updateService(data: ToolIntegrateParams) {
    return this.http.put(
      `api/v1/jenkinses/${data.name}`,
      mapIntegrateConfigToJenkinsPayload(data),
    );
  }

  deleteService(name: string) {
    return this.http.delete(`api/v1/jenkinses/${name}`);
  }
}
