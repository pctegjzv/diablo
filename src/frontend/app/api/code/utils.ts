import {
  CodeBinding,
  CodeRepoBindingResponse,
  CodeRepoOwner,
  CodeRepository,
  CodeRepositoryResponse,
  CodeService,
} from '@app/api/code/code-api.types';
import { SecretTypes } from '@app/api/secret-api.service';
import { ToolIntegrateParams } from '@app/api/tool-chain/tool-chain-api.types';
import { ANNOTATION_DESCRIPTION } from '@app/constants';
import { get } from 'lodash';

export function toCodeRepoBinding(
  resource: CodeRepoBindingResponse,
): CodeBinding {
  const meta = resource.objectMeta || resource.metadata || ({} as any);
  const annotations = meta.annotations || ({} as any);

  return {
    name: get(meta, 'name') || '',
    namespace: get(meta, 'namespace') || '',
    type: get(meta, 'labels.codeRepoServiceType') || '',
    creationTimestamp: get(meta, 'creationTimestamp') || '',
    description: annotations[ANNOTATION_DESCRIPTION] || '',
    secretType:
      (annotations['alauda.io/secretType'] as SecretTypes) ||
      SecretTypes.basicAuth,
    secret: get(resource, 'spec.account.secret.name') || '',
    service: get(resource, 'spec.codeRepoService.name') || '',
    owners: get(resource, 'spec.account.owners') || [],
    status: resource.status,
    __original: resource,
  };
}

export function toCodeRepository(
  resource: CodeRepositoryResponse,
): CodeRepository {
  return {
    namespace: resource.objectMeta.namespace,
    name: resource.objectMeta.name,
    httpURL: resource.spec.repository.htmlURL,
    sshURL: resource.spec.repository.sshURL,
    ownerName: resource.spec.repository.owner.name,
    capacity: resource.spec.repository.size,
    commit: resource.spec.repository.lastCommitAt,
    type: resource.spec.repository.codeRepoServiceType,
    fullName: resource.spec.repository.fullName,
    status: resource.status,
  };
}

export function fromCodeRepoBinding(
  model: CodeBinding,
): CodeRepoBindingResponse {
  const original = model.__original || ({} as CodeRepoBindingResponse);
  const originalMeta = original.metadata ||
    original.objectMeta || {
      labels: {},
      annotations: {},
    };
  const originalSpec = original.spec || {
    codeRepoService: {},
    account: {
      owners: null as CodeRepoOwner[],
    },
  };

  return {
    ...original,
    metadata: {
      ...originalMeta,
      name: model.name,
      namespace: model.namespace,
      labels: {
        ...originalMeta.labels,
        codeRepoService: model.service,
      },
      annotations: {
        ...originalMeta.annotations,
        [ANNOTATION_DESCRIPTION]: model.description,
        'alauda.io/secretType': model.secretType,
      },
    },
    spec: {
      ...originalSpec,
      codeRepoService: {
        ...originalSpec.codeRepoService,
        name: model.service,
      },
      account: {
        ...originalSpec.account,
        secret: {
          name: model.secret,
        },
        owners: model.owners || originalSpec.account.owners,
      },
    },
  };
}

export function mapResourceToCodeService(resource: any): CodeService {
  return {
    name: get(resource, 'metadata.name', ''),
    creationTimestamp: get(resource, 'metadata.creationTimestamp', ''),
    host: get(resource, 'spec.http.host', ''),
    html: get(resource, 'spec.http.html', ''),
    status: resource.status,
    type: get(resource, 'spec.type', ''),
    public: get(resource, 'spec.public', false),
    createAppUrl: get(
      resource,
      ['metadata', 'annotations', 'alauda.io/createAppUrl'],
      '',
    ),
    __original: resource,
  };
}

export function mapIntegrateConfigToCodePayload(data: ToolIntegrateParams) {
  return {
    metadata: {
      name: data.name,
    },
    spec: {
      http: {
        host: data.host,
      },
      type: data.type,
      public: data.public,
    },
  };
}
