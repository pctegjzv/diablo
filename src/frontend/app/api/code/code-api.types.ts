import { ResourceService } from '@app/api/api.types';
import { SecretTypes } from '@app/api/secret-api.service';

export interface CodeRepoServiceType {
  type: string;
  name: string;
  public: boolean;
  displayName: {
    en: string;
    zh: string;
  };
  host: string;
  html?: string;
}

export interface CodeRepoServiceResponse {
  objectMeta?: {
    name: string;
    creationTimestamp: string;
  };
  typeMeta?: {
    kind: string;
  };
  metadata?: {
    name: string;
    creationTimestamp?: string;
  };
  spec: {
    http: {
      host: string;
      html?: string;
    };
    type: string;
    public: boolean;
  };
  status?: {
    phase: string;
    message: string;
  };
}

export interface CodeRepoBindingResponse {
  objectMeta?: {
    name: string;
    namespace: string;
    creationTimestamp?: string;
    annotations?: {
      [name: string]: any;
    };
    labels?: {
      [name: string]: any;
    };
  };
  metadata?: {
    name: string;
    namespace: string;
    creationTimestamp?: string;
    annotations?: {
      [name: string]: any;
    };
    labels?: {
      [name: string]: any;
    };
  };
  typeMeta?: {
    kind: string;
  };
  spec: {
    codeRepoService: {
      name: string;
    };
    account: {
      name?: string;
      secret: {
        name: string;
      };
      owners?: CodeRepoOwner[];
    };
  };
  status: CodeBindingStatus;
}

export interface CodeService extends ResourceService {
  type: string;
  public: boolean;
  createAppUrl: string;
}

export interface CodeRepoOwner {
  type: 'User' | 'Org';
  name: string;
  all: boolean;
  repositories: string[];
}

export interface CodeBindingStatus {
  http: {
    delay: number;
    lastAttempt: string;
    statusCode: number;
  };
  lastUpdated: any;
  phase: string;
  message: string;
}

export interface CodeBinding {
  name: string;
  type?: string;
  namespace?: string;
  creationTimestamp?: string;
  description: string;
  secretType: SecretTypes;
  secret?: string;
  service?: string;
  owners?: CodeRepoOwner[];
  status?: CodeBindingStatus;
  __original?: CodeRepoBindingResponse;
}

export interface CodeRepositoryResponse {
  objectMeta: {
    name: string;
    namespace: string;
    labels: {
      codeRepoBinding: string;
      codeRepoService: string;
    };
    creationTimestamp: string;
  };
  typeMeta: {
    kind: 'coderepository';
  };
  spec: {
    codeRepoBinding: {
      name: string;
    };
    repository: {
      codeRepoServiceType: string;
      name: string;
      fullName: string;
      description: string;
      htmlURL: string;
      cloneURL: string;
      sshURL: string;
      language: string;
      lastCommitID: string;
      lastCommitAt: string;
      createdAt: string;
      pushAt: string;
      updatedAt: string;
      private: boolean;
      size: number;
      data: any;
      owner: {
        id: string;
        type: string;
        name: string;
      };
    };
  };
  status: {
    phase: string;
    lastUpdated: string;
    conditions: any;
    message: string;
  };
}

export interface CodeRepositoriesResponse {
  listMeta: {
    totalItems: number;
  };
  coderepositories: CodeRepositoryResponse[];
  errors: any[];
}

export interface CodeRepository {
  namespace: string;
  name: string;
  httpURL: string;
  sshURL: string;
  capacity: number;
  commit: string;
  type: string;
  fullName: string;
  ownerName: string;
  status: {
    phase: string;
    lastUpdated: string;
    conditions: any;
    message: string;
  };
}

export interface CodeRepositoriesFindParams {
  name?: string;
  pageIndex?: number;
  itemsPerPage?: number;
  project: string;
}

export interface CodeRepoServicesResponse {
  listMeta: {
    totalItems: number;
  };
  codereposervices: CodeRepoServiceResponse[];
}

export interface CodeRepoBindingsResponse {
  listMeta: {
    totalItems: number;
  };
  coderepobindings: CodeRepoBindingResponse[];
}

export interface CodeRepoRelatedResource {
  name: string;
  namespace: string;
  kind: string;
}

export interface CodeRepoRelatedResourcesResponse {
  items: CodeRepoRelatedResource[];
}

export interface RemoteRepository {
  codeRepoServiceType: string;
  id: string;
  name: string;
  fullName: string;
  description: string;
  htmlURL: string;
  cloneURL: string;
  sshURL: string;
  language: string;
  lastCommitID: string;
  lastCommitTime: string;
  createdAt: Date;
  pushAt: Date;
  updatedAt: Date;
  private: boolean;
  size: number;
  data?: any;
}

export interface RemoteRepositoryOwner {
  type: 'User' | 'Org';
  name: string;
  email: string;
  htmlURL: string;
  avatarURL: string;
  diskUsage: number;
  repositories: RemoteRepository[];
}

export interface RemoteRepositoriesResponse {
  type: string;
  owners: RemoteRepositoryOwner[];
}
