import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ListResult } from '@app/api/api.types';
import {
  CodeBinding,
  CodeRepoBindingsResponse,
  CodeRepoRelatedResourcesResponse,
  CodeRepoServiceType,
  CodeRepoServicesResponse,
  CodeRepositoriesFindParams,
  CodeRepositoriesResponse,
  CodeRepository,
  CodeService,
  RemoteRepositoriesResponse,
} from '@app/api/code/code-api.types';
import {
  fromCodeRepoBinding,
  mapIntegrateConfigToCodePayload,
  mapResourceToCodeService,
  toCodeRepoBinding,
  toCodeRepository,
} from '@app/api/code/utils';
import { ToolIntegrateParams } from '@app/api/tool-chain/tool-chain-api.types';
import { Pagination } from '@app/types';
import { filterBy, getQuery, pageBy } from '@app/utils/query-builder';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CodeApiService {
  constructor(private http: HttpClient) {}

  findCodeServiceTypes() {
    return forkJoin(
      this.findServices().pipe(
        map(result => result.items),
        map(items => items.filter(item => item.public)),
      ),
      this.http.get('api/v1/settings/devops').pipe(
        map((settings: any) => {
          return (settings.codeRepoServiceTypes as CodeRepoServiceType[]) || [];
        }),
      ),
      /* of([
        {
          type: 'Github',
          name: 'github',
          public: true,
          displayName: {
            zh: 'Github',
            en: 'Github',
          },
          host: 'http://api.github.com',
        }
      ]), */
    ).pipe(
      map(([integrated, items]) => {
        return items.filter(
          item =>
            !item.public ||
            integrated.every(
              service => service.type.toLowerCase() !== item.type.toLowerCase(),
            ),
        );
      }),
    );
  }

  findCodeRepositories({
    name,
    pageIndex,
    itemsPerPage,
    project,
  }: CodeRepositoriesFindParams): Observable<Pagination<CodeRepository>> {
    return this.http
      .get<CodeRepositoriesResponse>('api/v1/coderepository/' + project, {
        params: getQuery(
          filterBy('name', name),
          pageBy(pageIndex, itemsPerPage),
        ),
      })
      .pipe(
        map(res =>
          Object.assign({
            total: res.listMeta.totalItems,
            items: res.coderepositories.map(toCodeRepository),
          }),
        ),
      );
  }

  findCodeRepositoriesByBinding(name: string, namespace: string) {
    return this.http
      .get<CodeRepositoriesResponse>(
        `api/v1/coderepobinding/${namespace}/${name}/repositories`,
      )
      .pipe(map(res => (res.coderepositories || []).map(toCodeRepository)));
  }

  findServices(): Observable<{ items: CodeService[]; length: number }> {
    return this.http.get('api/v1/codereposervice').pipe(
      map((res: CodeRepoServicesResponse) => ({
        items: (res.codereposervices || []).map(mapResourceToCodeService),
        length: res.listMeta.totalItems,
      })),
    );
  }

  findBindings(query: {
    [name: string]: string;
  }): Observable<ListResult<CodeBinding>> {
    return this.http.get('api/v1/coderepobinding', { params: query }).pipe(
      map((res: CodeRepoBindingsResponse) => ({
        items: (res.coderepobindings || []).map(toCodeRepoBinding),
        total: res.listMeta.totalItems,
      })),
    );
  }

  findBindingsByNamespace(
    namespace: string,
    query: { [name: string]: string },
  ): Observable<ListResult<CodeBinding>> {
    return this.http
      .get(`api/v1/coderepobinding/${namespace}`, { params: query })
      .pipe(
        map((res: CodeRepoBindingsResponse) => ({
          items: (res.coderepobindings || []).map(toCodeRepoBinding),
          total: res.listMeta.totalItems,
        })),
      );
  }

  findBindingRelatedResources(name: string, namespace: string) {
    return this.http
      .get(`api/v1/coderepobinding/${namespace}/${name}/resources`)
      .pipe(map((res: CodeRepoRelatedResourcesResponse) => res.items || []));
  }

  findBindingRelatedSecrets(name: string, namespace: string) {
    return this.http
      .get(`api/v1/coderepobinding/${namespace}/${name}/secrets`)
      .pipe(
        map((res: any) =>
          (res.secrets || []).map((item: any) => item.objectMeta.name),
        ),
      );
  }

  findServiceRelatedResources(name: string) {
    return this.http
      .get(`api/v1/codereposervice/${name}/resources`)
      .pipe(map((res: CodeRepoRelatedResourcesResponse) => res.items || []));
  }

  findServiceRelatedSecrets(name: string) {
    return this.http
      .get(`api/v1/codereposervice/${name}/secrets`)
      .pipe(
        map((res: any) =>
          (res.secrets || []).map((item: any) => item.objectMeta.name),
        ),
      );
  }

  getService(name: string): Observable<CodeService> {
    return this.http
      .get(`api/v1/codereposervice/${name}`)
      .pipe(map(mapResourceToCodeService));
  }

  createService(model: ToolIntegrateParams) {
    return this.http.post(
      `api/v1/codereposervice`,
      mapIntegrateConfigToCodePayload(model),
    );
  }

  updateService(model: ToolIntegrateParams) {
    return this.http.put(
      `api/v1/codereposervice/${model.name}`,
      mapIntegrateConfigToCodePayload(model),
    );
  }

  deleteService(name: string) {
    return this.http.delete(`api/v1/codereposervice/${name}`);
  }

  getBinding(name: string, namespace: string) {
    return this.http
      .get(`api/v1/coderepobinding/${namespace}/${name}`)
      .pipe(map(toCodeRepoBinding));
  }

  createBinding(model: CodeBinding) {
    return this.http.post(
      `api/v1/coderepobinding/${model.namespace}`,
      fromCodeRepoBinding(model),
    );
  }

  updateBinding(model: CodeBinding) {
    return this.http.put<CodeRepoRelatedResourcesResponse>(
      `api/v1/coderepobinding/${model.namespace}/${model.name}`,
      fromCodeRepoBinding(model),
    );
  }

  deleteBinding(name: string, namespace: string) {
    return this.http.delete(`api/v1/coderepobinding/${namespace}/${name}`);
  }

  getBindingRemoteRepositories(name: string, namespace: string) {
    return this.http.get<RemoteRepositoriesResponse>(
      `api/v1/coderepobinding/${namespace}/${name}/remote-repositories`,
    );
  }
}
