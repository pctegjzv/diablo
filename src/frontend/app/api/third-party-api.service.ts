import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isArray } from 'lodash';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

export interface ThirdPartyContent {
  enabled: boolean;
  type: string;
  [name: string]:
    | {
        host: string;
      }
    | boolean
    | string;
}

export interface ThirdPartyContentFindResult {
  data: ThirdPartyContent[];
  error: any;
}

@Injectable()
export class ThirdPartyApiService {
  constructor(private http: HttpClient) {}

  all(): Observable<ThirdPartyContentFindResult> {
    return this.http.get('api/v1/thirdparty').pipe(
      map((res: ThirdPartyContent[]) => {
        return {
          data: (res || []).filter(item => item.enabled),
          error: null,
        };
      }),
      catchError((error: any) =>
        of({
          data: [],
          error,
        }),
      ),
    );
  }

  get(type: string): Observable<any[]> {
    return this.http.get(`api/v1/thirdparty/${type}`).pipe(
      map((res: any) => {
        if (!isArray(res)) {
          throw res;
        }

        return res.slice(0, 3);
      }),
    );
  }
}
