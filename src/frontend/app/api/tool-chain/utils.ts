import { CodeService } from '@app/api/code/code-api.types';
import { JenkinsService } from '@app/api/jenkins/jenkins-api.types';
import {
  ToolBinding,
  ToolService,
  ToolType,
} from '@app/api/tool-chain/tool-chain-api.types';
import { ANNOTATION_PREFIX } from '@app/constants';
import { get, startCase } from 'lodash';

export enum ToolKind {
  Jenkins = 'jenkins',
  CodeRepo = 'codereposervice',
  Registry = 'imageregistry',
}

export enum BindingKind {
  Jenkins = 'jenkinsbinding',
  CodeRepo = 'coderepobinding',
  Registry = 'imageregistrybinding',
}

function getToolKindCamelCase(kind: ToolKind) {
  switch (kind) {
    case ToolKind.CodeRepo:
      return 'codeRepoService';
    case ToolKind.Jenkins:
      return 'jenkins';
    case ToolKind.Registry:
      return 'imageRegistry';
  }
}

const ENTERPRISE_KINDS = [ToolKind.CodeRepo];

export function mapToToolType(type: ToolType) {
  return {
    ...type,
    items: (type.items || []).map(item => ({
      ...item,
      type: item.type || item.kind,
      toolType: type.name,
    })),
  };
}

export function mapToToolService(ins: any): ToolService {
  const kind = get(ins, 'typeMeta.kind', '');
  const isPublic = get(ins, 'spec.public', false);
  const isEnterprise = ENTERPRISE_KINDS.includes(kind) && !isPublic;

  return {
    name: get(ins, 'objectMeta.name', ''),
    creationTimestamp: get(ins, 'objectMeta.creationTimestamp', ''),
    toolType: get(
      ins,
      ['objectMeta', 'annotations', `${ANNOTATION_PREFIX}/toolType`],
      '',
    ),
    kind: kind,
    type: get(ins, 'spec.type', startCase(kind)),
    host: get(ins, 'spec.http.host', ''),
    html: get(ins, 'spec.http.html', ''),
    public: isPublic,
    enterprise: isEnterprise,
    status: {
      phase: get(ins, 'status.phase', '') || get(ins, 'status.status', ''),
      message: get(ins, 'status.message', ''),
    },
    __original: ins,
  };
}

export function buildResourceServiceToToolServiceMapper(kind: ToolKind) {
  return (data: JenkinsService | CodeService): ToolService => {
    const isPublic = get(data, 'public', false);
    const serviceType = get(data, 'type', startCase(kind));
    return {
      ...data,
      kind,
      type: serviceType,
      public: isPublic,
      enterprise: ENTERPRISE_KINDS.includes(kind) ? !isPublic : false,
    };
  };
}

export function mapResourceToToolBinding(res: any): ToolBinding {
  const kind = get(res, 'typeMeta.kind', '');
  const toolKind = get(
    res,
    ['objectMeta', 'annotations', `${ANNOTATION_PREFIX}/toolItemKind`],
    '',
  );
  const toolIsPublic =
    get(
      res,
      ['objectMeta', 'annotations', `${ANNOTATION_PREFIX}/toolItemPublic`],
      'false',
    ) === 'true';
  const toolIsEnterprise = ENTERPRISE_KINDS.includes(toolKind) && !toolIsPublic;

  return {
    name: get(res, 'objectMeta.name', ''),
    namespace: get(res, 'objectMeta.namespace', ''),
    description: get(
      res,
      ['objectMeta', 'annotations', `${ANNOTATION_PREFIX}/description`],
      '',
    ),
    kind: kind,
    tool: {
      toolType: get(
        res,
        ['objectMeta', 'annotations', `${ANNOTATION_PREFIX}/toolType`],
        '',
      ),
      type:
        get(
          res,
          ['objectMeta', 'annotations', `${ANNOTATION_PREFIX}/toolItemType`],
          '',
        ) || startCase(toolKind),
      kind: toolKind,
      public: toolIsPublic,
      enterprise: toolIsEnterprise,
    },
    creationTimestamp: get(res, 'objectMeta.creationTimestamp', ''),
    secret:
      get(res, 'spec.account.secret.name', '') ||
      get(res, 'spec.secret.name', ''),
    service: get(res, ['spec', getToolKindCamelCase(toolKind), 'name'], ''),
    status: {
      phase: get(res, 'status.phase', '') || get(res, 'status.status', ''),
      message: get(res, 'status.message', ''),
    },
    __original: res,
  };
}
