import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ListResult } from '@app/api/api.types';
import { CodeApiService } from '@app/api/code/code-api.service';
import { JenkinsApiService } from '@app/api/jenkins/jenkins-api.service';
import { RegistryApiService } from '@app/api/registry/registry-api.service';
import {
  ToolBinding,
  ToolIntegrateParams,
  ToolService,
  ToolType,
} from '@app/api/tool-chain/tool-chain-api.types';
import {
  ToolKind,
  buildResourceServiceToToolServiceMapper,
  mapResourceToToolBinding,
  mapToToolService,
  mapToToolType,
} from '@app/api/tool-chain/utils';
import { filterBy, getQuery } from '@app/utils/query-builder';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const TOOL_CHAINS_URL = 'api/v1/settings/devops';
const TOOL_SERVICES_URL = 'api/v1/toolchain';
const PROJECT_TOOL_BINDING_URL = 'api/v1/toolchain/bindings';

@Injectable()
export class ToolChainApiService {
  constructor(
    private http: HttpClient,
    private jenkinsApi: JenkinsApiService,
    private codeRepoApi: CodeApiService,
    private registryApi: RegistryApiService,
  ) {}

  getToolChains(): Observable<ToolType[]> {
    return this.http
      .get(TOOL_CHAINS_URL)
      .pipe(map((res: any) => (res.toolChains || []).map(mapToToolType)));
  }

  getToolServices(type = ''): Observable<ToolService[]> {
    return this.http
      .get(TOOL_SERVICES_URL, {
        params: {
          tool_type: type,
        },
      })
      .pipe(map((res: any) => (res.items || []).map(mapToToolService)));
  }

  getToolService(kind: ToolKind, name: string): Observable<ToolService> {
    return this.getServiceApi(kind)(name).pipe(
      map(buildResourceServiceToToolServiceMapper(kind)),
    );
  }

  integrateTool(kind: ToolKind, data: ToolIntegrateParams) {
    return this.getIntegrateApi(kind)(data);
  }

  updateTool(kind: ToolKind, config: ToolIntegrateParams) {
    return this.getUpdateApi(kind)(config);
  }

  deleteTool(kind: ToolKind, name: string) {
    return this.getDeleteApi(kind)(name);
  }

  getBindingsByToolKind(
    kind: ToolKind,
    name: string,
  ): Observable<ToolBinding[]> {
    return this.getBindingsApi(kind)(name).pipe(
      map(res =>
        res.items.map((item: any) => mapResourceToToolBinding(item.__original)),
      ),
    );
  }

  getBindingsByProject(
    namespace: string,
    type: string = '',
  ): Observable<ToolBinding[]> {
    return this.http
      .get(`${PROJECT_TOOL_BINDING_URL}/${namespace}`, {
        params: {
          tool_type: type,
        },
      })
      .pipe(map((res: any) => (res.items || []).map(mapResourceToToolBinding)));
  }

  private getIntegrateApi(kind: ToolKind) {
    switch (kind) {
      case ToolKind.Jenkins:
        return this.jenkinsApi.createService.bind(this.jenkinsApi);
      case ToolKind.CodeRepo:
        return this.codeRepoApi.createService.bind(this.codeRepoApi);
      case ToolKind.Registry:
        return this.registryApi.createService.bind(this.registryApi);
    }
  }

  private getServiceApi(kind: ToolKind) {
    switch (kind) {
      case ToolKind.Jenkins:
        return this.jenkinsApi.getService.bind(this.jenkinsApi);
      case ToolKind.CodeRepo:
        return this.codeRepoApi.getService.bind(this.codeRepoApi);
      case ToolKind.Registry:
        return this.registryApi.getService.bind(this.registryApi);
    }
  }

  private getUpdateApi(kind: ToolKind) {
    switch (kind) {
      case ToolKind.Jenkins:
        return this.jenkinsApi.updateService.bind(this.jenkinsApi);
      case ToolKind.CodeRepo:
        return this.codeRepoApi.updateService.bind(this.codeRepoApi);
      case ToolKind.Registry:
        return this.registryApi.updateService.bind(this.registryApi);
    }
  }

  private getDeleteApi(kind: ToolKind) {
    switch (kind) {
      case ToolKind.Jenkins:
        return this.jenkinsApi.deleteService.bind(this.jenkinsApi);
      case ToolKind.CodeRepo:
        return this.codeRepoApi.deleteService.bind(this.codeRepoApi);
      case ToolKind.Registry:
        return this.registryApi.deleteService.bind(this.codeRepoApi);
    }
  }

  private getBindingsApi(
    kind: ToolKind,
  ): (name: string) => Observable<ListResult<any>> {
    switch (kind) {
      case ToolKind.Jenkins:
        return (name: string) =>
          this.jenkinsApi.findBindings(getQuery(filterBy('jenkins', name)));
      case ToolKind.CodeRepo:
        return (name: string) =>
          this.codeRepoApi.findBindings(
            getQuery(filterBy('label', `codeRepoService:${name}`)),
          );
      case ToolKind.Registry:
        return (name: string) =>
          this.registryApi.findBindings(
            getQuery(filterBy('label', `imageRegistry:${name}`)),
          );
    }
  }
}
