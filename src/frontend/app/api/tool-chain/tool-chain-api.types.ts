import {
  K8SResource,
  ResourceBinding,
  ResourceStatus,
} from '@app/api/api.types';
import { BindingKind, ToolKind } from '@app/api/tool-chain/utils';

export interface ToolType {
  name: string;
  displayName: { [key: string]: string };
  enabled: boolean;
  items: Tool[];
}

export interface Tool {
  name: string;
  displayName: { [key: string]: string };
  toolType: string;
  kind: ToolKind;
  type: string;
  host: string;
  html: string;
  public: boolean;
  enterprise: boolean;
  enabled: boolean;
}

export interface ToolService {
  name: string;
  creationTimestamp: string;
  toolType?: string;
  kind: ToolKind;
  type: string;
  host: string;
  html: string;
  public: boolean;
  enterprise: boolean;
  status: ResourceStatus;
  __original: K8SResource;
  [key: string]: any;
}

export interface ToolIntegrateParams {
  name: string;
  host: string;
  type?: string;
  public?: boolean;
}

export interface ToolBinding extends ResourceBinding {
  kind: BindingKind;
  tool: {
    toolType: string;
    type: string;
    kind: ToolKind;
    public: boolean;
    enterprise: boolean;
  };
}
