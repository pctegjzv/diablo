export interface K8SResource {
  apiVersion: string;
  kind: string;
  metadata: {
    name: string;
    namespace?: string;
    description?: string;
    labels?: { [key: string]: string };
    annotations?: { [key: string]: string };
  };
  spec: {
    [key: string]: any;
  };
  status?: {
    phase: string;
    message: string;
  };
}

export interface ResourceService {
  name: string;
  creationTimestamp: string;
  host: string;
  html: string;
  status: ResourceStatus;
  __original: K8SResource;
}

export interface ResourceStatus {
  phase: string;
  message: string;
}

export interface ResourceBinding {
  name: string;
  namespace: string;
  creationTimestamp: string;
  description: string;
  secret: string;
  service: string;
  status: ResourceStatus;
  __original?: K8SResource;
}

export interface BindingParams {
  name: string;
  namespace: string;
  secret: string;
  service: string;
  description: string;
}

export interface ListResult<T> {
  total: number;
  items: Array<T>;
}
