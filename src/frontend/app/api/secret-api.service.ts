import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_PRODUCT,
  PRODUCT_NAME,
} from '@app/constants';
import { head, mapValues } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export enum SecretTypes {
  basicAuth = 'kubernetes.io/basic-auth',
  opaque = 'Opaque',
  dockerConfig = 'kubernetes.io/dockerconfigjson',
  oauth2 = 'devops.alauda.io/oauth2',
}

export interface SecretIdentity {
  name: string;
  namespace: string;
}

export interface Secret extends SecretIdentity {
  displayName: string;
  description: string;
  type: SecretTypes;
  username?: string;
  password?: string;
  clientID?: string;
  clientSecret?: string;
  generic?: { [name: string]: any };
  dockerAddress?: string;
  dockerUsername?: string;
  dockerPassword?: string;
  dockerEmail?: string;
  ownerReferences: any[];
  accessToken?: string;
  __orignal?: any;
}

@Injectable()
export class SecretApiService {
  constructor(private http: HttpClient) {}

  find(
    query: { [name: string]: string },
    namespace = '',
  ): Observable<{ items: Secret[]; length: number }> {
    const url = namespace ? `api/v1/secret/${namespace}` : 'api/v1/secret';

    return this.http.get(url, { params: query }).pipe(
      map((res: any) => ({
        items: (res.secrets || []).map(toModel),
        length: res.listMeta.totalItems,
      })),
    );
  }

  get(identity: SecretIdentity) {
    return this.http
      .get(`api/v1/secret/${identity.namespace}/${identity.name}`)
      .pipe(map(toModel));
  }

  post(data: Secret) {
    return this.http.post(`api/v1/others`, [toResource(data)]).pipe(
      map((result: any) => {
        if (result && result.failed_resource_count > 0) {
          throw head(result.create_messages || []) || { message: 'unknown' };
        }
        return result;
      }),
    );
  }

  put(data: Secret) {
    return this.http
      .put(`api/v1/secret/${data.namespace}/${data.name}`, toResource(data))
      .pipe(map((result: any) => result));
  }
}

function toModel(resource: any): Secret {
  const metadata = resource.metadata || resource.objectMeta || {};
  const annotations = metadata.annotations || {};
  const data = resource.data || null;

  const secretFields: {
    [key: string]: string;
  } =
    resource.type !== SecretTypes.dockerConfig
      ? mapValues(data || {}, value => atob(value))
      : data
        ? parseDockerJson(JSON.parse(atob(data['.dockerconfigjson'])))
        : {};

  const secretProperties =
    resource.type === SecretTypes.opaque
      ? {
          generic: Object.keys(secretFields).map(key => ({
            key,
            value: secretFields[key],
          })),
        }
      : secretFields;

  return {
    name: metadata.name,
    namespace: metadata.namespace,
    ownerReferences: metadata.ownerReferences || [],
    displayName: annotations[ANNOTATION_DISPLAY_NAME],
    description: annotations[ANNOTATION_DESCRIPTION],
    type: resource.type,
    ...secretProperties,
    __orignal: resource,
  };
}

function parseDockerJson(data: any) {
  const dockerAddress = head(Object.keys(data.auths || {}));
  return {
    dockerAddress,
    dockerUsername: (dockerAddress && data.auths[dockerAddress].username) || '',
    dockerPassword: (dockerAddress && data.auths[dockerAddress].password) || '',
    dockerEmail: (dockerAddress && data.auths[dockerAddress].email) || '',
  };
}

function toResource(model: Secret): any {
  const orignal = model.__orignal || {};

  const data =
    model.type === SecretTypes.basicAuth
      ? {
          username: btoa(model.username),
          password: btoa(model.password),
        }
      : model.type === SecretTypes.oauth2
        ? {
            ...(orignal.data || {}),
            clientID: btoa(model.clientID),
            clientSecret: btoa(model.clientSecret),
          }
        : model.type === SecretTypes.dockerConfig
          ? {
              '.dockerconfigjson': btoa(
                JSON.stringify({
                  auths: {
                    [model.dockerAddress]: {
                      username: model.dockerUsername,
                      password: model.dockerPassword,
                      email: model.dockerEmail,
                      auth: btoa(
                        `${model.dockerUsername}:${model.dockerPassword}`,
                      ),
                    },
                  },
                }),
              ),
            }
          : (model.generic || []).reduce((accum: any, pair: any) => {
              return {
                ...accum,
                [pair.key]: btoa(pair.value),
              };
            }, {});

  return {
    apiVersion: 'v1',
    kind: 'Secret',
    metadata: {
      name: model.name,
      annotations: {
        [ANNOTATION_DISPLAY_NAME]: model.displayName,
        [ANNOTATION_DESCRIPTION]: model.description,
        [ANNOTATION_PRODUCT]: PRODUCT_NAME,
      },
      namespace: model.namespace,
    },
    type: model.type,
    data,
  };
}
