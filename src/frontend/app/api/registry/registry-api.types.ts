import {
  K8SResource,
  ResourceBinding,
  ResourceService,
  ResourceStatus,
} from '@app/api/api.types';

export interface RegistryService extends ResourceService {
  type: string;
}

export interface RegistryBinding extends ResourceBinding {
  repositories: string[];
}

export interface Repository {
  name: string;
  namespace: string;
  creationTimestamp: string;
  image: string;
  tags: ImageTag[];
  status: ResourceStatus;
  __original: K8SResource;
}

export interface ImageTag {
  name: string;
  digest: string;
  createdAt: string;
}
