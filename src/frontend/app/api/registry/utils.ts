import { BindingParams, K8SResource, ListResult } from '@app/api/api.types';
import {
  ImageTag,
  RegistryBinding,
  RegistryService,
  Repository,
} from '@app/api/registry/registry-api.types';
import { ToolIntegrateParams } from '@app/api/tool-chain/tool-chain-api.types';
import { ANNOTATION_DESCRIPTION, API_GROUP_VERSION } from '@app/constants';
import { cloneDeep, get, set } from 'lodash';

export function mapIntegrateConfigToK8SResource(
  data: ToolIntegrateParams,
): K8SResource {
  return {
    apiVersion: API_GROUP_VERSION,
    kind: 'ImageRegistry',
    metadata: {
      name: data.name,
    },
    spec: {
      http: {
        host: data.host,
      },
      type: data.type,
    },
  };
}

export function mapResourceToRegistryService(
  resource: K8SResource,
): RegistryService {
  return {
    name: get(resource, 'metadata.name', ''),
    creationTimestamp: get(resource, 'metadata.creationTimestamp', ''),
    type: get(resource, 'spec.type', ''),
    host: get(resource, 'spec.http.host', ''),
    html: get(resource, 'spec.http.html', ''),
    status: {
      phase: get(resource, 'status.phase', ''),
      message: get(resource, 'status.message', ''),
    },
    __original: resource,
  };
}

export function mapResourceToRegistryBinding(
  resource: K8SResource,
): RegistryBinding {
  return {
    name: get(resource, 'metadata.name', ''),
    namespace: get(resource, 'metadata.namespace', ''),
    description: get(
      resource,
      ['metadata', 'annotations', ANNOTATION_DESCRIPTION],
      '',
    ),
    creationTimestamp: get(resource, 'metadata.creationTimestamp', ''),
    secret: get(resource, 'spec.secret.name', ''),
    service: get(resource, 'spec.imageRegistry.name', ''),
    repositories: get(resource, 'spec.repoInfo.repositories') || [],
    status: {
      phase: get(resource, 'status.phase', ''),
      message: get(resource, 'status.message', ''),
    },
    __original: resource,
  };
}

export function mapBindingParamsToK8SResource(
  model: BindingParams,
): K8SResource {
  return {
    apiVersion: API_GROUP_VERSION,
    kind: 'ImageRegistryBinding',
    metadata: {
      name: model.name,
      namespace: model.namespace,
      annotations: {
        [ANNOTATION_DESCRIPTION]: model.description,
      },
      labels: {
        imageRegistry: model.service,
      },
    },
    spec: {
      imageRegistry: {
        name: model.service,
      },
      secret: {
        name: model.secret,
        usernameKey: 'username',
        apiTokenKey: 'password',
      },
    },
  };
}

export function mapRegsitryBindingToK8SResource(
  binding: RegistryBinding,
): K8SResource {
  const result = cloneDeep(binding.__original);
  set(result, 'metadata.name', binding.name);
  set(result, 'metadata.namespace', binding.namespace);
  set(
    result,
    ['metadata', 'annotations', ANNOTATION_DESCRIPTION],
    binding.description,
  );
  set(result, 'metadata.labels.imageRegistry', binding.service);
  set(result, 'spec.imageRegistry.name', binding.service);
  set(result, 'spec.secret.name', binding.secret);
  set(result, 'spec.repoInfo.repositories', binding.repositories);
  return result;
}

export function mapResourceToRepository(res: K8SResource): Repository {
  return {
    name: get(res, 'objectMeta.name', ''),
    namespace: get(res, 'objectMeta.namespace', ''),
    creationTimestamp: get(res, 'objectMeta.creationTimestamp', ''),
    image: get(res, 'spec.image', ''),
    status: {
      phase: get(res, 'status.phase', ''),
      message: get(res, 'status.message', ''),
    },
    tags: (get(res, 'status.tags') || []).map(mapDataToRepoTag),
    __original: res,
  };
}

export function mapDataToRepoTag(data: any): ImageTag {
  return {
    name: data.name,
    digest: data.digest,
    createdAt: data.created_at,
  };
}

export function mapFindBindingResponseToList(
  res: any,
): ListResult<RegistryBinding> {
  return {
    total: get(res, 'listMeta.totalItems', 0),
    items: res.imageregistrybindings.map(mapResourceToRegistryBinding),
  };
}

export function mapFindRepositoriesResponseToList(
  res: any,
): ListResult<Repository> {
  return {
    total: get(res, 'listMeta.totalItems', 0),
    items: res.imagerepositories.map(mapResourceToRepository),
  };
}
