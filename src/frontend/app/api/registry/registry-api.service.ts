import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BindingParams, ListResult } from '@app/api/api.types';
import {
  ImageTag,
  RegistryBinding,
  RegistryService,
  Repository,
} from '@app/api/registry/registry-api.types';
import {
  mapBindingParamsToK8SResource,
  mapDataToRepoTag,
  mapFindBindingResponseToList,
  mapFindRepositoriesResponseToList,
  mapIntegrateConfigToK8SResource,
  mapRegsitryBindingToK8SResource,
  mapResourceToRegistryBinding,
  mapResourceToRegistryService,
  mapResourceToRepository,
} from '@app/api/registry/utils';
import { ToolIntegrateParams } from '@app/api/tool-chain/tool-chain-api.types';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const REGISTRY_URL = 'api/v1/imageregistry';
const REGISTRY_BINDING_URL = 'api/v1/imageregistrybinding';
const REPOSITORIES_URL = 'api/v1/imagerepository';

@Injectable()
export class RegistryApiService {
  constructor(private http: HttpClient) {}

  createService(params: ToolIntegrateParams) {
    return this.http.post(
      REGISTRY_URL,
      mapIntegrateConfigToK8SResource(params),
    );
  }

  getService(name: string): Observable<RegistryService> {
    return this.http
      .get(`${REGISTRY_URL}/${name}`)
      .pipe(map(mapResourceToRegistryService));
  }

  updateService(data: ToolIntegrateParams) {
    return this.http.put(
      `${REGISTRY_URL}/${data.name}`,
      mapIntegrateConfigToK8SResource(data),
    );
  }

  deleteService(name: string) {
    return this.http.delete(`${REGISTRY_URL}/${name}`);
  }

  createBinding(params: BindingParams): Observable<RegistryBinding> {
    return this.http
      .post(
        `${REGISTRY_BINDING_URL}/${params.namespace}`,
        mapBindingParamsToK8SResource(params),
      )
      .pipe(map(mapResourceToRegistryBinding));
  }

  getBinding(namespace: string, name: string): Observable<RegistryBinding> {
    return this.http
      .get(`${REGISTRY_BINDING_URL}/${namespace}/${name}`)
      .pipe(map(mapResourceToRegistryBinding));
  }

  updateBinding(params: RegistryBinding) {
    return this.http.put(
      `${REGISTRY_BINDING_URL}/${params.namespace}/${params.name}`,
      mapRegsitryBindingToK8SResource(params),
    );
  }

  deleteBinding(namespace: string, name: string) {
    return this.http.delete(`${REGISTRY_BINDING_URL}/${namespace}/${name}`);
  }

  findBindings(query: {
    [key: string]: string;
  }): Observable<ListResult<RegistryBinding>> {
    return this.http
      .get(REGISTRY_BINDING_URL, { params: query })
      .pipe(map(mapFindBindingResponseToList));
  }

  findBindingsByNamespace(
    namespace: string = '',
    query: { [key: string]: string },
  ): Observable<ListResult<RegistryBinding>> {
    return this.http
      .get(`${REGISTRY_BINDING_URL}/${namespace}`, { params: query })
      .pipe(map(mapFindBindingResponseToList));
  }

  getAllRemoteRepositoriesByRegistryBinding(
    namespace: string,
    name: string,
  ): Observable<string[]> {
    return this.http
      .get(`${REGISTRY_BINDING_URL}/${namespace}/${name}/remote-repositories`)
      .pipe(map((res: { items: string[] }) => res.items));
  }

  getRepositoriesByRegistryBinding(
    namespace: string,
    name: string,
  ): Observable<ListResult<Repository>> {
    return this.http
      .get(`${REGISTRY_BINDING_URL}/${namespace}/${name}/repositories`)
      .pipe(map(mapFindRepositoriesResponseToList));
  }

  getRepository(namespace: string, name: string): Observable<Repository> {
    return this.http
      .get(`${REPOSITORIES_URL}/${namespace}/${name}`)
      .pipe(map(mapResourceToRepository));
  }

  findRepositoriesByNamespace(
    namespace: string,
    query: { [key: string]: string },
  ): Observable<ListResult<Repository>> {
    return this.http
      .get(`${REPOSITORIES_URL}/${namespace}`, { params: query })
      .pipe(map(mapFindRepositoriesResponseToList));
  }

  findRepositoryTags(
    namespace: string,
    repo: string,
    query: { [key: string]: string },
  ): Observable<ListResult<ImageTag>> {
    return this.http
      .get(`${REPOSITORIES_URL}/${namespace}/${repo}/tags`, {
        params: query,
      })
      .pipe(
        map((res: any) => ({
          total: res.listMeta.totalItems,
          items: res.tags.map(mapDataToRepoTag),
        })),
      );
  }
}
