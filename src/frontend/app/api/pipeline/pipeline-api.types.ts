export interface PipelineParams {
  sortBy?: string;
  filterBy?: string;
  page?: string;
  itemsPerPage?: string;
}

export interface PipelineConfigListResponse {
  total: number;
  items: PipelineConfig[];
}

export interface PipelineTemplateListResponse {
  listMeta: { totalItems: number };
  pipelinetemplates?: PipelineTemplateResponse[];
  clusterpipelinetemplates?: PipelineTemplateResponse[];
}

export interface PipelineIdentity {
  name: string;
  namespace: string;
}

export interface PipelineHistoryCause {
  type: 'manual' | 'cron' | 'codeChange';
  message: string;
}

export interface PipelineHistoryStrategy {
  jenkins: { jenkinsfile?: string; jenkinsfilePath?: string };
  template?: string;
}

export interface PipelineRepositorySource {
  git: {
    ref: string;
    uri: string;
  };
  path?: string;
  branch?: string;
  secret?: string;
  codeRepository: { name: string; ref: string };
}

export interface PipelineConfig extends PipelineIdentity {
  displayName: string;
  application?: string;
  createdAt: string;
  histories: PipelineHistory[];
  jenkinsInstance: string;
  codeRepository: string;
  strategy: PipelineHistoryStrategy;
  source: PipelineRepositorySource;
  triggers: PipelineTrigger[];
  __original: any;
}

export interface PipelineTrigger {
  type: string;
  enabled: boolean;
  rule: string;
}

export interface PipelineHistory {
  name: string;
  createdAt: string;
  namespace: string;
  cause?: PipelineHistoryCause;
  status: {
    [key: string]: any;
  };
  jenkins: {
    [key: string]: any;
  };
  __original: any;
}

export interface PipelineHistoryLog {
  more: boolean;
  text: string;
  nextStart: number;
}

export interface PipelineHistoryStep {
  id: string;
  type: string;
  displayDescription: string;
  displayName: string;
  durationInMillis: number;
  input: any;
  result: string;
  state: string;
  startTime: string;
  edges: any;
  text: string;
}

export interface PipelineConfigTriggerResponse {
  codeChange?: {
    enabled: boolean;
    periodicCheck: string;
  };
  cron?: {
    enabled: boolean;
    rule: string;
  };
  type: string;
}

export interface PipelineConfigResponse {
  objectMeta: {
    name: string;
    annotations?: any;
    namespace: string;
    labels?: any;
    displayName?: string;
  };
  spec: {
    jenkinsBinding: {
      name: string;
    };
    runPolicy: string;
    source?: {
      git: {
        ref: string;
        uri: string;
      };
      secret: {
        name: string;
      };
    };
    strategy?: {
      jenkins: {
        jenkinsfile: string;
        jenkinsfilePath: string;
      };
    };
    triggers: PipelineConfigTriggerResponse[];
  };
  typeMeta: {
    kind: string;
  };
}

export interface PipelineConfigModel {
  basic: {
    name: string;
    display_name: string;
    jenkins_instance: string;
    app: string;
    source: string;
  };
  jenkinsfile: {
    repo: { repo: string; secret: string };
    branch: string;
    path: string;
    secret?: string;
  };
  editor_script: {
    script: string;
  };
  triggers: {
    enabled: boolean;
    cron_string: string;
  }[];
  __original: any;
}

export interface PipelineTemplateSyncCondition {
  lastTransitionTime: string;
  lastUpdateTime: string;
  message: string;
  name: string;
  reason: string;
  status: string;
  target: string;
  type: string;
  previousVersion: string;
  version: string;
}

export interface PipelineTemplateSyncStatus {
  commitID?: string;
  conditions?: PipelineTemplateSyncCondition[];
  endTime?: string;
  message?: string;
  phase?: string;
  startTime?: string;
}

export interface PipelineTemplateSyncResponse {
  metadata: {
    [key: string]: any;
  };
  spec: {
    source?: {
      codeRepository?: { name: string; ref: string };
      git?: { ref: string; uri: string };
      secret?: { name: string };
    };
  };
  status?: PipelineTemplateSyncStatus;
}

export interface PipelineTemplateSyncConfig {
  codeRepository?: { name: string; ref: string };
  git?: { ref: string; uri: string };
  secret?: { name: string };
  status?: {
    phase: 'Draft' | 'Pending' | 'Error' | 'Ready';
  };
}

export interface PipelineTemplateSync {
  name: string;
  codeRepository?: { name: string; ref: string };
  git?: { ref: string; uri: string };
  secret?: { name: string };
  status: PipelineTemplateSyncStatus;
  __original: any;
}

export interface PipelineTemplateStage {
  kind: string;
  name: string;
  tasks: [
    {
      agent: {
        label: string;
      };
      approve: {
        message: string;
        timeout: number;
      };
      environments: [
        {
          name: string;
          value: string;
        }
      ];
      kind: string;
      name: string;
      options: {
        timeout: number;
      };
      type: string;
    }
  ];
}

export interface PipelineTemplateResponse {
  metadata: {
    [key: string]: any;
  };
  spec: {
    agent: {
      label: string;
    };
    arguments: [
      {
        displayName: {
          en: string;
          'zh-CN': string;
        };
        items: TemplateArgumentItem[];
      }
    ];
    engine: string;
    parameters: [
      {
        description: string;
        name: string;
        type: string;
        value: string;
      }
    ];
    stages: PipelineTemplateStage[];
    withSCM: boolean;
  };
}

export interface PipelineTemplate {
  name: string;
  labels?: { key: string; value: string }[];
  description: { en: string; 'zh-CN': string };
  version: string;
  arguments: TemplateArgumentField[];
  stages: PipelineTemplateStage[];
  withSCM: boolean;
  __original: any;
}

export interface TemplateBasicDescription {
  'zh-CN': string;
  en: string;
}

export interface TemplateArgumentItem {
  name: string;
  schema: { type: string };
  binding: string[];
  display: {
    type: string;
    name: TemplateBasicDescription;
  };
  description: TemplateBasicDescription;
  required: boolean;
  default?: any;
  validation?: any;
  relation?: any;
  value?: any;
}

export interface TemplateArgumentField {
  displayName: TemplateBasicDescription;
  items: TemplateArgumentItem[];
}
