import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  PipelineConfig,
  PipelineConfigListResponse,
  PipelineConfigModel,
  PipelineHistoryStep,
  PipelineParams,
  PipelineTemplate,
  PipelineTemplateListResponse,
  PipelineTemplateResponse,
  PipelineTemplateSync,
  PipelineTemplateSyncConfig,
  PipelineTemplateSyncResponse,
} from '@app/api/pipeline/pipeline-api.types';
import {
  fromPipelineConfig,
  fromPipelineTemplate,
  fromPipelineTemplateSync,
  mapPipelineHistoryResponse,
  mapPipelineResponse,
  mapPipelinesResponse,
  toPipelineConfig,
  toPipelineTemplateSync,
} from '@app/api/pipeline/utils';
import { get, head } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class PipelineApiService {
  static readonly PIPELINE_CONFIG_URL = 'api/v1/pipelineconfig';
  static readonly PIPELINE_URL = 'api/v1/pipeline';
  static readonly PIPELINE_TEMPLATE_SYNC_URL = 'api/v1/pipelinetemplatesync';
  static readonly PIPELINE_TEMPLATE_URL = 'api/v1/pipelinetemplate';
  static readonly PIPELINE_CLUSTER_TEMPLATE_URL =
    'api/v1/clusterpipelinetemplate';

  constructor(private http: HttpClient) {}

  findPipelineConfigs(
    project: string,
    params: PipelineParams,
  ): Observable<PipelineConfigListResponse> {
    return this.http
      .get(`${PipelineApiService.PIPELINE_CONFIG_URL}/${project}`, {
        params: {
          ...params,
        },
      })
      .pipe(
        map((res: any) => ({
          total: get(res, 'listMeta.totalItems', 0) as number,
          items: mapPipelinesResponse(res.pipelineconfigs),
        })),
      );
  }

  getPipelineConfig(project: string, name: string): Observable<PipelineConfig> {
    return this.http
      .get(`${PipelineApiService.PIPELINE_CONFIG_URL}/${project}/${name}`)
      .pipe(map(mapPipelineResponse));
  }

  deletePipelineConfig(namespace: string, name: string) {
    return this.http.delete(
      `${PipelineApiService.PIPELINE_CONFIG_URL}/${namespace}/${name}`,
    );
  }

  createPipelineConfig(namespace: string, data: PipelineConfigModel) {
    return this.http.post(
      `${PipelineApiService.PIPELINE_CONFIG_URL}/${namespace}`,
      toPipelineConfig(data, namespace),
    );
  }

  getPipelineConfigToModel(namespace: string, name: string) {
    return this.http
      .get(`${PipelineApiService.PIPELINE_CONFIG_URL}/${namespace}/${name}`)
      .pipe(map((response: any) => fromPipelineConfig(response)));
  }

  updatePipelineConfig({
    namespace,
    name,
    data,
  }: {
    namespace: string;
    name: string;
    data: PipelineConfigModel;
  }) {
    return this.http
      .put(
        `${PipelineApiService.PIPELINE_CONFIG_URL}/${namespace}/${name}`,
        toPipelineConfig(data, namespace),
      )
      .pipe(map((response: any) => fromPipelineConfig(response)));
  }

  getPipelineHistorySteps(namespace: string, name: string, stageId?: string) {
    return this.http.get<{ tasks: PipelineHistoryStep[] }>(
      `${PipelineApiService.PIPELINE_URL}/${namespace}/${name}/tasks`,
      {
        params: {
          stage: stageId,
        },
      },
    );
  }

  triggerPipeline(namespace: string, name: string) {
    return this.http.post(
      `${PipelineApiService.PIPELINE_CONFIG_URL}/${namespace}/${name}/trigger`,
      {},
    );
  }

  getPipelineHistoryLog(
    namespace: string,
    name: string,
    params: { start?: number } = {
      start: 0,
    },
  ) {
    return this.http.get(
      `${PipelineApiService.PIPELINE_URL}/${namespace}/${name}/logs`,
      {
        params: {
          start: `${params.start}`,
        },
      },
    );
  }

  getPipelineHistoryStepLog(
    namespace: string,
    name: string,
    params: { start?: number; stage?: string; step?: string } = {
      start: 0,
      stage: '',
      step: '',
    },
  ) {
    return this.http.get(
      `${PipelineApiService.PIPELINE_URL}/${namespace}/${name}/logs`,
      {
        params: {
          start: `${params.start}`,
          stage: params.stage,
          step: params.step,
        },
      },
    );
  }

  getPipelineHistories(namespace: string, params?: any) {
    return this.http
      .get(`${PipelineApiService.PIPELINE_URL}/${namespace}`, {
        params: params,
      })
      .pipe(
        map((response: any) => ({
          total: response.listMeta.totalItems,
          histories: response.pipelines.map(mapPipelineHistoryResponse),
        })),
      );
  }

  deletePipeline(namespace: string, name: string) {
    return this.http.delete(
      `${PipelineApiService.PIPELINE_URL}/${namespace}/${name}`,
    );
  }

  abortPipeline(namespace: string, name: string) {
    return this.http.put(
      `${PipelineApiService.PIPELINE_URL}/${namespace}/${name}/abort`,
      null,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }

  retryPipeline(
    namespace: string,
    name: string,
  ): Observable<PipelineTemplateSync> {
    return this.http
      .post<PipelineTemplateSyncResponse>(
        `${PipelineApiService.PIPELINE_URL}/${namespace}/${name}/retry`,
        null,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      )
      .pipe(map(mapPipelineHistoryResponse));
  }

  templateSetting(
    namespace: string,
    data: PipelineTemplateSyncConfig,
  ): Observable<PipelineTemplateSync> {
    return this.http
      .post<PipelineTemplateSyncResponse>(
        `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}/${namespace}`,
        toPipelineTemplateSync(data),
      )
      .pipe(map(fromPipelineTemplateSync));
  }

  updateTemplateSetting(
    namespace: string,
    name: string,
    data: PipelineTemplateSyncConfig,
  ): Observable<PipelineTemplateSync> {
    return this.http
      .put<PipelineTemplateSyncResponse>(
        `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}/${namespace}/${name}`,
        toPipelineTemplateSync(data),
      )
      .pipe(map(fromPipelineTemplateSync));
  }

  templateSyncTrigger(
    namespace: string,
    name: string,
    data: PipelineTemplateSync,
  ) {
    return this.http
      .put(
        `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}/${namespace}/${name}`,
        data,
      )
      .pipe(map(fromPipelineTemplateSync));
  }

  templateSyncDetail(namespace: string): Observable<PipelineTemplateSync> {
    return this.http
      .get<PipelineTemplateSyncResponse>(
        `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}/${namespace}`,
      )
      .pipe(
        map((result: any) => head(result.pipelinetemplatesyncs)),
        map(fromPipelineTemplateSync),
      );
  }

  templateList(
    namespace: string,
    query?: { [key: string]: string },
  ): Observable<PipelineTemplate[]> {
    return this.http
      .get<PipelineTemplateListResponse>(
        `${PipelineApiService.PIPELINE_TEMPLATE_URL}/${namespace}`,
        {
          params: query,
        },
      )
      .pipe(
        map((result: PipelineTemplateListResponse) =>
          result.pipelinetemplates.map(fromPipelineTemplate),
        ),
      );
  }

  templateDetail(
    namespace: string,
    name: string,
  ): Observable<PipelineTemplate> {
    return this.http
      .get<PipelineTemplateResponse>(
        `${PipelineApiService.PIPELINE_TEMPLATE_URL}/${namespace}/${name}`,
      )
      .pipe(map(fromPipelineTemplate));
  }

  clusterTemplateList(query?: any): Observable<PipelineTemplate[]> {
    return this.http
      .get<PipelineTemplateListResponse>(
        `${PipelineApiService.PIPELINE_CLUSTER_TEMPLATE_URL}`,
        {
          params: query,
        },
      )
      .pipe(
        map((result: PipelineTemplateListResponse) =>
          result.clusterpipelinetemplates.map(fromPipelineTemplate),
        ),
      );
  }

  clusterTemplateDetail(name: string): Observable<PipelineTemplate> {
    return this.http
      .get<PipelineTemplateResponse>(
        `${PipelineApiService.PIPELINE_TEMPLATE_URL}/${name}`,
      )
      .pipe(map(fromPipelineTemplate));
  }
}
