import {
  PipelineConfig,
  PipelineConfigModel,
  PipelineConfigResponse,
  PipelineConfigTriggerResponse,
  PipelineHistory,
  PipelineTemplate,
  PipelineTemplateResponse,
  PipelineTemplateSync,
  PipelineTemplateSyncConfig,
  PipelineTemplateSyncResponse,
  PipelineTrigger,
} from '@app/api/pipeline/pipeline-api.types';
import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_TEMPLATE_VERSION,
} from '@app/constants';
import { find, get, map } from 'lodash';

export function mapPipelinesResponse(configs: any[]): PipelineConfig[] {
  return configs.map(mapPipelineResponse);
}

export function mapPipelineResponse(config: any): PipelineConfig {
  const metaKey = config.objectMeta ? 'objectMeta' : 'metadata';
  return {
    name: get(config, [metaKey, 'name'], ''),
    displayName: get(
      config,
      [metaKey, 'annotations', ANNOTATION_DISPLAY_NAME],
      '',
    ),
    namespace: get(config, [metaKey, 'namespace'], ''),
    createdAt: get(config, [metaKey, 'creationTimestamp'], ''),
    application: get(config, [metaKey, 'labels', 'app'], ''),
    histories: (get(config, 'pipelines') || []).map(mapPipelineHistoryResponse),
    jenkinsInstance: get(config, 'spec.jenkinsBinding.name', ''),
    codeRepository: get(config, 'spec.source.git.uri', ''),
    source: get(config, 'spec.source'),
    strategy: get(config, 'spec.strategy'),
    triggers: (get(config, 'spec.triggers') || []).map(mapPipelineTrigger),
    __original: config,
  };
}

export function mapPipelineTrigger(trigger: any): PipelineTrigger {
  const detail = trigger[trigger.type];
  return {
    type: trigger.type,
    enabled: detail.enabled,
    rule: detail.rule || detail.periodicCheck,
  };
}

export function mapPipelineHistoryResponse(history: any): PipelineHistory {
  const metaKey = history.objectMeta ? 'objectMeta' : 'metadata';
  const sourceStages = JSON.parse(get(history, 'status.jenkins.stages', '{}'));
  return {
    name: get(history, [metaKey, 'name'], ''),
    createdAt: get(history, [metaKey, 'creationTimestamp'], ''),
    namespace: get(history, [metaKey, 'namespace'], ''),
    cause: get(history, 'spec.cause', ''),
    status: get(history, 'status', {}),
    jenkins: {
      ...get(history, 'status.jenkins', {}),
      ...{
        stages: get(sourceStages, 'stages', []),
        startStageId: get(sourceStages, 'start_stage_id', ''),
      },
    },
    __original: history,
  };
}

export function toPipelineConfig(
  model: PipelineConfigModel,
  namespace: string,
): PipelineConfigResponse {
  const result = {
    objectMeta: {
      annotations: {
        [ANNOTATION_DISPLAY_NAME]: get(model, 'basic.display_name', ''),
      },
      name: get(model, 'basic.name', ''),
      namespace: namespace,
      labels: {
        app: get(model, 'basic.app', ''),
      },
    },
    spec: {
      runPolicy: 'Serial',
      jenkinsBinding: {
        name: get(model, 'basic.jenkins_instance', ''),
      },
      source: {
        git: {
          ref: get(model, 'jenkinsfile.branch', ''),
          uri: get(model, 'jenkinsfile.repo', ''),
        },
        secret: {
          name: get(model, 'jenkinsfile.secret', ''),
        },
        codeRepository: {
          name: get(model, 'jenkinsfile.bindingRepository'),
          ref: get(model, 'jenkinsfile.branch', ''),
        },
      },
      strategy: {
        jenkins: {
          jenkinsfile: get(model, 'editor_script.script', ''),
          jenkinsfilePath: get(model, 'jenkinsfile.path', ''),
        },
      },
      triggers: fromTrigger(model.triggers),
    },
    typeMeta: {
      kind: 'Pipelineconfig',
    },
  };
  // codeRepository cannot exist with git concurrently
  if (get(model, 'jenkinsfile.bindingRepository')) {
    delete result.spec.source.git;
  } else {
    delete result.spec.source.codeRepository;
  }
  if (get(model, 'editor_script.script')) {
    delete result.spec.source;
  }
  return result;
}

function fromTrigger(
  t: { enabled: boolean; cron_string: string }[],
): PipelineConfigTriggerResponse[] {
  const triggers = [];
  if (get(t, '[0].cron_string', '')) {
    triggers.push({
      codeChange: {
        enabled: t[0].enabled,
        periodicCheck: t[0].cron_string,
      },
      type: 'codeChange',
    });
  }
  if (get(t, '[1].cron_string', '')) {
    triggers.push({
      cron: {
        enabled: t[1].enabled,
        rule: t[1].cron_string,
      },
      type: 'cron',
    });
  }
  return triggers;
}

export function fromPipelineConfig(
  pipeline: PipelineConfigResponse,
): PipelineConfigModel {
  const metadata = get(pipeline, 'objectMeta');
  const spec = get(pipeline, 'spec');
  const basic = {
    name: metadata.name,
    display_name: get(metadata, ['annotations', ANNOTATION_DISPLAY_NAME], ''),
    jenkins_instance: get(spec, 'jenkinsBinding.name'),
    app: get(metadata, 'labels.app', ''),
    source:
      get(spec, 'source.git.uri', '') ||
      get(spec, 'source.codeRepository.name', '')
        ? 'repo'
        : 'script',
  };
  const jenkinsfile = {
    repo: {
      repo: get(spec, 'source.git.uri', ''),
      secret: get(spec, 'source.secret.name', ''),
      bindingRepository: get(spec, 'source.codeRepository.name'),
    },
    branch:
      get(spec, 'source.git.ref', '') ||
      get(spec, 'source.codeRepository.ref', ''),
    path: get(spec, 'strategy.jenkins.jenkinsfilePath', ''),
  };
  const editor_script = { script: get(spec, 'strategy.jenkins.jenkinsfile') };
  const triggersSource = get(spec, 'triggers');
  const codeChange = find(triggersSource, { type: 'codeChange' });
  const cron = find(triggersSource, { type: 'cron' });
  const triggers = [
    {
      enabled: get(codeChange, 'codeChange.enabled', false),
      cron_string: get(codeChange, 'codeChange.periodicCheck', ''),
    },
    {
      enabled: get(cron, 'cron.enabled', false),
      cron_string: get(cron, 'cron.rule', ''),
    },
  ];
  return {
    basic,
    jenkinsfile,
    editor_script,
    triggers,
    __original: pipeline,
  };
}

export function fromPipelineTemplateSync(
  sync: PipelineTemplateSyncResponse,
): PipelineTemplateSync {
  if (!sync) {
    return;
  }
  return {
    name: get(sync, 'metadata.name', ''),
    ...get(sync, 'spec.source', ''),
    status: get(sync, 'status', ''),
    __original: sync,
  };
}

export function fromPipelineTemplate(
  template: PipelineTemplateResponse,
): PipelineTemplate {
  if (!template) {
    return;
  }
  return {
    name: get(template, 'metadata.name', ''),
    labels: map(get(template, 'metadata.labels', {}), (value, key) => ({
      key,
      value,
    })),
    description: {
      en: get(
        template,
        ['metadata', 'annotations', `${ANNOTATION_DESCRIPTION}.en`],
        '',
      ),
      'zh-CN': get(
        template,
        ['metadata', 'annotations', `${ANNOTATION_DESCRIPTION}.zh-CN`],
        '',
      ),
    },
    version: get(
      template,
      ['metadata', 'annotations', ANNOTATION_TEMPLATE_VERSION],
      '',
    ),
    arguments: get(template, 'spec.arguments') || [],
    stages: get(template, 'spec.stages', []),
    withSCM: get(template, 'spec.withSCM', false),
    __original: template,
  };
}

export function toPipelineTemplateSync(
  config: PipelineTemplateSyncConfig,
): PipelineTemplateSyncResponse {
  return {
    metadata: {},
    spec: {
      source: config,
    },
    status: {
      phase: 'Pending',
    },
  };
}

export function templateStagesConvert(stages: any[]): any[] {
  let initID = 0;
  return stages
    .map((stage: any, stageIndex: number) => {
      let tasks = stage.tasks || [];
      const diagramStage: any = {
        id: ++initID,
        name: stage.name,
        edges: [],
      };
      if (tasks.length === 1) {
        diagramStage.name = tasks[0].name;
        tasks = [];
      } else if (tasks.length > 1) {
        tasks = tasks.map((task: any, taskIndex: number) => {
          const diagramTask: any = {
            id: ++initID,
            name: task.name,
            edges: [],
          };
          if (stages[stageIndex + 1]) {
            diagramTask.edges.push({
              id: initID + tasks.length - taskIndex,
              type: 'STAGE',
            });
          }
          return diagramTask;
        });
        diagramStage.edges = tasks.map((task: any) => ({
          id: task.id,
          type: 'PARALLEL',
        }));
      }
      if (stages[stageIndex + 1] && stage.tasks.length <= 1) {
        diagramStage.edges.push({ id: initID + 1, type: 'STAGE' });
      }
      return [diagramStage, ...tasks];
    })
    .reduce((accum, stageDiagramArray) => [...accum, ...stageDiagramArray], []);
}
