import { NgModule } from '@angular/core';
import { CodeApiService } from '@app/api/code/code-api.service';
import { JenkinsApiService } from '@app/api/jenkins/jenkins-api.service';
import { PipelineApiService } from '@app/api/pipeline/pipeline-api.service';
import { RegistryApiService } from '@app/api/registry/registry-api.service';
import { ToolChainApiService } from '@app/api/tool-chain/tool-chain-api.service';

import { ApplicationApiService } from './application/application-api.service';
import { ProjectApiService } from './project-api.service';
import { SecretApiService } from './secret-api.service';
import { ThirdPartyApiService } from './third-party-api.service';

@NgModule({
  providers: [
    ProjectApiService,
    SecretApiService,
    ThirdPartyApiService,
    CodeApiService,
    PipelineApiService,
    JenkinsApiService,
    ApplicationApiService,
    ToolChainApiService,
    RegistryApiService,
  ],
})
export class ApiModule {}
