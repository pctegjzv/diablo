import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pagination } from '@app/types';
import { filterBy, getQuery, pageBy } from '@app/utils/query-builder';
import { safeDump, safeLoad } from 'js-yaml';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

import {
  ApplicationIdentity,
  ApplicationsFindParams,
  ApplicationsResponse,
  DetailParams,
  K8sResourceDetail,
  Pod,
  ResourceIdentity,
} from '@app/api/application/application-api.types';
import {
  generateDetailUrl,
  toApplicaitonListItem,
  toContainer,
  toList,
  toModel,
} from '@app/api/application/utils';
import { get } from 'lodash';

@Injectable()
export class ApplicationApiService {
  constructor(private http: HttpClient) {}
  findApplications({
    name,
    pageIndex,
    itemsPerPage,
    project,
  }: ApplicationsFindParams): Observable<Pagination<any>> {
    return this.http
      .get<ApplicationsResponse>(`api/v1/applications/${project}`, {
        params: getQuery(
          filterBy('name', name),
          pageBy(pageIndex, itemsPerPage),
        ),
      })
      .pipe(
        map(res =>
          Object.assign({
            total: res.listMeta.totalItems,
            items: res.applications.map(toApplicaitonListItem),
          }),
        ),
      );
  }

  find(namespace: string) {
    return this.http.get(`api/v1/applications/${namespace}`).pipe(
      map(response => {
        const items = get(response, 'applications');
        return {
          total: get(response, 'listMeta.totalItems'),
          items: items.map(toList),
        };
      }),
    );
  }

  get({ name, namespace }: ApplicationIdentity): Observable<any> {
    return this.http.get(`api/v1/applications/${namespace}/${name}`).pipe(
      map((res: any) => {
        return {
          data: toModel(res),
          error: null,
        };
      }),
      catchError((error: any) => {
        console.log(error);
        return of({
          data: null,
          error,
        });
      }),
    );
  }

  delete({ name, namespace }: ApplicationIdentity, body: any): Observable<any> {
    return this.http.post(
      `api/v1/applications/${namespace}/${name}/actions/delete`,
      body,
    );
  }

  getYaml({ name, namespace }: ApplicationIdentity): Observable<any> {
    return this.http.get(`api/v1/applications/${namespace}/${name}/yaml`).pipe(
      map((res: any) => res.resources),
      map((resources: any) => safeDump(resources)),
    );
  }

  getJsonYaml({ name, namespace }: ApplicationIdentity): Observable<any> {
    return this.http
      .get(`api/v1/applications/${namespace}/${name}/yaml`)
      .pipe(map((res: any) => res.resources));
  }

  putYaml(
    { name, namespace }: ApplicationIdentity,
    yaml: string,
  ): Observable<any> {
    return of(yaml).pipe(
      map(y => ({ resources: safeLoad(y) })),
      concatMap(data =>
        this.http.put(`api/v1/applications/${namespace}/${name}/yaml`, data),
      ),
    );
  }

  scaleK8sResource(
    name: string,
    namespace: string,
    kind: string,
    replicas = 0,
  ) {
    return this.http.put(
      `api/v1/${kind.toLocaleLowerCase()}/${namespace}/${name}/replicas`,
      {
        replicas: replicas < 0 ? 0 : replicas,
      },
    );
  }

  getPods(
    deployment: string,
    namespace: string,
    kind: string,
  ): Observable<Pod[]> {
    return this.http.get(`api/v1/${kind}/${namespace}/${deployment}/pods`).pipe(
      map((res: any) =>
        ((res.pods as any[]) || []).map(pod => pod.objectMeta.name as string),
      ),
      concatMap(pods =>
        forkJoin(
          pods.map(pod => {
            return this.getContainers(namespace, pod).pipe(
              map(containers => ({
                name: pod,
                containers,
              })),
            );
          }),
        ),
      ),
    );
  }

  getK8sResource(
    { namespace, resourceName }: ResourceIdentity,
    kind: string,
  ): Observable<any> {
    return this.http
      .get(`api/v1/${kind.toLocaleLowerCase()}/${namespace}/${resourceName}`)
      .pipe(
        map((res: K8sResourceDetail) => {
          res.containers = (get(res, 'containers') || []).map(toContainer);
          return {
            data: res,
            error: null,
          };
        }),
      );
  }

  putK8sResource(
    { namespace, resourceName }: ResourceIdentity,
    yaml: any,
    kind: string,
  ): Observable<any> {
    return this.http.put(
      `api/v1/${kind.toLocaleLowerCase()}/${namespace}/${resourceName}`,
      yaml,
    );
  }

  getContainers(namespace: string, pod: string): Observable<string[]> {
    return this.http
      .get(`api/v1/pod/${namespace}/${pod}/container`)
      .pipe(map((res: any) => res.containers as string[]));
  }

  patchField(params: DetailParams, field: string, data: any) {
    return this.http.patch<any>(generateDetailUrl(params) + '/' + field, data);
  }

  createVolumeMount(
    resourceKind: string,
    namespace: string,
    resourceName: string,
    containerName: string,
    body: any,
  ) {
    return this.http.post(
      `api/v1/${resourceKind.toLocaleLowerCase()}/${namespace}/${resourceName}/container/${containerName}/volumeMount`,
      body,
    );
  }

  getVolumeMount(type: string, namespace: string) {
    return this.http.get(`api/v1/${type.toLocaleLowerCase()}/${namespace}/`);
  }

  getVolumeMountKeyOptions(type: string, namespace: string, name: string) {
    return this.http.get(
      `api/v1/${type.toLocaleLowerCase()}/${namespace}/${name}`,
    );
  }

  putContainer(
    kind: string,
    namespace: string,
    resourceName: string,
    containerName: string,
    payload: any,
  ): Observable<any> {
    return this.http.put(
      `api/v1/${kind.toLocaleLowerCase()}/${namespace}/${resourceName}/container/${containerName}`,
      payload,
    );
  }

  previewYaml(
    kind: string,
    namespace: string,
    resourceName: string,
    containerName: string,
    payload: any,
  ): Observable<any> {
    return this.http.put(
      `api/v1/${kind.toLocaleLowerCase()}/${namespace}/${resourceName}/container/${containerName}`,
      payload,
      {
        params: {
          isDryRun: 'true',
        },
      },
    );
  }

  putImage(
    kind: string,
    namespace: string,
    resourceName: string,
    containerName: string,
    payload: any,
  ): Observable<any> {
    return this.http.put(
      `api/v1/${kind.toLocaleLowerCase()}/${namespace}/${resourceName}/container/${containerName}/image`,
      payload,
    );
  }

  putEnvAndEnvFrom(
    kind: string,
    namespace: string,
    resourceName: string,
    containerName: string,
    payload: any,
  ): Observable<any> {
    return this.http.put(
      `api/v1/${kind.toLocaleLowerCase()}/${namespace}/${resourceName}/container/${containerName}/env`,
      payload,
    );
  }
}

export function toReports(results: any[]) {
  return (results || []).map(result => ({
    name: result.resource.name,
    type: result.resource.kind,
    operation: result.operation,
    error: result.error,
  }));
}
