import {
  AppK8sResource,
  Application,
  ApplicationInfo,
  Container,
  DetailParams,
  K8sResourceMap,
  OtherResource,
} from '@app/api/application/application-api.types';
import { mapPipelinesResponse } from '@app/api/pipeline/utils';
import { get } from 'lodash';

export function toApplicaitonListItem(resource: any): any {
  const appInfo: ApplicationInfo = {
    name: resource.objectMeta.name,
    creationTimestamp: resource.objectMeta.creationTimestamp,
    resourceList: [],
    visitAddresses: [],
    appStatus: {
      failed: 0,
      pending: 0,
      running: 0,
      total: 0,
    },
  };
  K8sResourceMap.forEach(resourceKind => {
    if (resource[resourceKind] && resource[resourceKind].length > 0) {
      resource[resourceKind].forEach((item: any) => {
        item.resourceKind = resourceKind;
        appInfo.resourceList.push(item);
        switch (item.status) {
          case 'Succeeded':
            appInfo.appStatus.running++;
            break;
          case 'Failed':
            appInfo.appStatus.failed++;
            break;
          case 'Pending':
            appInfo.appStatus.pending++;
            break;
        }
        appInfo.appStatus.total++;
        if (item.visitAddresses) {
          appInfo.visitAddresses = appInfo.visitAddresses.concat(
            item.visitAddresses,
          );
        }
      });
    }
  });
  return appInfo;
}

export function toModel(resource: any): Application {
  const meta = resource.objectMeta || resource.metadata || {};

  const modelResourcceList: any = {};
  K8sResourceMap.forEach(resourceKind => {
    modelResourcceList[resourceKind] = (resource[resourceKind] || []).map(
      toAppK8sResource,
    );
  });
  const others = (resource.others || []).map(toOtherResource);

  const appInfo: any = {
    visitAddresses: [],
    appStatus: {
      failed: 0,
      pending: 0,
      running: 0,
      total: 0,
    },
  };

  K8sResourceMap.forEach(resourceKind => {
    if (resource[resourceKind] && resource[resourceKind].length > 0) {
      resource[resourceKind].forEach((item: any) => {
        item.resourceKind = resourceKind;
        switch (item.status) {
          case 'Succeeded':
            appInfo.appStatus.running++;
            break;
          case 'Failed':
            appInfo.appStatus.failed++;
            break;
          case 'Pending':
            appInfo.appStatus.pending++;
            break;
        }
        appInfo.appStatus.total++;
        if (item.visitAddresses) {
          appInfo.visitAddresses = appInfo.visitAddresses.concat(
            item.visitAddresses,
          );
        }
      });
    }
  });

  return {
    name: meta.name,
    namespace: meta.namespace,
    deployments: modelResourcceList.deployments,
    statefulsets: modelResourcceList.statefulsets,
    daemonsets: modelResourcceList.daemonsets,
    pipelines: mapPipelinesResponse(resource.pipelines || []),
    others,
    appStatus: appInfo.appStatus,
    visitAddresses: appInfo.visitAddresses,
  };
}

export function toAppK8sResource(resource: any): AppK8sResource {
  return {
    name: resource.objectMeta.name,
    namespace: resource.objectMeta.namespace,
    podInfo: resource.podInfo,
    containers: (get(resource, 'containers') || []).map(toContainer),
    kind: resource.typeMeta.kind,
  };
}

export function toContainer(resource: any): Container {
  return {
    name: resource.name,
    image: resource.image,
    env: resource.env || [{ name: '', value: '' }],
    envFrom: resource.envFrom,
    ports: (resource.ports || [])
      .map((port: any) => `${port.protocol}/${port.containerPort}`)
      .join(','),
    resources: {
      limits: {
        cpu: get(resource, 'resources.limits.cpu', ''),
        memory: get(resource, 'resources.limits.memory', ''),
      },
      requests: {
        cpu: get(resource, 'resources.requests.cpu', ''),
        memory: get(resource, 'resources.requests.memory', ''),
      },
    },
  };
}

export function toOtherResource(resource: any): OtherResource {
  return {
    name: resource.objectMeta.name,
    namespace: resource.objectMeta.namespace,
    kind: resource.typeMeta.kind,
  };
}

export function toList(resource: any) {
  const meta = resource.objectMeta || resource.metadata || {};
  return {
    name: meta.name,
  };
}

export function generateDetailUrl(params: DetailParams) {
  const apiVersion = params.apiVersion.split('/');
  let group = '_';
  let version = apiVersion[0];
  if (apiVersion.length === 2) {
    group = apiVersion[0];
    version = apiVersion[1];
  }
  return (
    'api/v1/others/' +
    [
      group || '_',
      version || '_',
      params.kind,
      params.namespace || '_',
      params.name,
    ].join('/')
  );
}
