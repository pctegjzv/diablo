// all api related service

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  API_GROUP_VERSION,
} from '../constants';
import { filterBy, getQuery, pageBy, sortBy } from '../utils/query-builder';

export interface Project {
  name: string;
  displayName: string;
  description: string;
  projectManagers: string[];
  creationTimestamp: string;
}

export interface ProjectFindParams {
  searchBy: string;
  keywords: string;
  sort: string;
  direction: string;
  pageIndex: number;
  pageSize: number;
}

interface ProjectMetaData {
  name: string;
  annotations: {
    'alauda.io/description': string;
    'alauda.io/displayName': string;
    'alauda.io/product': string;
  };
  creationTimestamp: string;
}

export interface ProjectResponse {
  metadata?: ProjectMetaData;
  objectMeta?: ProjectMetaData;
  typeMeta: {
    kind: 'Project';
    groupVersion: string;
  };
  data: {
    apiVersion: string;
    kind: 'Project';
    metadata: {
      annotations: {
        'alauda.io/description': string;
        'alauda.io/displayName': string;
        'alauda.io/product': string;
      };
      creationTimestamp: string;
      name: string;
      resourceVersion: string;
      selfLink: string;
      uid: string;
    };
    spec: {
      namespaces: [
        {
          name: string;
          type: string;
        }
      ];
    };
    status: {
      namespaces: Array<{
        name: string;
        status: string;
      }>;
    };
  };
  project_managers?: string[];
}

export interface ProjectsResponse {
  listMeta: {
    totalItems: number;
  };
  projects: ProjectResponse[];
}

@Injectable()
export class ProjectApiService {
  constructor(private http: HttpClient) {}

  find(
    params: ProjectFindParams,
  ): Observable<{ items: Project[]; length: number }> {
    return this.http
      .get<ProjectsResponse>('api/v1/projects', {
        params: getQuery(
          filterBy(params.searchBy, params.keywords),
          sortBy(params.sort, params.direction === 'desc'),
          pageBy(params.pageIndex, params.pageSize),
        ),
      })
      .pipe(
        map(res => ({
          items: res.projects.map(toModel),
          length: res.listMeta.totalItems,
        })),
      );
  }

  get(name: string): Observable<Project> {
    return this.http
      .get(`api/v1/others/${API_GROUP_VERSION}/Project/_/${name}`)
      .pipe(map(toModel));
  }

  delete(name: string) {
    return this.http.delete(
      `api/v1/others/${API_GROUP_VERSION}/Project/_/${name}`,
    );
  }
}

function toModel(resource: ProjectResponse): Project {
  if (!resource) {
    return {} as Project;
  }

  const meta =
    resource.objectMeta || resource.metadata || ({} as ProjectMetaData);
  const annotations = (meta.annotations || {}) as { [key: string]: string };

  return {
    name: meta.name,
    creationTimestamp: meta.creationTimestamp,
    projectManagers: resource.project_managers,
    displayName: annotations[ANNOTATION_DISPLAY_NAME],
    description: annotations[ANNOTATION_DESCRIPTION],
  };
}
