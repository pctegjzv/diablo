import { Injectable } from '@angular/core';
import { PaginatorIntl } from 'alauda-ui';

import { TranslateService } from '@app/translate';

@Injectable()
export class AppPaginatorIntl extends PaginatorIntl {
  constructor(private translate: TranslateService) {
    super();
    this.translate.currentLang$.subscribe(() => this.setLabels());
  }

  setLabels() {
    this.itemsPerPageLabel = this.translate.get('paginator.page_items');
    this.nextPageLabel = this.translate.get('paginator.next_page');
    this.previousPageLabel = this.translate.get('paginator.prev_page');
    this.firstPageLabel = this.translate.get('paginator.first_page');
    this.lastPageLabel = this.translate.get('paginator.last_page');
  }

  getTotalLabel = (length: number) =>
    this.translate.get('paginator.total_records', { length });
}
