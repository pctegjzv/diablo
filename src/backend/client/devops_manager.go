package client

import (
	catalogclient "catalog-controller/pkg/client/clientset/versioned"

	asfclient "alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	clientapi "alauda.io/diablo/src/backend/client/api"
	restful "github.com/emicklei/go-restful"
)

type devopsClient struct {
	*clientManager
}

// NewDevopsClient constructs a new DevopsClient
func NewDevopsClientManager(kubeConfigPath, apiserverHost string, enableAnounymous bool) clientapi.DevOpsClientManager {
	result := &devopsClient{
		clientManager: &clientManager{
			kubeConfigPath:   kubeConfigPath,
			apiserverHost:    apiserverHost,
			enableAnounymous: enableAnounymous,
		},
	}
	result.init()
	return result
}

var _ clientapi.DevOpsClientManager = &devopsClient{}

// DevOpsClient returns DevOpsClient
func (self *devopsClient) DevOpsClient(req *restful.Request) (devopsclient.Interface, error) {
	cfg, err := self.Config(req)
	if err != nil {
		return nil, err
	}
	cfg.ContentType = "application/json"
	cfg.AcceptContentTypes = "application/json"

	client, err := devopsclient.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// CatalogClient returns CatalogClient
func (self *devopsClient) CatalogClient(req *restful.Request) (catalogclient.Interface, error) {
	cfg, err := self.Config(req)
	if err != nil {
		return nil, err
	}
	return catalogclient.NewForConfig(cfg)
}

// CatalogClient returns CatalogClient
func (self *devopsClient) ASFClient(req *restful.Request) (asfclient.Interface, error) {
	cfg, err := self.Config(req)
	if err != nil {
		return nil, err
	}
	cfg.ContentType = "application/json"
	cfg.AcceptContentTypes = "application/json"

	return asfclient.NewForConfig(cfg)
}
