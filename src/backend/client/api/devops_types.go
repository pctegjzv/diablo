package api

import (
	catalogclient "catalog-controller/pkg/client/clientset/versioned"

	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"

	asfclient "alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"

	"github.com/emicklei/go-restful"
)

// DevOpsClientManager beside implementing ClientManager
// it needs to add some of its own methods
type DevOpsClientManager interface {
	ClientManager
	DevOpsClient(req *restful.Request) (devopsclient.Interface, error)
	CatalogClient(req *restful.Request) (catalogclient.Interface, error)
	ASFClient(req *restful.Request) (asfclient.Interface, error)
}
