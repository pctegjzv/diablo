/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	v1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeMicroservicesEnvironments implements MicroservicesEnvironmentInterface
type FakeMicroservicesEnvironments struct {
	Fake *FakeAsfV1alpha1
}

var microservicesenvironmentsResource = schema.GroupVersionResource{Group: "asf.alauda.io", Version: "v1alpha1", Resource: "microservicesenvironments"}

var microservicesenvironmentsKind = schema.GroupVersionKind{Group: "asf.alauda.io", Version: "v1alpha1", Kind: "MicroservicesEnvironment"}

// Get takes name of the microservicesEnvironment, and returns the corresponding microservicesEnvironment object, and an error if there is any.
func (c *FakeMicroservicesEnvironments) Get(name string, options v1.GetOptions) (result *v1alpha1.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootGetAction(microservicesenvironmentsResource, name), &v1alpha1.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironment), err
}

// List takes label and field selectors, and returns the list of MicroservicesEnvironments that match those selectors.
func (c *FakeMicroservicesEnvironments) List(opts v1.ListOptions) (result *v1alpha1.MicroservicesEnvironmentList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootListAction(microservicesenvironmentsResource, microservicesenvironmentsKind, opts), &v1alpha1.MicroservicesEnvironmentList{})
	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1alpha1.MicroservicesEnvironmentList{}
	for _, item := range obj.(*v1alpha1.MicroservicesEnvironmentList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested microservicesEnvironments.
func (c *FakeMicroservicesEnvironments) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewRootWatchAction(microservicesenvironmentsResource, opts))
}

// Create takes the representation of a microservicesEnvironment and creates it.  Returns the server's representation of the microservicesEnvironment, and an error, if there is any.
func (c *FakeMicroservicesEnvironments) Create(microservicesEnvironment *v1alpha1.MicroservicesEnvironment) (result *v1alpha1.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootCreateAction(microservicesenvironmentsResource, microservicesEnvironment), &v1alpha1.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironment), err
}

// Update takes the representation of a microservicesEnvironment and updates it. Returns the server's representation of the microservicesEnvironment, and an error, if there is any.
func (c *FakeMicroservicesEnvironments) Update(microservicesEnvironment *v1alpha1.MicroservicesEnvironment) (result *v1alpha1.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateAction(microservicesenvironmentsResource, microservicesEnvironment), &v1alpha1.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironment), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeMicroservicesEnvironments) UpdateStatus(microservicesEnvironment *v1alpha1.MicroservicesEnvironment) (*v1alpha1.MicroservicesEnvironment, error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateSubresourceAction(microservicesenvironmentsResource, "status", microservicesEnvironment), &v1alpha1.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironment), err
}

// Delete takes name of the microservicesEnvironment and deletes it. Returns an error if one occurs.
func (c *FakeMicroservicesEnvironments) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewRootDeleteAction(microservicesenvironmentsResource, name), &v1alpha1.MicroservicesEnvironment{})
	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeMicroservicesEnvironments) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewRootDeleteCollectionAction(microservicesenvironmentsResource, listOptions)

	_, err := c.Fake.Invokes(action, &v1alpha1.MicroservicesEnvironmentList{})
	return err
}

// Patch applies the patch and returns the patched microservicesEnvironment.
func (c *FakeMicroservicesEnvironments) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootPatchSubresourceAction(microservicesenvironmentsResource, name, data, subresources...), &v1alpha1.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironment), err
}
