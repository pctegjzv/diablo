/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	asf "alauda.io/asf-apiserver/pkg/apis/asf"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeMicroservicesEnvironments implements MicroservicesEnvironmentInterface
type FakeMicroservicesEnvironments struct {
	Fake *FakeAsf
}

var microservicesenvironmentsResource = schema.GroupVersionResource{Group: "asf.alauda.io", Version: "", Resource: "microservicesenvironments"}

var microservicesenvironmentsKind = schema.GroupVersionKind{Group: "asf.alauda.io", Version: "", Kind: "MicroservicesEnvironment"}

// Get takes name of the microservicesEnvironment, and returns the corresponding microservicesEnvironment object, and an error if there is any.
func (c *FakeMicroservicesEnvironments) Get(name string, options v1.GetOptions) (result *asf.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootGetAction(microservicesenvironmentsResource, name), &asf.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironment), err
}

// List takes label and field selectors, and returns the list of MicroservicesEnvironments that match those selectors.
func (c *FakeMicroservicesEnvironments) List(opts v1.ListOptions) (result *asf.MicroservicesEnvironmentList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootListAction(microservicesenvironmentsResource, microservicesenvironmentsKind, opts), &asf.MicroservicesEnvironmentList{})
	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &asf.MicroservicesEnvironmentList{}
	for _, item := range obj.(*asf.MicroservicesEnvironmentList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested microservicesEnvironments.
func (c *FakeMicroservicesEnvironments) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewRootWatchAction(microservicesenvironmentsResource, opts))
}

// Create takes the representation of a microservicesEnvironment and creates it.  Returns the server's representation of the microservicesEnvironment, and an error, if there is any.
func (c *FakeMicroservicesEnvironments) Create(microservicesEnvironment *asf.MicroservicesEnvironment) (result *asf.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootCreateAction(microservicesenvironmentsResource, microservicesEnvironment), &asf.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironment), err
}

// Update takes the representation of a microservicesEnvironment and updates it. Returns the server's representation of the microservicesEnvironment, and an error, if there is any.
func (c *FakeMicroservicesEnvironments) Update(microservicesEnvironment *asf.MicroservicesEnvironment) (result *asf.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateAction(microservicesenvironmentsResource, microservicesEnvironment), &asf.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironment), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeMicroservicesEnvironments) UpdateStatus(microservicesEnvironment *asf.MicroservicesEnvironment) (*asf.MicroservicesEnvironment, error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateSubresourceAction(microservicesenvironmentsResource, "status", microservicesEnvironment), &asf.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironment), err
}

// Delete takes name of the microservicesEnvironment and deletes it. Returns an error if one occurs.
func (c *FakeMicroservicesEnvironments) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewRootDeleteAction(microservicesenvironmentsResource, name), &asf.MicroservicesEnvironment{})
	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeMicroservicesEnvironments) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewRootDeleteCollectionAction(microservicesenvironmentsResource, listOptions)

	_, err := c.Fake.Invokes(action, &asf.MicroservicesEnvironmentList{})
	return err
}

// Patch applies the patch and returns the patched microservicesEnvironment.
func (c *FakeMicroservicesEnvironments) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *asf.MicroservicesEnvironment, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootPatchSubresourceAction(microservicesenvironmentsResource, name, data, subresources...), &asf.MicroservicesEnvironment{})
	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironment), err
}
