/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	v1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeMicroservicesEnvironmentBindings implements MicroservicesEnvironmentBindingInterface
type FakeMicroservicesEnvironmentBindings struct {
	Fake *FakeAsfV1alpha1
	ns   string
}

var microservicesenvironmentbindingsResource = schema.GroupVersionResource{Group: "asf.alauda.io", Version: "v1alpha1", Resource: "microservicesenvironmentbindings"}

var microservicesenvironmentbindingsKind = schema.GroupVersionKind{Group: "asf.alauda.io", Version: "v1alpha1", Kind: "MicroservicesEnvironmentBinding"}

// Get takes name of the microservicesEnvironmentBinding, and returns the corresponding microservicesEnvironmentBinding object, and an error if there is any.
func (c *FakeMicroservicesEnvironmentBindings) Get(name string, options v1.GetOptions) (result *v1alpha1.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(microservicesenvironmentbindingsResource, c.ns, name), &v1alpha1.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironmentBinding), err
}

// List takes label and field selectors, and returns the list of MicroservicesEnvironmentBindings that match those selectors.
func (c *FakeMicroservicesEnvironmentBindings) List(opts v1.ListOptions) (result *v1alpha1.MicroservicesEnvironmentBindingList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(microservicesenvironmentbindingsResource, microservicesenvironmentbindingsKind, c.ns, opts), &v1alpha1.MicroservicesEnvironmentBindingList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1alpha1.MicroservicesEnvironmentBindingList{}
	for _, item := range obj.(*v1alpha1.MicroservicesEnvironmentBindingList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested microservicesEnvironmentBindings.
func (c *FakeMicroservicesEnvironmentBindings) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(microservicesenvironmentbindingsResource, c.ns, opts))

}

// Create takes the representation of a microservicesEnvironmentBinding and creates it.  Returns the server's representation of the microservicesEnvironmentBinding, and an error, if there is any.
func (c *FakeMicroservicesEnvironmentBindings) Create(microservicesEnvironmentBinding *v1alpha1.MicroservicesEnvironmentBinding) (result *v1alpha1.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(microservicesenvironmentbindingsResource, c.ns, microservicesEnvironmentBinding), &v1alpha1.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironmentBinding), err
}

// Update takes the representation of a microservicesEnvironmentBinding and updates it. Returns the server's representation of the microservicesEnvironmentBinding, and an error, if there is any.
func (c *FakeMicroservicesEnvironmentBindings) Update(microservicesEnvironmentBinding *v1alpha1.MicroservicesEnvironmentBinding) (result *v1alpha1.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(microservicesenvironmentbindingsResource, c.ns, microservicesEnvironmentBinding), &v1alpha1.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironmentBinding), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeMicroservicesEnvironmentBindings) UpdateStatus(microservicesEnvironmentBinding *v1alpha1.MicroservicesEnvironmentBinding) (*v1alpha1.MicroservicesEnvironmentBinding, error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateSubresourceAction(microservicesenvironmentbindingsResource, "status", c.ns, microservicesEnvironmentBinding), &v1alpha1.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironmentBinding), err
}

// Delete takes name of the microservicesEnvironmentBinding and deletes it. Returns an error if one occurs.
func (c *FakeMicroservicesEnvironmentBindings) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(microservicesenvironmentbindingsResource, c.ns, name), &v1alpha1.MicroservicesEnvironmentBinding{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeMicroservicesEnvironmentBindings) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(microservicesenvironmentbindingsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &v1alpha1.MicroservicesEnvironmentBindingList{})
	return err
}

// Patch applies the patch and returns the patched microservicesEnvironmentBinding.
func (c *FakeMicroservicesEnvironmentBindings) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(microservicesenvironmentbindingsResource, c.ns, name, data, subresources...), &v1alpha1.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesEnvironmentBinding), err
}
