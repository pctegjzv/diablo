package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetDefaultToolChains() (toolChain []*ToolChainElement) {
	toolChain = []*ToolChainElement{}

	// codeRepository
	codeRepository := ToolChainElement{
		Name: ToolChainCodeRepositoryName,
		DisplayName: DisplayName{
			EN: ToolChainCodeRepositoryDisplayNameEN,
			ZH: ToolChainCodeRepositoryDisplayNameCN,
		},
		Enabled: true,
		Items:   getDefaultCodeRepositoryItems(),
	}
	toolChain = append(toolChain, &codeRepository)

	// continuousIntegration
	continuousIntegration := ToolChainElement{
		Name: ToolChainContinuousIntegrationName,
		DisplayName: DisplayName{
			EN: ToolChainContinuousIntegrationDisplayNameEN,
			ZH: ToolChainContinuousIntegrationDisplayNameCN,
		},
		Enabled: true,
		Items:   getDefaultContinuousIntegrationItems(),
	}
	toolChain = append(toolChain, &continuousIntegration)

	// artifactRepository
	artifactRepository := ToolChainElement{
		Name: ToolChainArtifactRepositoryName,
		DisplayName: DisplayName{
			EN: ToolChainArtifactRepositoryDisplayNameEN,
			ZH: ToolChainArtifactRepositoryDisplayNameCN,
		},
		Enabled: true,
		Items:   getDefaultArtifactRepositoryItems(),
	}
	toolChain = append(toolChain, &artifactRepository)

	// testTool
	testTool := ToolChainElement{
		Name: ToolChainTestToolName,
		DisplayName: DisplayName{
			EN: ToolChainTestToolDisplayNameEN,
			ZH: ToolChainTestToolDisplayNameCN,
		},
		Enabled: true,
		Items:   getDefaultTestToolItems(),
	}
	toolChain = append(toolChain, &testTool)

	// projectManage
	projectManage := ToolChainElement{
		Name: ToolChainProjectManagementName,
		DisplayName: DisplayName{
			EN: ToolChainProjectManagementDisplayNameEN,
			ZH: ToolChainProjectManagementDisplayNameCN,
		},
		Enabled: true,
		Items:   getDefaultProjectManageItems(),
	}
	toolChain = append(toolChain, &projectManage)

	return
}

func getDefaultProjectManageItems() (items []*ToolChainElementItem) {
	items = []*ToolChainElementItem{}
	// append jira
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindProjectManagement,
		Type:    ProjectManageTypeJira.String(),
		Public:  false,
		Enabled: true,
		Name:    JiraName,
		DisplayName: DisplayName{
			EN: JiraDisplayNameEN,
			ZH: JiraDisplayNameCN,
		},
	})
	// append taiga
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindProjectManagement,
		Type:    ProjectManageTypeTaiga.String(),
		Public:  false,
		Enabled: true,
		Name:    TaigaName,
		DisplayName: DisplayName{
			EN: TaigaDisplayNameEN,
			ZH: TaigaDisplayNameCN,
		},
		Host: "",
		HTML: "",
	})
	return
}

func getDefaultTestToolItems() (items []*ToolChainElementItem) {
	items = []*ToolChainElementItem{}
	// append redwoodHQ
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindTestTool,
		Type:    TestToolTypeRedwoodHQ.String(),
		Public:  false,
		Enabled: true,
		Name:    RedwoodHQName,
		DisplayName: DisplayName{
			EN: RedwoodHQDisplayNameEN,
			ZH: RedwoodHQDisplayNameCN,
		},
	})
	return
}

func getDefaultArtifactRepositoryItems() (items []*ToolChainElementItem) {
	items = []*ToolChainElementItem{}
	// append docker registry
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindImageRegistry,
		Type:    RegistryTypeDocker.String(),
		Public:  false,
		Enabled: true,
		Name:    DockerRegistryName,
		DisplayName: DisplayName{
			EN: DockerRegistryDisplayNameEN,
			ZH: DockerRegistryDisplayNameCN,
		},
		Host: "",
		HTML: "",
	})
	//// append jfrog
	//items = append(items, &ToolChainElementItem{
	//	Kind:    ResourceKindImageRegistry,
	//	Type:    "jfrog",
	//	Public:  false,
	//	Enabled: false,
	//	Name:    "jfrog-registry",
	//	DisplayName: DisplayName{
	//		EN: "JFrog Registry",
	//		ZH: "JFrog Registry",
	//	},
	//	Host: "",
	//	HTML: "",
	//})
	return
}

func getDefaultContinuousIntegrationItems() (items []*ToolChainElementItem) {
	items = []*ToolChainElementItem{}
	// append jenkins
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindJenkins,
		Type:    "",
		Public:  false,
		Enabled: true,
		Name:    JenkinsName,
		DisplayName: DisplayName{
			EN: JenkinsDisplayNameEN,
			ZH: JenkinsDisplayNameCN,
		},
		Host: "",
		HTML: "",
	})
	return
}

func getDefaultCodeRepositoryItems() (items []*ToolChainElementItem) {
	items = []*ToolChainElementItem{}
	// append github
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindCodeRepoService,
		Type:    CodeRepoServiceTypeGithub.String(),
		Public:  true,
		Enabled: true,
		Name:    GithubName,
		DisplayName: DisplayName{
			EN: GithubDisplayNameEN,
			ZH: GithubDisplayNameCN,
		},
		Host: GithubHost,
		HTML: GithubHTML,
	})
	// append gitlab
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindCodeRepoService,
		Type:    CodeRepoServiceTypeGitlab.String(),
		Public:  true,
		Enabled: true,
		Name:    GitlabName,
		DisplayName: DisplayName{
			EN: GitlabDisplayNameEN,
			ZH: GitlabDisplayNameCN,
		},
		Host: GitlabHost,
		HTML: GitlabHTML,
	})
	// append private gitlab
	items = append(items, &ToolChainElementItem{
		Kind:       ResourceKindCodeRepoService,
		Type:       CodeRepoServiceTypeGitlab.String(),
		Public:     false,
		Enabled:    true,
		Enterprise: true,
		Name:       GitlabPrivateName,
		DisplayName: DisplayName{
			EN: GitlabPrivateDisplayNameEN,
			ZH: GitlabPrivateDisplayNameCN,
		},
		Host: GitlabPrivateHost,
		HTML: GitlabPrivateHTML,
	})
	// append gitee
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindCodeRepoService,
		Type:    CodeRepoServiceTypeGitee.String(),
		Public:  true,
		Enabled: true,
		Name:    GiteeName,
		DisplayName: DisplayName{
			EN: GiteeDisplayNameEN,
			ZH: GiteeDisplayNameCN,
		},
		Host: GiteeHost,
		HTML: GiteeHTML,
	})
	// append private gitee
	items = append(items, &ToolChainElementItem{
		Kind:       ResourceKindCodeRepoService,
		Type:       CodeRepoServiceTypeGitee.String(),
		Public:     false,
		Enabled:    true,
		Enterprise: true,
		Name:       GiteePrivateName,
		DisplayName: DisplayName{
			EN: GiteePrivateDisplayNameEN,
			ZH: GiteePrivateDisplayNameCN,
		},
		Host: GiteePrivateHost,
		HTML: GiteePrivateHTML,
	})
	// append bitbucket
	items = append(items, &ToolChainElementItem{
		Kind:    ResourceKindCodeRepoService,
		Type:    CodeRepoServiceTypeBitbucket.String(),
		Public:  true,
		Enabled: true,
		Name:    BitbucketName,
		DisplayName: DisplayName{
			EN: BitbucketDisplayNameEN,
			ZH: BitbucketDisplayNameCN,
		},
		Host: BitbucketHost,
		HTML: BitbucketHTML,
	})
	return
}

func GetDefaultGithubService() *CodeRepoService {
	return &CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{
			Name: GithubName,
		},
		TypeMeta: metav1.TypeMeta{
			Kind:       TypeCodeRepoService,
			APIVersion: APIVersionV1Alpha1,
		},
		Spec: CodeRepoServiceSpec{
			HTTP: HostPort{
				Host: GithubHost,
			},
			Type:   CodeRepoServiceTypeGithub,
			Public: true,
		},
	}
}
