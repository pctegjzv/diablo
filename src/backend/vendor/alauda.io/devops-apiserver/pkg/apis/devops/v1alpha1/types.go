/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"fmt"
	"time"

	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// ListEverything is a list options used to list all objects without any filtering.
var ListEverything = metav1.ListOptions{
	LabelSelector: labels.Everything().String(),
	FieldSelector: fields.Everything().String(),
}

// region Project

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Project struct to hold the project data
type Project struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Specification of the desired behavior of the Project.
	// +optional
	Spec ProjectSpec `json:"spec,omitempty"`
	// Most recently observed status of the Project.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ProjectStatus `json:"status,omitempty"`
}

// ProjectSpec defines Project's specs
type ProjectSpec struct {
	// Namespaces is a list of ProjectNamespace objects.
	// +optional
	Namespaces []ProjectNamespace `json:"namespaces"`
}

// ProjectNamespace defines a namespace inside ProjectSpec
type ProjectNamespace struct {
	// Name of the namespace in project.
	// +optional
	Name string `json:"name"`
	// Type of the namespace in project.
	// +optional
	Type string `json:"type"`
}

// ProjectStatus defines Project's status
type ProjectStatus struct {
	// Namespaces is a list of ProjectNamespace objects.
	// +optional
	Namespaces []ProjectNamespaceStatus `json:"namespaces"`
}

// ProjectNamespaceStatus defines a Project Namespace relationship status
type ProjectNamespaceStatus struct {
	// Name of the namespace in project.
	// +optional
	Name string `json:"name"`
	// Status of the namespace in project.
	// +optional
	Status string `json:"status"`
}

// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectList is a list of Fischer objects.
type ProjectList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of Project objects.
	Items []Project `json:"items"`
}

// endregion

// region Jenkins

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Jenkins struct to hold the jenkins data
type Jenkins struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Specification of the desired behavior of the Jenkins.
	// +optional
	Spec JenkinsSpec `json:"spec,omitempty"`
	// Most recently observed status of the Jenkins.
	// Populated by the system.
	// Read-only.
	// +optional
	Status JenkinsStatus `json:"status,omitempty"`
}

// JenkinsSpec defines Jenkins' specs
type JenkinsSpec struct {
	// HTTP
	// Cannot be updated.
	// +optional
	HTTP HostPort `json:"http"`
}

// JenkinsStatus defines Jenkins' status
type JenkinsStatus struct {
	// Status is the type of the jenkins.
	// +optional
	Status string `json:"status"`
	// HTTPStatus is the status of the jenkins.
	// +optional
	HTTPStatus *HostPortStatus `json:"http,omitempty"`
}

// HostPortStatus defines a status for a HostPort setting
type HostPortStatus struct {
	// StatusCode is the status code of http response
	// +optional
	StatusCode int `json:"statusCode"`
	// Response is the response of the http request.
	// +optional
	Response string `json:"response,omitempty"`
	// Version is the version of the http request.
	// +optional
	Version string `json:"version,omitempty"`
	// Delay means the http request will attempt later
	// +optional
	Delay *time.Duration `json:"delay,omitempty"`
	// Last time we probed the http request.
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
}

// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsList is a list of Jenkins objects.
type JenkinsList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of Jenkins objects.
	Items []Jenkins `json:"items"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBinding struct holds a reference to a specific jenkins object
// and some user data for access
type JenkinsBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Specification of the desired behavior of the JenkinsBinding.
	// +optional
	Spec JenkinsBindingSpec `json:"spec,omitempty"`
	// Most recently observed status of the JenkinsBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status JenkinsBindingStatus `json:"status,omitempty"`
}

// JenkinsBindingSpec defines JenkinsBinding's specs
type JenkinsBindingSpec struct {
	// Jenkins is the jenkins defined in the binding.
	// +optional
	Jenkins JenkinsInstance `json:"jenkins"`
	// Account is the account to access the jenkins.
	// +optional
	Account UserAccount `json:"account"`
}

// JenkinsInstance defines a Jenkins instance for JenkinsBinding
type JenkinsInstance struct {
	// Name is the name of the jenkins.
	// +optional
	Name string `json:"name"`
}

// UserAccount user account data for access
// currently only suports a secret as storage
type UserAccount struct {
	// Secret is the secret of the account.
	// +optional
	Secret SecretKeySetRef `json:"secret"`
}

// SecretKeySetRef reference of a set of username/api token keys in a Secret
type SecretKeySetRef struct {
	// Name is the name of the jenkins.
	// +optional
	Name string `json:"name"`
	// UsernameKey is the username key defined in the secret.
	// +optional
	UsernameKey string `json:"usernameKey"`
	// APITokenKey is the apiToken key defined in the secret.
	// +optional
	APITokenKey string `json:"apiTokenKey"`
}

// JenkinsBindingStatus defines JenkinsBinding' status
type JenkinsBindingStatus struct {
	// Status is the status of the jenkinsBinding.
	// +optional
	Status string `json:"status"`
	// Conditions is a list of condition in the jenkinsBinding.
	// +optional
	Conditions []JenkinsBindingCondition `json:"conditions"`
	// HTTPStatus is the status of the jenkinsBinding.
	// +optional
	HTTPStatus *HostPortStatus `json:"http,omitempty"`
}

// JenkinsBindingCondition jenkins binding condition clause
type JenkinsBindingCondition struct {
	// Type is the type of the condition.
	// +optional
	Type string `json:"type"`
	// Last time we probed the condition.
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	Status  string `json:"status"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBindingList is a list of JenkinsBinding objects.
type JenkinsBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of JenkinsBinding objects.
	Items []JenkinsBinding `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsfilePreview jenkinsfile's preview
type JenkinsfilePreview struct {
	metav1.TypeMeta `json:",inline"`

	// Jenkinsfile generated by template or other stuff
	Jenkinsfile string `json:"jenkinsfile"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsfilePreviewOptions request option for jenkinsfile's preview
type JenkinsfilePreviewOptions struct {
	metav1.TypeMeta `json:",inline"`

	// PipelineConfigSpec specification of PipelineConfig
	PipelineConfigSpec *PipelineConfigSpec `json:"template"`
	// Source git source
	// +optional
	Source *PipelineSource `json:"source"`
	// Values arguments values
	// +optional
	Values map[string]string `json:"values"`
}

// endregion

// LocalObjectReference simple local reference for local objects
// contains enough information to let you locate the
// referenced object inside the same namespace
// k8s.io/api/core/v1/types.go
type LocalObjectReference struct {
	// Name of the referent.
	// More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
	// TODO: Add other useful fields. apiVersion, kind, uid?
	// +optional
	Name string `json:"name,omitempty"`
}

// region Pipeline

// +genclient
// +genclient:method=Preview,verb=create,subresource=preview,input=JenkinsfilePreviewOptions,result=JenkinsfilePreview
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineConfig struct holds a reference to a specific pipeline configuration
// and some user data for access
type PipelineConfig struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the PipelineConfig.
	// +optional
	Spec PipelineConfigSpec `json:"spec"`
	// Most recently observed status of the PipelineConfig.
	// Populated by the system.
	// Read-only.
	// +optional
	Status PipelineConfigStatus `json:"status"`
}

// PipelineConfigSpec defines PipelineConfig's specs
type PipelineConfigSpec struct {
	// JenkinsBinding is the jenkinsBinding of the pipelineConfig.
	// PipelineConfig will be synced between k8s and jenkins defined in this jenkinsBinding.
	// +optional
	JenkinsBinding LocalObjectReference `json:"jenkinsBinding"`
	// RunPolicy is the runPolicy of the pipelineConfig.
	// One of the "Serial" and "Parallel".
	// +optional
	RunPolicy PipelineRunPolicy `json:"runPolicy"`
	// RunLimits is the runLimits of the pipelineConfig.
	// Limit the max number of the pipelines stored.
	// +optional
	RunLimits PipelineRunLimits `json:"runLimits"`
	// Parameters is the parameters of the pipelineConfig.
	// +optional
	Parameters []PipelineParameter `json:"parameters"`
	// Triggers is the triggers of the pipelineConfig.
	// +optional
	Triggers []PipelineTrigger `json:"triggers"`
	// Strategy is the strategy of the pipelineConfig.
	// +optional
	Strategy PipelineStrategy `json:"strategy"`
	// Hooks is the hooks of the pipelineConfig.
	// +optional
	Hooks []PipelineHook `json:"hooks"`
	// Source is the source of the pipelineConfig.
	// +optional
	Source PipelineSource `json:"source"`
}

// PipelineRunPolicy pipeline run policy
type PipelineRunPolicy string

const (
	// PipelinePolicySerial Serial run policy
	// used to have sequential pipelines for the same PipelineConfig
	PipelinePolicySerial PipelineRunPolicy = "Serial"
	// PipelinePolicyParallel Parallel run policy
	// used to run multiple pipelines of the same PipelineConfig in parallel
	PipelinePolicyParallel PipelineRunPolicy = "Parallel"
)

// PipelineRunLimits specifies a limited number of stored Pipeline runs
type PipelineRunLimits struct {
	// If set, the number of success pipeline stored will be limited.
	// +optional
	SuccessCount int64 `json:"successCount"`
	// If set, the number of failure pipeline stored will be limited.
	// +optional
	FailureCount int64 `json:"failureCount"`
}

// PipelineParameter specifies a parameter for a pipeline
type PipelineParameter struct {
	// Name is the name of the parameter.
	// +optional
	Name string `json:"name"`
	// Type is the type of the parameter.
	// +optional
	Type PipelineParameterType `json:"type"`
	// Value is the value of the parameter.
	// +optional
	Value string `json:"value"`
	// Description is the description of the parameter.
	// +optional
	Description string `json:"description"`
}

// PipelineParameterType type of parameter for pipeline
type PipelineParameterType string

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTaskTemplate specified a task for a pipeline
type PipelineTaskTemplate struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the PipelineTaskTemplate
	Spec PipelineTaskTemplateSpec `json:"spec"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTaskTemplateList is list of PipelineTaskTemplate
type PipelineTaskTemplateList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []PipelineTaskTemplate `json:"items"`
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTaskTemplate is kind of cluster PipelineTaskTemplate
type ClusterPipelineTaskTemplate struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the PipelineTaskTemplate
	Spec PipelineTaskTemplateSpec `json:"spec"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTaskTemplateList is a list of ClusterPipelineTaskTemplate
type ClusterPipelineTaskTemplateList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ClusterPipelineTaskTemplate `json:"items"`
}

// PipelineDependency for PipelineTaskTemplate dependency on jenkins plugins
type PipelineDependency struct {
	// Plugins hold Jenkins plugins dependency for the specific task
	// +optional
	Plugins []JenkinsPlugin `json:"plugins"`
}

// JenkinsPlugin is Jenkins plugin info
type JenkinsPlugin struct {
	// Name is the name of plugin
	Name string `json:"name"`
	// Version is the version of plugin
	Version string `json:"version"`
}

// PipelineTemplateTaskEngine describe the kind of engine used for the PipelineTaskTemplate
type PipelineTemplateTaskEngine string

const (
	// PipelineTaskTemplateEngineGoTemplate go template for rendering
	PipelineTaskTemplateEngineGoTemplate = "gotpl"
)

// PipelineTaskTemplateSpec represents PipelineTaskTemplate's specs
type PipelineTaskTemplateSpec struct {
	// Engine the way of how to render taskTemplate
	// +optinal
	Engine PipelineTemplateTaskEngine `json:"engine"`
	// Agent indicates where the task should be running
	// +optional
	Agent *JenkinsAgent `json:"agent"`
	// Body task template body
	Body string `json:"body"`
	// Exports all envrionments will be exports
	// +optional
	Exports []GlobalParameter `json:"exports"`
	// Parameters that will be use in running
	// +optional
	Parameters []PipelineParameter `json:"parameters"`
	// Arguments the task template's arguments
	// +optional
	Arguments []PipelineTaskArgument `json:"arguments"`
	// Dependencies indicates plugins denpendencies of task
	// +optional
	Dependencies *PipelineDependency `json:"dependencies"`
}

// GlobalParameter for export
type GlobalParameter struct {
	// Name the name of parameter
	Name string `json:"name"`
	// Description description of parameter
	// +optional
	Description *I18nName `json:"description"`
}

// PipelineTaskArgument sepcified a arugment for PipelineTaskTemplate
type PipelineTaskArgument struct {
	// Name the name of task
	Name string `json:"name"`
	// Schema schema of task
	Schema PipelineTaskArgumentSchema `json:"schema"`
	// Display display of task
	Display PipelineTaskArgumentDisplay `json:"display"`
	// Required indicate whether required
	// +optional
	Required bool `json:"required"`
	// Default default value of arugment
	// +optional
	Default string `json:"default"`
	// Validation validation of arugment
	// +optional
	Validation *PipelineTaskArgumentValidation `json:"validation"`
	// Relation relation between arguments
	// +optional
	Relation []PipelineTaskArgumentAction `json:"relation"`
}

// PipelineTaskArgumentValidation for task arument validation
type PipelineTaskArgumentValidation struct {
	// Pattern pattern of validation
	Pattern string `json:"pattern"`
}

// PipelineTaskArgumentAction action for task argument
type PipelineTaskArgumentAction struct {
	// Action action for task argument
	Action string `json:"action"`
	// When time condition for task execution
	When PipelineTaskArgumentWhen `json:"when"`
}

// PipelineTaskArgumentWhen action time config
type PipelineTaskArgumentWhen struct {
	// Name name of when
	Name string `json:"name"`
	// Value value of when
	Value bool `json:"value"`
}

// PipelineTemplateEngine describe the kind of engine used for the PipelineTemplate
type PipelineTemplateEngine string

const (
	// PipelineTemplateEngineGraph render template as the graph form
	PipelineTemplateEngineGraph PipelineTemplateEngine = "graph"
)

// +genclient
// +genclient:method=Preview,verb=create,subresource=preview,input=JenkinsfilePreviewOptions,result=JenkinsfilePreview
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplate specified a jenkinsFile template
type PipelineTemplate struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Spec specification of PipelineTemplate
	Spec PipelineTemplateSpec `json:"spec"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplateList is a list of PipelineTemplate
type PipelineTemplateList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items items of PipelineTemplates
	Items []PipelineTemplate `json:"items"`
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTemplate specified a cluster kind of PipelineTemplate
type ClusterPipelineTemplate struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Spec specification of PipelineTemplate
	Spec PipelineTemplateSpec `json:"spec"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTemplateList is a list of ClusterPipelineTemplate
type ClusterPipelineTemplateList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items items of ClusterPipelineTemplate
	Items []ClusterPipelineTemplate `json:"items"`
}

// PipelineTemplateSpec represents PipelineTemplate's specs
type PipelineTemplateSpec struct {
	// Engine the way how to render PipelineTemplate
	// +optinal
	Engine PipelineTemplateEngine `json:"engine"`
	// WithSCM indicate if we use scm in Pipeline
	// +optional
	WithSCM bool `json:"withSCM"`
	// Agent indicate where the pipeline will running
	// +optional
	Agent *JenkinsAgent `json:"agent"`
	// Stages contains all stages of a pipeline script
	Stages []PipelineStage `json:"stages"`
	// Parameters will need before pipeline run
	// +optional
	Parameters []PipelineParameter `json:"parameters"`
	// Arguments is arguments for templates
	// +optional
	Arguments []PipelineTemplateArgumentGroup `json:"arguments"`
}

// JenkinsAgent specifed agent for PipelineTemplate
type JenkinsAgent struct {
	// Label is the label for agents
	Label string `json:"label"`
}

// PipelineStage specifed stage for pipeline
type PipelineStage struct {
	// Name is name for a stage
	Name string `json:"name"`
	// Tasks contains all tasks which will running
	Tasks []PipelineTemplateTask `json:"tasks"`
}

// PipelineTemplateTask spcifed task template for PipelineTemplate
type PipelineTemplateTask struct {
	// Name is the name of a task
	Name string `json:"name"`
	// Agent indicate that where the current task will running
	// +optional
	Agent *JenkinsAgent `json:"agent"`
	// Type is type of a task
	Type string `json:"type"`
	// Kind is kind of a task
	Kind string `json:"kind"`
	// Options is some options for a task
	// +optional
	Options *PipelineTaskOption `json:"options"`
	// Approve is a option for maual confirm
	// +optional
	Approve *PipelineTaskApprove `json:"approve"`
	// Environments contains custom define variables
	// +optional
	Environments []PipelineEnvironment `json:"environments"`
}

// PipelineEnvironment specifed environment for Pipeline
type PipelineEnvironment struct {
	// Name is a key for environment map
	Name string `json:"name"`
	// Value is a value for environment map
	Value string `json:"value"`
}

// PipelineTaskApprove specfied approve option for pipeline
type PipelineTaskApprove struct {
	// Message is the message show to users
	Message string `json:"message"`
	// Timeout is timeout for waiting
	// +optional
	Timeout int64 `json:"timeout"`
}

// PipelineTaskOption spcified task option for task template
type PipelineTaskOption struct {
	// Timeout is timeout for a operation
	// +optional
	Timeout int64 `json:"timeout"`
}

// PipelineTemplateArgumentGroup specifed argument group for PipelineTemplate
type PipelineTemplateArgumentGroup struct {
	// DisplayName is used to display
	DisplayName I18nName `json:"displayName"`
	// Items contains all argument for templates
	Items []PipelineTemplateArgumentValue `json:"items"`
}

// I18nName spcified name for Piepline's stage or aruguments
type I18nName struct {
	// Zh is the Chinese name
	Zh string `json:"zh-CN"`
	// EN is the English name
	En string `json:"en"`
}

// PipelineTemplateArgument specified arugment for PipelineTemplate
type PipelineTemplateArgument struct {
	// Name is the name of a PipelineTemplate
	Name string `json:"name"`
	// Schema is the schema of a template
	Schema PipelineTaskArgumentSchema `json:"schema"`
	// Binding mean bind argument to task
	// +optional
	Binding []string `json:"binding"`
	// Display is used to display
	Display PipelineTaskArgumentDisplay `json:"display"`
	// Required specific argument is required
	// +optional
	Required bool `json:"required"`
	// Default specific argument has default value
	// +optional
	Default string `json:"default"`
	// Validation specific validation for argument
	// +optional
	Validation *PipelineTaskArgumentValidation `json:"validation"`
	// Relation relation between arguments
	// +optional
	Relation []PipelineTaskArgumentAction `json:"relation"`
}

// PipelineTemplateArgumentValue hold argument and value
type PipelineTemplateArgumentValue struct {
	PipelineTemplateArgument `json:",inline"`
	// Value is the value of a argument
	Value string `json:"value"`
}

// PipelineTaskArgumentSchema specifed arugment schema
type PipelineTaskArgumentSchema struct {
	// Type is the type of a argument
	Type string `json:"type"`
}

// PipelineTaskArgumentDisplay specifed the way of dipslay
type PipelineTaskArgumentDisplay struct {
	// Type is the type of a argument
	Type string `json:"type"`
	// Name contains multi-languages name
	Name I18nName `json:"name"`
	// Related related to other arguments
	// +optional
	Related string `json:"related,omitempty"`
	// Description is used to describe the arugments
	// +optional
	Description I18nName `json:"description,omitempty"`
}

const (
	// PipelineParameterTypeString parameter type string
	PipelineParameterTypeString PipelineParameterType = "string"
	// PipelineParameterTypeBoolean parameter type boolean
	PipelineParameterTypeBoolean PipelineParameterType = "boolean"
)

// PipelineTrigger specifies a trigger for a pipeline
type PipelineTrigger struct {
	// Type is the type of the pipeline trigger.
	// One of "cron" or "codeChange".
	// +optional
	Type PipelineTriggerType `json:"type"`
	// Cron is one trigger type of pipeline.
	// The pipeline will be triggered in accordance with the cron rule.
	// +optional
	Cron *PipelineTriggerCron `json:"cron,omitempty"`
	// CodeChange is one trigger type of pipeline.
	// The pipeline will be triggered once the code was pushed to the repository.
	// +optional
	CodeChange *PipelineTriggerCodeChange `json:"codeChange,omitempty"`
}

// PipelineTriggerType trigger type for pipelines
type PipelineTriggerType string

const (
	// PipelineTriggerTypeCron cron trigger type
	PipelineTriggerTypeCron PipelineTriggerType = "cron"
	// PipelineTriggerTypeCodeChange code change trigger type
	PipelineTriggerTypeCodeChange PipelineTriggerType = "codeChange"
)

// PipelineTriggerCron cron trigger type
type PipelineTriggerCron struct {
	// Enabled timing trigger pipeline or not.
	// +optional
	Enabled bool `json:"enabled"`
	// Rule is the rule of cron trigger.
	// +optional
	Rule string `json:"rule"`
}

// PipelineTriggerCodeChange code change trigger type
type PipelineTriggerCodeChange struct {
	// Enabled trigger pipeline by code change or not.
	// +optional
	Enabled bool `json:"enabled"`
	// PeriodicCheck specifies how often check code changes.
	// +optional
	PeriodicCheck string `json:"periodicCheck"`
}

// PipelineStrategy pipeline execution strategy
type PipelineStrategy struct {
	// Template is a template for pipeline
	// +optional
	Template *PipelineConfigTemplate `json:"template"`
	// Jenkins specific jenkinsfile path
	// +optional
	Jenkins PipelineStrategyJenkins `json:"jenkins"`
}

// PipelineConfigTemplate is instance of template
type PipelineConfigTemplate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`

	// Labels add some marks
	// +optional
	Labels map[string]string `json:"labels"`
	// Spec specification of PipelineConfigTemplate
	Spec PipelineConfigTemplateSpec `json:"spec"`
}

// PipelineConfigTemplateSpec specified for  PipelineConfigTemplate
type PipelineConfigTemplateSpec struct {
	// Engine is a engine for render PipelineTemplate
	// +optional
	Engine PipelineTemplateEngine `json:"engine"`
	// WithSCM indicate if pipeline needs a scm
	// +optional
	WithSCM bool `json:"withSCM"`
	// Agent agent will indicates where the pipeline will running
	// +optional
	Agent *JenkinsAgent `json:"agent"`
	// Stages contains all stages for a pipeline
	Stages []PipelineStageInstance `json:"stages"`
	// Parameters is for execute process usage
	// +optional
	Parameters []PipelineParameter `json:"parameters"`
	// Arguments contains all arguments need by template
	// +optional
	Arguments []PipelineTemplateArgumentGroup `json:"arguments"`
	// Dependencies indicates plugins denpendencies of task
	// +optional
	Dependencies *PipelineDependency `json:"dependencies"`
}

// PipelineStageInstance is a instance of PipelineStage
type PipelineStageInstance struct {
	// Name is the name a stage
	Name string `json:"name"`
	// Tasks contains all task include in a stage
	Tasks []PipelineTemplateTaskInstance `json:"tasks"`
}

// PipelineTemplateTaskInstance is a instance of PipelineTemplateTask
type PipelineTemplateTaskInstance struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`

	Spec PipelineTemplateTaskInstanceSpec `json:"spec"`
}

// PipelineTemplateTaskInstanceSpec specified PipelineTemplateTaskInstance
type PipelineTemplateTaskInstanceSpec struct {
	// Agent specific task running agent
	// +optional
	Agent *JenkinsAgent `json:"agent"`
	// Engine will render task template
	// +optional
	Engine PipelineTemplateTaskEngine `json:"engine"`
	// Type is type of task
	Type string `json:"type"`
	// Body is the body of a pipeline
	Body string `json:"body"`
	// Options is option for task
	// +optional
	Options *PipelineTaskOption `json:"options"`
	// Approve mean task need to approve
	// +optional
	Approve *PipelineTaskApprove `json:"approve"`
	// Environments is variables for pipeline
	// +optional
	Environments []PipelineEnvironment `json:"environments"`
	// Exports mean task will export some variables
	// +optional
	Exports []GlobalParameter `json:"exports"`
	// Arguments contains all arguments include in templates
	// +optional
	Arguments []PipelineTemplateArgument `json:"arguments"`
}

// PipelineStrategyJenkins jenkins execution strategy
type PipelineStrategyJenkins struct {
	// Jenkinsfile approves the jenkinsfile script will be executed.
	// +optional
	Jenkinsfile string `json:"jenkinsfile,omitempty"`
	// JenkinsfilePath is the jenkinsfile path in the code repository.
	// +optional
	JenkinsfilePath string `json:"jenkinsfilePath,omitempty"`
}

// PipelineHook pipeline hook definition
type PipelineHook struct {
	// Type is the type of the pipeline hook.
	// Now just supports "httpRequest".
	// +optional
	Type PipelineHookType `json:"type"`
	// Events is a list of events of the pipeline changed.
	// +optional
	Events []PipelineEvent `json:"events"`
	// HTTPRequest is the httpRequest of the pipeline hook.
	// +optional
	HTTPRequest *PipelineHookHTTPRequest `json:"httpRequest,omitempty"`
}

// PipelineHookType pipeline hook types
type PipelineHookType string

const (
	// PipelineHookTypeHTTPRequest httpRequest type for sending requests
	PipelineHookTypeHTTPRequest PipelineHookType = "httpRequest"
)

// PipelineEvent pipeline event types
type PipelineEvent string

const (
	// PipelineEventConfigCreated event for creating PipelineConfig
	PipelineEventConfigCreated PipelineEvent = "PipelineConfigCreated"
	// PipelineEventConfigUpdated event for updating PipelineConfig
	PipelineEventConfigUpdated PipelineEvent = "PipelineConfigUpdated"
	// PipelineEventConfigDeleted event for deleting PipelineConfig
	PipelineEventConfigDeleted PipelineEvent = "PipelineConfigDeleted"
	// PipelineEventPipelineStarted event for starting PipelineConfig
	PipelineEventPipelineStarted PipelineEvent = "PipelineStarted"
	// PipelineEventPipelineStopped event for stoping PipelineConfig
	PipelineEventPipelineStopped PipelineEvent = "PipelineStopped"
)

// PipelineHookHTTPRequest HTTP request hook type
type PipelineHookHTTPRequest struct {
	// URI is the uri of the http request.
	// +optional
	URI string `json:"uri"`
	// Method is the method of the http request.
	// +optional
	Method string `json:"method"`
	// Headers is the header of the http request.
	// +optional
	Headers map[string]string `json:"headers"`
}

// PipelineSource source code specification for Pipeline
type PipelineSource struct {
	// CodeRepository contains git url and user info
	// +optional
	CodeRepository *CodeRepositoryRef `json:"codeRepository,omitempty"`
	// Git is the git code repository settings of the pipeline source.
	// +optional
	Git *PipelineSourceGit `json:"git,omitempty"`
	// Secret is the secret to access the code repository.
	// +optional
	Secret *LocalObjectReference `json:"secret,omitempty"`
}

// PipelineSourceGit generic git implementation for PipelineSource
type PipelineSourceGit struct {
	// URI is the uri of the git code repository.
	// +optional
	URI string `json:"uri"`
	// URI is the branch of the git code repository.
	// +optional
	Ref string `json:"ref"`
}

// CodeRepositoryRef is reference of CodeRepostiory
type CodeRepositoryRef struct {
	LocalObjectReference `json:",inline"`
	// Ref is branch of git repo
	Ref string `json:"ref"`
}

// PipelineConfigStatus represents PipelineConfig's status
// PipelineConfigStatus defines PipelineConfig's status
type PipelineConfigStatus struct {
	// Current condition of the pipelineConfig.
	// +optional
	Phase PipelineConfigPhase `json:"phase"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// Last time we update the pipelineConfig.
	// +optional
	LastUpdate *metav1.Time `json:"lastUpdated"`
	// A list of condition referenced to the pipelineConfig.
	// +optional
	Conditions []Condition `json:"conditions,omitempty"`
}

// Condition generic condition for devops objects
type Condition struct {
	// Type is the type of the condition.
	// +optional
	Type string `json:"type"`
	// Last time we probed the condition.
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// Status is the status of the condition.
	// +optional
	Status string `json:"status"`
}

// PipelineConfigPhase a phase for PipelineConfigStatus
type PipelineConfigPhase string

const (
	// PipelineConfigPhaseCreating creating phase of PipelineConfig
	PipelineConfigPhaseCreating PipelineConfigPhase = StatusCreating
	// PipelineConfigPhaseSyncing syncing phase of PipelineConfig
	PipelineConfigPhaseSyncing PipelineConfigPhase = StatusSyncing
	// PipelineConfigPhaseReady ready phase of PipelineConfig
	// this means that the PipelineConfig was already synced
	// to the destinatination service
	PipelineConfigPhaseReady PipelineConfigPhase = StatusReady
	// PipelineConfigPhaseError error phase for the PipelineConfig
	// this means that some error happened during the syncing phase
	// and added the related errors to the Reason and Message
	PipelineConfigPhaseError PipelineConfigPhase = StatusError
	// PipelineConfigPhaseDisabled error phase for the PipelineConfig
	// this means the pipelineconfig is disable for the coderepository deleted
	PipelineConfigPhaseDisabled PipelineConfigPhase = StatusDisabled
)

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineConfigList is a list of PipelineConfig objects.
type PipelineConfigList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items is a list of PipelineConfig objects.
	Items []PipelineConfig `json:"items"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Pipeline struct holds a reference to a specific pipeline run
type Pipeline struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the Pipeline.
	// +optional
	Spec PipelineSpec `json:"spec"`
	// Most recently observed status of the Pipeline.
	// Populated by the system.
	// Read-only.
	// +optional
	Status PipelineStatus `json:"status"`
}

// PipelineSpec specifications for a PipelineConfig
type PipelineSpec struct {
	// JenkinsBinding is the jenkinsBinding of the pipeline.
	// +optional
	JenkinsBinding LocalObjectReference `json:"jenkinsBinding"`
	// PipelineConfig is the pipelineConfig of the pipeline.
	// +optional
	PipelineConfig LocalObjectReference `json:"pipelineConfig"`
	// Cause is the cause of the pipeline.
	// +optional
	Cause PipelineCause `json:"cause"`
	// RunPolicy is the runPolicy of the pipeline.
	// +optional
	RunPolicy PipelineRunPolicy `json:"runPolicy"`
	// Parameters is the parameters of the pipeline.
	// +optional
	Parameters []PipelineParameter `json:"parameters"`
	// Triggers is the triggers of the pipeline.
	// +optional
	Triggers []PipelineTrigger `json:"triggers"`
	// Strategy is the strategy of the pipeline.
	// +optional
	Strategy PipelineStrategy `json:"strategy"`
	// Hooks is the hooks of the pipeline.
	// +optional
	Hooks []PipelineHook `json:"hooks"`
	// Source is the source of the pipeline.
	// +optional
	Source PipelineSource `json:"source"`
}

// PipelineCause describe the cause for a pipeline trigger
type PipelineCause struct {
	// Type is the type of the pipeline pipelineCause.
	// One of "manual"、"cron"、"codeChange".
	// +optional
	Type PipelineCauseType `json:"type"`
	// Human-readable message indicating details about a pipeline cause.
	// +optional
	Message string `json:"message"`
}

// PipelineCauseType pipeline run start cause
type PipelineCauseType string

const (
	// PipelineCauseTypeManual manual execution by user
	PipelineCauseTypeManual PipelineCauseType = "manual"
	// PipelineCauseTypeCron cron timer execution
	PipelineCauseTypeCron PipelineCauseType = "cron"
	// PipelineCauseTypeCodeChange code change execution
	PipelineCauseTypeCodeChange PipelineCauseType = "codeChange"
)

// PipelineStatus pipeline status
type PipelineStatus struct {
	// Current condition of the pipeline.
	// +optional
	Phase PipelinePhase `json:"phase"`
	// StartedAt is the start time of the pipeline.
	// +optional
	StartedAt *metav1.Time `json:"startedAt"`
	// FinishedAt is finish time of the pipeline.
	// +optional
	FinishedAt *metav1.Time `json:"finishedAt"`
	// UpdatedAt is the update time of the pipeline.
	// +optional
	UpdatedAt *metav1.Time `json:"updatedAt"`
	// Jenkins is the status of the jenkins this pipeline used.
	// +optional
	Jenkins *PipelineStatusJenkins `json:"jenkins,omitempty"`
	// Aborted is aborted status of the pipeline trigger.
	// +optional
	Aborted bool `json:"aborted"`
}

// PipelinePhase a phase for PipelineStatus
type PipelinePhase string

// IsValid check whether the pipeline is valid or not.
func (phase PipelinePhase) IsValid() bool {
	switch phase {
	case PipelinePhasePending, PipelinePhaseQueued, PipelinePhaseRunning,
		PipelinePhaseComplete, PipelinePhaseFailed,
		PipelinePhaseError, PipelinePhaseCancelled, PipelinePhaseAborted:
		return true
	}
	return false
}

// IsFinalPhase check whether the pipeline is finished.
func (phase PipelinePhase) IsFinalPhase() bool {
	switch phase {
	case PipelinePhaseComplete, PipelinePhaseFailed, PipelinePhaseError,
		PipelinePhaseCancelled, PipelinePhaseAborted:
		return true
	}
	return false
}

const (
	// PipelinePhasePending created but not yet sinced
	PipelinePhasePending PipelinePhase = "Pending"
	// PipelinePhaseQueued entered in the jenkins queue
	PipelinePhaseQueued PipelinePhase = "Queued"
	// PipelinePhaseRunning started execution
	PipelinePhaseRunning PipelinePhase = "Running"
	// PipelinePhaseComplete finished execution
	PipelinePhaseComplete PipelinePhase = "Complete"
	// PipelinePhaseFailed finished execution but failed
	PipelinePhaseFailed PipelinePhase = "Failed"
	// PipelinePhaseError finished execution but failed
	PipelinePhaseError PipelinePhase = "Error"
	// PipelinePhaseCancelled paused execution
	PipelinePhaseCancelled PipelinePhase = "Cancelled"
	// PipelinePhaseAborted when user aborts a pipeline while in queue
	PipelinePhaseAborted PipelinePhase = "Aborted"
)

// PipelineStatusJenkins used to store jenkins related information
type PipelineStatusJenkins struct {
	// Result is the result of the jenkins.
	// +optional
	Result string `json:"result"`
	// Status is the status of the jenkins.
	// +optional
	Status string `json:"status"`
	// Build is the build of the jenkins.
	// +optional
	Build string `json:"build"`
	// Stages is the stages of the jenkins.
	// +optional
	Stages string `json:"stages"`
	// StartStageID is the startStageID of the jenkins.
	// +optional
	StartStageID string `json:"startStageID"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineList is a list of Pipeline objects.
type PipelineList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of Pipeline objects.
	Items []Pipeline `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineLog used to retrieve logs from a pipeline
type PipelineLog struct {
	metav1.TypeMeta `json:",inline"`

	// True means has more log behind。
	// +optional
	HasMore bool `json:"more"`
	// NextStart is next start number to fetch new log.
	// +optional
	NextStart *int64 `json:"nextStart,omitempty"`
	// Text is the context of the log.
	// +optional
	Text string `json:"text"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineLogOptions used to fetch logs from a pipeline
type PipelineLogOptions struct {
	metav1.TypeMeta `json:",inline"`

	// Start is the start number to fetch the log.
	// +optional
	Start int64 `json:"start"`

	// Stage if given will limit the log to a specific stage
	// +optional
	Stage int64 `json:"stage"`

	// Step if given will limit the log to a specific step
	// +optional
	Step int64 `json:"step"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTask retrieve steps or stages from a pipeline
type PipelineTask struct {
	metav1.TypeMeta `json:",inline"`

	// Tasks steps/stages for a Pipeline
	Tasks []PipelineBlueOceanTask `json:"tasks"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTaskOptions options for requesting stage/steps from jenkins blue ocean
type PipelineTaskOptions struct {
	metav1.TypeMeta `json:",inline"`

	// Stage indicates the stage id to fetch the step list
	// if not provided will fetch the stage list
	// +optional
	Stage int64 `json:"stage"`
}

// PipelineBlueOceanTask a task from BlueOcean API
type PipelineBlueOceanTask struct {
	// extends PipelineBlueOceanRef
	PipelineBlueOceanRef
	// DisplayDescription description for step/stage
	DisplayDescription string `json:"displayDescription"`
	// DisplayName is a display name for step/stage
	// +optional
	DisplayName string `json:"displayName"`
	// Duration in milliseconds
	// +optional
	DurationInMillis int64 `json:"durationInMillis"`
	// Input describes a input for Jenkins step
	// +optional
	Input *PipelineBlueOceanInput `json:"input"`

	// Result describes a result for a stage/step in Jenkins
	Result string `json:"result"`
	// Stage describe the current state of the stage/step in Jenkins
	State string `json:"state"`
	// StartTime the starting time for the stage/step
	// +optional
	StartTime string `json:"startTime,omitempty"`

	// Edges edges for a specific stage
	// +optional
	Edges []PipelineBlueOceanRef `json:"edges,omitempty"`
	// Actions
	// +optional
	Actions []PipelineBlueOceanRef `json:"actions,omitempty"`
}

// PipelineBlueOceanRef reference of a class/resource
type PipelineBlueOceanRef struct {
	// Href reference url for resource
	// +optional
	Href string `json:"href,omitempty"`
	// ID unique identifier for step/stage
	// +optional
	ID string `json:"id,omitempty"`
	// Type describes the resource type
	// +optional
	Type string `json:"type,omitempty"`
	// URLName describes a url name for the resource
	// +optional
	URLName string `json:"urlName,omitempty"`

	// Description description for reference
	// +optional
	Description string `json:"description,omitempty"`
	// Name name for reference
	// +optional
	Name string `json:"name,omitempty"`

	// Value for reference
	// +optional
	Value string `json:"value,omitempty"`
}

// PipelineBlueOceanInput describes a Jenkins input for a step
type PipelineBlueOceanInput struct {
	// extends PipelineBlueOceanRef
	PipelineBlueOceanRef
	// Message describes the message for the input
	Message string `json:"message"`
	// OK describes which option is used for successful submit
	OK string `json:"ok"`

	// Parameters parameters for input
	// +optional
	Parameters []PipelineBlueOceanParameter `json:"parameters,omitempty"`
	// Submitter list of usernames or user ids that can approve
	// +optional
	Submitter string `json:"submitter"`
}

// PipelineBlueOceanParameter one step parameter for Jenkins step
type PipelineBlueOceanParameter struct {
	PipelineBlueOceanRef
	// DefaultParameterValue type and default value for parameter
	// +optional
	DefaultParameterValue PipelineBlueOceanRef `json:"defaultParameterValue"`
}

// PipelineConfigData defines the old and new config info.
type PipelineConfigData struct {
	// Old is the old pipelineConfig info.
	// +optional
	Old *PipelineConfig `json:"old"`
	// New is the old pipelineConfig info.
	// +optional
	New *PipelineConfig `json:"new"`
}

// PipelineConfigPayload defines pipelineConfig payload in event.
type PipelineConfigPayload struct {
	// Event is the event of the payload.
	// +optional
	Event PipelineEvent `json:"event"`
	// Data is the data of the payload.
	// +optional
	Data PipelineConfigData `json:"data"`
}

type PipelineData struct {
	// Old is the old pipeline info.
	// +optional
	Old *Pipeline `json:"old"`
	// New is the new pipeline info.
	// +optional
	New *Pipeline `json:"new"`
}

// PipelinePayload defines pipeline payload in event.
type PipelinePayload struct {
	// Event is the event of the payload.
	// +optional
	Event PipelineEvent `json:"event"`
	// Data is the data of the payload.
	// +optional
	Data PipelineData `json:"data"`
}

// HasPipelineEvent check whether include events
func (ph *PipelineHook) HasPipelineEvent(event PipelineEvent) bool {
	if ph.Events == nil || len(ph.Events) == 0 {
		return false
	}

	for _, e := range ph.Events {
		if e == event {
			return true
		}
	}
	return false
}

// GetLastNumber get the last trigger number
func (p *PipelineConfig) GetLastNumber() (lastNumber string) {
	annotations := p.GetAnnotations()
	if annotations == nil || len(annotations) == 0 {
		return
	}

	lastNumber, _ = annotations[AnnotationsKeyPipelineLastNumber]
	return
}

// endregion

// region CodeRepository

// CodeRepoServiceType type for the repo service
type CodeRepoServiceType string

const (
	// CodeRepoServiceTypeGithub github
	CodeRepoServiceTypeGithub CodeRepoServiceType = "Github"
	// CodeRepoServiceTypeGitlab gitlab
	CodeRepoServiceTypeGitlab CodeRepoServiceType = "Gitlab"
	// CodeRepoServiceTypeGitee gitee
	CodeRepoServiceTypeGitee CodeRepoServiceType = "Gitee"
	// CodeRepoServiceTypeBitbucket bitbucket
	CodeRepoServiceTypeBitbucket CodeRepoServiceType = "Bitbucket"
)

func (c CodeRepoServiceType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoService struct holds a reference to a specific CodeRepoService configuration
// and some user data for access
type CodeRepoService struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the CodeRepoService.
	// +optional
	Spec CodeRepoServiceSpec `json:"spec"`
	// Most recently observed status of the CodeRepoService.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// GetHost get the host defined in the service.
func (c *CodeRepoService) GetHost() string {
	return c.Spec.HTTP.Host
}

// GetHost get the html defined in the service.
func (c *CodeRepoService) GetHtml() string {
	switch c.Spec.Type {
	case CodeRepoServiceTypeGitlab, CodeRepoServiceTypeGitee:
		return c.Spec.HTTP.Host
	case CodeRepoServiceTypeGithub:
		return GithubHTML
	case CodeRepoServiceTypeBitbucket:
		return BitbucketHTML
	default:
		return fmt.Sprintf("invalid codeRepoService type %s", c.Spec.Type)
	}
}

// GetType get the type of the service.
func (c *CodeRepoService) GetType() CodeRepoServiceType {
	return c.Spec.Type
}

// IsPublic defines whether the service is public.
func (c *CodeRepoService) IsPublic() bool {
	return c.Spec.Public
}

// CodeRepoServiceSpec defines CodeRepoService's specs
type CodeRepoServiceSpec struct {
	// HTTP defines the http.
	HTTP HostPort `json:"http"`
	// Type defines the service type.
	Type CodeRepoServiceType `json:"type"`
	// Public defines the public of the service.
	// +optional
	Public bool `json:"public"`
	// Data defines the data.
	// +optional
	Data map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceList is a list of CodeRepoService objects.
type CodeRepoServiceList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items is a list of CodeRepoService.
	Items []CodeRepoService `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeResponse used to validate secret used in service
type CodeRepoServiceAuthorizeResponse struct {
	metav1.TypeMeta

	// AuthorizeUrl specifies the authorizeUrl used to authorize web page.
	// +optional
	AuthorizeUrl string `json:"authorizeUrl"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeOptions used to fetch service options
type CodeRepoServiceAuthorizeOptions struct {
	metav1.TypeMeta

	// SecretName specifies the name of the secret.
	// +optional
	SecretName string `json:"secretName"`
	// Namespace specifies the namespace of the secret.
	// +optional
	Namespace string `json:"namespace"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBinding struct holds a reference to a specific CodeRepoBinding configuration
// and some user data for access
type CodeRepoBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the CodeRepoBinding.
	// +optional
	Spec CodeRepoBindingSpec `json:"spec"`
	// Most recently observed status of the CodeRepoBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// GetAccountName get account name.
func (c *CodeRepoBinding) GetAccountName() string {
	return c.Spec.Account.Name
}

// CodeRepoBindingSpec defines CodeRepoBinding's spec
type CodeRepoBindingSpec struct {
	// CodeRepoService defines the codeRepoService.
	CodeRepoService LocalObjectReference `json:"codeRepoService"`
	// Account defines the account.
	Account CodeRepoBindingAccount `json:"account"`
}

// BindingCondition defines the resource associated with the binding.
// The binding controller will check the status of the resource periodic and change it's status.
// The resource can be found by "name"+"type"+"binding's namespace"
type BindingCondition struct {
	// Name defines the name.
	// +optional
	Name string `json:"name"`
	// Type defines the type.
	// +optional
	Type string `json:"type"`
	// Last time we probed the condition.
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// Status defines the status.
	// +optional
	Status string `json:"status"`
}

// CodeRepoBindingAccount defines CodeRepoBinding' status
type CodeRepoBindingAccount struct {
	// Name defines the Name.
	Name string `json:"name"`
	// Secret defines the secret.
	// +optional
	Secret SecretKeySetRef `json:"secret"`
	// Owners defines the owners in this account.
	// +optional
	Owners []CodeRepositoryOwnerSync `json:"owners"`
}

// CodeRepositoryOwnerSync defines owner which will be synced.
type CodeRepositoryOwnerSync struct {
	// Type defines the type.
	Type OriginCodeRepoOwnerType `json:"type"`
	// Name defines the name.
	Name string `json:"name"`
	// All defines whether sync all repositories in this owner.
	// +optional
	All bool `json:"all"`
	// Repositories defines the repositories will be synced.
	// +optional
	Repositories []string `json:"repositories"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingList is a list of CodeRepoBinding objects.
type CodeRepoBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items is a list of CodeRepoBinding objects
	Items []CodeRepoBinding `json:"items"`
}

const (
	GithubOrgKey    = "Organization"
	GitlabOrgKey    = "group"
	BitbucketOrgKey = "team"
	// gitee was not defined here because of no org key in response.
)

// GetOwnerType unify the owner type from different code repository service.
func GetOwnerType(strOwnerType string) OriginCodeRepoOwnerType {
	switch strOwnerType {
	case GithubOrgKey, GitlabOrgKey, BitbucketOrgKey:
		return OriginCodeRepoRoleTypeOrg
	default:
		return OriginCodeRepoOwnerTypeUser
	}
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepository struct holds a reference to a specific CodeRepository configuration
// and some user data for access
type CodeRepository struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the CodeRepository.
	// +optional
	Spec CodeRepositorySpec `json:"spec"`
	// Most recently observed status of the CodeRepository.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// CodeRepositorySpec defines CodeRepository's specs
type CodeRepositorySpec struct {
	// CodeRepoBinding defines the codeRepoBinding
	CodeRepoBinding LocalObjectReference `json:"codeRepoBinding"`
	// Repository defines the repository
	Repository OriginCodeRepository `json:"repository"`
}

// GetServiceName get service name
func (c *CodeRepository) GetServiceName() string {
	repoLabels := c.GetLabels()
	if repoLabels == nil {
		return ""
	}

	if serviceName, ok := repoLabels[LabelCodeRepoService]; ok {
		return serviceName
	}
	return ""
}

// GetRepoFullName defines repository's fullname. eg: "alauda/devops/devops-1/sy-prj4"
func (c *CodeRepository) GetRepoFullName() string {
	return c.Spec.Repository.FullName
}

// GetRepoID defines repository's id
func (c *CodeRepository) GetRepoID() string {
	return c.Spec.Repository.ID
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepositoryList is a list of CodeRepository objects.
type CodeRepositoryList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items list of CodeRepository objects.
	Items []CodeRepository `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingRepositoryOptions used to fetch service options
type CodeRepoBindingRepositoryOptions struct {
	metav1.TypeMeta `json:",inline"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingRepositories used to retrieve logs from a pipeline
type CodeRepoBindingRepositories struct {
	metav1.TypeMeta `json:",inline"`

	// CodeRepoServiceType defines the service type.
	Type CodeRepoServiceType `json:"type"`
	// Owners a list of owner can be fetched from a binding.
	// +optional
	Owners []CodeRepositoryOwner `json:"owners"`
}

// CodeRepositoryOwner defines the repository owner.
type CodeRepositoryOwner struct {
	// Type defines the owner type.
	Type OriginCodeRepoOwnerType `json:"type"`
	// ID defines the id.
	ID string `json:"id"`
	// Name defines the name.
	Name string `json:"name"`
	// Email defines the email.
	// +optional
	Email string `json:"email"`
	// HTMLURL defines the htmlURL.
	// +optional
	HTMLURL string `json:"htmlURL"`
	// AvatarURL defines the avatarURL.
	// +optional
	AvatarURL string `json:"avatarURL"`
	// DiskUsage defines the diskUsage.
	// +optional
	DiskUsage int `json:"diskUsage"`
	// Repositories defines the repositories in this owner.
	// +optional
	Repositories []OriginCodeRepository `json:"repositories"`
}

type OriginCodeRepository struct {
	// CodeRepoServiceType defines the service type.
	CodeRepoServiceType CodeRepoServiceType `json:"codeRepoServiceType"`
	// ID defines the id.
	ID string `json:"id"`
	// Name defines the name.
	Name string `json:"name"`
	// FullName defines the full name
	// +optional
	FullName string `json:"fullName"`
	// Description defines the description.
	// +optional
	Description string `json:"description"`
	// HTMLURL defines the html url.
	// +optional
	HTMLURL string `json:"htmlURL"`
	// CloneURL defines the clone url which begins with "https://" or "http://".
	CloneURL string `json:"cloneURL"`
	// SSHURL defines ssh clone url which begins with "git@".
	SSHURL string `json:"sshURL"`
	// Language defines the language.
	// +optional
	Language string `json:"language"`
	// Owner defines the owner.
	Owner OwnerInRepository `json:"owner"`
	// LastCommitID defines the last commit id.
	// +optional
	LastCommitID string `json:"lastCommitID"`
	// LastCommitAt defines the last commit time.
	// +optional
	LastCommitAt string `json:"lastCommitAt"`
	// CreatedAt defines the created time.
	CreatedAt *metav1.Time `json:"createdAt"`
	// PushedAt defines the pushed time.
	// +optional
	PushedAt *metav1.Time `json:"pushedAt"`
	// UpdatedAt defines the updated time.
	// +optional
	UpdatedAt *metav1.Time `json:"updatedAt"`
	// Private defines the visibility of the repository.
	// +optional
	Private bool `json:"private"`
	// Size defines the size of the repository used.
	// +optional
	Size int `json:"size"`
	// Size defines the size of the repository used which is humanize.
	// +optional
	SizeHumanize string `json:"sizeHumanize"`
	// Data defines reserved fields for the repository
	// +optional
	Data map[string]string `json:"data"`
}

// OwnerInRepository defines the owner in repository which includes fewer properties.
type OwnerInRepository struct {
	// OriginCodeRepoOwnerType defines the type of the owner
	// +optional
	Type OriginCodeRepoOwnerType `json:"type"`
	// ID defines the id of the owner
	// +optional
	ID string `json:"id"`
	// Name defines the name of the owner
	// +optional
	Name string `json:"name"`
}

// OriginCodeRepoOwnerType defines the owner type of the repository
type OriginCodeRepoOwnerType string

const (
	// OriginCodeRepoOwnerTypeUser defines the owner type of user
	OriginCodeRepoOwnerTypeUser OriginCodeRepoOwnerType = "User"
	// OriginCodeRepoRoleTypeOrg defines the owner type of org
	OriginCodeRepoRoleTypeOrg OriginCodeRepoOwnerType = "Org"
)

// endregion

// region PipelineTemplate
// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplateSync specified a PipelineTemplate sync setting
type PipelineTemplateSync struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Spec specification for PipelineTemplateSync
	Spec PipelineTemplateSyncSpec `json:"spec"`
	// Status indicate status of PipelineTemplateSync
	// +optional
	Status *PipelineTemplateSyncStatus `json:"status"`
}

// PipelineTemplateSyncSpec represents PipelineTemplateSync's specs
type PipelineTemplateSyncSpec struct {
	// Source git source
	Source PipelineSource `json:"source"`
}

// PipelineTemplateSyncStatus represents PipelineTemplateSync's status
type PipelineTemplateSyncStatus struct {
	// Phase describe phase of PipelineTemplateSync
	Phase PipelineTemplateSyncPhase `json:"phase"`
	// Message is message in the process of sync
	// +optional
	Message string `json:"message"`
	// Conditions contains all file will be synced
	// +optional
	Conditions []PipelineTemplateSyncCondition `json:"conditions"`
	// StartTime is the time of start sync process
	StartTime metav1.Time `json:"startTime"`
	// EndTime is the time of end sync process
	// +optional
	EndTime metav1.Time `json:"endTime"`
	// CommitID is git commit log id
	// +optional
	CommitID string `json:"commitID"`
}

// PipelineTemplateSyncPhase a phase of PipelineTemplateSyncStatus
type PipelineTemplateSyncPhase string

const (
	// PipelineTemplateSyncPhaseDraft draft phase of PipelineTemplateSync
	PipelineTemplateSyncPhaseDraft PipelineTemplateSyncPhase = "Draft"
	// PipelineTemplateSyncPhasePending pending phase of PipelineTemplateSync
	PipelineTemplateSyncPhasePending PipelineTemplateSyncPhase = "Pending"
	// PipelineTemplateSyncPhaseReady ready phase of PipelineTemplateSync
	PipelineTemplateSyncPhaseReady PipelineTemplateSyncPhase = "Ready"
	// PipelineTemplateSyncPhaseError error phase of PipelineTemplateSync
	PipelineTemplateSyncPhaseError PipelineTemplateSyncPhase = "Error"
)

// PipelineTemplateSyncCondition represent for one record for sync
type PipelineTemplateSyncCondition struct {
	// LastTransitionTime
	// +optional
	LastTransitionTime metav1.Time `json:"lastTransitionTime"`
	// LastUpdateTime
	// +optional
	LastUpdateTime metav1.Time `json:"lastUpdateTime"`
	// Message contains describe message for sync process
	// +optional
	Message string `json:"message"`
	// Reason is the reason for success or failure
	// +optional
	Reason string `json:"reason"`
	// Status is status of sync process
	Status SyncStatus `json:"status"`
	// Type is the type of template
	Type string `json:"type"`
	// Target represent target template file relative path
	Target string `json:"target"`
	// Name is the name template
	Name string `json:"name"`
	// Version is the version of template file
	Version string `json:"version"`
	// PreviousVersion is the version of previous template file
	PreviousVersion string `json:"previousVersion"`
}

// SyncStatus represent status of sync
type SyncStatus string

// IsValid validates for SyncStatus
func (status SyncStatus) IsValid() bool {
	switch status {
	case SyncStatusSuccess:
	case SyncStatusFailure:
	case SyncStatusSkip:
		return true
	}

	return false
}

const (
	// SyncStatusSuccess show the process is ok
	SyncStatusSuccess SyncStatus = "Success"
	// SyncStatusFailure show the process is failure
	SyncStatusFailure SyncStatus = "Failure"
	// SyncStatusSkip show the process is kip
	SyncStatusSkip SyncStatus = "Skip"
	// SyncStatusDeleted show the process is deleted
	SyncStatusDeleted SyncStatus = "Deleted"
)

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplateSyncList is a list of PipelineTemplateSync
type PipelineTemplateSyncList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items hold all templates for sync
	// +optional
	Items []PipelineTemplateSync `json:"items"`
}

// endregion

// HostPort defines a host/port  construct
type HostPort struct {
	// Host defines the host.
	// +optional
	Host string `json:"host"`
}

// ServiceStatusPhase defines the repo status
type ServiceStatusPhase string

const (
	// ServiceStatusPhaseCreating means the resource is creating
	ServiceStatusPhaseCreating ServiceStatusPhase = StatusCreating
	// ServiceStatusPhaseReady means the connection is ok
	ServiceStatusPhaseReady ServiceStatusPhase = StatusReady
	// ServiceStatusPhaseError means the connection is bad
	ServiceStatusPhaseError ServiceStatusPhase = StatusError
	// ServiceStatusPhaseWaitingToDelete means the resource will be deleted when no resourced reference
	ServiceStatusPhaseWaitingToDelete ServiceStatusPhase = StatusWaitingToDelete
	// ServiceStatusPhaseListTagError means registry list tag detail error
	ServiceStatusPhaseListTagError ServiceStatusPhase = StatusListTagError
)

// ServiceStatus defines the status of the service.
type ServiceStatus struct {
	// Current condition of the service.
	// One of: "Creating" or "Ready" or "Error" or "WaitingToDelete".
	// +optional
	Phase ServiceStatusPhase `json:"phase"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// LastUpdate is the latest time when updated the service.
	// +optional
	LastUpdate *metav1.Time `json:"lastUpdated"`
	// HTTPStatus is http status of the service.
	// +optional
	HTTPStatus *HostPortStatus `json:"http,omitempty"`
	// Conditions is a list of BindingCondition objects.
	// +optional
	Conditions []BindingCondition `json:"conditions"`
}

// DisplayName defines a set of readable names
type DisplayName struct {
	// EN is a human readable Chinese name.
	EN string `json:"en"`
	// ZH is a human readable English name.
	ZH string `json:"zh"`
}

// region ToolChain
// ToolChainElement defines the element of the tool chain.
type ToolChainElement struct {
	// Name is the name of the element
	// +optional
	Name string `json:"name"`
	// DisplayName is a set of humanize names
	// +optional
	DisplayName DisplayName `json:"displayName"`
	// Enabled specifies the enabled of the element.
	// +optional
	Enabled bool `json:"enabled"`
	// Items is a list of items defined in the element.
	// +optional
	Items []*ToolChainElementItem `json:"items"`
}

// ToolChainElementItem defines the item of the tool chain element.
type ToolChainElementItem struct {
	// DisplayName is a set of humanize names
	// +optional
	DisplayName DisplayName `json:"displayName"`
	// Host is api address
	// +optional
	Host string `json:"host"`
	// HTML is web address
	// +optional
	HTML string `json:"html"`
	// Name is the name of the item
	// +optional
	Name string `json:"name"`
	// Kind is the kind of the item
	// +optional
	Kind string `json:"kind"`
	// Type is the type of the item
	// +optional
	Type string `json:"type"`
	// Public specifies the visibility of the item.
	// +optional
	Public bool `json:"public"`
	// Enterprise specifies whether the item is enterprise or not.
	// +optional
	Enterprise bool `json:"enterprise"`
	// Enabled specifies the enabled of the item.
	// +optional
	Enabled bool `json:"enabled"`
}

// endregion

// region ImageRegistry
type ImageRegistryType string

const (
	// RegistryTypeDocker, normal registry, deploy registry alone
	RegistryTypeDocker ImageRegistryType = "Docker"
)

func (c ImageRegistryType) String() string {
	return string(c)
}

type ImageRepoPhase string

const (
	// ImageRepoPhaseCreating, registry service binding phase is creating
	ImageRepoPhaseCreating ImageRepoPhase = StatusCreating
	// ImageRepoPhaseReady, registry service binding phase is ready
	ImageRepoPhaseReady ImageRepoPhase = StatusReady
	// ImageRepoPhaseListRepoError, could not list repo
	ImageRepoPhaseListRepoError ImageRepoPhase = StatusListRepoError
	// ImageRepoPhaseError, connect registry service error
	ImageRepoPhaseError ImageRepoPhase = StatusError
	// ImageRepoPhaseWaitingToDelete, will delete
	ImageRepoPhaseWaitingToDelete ImageRepoPhase = StatusWaitingToDelete
)

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// registry service
type ImageRegistry struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ImageRegistrySpec `json:"spec"`
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

// Get registry service endpoint
func (c *ImageRegistry) GetEndpoint() string {
	endpoint := c.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

// Get registry service type
func (c *ImageRegistry) GetType() ImageRegistryType {
	return c.Spec.Type
}

// Get registry service spec
type ImageRegistrySpec struct {
	HTTP HostPort          `json:"http"`
	Type ImageRegistryType `json:"type"`
	// +optional
	Data map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Get image registry list
type ImageRegistryList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ImageRegistry `json:"items"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// registry service binding
type ImageRegistryBinding struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ImageRegistryBindingSpec `json:"spec"`
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

// ImageRegistryBindingSpec represents ImageRegistry specs
type ImageRegistryBindingSpec struct {
	ImageRegistry LocalObjectReference `json:"imageRegistry"`
	// +optional
	Secret SecretKeySetRef `json:"secret"`
	// +optional
	RepoInfo ImageRegistryBindingRepo `json:"repoInfo"`
}

// ImageRegistryBindingRepo save the path of user choose
type ImageRegistryBindingRepo struct {
	// +optional
	Repositories []string `json:"repositories"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingList is a list of ImageRegistryBinding objects.
type ImageRegistryBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ImageRegistryBinding `json:"items"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRepository save image info, include image path and tag info
type ImageRepository struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ImageRepositorySpec `json:"spec"`
	// Read-only
	// +optional
	Status ImageRepositoryStatus `json:"status"`
}

// ImageRepositorySpec represents ImageRepository's specs
type ImageRepositorySpec struct {
	ImageRegistry        LocalObjectReference `json:"imageRegistry"`
	ImageRegistryBinding LocalObjectReference `json:"imageRegistryBinding"`
	Image                string               `json:"image"`
}

// ImageRepositoryStatus defines the status of the service, inherit ServiceStatus
type ImageRepositoryStatus struct {
	ServiceStatus
	// +optional
	Tags []ImageTag `json:"tags"`
}

// ImageTag defines the image tag ttributes
type ImageTag struct {
	Name      string       `json:"name"`
	Digest    string       `json:"digest"`
	CreatedAt *metav1.Time `json:"created_at"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRepositoryList is a list of ImageRepository objects.
type ImageRepositoryList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ImageRepository `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingRepositories used to retrieve repository from registry
type ImageRegistryBindingRepositories struct {
	metav1.TypeMeta `json:",inline"`

	Items []string `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingRepositoryOptions user to fetch registry binding options
type ImageRegistryBindingRepositoryOptions struct {
	metav1.TypeMeta `json:",inline"`

	// +optional
}

// GetRegistryName get service name
func (c *ImageRepository) GetRegistryName() string {
	repoLabels := c.GetLabels()
	if repoLabels == nil {
		return ""
	}

	if serviceName, ok := repoLabels[LabelImageRegistry]; ok {
		return serviceName
	}
	return ""
}

// endregion

// region ProjectManagement

// ProjectManagementType type for the ProjectManagement
type ProjectManagementType string

const (
	// ProjectManageTypeJira Jira
	ProjectManageTypeJira ProjectManagementType = "Jira"
	// ProjectManageTypeTaiga Taiga
	ProjectManageTypeTaiga ProjectManagementType = "Taiga"
)

func (c ProjectManagementType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagement struct holds ProjectManagement data
type ProjectManagement struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the ProjectManagement.
	// +optional
	Spec ProjectManagementSpec `json:"spec"`
	// Most recently observed status of the ProjectManagement.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// ProjectManagementSpec is the spec in ProjectManagement
type ProjectManagementSpec struct {
	// HTTP defines the http.
	// +optional
	HTTP HostPort `json:"http"`
	// Type defines the service type.
	Type ProjectManagementType `json:"type"`
	// Public defines the public of the service.
	// +optional
	Public bool `json:"public"`
	// Data defines the data.
	// +optional
	Data map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementList is a list of ProjectManagement objects.
type ProjectManagementList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of ProjectManagement
	Items []ProjectManagement `json:"items"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBinding is the binding referenced to ProjectManagement
type ProjectManagementBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the ProjectManagementBinding.
	// +optional
	Spec ProjectManagementBindingSpec `json:"spec"`
	// Most recently observed status of the ProjectManagementBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// ProjectManagementBindingSpec is the spec in ProjectManagementBinding
type ProjectManagementBindingSpec struct {
	ProjectManagement LocalObjectReference `json:"projectManagement"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBindingList is a list of ProjectManagementBinding objects.
type ProjectManagementBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of ProjectManagementBinding
	Items []ProjectManagementBinding `json:"items"`
}

// endregion

// region TestTool

// TestToolType type for the TestTool
type TestToolType string

const (
	// TestToolTypeRedwoodHQ RedwoodHQ
	TestToolTypeRedwoodHQ TestToolType = "RedwoodHQ"
)

func (c TestToolType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestTool struct holds TestTool data
type TestTool struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the TestTool.
	// +optional
	Spec TestToolSpec `json:"spec"`
	// Most recently observed status of the TestTool.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// TestToolSpec is the spec in TestTool
type TestToolSpec struct {
	// HTTP defines the http.
	// +optional
	HTTP HostPort `json:"http"`
	// Type defines the service type.
	Type TestToolType `json:"type"`
	// Public defines the public of the service.
	// +optional
	Public bool `json:"public"`
	// Data defines the data.
	// +optional
	Data map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolList is a list of TestTool objects.
type TestToolList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional

	// Items is a list of TestTool
	Items []TestTool `json:"items"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolBinding is the binding referenced to TestTool
type TestToolBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the TestToolBinding.
	// +optional
	Spec TestToolBindingSpec `json:"spec"`
	// Most recently observed status of the TestToolBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// TestToolBindingSpec is the spec in TestToolBinding
type TestToolBindingSpec struct {
	TestTool LocalObjectReference `json:"testTool"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolBindingList is a list of TestToolBinding objects.
type TestToolBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of TestToolBinding
	Items []TestToolBinding `json:"items"`
}

// endregion
