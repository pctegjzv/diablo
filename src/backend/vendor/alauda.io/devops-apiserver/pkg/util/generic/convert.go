package generic

import (
	"encoding/json"
	"github.com/golang/glog"
	"log"
)

func ConvertToMapString(obj interface{}) (result interface{}) {
	result = map[string]interface{}{}
	data, err := json.Marshal(obj)
	if err != nil {
		log.Println("marshal error", err)
		return nil
	}
	err = json.Unmarshal(data, &result)
	if err != nil {
		log.Println("unmarshal error", err)
	}
	return
}

// Bool is a helper routine that allocates a new bool value
// to store v and returns a pointer to it.
func Bool(v bool) *bool { return &v }

// Int is a helper routine that allocates a new int value
// to store v and returns a pointer to it.
func Int(v int) *int { return &v }

// Int64 is a helper routine that allocates a new int64 value
// to store v and returns a pointer to it.
func Int64(v int64) *int64 { return &v }

// String is a helper routine that allocates a new string value
// to store v and returns a pointer to it.
func String(v string) *string { return &v }

func GetPrettyString(result interface{}) string {
	js, err := json.MarshalIndent(&result, "", "\t")
	if err != nil {
		glog.Error(err)
	}
	return string(js)
}

func PrintJson(result interface{}) {
	js := GetPrettyString(result)
	glog.V(5).Infoln(string(js))
}
