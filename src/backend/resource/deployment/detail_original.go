package deployment

import (
	"fmt"
	"log"

	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/container"
	v1 "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	client "k8s.io/client-go/kubernetes"
)

func setTypeMeta(deploy *v1.Deployment) {
	if deploy == nil {
		return
	}
	deploy.TypeMeta.SetGroupVersionKind(schema.GroupVersionKind{
		Group:   "apps",
		Version: "v1",
		Kind:    "Deployment",
	})
}

func GetDeploymentDetailOriginal(client client.Interface, namespace string,
	deploymentName string) (*v1.Deployment, error) {

	log.Printf("Getting details of %s deployment in %s namespace", deploymentName, namespace)
	deploy, err := client.Apps().Deployments(namespace).Get(deploymentName, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}
	setTypeMeta(deploy)
	return deploy, nil
}

func UpdateDeploymentOriginal(client client.Interface, namespace string,
	deploymentName string, deploy *v1.Deployment) (*v1.Deployment, error) {
	old, err := GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return nil, err
	}
	deploy.ObjectMeta.ResourceVersion = old.ObjectMeta.ResourceVersion
	deploy.ObjectMeta.Generation = old.ObjectMeta.Generation
	deploy.ObjectMeta.UID = old.ObjectMeta.UID
	deploy, err = client.AppsV1().Deployments(namespace).Update(deploy)
	setTypeMeta(deploy)
	return deploy, err
}

type DeploymentReplica struct {
	Replicas int32 `json:"replicas"`
}

func UpdateDeploymentReplica(client client.Interface, namespace string,
	deploymentName string, replicas DeploymentReplica) (deploy *v1.Deployment, err error) {
	deploy, err = GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return
	}

	deploy.Spec.Replicas = &replicas.Replicas
	deploy, err = client.AppsV1().Deployments(namespace).Update(deploy)
	setTypeMeta(deploy)
	return
}

func DeleteObject(client client.Interface, obj runtime.Object) (name string, err error) {
	if deploy, ok := obj.(*v1.Deployment); ok {
		name = deploy.GetName()
		err = client.AppsV1().Deployments(deploy.GetNamespace()).Delete(deploy.GetName(), nil)
	} else {
		err = fmt.Errorf("Deployment runtime is not a valid object type: %v", obj)
	}
	return
}

// UpdateContainerEnv func
func UpdateContainerEnv(client client.Interface, namespace string,
	controllerName string, containerName string, ucer container.UpdateContainerEnvRequest) (deployment *v1.Deployment, err error) {
	deployment, err = GetDeploymentDetailOriginal(client, namespace, controllerName)
	if err != nil {
		return
	}
	for index, container := range deployment.Spec.Template.Spec.Containers {
		if containerName == container.Name {
			deployment.Spec.Template.Spec.Containers[index].Env = ucer.Env
			deployment.Spec.Template.Spec.Containers[index].EnvFrom = ucer.EnvFrom
			break
		}
	}
	deployment, err = client.AppsV1().Deployments(namespace).Update(deployment)
	return
}

// UpdateContainerImage func
func UpdateContainerImage(client client.Interface, namespace string,
	deploymentName string, containerName string, ucir container.UpdateContainerImageRequest) (deployment *v1.Deployment, err error) {
	deployment, err = GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return
	}
	for index, container := range deployment.Spec.Template.Spec.Containers {
		if containerName == container.Name {
			deployment.Spec.Template.Spec.Containers[index].Image = ucir.Image
			break
		}
	}
	deployment, err = client.AppsV1().Deployments(namespace).Update(deployment)
	return
}

// PutDeploymentContainer func
func PutDeploymentContainer(client client.Interface, namespace string,
	deploymentName string, containerName string, isDryRun bool, udsc container.UpdateContainerRequest) (deployment *v1.Deployment, err error) {
	deployment, err = GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return
	}
	usedValumeMap := make(map[string]bool)
	for index, container := range deployment.Spec.Template.Spec.Containers {
		if containerName == container.Name {

			deployment.Spec.Template.Spec.Containers[index].Image = udsc.Container.Image

			deployment.Spec.Template.Spec.Containers[index].Resources = udsc.Container.Resources

			deployment.Spec.Template.Spec.Containers[index].Env = udsc.Container.Env

			deployment.Spec.Template.Spec.Containers[index].EnvFrom = udsc.Container.EnvFrom

			deployment.Spec.Template.Spec.Containers[index].VolumeMounts = make([]core.VolumeMount, 0)
			for _, vi := range udsc.VolumeInfo {
				deployment.Spec.Template.Spec.Containers[index].VolumeMounts, deployment.Spec.Template.Spec.Volumes = common.CreateContainerVolumeMount(
					deployment.Spec.Template.Spec.Containers[index], deployment.Spec.Template.Spec.Volumes, *vi)
			}
			break
		}
	}

	for index := range deployment.Spec.Template.Spec.Containers {
		for _, vm := range deployment.Spec.Template.Spec.Containers[index].VolumeMounts {
			usedValumeMap[vm.Name] = true
		}
	}

	// remove useless volume
	newVolumes := make([]core.Volume, 0)
	for _, v := range deployment.Spec.Template.Spec.Volumes {
		if usedValumeMap[v.Name] {
			newVolumes = append(newVolumes, v)
		}
	}
	deployment.Spec.Template.Spec.Volumes = newVolumes
	if isDryRun {
		return
	}
	deployment, err = client.AppsV1().Deployments(namespace).Update(deployment)
	return
}

// CreateDeploymentVolumeMount func
func CreateDeploymentVolumeMount(client client.Interface, namespace string,
	deploymentName string, containerName string, vi common.VolumeInfo) (deploy *v1.Deployment, err error) {
	deploy, err = GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return
	}
	for index, container := range deploy.Spec.Template.Spec.Containers {
		if containerName == container.Name {
			deploy.Spec.Template.Spec.Containers[index].VolumeMounts, deploy.Spec.Template.Spec.Volumes = common.CreateContainerVolumeMount(
				container, deploy.Spec.Template.Spec.Volumes, vi)
			break
		}
	}
	deploy, err = client.AppsV1().Deployments(namespace).Update(deploy)
	return
}
