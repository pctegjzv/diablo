// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package deployment

import (
	"log"
	"sync"

	"alauda.io/diablo/src/backend/api"
	"alauda.io/diablo/src/backend/errors"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/dataselect"
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	extensions "k8s.io/api/extensions/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	client "k8s.io/client-go/kubernetes"
)

// RollingUpdateStrategy is behavior of a rolling update. See RollingUpdateDeployment K8s object.
type RollingUpdateStrategy struct {
	MaxSurge       *intstr.IntOrString `json:"maxSurge"`
	MaxUnavailable *intstr.IntOrString `json:"maxUnavailable"`
}

// StatusInfo struct
type StatusInfo struct {
	// Total number of desired replicas on the deployment
	Replicas int32 `json:"replicas"`

	// Number of non-terminated pods that have the desired template spec
	Updated int32 `json:"updated"`

	// Number of available pods (ready for at least minReadySeconds)
	// targeted by this deployment
	Available int32 `json:"available"`

	// Total number of unavailable pods targeted by this deployment.
	Unavailable int32 `json:"unavailable"`
}

// DeploymentDetail is a presentation layer view of Kubernetes Deployment resource.
type DeploymentDetail struct {
	ObjectMeta     api.ObjectMeta           `json:"objectMeta"`
	TypeMeta       api.TypeMeta             `json:"typeMeta"`
	PodInfo        common.PodControllerInfo `json:"podInfo"`
	Status         common.ControllerStatus  `json:"status"`
	VisitAddresses []string                 `json:"visitAddresses"`
	Containers     []core.Container         `json:"containers"`
	VolumeInfos    []common.VolumeInfos     `json:"volumeInfos"`
	UpdateStrategy apps.DeploymentStrategy  `json:"updateStrategy"`
	Data           *apps.Deployment         `json:"data"`
	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetObjectMeta func
func (detail DeploymentDetail) GetObjectMeta() api.ObjectMeta {
	return detail.ObjectMeta
}

// GetDeploymentDetailList func
func GetDeploymentDetailList(client client.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (*[]DeploymentDetail, error) {
	log.Print("Getting list of all deployments detail in the cluster")

	deploymentList, err := GetOriginalList(client, nsQuery, dsQuery)
	if err != nil {
		return nil, err
	}
	var wait sync.WaitGroup
	var detailList []DeploymentDetail
	for _, deployment := range deploymentList.Items {
		wait.Add(1)
		go func(deployment apps.Deployment) {
			detail, err := generateDeploymentDetail(client, &deployment, deployment.GetNamespace())
			if err == nil {
				detailList = append(detailList, *detail)
			}
			wait.Done()
		}(deployment)
	}
	wait.Wait()
	return &detailList, nil
}

// GetDeploymentDetail func
func GetDeploymentDetail(client client.Interface, namespace string,
	deploymentName string) (detail *DeploymentDetail, err error) {
	deployment, err := GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return nil, err
	}
	detail, err = generateDeploymentDetail(client, deployment, namespace)
	return
}

func generateDeploymentDetail(client client.Interface, deployment *apps.Deployment, namespace string) (detail *DeploymentDetail, err error) {
	selector, err := metaV1.LabelSelectorAsSelector(deployment.Spec.Selector)
	if err != nil {
		return nil, err
	}
	options := metaV1.ListOptions{LabelSelector: selector.String()}
	nsQuery := common.NewSameNamespaceQuery(namespace)
	channels := &common.ResourceChannels{
		ReplicaSetList: common.GetReplicaSetListChannelWithOptions(client,
			nsQuery, options, 1),
		PodList: common.GetPodListChannelWithOptions(client,
			nsQuery, options, 1),
		EventList: common.GetEventListChannel(client,
			nsQuery, 1),
		ServiceList: common.GetServiceListChannel(client,
			nsQuery, 1),
		// List and error channels to Ingresses.
		IngressList: common.GetIngressListChannel(client, nsQuery, 1),
	}
	detail, err = getDeploymentDetailFromChannels(deployment, channels)
	return
}

// GetDeploymentDetailListFromChannels returns a list of all Deployments in the cluster
// reading required resource list once from the channels.
func getDeploymentDetailFromChannels(deployment *apps.Deployment, channels *common.ResourceChannels) (*DeploymentDetail, error) {

	pods := <-channels.PodList.List
	err := <-channels.PodList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	events := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	rs := <-channels.ReplicaSetList.List
	err = <-channels.ReplicaSetList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	ss := <-channels.ServiceList.List
	err = <-channels.ServiceList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	is := <-channels.IngressList.List
	err = <-channels.IngressList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}
	detail := toDeploymentDetail(deployment, pods.Items, events.Items, rs.Items, ss.Items, is.Items, nonCriticalErrors)
	return detail, nil
}

func toDeploymentDetail(deployment *apps.Deployment, pods []core.Pod, events []core.Event, rs []apps.ReplicaSet, ss []core.Service, is []extensions.Ingress, nonCriticalErrors []error) (detail *DeploymentDetail) {
	matchPods := common.FilterDeploymentPodsByOwnerReference(*deployment, rs, pods)
	podInfo := common.GetPodControllerInfo(deployment.Status.Replicas, deployment.Spec.Replicas, deployment.GetObjectMeta(), matchPods, events)
	detail = &DeploymentDetail{
		ObjectMeta:     api.NewObjectMeta(deployment.ObjectMeta),
		TypeMeta:       api.NewTypeMeta(api.ResourceKindDeployment),
		PodInfo:        podInfo,
		VisitAddresses: common.GetVisitAddressByIngressAndServiceInfo(is, ss, deployment.Namespace, deployment.Spec.Template.Labels, deployment.Spec.Template.Spec.Containers),
		Status:         common.GetControllerStatus(&podInfo),
		Containers:     deployment.Spec.Template.Spec.Containers,
		VolumeInfos:    common.GetVolumeInfo(deployment.Spec.Template.Spec.Containers, deployment.Spec.Template.Spec.Volumes),
		UpdateStrategy: deployment.Spec.Strategy,
		Data:           deployment,
		Errors:         nonCriticalErrors,
	}
	return
}
