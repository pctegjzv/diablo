// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package release

import (
	"catalog-controller/pkg/apis/catalogcontroller/v1alpha1"
	"log"

	catalogclient "catalog-controller/pkg/client/clientset/versioned"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ReleaseDetails struct {
	*v1alpha1.Release
	Ops interface{} `json:"ops"`
}

func CreateRelease(client catalogclient.Interface, newJenkins *v1alpha1.Release) (*v1alpha1.Release, error) {
	return client.CatalogControllerV1alpha1().Releases(newJenkins.GetNamespace()).Create(newJenkins)
}

func DeleteRelease(client catalogclient.Interface, namespace, name string) error {
	log.Printf("Deleting %s release in %s namespace\n", name, namespace)
	return client.CatalogControllerV1alpha1().Releases(namespace).Delete(name, &v1.DeleteOptions{})
}

// GetReleaseDetail returns returns detailed information about a release
func GetReleaseDetail(client catalogclient.Interface, namespace, name string) (*ReleaseDetails, error) {
	log.Printf("Getting details of %s release in %s namespace\n", name, namespace)
	rawRelease, err := client.CatalogControllerV1alpha1().Releases(namespace).Get(name, v1.GetOptions{})
	if err != nil {
		return nil, err
	}
	details := &ReleaseDetails{rawRelease, nil}
	return details, nil
}
