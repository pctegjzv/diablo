package jenkinsbinding

import (
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

func DeleteJenkinsBinding(client devopsclient.Interface, namespace, name string) error {
	return client.DevopsV1alpha1().JenkinsBindings(namespace).Delete(name, &v1.DeleteOptions{})
}
