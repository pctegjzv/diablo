package statefulset

import (
	"log"

	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/container"
	v1 "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	client "k8s.io/client-go/kubernetes"
)

func GetStatefulSetDetailOriginal(client client.Interface, namespace string,
	statefulSetName string) (*v1.StatefulSet, error) {

	log.Printf("Getting details of %s statefulSetName in %s namespace", statefulSetName, namespace)
	statefulSet, err := client.Apps().StatefulSets(namespace).Get(statefulSetName, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}
	setTypeMeta(statefulSet)
	return statefulSet, nil
}

func UpdateStatefulSetOriginal(client client.Interface, namespace string,
	statefulSetName string, statefulSet *v1.StatefulSet) (*v1.StatefulSet, error) {
	old, err := GetStatefulSetDetailOriginal(client, namespace, statefulSetName)
	if err != nil {
		return nil, err
	}
	statefulSet.ObjectMeta.ResourceVersion = old.ObjectMeta.ResourceVersion
	statefulSet.ObjectMeta.Generation = old.ObjectMeta.Generation
	statefulSet.ObjectMeta.UID = old.ObjectMeta.UID
	statefulSet, err = client.AppsV1().StatefulSets(namespace).Update(statefulSet)
	setTypeMeta(statefulSet)
	return statefulSet, err
}

type StatefulSetReplica struct {
	Replicas int32 `json:"replicas"`
}

func UpdateStatefulSetReplica(client client.Interface, namespace string,
	statefulSetName string, replicas StatefulSetReplica) (statefulSet *v1.StatefulSet, err error) {
	statefulSet, err = GetStatefulSetDetailOriginal(client, namespace, statefulSetName)
	if err != nil {
		return
	}

	statefulSet.Spec.Replicas = &replicas.Replicas
	statefulSet, err = client.AppsV1().StatefulSets(namespace).Update(statefulSet)
	setTypeMeta(statefulSet)
	return
}

// PutStatefulsetContainer func
func PutStatefulsetContainer(client client.Interface, namespace string,
	statefulSetName string, containerName string, isDryRun bool, udsc container.UpdateContainerRequest) (statefulSet *v1.StatefulSet, err error) {
	statefulSet, err = GetStatefulSetDetailOriginal(client, namespace, statefulSetName)
	if err != nil {
		return
	}
	usedValumeMap := make(map[string]bool)
	for index, container := range statefulSet.Spec.Template.Spec.Containers {
		if containerName == container.Name {

			statefulSet.Spec.Template.Spec.Containers[index].Image = udsc.Container.Image

			statefulSet.Spec.Template.Spec.Containers[index].Resources = udsc.Container.Resources

			statefulSet.Spec.Template.Spec.Containers[index].Env = udsc.Container.Env

			statefulSet.Spec.Template.Spec.Containers[index].EnvFrom = udsc.Container.EnvFrom

			statefulSet.Spec.Template.Spec.Containers[index].VolumeMounts = make([]core.VolumeMount, 0)
			for _, vi := range udsc.VolumeInfo {
				statefulSet.Spec.Template.Spec.Containers[index].VolumeMounts, statefulSet.Spec.Template.Spec.Volumes = common.CreateContainerVolumeMount(
					statefulSet.Spec.Template.Spec.Containers[index], statefulSet.Spec.Template.Spec.Volumes, *vi)
			}
			break
		}
	}

	for index := range statefulSet.Spec.Template.Spec.Containers {
		for _, vm := range statefulSet.Spec.Template.Spec.Containers[index].VolumeMounts {
			usedValumeMap[vm.Name] = true
		}
	}

	// remove useless volume
	newVolumes := make([]core.Volume, 0)
	for _, v := range statefulSet.Spec.Template.Spec.Volumes {
		if usedValumeMap[v.Name] {
			newVolumes = append(newVolumes, v)
		}
	}
	statefulSet.Spec.Template.Spec.Volumes = newVolumes
	if isDryRun {
		return
	}
	statefulSet, err = client.AppsV1().StatefulSets(namespace).Update(statefulSet)
	return
}

// UpdateContainerEnv func
func UpdateContainerEnv(client client.Interface, namespace string,
	controllerName string, containerName string, ucer container.UpdateContainerEnvRequest) (statefulSet *v1.StatefulSet, err error) {
	statefulSet, err = GetStatefulSetDetailOriginal(client, namespace, controllerName)
	if err != nil {
		return
	}
	for index, container := range statefulSet.Spec.Template.Spec.Containers {
		if containerName == container.Name {
			statefulSet.Spec.Template.Spec.Containers[index].Env = ucer.Env
			statefulSet.Spec.Template.Spec.Containers[index].EnvFrom = ucer.EnvFrom
			break
		}
	}
	statefulSet, err = client.AppsV1().StatefulSets(namespace).Update(statefulSet)
	return
}

// UpdateContainerImage func
func UpdateContainerImage(client client.Interface, namespace string,
	controllerName string, containerName string, ucir container.UpdateContainerImageRequest) (statefulSet *v1.StatefulSet, err error) {
	statefulSet, err = GetStatefulSetDetailOriginal(client, namespace, controllerName)
	if err != nil {
		return
	}
	for index, container := range statefulSet.Spec.Template.Spec.Containers {
		if containerName == container.Name {
			statefulSet.Spec.Template.Spec.Containers[index].Image = ucir.Image
			break
		}
	}
	statefulSet, err = client.AppsV1().StatefulSets(namespace).Update(statefulSet)
	return
}

// CreateStatefulSetVolumeMount func
func CreateStatefulSetVolumeMount(client client.Interface, namespace string,
	statefulSetName string, containerName string, vi common.VolumeInfo) (statefulSet *v1.StatefulSet, err error) {
	statefulSet, err = GetStatefulSetDetailOriginal(client, namespace, statefulSetName)
	if err != nil {
		return
	}
	for index, container := range statefulSet.Spec.Template.Spec.Containers {
		if containerName == container.Name {
			statefulSet.Spec.Template.Spec.Containers[index].VolumeMounts, statefulSet.Spec.Template.Spec.Volumes = common.CreateContainerVolumeMount(
				container, statefulSet.Spec.Template.Spec.Volumes, vi)
			break
		}
	}
	statefulSet, err = client.AppsV1().StatefulSets(namespace).Update(statefulSet)
	return
}
