package jenkins

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

// UpdateJenkins Updates a jenkins instance in the API server
func UpdateJenkins(client devopsclient.Interface, newJenkins *v1alpha1.Jenkins, name string) (*v1alpha1.Jenkins, error) {
	oldJenkins, err := client.DevopsV1alpha1().Jenkinses().Get(name, v1.GetOptions{})
	if err != nil {
		return nil, err
	}

	oldJenkins.SetAnnotations(newJenkins.GetAnnotations())
	oldJenkins.Spec = newJenkins.Spec

	oldJenkins, err = client.DevopsV1alpha1().Jenkinses().Update(oldJenkins)
	if err != nil {
		return nil, err
	}
	return oldJenkins, nil
}

// CreateJenkins creates a Jenkins instance in the API server
func CreateJenkins(client devopsclient.Interface, newJenkins *v1alpha1.Jenkins) (*v1alpha1.Jenkins, error) {
	return client.DevopsV1alpha1().Jenkinses().Create(newJenkins)
}

// RetrieveJenkins fetches a Jenkins instance from API
func RetrieveJenkins(client devopsclient.Interface, name string) (*v1alpha1.Jenkins, error) {
	return client.DevopsV1alpha1().Jenkinses().Get(name, v1.GetOptions{})
}

// RetrieveJenkins delete a Jenkins instance in the API server
func DeleteJenkins(client devopsclient.Interface, name string) error {
	return client.DevopsV1alpha1().Jenkinses().Delete(name, &v1.DeleteOptions{})
}
