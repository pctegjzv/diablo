package project

import (
	"log"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/api"
	"alauda.io/diablo/src/backend/errors"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/dataselect"
	rbac "k8s.io/api/rbac/v1"
	"k8s.io/client-go/kubernetes"
)

// ProjectList contains a list of namespaces in the cluster.
type ProjectList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Projects.
	Projects []Project `json:"projects"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Project is a presentation layer view of Kubernetes namespaces. This means it is namespace plus
// additional augmented data we can get from other sources.
type Project struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	ProjectManagers []string `json:"project_managers"`
}

// GetProjectListFromChannels returns a list of all namespaces in the cluster.
func GetProjectListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (*ProjectList, error) {
	projects := <-channels.ProjectList.List
	err := <-channels.ProjectList.Error

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	return toProjectList(projects.Items, nil, nonCriticalErrors, dsQuery), nil
}

// GetProjectList returns a list of all namespaces in the cluster.
func GetProjectList(client devopsclient.Interface, k8sclient kubernetes.Interface, dsQuery *dataselect.DataSelectQuery) (*ProjectList, error) {
	log.Println("Getting list of projects")
	var projectRoleBindings map[string][]rbac.RoleBinding
	projectList, err := client.DevopsV1alpha1().Projects().List(devopsv1alpha1.ListEverything)
	if err != nil {
		log.Println("error while listing projects", err)

	} else {
		projectRoleBindings = make(map[string][]rbac.RoleBinding, len(projectList.Items))
		if len(projectList.Items) > 0 {
			for _, p := range projectList.Items {

				rolebindings, err := k8sclient.RbacV1().RoleBindings(p.Name).List(api.ListEverything)
				if err != nil {
					log.Println("Listing RoleBindings for project", p.Name, "err", err)
					continue
				}
				projectRoleBindings[p.Name] = make([]rbac.RoleBinding, 0, len(rolebindings.Items))
				for _, rb := range rolebindings.Items {
					if common.IsDevopsProjectAdmin(rb) {
						projectRoleBindings[p.Name] = append(projectRoleBindings[p.Name], rb)
					}
				}
			}
		}
	}

	// namespaces, err := client. .List(api.ListEverything)
	// client.
	// namespaces, err := client. .List(api.ListEverything)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	return toProjectList(projectList.Items, projectRoleBindings, nonCriticalErrors, dsQuery), nil
}

func toProjectList(projects []devopsv1alpha1.Project, rolebindings map[string][]rbac.RoleBinding, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *ProjectList {
	projectList := &ProjectList{
		Projects: make([]Project, 0),
		ListMeta: api.ListMeta{TotalItems: len(projects)},
	}

	projectCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(projects), dsQuery)
	projects = fromCells(projectCells)
	projectList.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	projectList.Errors = nonCriticalErrors

	for _, project := range projects {
		projectList.Projects = append(projectList.Projects, toProject(project, rolebindings))
	}

	return projectList
}

func toProject(project devopsv1alpha1.Project, roleBinding map[string][]rbac.RoleBinding) Project {
	proj := Project{
		ObjectMeta: api.NewObjectMetaSplit(
			project.ObjectMeta.Name, project.ObjectMeta.Namespace,
			project.ObjectMeta.Labels, project.ObjectMeta.Annotations,
			project.ObjectMeta.CreationTimestamp.Time,
		),
		TypeMeta: api.NewTypeMeta(api.ResourceKindProject),

		ProjectManagers: []string{},
		// data here
	}
	if roleBinding != nil {
		if binding, ok := roleBinding[proj.ObjectMeta.Name]; ok && len(binding) > 0 {
			for _, b := range binding {
				for _, s := range b.Subjects {
					if s.Kind == rbac.UserKind {
						proj.ProjectManagers = append(proj.ProjectManagers, s.Name)
					}

				}

			}
		}
	}
	return proj
}
