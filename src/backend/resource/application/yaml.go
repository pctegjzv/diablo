package application

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync"

	ccClient "catalog-controller/pkg/client/clientset/versioned"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	clientapi "alauda.io/diablo/src/backend/client/api"
	metricapi "alauda.io/diablo/src/backend/integration/metric/api"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/configmap"
	"alauda.io/diablo/src/backend/resource/daemonset"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"alauda.io/diablo/src/backend/resource/deployment"
	"alauda.io/diablo/src/backend/resource/ingress"
	"alauda.io/diablo/src/backend/resource/job"
	"alauda.io/diablo/src/backend/resource/other"
	"alauda.io/diablo/src/backend/resource/pipelineconfig"
	"alauda.io/diablo/src/backend/resource/secret"
	"alauda.io/diablo/src/backend/resource/service"
	"alauda.io/diablo/src/backend/resource/statefulset"
	"github.com/emicklei/go-restful"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	extv1beta1 "k8s.io/api/extensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/dynamic"
	client "k8s.io/client-go/kubernetes"
)

type resourceListHolder struct {
	Resources []runtime.Object
	lock      sync.RWMutex
}

func newResourceListHolder() *resourceListHolder {
	return &resourceListHolder{
		Resources: make([]runtime.Object, 0, 50),
		lock:      sync.RWMutex{},
	}
}

func (r *resourceListHolder) append(slice []runtime.Object) {
	r.lock.Lock()
	r.Resources = append(r.Resources, slice...)
	r.lock.Unlock()
}

type ListResourceYamlFunc func(
	holder *resourceListHolder,
	cc *ClientCollection,
	nsQuery *common.NamespaceQuery,
	dsQuery *dataselect.DataSelectQuery,
) (err error)

var yamlFunc = []ListResourceYamlFunc{}
var deletePropagation = metav1.DeletePropagationBackground
var deleteOptions = metav1.DeleteOptions{
	PropagationPolicy: &deletePropagation,
}

func init() {
	//// release list
	//yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection,nsQuery *common.NamespaceQuery,dsQuery *dataselect.DataSelectQuery) (err error) {
	//	list, err := release.GetOriginalList(catalogClient, nsQuery, dsQuery)
	//	holder.append(release.GetObjectList(list))
	//	return err
	//})
	// pod list
	// yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection,nsQuery *common.NamespaceQuery,dsQuery *dataselect.DataSelectQuery) (err error) {
	// 	list, err := pod.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
	// 	holder.append(pod.GetObjectList(list))
	// 	return err
	// })
	// deployment list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := deployment.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(deployment.GetObjectList(list))
		return err
	})
	// pipelineconfig list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := pipelineconfig.GetOriginalList(cc.K8sClient, cc.DevopsClient, nsQuery, dsQuery)
		holder.append(pipelineconfig.GetObjectList(list))
		return err
	})
	// configmap list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := configmap.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(configmap.GetObjectList(list))
		return err
	})
	// ingress list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := ingress.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(ingress.GetObjectList(list))
		return err
	})
	// secret list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := secret.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(secret.GetObjectList(list))
		return err
	})
	// service list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := service.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(service.GetObjectList(list))
		return err
	})
	// daemonset list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := daemonset.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(daemonset.GetObjectList(list))
		return err
	})
	// statefulset list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := statefulset.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(statefulset.GetObjectList(list))
		return err
	})
	// job list
	yamlFunc = append(yamlFunc, func(holder *resourceListHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
		list, err := job.GetOriginalList(cc.K8sClient, nsQuery, dsQuery)
		holder.append(job.GetObjectList(list))
		return err
	})
}

type ApplicationYAML struct {
	Objects []runtime.Object `json:"resources"`
	Errors  []error          `json:"errors"`
}

func (app *ApplicationYAML) GetResourceTree(request *restful.Request, mngr clientapi.DevOpsClientManager) (tree *resourceTree) {
	tree = &resourceTree{
		tree:    make(map[string]map[string]*objectHit),
		clients: make(map[string]*dynamic.Client),
	}

	if app != nil && len(app.Objects) > 0 {
		var kindMap map[string]*objectHit
		var mapOK bool
		for _, r := range app.Objects {
			kind := strings.ToLower(r.GetObjectKind().GroupVersionKind().Kind)
			kindMap, mapOK = tree.tree[kind]
			if !mapOK || kindMap == nil {
				kindMap = make(map[string]*objectHit)
			}
			tree.PrepareClient(r, request, mngr)
			switch kind {
			case "deployment":
				if deploy, ok := r.(*appsv1.Deployment); ok {
					kindMap[deploy.GetName()] = newObj(deploy)
				}
			case "configmap":
				if res, ok := r.(*corev1.ConfigMap); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			case "secret":
				if res, ok := r.(*corev1.Secret); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			case "service":
				if res, ok := r.(*corev1.Service); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			case "pipelineconfig":
				if res, ok := r.(*devopsv1alpha1.PipelineConfig); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			case "daemonset":
				if res, ok := r.(*appsv1.DaemonSet); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			case "statefulset":
				if res, ok := r.(*appsv1.StatefulSet); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			case "ingress":
				if res, ok := r.(*extv1beta1.Ingress); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			case "job":
				if res, ok := r.(*batchv1.Job); ok {
					kindMap[res.GetName()] = newObj(res)
				}
			}
			tree.tree[kind] = kindMap
		}
	}
	return
}

// kind - namespace - name

type resourceTree struct {
	tree    map[string]map[string]*objectHit
	clients map[string]*dynamic.Client
}

type objectHit struct {
	Object runtime.Object
	Hits   int
}

func newObj(obj runtime.Object) *objectHit {
	return &objectHit{
		Hits:   0,
		Object: obj,
	}
}

func (tree *resourceTree) Compare(
	spec []*unstructured.Unstructured,
	request *restful.Request,
	mngr clientapi.DevOpsClientManager,
	k8sclient client.Interface) (ops []*ResourceOperation) {
	ops = make([]*ResourceOperation, 0, len(spec)+len(tree.tree))
	// toCreate, toUpdate = make([]*unstructured.Unstructured, 0, len(spec)), make([]*unstructured.Unstructured, 0, len(spec))
	if len(spec) > 0 {
		var op = ""
		for _, r := range spec {
			client := tree.PrepareClient(r, request, mngr)
			if tree.IsInTree(r.GetKind(), r.GetName()) {
				op = "update"
			} else {
				op = "create"
			}
			ops = append(ops, NewResourceOp(op, client, nil, r, k8sclient))
		}
	}
	toDelete := tree.GetNoHits(request, mngr)
	for _, d := range toDelete {
		client := tree.PrepareClient(d, request, mngr)
		ops = append(ops, NewResourceOp("delete", client, d, nil, k8sclient))
	}
	return
}

func (tree *resourceTree) IsInTree(kind, name string) bool {
	if len(tree.tree) == 0 {
		return false
	}
	kind = strings.ToLower(kind)
	idx, ok := tree.tree[kind]
	if !ok {
		return false
	}
	hit, ok := idx[name]
	if ok {
		hit.Hits++
		idx[name] = hit
	}
	return ok
}
func (tree *resourceTree) GetNoHits(request *restful.Request, mngr clientapi.DevOpsClientManager) (nohits []runtime.Object) {
	nohits = make([]runtime.Object, 0, len(tree.tree))
	for _, idx := range tree.tree {
		for _, hits := range idx {
			if hits.Hits == 0 {
				nohits = append(nohits, hits.Object)
				tree.PrepareClient(hits.Object, request, mngr)
			}
		}
	}
	return
}

func (tree *resourceTree) PrepareClient(obj runtime.Object, request *restful.Request, mngr clientapi.DevOpsClientManager) *dynamic.Client {
	var (
		client   *dynamic.Client
		clientOk bool
	)
	kind := strings.ToLower(obj.GetObjectKind().GroupVersionKind().Kind)
	if client, clientOk = tree.clients[kind]; !clientOk {
		var err error
		groupVersion := obj.GetObjectKind().GroupVersionKind().GroupVersion()
		client, err = mngr.DynamicClient(request, &groupVersion)
		if err != nil {
			log.Println("error creating dynamic client for ", kind, " err ", err)
		} else {
			tree.clients[kind] = client
		}
	}

	return client
}

func getExactLabelQuery(name string) *dataselect.DataSelectQuery {
	return dataselect.NewDataSelectQuery(
		dataselect.NoPagination, dataselect.NoSort,
		dataselect.NewFilterQuery([]string{dataselect.LabelEqualProperty, "app:" + name}),
		dataselect.StandardMetrics,
	)
}

func GetApplicationYAML(k8sClient client.Interface, devopsClient devopsclient.Interface, catalogClient ccClient.Interface, metricsClient metricapi.MetricClient, namespace, name string) (appYaml *ApplicationYAML, err error) {
	log.Println("Getting application yaml: " + namespace + "/" + name)
	nsQuery := getNamespaceQuery(namespace)
	dsQuery := getExactLabelQuery(name)
	var (
		wait   sync.WaitGroup
		holder = newResourceListHolder()
	)
	for _, f := range yamlFunc {
		wait.Add(1)
		go func(method ListResourceYamlFunc) {
			cc := &ClientCollection{
				K8sClient:     k8sClient,
				DevopsClient:  devopsClient,
				CatalogClient: catalogClient,
				MetricClient:  metricsClient,
			}
			funcErr := method(holder, cc, nsQuery, dsQuery)
			if funcErr != nil {
				//
				log.Println("error listing resource: ", err)
			}
			wait.Done()
		}(f)
	}
	wait.Wait()
	appYaml = toApplicationYAML(holder)
	return
}

func toApplicationYAML(holder *resourceListHolder) *ApplicationYAML {
	return &ApplicationYAML{Objects: holder.Resources}
}

type UpdateAppYAMLSpec struct {
	Resources []*unstructured.Unstructured `json:"resources"`
}

type DeleteAppYAMLSpec struct {
	RemoveLabelResources []*unstructured.Unstructured `json:"removeLabelResources"`
}

// ResourceData represents a resource
type ResourceData map[string]interface{}

func (r ResourceData) GetKind() string {
	if val, ok := r["kind"]; ok {
		return fmt.Sprintf("%s", val)
	}
	return ""
}

func (r ResourceData) GetMetadata() *metav1.ObjectMeta {
	if val, ok := r["metadata"]; ok {
		var meta *metav1.ObjectMeta
		if meta, ok = val.(*metav1.ObjectMeta); ok {
			return meta
		}
		meta = &metav1.ObjectMeta{}
		data, err := json.Marshal(val)
		if err != nil {
			return nil
		}
		err = json.Unmarshal(data, meta)
		if err != nil {
			return nil
		}
		r["metadata"] = meta
		return meta
	}
	return nil
}

func DeleteApplicationYaml(request *restful.Request, cc *ClientCollection, namespace, name string, spec *DeleteAppYAMLSpec) ([]*ResourceOperation, error) {
	for _, resource := range spec.RemoveLabelResources {
		labels := resource.GetLabels()
		delete(labels, AppLabelKey)
		labels[RemovedLabelKey] = "true"
		resource.SetLabels(labels)
	}
	updateSepc := &UpdateAppYAMLSpec{Resources: spec.RemoveLabelResources}
	operations, err := UpdateApplicationYAML(request, cc, namespace, name, updateSepc)
	return operations, err
}

func UpdateApplicationYAML(request *restful.Request, cc *ClientCollection, namespace, name string, spec *UpdateAppYAMLSpec) ([]*ResourceOperation, error) {
	// retrieving current yaml for comparisson:
	appYaml, err := GetApplicationYAML(cc.K8sClient, cc.DevopsClient, cc.CatalogClient, cc.MetricClient, namespace, name)
	if err != nil {
		return nil, err
	}
	tree := appYaml.GetResourceTree(request, cc.ClientMgr)
	operations := tree.Compare(spec.Resources, request, cc.ClientMgr, cc.K8sClient)
	for i, op := range operations {
		log.Println(i, " ", *op)
	}

	var wait sync.WaitGroup
	for _, op := range operations {
		wait.Add(1)
		go func(resOp *ResourceOperation) {
			resOp.Execute()
			wait.Done()
		}(op)
	}
	wait.Wait()
	return operations, nil
}

type ResourceOperation struct {
	Operation    string                     `json:"operation"`
	Namespace    string                     `json:"namespace"`
	Error        error                      `json:"error,omitempty"`
	Client       *dynamic.Client            `json:"-"`
	Resource     *metav1.APIResource        `json:"resource"`
	Unstructured *unstructured.Unstructured `json:"data,omitempty"`
}

func NewResourceOp(op string, client *dynamic.Client, obj interface{}, unstr *unstructured.Unstructured, k8sClient client.Interface) (resOp *ResourceOperation) {
	resOp = &ResourceOperation{
		Operation:    op,
		Client:       client,
		Unstructured: unstr,
	}
	if obj != nil && unstr == nil {
		data, err := json.Marshal(obj)
		if err != nil {
			resOp.Error = err
			return
		}
		unstr = &unstructured.Unstructured{}
		err = json.Unmarshal(data, unstr)
		if err != nil {
			resOp.Error = err
			return
		}
	}
	if unstr != nil {
		apiVersion := unstr.GetAPIVersion()
		split := strings.Split(apiVersion, "/")
		var group, version string
		if len(split) == 1 {
			version = split[0]
		} else if len(split) > 1 {
			group, version = split[0], split[1]
		}
		resourceName, err := other.GetKindName(k8sClient, unstr.GetKind())
		if err != nil {
			resOp.Error = err
			return
		}
		resOp.Resource = &metav1.APIResource{
			Name:       resourceName,
			Namespaced: unstr.GetNamespace() != "",
			Kind:       unstr.GetKind(),
			Group:      group,
			Version:    version,
		}
		resOp.Namespace = unstr.GetNamespace()
		resOp.Unstructured = unstr
	}
	return resOp
}

func (op *ResourceOperation) Execute() {
	if op.Error != nil {
		log.Println("operation has error already: ", op.Error)
		return
	}
	resourceInterface := op.Client.Resource(op.Resource, op.Namespace)

	switch op.Operation {
	case "delete":
		op.Error = resourceInterface.Delete(op.Unstructured.GetName(), &deleteOptions)
	case "create":
		var data *unstructured.Unstructured
		data, op.Error = resourceInterface.Create(op.Unstructured)
		if data != nil {
			op.Unstructured = data
		}
	case "update":
		var data *unstructured.Unstructured
		data, op.Error = resourceInterface.Update(op.Unstructured)
		if data != nil {
			op.Unstructured = data
		}
	default:
		op.Error = fmt.Errorf("Operation is not supported: %s", op.Operation)
	}
}
