package application

import (
	"log"
	"time"

	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/api"
	metricapi "alauda.io/diablo/src/backend/integration/metric/api"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/daemonset"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"alauda.io/diablo/src/backend/resource/deployment"
	"alauda.io/diablo/src/backend/resource/statefulset"
	client "k8s.io/client-go/kubernetes"
)

func getApplicationListFromAggregation(aggregation map[string][]common.Resource) (apps []Application) {
	apps = make([]Application, 0, len(aggregation))
	for k, v := range aggregation {
		apps = append(apps,
			Application{
				ObjectMeta:   api.NewObjectMetaSplit(k, "", nil, nil, time.Now()),
				Deployments:  deployment.ToDeploymentList(v),
				Daemonsets:   daemonset.ToDaemonSetList(v),
				StatefulSets: statefulset.ToStatefulSetList(v),
			},
		)
	}
	return
}

// ApplicationList contains a list of Applications in the cluster.
type ApplicationList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Applications.
	Applications []Application `json:"applications"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Application sets a definition for application
type Application struct {
	ObjectMeta   api.ObjectMeta            `json:"objectMeta"`
	Deployments  []deployment.Deployment   `json:"deployments"`
	Daemonsets   []daemonset.DaemonSet     `json:"daemonsets"`
	StatefulSets []statefulset.StatefulSet `json:"statefulsets"`
}

// GetApplicationList returns a list of all applications in the cluster.
func GetApplicationList(client client.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery,
	devopsclient devopsclient.Interface, metricClient metricapi.MetricClient) (list *ApplicationList, err error) {
	log.Print("Getting list of all applications in namespace: ", nsQuery.ToRequestParam())
	funcs := getAppListFuncList()
	holder, err := getResourceHolder(client, nsQuery, dsQuery, devopsclient, metricClient, funcs)
	if err != nil {
		return nil, err
	}
	return toApplicationList(holder, dsQuery)
}

func toApplicationList(
	holder *ResourceHolder,
	dsQuery *dataselect.DataSelectQuery,
) (list *ApplicationList, err error) {
	resourceList := holder.getResourceList()
	aggregator := common.ResourceAggregator{}
	appAggregation := aggregator.ByLabelKey(common.LabelApplicationKey, resourceList...)

	list = &ApplicationList{
		ListMeta:     api.ListMeta{TotalItems: len(appAggregation)},
		Applications: make([]Application, 0, len(appAggregation)),
	}

	apps := getApplicationListFromAggregation(appAggregation)
	appCells, _ := dataselect.GenericDataSelectWithFilter(toCells(apps), dsQuery)
	apps = fromCells(appCells)
	list.Applications = apps
	list.ListMeta = api.ListMeta{TotalItems: len(apps)}
	return list, nil
}
