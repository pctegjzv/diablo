package application

import (
	"log"
	"sync"
	"time"

	catalogclient "catalog-controller/pkg/client/clientset/versioned"

	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/api"
	metricapi "alauda.io/diablo/src/backend/integration/metric/api"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/configmap"
	"alauda.io/diablo/src/backend/resource/daemonset"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"alauda.io/diablo/src/backend/resource/deployment"
	"alauda.io/diablo/src/backend/resource/ingress"
	"alauda.io/diablo/src/backend/resource/job"
	"alauda.io/diablo/src/backend/resource/pipelineconfig"
	"alauda.io/diablo/src/backend/resource/pod"
	"alauda.io/diablo/src/backend/resource/release"
	"alauda.io/diablo/src/backend/resource/secret"
	"alauda.io/diablo/src/backend/resource/service"
	"alauda.io/diablo/src/backend/resource/statefulset"
	client "k8s.io/client-go/kubernetes"
)

type DeleteResultHolder struct {
	Results map[string]error `json:"results"`
	lock    sync.RWMutex
}

func newDeleteResult() *DeleteResultHolder {
	return &DeleteResultHolder{
		Results: make(map[string]error),
		lock:    sync.RWMutex{},
	}
}

func (d *DeleteResultHolder) append(kind string, err error) {
	if err == nil {
		return
	}
	d.lock.Lock()
	d.Results[kind] = err
	d.lock.Unlock()
}

type DeleteResourceFunc func(
	holder *DeleteResultHolder,
	client client.Interface,
	devopsclient devopsclient.Interface,
	catalogclient catalogclient.Interface,
	nsQuery *common.NamespaceQuery,
	dsQuery *dataselect.DataSelectQuery,
	metricClient metricapi.MetricClient,
) error

var deleteFuncs = []DeleteResourceFunc{}
var listFuncs = []ListResourceFunc{}

func init() {
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := release.DeleteOriginalList(catalogclient, nsQuery, dsQuery)
		holder.append("Release", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := deployment.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("Deployment", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := pipelineconfig.DeleteOriginalList(client, devopsclient, nsQuery, dsQuery)
		holder.append("PipelineConfig", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := configmap.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("ConfigMap", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := ingress.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("Ingress", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := secret.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("Secret", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := service.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("Service", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := daemonset.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("DaemonSet", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := statefulset.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("StatefulSet", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := job.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("Job", err)
		return err
	})
	deleteFuncs = append(deleteFuncs, func(holder *DeleteResultHolder, client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) error {
		err := pod.DeleteOriginalList(client, nsQuery, dsQuery)
		holder.append("Pod", err)
		return err
	})
}

// ApplicationDetail sets a definition for applicationdetail
type ApplicationDetail struct {
	ObjectMeta   api.ObjectMeta                   `json:"objectMeta"`
	Deployments  *[]deployment.DeploymentDetail   `json:"deployments"`
	Daemonsets   *[]daemonset.DaemonSetDetail     `json:"daemonsets"`
	StatefulSets *[]statefulset.StatefulSetDetail `json:"statefulsets"`
	// TODO: Change pipeline to definition when present
	Pipelines *[]pipelineconfig.PipelineConfig `json:"pipelines"`
	// Configmaps []configmap.ConfigMapDetail `json:"configmaps"`
	Others []common.Resource `json:"others"`
}

type DomainMapping struct {
	Host           string `json:"host"`
	IngressName    string `json:"ingressName"`
	ServiceName    string `json:"serviceName"`
	DeploymentName string `json:"deploymentName"`
}

func getLabelQuery(name string) *dataselect.DataSelectQuery {
	return dataselect.NewDataSelectQuery(
		dataselect.NoPagination, dataselect.NoSort,
		dataselect.NewFilterQuery([]string{dataselect.LabelProperty, "app:" + name}),
		dataselect.StandardMetrics,
	)
}

func getNamespaceQuery(namespace string) *common.NamespaceQuery {
	return common.NewNamespaceQuery([]string{namespace})
}

func GetApplicationDetail(client client.Interface, namespace, name string,
	devopsclient devopsclient.Interface, metricClient metricapi.MetricClient) (appDetail *ApplicationDetail, err error) {
	log.Print("Getting detail for application", namespace+"/"+name)
	nsQuery := getNamespaceQuery(namespace)
	dsQuery := getLabelQuery(name)
	funcs := getAppDetailFuncList()
	holder, err := getResourceHolder(client, nsQuery, dsQuery, devopsclient, metricClient, funcs)
	if err != nil {
		return
	}
	appDetail = &ApplicationDetail{
		ObjectMeta:   api.NewObjectMetaSplit(name, namespace, nil, nil, time.Now()),
		Deployments:  holder.DeploymentDetailList,
		Daemonsets:   holder.DaemonSetDetailList,
		StatefulSets: holder.StatefulSeDetailList,
		// TODO: Change pipeline to definition when present
		Pipelines: &holder.PipelineList.Items,
		// Configmaps []configmap.ConfigMapDetail `json:"configmaps"`
		Others: getResources(holder),
	}
	return
}

func getResources(holder *ResourceHolder) (res []common.Resource) {
	resources := holder.getResourceList()
	res = make([]common.Resource, 0, len(resources))
	for _, r := range resources {
		if _, ok := r.(deployment.DeploymentDetail); ok {
			continue
		}
		if _, ok := r.(pipelineconfig.PipelineConfig); ok {
			continue
		}
		if _, ok := r.(daemonset.DaemonSetDetail); ok {
			continue
		}
		if _, ok := r.(statefulset.StatefulSetDetail); ok {
			continue
		}
		res = append(res, r)
	}
	return
}

// DeleteApplication delete an entire application
func DeleteApplication(client client.Interface, devopsclient devopsclient.Interface, catalogclient catalogclient.Interface, metricClient metricapi.MetricClient, namespace, name string) (result *DeleteResultHolder, err error) {
	log.Println("Deleting entire application: " + namespace + "/" + name)
	nsQuery := getNamespaceQuery(namespace)
	dsQuery := getExactLabelQuery(name)
	result = newDeleteResult()
	var wait sync.WaitGroup
	for _, f := range deleteFuncs {
		wait.Add(1)
		go func(method DeleteResourceFunc) {
			funcErr := method(result, client, devopsclient, catalogclient, nsQuery, dsQuery, metricClient)
			if funcErr != nil {
				log.Println("Error deleting resource: ", err)
			}
			wait.Done()
		}(f)
	}
	wait.Wait()
	return
}
