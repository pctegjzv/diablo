package application

import (
	ccClient "catalog-controller/pkg/client/clientset/versioned"
	"sync"

	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	clientapi "alauda.io/diablo/src/backend/client/api"
	"alauda.io/diablo/src/backend/errors"
	metricapi "alauda.io/diablo/src/backend/integration/metric/api"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/configmap"
	"alauda.io/diablo/src/backend/resource/daemonset"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"alauda.io/diablo/src/backend/resource/deployment"
	"alauda.io/diablo/src/backend/resource/ingress"
	"alauda.io/diablo/src/backend/resource/pipelineconfig"
	"alauda.io/diablo/src/backend/resource/secret"
	"alauda.io/diablo/src/backend/resource/service"
	"alauda.io/diablo/src/backend/resource/statefulset"
	client "k8s.io/client-go/kubernetes"
)

const AppLabelKey = "app"
const RemovedLabelKey = "app.alauda.io/appLabelRemoved"

// ResourceHolder holds the result for an application list
type ResourceHolder struct {
	PipelineList         *pipelineconfig.PipelineConfigList
	DeploymentList       *deployment.DeploymentList
	ConfigMapList        *configmap.ConfigMapList
	IngressList          *ingress.IngressList
	SecretList           *secret.SecretList
	ServiceList          *service.ServiceList
	DaemonSetList        *daemonset.DaemonSetList
	StatefulSetList      *statefulset.StatefulSetList
	DeploymentDetailList *[]deployment.DeploymentDetail
	DaemonSetDetailList  *[]daemonset.DaemonSetDetail
	StatefulSeDetailList *[]statefulset.StatefulSetDetail

	NonCriticalErrors []error
	CriticalErrors    []error
}

func (h *ResourceHolder) getResourceList() (resourceList []common.Resource) {
	resourceList = make([]common.Resource, 0, 50)
	if h.PipelineList != nil {
		resourceList = append(resourceList, h.PipelineList.GetItems()...)
	}
	if h.DeploymentList != nil {
		resourceList = append(resourceList, h.DeploymentList.GetItems()...)
	}
	if h.ConfigMapList != nil {
		resourceList = append(resourceList, h.ConfigMapList.GetItems()...)
	}
	if h.IngressList != nil {
		resourceList = append(resourceList, h.IngressList.GetItems()...)
	}
	if h.SecretList != nil {
		resourceList = append(resourceList, h.SecretList.GetItems()...)
	}
	if h.DaemonSetList != nil {
		resourceList = append(resourceList, h.DaemonSetList.GetItems()...)
	}
	if h.StatefulSetList != nil {
		resourceList = append(resourceList, h.StatefulSetList.GetItems()...)
	}
	if h.ServiceList != nil {
		resourceList = append(resourceList, h.ServiceList.GetItems()...)
	}
	return
}

type ListResourceFunc func(
	holder *ResourceHolder,
	cc *ClientCollection,
	nsQuery *common.NamespaceQuery,
	dsQuery *dataselect.DataSelectQuery,
) (err error)

type ApplicationCell Application

func (self ApplicationCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Name)
	case dataselect.ExactNameProperty:
		return dataselect.StdExactString(self.ObjectMeta.Name)
	default:
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func (self ApplicationCell) GetResourceSelector() *metricapi.ResourceSelector {
	return &metricapi.ResourceSelector{
		Namespace:    self.ObjectMeta.Namespace,
		ResourceName: self.ObjectMeta.Name,
	}
}

func toCells(std []Application) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = ApplicationCell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []Application {
	std := make([]Application, len(cells))
	for i := range std {
		std[i] = Application(cells[i].(ApplicationCell))
	}
	return std
}

// LoadArgs struct
type ClientCollection struct {
	K8sClient     client.Interface
	DevopsClient  devopsclient.Interface
	ClientMgr     clientapi.DevOpsClientManager
	MetricClient  metricapi.MetricClient
	CatalogClient ccClient.Interface
}

func loadDeploymentList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.DeploymentList, err = deployment.GetDeploymentList(cc.K8sClient, nsQuery, dsQuery, cc.MetricClient)
	return
}

func loadDeploymentDetailList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.DeploymentDetailList, err = deployment.GetDeploymentDetailList(cc.K8sClient, nsQuery, dsQuery)
	return
}

func loadPipelineList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.PipelineList, err = pipelineconfig.GetPipelineConfigList(cc.DevopsClient, nsQuery, dsQuery)
	return
}

func loadConfigMapList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.ConfigMapList, err = configmap.GetConfigMapList(cc.K8sClient, nsQuery, dsQuery)
	return
}

func loadIngressList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.IngressList, err = ingress.GetIngressList(cc.K8sClient, nsQuery, dsQuery)
	return
}

func loadSecretList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.SecretList, err = secret.GetSecretList(cc.K8sClient, nsQuery, dsQuery)
	return
}

func loadDaemonSetList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.DaemonSetList, err = daemonset.GetDaemonSetList(cc.K8sClient, nsQuery, dsQuery, cc.MetricClient)
	return
}

func loadDaemonSetDetailList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.DaemonSetDetailList, err = daemonset.GetDaemonSetDetailList(cc.K8sClient, nsQuery, dsQuery)
	return
}

func loadStatefulSetList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.StatefulSetList, err = statefulset.GetStatefulSetList(cc.K8sClient, nsQuery, dsQuery, cc.MetricClient)
	return
}

func loadStatefulSetDetailList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.StatefulSeDetailList, err = statefulset.GetStatefulSetDetailList(cc.K8sClient, nsQuery, dsQuery)
	return
}

func loadServiceList(holder *ResourceHolder, cc *ClientCollection, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (err error) {
	holder.ServiceList, err = service.GetServiceList(cc.K8sClient, nsQuery, dsQuery)
	return
}

func getAppListFuncList() []ListResourceFunc {
	listFuncs = []ListResourceFunc{}
	listFuncs = append(listFuncs, loadDeploymentList)
	listFuncs = append(listFuncs, loadDaemonSetList)
	listFuncs = append(listFuncs, loadStatefulSetList)
	return listFuncs
}

func getAppDetailFuncList() []ListResourceFunc {
	listFuncs = []ListResourceFunc{}
	listFuncs = append(listFuncs, loadDeploymentDetailList)
	listFuncs = append(listFuncs, loadDaemonSetDetailList)
	listFuncs = append(listFuncs, loadStatefulSetDetailList)
	listFuncs = append(listFuncs, loadConfigMapList)
	listFuncs = append(listFuncs, loadIngressList)
	listFuncs = append(listFuncs, loadSecretList)
	listFuncs = append(listFuncs, loadServiceList)
	listFuncs = append(listFuncs, loadPipelineList)
	return listFuncs

}

func getResourceHolder(client client.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery,
	devopsclient devopsclient.Interface, metricClient metricapi.MetricClient, listFuncs []ListResourceFunc) (*ResourceHolder, error) {
	var (
		wait   sync.WaitGroup
		holder = &ResourceHolder{
			CriticalErrors:    []error{},
			NonCriticalErrors: []error{},
		}
		err error
	)
	cc := &ClientCollection{
		K8sClient:    client,
		DevopsClient: devopsclient,
		MetricClient: metricClient,
	}
	// TODO: Make it concurrent
	// make use of channels like deployment api
	for _, f := range listFuncs {
		wait.Add(1)
		go func(method ListResourceFunc) {
			funcErr := method(holder, cc, nsQuery, dsQuery)
			if funcErr != nil {
				nonCritical, critical := errors.HandleError(funcErr)
				holder.NonCriticalErrors = append(holder.NonCriticalErrors, nonCritical...)
				holder.CriticalErrors = append(holder.CriticalErrors, critical)
			}
			wait.Done()
		}(f)
	}
	wait.Wait()
	if len(holder.CriticalErrors) > 0 {
		err = holder.CriticalErrors[0]
		return nil, err
	}
	return holder, err
}
