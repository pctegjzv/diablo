package imageregistrybinding

import (
  "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
  devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
  "alauda.io/diablo/src/backend/api"
  "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func toDetails(imageRegistryBinding *v1alpha1.ImageRegistryBinding) *ImageRegistryBinding {
  irb := ImageRegistryBinding{
    ObjectMeta: api.NewObjectMeta(imageRegistryBinding.ObjectMeta),
    TypeMeta:   api.NewTypeMeta(api.ResourceKindImageRegistryBinding),
    Spec:       imageRegistryBinding.Spec,
    Status:     imageRegistryBinding.Status,
  }
  return &irb
}

func GetImageRegistryBinding(client devopsclient.Interface, namespace, name string) (*v1alpha1.ImageRegistryBinding, error) {
  irb, err := client.DevopsV1alpha1().ImageRegistryBindings(namespace).Get(name, v1.GetOptions{})
  if err != nil {
    return nil, err
  }
  return irb, nil
}

func UpdateImageRegistryBinding(client devopsclient.Interface, oldImageRegistryBinding, newImageRegistryBinding *v1alpha1.ImageRegistryBinding) (*v1alpha1.ImageRegistryBinding, error) {
  binding := oldImageRegistryBinding.DeepCopy()
  binding.SetAnnotations(newImageRegistryBinding.GetAnnotations())
  binding.Spec = newImageRegistryBinding.Spec
  return client.DevopsV1alpha1().ImageRegistryBindings(newImageRegistryBinding.Namespace).Update(binding)
}

func CreateImageRegistryBinding(client devopsclient.Interface, imageRegistryBinding *v1alpha1.ImageRegistryBinding, namespace string) (*v1alpha1.ImageRegistryBinding, error) {
  return client.DevopsV1alpha1().ImageRegistryBindings(namespace).Create(imageRegistryBinding)
}

func DeleteImageRegistryBinding(client devopsclient.Interface, namespace, name string) error {
  return client.DevopsV1alpha1().ImageRegistryBindings(namespace).Delete(name, &v1.DeleteOptions{})
}
