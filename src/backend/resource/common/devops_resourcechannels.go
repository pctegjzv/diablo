package common

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/api"
)

// ProjectListChannel is a list and error channels to Projects.
type ProjectListChannel struct {
	List  chan *devopsv1alpha1.ProjectList
	Error chan error
}

// GetProjectListChannel returns a pair of channels to a Project list and errors that both must
// be read
// numReads times.
func GetProjectListChannel(client devopsclient.Interface, numReads int) ProjectListChannel {
	channel := ProjectListChannel{
		List:  make(chan *devopsv1alpha1.ProjectList, numReads),
		Error: make(chan error, numReads),
	}

	go func() {
		list, err := client.DevopsV1alpha1().Projects().List(api.ListEverything)

		for i := 0; i < numReads; i++ {
			channel.List <- list
			channel.Error <- err
		}
	}()

	return channel
}
