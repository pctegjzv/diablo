package common

// FilterNamespacedServicesBySelector returns services targeted by given resource selector in

import (
	"k8s.io/api/core/v1"
	extensions "k8s.io/api/extensions/v1beta1"
)

type IngressInfo struct {
	host string
	path string
}

func (ii IngressInfo) getVisitAddress() string {
	return "http://" + ii.host + ii.path
}

func GetVisitAddressByIngressAndServiceInfo(is []extensions.Ingress, ss []v1.Service, namespace string, resourceSelector map[string]string, containers []v1.Container) (visitAddresses []string) {
	sis := FilterServicesInfoBySelectorAndPort(ss, namespace, resourceSelector, containers)
	var portMap = make(map[ServiceInfo]bool)
	for _, si := range sis {
		portMap[si] = true
	}
	visitAddresses = make([]string, 0)
	for _, i := range is {
		if i.Namespace != namespace {
			continue
		}
		for _, rule := range i.Spec.Rules {
			for _, path := range rule.HTTP.Paths {
				si := ServiceInfo{
					Name:      path.Backend.ServiceName,
					Namespace: namespace,
					Port:      path.Backend.ServicePort,
				}
				if portMap[si] {
					visitAddresses = append(visitAddresses, IngressInfo{
						host: rule.Host,
						path: path.Path,
					}.getVisitAddress())
				}
			}
		}
	}

	return visitAddresses
}
