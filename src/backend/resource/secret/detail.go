// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package secret

import (
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"log"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/api"
	"alauda.io/diablo/src/backend/resource/common"

	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// SecretDetail API resource provides mechanisms to inject containers with configuration data while keeping
// containers agnostic of Kubernetes
type SecretDetail struct {
	ObjectMeta api.ObjectMeta `json:"metadata"`
	TypeMeta   api.TypeMeta   `json:",inline"`

	// Data contains the secret data.  Each key must be a valid DNS_SUBDOMAIN
	// or leading dot followed by valid DNS_SUBDOMAIN.
	// The serialized form of the secret data is a base64 encoded string,
	// representing the arbitrary (possibly non-string) data value here.
	Data map[string][]byte `json:"data"`

	// Used to facilitate programmatic handling of secret data.
	Type v1.SecretType `json:"type"`
}

// GetSecretDetail returns returns detailed information about a secret
func GetSecretDetail(client kubernetes.Interface, namespace, name string) (*SecretDetail, error) {
	log.Printf("Getting details of %s secret in %s namespace\n", name, namespace)

	rawSecret, err := client.CoreV1().Secrets(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return getSecretDetail(rawSecret), nil
}

// UpdateSecret updates a single secret using the cluster API client
func UpdateSecret(client kubernetes.Interface, spec *SecretDetail) (*Secret, error) {
	namespace := spec.ObjectMeta.Namespace
	secret, err := client.CoreV1().Secrets(namespace).Get(spec.ObjectMeta.Name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	if secret.Type == devopsv1alpha1.SecretTypeOAuth2 {
		ownerReferences := secret.GetOwnerReferences()
		if ownerReferences != nil && len(ownerReferences) > 1 {
			return nil, errors.NewForbidden(schema.GroupResource{}, secret.GetName(),
				fmt.Errorf("Secret '%s' is not allowed to update if it is referenced by other resources", secret.GetName()))
		}
	}

	anno := common.DevOpsAnnotator{}
	newMeta := api.NewRawObjectMeta(spec.ObjectMeta)
	secret.ObjectMeta = anno.GetProductAnnotations(api.CompleteMeta(newMeta, secret.ObjectMeta))
	secret.Data = spec.Data
	_, err = client.CoreV1().Secrets(namespace).Update(secret)
	return toSecret(secret), err
}

func getSecretDetail(rawSecret *v1.Secret) *SecretDetail {
	return &SecretDetail{
		ObjectMeta: api.NewObjectMeta(rawSecret.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindSecret),
		Data:       rawSecret.Data,
		Type:       rawSecret.Type,
	}
}

func OAuthCallback(client devopsclient.Interface, k8sclient kubernetes.Interface,
	namespace, secretName, serviceName, code string) error {
	log.Printf("oauth callback, secretName: %s; serviceName: %s; code: %s", secretName, serviceName, code)
	var (
		hasCodeRepoService bool
		service            *devopsv1alpha1.CodeRepoService
		secret, secretCopy *v1.Secret
		err                error
		controller         = false
		blockOwnerDeletion = false
	)

	if code == "" {
		return fmt.Errorf("oauth2 callback url does not include the param 'code'")
	}

	service, err = client.DevopsV1alpha1().CodeRepoServices().Get(serviceName, metaV1.GetOptions{})
	if err != nil {
		return err
	}

	secret, err = k8sclient.CoreV1().Secrets(namespace).Get(secretName, metaV1.GetOptions{})
	if err != nil {
		return err
	}
	secretCopy = secret.DeepCopy()

	ownerReferences := secretCopy.GetOwnerReferences()
	if ownerReferences != nil && len(ownerReferences) > 0 {
		for _, ref := range ownerReferences {
			if ref.Kind == devopsv1alpha1.TypeCodeRepoService && ref.Name == serviceName {
				hasCodeRepoService = true
				break
			}
		}
	}

	if !hasCodeRepoService {
		// will override the ownerReferences
		ownerReferences = make([]metaV1.OwnerReference, 0)
		ownerReferences = append(ownerReferences, metaV1.OwnerReference{
			APIVersion:         devopsv1alpha1.APIVersionV1Alpha1,
			Kind:               devopsv1alpha1.TypeCodeRepoService,
			UID:                service.GetUID(),
			Name:               service.GetName(),
			Controller:         &controller,
			BlockOwnerDeletion: &blockOwnerDeletion,
		})
		secretCopy.SetOwnerReferences(ownerReferences)
	}

	if secretCopy.StringData == nil {
		secretCopy.StringData = make(map[string]string, 0)
	}

	secretCopy.StringData[devopsv1alpha1.OAuth2CodeKey] = code
	_, err = k8sclient.CoreV1().Secrets(namespace).Update(secretCopy)
	return err
}
