package pipelinetemplate

import (
	"log"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
)

// PreviewOptions used for render jenkinsfile
type PreviewOptions struct {
	Source *v1alpha1.PipelineSource
	Values map[string]string
}

// RenderJenkinsfile render jenkinsfile
func RenderJenkinsfile(client devopsclient.Interface, namespace string, name string, options *PreviewOptions) (jenkinsfile string, err error) {
	var source *v1alpha1.PipelineSource
	if options != nil && options.Source != nil {
		source = options.Source
	}

	opts := &v1alpha1.JenkinsfilePreviewOptions{
		Source: source,
		Values: options.Values,
	}

	log.Printf("Render jenkinsfile from PipelineTemplate namespace[%s], name[%s]", namespace, name)

	result, err := client.DevopsV1alpha1().PipelineTemplates(namespace).Preview(name, opts)
	return result.Jenkinsfile, err
}
