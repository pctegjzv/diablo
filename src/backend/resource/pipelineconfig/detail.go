package pipelineconfig

import (
	"log"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/resource/common"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type PipelineConfigDetail struct {
	// same as PipelineConfig
	PipelineConfig

	// but add the last execution details
	// TODO
}

type PipelineConfigTrigger struct {
	Name      string                             `json:"name"`
	Namespace string                             `json:"namespace"`
	Commit    string                             `json:"commit"`
	Params    []devopsv1alpha1.PipelineParameter `json:"params"`
}

type PipelineTriggerResponse struct {
	*devopsv1alpha1.Pipeline
}

// GetPipelineConfigDetail get pipeline config details
func GetPipelineConfigDetail(client devopsclient.Interface, k8sclient kubernetes.Interface, namespace string,
	name string) (*PipelineConfigDetail, error) {
	log.Printf("Getting details of %s pipeline config in %s namespace", name, namespace)

	config, err := client.DevopsV1alpha1().PipelineConfigs(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		log.Printf("Error listing PipelineConfig: namespace %s. err %v", namespace, err)
		return nil, err
	}

	pipelines, err := client.DevopsV1alpha1().Pipelines(namespace).List(devopsv1alpha1.ListEverything)
	if err != nil {
		log.Printf("Error listing Pipeline: namespace %s. err %v", namespace, err)
		return nil, err
	}
	return &PipelineConfigDetail{
		PipelineConfig: toPipelineConfig(*config, pipelines.Items),
	}, nil
}

// UpdatePipelineConfigDetail update pipeline config detail
func UpdatePipelineConfigDetail(client devopsclient.Interface, k8sclient kubernetes.Interface, spec *PipelineConfigDetail) (*PipelineConfigDetail, error) {
	namespace := spec.ObjectMeta.Namespace
	old, err := client.DevopsV1alpha1().PipelineConfigs(namespace).Get(spec.ObjectMeta.Name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}
	old.Spec = spec.Spec

	old.SetAnnotations(common.MergeAnnotations(old.ObjectMeta.Annotations, spec.ObjectMeta.Annotations))
	old.SetLabels(common.MergeAnnotations(old.ObjectMeta.Labels, spec.ObjectMeta.Labels))

	// set phase as initial
	old.Status.Phase = devopsv1alpha1.PipelineConfigPhaseCreating
	old, err = client.DevopsV1alpha1().PipelineConfigs(namespace).Update(old)
	if err != nil {
		return nil, err
	}
	return spec, nil
}

// DeletePipelineConfig delete pipeline config
func DeletePipelineConfig(client devopsclient.Interface, k8sclient kubernetes.Interface, namespace, name string) error {
	return client.DevopsV1alpha1().PipelineConfigs(namespace).Delete(name, &metaV1.DeleteOptions{})
}

// TriggerPipelineConfig triggers a pipeline
func TriggerPipelineConfig(client devopsclient.Interface, k8sclient kubernetes.Interface, spec *PipelineConfigTrigger) (response *PipelineTriggerResponse, err error) {
	var (
		config *devopsv1alpha1.PipelineConfig
	)
	config, err = client.DevopsV1alpha1().PipelineConfigs(spec.Namespace).Get(spec.Name, metaV1.GetOptions{})
	if err != nil {
		return
	}
	// read only copy to be writable
	config = config.DeepCopy()
	pipe := generatePipelineFromConfig(config)
	if spec.Commit != "" {
		ann := pipe.ObjectMeta.GetAnnotations()
		if ann == nil {
			ann = make(map[string]string)
		}
		ann[common.AnnotationsCommit] = spec.Commit
		pipe.ObjectMeta.SetAnnotations(ann)
	}

	log.Println("Receive params: ", spec.Params)

	pipe.Spec.Parameters = append(pipe.Spec.Parameters, spec.Params...)

	pipe, err = client.DevopsV1alpha1().Pipelines(spec.Namespace).Create(pipe)
	if err != nil {
		return
	}
	response = &PipelineTriggerResponse{
		Pipeline: pipe,
	}
	return
}

func generatePipelineFromConfig(config *devopsv1alpha1.PipelineConfig) (pipe *devopsv1alpha1.Pipeline) {
	pipe = &devopsv1alpha1.Pipeline{
		ObjectMeta: common.CloneMeta(config.ObjectMeta),
		Spec: devopsv1alpha1.PipelineSpec{
			JenkinsBinding: config.Spec.JenkinsBinding,
			PipelineConfig: devopsv1alpha1.LocalObjectReference{
				Name: config.GetName(),
			},
			Cause: devopsv1alpha1.PipelineCause{
				Type:    devopsv1alpha1.PipelineCauseTypeManual,
				Message: "Triggered using Alauda DevOps Console",
			},
			RunPolicy: config.Spec.RunPolicy,
			Triggers:  config.Spec.Triggers,
			Strategy:  config.Spec.Strategy,
			Hooks:     config.Spec.Hooks,
			Source:    config.Spec.Source,
		},
	}
	return
}
