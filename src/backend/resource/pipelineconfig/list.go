package pipelineconfig

import (
	"fmt"
	"log"
	"sort"
	"sync"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/api"
	"alauda.io/diablo/src/backend/errors"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"alauda.io/diablo/src/backend/resource/pipeline"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// PipelineConfigList a list of PipelineConfigs
type PipelineConfigList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of PipelineConfig.
	Items []PipelineConfig `json:"pipelineconfigs"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetItems return a slice of common.Resource
func (list *PipelineConfigList) GetItems() (res []common.Resource) {
	if list == nil {
		res = []common.Resource{}
	} else {
		res = make([]common.Resource, len(list.Items))
		for i, d := range list.Items {
			res[i] = d
		}
	}
	return
}

// PipelineConfig is a presentation layer view of Kubernetes namespaces. This means it is namespace plus
// additional augmented data we can get from other sources.
type PipelineConfig struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Spec      devopsv1alpha1.PipelineConfigSpec   `json:"spec"`
	Status    devopsv1alpha1.PipelineConfigStatus `json:"status"`
	Pipelines []pipeline.Pipeline                 `json:"pipelines"`
}

// GetObjectMeta object meta
func (p PipelineConfig) GetObjectMeta() api.ObjectMeta {
	return p.ObjectMeta
}

// CreatePipelineConfigDetail create pipeline config detail
func CreatePipelineConfigDetail(client devopsclient.Interface, spec *PipelineConfigDetail) (config *devopsv1alpha1.PipelineConfig, err error) {
	if spec == nil {
		return nil, nil
	}
	config = &devopsv1alpha1.PipelineConfig{
		TypeMeta: metaV1.TypeMeta{
			Kind:       "PipelineConfig",
			APIVersion: "devops.alauda.io/v1alpha1",
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Namespace:   spec.ObjectMeta.Namespace,
			Labels:      spec.ObjectMeta.Labels,
			Annotations: spec.ObjectMeta.Annotations,
		},
		Spec: spec.PipelineConfig.Spec,
		Status: devopsv1alpha1.PipelineConfigStatus{
			Phase: devopsv1alpha1.PipelineConfigPhaseCreating, // initial phase should be creating
		},
	}

	err = handleSpec(client, config, spec.PipelineConfig.Spec.Strategy.Template)
	if err == nil {
		namespace := spec.ObjectMeta.Namespace
		config, err = client.DevopsV1alpha1().PipelineConfigs(namespace).Create(config)
		if err != nil {
			return nil, err
		}
	}

	return config, err
}

func handleSpec(client devopsclient.Interface, config *devopsv1alpha1.PipelineConfig, template *devopsv1alpha1.PipelineConfigTemplate) (err error) {
	configTemplate := template
	if configTemplate != nil {
		name := configTemplate.Name
		namespace := configTemplate.Namespace

		kind := configTemplate.Kind
		switch kind {
		case "ClusterPipelineTemplate":
			clusterPipelineTemplate, err := client.DevopsV1alpha1().ClusterPipelineTemplates().Get(name, metaV1.GetOptions{})

			if err == nil {
				handleTemplateInstance(client, namespace, &clusterPipelineTemplate.DeepCopy().Spec, config, configTemplate, getClusterTaskTemplate)
			}
		case "PipelineTemplate":
			pipelineTemplate, err := client.DevopsV1alpha1().PipelineTemplates(namespace).Get(name, metaV1.GetOptions{})

			if err == nil {
				handleTemplateInstance(client, namespace, &pipelineTemplate.DeepCopy().Spec, config, configTemplate, getTaskTemplate)
			}
		default:
			return fmt.Errorf("unknow template kind: %s", kind)
		}
	}

	return
}

func getTaskTemplate(client devopsclient.Interface, namespace string, taskName string) (*devopsv1alpha1.PipelineTaskTemplateSpec, error) {
	taskTemplate, err := client.DevopsV1alpha1().PipelineTaskTemplates(namespace).Get(taskName, metaV1.GetOptions{})
	if err == nil {
		return &taskTemplate.Spec, nil
	}
	return nil, err
}

func getClusterTaskTemplate(client devopsclient.Interface, namespace string, taskName string) (*devopsv1alpha1.PipelineTaskTemplateSpec, error) {
	log.Printf("skip namespace: %s", namespace)
	taskTemplate, err := client.DevopsV1alpha1().ClusterPipelineTaskTemplates().Get(taskName, metaV1.GetOptions{})
	if err == nil {
		return &taskTemplate.Spec, nil
	}
	return nil, err
}

func handleTemplateInstance(client devopsclient.Interface, namespace string, templateSpec *devopsv1alpha1.PipelineTemplateSpec, config *devopsv1alpha1.PipelineConfig, configTemplate *devopsv1alpha1.PipelineConfigTemplate,
	getTaskSpec func(devopsclient.Interface, string, string) (*devopsv1alpha1.PipelineTaskTemplateSpec, error)) {
	for i, param := range templateSpec.Parameters {
		param.Value = configTemplate.Spec.Parameters[i].Value
	}
	for i, arg := range templateSpec.Arguments {
		for j, item := range arg.Items {
			item.Value = configTemplate.Spec.Arguments[i].Items[j].Value
		}
	}

	for i, stage := range config.Spec.Strategy.Template.Spec.Stages {
		for j, task := range stage.Tasks {
			task.ObjectMeta.Name = templateSpec.Stages[i].Tasks[j].Name
			task.Spec.Type = templateSpec.Stages[i].Tasks[j].Type

			taskSpec, err := getTaskSpec(client, namespace, task.Name)
			if err == nil {
				task.Spec.Body = taskSpec.Body
			}

			stage.Tasks[j] = task
		}
		config.Spec.Strategy.Template.Spec.Stages[i] = stage
	}

	config.Spec.Strategy.Template.Spec.Agent = templateSpec.Agent
	config.Spec.Strategy.Template.Spec.Parameters = templateSpec.Parameters
	config.Spec.Strategy.Template.Spec.Arguments = templateSpec.Arguments
}

// GetPipelineConfigList returns a PipelineConfigList
func GetPipelineConfigList(client devopsclient.Interface,
	namespace *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (*PipelineConfigList, error) {
	log.Println("Getting list of pipelineconfigs")
	var (
		configCritical, pipelineCritical       error
		configNonCritical, pipelineNonCritical []error
		pipelineConfigList                     *devopsv1alpha1.PipelineConfigList
		pipelineList                           *devopsv1alpha1.PipelineList
		wait                                   sync.WaitGroup
	)
	wait.Add(2)
	// fetching list of PipelineConfig
	go func() {
		pipelineConfigList, configCritical = client.DevopsV1alpha1().PipelineConfigs(namespace.ToRequestParam()).List(devopsv1alpha1.ListEverything)
		configNonCritical, configCritical = errors.HandleError(configCritical)
		if configCritical != nil {
			log.Println("error while listing pipeline configs", configCritical)
		}
		wait.Done()
	}()
	go func() {
		pipelineList, pipelineCritical = client.DevopsV1alpha1().Pipelines(namespace.ToRequestParam()).List(devopsv1alpha1.ListEverything)
		pipelineNonCritical, pipelineCritical = errors.HandleError(pipelineCritical)
		if pipelineCritical != nil {
			log.Println("error while listing pipelines", pipelineCritical)
		}
		wait.Done()
	}()
	wait.Wait()
	if configCritical != nil {
		return nil, configCritical
	}
	if pipelineCritical != nil {
		return nil, pipelineCritical
	}
	return toPipelineConfigList(pipelineConfigList.Items, pipelineList.Items, append(configNonCritical, pipelineNonCritical...), dsQuery), nil
}

func toPipelineConfigList(configs []devopsv1alpha1.PipelineConfig, pipelines []devopsv1alpha1.Pipeline, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *PipelineConfigList {
	configList := &PipelineConfigList{
		Items:    make([]PipelineConfig, 0),
		ListMeta: api.ListMeta{TotalItems: len(configs)},
	}

	configCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(configs), dsQuery)
	configs = fromCells(configCells)
	configList.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	configList.Errors = nonCriticalErrors

	for _, conf := range configs {
		configList.Items = append(configList.Items, toPipelineConfig(conf, pipelines))
	}

	var asc, sortedByPipelineCreationTimestampProperty bool
	if dsQuery != nil && dsQuery.SortQuery != nil && len(dsQuery.SortQuery.SortByList) > 0 {
		for _, sortBy := range dsQuery.SortQuery.SortByList {
			if sortBy.Property == dataselect.PipelineCreationTimestampProperty {
				asc = sortBy.Ascending
				sortedByPipelineCreationTimestampProperty = true
				break
			}
		}
	}

	if !sortedByPipelineCreationTimestampProperty {
		return configList
	}

	sort.SliceStable(configList.Items, func(i, j int) bool {
		if asc {
			if len(configList.Items[i].Pipelines) == 0 {
				return true
			}
			if len(configList.Items[j].Pipelines) == 0 {
				return false
			}
			return !configList.Items[i].Pipelines[0].GetObjectMeta().CreationTimestamp.After(configList.Items[j].Pipelines[0].GetObjectMeta().CreationTimestamp.Time)
		}

		if len(configList.Items[i].Pipelines) == 0 {
			return false
		}
		if len(configList.Items[j].Pipelines) == 0 {
			return true
		}
		return configList.Items[i].Pipelines[0].GetObjectMeta().CreationTimestamp.After(configList.Items[j].Pipelines[0].GetObjectMeta().CreationTimestamp.Time)
	})

	return configList
}

func toPipelineConfig(config devopsv1alpha1.PipelineConfig, pipelines []devopsv1alpha1.Pipeline) PipelineConfig {
	pipelineConfig := PipelineConfig{
		ObjectMeta: api.NewObjectMeta(config.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindPipelineConfig),
		Pipelines:  make([]pipeline.Pipeline, 0, len(pipelines)),
		// data here
		Spec:   config.Spec,
		Status: config.Status,
	}
	if len(pipelines) > 0 {
		for _, p := range pipelines {
			if p.Spec.PipelineConfig.Name == config.GetName() {
				pipelineConfig.Pipelines = append(pipelineConfig.Pipelines, pipeline.ToPipeline(p))
			}
		}
		sort.SliceStable(pipelineConfig.Pipelines, func(i, j int) bool {
			return pipelineConfig.Pipelines[i].GetObjectMeta().CreationTimestamp.After(pipelineConfig.Pipelines[j].GetObjectMeta().CreationTimestamp.Time)
		})
	}
	return pipelineConfig
}
