package coderepository

import (
	"alauda.io/diablo/src/backend/resource/common"
	"fmt"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
	"sync"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/diablo/src/backend/api"
	"alauda.io/diablo/src/backend/errors"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"alauda.io/diablo/src/backend/resource/pipelineconfig"
)

type ResourceItem struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
	Kind      string `json:"kind"`
}

type ResourceList struct {
	Items []ResourceItem `json:"items"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// CodeRepositoryList contains a list of CodeRepository in the cluster.
type CodeRepositoryList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of CodeRepository.
	Items []CodeRepository `json:"coderepositories"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// CodeRepository is a presentation layer view of Kubernetes namespaces. This means it is namespace plus
// additional augmented data we can get from other sources.
type CodeRepository struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Spec   v1alpha1.CodeRepositorySpec `json:"spec"`
	Status v1alpha1.ServiceStatus      `json:"status"`
}

// GetResourceList get the resources refer to codeRepository
func GetResourceList(client devopsclient.Interface, namespace *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (resourceList *ResourceList, err error) {
	resourceList = &ResourceList{}
	repositoryList, err := GetCodeRepositoryList(client, namespace, dsQuery)
	if err != nil {
		return
	}

	wait := sync.WaitGroup{}
	for _, r := range repositoryList.Items {
		go func(namespace, name string) {
			items := GetResourceListByRepository(client, namespace, name)
			resourceList.Items = append(resourceList.Items, items...)
			wait.Done()
		}(r.ObjectMeta.Namespace, r.ObjectMeta.Name)
		wait.Add(1)
	}
	wait.Wait()
	return
}

func GetResourcesReferToRemovedRepos(client devopsclient.Interface, oldBinding, newBinding *v1alpha1.CodeRepoBinding) (resourceList *ResourceList) {
	resourceList = &ResourceList{}

	var removedRepoNames []string
	for _, oldCondition := range oldBinding.Status.Conditions {
		if oldCondition.Type != v1alpha1.JenkinsBindingStatusTypeRepository {
			continue
		}

		var found bool
		for _, newCondition := range newBinding.Status.Conditions {
			if newCondition.Type != v1alpha1.JenkinsBindingStatusTypeRepository {
				continue
			}

			if oldCondition.Name == newCondition.Name {
				found = true
				break
			}
		}
		if !found {
			removedRepoNames = append(removedRepoNames, oldCondition.Name)
		}
	}

	log.Println("removedRepoNames: ", removedRepoNames)
	wait := sync.WaitGroup{}
	for _, repoName := range removedRepoNames {
		log.Println("get resource refer to ", repoName)
		go func(namespace, name string) {
			items := GetResourceListByRepository(client, namespace, name)
			resourceList.Items = append(resourceList.Items, items...)
			wait.Done()
		}(newBinding.Namespace, repoName)
		wait.Add(1)
	}
	wait.Wait()

	return
}

func GetResourceListByRepository(client devopsclient.Interface, namespace, name string) (items []ResourceItem) {
	items = []ResourceItem{}
	repoQuery := dataselect.GeSimpletLabelQuery(dataselect.CodeRepositoryProperty, name)
	pipelineConfigs, depErr := pipelineconfig.GetPipelineConfigList(client, common.NewSameNamespaceQuery(namespace), repoQuery)
	if depErr != nil {
		log.Println(fmt.Sprintf("error fetch pipelineconfigs by codeRepository: %s/%s", namespace, name))
	}
	if pipelineConfigs != nil {
		for _, item := range pipelineConfigs.Items {
			items = append(items, ResourceItem{
				Name:      item.ObjectMeta.Name,
				Namespace: item.ObjectMeta.Namespace,
				Kind:      string(item.TypeMeta.Kind),
			})
		}
	}
	return
}

// GetCodeRepositoryList returns a list of coderepobinding
func GetCodeRepositoryList(client devopsclient.Interface, namespace *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (*CodeRepositoryList, error) {
	log.Println("Getting list of repository")

	crsList, err := client.DevopsV1alpha1().CodeRepositories(namespace.ToRequestParam()).List(v1alpha1.ListEverything)
	if err != nil {
		log.Println("error while listing repositories", err)
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	return toList(crsList.Items, nonCriticalErrors, dsQuery), nil
}

// GetCodeRepositoryList returns a list of coderepobinding
func GetCodeRepositoryListInBinding(client devopsclient.Interface, namespace, name string, dsQuery *dataselect.DataSelectQuery) (*CodeRepositoryList, error) {
	log.Println("Getting list of repository from binding ", name)

	binding, err := client.DevopsV1alpha1().CodeRepoBindings(namespace).Get(name, v1.GetOptions{})
	if err != nil {
		return nil, err
	}

	crsList, err := client.DevopsV1alpha1().CodeRepositories(namespace).List(v1alpha1.ListEverything)
	if err != nil {
		log.Println("error while listing repositories", err)
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	var repos []v1alpha1.CodeRepository
	for _, item := range crsList.Items {
		for _, condition := range binding.Status.Conditions {
			if item.GetName() == condition.Name {
				repos = append(repos, item)
			}
		}
	}

	return toList(repos, nonCriticalErrors, dsQuery), nil
}

func toList(codeRepositories []v1alpha1.CodeRepository, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *CodeRepositoryList {
	crsList := &CodeRepositoryList{
		Items:    make([]CodeRepository, 0),
		ListMeta: api.ListMeta{TotalItems: len(codeRepositories)},
	}

	crsCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(codeRepositories), dsQuery)
	codeRepositories = fromCells(crsCells)
	crsList.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	crsList.Errors = nonCriticalErrors

	for _, repo := range codeRepositories {
		crsList.Items = append(crsList.Items, toDetailsInList(repo))
	}

	return crsList
}

func toDetailsInList(codeRepository v1alpha1.CodeRepository) CodeRepository {
	crs := CodeRepository{
		ObjectMeta: api.NewObjectMeta(codeRepository.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindCodeRepository),
		Spec:       codeRepository.Spec,
		Status:     codeRepository.Status,
	}
	return crs
}
