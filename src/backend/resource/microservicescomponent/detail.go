package microservicescomponent

import (
	"log"

	asfclient "alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	asfv1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	client "alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	"alauda.io/diablo/src/backend/errors"
)

func UpdateMicroservicesComponent(client client.Interface, namespace, name string, component *asfv1.MicroservicesComponent) (mscompDetail *asfv1.MicroservicesComponent, err error) {
	msComp, err := client.AsfV1alpha1().MicroservicesComponents(namespace).Update(component)
	if err != nil {
		log.Println("error while update microservice component", err)
	}

	_, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	return msComp, nil
}

func GetMicroservicesComponentDetail(client asfclient.Interface, k8sclient kubernetes.Interface, namespace, name string) (mscompDetail *asfv1.MicroservicesComponent, err error) {
	mscomp, err := client.AsfV1alpha1().MicroservicesComponents(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		log.Println("error while get microservice Components detail", err)

	}
	_, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	return mscomp, nil
}
