package other

import (
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"
)

func CreateResource(client *dynamic.Client, resource *v1.APIResource, namespace string, payload *unstructured.Unstructured) error {
	_, err := client.Resource(resource, namespace).Create(payload)
	return err
}
