package microservicesconfiguration

import (
	"crypto/tls"
	"encoding/json"
	errs "errors"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"k8s.io/apimachinery/pkg/runtime"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	asfclient "alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	"alauda.io/diablo/src/backend/api"
	"alauda.io/diablo/src/backend/errors"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"

	asfv1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"

	//
	"github.com/hudl/fargo"
	"k8s.io/client-go/kubernetes"
)

const (
	UP           string = "UP"
	DOWN         string = "DOWN"
	STARTING     string = "STARTING"
	OUTOFSERVICE string = "OUT_OF_SERVICE"
	UNKNOWN      string = "UNKNOWN"
)

type Entry interface{}

type Configuration struct {
	Name     string                 `json:"name"`
	Profile  string                 `json:"profile"`
	Label    string                 `json:"label"`
	FileName string                 `json:"fileName"`
	Source   map[string]interface{} `json:"source"`
}

// MicroservicesEnvironmentDetailList contains a list of MicroservicesEnvironments in the cluster.
type MicroservicesConfigurationList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Jenkins.
	Configs []Configuration `json:"apps"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetMicroservicesEnvironmentDetailList returns a list of microservicesenvironments in the cluster.
func GetMicroservicesConfigurationList(client asfclient.Interface, k8sclient kubernetes.Interface, projectName string, dsQuery *dataselect.DataSelectQuery) (*MicroservicesConfigurationList, error) {
	log.Println("Getting list of microservice applications")

	var err, criticalError error
	var nonCriticalErrors []error

	configurations := make([]Configuration, 0)

	msEnv, err := GetMicroservicesEnvironmentByProjectName(client, k8sclient, projectName)
	if err != nil {
		log.Println("error while get microservice envrionment", err)

	}

	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	eurekaUrl, configserverUrl, profiles, labels := GetServicesURLs(*msEnv)

	if configserverUrl == "" {
		log.Println("faild to get eureka url for project " + projectName)
		return nil, errs.New("faild to get eureka url for project " + projectName)
	}

	//applications := fakeApplications()
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	fargo.HttpClient = &http.Client{Transport: tr}

	c := fargo.NewConn(eurekaUrl + "eureka")
	apps, err := c.GetApps()

	if err != nil {
		log.Println("faild to get eureka apps for project  " + projectName)
		return nil, errs.New("faild to get eureka apps for project " + projectName)

	}
	appNames := []string{}

	for _, app := range apps {
		appName := strings.ToLower(app.Name)
		appNames = append(appNames, appName)
	}

	loader := &ConfigServerLoader{
		ConfigServerURL: configserverUrl,
		Apps:            appNames,
		Profiles:        profiles,
		Labels:          labels,
	}
	configMaplist := loader.Load()

	for _, config := range configMaplist {
		configurations = append(configurations, *config)
	}

	//applications = fakeApplications()

	return toList(configurations, nonCriticalErrors, dsQuery), nil
}

func toList(configs []Configuration, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *MicroservicesConfigurationList {
	list := &MicroservicesConfigurationList{
		ListMeta: api.ListMeta{TotalItems: len(configs)},
	}

	cells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(configs), dsQuery)
	configs = fromCells(cells)
	list.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	list.Errors = nonCriticalErrors
	list.Configs = configs

	return list
}

func fakeApplications() []fargo.Application {
	apps := make([]fargo.Application, 0)
	appNames := []string{"demoservice", "asfdemo-client", "stockService", "userService"}
	for _, appName := range appNames {
		apps = append(apps, fargo.Application{
			Name:      appName,
			Instances: fakeInstance(appName, []string{UP, DOWN, UNKNOWN, OUTOFSERVICE, STARTING}),
		})

	}

	return apps
}

func fakeInstance(appName string, status []string) []*fargo.Instance {

	instances := make([]*fargo.Instance, 0)

	for _, instanceStatus := range status {
		instances = append(instances, &fargo.Instance{
			InstanceId: GetRandomString(8),
			HostName:   GetRandomString(6),
			App:        appName,
			Status:     fargo.StatusType(instanceStatus),
		})
	}

	return instances

}

func GetRandomString(l int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyz"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

func GetMicroservicesEnvironmentByProjectName(client asfclient.Interface, k8sclient kubernetes.Interface, projectName string) (*asfv1alpha1.MicroservicesEnvironment, error) {

	var bindings *asfv1alpha1.MicroservicesEnvironmentBindingList
	var err, criticalError error
	bindings, err = client.AsfV1alpha1().MicroservicesEnvironmentBindings(projectName).List(metaV1.ListOptions{
		LabelSelector: labels.Everything().String(),
		FieldSelector: fields.Everything().String(),
	})
	if err != nil {
		log.Println("error while listing microservice bindings", err)

		_, criticalError = errors.HandleError(err)
		if criticalError != nil {
			return nil, criticalError
		}

	}

	if bindings != nil && len(bindings.Items) > 0 {
		// for now , one project bind to one envrionment
		binding := bindings.Items[0]

		microservicesEnvironmet, err := client.AsfV1alpha1().MicroservicesEnvironments().Get(binding.Spec.MicroservicesEnviromentRef.Name, metaV1.GetOptions{})

		if err != nil {
			log.Println("error while listing microservice bindings", err)

			_, criticalError = errors.HandleError(err)
			if criticalError != nil {
				return nil, criticalError
			}

		}

		return microservicesEnvironmet, nil
	}

	return nil, errs.New("failed to find binding microservicesenvironment for project " + projectName)

}

func GetServicesURLs(msEnv asfv1alpha1.MicroservicesEnvironment) (string, string, []string, []string) {

	var eurekaUrl, configserverUrl = "", ""
	profiles := []string{"development", "test", "production", "dev", "prod", "qa"}
	labels := []string{"master", "test"}
	if len(msEnv.Spec.MicroservicesComponentRefs) > 0 {
		for _, componentRef := range msEnv.Spec.MicroservicesComponentRefs {

			if componentRef.Name == "config-server" && componentRef.Status == asfv1alpha1.StatusRunning {
				if *componentRef.IngressHost != "" {
					configserverUrl = *componentRef.IngressHost
				} else {
					configserverUrl = *componentRef.Host + "/"
				}
				profiles = GetSubValues(componentRef, "profiles")
				labels = GetSubValues(componentRef, "labels")

			} else if componentRef.Name == "eureka" && componentRef.Status == asfv1alpha1.StatusRunning {
				if *componentRef.IngressHost != "" {
					eurekaUrl = *componentRef.IngressHost
				} else {
					eurekaUrl = *componentRef.Host + "/"
				}

			}
		}
	}

	if len(labels) == 0 {
		labels = []string{"master"}
	}

	if len(profiles) == 0 {
		profiles = []string{"dev", "test", "prod"}
	}

	return eurekaUrl, configserverUrl, profiles, labels

}

func GetSubValues(msComp asfv1alpha1.MicroservicesComponentRef, key string) []string {

	var subValues *map[string]*runtime.RawExtension = msComp.SubValues

	//result := make(map[string]interface{})

	var entry []string

	for k, out := range *subValues {

		if k == key {
			err := json.Unmarshal(out.Raw, &entry)

			if err != nil {
				log.Println("error get subvalues for  ", key, err)

				continue

			}

			return entry

		}

	}

	return make([]string, 0)

}
