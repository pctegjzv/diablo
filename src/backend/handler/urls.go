package handler

import (
	"net/http"

	asfv1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/diablo/src/backend/auth"
	authApi "alauda.io/diablo/src/backend/auth/api"
	clientapi "alauda.io/diablo/src/backend/client/api"
	"alauda.io/diablo/src/backend/integration"
	"alauda.io/diablo/src/backend/resource/application"
	"alauda.io/diablo/src/backend/resource/clusterpipelinetemplate"
	"alauda.io/diablo/src/backend/resource/coderepobinding"
	"alauda.io/diablo/src/backend/resource/codereposervice"
	"alauda.io/diablo/src/backend/resource/coderepository"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/configmap"
	"alauda.io/diablo/src/backend/resource/deployment"
	"alauda.io/diablo/src/backend/resource/imageregistry"
	"alauda.io/diablo/src/backend/resource/imageregistrybinding"
	"alauda.io/diablo/src/backend/resource/imagerepository"
	"alauda.io/diablo/src/backend/resource/jenkins"
	"alauda.io/diablo/src/backend/resource/jenkinsbinding"
	"alauda.io/diablo/src/backend/resource/logs"
	"alauda.io/diablo/src/backend/resource/microservicesapplication"
	"alauda.io/diablo/src/backend/resource/microservicesenvironment"
	"alauda.io/diablo/src/backend/resource/microservicesconfiguration"
	ns "alauda.io/diablo/src/backend/resource/namespace"
	"alauda.io/diablo/src/backend/resource/other"
	"alauda.io/diablo/src/backend/resource/pipeline"
	"alauda.io/diablo/src/backend/resource/pipelineconfig"
	"alauda.io/diablo/src/backend/resource/pipelinetasktemplate"
	"alauda.io/diablo/src/backend/resource/pipelinetemplate"
	"alauda.io/diablo/src/backend/resource/pipelinetemplatesync"
	"alauda.io/diablo/src/backend/resource/pod"
	"alauda.io/diablo/src/backend/resource/project"
	"alauda.io/diablo/src/backend/resource/projectmanagement"
	"alauda.io/diablo/src/backend/resource/projectmanagementbinding"
	"alauda.io/diablo/src/backend/resource/rbacrolebindings"
	"alauda.io/diablo/src/backend/resource/rbacroles"
	"alauda.io/diablo/src/backend/resource/release"
	"alauda.io/diablo/src/backend/resource/secret"
	"alauda.io/diablo/src/backend/resource/testtool"
	"alauda.io/diablo/src/backend/resource/testtoolbinding"
	"alauda.io/diablo/src/backend/resource/toolchain"
	"alauda.io/diablo/src/backend/settings"
	"alauda.io/diablo/src/backend/systembanner"
	"alauda.io/diablo/src/backend/thirdparty"
	thirdpartyapi "alauda.io/diablo/src/backend/thirdparty/api"
	"github.com/emicklei/go-restful"
	appsv1 "k8s.io/api/apps/v1"
	authv1 "k8s.io/api/authorization/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

const (
	URLIMAGEREGISTRYBINDINGDETAIL = "/imageregistrybinding/{namespace}/{name}"
)

// TerminalResponse is sent by handleExecShell. The Id is a random session id that binds the original REST request and the SockJS connection.
// Any clientapi in possession of this Id can hijack the terminal session.
type TerminalResponse struct {
	Id string `json:"id"`
}

// CreateHTTPAPIHandler creates a new HTTP handler that handles all requests to the API of the backend.
func CreateHTTPAPIHandler(iManager integration.IntegrationManager, cManager clientapi.DevOpsClientManager,
	authManager authApi.AuthManager, sManager settings.SettingsManager,
	sbManager systembanner.SystemBannerManager,
	tpManager thirdpartyapi.ThirdPartyManager) (

	http.Handler, error) {
	apiHandler := APIHandler{iManager: iManager, cManager: cManager, sManager: &sManager}
	wsContainer := restful.NewContainer()
	wsContainer.EnableContentEncoding(true)

	apiV1Ws := new(restful.WebService)

	InstallFilters(apiV1Ws, cManager)

	apiV1Ws.Path("/api/v1").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON).
		Param(restful.HeaderParameter("Authorization", "Given Bearer token will use this as authorization for the API"))

	wsContainer.Add(apiV1Ws)

	integrationHandler := integration.NewIntegrationHandler(iManager)
	integrationHandler.Install(apiV1Ws)

	authHandler := auth.NewAuthHandler(authManager)
	authHandler.Install(apiV1Ws)

	settingsHandler := settings.NewSettingsHandler(sManager)
	settingsHandler.Install(apiV1Ws)

	systemBannerHandler := systembanner.NewSystemBannerHandler(sbManager)
	systemBannerHandler.Install(apiV1Ws)

	thirPartyHandler := thirdparty.NewThirdPartyHandler(&sManager, cManager, tpManager)
	thirPartyHandler.Install(apiV1Ws)

	apiV1Ws.Route(
		apiV1Ws.POST("/appdeployment").
			To(apiHandler.handleDeploy).
			Reads(deployment.AppDeploymentSpec{}).
			Writes(deployment.AppDeploymentSpec{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/configuration").
			To(apiHandler.handleGetPlatformConfiguration).
			Writes(configmap.ConfigMapDetail{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/cani").
			To(apiHandler.handleCanI).
			Reads(authv1.SelfSubjectAccessReviewSpec{}).
			Writes(common.CanIResponse{}).
			Doc("Validates access for user").
			Returns(200, "OK", common.CanIResponse{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/apis").
			To(apiHandler.handleGetAPIGroups).
			Writes(metav1.APIGroupList{}).
			Doc("Fetches a list of API groups available").
			Returns(200, "OK", metav1.APIGroupList{}))

	//apiV1Ws.Route(
	//	apiV1Ws.POST("/appdeployment/validate/name").
	//		To(apiHandler.handleNameValidity).
	//		Reads(validation.AppNameAppNameValiditySpecValiditySpec{}).
	//		Writes(validation.AppNameValidity{}))
	//apiV1Ws.Route(
	//	apiV1Ws.POST("/appdeployment/validate/imagereference").
	//		To(apiHandler.handleImageReferenceValidity).
	//		Reads(validation.ImageReferenceValiditySpec{}).
	//		Writes(validation.ImageReferenceValidity{}))
	//apiV1Ws.Route(
	//	apiV1Ws.POST("/appdeployment/validate/protocol").
	//		To(apiHandler.handleProtocolValidity).
	//		Reads(validation.ProtocolValiditySpec{}).
	//		Writes(validation.ProtocolValidity{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/appdeployment/protocols").
	//		To(apiHandler.handleGetAvailableProcotols).
	//		Writes(deployment.Protocols{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.POST("/appdeploymentfromfile").
	//		To(apiHandler.handleDeployFromFile).
	//		Reads(deployment.AppDeploymentFromFileSpec{}).
	//		Writes(deployment.AppDeploymentFromFileResponse{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicationcontroller").
	//		To(apiHandler.handleGetReplicationControllerList).
	//		Writes(replicationcontroller.ReplicationControllerList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicationcontroller/{namespace}").
	//		To(apiHandler.handleGetReplicationControllerList).
	//		Writes(replicationcontroller.ReplicationControllerList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicationcontroller/{namespace}/{replicationController}").
	//		To(apiHandler.handleGetReplicationControllerDetail).
	//		Writes(replicationcontroller.ReplicationControllerDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.POST("/replicationcontroller/{namespace}/{replicationController}/update/pod").
	//		To(apiHandler.handleUpdateReplicasCount).
	//		Reads(replicationcontroller.ReplicationControllerSpec{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicationcontroller/{namespace}/{replicationController}/pod").
	//		To(apiHandler.handleGetReplicationControllerPods).
	//		Writes(pod.PodList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicationcontroller/{namespace}/{replicationController}/event").
	//		To(apiHandler.handleGetReplicationControllerEvents).
	//		Writes(common.EventList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicationcontroller/{namespace}/{replicationController}/service").
	//		To(apiHandler.handleGetReplicationControllerServices).
	//		Writes(resourceService.ServiceList{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/workload").
	//		To(apiHandler.handleGetWorkloads).
	//		Writes(workload.Workloads{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/workload/{namespace}").
	//		To(apiHandler.handleGetWorkloads).
	//		Writes(workload.Workloads{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/cluster").
	//		To(apiHandler.handleGetCluster).
	//		Writes(cluster.Cluster{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/discovery").
	//		To(apiHandler.handleGetDiscovery).
	//		Writes(discovery.Discovery{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/discovery/{namespace}").
	//		To(apiHandler.handleGetDiscovery).
	//		Writes(discovery.Discovery{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/config").
	//		To(apiHandler.handleGetConfig).
	//		Writes(config.Config{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/config/{namespace}").
	//		To(apiHandler.handleGetConfig).
	//		Writes(config.Config{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicaset").
	//		To(apiHandler.handleGetReplicaSets).
	//		Writes(replicaset.ReplicaSetList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicaset/{namespace}").
	//		To(apiHandler.handleGetReplicaSets).
	//		Writes(replicaset.ReplicaSetList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicaset/{namespace}/{replicaSet}").
	//		To(apiHandler.handleGetReplicaSetDetail).
	//		Writes(replicaset.ReplicaSetDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicaset/{namespace}/{replicaSet}/pod").
	//		To(apiHandler.handleGetReplicaSetPods).
	//		Writes(pod.PodList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/replicaset/{namespace}/{replicaSet}/event").
	//		To(apiHandler.handleGetReplicaSetEvents).
	//		Writes(common.EventList{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/pod").
	//		To(apiHandler.handleGetPods).
	//		Writes(pod.PodList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/pod/{namespace}").
	//		To(apiHandler.handleGetPods).
	//		Writes(pod.PodList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/pod/{namespace}/{pod}").
	//		To(apiHandler.handleGetPodDetail).
	//		Writes(pod.PodDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/pod/{namespace}/{pod}/container").
			To(apiHandler.handleGetPodContainers).
			Writes(pod.PodDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/pod/{namespace}/{pod}/event").
	//		To(apiHandler.handleGetPodEvents).
	//		Writes(common.EventList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/pod/{namespace}/{pod}/shell/{container}").
			To(apiHandler.handleExecShell).
			Writes(TerminalResponse{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/pod/{namespace}/{pod}/persistentvolumeclaim").
	//		To(apiHandler.handleGetPodPersistentVolumeClaims).
	//		Writes(persistentvolumeclaim.PersistentVolumeClaimList{}))
	//

	// region Deployment
	apiV1Ws.Route(
		apiV1Ws.GET("/deployment").
			To(apiHandler.handleGetDeployments).
			Writes(deployment.DeploymentList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployment/{namespace}").
			To(apiHandler.handleGetDeployments).
			Writes(deployment.DeploymentList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployment/{namespace}/{deployment}").
			To(apiHandler.handleGetDeploymentDetail).
			Writes(deployment.DeploymentDetail{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/deployment/{namespace}/{deployment}").
			To(apiHandler.handleUpdateDeploymentDetail).
			Writes(appsv1.Deployment{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/deployment/{namespace}/{deployment}/replicas").
			To(apiHandler.handleUpdateDeploymentReplicas).
			Writes(deployment.DeploymentReplica{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/deployment/{namespace}/{deployment}/container/{container}/").
			To(apiHandler.handlePutDeploymentContainer).
			Writes(appsv1.Deployment{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/deployment/{namespace}/{deployment}/container/{container}/image").
			To(apiHandler.handleUpdateDeploymentContainerImage).
			Writes(appsv1.Deployment{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/deployment/{namespace}/{deployment}/container/{container}/env").
			To(apiHandler.handleUpdateDeploymentContainerEnv).
			Writes(appsv1.Deployment{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/deployment/{namespace}/{deployment}/container/{container}/volumeMount/").
			To(apiHandler.handleCreateDeploymentVolumeMount).
			Writes(appsv1.Deployment{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployment/{namespace}/{deployment}/event").
			To(apiHandler.handleGetDeploymentEvents).
			Writes(common.EventList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployment/{namespace}/{deployment}/pods").
			To(apiHandler.handleGetDeploymentPods).
			Writes(pod.PodList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/deployment/{namespace}/{deployment}/oldreplicaset").
	//		To(apiHandler.handleGetDeploymentOldReplicaSets).
	//		Writes(replicaset.ReplicaSetList{}))

	// endregion

	// region Scale
	//
	//apiV1Ws.Route(
	//	apiV1Ws.PUT("/scale/{kind}/{namespace}/{name}/").
	//		To(apiHandler.handleScaleResource).
	//		Writes(scaling.ReplicaCounts{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/scale/{kind}/{namespace}/{name}").
	//		To(apiHandler.handleGetReplicaCount).
	//		Writes(scaling.ReplicaCounts{}))
	// endregion

	// region Deamonset

	//apiV1Ws.Route(
	//	apiV1Ws.GET("/daemonset").
	//		To(apiHandler.handleGetDaemonSetList).
	//		Writes(daemonset.DaemonSetList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/daemonset/{namespace}").
	//		To(apiHandler.handleGetDaemonSetList).
	//		Writes(daemonset.DaemonSetList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/daemonset/{namespace}/{daemonset}").
			To(apiHandler.handleGetDaemonSetDetail).
			Writes(appsv1.DaemonSet{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/daemonset/{namespace}/{daemonset}").
			To(apiHandler.handleUpdateDaemonSetDetail).
			Writes(appsv1.DaemonSet{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/daemonset/{namespace}/{daemonset}/pods").
			To(apiHandler.handleGetDaemonSetPods).
			Writes(pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/daemonset/{namespace}/{daemonset}/container/{container}/").
			To(apiHandler.handlePutDaemonSetContainer).
			Writes(appsv1.DaemonSet{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/daemonset/{namespace}/{daemonset}/container/{container}/image").
			To(apiHandler.handleUpdateDaemonSetContainerImage).
			Writes(appsv1.DaemonSet{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/daemonset/{namespace}/{daemonset}/container/{container}/env").
			To(apiHandler.handleUpdateDaemonSetContainerEnv).
			Writes(appsv1.DaemonSet{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/daemonset/{namespace}/{daemonset}/container/{container}/volumeMount/").
			To(apiHandler.handleCreateDaemonSetVolumeMount).
			Writes(appsv1.DaemonSet{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/daemonset/{namespace}/{daemonSet}/service").
	//		To(apiHandler.handleGetDaemonSetServices).
	//		Writes(resourceService.ServiceList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/daemonset/{namespace}/{daemonSet}/event").
	//		To(apiHandler.handleGetDaemonSetEvents).
	//		Writes(common.EventList{}))

	// endregion

	//apiV1Ws.Route(
	//	apiV1Ws.GET("/horizontalpodautoscaler").
	//		To(apiHandler.handleGetHorizontalPodAutoscalerList).
	//		Writes(horizontalpodautoscaler.HorizontalPodAutoscalerList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/horizontalpodautoscaler/{namespace}").
	//		To(apiHandler.handleGetHorizontalPodAutoscalerList).
	//		Writes(horizontalpodautoscaler.HorizontalPodAutoscalerList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/horizontalpodautoscaler/{namespace}/{horizontalpodautoscaler}").
	//		To(apiHandler.handleGetHorizontalPodAutoscalerDetail).
	//		Writes(horizontalpodautoscaler.HorizontalPodAutoscalerDetail{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/job").
	//		To(apiHandler.handleGetJobList).
	//		Writes(job.JobList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/job/{namespace}").
	//		To(apiHandler.handleGetJobList).
	//		Writes(job.JobList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/job/{namespace}/{name}").
	//		To(apiHandler.handleGetJobDetail).
	//		Writes(job.JobDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/job/{namespace}/{name}/pod").
	//		To(apiHandler.handleGetJobPods).
	//		Writes(pod.PodList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/job/{namespace}/{name}/event").
	//		To(apiHandler.handleGetJobEvents).
	//		Writes(common.EventList{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/cronjob").
	//		To(apiHandler.handleGetCronJobList).
	//		Writes(cronjob.CronJobList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/cronjob/{namespace}").
	//		To(apiHandler.handleGetCronJobList).
	//		Writes(cronjob.CronJobList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/cronjob/{namespace}/{name}").
	//		To(apiHandler.handleGetCronJobDetail).
	//		Writes(cronjob.CronJobDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/cronjob/{namespace}/{name}/job").
	//		To(apiHandler.handleGetCronJobJobs).
	//		Writes(job.JobList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/cronjob/{namespace}/{name}/event").
	//		To(apiHandler.handleGetCronJobEvents).
	//		Writes(common.EventList{}))
	//

	// region Namespace

	//apiV1Ws.Route(
	//	apiV1Ws.POST("/namespace").
	//		To(apiHandler.handleCreateNamespace).
	//		Reads(ns.NamespaceSpec{}).
	//		Writes(ns.NamespaceSpec{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/namespaces").
			To(apiHandler.handleGetNamespaces).
			Writes(ns.NamespaceList{}).
			Doc("get namespaces list").
			Returns(200, "OK", ns.NamespaceList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/namespace/{name}").
	//		To(apiHandler.handleGetNamespaceDetail).
	//		Writes(ns.NamespaceDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/namespace/{name}/event").
	//		To(apiHandler.handleGetNamespaceEvents).
	//		Writes(common.EventList{}))
	//
	// endregion

	// region Secret
	apiV1Ws.Route(
		apiV1Ws.GET("/secret").
			To(apiHandler.handleGetSecretList).
			Writes(secret.SecretList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/secret/{namespace}").
			To(apiHandler.handleGetSecretList).
			Writes(secret.SecretList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/secret/{namespace}/{name}").
			To(apiHandler.handleGetSecretDetail).
			Writes(secret.SecretDetail{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/secret/{namespace}/{name}").
			To(apiHandler.handleUpdateSecret).
			Writes(secret.SecretDetail{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/secret").
			To(apiHandler.handleCreateImagePullSecret).
			Reads(secret.ImagePullSecretSpec{}).
			Writes(secret.Secret{}))
	// endregion

	//
	apiV1Ws.Route(
		apiV1Ws.GET("/configmap").
			To(apiHandler.handleGetConfigMapList).
			Writes(configmap.ConfigMapList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/configmap/{namespace}").
			To(apiHandler.handleGetConfigMapList).
			Writes(configmap.ConfigMapList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/configmap/{namespace}/{configmap}").
			To(apiHandler.handleGetConfigMapDetail).
			Writes(configmap.ConfigMapDetail{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/service").
	//		To(apiHandler.handleGetServiceList).
	//		Writes(resourceService.ServiceList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/service/{namespace}").
	//		To(apiHandler.handleGetServiceList).
	//		Writes(resourceService.ServiceList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/service/{namespace}/{service}").
	//		To(apiHandler.handleGetServiceDetail).
	//		Writes(resourceService.ServiceDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/service/{namespace}/{service}/pod").
	//		To(apiHandler.handleGetServicePods).
	//		Writes(pod.PodList{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/ingress").
	//		To(apiHandler.handleGetIngressList).
	//		Writes(ingress.IngressList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/ingress/{namespace}").
	//		To(apiHandler.handleGetIngressList).
	//		Writes(ingress.IngressList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/ingress/{namespace}/{name}").
	//		To(apiHandler.handleGetIngressDetail).
	//		Writes(ingress.IngressDetail{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/statefulset").
	//		To(apiHandler.handleGetStatefulSetList).
	//		Writes(statefulset.StatefulSetList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/statefulset/{namespace}").
	//		To(apiHandler.handleGetStatefulSetList).
	//		Writes(statefulset.StatefulSetList{}))

	// region Statefulset
	apiV1Ws.Route(
		apiV1Ws.GET("/statefulset/{namespace}/{statefulset}").
			To(apiHandler.handleGetStatefulSetDetail).
			Writes(appsv1.StatefulSet{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/statefulset/{namespace}/{statefulset}").
			To(apiHandler.handleUpdateStatefulSetDetail).
			Writes(appsv1.StatefulSet{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/statefulset/{namespace}/{statefulset}/replicas").
			To(apiHandler.handleUpdateStatefulSetReplicas).
			Writes(appsv1.StatefulSet{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/statefulset/{namespace}/{statefulset}/pods").
			To(apiHandler.handleGetStatefulSetPods).
			Writes(pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/statefulset/{namespace}/{statefulset}/container/{container}/").
			To(apiHandler.handlePutStatefulSetContainer).
			Writes(appsv1.StatefulSet{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/statefulset/{namespace}/{statefulset}/container/{container}/image").
			To(apiHandler.handleUpdateStatefulSetContainerImage).
			Writes(appsv1.StatefulSet{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/statefulset/{namespace}/{statefulset}/container/{container}/env").
			To(apiHandler.handleUpdateStatefulSetContainerEnv).
			Writes(appsv1.StatefulSet{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/statefulset/{namespace}/{statefulset}/container/{container}/volumeMount/").
			To(apiHandler.handleCreateStatefulSetVolumeMount).
			Writes(appsv1.StatefulSet{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/statefulset/{namespace}/{statefulset}/event").
	//		To(apiHandler.handleGetStatefulSetEvents).
	//		Writes(common.EventList{}))

	// endregion

	//apiV1Ws.Route(
	//	apiV1Ws.GET("/node").
	//		To(apiHandler.handleGetNodeList).
	//		Writes(node.NodeList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/node/{name}").
	//		To(apiHandler.handleGetNodeDetail).
	//		Writes(node.NodeDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/node/{name}/event").
	//		To(apiHandler.handleGetNodeEvents).
	//		Writes(common.EventList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/node/{name}/pod").
	//		To(apiHandler.handleGetNodePods).
	//		Writes(pod.PodList{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.DELETE("/_raw/{kind}/namespace/{namespace}/name/{name}").
	//		To(apiHandler.handleDeleteResource))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/_raw/{kind}/namespace/{namespace}/name/{name}").
	//		To(apiHandler.handleGetResource))
	//apiV1Ws.Route(
	//	apiV1Ws.PUT("/_raw/{kind}/namespace/{namespace}/name/{name}").
	//		To(apiHandler.handlePutResource))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.DELETE("/_raw/{kind}/name/{name}").
	//		To(apiHandler.handleDeleteResource))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/_raw/{kind}/name/{name}").
	//		To(apiHandler.handleGetResource))
	//apiV1Ws.Route(
	//	apiV1Ws.PUT("/_raw/{kind}/name/{name}").
	//		To(apiHandler.handlePutResource))
	//

	// region RBAC
	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/role").
			To(apiHandler.handleGetRbacRoleList).
			Writes(rbacroles.RbacRoleList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/rolebinding").
			To(apiHandler.handleGetRbacRoleBindingList).
			Writes(rbacrolebindings.RbacRoleBindingList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/rbac/status").
	//		To(apiHandler.handleRbacStatus).
	//		Writes(validation.RbacStatus{}))

	// endregion

	//apiV1Ws.Route(
	//	apiV1Ws.GET("/persistentvolume").
	//		To(apiHandler.handleGetPersistentVolumeList).
	//		Writes(persistentvolume.PersistentVolumeList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/persistentvolume/{persistentvolume}").
	//		To(apiHandler.handleGetPersistentVolumeDetail).
	//		Writes(persistentvolume.PersistentVolumeDetail{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/persistentvolume/namespace/{namespace}/name/{persistentvolume}").
	//		To(apiHandler.handleGetPersistentVolumeDetail).
	//		Writes(persistentvolume.PersistentVolumeDetail{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/persistentvolumeclaim/").
	//		To(apiHandler.handleGetPersistentVolumeClaimList).
	//		Writes(persistentvolumeclaim.PersistentVolumeClaimList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/persistentvolumeclaim/{namespace}").
	//		To(apiHandler.handleGetPersistentVolumeClaimList).
	//		Writes(persistentvolumeclaim.PersistentVolumeClaimList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/persistentvolumeclaim/{namespace}/{name}").
	//		To(apiHandler.handleGetPersistentVolumeClaimDetail).
	//		Writes(persistentvolumeclaim.PersistentVolumeClaimDetail{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/storageclass").
	//		To(apiHandler.handleGetStorageClassList).
	//		Writes(storageclass.StorageClassList{}))
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/storageclass/{storageclass}").
	//		To(apiHandler.handleGetStorageClass).
	//		Writes(storageclass.StorageClass{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/storageclass/{storageclass}/persistentvolume").
	//		To(apiHandler.handleGetStorageClassPersistentVolumes).
	//		Writes(persistentvolume.PersistentVolumeList{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/log/source/{namespace}/{resourceName}/{resourceType}").
	//		To(apiHandler.handleLogSource).
	//		Writes(controller.LogSources{}))

	// region log
	apiV1Ws.Route(
		apiV1Ws.GET("/log/{namespace}/{pod}").
			To(apiHandler.handleLogs).
			Writes(logs.LogDetails{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/log/{namespace}/{pod}/{container}").
			To(apiHandler.handleLogs).
			Writes(logs.LogDetails{}))
	//
	apiV1Ws.Route(
		apiV1Ws.GET("/log/file/{namespace}/{pod}/{container}").
			To(apiHandler.handleLogFile).
			Writes(logs.LogDetails{}))
	// endregion

	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/overview/").
	//		To(apiHandler.handleOverview).
	//		Writes(overview.Overview{}))
	//
	//apiV1Ws.Route(
	//	apiV1Ws.GET("/overview/{namespace}").
	//		To(apiHandler.handleOverview).
	//		Writes(overview.Overview{}))
	//

	// region others
	apiV1Ws.Route(
		apiV1Ws.GET("/others").
			To(apiHandler.handleOtherResourcesList).
			Writes(other.ResourceList{}).
			Doc("get all resources").
			Param(restful.QueryParameter("filterBy", "filter option separated by comma. For example parameter1,value1,parameter2,value2 - means that the data should be filtered by parameter1 equals value1 and parameter2 equals value2").
				DataType("string").
				AllowableValues(map[string]string{
					"name":      "search by name partial match",
					"namespace": "filter by namespace",
					"kind":      "filter by kind",
					"scope":     "allowed value `namespaced` and `clustered` filter by if a resource is namespaced",
				})).
			Param(restful.QueryParameter("sortBy", "sort option separated by comma. For example a,parameter1,d,parameter2 - means that the data should be sorted by parameter1 (ascending) and later sort by parameter2 (descending)").
				DataType("string").
				AllowableValues(map[string]string{
					"name":              "",
					"namespace":         "",
					"kind":              "",
					"creationTimestamp": "",
				})).
			Param(restful.QueryParameter("itemsPerPage", "items per page").
				DataType("integer")).
			Param(restful.QueryParameter("page", "page number").DataType("integer")).
			Returns(200, "OK", other.ResourceList{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/others").
			To(apiHandler.handleOtherResourceCreate).
			Doc("create a resource").
			Reads([]unstructured.Unstructured{}).
			Consumes(restful.MIME_JSON).
			Returns(200, "OK", CreateResponse{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/releases").
			To(apiHandler.handleReleaseCreate).
			Doc("create a release").
			Reads([]unstructured.Unstructured{}).
			Consumes(restful.MIME_JSON).
			Returns(200, "OK", []unstructured.Unstructured{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/releases/{namespace}/{name}").
			To(apiHandler.handleGetReleaseDetail).
			Doc("get a release").
			Reads(release.ReleaseDetails{}).
			Consumes(restful.MIME_JSON).
			Returns(200, "OK", release.ReleaseDetails{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/others/{group}/{version}/{kind}/{namespace}/{name}").
			To(apiHandler.handleOtherResourceDetail).
			Writes(other.OtherResourceDetail{}).
			Doc("get a resource detail with events").
			Returns(200, "OK", other.OtherResourceDetail{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/others/{group}/{version}/{kind}/{namespace}/{name}").
			To(apiHandler.handleOtherResourceDetail).
			Doc("delete a resource"))

	apiV1Ws.Route(
		apiV1Ws.PUT("/others/{group}/{version}/{kind}/{namespace}/{name}").
			To(apiHandler.handleOtherResourceDetail).
			Doc("update a resource with whole resource json").
			Reads(unstructured.Unstructured{}).
			Consumes(restful.MIME_JSON))

	apiV1Ws.Route(
		apiV1Ws.PATCH("/others/{group}/{version}/{kind}/{namespace}/{name}/{field}").
			To(apiHandler.handleOtherResourcePatch).
			Doc("update resource annotations or labels").
			Reads(other.FieldPayload{}).
			Consumes(restful.MIME_JSON))
	// endregion

	// ---- DEVOPS APIS ----

	// region Projects
	apiV1Ws.Route(
		apiV1Ws.GET("/projects").
			To(apiHandler.handleGetProjects).
			Writes(project.ProjectList{}).
			Doc("get project list").
			Returns(200, "OK", project.ProjectList{}))
	// endregion

	// region Jenkins
	apiV1Ws.Route(
		apiV1Ws.GET("/jenkinses").
			To(apiHandler.handleGetJenkins).
			Writes(jenkins.JenkinsList{}).
			Doc("get jenkins list").
			Returns(200, "OK", jenkins.JenkinsList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/jenkinses/{name}").
			To(apiHandler.handleRetriveJenkins).
			Writes(v1alpha1.Jenkins{}).
			Doc("retrieve jenkins config").
			Returns(200, "OK", v1alpha1.Jenkins{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE("/jenkinses/{name}").
			To(apiHandler.handleDeleteJenkins).
			Writes(jenkins.Jenkins{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/jenkinses/{name}").
			To(apiHandler.handlePutJenkins).
			Writes(v1alpha1.Jenkins{}).
			Doc("update jenkins config").
			Returns(200, "OK", v1alpha1.Jenkins{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/jenkinses").
			To(apiHandler.handleCreateJenkins).
			Writes(v1alpha1.Jenkins{}).
			Doc("update jenkins config").
			Returns(200, "OK", v1alpha1.Jenkins{}))
	// endregion

	// region JenkinsBinding
	apiV1Ws.Route(
		apiV1Ws.GET("/jenkinsbinding").
			To(apiHandler.handleGetJenkinsBindingList).
			Writes(jenkinsbinding.JenkinsBindingList{}).
			Doc("get jenkinsbinding list").
			Returns(200, "OK", jenkinsbinding.JenkinsBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/jenkinsbinding/{namespace}").
			To(apiHandler.handleGetJenkinsBindingList).
			Writes(jenkinsbinding.JenkinsBindingList{}).
			Doc("get namespaced jenkinsbinding list").
			Returns(200, "OK", jenkinsbinding.JenkinsBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE("/jenkinsbinding/{namespace}/{name}").
			To(apiHandler.handleDeleteJenkinsBinding).
			Writes(v1alpha1.JenkinsBinding{}))
	// endregion

	// region Applications
	apiV1Ws.Route(
		apiV1Ws.GET("/applications/{namespace}").
			To(apiHandler.handleGetApplicationsList).
			Writes(application.ApplicationList{}).
			Doc("get namespaced application list"))
	apiV1Ws.Route(
		apiV1Ws.GET("/applications/{namespace}/{name}").
			To(apiHandler.handleGetApplicationDetail).
			Writes(application.ApplicationDetail{}).
			Doc("get application details").
			Returns(200, "OK", application.ApplicationDetail{}))

	// this is not just a delete, we will post a body, the resource what in the body will just remove the label
	// then the resource will not belong the any app any more
	apiV1Ws.Route(
		apiV1Ws.POST("/applications/{namespace}/{name}/actions/delete").
			To(apiHandler.handleDeleteApplication).
			Writes(application.DeleteAppYAMLSpec{}).
			Doc("deletes application with all related resources").
			Returns(200, "OK", application.UpdateAppYAMLSpec{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/applications/{namespace}/{name}/yaml").
			To(apiHandler.handleGetApplicationYAML).
			Writes(application.ApplicationDetail{}).
			Doc("get application details").
			Returns(200, "OK", application.ApplicationYAML{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/applications/{namespace}/{name}/yaml").
			To(apiHandler.handleUpdateApplicationYAML).
			Writes(application.UpdateAppYAMLSpec{}).
			Doc("update application details").
			Returns(200, "OK", application.UpdateAppYAMLSpec{}))
	// endregion

	// region PipelineTemplate
	// PipelineTemplateSync
	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplatesync/{namespace}").
			To(apiHandler.handleGetPipelineTemplateSyncList).
			Writes(pipelinetemplatesync.PipelineTemplateSyncList{}).
			Doc("get pipelineTemplateSync list").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSyncList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplatesync/{namespace}/{name}").
			To(apiHandler.handleGetPipelineTemplateSync).
			Writes(pipelinetemplatesync.PipelineTemplateSync{}).
			Doc("get detail of specific PipelineTemplateSync").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSync{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/pipelinetemplatesync/{namespace}").
			To(apiHandler.handleCreatePipelineTemplateSync).
			Writes(pipelinetemplatesync.PipelineTemplateSync{}).
			Doc("create a pipelineTemplateSync").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSync{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/pipelinetemplatesync/{namespace}/{name}").
			To(apiHandler.handleUpdatePipelineTemplateSync).
			Writes(pipelinetemplatesync.PipelineTemplateSync{}).
			Doc("update a pipelineTemplateSync").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSync{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/pipelinetemplatesync/{namespace}/{name}").
			To(apiHandler.handleDeletePipelineTemplateSync).
			Writes(struct{}{}).
			Doc("delete a PipelineTemplateSync").
			Returns(200, "OK", struct{}{}))

	// PipelineTaskTemplate
	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetasktemplate/{namespace}").
			To(apiHandler.handleGetPipelineTaskTemplateList).
			Writes(pipelinetasktemplate.PipelineTaskTemplateList{}).
			Doc("get a list of PipelineTaskTemplate").
			Returns(200, "OK", pipelinetasktemplate.PipelineTaskTemplate{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetasktemplate/{namespace}/{name}").
			To(apiHandler.handleGetPipelineTaskTemplate).
			Writes(pipelinetasktemplate.PipelineTaskTemplate{}).
			Doc("get a PipelineTaskTemplate").
			Returns(200, "OK", pipelinetasktemplate.PipelineTaskTemplate{}))

	// PipelineTemplate
	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplate/{namespace}").
			To(apiHandler.handleGetPipelineTemplateList).
			Writes(pipelinetemplate.PipelineTemplateList{}).
			Doc("get a list of PipelineTemplate").
			Returns(200, "OK", pipelinetemplate.PipelineTemplateList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplate/{namespace}/{name}").
			To(apiHandler.handleGetPipelineTemplate).
			Writes(pipelinetemplate.PipelineTemplate{}).
			Doc("get a PipelineTemplate").
			Returns(200, "OK", pipelinetemplate.PipelineTemplate{}))

	// ClusterPipelineTemplate
	apiV1Ws.Route(
		apiV1Ws.GET("/clusterpipelinetemplate").
			To(apiHandler.handleGetClusterPipelineTemplateList).
			Writes(clusterpipelinetemplate.ClusterPipelineTemplateList{}).
			Doc("get a list of ClusterPipelineTemplate").
			Returns(200, "OK", clusterpipelinetemplate.ClusterPipelineTemplateList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/clusterpipelinetemplate/{name}").
			To(apiHandler.handleGetClusterPipelineTemplate).
			Writes(clusterpipelinetemplate.ClusterPipelineTemplate{}).
			Doc("get a ClusterPipelineTemplate").
			Returns(200, "OK", clusterpipelinetemplate.ClusterPipelineTemplate{}))

	// PipelineTemplateSync
	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplatesync/{namespace}").
			To(apiHandler.handleGetPipelineTemplateSyncList).
			Writes(pipelinetemplatesync.PipelineTemplateSyncList{}).
			Doc("get pipelineTemplateSync list").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSyncList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplatesync/{namespace}/{name}").
			To(apiHandler.handleGetPipelineTemplateSync).
			Writes(pipelinetemplatesync.PipelineTemplateSync{}).
			Doc("get detail of specific PipelineTemplateSync").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSync{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/pipelinetemplatesync/{namespace}").
			To(apiHandler.handleCreatePipelineTemplateSync).
			Writes(pipelinetemplatesync.PipelineTemplateSync{}).
			Doc("create a pipelineTemplateSync").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSync{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/pipelinetemplatesync/{namespace}/{name}").
			To(apiHandler.handleUpdatePipelineTemplateSync).
			Writes(pipelinetemplatesync.PipelineTemplateSync{}).
			Doc("update a pipelineTemplateSync").
			Returns(200, "OK", pipelinetemplatesync.PipelineTemplateSync{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/pipelinetemplatesync/{namespace}/{name}").
			To(apiHandler.handleDeletePipelineTemplateSync).
			Writes(struct{}{}).
			Doc("delete a PipelineTemplateSync").
			Returns(200, "OK", struct{}{}))

	// PipelineTaskTemplate
	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetasktemplate/{namespace}").
			To(apiHandler.handleGetPipelineTaskTemplateList).
			Writes(pipelinetasktemplate.PipelineTaskTemplateList{}).
			Doc("get a list of PipelineTaskTemplate").
			Returns(200, "OK", pipelinetasktemplate.PipelineTaskTemplate{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetasktemplate/{namespace}/{name}").
			To(apiHandler.handleGetPipelineTaskTemplate).
			Writes(pipelinetasktemplate.PipelineTaskTemplate{}).
			Doc("get a PipelineTaskTemplate").
			Returns(200, "OK", pipelinetasktemplate.PipelineTaskTemplate{}))

	// PipelineTemplate
	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplate/{namespace}").
			To(apiHandler.handleGetPipelineTemplateList).
			Writes(pipelinetemplate.PipelineTemplateList{}).
			Doc("get a list of PipelineTemplate").
			Returns(200, "OK", pipelinetemplate.PipelineTemplateList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipelinetemplate/{namespace}/{name}").
			To(apiHandler.handleGetPipelineTemplate).
			Writes(pipelinetemplate.PipelineTemplate{}).
			Doc("get a PipelineTemplate").
			Returns(200, "OK", pipelinetemplate.PipelineTemplate{}))

	apiV1Ws.Route(
		apiV1Ws.POST("pipelinetemplate/{namespace}/{name}/preview").
			To(apiHandler.handlePreviewPipelineTemplate).
			Doc("jenkinsfile preview from PipelineTemplate"))

	// endregion

	// region Pipeline
	apiV1Ws.Route(
		apiV1Ws.GET("/pipelineconfig/{namespace}").
			To(apiHandler.handleGetPipelineConfigList).
			Writes(pipelineconfig.PipelineConfigList{}).
			Doc("get namespaced pipelineconfig list").
			Returns(200, "OK", pipelineconfig.PipelineConfigList{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/pipelineconfig/{namespace}").
			To(apiHandler.handleCreatePipelineConfig).
			Writes(pipelineconfig.PipelineConfigDetail{}).
			Doc("creates namespaced pipelineconfig").
			Returns(200, "OK", pipelineconfig.PipelineConfigDetail{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipelineconfig/{namespace}/{name}").
			To(apiHandler.handleGetPipelineConfigDetail).
			Writes(pipelineconfig.PipelineConfig{}).
			Doc("get pipeline config details").
			Returns(200, "OK", pipelineconfig.PipelineConfigDetail{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/pipelineconfig/{namespace}/{name}").
			To(apiHandler.handleUpdatePipelineConfig).
			Writes(pipelineconfig.PipelineConfig{}).
			Doc("update pipeline config").
			Returns(200, "OK", pipelineconfig.PipelineConfigDetail{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/pipelineconfig/{namespace}/{name}").
			To(apiHandler.handleDeletePipelineConfig).
			Writes(struct{}{}).
			Doc("deletes a pipeline config").
			Returns(200, "OK", struct{}{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/pipelineconfig/{namespace}/{name}/trigger").
			To(apiHandler.handleTriggerPipelineConfig).
			Writes(pipelineconfig.PipelineConfigTrigger{}).
			Doc("triggers pipeline").
			Returns(200, "OK", pipelineconfig.PipelineTriggerResponse{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/pipelineconfig/{namespace}/{name}/preview").
			To(apiHandler.handlePreviewPipelineConfig).
			Doc("jenkinsfile preview"))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipeline/{namespace}").
			To(apiHandler.handleGetPipelineList).
			Writes(pipeline.PipelineList{}).
			Doc("get namespaced pipeline list").
			Returns(200, "OK", pipeline.PipelineList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipeline/{namespace}/{name}").
			To(apiHandler.handleGetPipelineDetail).
			Writes(pipeline.Pipeline{}).
			Doc("get pipeline details").
			Returns(200, "OK", pipeline.Pipeline{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/pipeline/{namespace}/{name}").
			To(apiHandler.handleDeletePipeline).
			Writes(struct{}{}).
			Doc("deletes a pipeline").
			Returns(200, "OK", struct{}{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/pipeline/{namespace}/{name}/retry").
			To(apiHandler.handleRetryPipelineDetail).
			Writes(pipeline.RetryRequest{}).
			Doc("retries a pipeline").
			Returns(200, "OK", v1alpha1.Pipeline{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/pipeline/{namespace}/{name}/abort").
			To(apiHandler.handleAbortPipeline).
			Writes(pipeline.AbortRequest{}).
			Doc("aborts a pipeline").
			Returns(200, "OK", v1alpha1.Pipeline{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/pipeline/{namespace}/{name}/logs").
			Param(restful.PathParameter("namespace", "Namespace to use")).
			Param(restful.PathParameter("name", "Pipeline name to filter scope")).
			Param(restful.QueryParameter("start", "Start offset to fetch logs")).
			Param(restful.QueryParameter("stage", "Stage to fetch logs from")).
			Param(restful.QueryParameter("step", "Step to fetch logs from. Can be combined with stage")).
			To(apiHandler.handlePipelineLogs).
			Doc("gets logs for pipeline").
			Returns(200, "OK", v1alpha1.PipelineLog{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pipeline/{namespace}/{name}/tasks").
			Param(restful.PathParameter("namespace", "Namespace to use")).
			Param(restful.PathParameter("name", "Pipeline name to filter scope")).
			Param(restful.QueryParameter("stage", "Stage to fetch steps from. If not provided will return all stages")).
			To(apiHandler.handlePipelineTasks).
			Doc("gets steps for pipeline").
			Returns(200, "OK", v1alpha1.PipelineTask{}))

	// endregion

	// region CodeRepository
	apiV1Ws.Route(
		apiV1Ws.POST("/codereposervice").
			To(apiHandler.handleCreateCodeRepoService).
			Writes(codereposervice.CodeRepoServiceList{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE("/codereposervice/{name}").
			To(apiHandler.handleDeleteCodeRepoService).
			Writes(codereposervice.CodeRepoService{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/codereposervice/{name}").
			To(apiHandler.handleUpdateCodeRepoService).
			Writes(v1alpha1.CodeRepoService{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/codereposervice").
			To(apiHandler.handleGetCodeRepoServiceList).
			Writes(codereposervice.CodeRepoServiceList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/codereposervices").
			To(apiHandler.handleGetCodeRepoServiceList).
			Writes(codereposervice.CodeRepoServiceList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/codereposervice/{name}").
			To(apiHandler.handleGetCodeRepoServiceDetail).
			Writes(v1alpha1.CodeRepoService{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/codereposervice/{name}/resources").
			To(apiHandler.handleGetCodeRepoServiceResourceList).
			Writes(coderepository.ResourceList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/codereposervice/{name}/secrets").
			To(apiHandler.handleGetCodeRepoServiceSecretList).
			Writes(secret.SecretList{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/coderepobinding/{namespace}").
			To(apiHandler.handleCreateCodeRepoBinding).
			Writes(v1alpha1.CodeRepoBinding{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE("/coderepobinding/{namespace}/{name}").
			To(apiHandler.handleDeleteCodeRepoBinding).
			Writes(v1alpha1.CodeRepoBinding{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/coderepobinding/{namespace}/{name}").
			To(apiHandler.handleUpdateCodeRepoBinding).
			Writes(struct{}{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepobinding").
			To(apiHandler.handleGetCodeRepoBindingList).
			Writes(coderepobinding.CodeRepoBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepobinding/{namespace}").
			To(apiHandler.handleGetCodeRepoBindingList).
			Writes(coderepobinding.CodeRepoBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepobinding/{namespace}/{name}").
			To(apiHandler.handleGetCodeRepoBindingDetail).
			Writes(v1alpha1.CodeRepoBinding{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepobinding/{namespace}/{name}/resources").
			To(apiHandler.handleGetCodeRepoBindingResourceList).
			Writes(coderepository.ResourceList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepobinding/{namespace}/{name}/secrets").
			To(apiHandler.handleGetCodeRepoBindingSecretList).
			Writes(secret.SecretList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepobinding/{namespace}/{name}/repositories").
			To(apiHandler.handleGetCodeRepositoryListInBinding).
			Writes(coderepository.CodeRepositoryList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepobinding/{namespace}/{name}/remote-repositories").
			To(apiHandler.handleGetRemoteRepositoryList).
			Writes(coderepository.CodeRepositoryList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/coderepository/{namespace}").
			To(apiHandler.handleGetCodeRepositoryList).
			Writes(coderepository.CodeRepositoryList{}).
			Doc("get namespaced coderepository list").
			Returns(200, "OK", coderepository.CodeRepositoryList{}))
	// endregion

	// region ToolChain
	apiV1Ws.Route(
		apiV1Ws.GET("/toolchain").
			To(apiHandler.handleGetToolChains).
			Writes(toolchain.ToolChainList{}).
			Doc("get namespaced coderepository list").
			Returns(200, "OK", coderepository.CodeRepositoryList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/toolchain/bindings").
			To(apiHandler.handleGetToolChainBindings).
			Writes(toolchain.ToolChainBindingList{}).
			Doc("get toolchain binding list").
			Returns(200, "OK", coderepository.CodeRepositoryList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/toolchain/bindings/{namespace}").
			To(apiHandler.handleGetToolChainBindings).
			Writes(toolchain.ToolChainBindingList{}).
			Doc("get namespaced toolchain binding list").
			Returns(200, "OK", coderepository.CodeRepositoryList{}))
	// endregion

	// region callback
	apiV1Ws.Route(
		apiV1Ws.GET("/callback/oauth/{namespace}/secret/{secretName}/codereposervice/{serviceName}").
			To(apiHandler.handleOAuthCallback).
			Writes(struct{}{}))
	// endregion

	// region imageregistry
	apiV1Ws.Route(
		apiV1Ws.POST("/imageregistry").
			To(apiHandler.handleCreateImageRegistry).
			Writes(imageregistry.ImageRegistryList{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE("/imageregistry/{name}").
			To(apiHandler.handleDeleteImageRegsitry).
			Writes(imageregistry.ImageRegistry{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/imageregistry/{name}").
			To(apiHandler.handleUpdateImageRegistry).
			Writes(v1alpha1.ImageRegistry{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistry").
			To(apiHandler.handleGetImageRegistryList).
			Writes(imageregistry.ImageRegistryList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistry/{name}").
			To(apiHandler.handleGetImageRegistryDetail).
			Writes(v1alpha1.ImageRegistry{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistry/{name}/secrets").
			To(apiHandler.handleGetImageRegistrySecretList).
			Writes(secret.SecretList{}))
	// endregion

	// region ImageRegistryBinding
	apiV1Ws.Route(
		apiV1Ws.POST("/imageregistrybinding/{namespace}").
			To(apiHandler.handleCreateImageRegistryBinding).
			Writes(v1alpha1.ImageRegistryBinding{}))
	apiV1Ws.Route(
		apiV1Ws.PUT(URLIMAGEREGISTRYBINDINGDETAIL).
			To(apiHandler.handleUpdateImageRegistryBinding).
			Writes(v1alpha1.ImageRegistryBinding{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE(URLIMAGEREGISTRYBINDINGDETAIL).
			To(apiHandler.handleDeleteImageRegistryBinding).
			Writes(v1alpha1.ImageRegistryBinding{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistrybinding").
			To(apiHandler.handleGetImageRegistryBindingList).
			Writes(imageregistrybinding.ImageRegistryBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistrybinding/{namespace}").
			To(apiHandler.handleGetImageRegistryBindingList).
			Writes(imageregistrybinding.ImageRegistryBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET(URLIMAGEREGISTRYBINDINGDETAIL).
			To(apiHandler.handleGetImageRegistryBindingDetail).
			Writes(v1alpha1.ImageRegistryBinding{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistrybinding/{namespace}/{name}/secrets").
			To(apiHandler.handleGetImageRegistryBindingSecretList).
			Writes(secret.SecretList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistrybinding/{namespace}/{name}/repositories").
			To(apiHandler.handleGetImageRepositoryListInBinding).
			Writes(imagerepository.ImageRepositoryList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imageregistrybinding/{namespace}/{name}/remote-repositories").
			To(apiHandler.handleGetImageOriginRepositoryList).
			Writes(imagerepository.ImageRepositoryList{}))
	// endregion

	// region ImageRepository
	apiV1Ws.Route(
		apiV1Ws.GET("/imagerepository/{namespace}").
			To(apiHandler.handleGetImageRepositoryList).
			Writes(imagerepository.ImageRepositoryList{}).
			Doc("get namespaced imagerepository list").
			Returns(200, "OK", imagerepository.ImageRepositoryList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imagerepository/{namespace}/{name}").
			To(apiHandler.handleGetImageRepositoryDetail).
			Writes(v1alpha1.ImageRepository{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/imagerepository/{namespace}/{name}/tags").
			To(apiHandler.handleGetImageRepositoryTagList).
			Writes(imagerepository.ImageTagList{}))
	// endregion

	// region microservicesenvironments
	apiV1Ws.Route(
		apiV1Ws.GET("/microservicesenvironments").
			To(apiHandler.handleMicroservicesEnvironmentList).
			Writes(asfv1.MicroservicesEnvironmentList{}).
			Doc("get microservicesenvironment list").
			Returns(200, "OK", asfv1.MicroservicesEnvironmentList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/microservicesenvironments/{name}").
			Writes(microservicesenvironment.MicroservicesEnvironmentDetail{}).
			To(apiHandler.handleGetMicroservicesEnviromentDetail).
			Doc("get microservicesenvironments detail by name").
			Returns(200, "OK", microservicesenvironment.MicroservicesEnvironmentDetail{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/microservicescomponent/{namespace}/{name}").
			To(apiHandler.handlePutMicroservicesComponent).
			Writes(asfv1.MicroservicesComponent{}).
			Doc("install component").
			Returns(200, "OK", asfv1.MicroservicesComponent{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/microservicescomponent/{namespace}/{name}").
			To(apiHandler.handlePutMicroservicesComponent).
			Writes(asfv1.MicroservicesComponent{}).
			Doc("update component").
			Returns(200, "OK", asfv1.MicroservicesComponent{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/microservicescomponent/{namespace}/{name}/start").
			To(apiHandler.handlePutMicroservicesComponentStart).
			Writes(asfv1.MicroservicesComponent{}).
			Doc("start component").
			Returns(200, "OK", asfv1.MicroservicesComponent{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/microservicescomponent/{namespace}/{name}/stop").
			To(apiHandler.handlePutMicroservicesComponentStop).
			Writes(asfv1.MicroservicesComponentList{}).
			Doc("stop component").
			Returns(200, "OK", asfv1.MicroservicesComponent{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/microservicesapps").
			Writes(microservicesapplication.MicroservicesApplicationList{}).
			To(apiHandler.handleGetMicroservicesApps).
			Doc("get microservicesenvironments detail by name").
			Returns(200, "OK", microservicesapplication.MicroservicesApplicationList{}))

	// endregion

	// region ProjectManagement
	apiV1Ws.Route(
		apiV1Ws.POST("/projectmanagement").
			To(apiHandler.handleCreateProjectManagement).
			Writes(v1alpha1.ProjectManagement{}).
			Doc("create a projectmanagement").
			Returns(200, "OK", v1alpha1.ProjectManagement{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE(URLProjectManagementDetails).
			To(apiHandler.handleDeleteProjectManagement).
			Writes(struct{}{}).
			Doc("delete a projectmanagement").
			Returns(200, "OK", struct{}{}))
	apiV1Ws.Route(
		apiV1Ws.PUT(URLProjectManagementDetails).
			To(apiHandler.handleUpdateProjectManagement).
			Writes(v1alpha1.ProjectManagement{}).
			Doc("update a projectmanagement").
			Returns(200, "OK", v1alpha1.ProjectManagement{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/projectmanagement").
			To(apiHandler.handleGetProjectManagementList).
			Writes(projectmanagement.ProjectManagementList{}).
			Doc("get projectmanagement list").
			Returns(200, "OK", projectmanagement.ProjectManagementList{}))
	apiV1Ws.Route(
		apiV1Ws.GET(URLProjectManagementDetails).
			To(apiHandler.handleGetProjectManagementDetail).
			Writes(v1alpha1.ProjectManagement{}).
			Doc("get a projectmanagement").
			Returns(200, "OK", v1alpha1.ProjectManagement{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/projectmanagementbinding/{namespace}").
			To(apiHandler.handleCreateProjectManagementBinding).
			Writes(v1alpha1.ProjectManagementBinding{}).
			Doc("create a projectmanagementbinding").
			Returns(200, "OK", v1alpha1.ProjectManagementBinding{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE(URLProjectManagementBindingDetails).
			To(apiHandler.handleDeleteProjectManagementBinding).
			Writes(struct{}{}).
			Doc("delete a projectmanagementbinding").
			Returns(200, "OK", struct{}{}))
	apiV1Ws.Route(
		apiV1Ws.PUT(URLProjectManagementBindingDetails).
			To(apiHandler.handleUpdateProjectManagementBinding).
			Writes(v1alpha1.ProjectManagementBinding{}).
			Doc("update a projectmanagementbinding").
			Returns(200, "OK", v1alpha1.ProjectManagementBinding{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/projectmanagementbinding").
			To(apiHandler.handleGetProjectManagementBindingList).
			Writes(projectmanagementbinding.ProjectManagementBindingList{}).
			Doc("get projectmanagementbinding list in all namespaces").
			Returns(200, "OK", projectmanagementbinding.ProjectManagementBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/projectmanagementbinding/{namespace}").
			To(apiHandler.handleGetProjectManagementBindingList).
			Writes(projectmanagementbinding.ProjectManagementBindingList{}).
			Doc("get projectmanagementbinding list in one namespace").
			Returns(200, "OK", projectmanagementbinding.ProjectManagementBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET(URLProjectManagementBindingDetails).
			To(apiHandler.handleGetProjectManagementBindingDetail).
			Writes(v1alpha1.ProjectManagementBinding{}).
			Doc("get a projectmanagementbinding").
			Returns(200, "OK", v1alpha1.ProjectManagementBinding{}))
	// endregion

	// region TestTool
	apiV1Ws.Route(
		apiV1Ws.POST("/testtool").
			To(apiHandler.handleCreateTestTool).
			Writes(v1alpha1.TestTool{}).
			Doc("create a testtool").
			Returns(200, "OK", v1alpha1.TestTool{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE(URLTestToolDetails).
			To(apiHandler.handleDeleteTestTool).
			Writes(struct{}{}).
			Doc("delete a testtool").
			Returns(200, "OK", struct{}{}))
	apiV1Ws.Route(
		apiV1Ws.PUT(URLTestToolDetails).
			To(apiHandler.handleUpdateTestTool).
			Writes(v1alpha1.TestTool{}).
			Doc("update a testtool").
			Returns(200, "OK", v1alpha1.TestTool{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/testtool").
			To(apiHandler.handleGetTestToolList).
			Writes(testtool.TestToolList{}).
			Doc("get testtool list").
			Returns(200, "OK", testtool.TestToolList{}))
	apiV1Ws.Route(
		apiV1Ws.GET(URLTestToolDetails).
			To(apiHandler.handleGetTestToolDetail).
			Writes(v1alpha1.TestTool{}).
			Doc("get a testtool").
			Returns(200, "OK", v1alpha1.TestTool{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/testtoolbinding/{namespace}").
			To(apiHandler.handleCreateTestToolBinding).
			Writes(v1alpha1.TestToolBinding{}).
			Doc("create a testtoolbinding").
			Returns(200, "OK", v1alpha1.TestToolBinding{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE(URLTestToolBindingDetails).
			To(apiHandler.handleDeleteTestToolBinding).
			Writes(struct{}{}).
			Doc("delete a testtoolbinding").
			Returns(200, "OK", struct{}{}))
	apiV1Ws.Route(
		apiV1Ws.PUT(URLTestToolBindingDetails).
			To(apiHandler.handleUpdateTestToolBinding).
			Writes(v1alpha1.TestToolBinding{}).
			Doc("update a testtoolbinding").
			Returns(200, "OK", v1alpha1.TestToolBinding{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/testtoolbinding").
			To(apiHandler.handleGetTestToolBindingList).
			Writes(testtoolbinding.TestToolBindingList{}).
			Doc("get testtoolbinding list in all namespaces").
			Returns(200, "OK", testtoolbinding.TestToolBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/testtoolbinding/{namespace}").
			To(apiHandler.handleGetTestToolBindingList).
			Writes(testtoolbinding.TestToolBindingList{}).
			Doc("get testtoolbinding list in one namespace").
			Returns(200, "OK", testtoolbinding.TestToolBindingList{}))
	apiV1Ws.Route(
		apiV1Ws.GET(URLTestToolBindingDetails).
			To(apiHandler.handleGetTestToolBindingDetail).
			Writes(v1alpha1.TestToolBinding{}).
			Doc("get a testtoolbinding").
			Returns(200, "OK", v1alpha1.TestToolBinding{}))
	// endregion

	apiV1Ws.Route(
				apiV1Ws.GET("/microservicesconfigs").
						Writes(microservicesconfiguration.MicroservicesConfigurationList{}).
							To(apiHandler.handleGetMicroservicesConfigs).
						Doc("get microservicesenvironments detail by name").
							Returns(200, "OK", microservicesconfiguration.MicroservicesConfigurationList{}))
		 

	return wsContainer, nil
}

const (
	URLProjectManagementDetails        = "/projectmanagement/{name}"
	URLProjectManagementBindingDetails = "/projectmanagementbinding/{namespace}/{name}"
	URLTestToolDetails                 = "/testtool/{name}"
	URLTestToolBindingDetails          = "/testtoolbinding/{namespace}/{name}"
)
