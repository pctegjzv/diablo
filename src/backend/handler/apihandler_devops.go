package handler

import (
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/emicklei/go-restful"
	"github.com/golang/glog"

	"alauda.io/diablo/src/backend/api"
	kdErrors "alauda.io/diablo/src/backend/errors"
	"alauda.io/diablo/src/backend/resource/application"
	"alauda.io/diablo/src/backend/resource/clusterpipelinetemplate"
	"alauda.io/diablo/src/backend/resource/coderepobinding"
	"alauda.io/diablo/src/backend/resource/codereposervice"
	"alauda.io/diablo/src/backend/resource/coderepository"
	"alauda.io/diablo/src/backend/resource/common"
	"alauda.io/diablo/src/backend/resource/configmap"
	"alauda.io/diablo/src/backend/resource/dataselect"
	"alauda.io/diablo/src/backend/resource/imageregistry"
	"alauda.io/diablo/src/backend/resource/imageregistrybinding"
	"alauda.io/diablo/src/backend/resource/imagerepository"
	"alauda.io/diablo/src/backend/resource/jenkins"
	"alauda.io/diablo/src/backend/resource/jenkinsbinding"
	"alauda.io/diablo/src/backend/resource/pipeline"
	"alauda.io/diablo/src/backend/resource/pipelineconfig"
	"alauda.io/diablo/src/backend/resource/pipelinetasktemplate"
	"alauda.io/diablo/src/backend/resource/pipelinetemplate"
	"alauda.io/diablo/src/backend/resource/pipelinetemplatesync"
	"alauda.io/diablo/src/backend/resource/project"
	"alauda.io/diablo/src/backend/resource/projectmanagement"
	"alauda.io/diablo/src/backend/resource/projectmanagementbinding"
	"alauda.io/diablo/src/backend/resource/release"
	"alauda.io/diablo/src/backend/resource/secret"
	"alauda.io/diablo/src/backend/resource/testtool"
	"alauda.io/diablo/src/backend/resource/testtoolbinding"
	"alauda.io/diablo/src/backend/resource/toolchain"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

const (
	// PATH_NAMESPACE const for restful path variable namespace
	PATH_NAMESPACE string = "namespace"
	// PATH_NAME const for restful path variable name
	PATH_NAME string = "name"
)

// configuration
func (apiHandler *APIHandler) handleGetPlatformConfiguration(request *restful.Request, response *restful.Response) {
	// k8sClient, err := apiHandler.cManager.Client(request)
	// forcing to get the configuration
	// instead of using user permission
	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := "alauda-system"
	name := "global-configmap"
	// dataSelect := parseDataSelectPathParameter(request)
	// dataSelect.MetricQuery = dataselect.StandardMetrics
	result, err := configmap.GetConfigMapDetail(k8sClient, namespace, name)
	// result, err := resourceService.GetServicePods(k8sClient, apiHandler.iManager.Metric().Client(), namespace, name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// DevopsAPIs
func (apiHandler *APIHandler) handleGetProjects(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	// TODO: Add devops client here
	result, err := project.GetProjectList(devopsClient, k8sClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// region TODO: Add in the future if necessary
// func (apiHandler *APIHandler) handleGetNamespaceDetail(request *restful.Request, response *restful.Response) {
// 	k8sClient, err := apiHandler.cManager.Client(request)
// 	if err != nil {
// 		kdErrors.HandleInternalError(response, err)
// 		return
// 	}
// 	name := request.PathParameter("name")
// 	result, err := ns.GetNamespaceDetail(k8sClient, name)
// 	if err != nil {
// 		kdErrors.HandleInternalError(response, err)
// 		return
// 	}
// 	response.WriteHeaderAndEntity(http.StatusOK, result)
// }
// endregion

// region Jenkins
func (apiHandler *APIHandler) handleGetJenkins(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	// TODO: Add devops client here
	result, err := jenkins.GetJenkinsList(devopsClient, k8sClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleRetriveJenkins(request *restful.Request, response *restful.Response) {
	name := request.PathParameter("name")

	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := jenkins.RetrieveJenkins(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
	return
}

func (apiHandler *APIHandler) handlePutJenkins(request *restful.Request, response *restful.Response) {
	name := request.PathParameter("name")

	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	newJenkins := new(v1alpha1.Jenkins)
	if err := request.ReadEntity(newJenkins); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := jenkins.UpdateJenkins(devopsClient, newJenkins, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
	return
}

func (apiHandler *APIHandler) handleCreateJenkins(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	newJenkins := new(v1alpha1.Jenkins)
	if err := request.ReadEntity(newJenkins); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := jenkins.CreateJenkins(devopsClient, newJenkins)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusCreated, result)
}

func (apiHandler *APIHandler) handleDeleteJenkins(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	err = jenkins.DeleteJenkins(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleDeleteJenkinsBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = jenkinsbinding.DeleteJenkinsBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleGetJenkinsBindingList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := jenkinsbinding.GetJenkinsBindingList(devopsClient, k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region Application
func (apiHandler *APIHandler) handleGetApplicationsList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)

	// set the default sort by
	if len(dataSelect.SortQuery.SortByList) == 0 {
		sortBy := dataselect.SortBy{
			Property:  dataselect.PropertyName("name"),
			Ascending: true,
		}

		dataSelect.SortQuery.SortByList = append(dataSelect.SortQuery.SortByList, sortBy)
	}

	dataSelect.MetricQuery = dataselect.StandardMetrics
	namespace := parseNamespacePathParameter(request)
	result, err := application.GetApplicationList(k8sClient, namespace, dataSelect, devopsClient, apiHandler.iManager.Metric().Client())
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetApplicationDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := application.GetApplicationDetail(k8sClient, namespace, name, devopsClient, apiHandler.iManager.Metric().Client())
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetApplicationYAML(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	catalogClient, err := apiHandler.cManager.CatalogClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := application.GetApplicationYAML(k8sClient, devopsClient, catalogClient, apiHandler.iManager.Metric().Client(), namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeleteApplication(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	catalogClient, err := apiHandler.cManager.CatalogClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	spec := new(application.DeleteAppYAMLSpec)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	cc := &application.ClientCollection{
		K8sClient:     k8sClient,
		DevopsClient:  devopsClient,
		CatalogClient: catalogClient,
		MetricClient:  apiHandler.iManager.Metric().Client(),
		ClientMgr:     apiHandler.cManager,
	}
	ops, err := application.DeleteApplicationYaml(request, cc, namespace, name, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	err = release.DeleteRelease(catalogClient, namespace, name)
	log.Println(err)
	//releaseDetails, err := release.GetReleaseDetail(catalogClient, namespace, name)
	//if err == nil && releaseDetails != nil {
	//	op := application.NewResourceOp("delete", nil, releaseDetails, nil, k8sClient)
	//	log.Println("err: ", op.Error)
	//	if op.Error == nil {
	//		err = release.DeleteRelease(catalogClient, namespace, name)
	//		if err != nil {
	//			op.Error = err
	//		}
	//	}
	//	ops = append(ops, op)
	//}

	response.WriteHeaderAndEntity(http.StatusOK, ops)
}

func (apiHandler *APIHandler) handleUpdateApplicationYAML(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	catalogClient, err := apiHandler.cManager.CatalogClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(application.UpdateAppYAMLSpec)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	cc := &application.ClientCollection{
		K8sClient:     k8sClient,
		DevopsClient:  devopsClient,
		CatalogClient: catalogClient,
		MetricClient:  apiHandler.iManager.Metric().Client(),
		ClientMgr:     apiHandler.cManager,
	}
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	ops, err := application.UpdateApplicationYAML(request, cc, namespace, name, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, ops)
}

// endregion

// region PipelineTemplateSync

func (apiHandler *APIHandler) handleGetPipelineTemplateSyncList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := pipelinetemplatesync.GetPipelineTemplateSyncList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPipelineTemplateSync(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := pipelinetemplatesync.GetPipelineTemplateSync(devopsClient, k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleCreatePipelineTemplateSync(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(pipelinetemplatesync.PipelineTemplateSync)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec.ObjectMeta.Name = "TemplateSync"
	namespace := request.PathParameter("namespace")
	result, err := pipelinetemplatesync.CreatePipelineTemplateSync(devopsClient, namespace, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleUpdatePipelineTemplateSync(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(pipelinetemplatesync.PipelineTemplateSync)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := pipelinetemplatesync.UpdatePipelineTemplateSync(devopsClient, namespace, name, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeletePipelineTemplateSync(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = pipelinetemplatesync.DeletePipelineTemplateSync(devopsClient, k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

// endregion

// region PipelineTaskTemplate

func (apiHandler *APIHandler) handleGetPipelineTaskTemplateList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter(PATH_NAMESPACE)
	result, err := pipelinetasktemplate.GetPipelineTaskTemplateList(devopsClient, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPipelineTaskTemplate(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter(PATH_NAMESPACE)
	name := request.PathParameter(PATH_NAME)
	result, err := pipelinetasktemplate.GetPipelineTaskTemplate(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region PipelineTemplate

func (apiHandler *APIHandler) handleGetPipelineTemplateList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter(PATH_NAMESPACE)
	dataselect := parseDataSelectPathParameter(request)
	result, err := pipelinetemplate.GetPipelineTemplateList(devopsClient, namespace, dataselect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPipelineTemplate(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter(PATH_NAMESPACE)
	name := request.PathParameter(PATH_NAME)
	result, err := pipelinetemplate.GetPipelineTemplate(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handlePreviewPipelineTemplate(request *restful.Request, response *restful.Response) {
	options := new(pipelinetemplate.PreviewOptions)
	if err := request.ReadEntity(options); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")

	result, err := pipelinetemplate.RenderJenkinsfile(devopsClient, namespace, name, options)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region ClusterPipelineConfig
func (apiHandler *APIHandler) handleGetClusterPipelineTemplateList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	dataselect := parseDataSelectPathParameter(request)
	result, err := clusterpipelinetemplate.GetClusterPipelineTemplateList(devopsClient, namespace, dataselect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetClusterPipelineTemplate(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")

	result, err := clusterpipelinetemplate.GetClusterPipelineTemplate(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region PipelineConfig

func (apiHandler *APIHandler) handleGetPipelineConfigList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := pipelineconfig.GetPipelineConfigList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleCreatePipelineConfig(request *restful.Request, response *restful.Response) {
	// k8sClient, err := apiHandler.cManager.Client(request)
	// if err != nil {
	// 	kdErrors.HandleInternalError(response, err)
	// 	return
	// }
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	// namespace := request.PathParameter("namespace")
	// name := request.PathParameter("name")
	spec := new(pipelineconfig.PipelineConfigDetail)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := pipelineconfig.CreatePipelineConfigDetail(devopsClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPipelineConfigDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := pipelineconfig.GetPipelineConfigDetail(devopsClient, k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleUpdatePipelineConfig(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(pipelineconfig.PipelineConfigDetail)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := pipelineconfig.UpdatePipelineConfigDetail(devopsClient, k8sClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeletePipelineConfig(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = pipelineconfig.DeletePipelineConfig(devopsClient, k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleTriggerPipelineConfig(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	spec := new(pipelineconfig.PipelineConfigTrigger)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	spec.Namespace = namespace
	spec.Name = name
	result, err := pipelineconfig.TriggerPipelineConfig(devopsClient, k8sClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handlePreviewPipelineConfig(request *restful.Request, response *restful.Response) {
	spec := new(pipelineconfig.PipelineConfigDetail)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")

	result, err := pipelineconfig.RenderJenkinsfile(devopsClient, namespace, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region Pipeline

func (apiHandler *APIHandler) handleGetPipelineList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := pipeline.GetPipelineList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPipelineDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := pipeline.GetPipelineDetail(devopsClient, k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeletePipeline(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = pipeline.DeletePipeline(devopsClient, k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleRetryPipelineDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	spec := new(pipeline.RetryRequest)
	// if err := request.ReadEntity(spec); err != nil {
	// 	log.Println("error reading body", err)
	// 	kdErrors.HandleInternalError(response, err)
	// 	return
	// }
	spec.Namespace = namespace
	spec.Name = name
	result, err := pipeline.RetryPipeline(devopsClient, k8sClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleAbortPipeline(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	spec := pipeline.AbortRequest{
		Namespace: namespace,
		Name:      name,
	}
	result, err := pipeline.AbortPipeline(devopsClient, k8sClient, &spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handlePipelineLogs(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")

	start, err := strconv.Atoi(request.QueryParameter("start"))
	if err != nil {
		log.Println("Error parsing start query parameter: original ", request.QueryParameter("start"), "err:", err)
		start = 0
	}
	stage, err := strconv.Atoi(request.QueryParameter("stage"))
	if err != nil {
		log.Println("Error parsing stage query parameter: original ", request.QueryParameter("stage"), "err:", err)
		stage = 0
	}
	step, err := strconv.Atoi(request.QueryParameter("step"))
	if err != nil {
		log.Println("Error parsing step query parameter: original ", request.QueryParameter("step"), "err:", err)
		step = 0
	}

	result, err := pipeline.GetLogDetails(devopsClient, namespace, name, start, stage, step)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handlePipelineTasks(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")

	stage, err := strconv.Atoi(request.QueryParameter("stage"))
	if err != nil {
		stage = 0
	}

	result, err := pipeline.GetTaskDetails(devopsClient, namespace, name, stage)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region CodeRepoService

func (apiHandler *APIHandler) handleCreateCodeRepoService(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.CodeRepoService)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := codereposervice.CreateCodeRepoService(devopsClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusCreated, result)
}

func (apiHandler *APIHandler) handleDeleteCodeRepoService(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	err = codereposervice.DeleteCodeRepoService(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateCodeRepoService(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	spec := new(v1alpha1.CodeRepoService)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := codereposervice.UpdateCodeRepoService(devopsClient, spec, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoServiceDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	result, err := codereposervice.GetCodeRepoService(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoServiceList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := codereposervice.GetCodeRepoServiceList(devopsClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoServiceResourceList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	namespace := parseNamespacePathParameter(request)
	repositoryQuery := dataselect.GeSimpletLabelQuery(dataselect.CodeRepoServiceProperty, name)
	result, err := coderepository.GetResourceList(devopsClient, namespace, repositoryQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoServiceSecretList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	namespace := parseNamespacePathParameter(request)
	bindingQuery := dataselect.GeSimpletLabelQuery(dataselect.CodeRepoServiceProperty, name)
	bindingList, err := coderepobinding.GetCodeRepoBindingList(devopsClient, namespace, bindingQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	secretQuery := parseDataSelectPathParameter(request)
	secretList, err := secret.GetSecretList(k8sClient, namespace, secretQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	var secrets []secret.Secret
	for _, binding := range bindingList.Items {
		for _, se := range secretList.Secrets {
			if binding.Spec.Account.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
				secrets = append(secrets, se)
			}
		}
	}

	result := secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: len(secrets)},
		Secrets:  secrets,
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region CodeRepoBinding

func (apiHandler *APIHandler) handleCreateCodeRepoBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.CodeRepoBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")

	authorizeResponse, err := codereposervice.AuthorizeService(devopsClient, spec.Spec.CodeRepoService.Name, spec.Spec.Account.Secret.Name, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
		response.WriteHeaderAndEntity(http.StatusPaymentRequired, authorizeResponse)
		return
	}

	result, err := coderepobinding.CreateCodeRepoBinding(devopsClient, spec, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusCreated, result)
}

func (apiHandler *APIHandler) handleDeleteCodeRepoBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = coderepobinding.DeleteCodeRepoBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateCodeRepoBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	log.Println(request.PathParameters())

	spec := new(v1alpha1.CodeRepoBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	authorizeResponse, err := codereposervice.AuthorizeService(devopsClient, spec.Spec.CodeRepoService.Name, spec.Spec.Account.Secret.Name, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
		response.WriteHeaderAndEntity(http.StatusPaymentRequired, authorizeResponse)
		return
	}

	oldBinding, err := coderepobinding.GetCodeRepoBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	_, err = coderepobinding.UpdateCodeRepoBinding(devopsClient, oldBinding, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	newBinding, err := coderepobinding.GetCodeRepoBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result := coderepository.GetResourcesReferToRemovedRepos(devopsClient, oldBinding, newBinding)
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoBindingDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := coderepobinding.GetCodeRepoBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoBindingList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := coderepobinding.GetCodeRepoBindingList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepositoryList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := coderepository.GetCodeRepositoryList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepositoryListInBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := coderepository.GetCodeRepositoryListInBinding(devopsClient, namespace, name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoBindingResourceList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	namespace := parseNamespacePathParameter(request)
	repositoryQuery := dataselect.GeSimpletLabelQuery(dataselect.CodeRepoBindingProperty, name)
	result, err := coderepository.GetResourceList(devopsClient, namespace, repositoryQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCodeRepoBindingSecretList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	binding, err := coderepobinding.GetCodeRepoBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespaceQuery := common.NewSameNamespaceQuery(namespace)
	secretQuery := parseDataSelectPathParameter(request)
	secretList, err := secret.GetSecretList(k8sClient, namespaceQuery, secretQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	var secrets []secret.Secret
	for _, se := range secretList.Secrets {
		if binding.Spec.Account.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
			secrets = append(secrets, se)
			break
		}
	}

	result := secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: 1},
		Secrets:  secrets,
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetRemoteRepositoryList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := coderepobinding.GetRemoteRepositoryList(devopsClient, namespace, name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region ToolChain
func (apiHandler *APIHandler) handleGetToolChains(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	toolType := request.QueryParameter("tool_type")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := toolchain.GetToolChainList(devopsClient, k8sClient, dataSelect, toolType)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetToolChainBindings(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	toolType := request.QueryParameter("tool_type")
	namespace := request.PathParameter("namespace")
	namespaceQuery := common.NewSameNamespaceQuery(namespace)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := toolchain.GetToolChainBindingList(devopsClient, k8sClient, dataSelect, namespaceQuery, toolType)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

func (apiHandler *APIHandler) handleOAuthCallback(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	secretName := request.PathParameter("secretName")
	serviceName := request.PathParameter("serviceName")
	code := request.QueryParameter("code")

	// addition message from bitbucket
	errorDescription := request.QueryParameter("error_description")
	if code != "" {
		err = secret.OAuthCallback(devopsClient, k8sClient, namespace, secretName, serviceName, code)
	} else if errorDescription != "" {
		err = errors.New(errorDescription)
	} else {
		err = errors.New("except 'code' from third party, but not")
	}

	if err != nil {
		glog.Error(err)
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, nil)
}

// region ImageRegistry
func (apiHandler *APIHandler) handleCreateImageRegistry(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.ImageRegistry)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := imageregistry.CreateImageRegistry(devopsClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusCreated, result)
}

func (apiHandler *APIHandler) handleDeleteImageRegsitry(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	err = imageregistry.DeleteImageRegistry(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateImageRegistry(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	spec := new(v1alpha1.ImageRegistry)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := imageregistry.UpdateImageRegistry(devopsClient, spec, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRegistryDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	result, err := imageregistry.GetImageRegistry(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRegistryList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := imageregistry.GetImageRegistryList(devopsClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRegistrySecretList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	namespace := parseNamespacePathParameter(request)
	bindingQuery := dataselect.GeSimpletLabelQuery(dataselect.ImageRegistryProperty, name)
	bindingList, err := imageregistrybinding.GetImageRegistryBindingList(devopsClient, namespace, bindingQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	secretQuery := parseDataSelectPathParameter(request)
	secretList, err := secret.GetSecretList(k8sClient, namespace, secretQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	var secrets []secret.Secret
	for _, binding := range bindingList.Items {
		for _, se := range secretList.Secrets {
			if binding.Spec.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
				secrets = append(secrets, se)
			}
		}
	}

	result := secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: len(secrets)},
		Secrets:  secrets,
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region ImageRegistryBinding

func (apiHandler *APIHandler) handleCreateImageRegistryBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.ImageRegistryBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	result, err := imageregistrybinding.CreateImageRegistryBinding(devopsClient, spec, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusCreated, result)
}

func (apiHandler *APIHandler) handleDeleteImageRegistryBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = imageregistrybinding.DeleteImageRegistryBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateImageRegistryBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	log.Println(request.PathParameters())

	spec := new(v1alpha1.ImageRegistryBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	oldBinding, err := imageregistrybinding.GetImageRegistryBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	_, err = imageregistrybinding.UpdateImageRegistryBinding(devopsClient, oldBinding, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	//newBinding, err := imageregistrybinding.GetImageRegistryBinding(devopsClient, namespace, name)
	//if err != nil {
	//  kdErrors.HandleInternalError(response, err)
	//  return
	//}
	//
	//// todo old and new has no difference
	//result := imagerepository.GetResourcesReferToRemovedRepos(devopsClient, oldBinding, newBinding)
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleGetImageRegistryBindingDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := imageregistrybinding.GetImageRegistryBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRegistryBindingList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := imageregistrybinding.GetImageRegistryBindingList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRepositoryList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := imagerepository.GetImageRepositoryList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRepositoryListInBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := imagerepository.GetImageRepositoryListInBinding(devopsClient, namespace, name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRegistryBindingSecretList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	binding, err := imageregistrybinding.GetImageRegistryBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespaceQuery := common.NewSameNamespaceQuery(namespace)
	secretQuery := parseDataSelectPathParameter(request)
	secretList, err := secret.GetSecretList(k8sClient, namespaceQuery, secretQuery)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	var secrets []secret.Secret
	for _, se := range secretList.Secrets {
		if binding.Spec.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
			secrets = append(secrets, se)
			break
		}
	}

	result := secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: 1},
		Secrets:  secrets,
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageOriginRepositoryList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := imageregistrybinding.GetImageOriginRepositoryList(devopsClient, namespace, name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region ImageRepository

func (apiHandler *APIHandler) handleGetImageRepositoryTagList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	namespace := request.PathParameter("namespace")
	result, err := imagerepository.GetImageTagList(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetImageRepositoryDetail(request *restful.Request, response *restful.Response) {
  devopsClient, err := apiHandler.cManager.DevOpsClient(request)
  if err != nil {
    kdErrors.HandleInternalError(response, err)
    return
  }
  name := request.PathParameter("name")
  namespace := request.PathParameter("namespace")
  result, err := imagerepository.GetImageRepository(devopsClient, namespace, name)
  if err != nil {
    kdErrors.HandleInternalError(response, err)
    return
  }
  response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region ProjectManagement

func (apiHandler *APIHandler) handleCreateProjectManagement(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.ProjectManagement)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := projectmanagement.CreateProjectManagement(devopsClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeleteProjectManagement(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	err = projectmanagement.DeleteProjectManagement(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateProjectManagement(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	spec := new(v1alpha1.ProjectManagement)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := projectmanagement.UpdateProjectManagement(devopsClient, spec, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetProjectManagementDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	result, err := projectmanagement.GetProjectManagement(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetProjectManagementList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := projectmanagement.GetProjectManagementList(devopsClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleCreateProjectManagementBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.ProjectManagementBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	result, err := projectmanagementbinding.CreateProjectManagementBinding(devopsClient, spec, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeleteProjectManagementBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = projectmanagementbinding.DeleteProjectManagementBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateProjectManagementBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	spec := new(v1alpha1.ProjectManagementBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := projectmanagementbinding.UpdateProjectManagementBinding(devopsClient, spec, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetProjectManagementBindingDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := projectmanagementbinding.GetProjectManagementBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetProjectManagementBindingList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := projectmanagementbinding.GetProjectManagementBindingList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion

// region TestTool

func (apiHandler *APIHandler) handleCreateTestTool(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.TestTool)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := testtool.CreateTestTool(devopsClient, spec)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeleteTestTool(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	err = testtool.DeleteTestTool(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateTestTool(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	spec := new(v1alpha1.TestTool)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := testtool.UpdateTestTool(devopsClient, spec, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetTestToolDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	result, err := testtool.GetTestTool(devopsClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetTestToolList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := testtool.GetTestToolList(devopsClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleCreateTestToolBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	spec := new(v1alpha1.TestToolBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	result, err := testtoolbinding.CreateTestToolBinding(devopsClient, spec, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleDeleteTestToolBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	err = testtoolbinding.DeleteTestToolBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, struct{}{})
}

func (apiHandler *APIHandler) handleUpdateTestToolBinding(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	spec := new(v1alpha1.TestToolBinding)
	if err := request.ReadEntity(spec); err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := testtoolbinding.UpdateTestToolBinding(devopsClient, spec, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetTestToolBindingDetail(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := testtoolbinding.GetTestToolBinding(devopsClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetTestToolBindingList(request *restful.Request, response *restful.Response) {
	devopsClient, err := apiHandler.cManager.DevOpsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := testtoolbinding.GetTestToolBindingList(devopsClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// endregion
